/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.ProductCRUD;

import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ProductLogic {

    ProductCRUD p4 = new ProductCRUD();
  //  ProductStockCRUD ps4 = new ProductStockCRUD();

    public boolean createProduct(Long companyId, Product newProduct, List<GoDownStockDetail> gsds) {

        EventStatus result = new EventStatus();
        result = p4.createProduct(companyId, newProduct, gsds);
        result.isCreateDone();

        return result.isCreateDone();
    }

    public List<Product> fetchAllProducts(Long companyId) {
        return p4.fetchAllProducts(companyId);
    }
    public List<Product> fetchAllProductsWithoutService(Long companyId) {
        return p4.fetchAllProductsWithoutService(companyId);
    }

    public boolean updateProduct(Product updatedProduct, ProductStockEntry updatedStockEntry, Long companyId) {

        EventStatus result = new EventStatus();
        result = p4.updateProduct(companyId, updatedProduct);
        return result.isUpdateDone();
    }

    public ProductStockEntry getOpeningStock(Long productId) {
        return p4.getOpeningStock(productId);
    }

    public EventStatus deleteProduct(Long productId) {
        return p4.deleteProduct(productId);
    }
    
    public Product getProduct(Long productId){
         return p4.getProduct(productId);
    }
    
    public EventStatus updateProductDescription(Product updateProduct){
        return p4.updateProductDescription(updateProduct);
    }
}
