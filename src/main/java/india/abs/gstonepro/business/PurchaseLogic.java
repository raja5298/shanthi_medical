/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.PurchaseInvoiceCRUD;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SGS
 */
public class PurchaseLogic {

    PurchaseInvoiceCRUD pi4 = new PurchaseInvoiceCRUD();

    public EventStatus createPurchase(PurchaseInvoice pi, List<PurchaseInvoiceLineItem> piLis) {
        EventStatus result = pi4.createPurchaseInvoice(null, SessionDataUtil.getSelectedCompany(), pi, piLis);
        return result;
    }

    public List<PurchaseInvoice> fetchAllPurchaseByDate(Date fromDate, Date toDate) {
        return pi4.getAllPurchaseInvoiceByDate(fromDate, toDate, SessionDataUtil.getSelectedCompany());
    }

    public List<PurchaseInvoice> getAllPurchase(String partyId, Date fromDate, Date toDate) {
        
        if (partyId.equalsIgnoreCase("ALL")) {
            return pi4.getAllPurchaseInvoiceByDate(fromDate, toDate, SessionDataUtil.getSelectedCompany());
        } else {
            return pi4.getAllPurchaseInvoiceByDateAndLedger(Long.parseLong(partyId), fromDate, toDate, SessionDataUtil.getSelectedCompany());
        }
    }
    
   public List<PurchaseInvoiceLineItem> getAllPurchaseByProduct(Date fromDate, Date toDate, Product product) {
        return pi4.getAllPurchaseByProduct(fromDate, toDate, product,SessionDataUtil.getSelectedCompany());
    }
    public PurchaseInvoice fetchAPurchaseInvoice(String piId) {
        return pi4.getPurchaseInvoice(piId);

    }

    public EventStatus deletePurchase(String piId) {
        EventStatus result = pi4.deletePurchaseInvoice(null, piId);
        return result;
    }

    public EventStatus updDatePurchase(PurchaseInvoice pi, List<PurchaseInvoiceLineItem> piLis) {
        EventStatus result = pi4.updatePurchaseInvoice(SessionDataUtil.getSelectedCompany(), pi, piLis);
        return result;
    }
    
    public List<PurchaseInvoice> fetchAllPurchaseByParty(Ledger ledger) {
        return pi4.getAllPurchaseInvoiceByParty(ledger);
    }
}
