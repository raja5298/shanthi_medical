/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.GoDownCRUD;
import india.abs.gstonepro.api.dao.GoDownStockCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.models.StockReportModel;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class GoDownLogic {

    GoDownCRUD go4 = new GoDownCRUD();
    GoDownStockCRUD gos4 = new GoDownStockCRUD();

    public List<GoDown> fetchAllGodowns(Long companyId) {
        return go4.fetchAllGodowns(companyId);
    }

    public List<GoDownStockDetail> fetchAllProductsGSDs(Long godownId) {
        return gos4.fetchProductStockDetailsByGoDown(godownId);
    }

    public List<GoDownStockDetail> fetchAllOpeningStocks(Long productId) {
        return gos4.fetchAllOpeningStocks(productId);
    }

    public EventStatus updateOpeningStock(GoDownStockDetail gsd) {
        return gos4.updateOpeningStock(gsd);
    }
    
    public EventStatus createGoDown(GoDown newGoDown, Company company) {
        return go4.createGoDown(newGoDown,company);
    }

    public EventStatus manualStockUpdate(Set<GoDownStockEntry> gses, String message, Date updateDate) {
        return gos4.manualGoDownStockUpdate(gses, message, updateDate, SessionDataUtil.getSelectedCompany());
    }

    public GoDownStockDetail getGoDownStockDetail(Long productId, Long godownId) {
        return gos4.getGoDownStockDetail(productId, godownId);
    }

    public Set<StockReportModel> getClosingStock(Date toDate) {
        Date finStartDate = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFinancialYearStart();

        return gos4.fetchClosingStock(finStartDate, toDate, SessionDataUtil.getSelectedCompany());
    }
    
    public Set<StockReportModel> getTransactionSummaryReport(Date fromDate,Date toDate,GoDown godown) {
        Date finStartDate = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFinancialYearStart();
        return gos4.fetchTransactionSummary(SessionDataUtil.getSelectedCompany(),fromDate,toDate,godown);
    }
    
    public Set<StockReportModel> getProductTransactionReport(Date fromDate,Date toDate,Product product) {
        Date finStartDate = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFinancialYearStart();
        return null;
    }
    
    public Map<Date, StockReportModel> fetchProductTransactionSummary( Date fromDate, Date toDate, Product pro) {
        
        return gos4.fetchProductTransactionSummary(SessionDataUtil.getSelectedCompany(), fromDate, toDate, pro);
    }
    public Map<String,StockReportModel> fetchProductsSoldOut(){
        return gos4.fetchProductsSoldOut();
    }
}
