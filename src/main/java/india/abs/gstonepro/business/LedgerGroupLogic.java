/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.LedgerGroupCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.List;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class LedgerGroupLogic {

    LedgerGroupCRUD lg4 = new LedgerGroupCRUD();

    public List<LedgerGroup> fetchAllLedgerGroups() {

        return lg4.fetchAllLedgerGroups(SessionDataUtil.getSelectedCompany());
    }

    public LedgerGroup createLedgerGroup(LedgerGroup newLedgerGroup) {

        LedgerGroup resultGroup = lg4.createLedgerGroup(SessionDataUtil.getSelectedCompany(), newLedgerGroup);
        return resultGroup;
    }

    public boolean createLedgerGroupWithCompany(Company company, LedgerGroup newLedgerGroup) {
        Boolean isCreate = false;
        LedgerGroup resultGroup = lg4.createLedgerGroup(SessionDataUtil.getSelectedCompany(), newLedgerGroup);
        if (resultGroup != null) {
            isCreate = true;
        }
        return isCreate;
    }

    public boolean updateLedgerGroup(Long lgId, String updatedName, Set<Ledger> childrenLedgers) {
        EventStatus result = lg4.updateLedgerGroup(lgId, updatedName, childrenLedgers);
        return result.isUpdateDone();
    }

    public boolean deleteLedgerGroup(Long lgId) {
        EventStatus result = lg4.deleteLedgerGroup(lgId);
        return result.isDeleteDone();
    }

    public LedgerGroup fetchLedgerGroupByName(String name, Long CompanyId) {
        return lg4.fetchLedgerGroupByName(CompanyId, name);
    }
}
