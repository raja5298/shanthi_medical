/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.GSTOneReport;
import india.abs.gstonepro.api.models.GstReport;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SGS
 */
public class GSTOneReportsLogic {

    GSTOneReport gst1 = new GSTOneReport();

    public List<GstReport> getB2B(Date fromDate, Date toDate) {
        return gst1.getB2BDetails(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getB2C(Date fromDate, Date toDate) {
        return gst1.getB2CDetails(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getB2CL(Date fromDate, Date toDate) throws ParseException {
        return gst1.getB2CLDetails(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getHSN(Date fromDate, Date toDate) {
        return gst1.getHSNSummary(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getCdnr(Date fromDate, Date toDate) {
        return gst1.getCdnr(SessionDataUtil.getSelectedCompany(), fromDate, toDate, "CREDIT-NOTE");
    }

    public List<GstReport> getCdnUr(Date fromDate, Date toDate) {
        return gst1.getCdnUr(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getGst2B2B(Date fromDate, Date toDate) {
        return gst1.getGst2B2BDetails(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }

    public List<GstReport> getGst2Cdnr(Date fromDate, Date toDate) {
        return gst1.getCdnr(SessionDataUtil.getSelectedCompany(), fromDate, toDate, "DEBIT-NOTE");
    }
    
     public List<GstReport> getPurchaseHSN(Date fromDate, Date toDate) {
        return gst1.getPurchaseHSNSummary(SessionDataUtil.getSelectedCompany(), fromDate, toDate);
    }
}
