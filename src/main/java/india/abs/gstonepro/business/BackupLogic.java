/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.BackupRestoreCRUD;

/**
 *
 * @author SGS
 */
public class BackupLogic {

    BackupRestoreCRUD br4 = new BackupRestoreCRUD();

    public boolean backUPDB(String filePath) {
        return br4.backupH2DB(filePath);
    }

    public boolean restoreDB(String parentDir, String fileName) {

        return br4.restoreH2DB(parentDir, fileName);

    }
    
    public void migrateDB(){
        br4.migrateDB();
    }
}
