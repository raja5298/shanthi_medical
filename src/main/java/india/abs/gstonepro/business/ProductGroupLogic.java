/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.ProductGroupCRUD;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.models.SalesReportModel;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author SGS
 */
public class ProductGroupLogic {

    ProductGroupCRUD pg4 = new ProductGroupCRUD();

    public EventStatus createProductGroup(ProductGroup newPG) {
        return pg4.createProductGroup(newPG, SessionDataUtil.getSelectedCompany(),true,true);
    }

    public List<ProductGroup> fetchAllPG() {
        return pg4.getCompanyProductGroups(SessionDataUtil.getSelectedCompany());
    }

    public EventStatus updateProductGroup(ProductGroup updatedPG) {
        return pg4.updateProductGroup(updatedPG);
    }

    public EventStatus deleteProductGroup(Long productGroupId) {
        return pg4.deleteProductGroup(productGroupId);
    }
    public ProductGroup getProductGroup(Long productGroupId){
        return pg4.getAPG(productGroupId);
    }
    public Map<ProductGroup,SalesReportModel> getProductGroupSalesReport(Date dateFrom,Date dateTo){
        return pg4.getProductGroupSalesReport(dateFrom, dateTo); 
    }

}
