/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.UserCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;

/**
 *
 * @author Admin
 */
public class UserLogic {

    UserCRUD u4 = new UserCRUD();

    public boolean createUser(String userName, String phoneNumber, String password,boolean isAdmin) {
        EventStatus es = u4.createUser(userName, phoneNumber, password, isAdmin);
        return es.isCreateDone();
    }

    public boolean updateUser(Long userId, String userName, String phoneNumber, String password,boolean isAdmin) {
        EventStatus es = u4.updateUser(userId, userName, phoneNumber, password,isAdmin);

        return es.isUpdateDone();
    }

    public List<AppUser> fetchAllUsers() {
        return u4.fetchAllUsers();
    }

    public AppUser fetchAUser(Long userId) {
        return u4.fetchAUser(userId);
    }
    
    public AppUser fetchAuthorisedUser(String userName,String password){
        return u4.fetchAuthorisedUser(userName, password);
    }

    public boolean deleteUser(Long userId) {
        return false;
    }

}
