/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;

/**
 *
 * @author Admin
 */
public class UQCLogic {

    uqcCRUD u4 = new uqcCRUD();

    public boolean createUQC(String quantityName, String quantityType, String uqcCode) {
        EventStatus result = u4.createUQC(quantityName, quantityType, uqcCode);

        return result.isCreateDone();
    }

    public boolean updateUQC(Long uqcId, String quantityName, String quantityType, String uqcCode) {
        EventStatus result = u4.updateUQC(uqcId, quantityName, quantityType, uqcCode);

        return result.isUpdateDone();
    }

    public List<UQC> fetchAllUQCs() {
        return u4.fetchAllUQCs();
    }
    
    public boolean deleteUQC(Long uqcId) {
        EventStatus result = u4.deleteUQC(uqcId);
        return result.isDeleteDone();
    }

}
