/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.CompanyUserRoleCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class CompanyUserRoleLogic {

    CompanyUserRoleCRUD cur4 = new CompanyUserRoleCRUD();

    public boolean updateUserRole(CompanyUserRole updatedCUR) {
        EventStatus result = cur4.updateCUR(updatedCUR);
        return result.isUpdateDone();
    }

    public CompanyUserRole getCompanyUserRole(Company company, AppUser user) {

        return cur4.fetchCURByUserAndCompany(company, user);
    }

    public List<CompanyUserRole> getUserRoles(AppUser user) {
        return cur4.fetchCURByUser(user);
    }

}
