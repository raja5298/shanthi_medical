/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.TransactionCRUD;
import india.abs.gstonepro.api.models.Transaction;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SGS
 */
public class TransactionLogic {

    TransactionCRUD trans4 = new TransactionCRUD();

    public List<Transaction> getTransactionsByDay(Date fromDate, Date toDate) {
        return trans4.getAllTransactionsByDay(fromDate, toDate);
    }

    public List<Transaction> getAllTransactionsByType(Date fromDate, Date toDate, String type) {
        return trans4.getAllTransactionsByType(fromDate, toDate, type);
    }
}
