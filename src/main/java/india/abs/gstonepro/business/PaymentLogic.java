/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.TransactionCRUD;
import india.abs.gstonepro.api.dao.PaymentCRUD;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class PaymentLogic {

    PaymentCRUD pay4 = new PaymentCRUD();
    TransactionCRUD trans4 = new TransactionCRUD();

    public boolean createPayment(Ledger ledger,Ledger bankLedger, String particulars, Date transactionDate, float amount) {
        EventStatus result;
        result = pay4.createPayment(SessionDataUtil.getSelectedCompany(),ledger,bankLedger, particulars, amount, transactionDate);
        return result.isCreateDone();

    }

    public List<Transaction> findAllTransactionsByLedger(Ledger ledger, Date fromDate, Date toDate) {
       
        return trans4.getAllTransactionsByLedger(ledger, fromDate, toDate);
    }

    public boolean updatePayment(Transaction transaction) {
        EventStatus result = pay4.updatePayment(transaction);
        return result.isUpdateDone();
    }

    public boolean deletePayment(Transaction transaction) {
        EventStatus result = pay4.deletePayment(transaction);
        return result.isDeleteDone();
    }
    
    public boolean payBillWise(Set<PurchaseSale> pss){
        
        for(PurchaseSale ps:pss){
            
        }
        
    return false;
    }

}
