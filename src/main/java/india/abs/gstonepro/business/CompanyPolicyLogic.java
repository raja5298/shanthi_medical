/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.CompanyPolicyCRUD;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.EventStatus;

/**
 *
 * @author SGS
 */
public class CompanyPolicyLogic {

    CompanyPolicyCRUD cp4 = new CompanyPolicyCRUD();

    public boolean updateCompanyPolicy(CompanyPolicy updatedCP) {
        EventStatus result = new EventStatus();
        result = cp4.updateCompanyPolicy(updatedCP);
        return result.isUpdateDone();
    }
    
    public boolean updateFinancialYear(CompanyPolicy updatedCP){
        EventStatus result = new EventStatus();
        result = cp4.changeFinancialYear(updatedCP);
        return result.isUpdateDone();
    }

}
