/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.CompanyCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class CompanyLogic {

    CompanyCRUD c4 = new CompanyCRUD();

    public boolean createCompany(Company newCompany, Date financialYrStart, Date financialYrEnd) {
        
        
        EventStatus es = c4.createCompany(newCompany, financialYrStart, financialYrEnd);

        return es.isCreateDone();
    }

    public boolean updateCompany(Company UpdateCompany) {
        EventStatus es = c4.updateCompany(UpdateCompany);
        return es.isUpdateDone();
    }

    public List<Company> fetchAllCompanies() {
        return c4.fetchAllCompanies();
    }

    public Company fetchACompany(Long companyId) {
        return c4.fetchACompany(companyId);
    }

    public boolean deleteCompany(Long companyId) {
        return false;
    }

}
