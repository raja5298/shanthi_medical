/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.TaxInvoiceCRUD;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;

/**
 *
 * @author SGS
 */
public class TaxInvoiceLogic {

    TaxInvoiceCRUD tx4 = new TaxInvoiceCRUD();

    public EventStatus createTaxInvoice(PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {

        EventStatus result = tx4.createTaxInvoice(null, newPS, psItems, psTaxSummaries);
        return result;

    }

}
