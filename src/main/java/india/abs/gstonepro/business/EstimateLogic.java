/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.estimate.CancelEstimate;
import india.abs.gstonepro.api.dao.estimate.CreateEstimate;
import india.abs.gstonepro.api.dao.estimate.ReadEstimate;
import india.abs.gstonepro.api.dao.estimate.UpdateEstimate;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.models.EstimateLineItem;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SGS
 */
public class EstimateLogic {

    CreateEstimate cEstimate = new CreateEstimate();
    ReadEstimate rEstimate = new ReadEstimate();
    UpdateEstimate uEstimate = new UpdateEstimate();
    CancelEstimate cancelEstimate = new CancelEstimate();

    public EventStatus createEstimate(Estimate estimate, List<EstimateLineItem> estimateLineItems) {
        EventStatus result = null;
        try {
            result = cEstimate.createEstimate(null, estimate, estimateLineItems, SessionDataUtil.getSelectedCompany());
        } catch (ParseException ex) {
            Logger.getLogger(EstimateLogic.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
    
     public EventStatus updateEstimate(Estimate estimate, List<EstimateLineItem> estimateLineItem) throws ParseException {
        EventStatus result = uEstimate.updateEstimate(estimate, estimateLineItem, SessionDataUtil.getSelectedCompany());
        return result;
    }

    public boolean cancelEstimate(String estimateId) {
        EventStatus result = cancelEstimate.cancelEstimate(null, estimateId);
        return result.isDeleteDone();
    }
    
    public Estimate readEstimate(String estimateId) {
        return rEstimate.readEstimate(estimateId);
    }
    
    public List<Estimate> readEstimateByDuration(Date fromDate, Date toDate, boolean withCancelledBill) {
        return rEstimate.fetchAllEstimate(fromDate, toDate, withCancelledBill);
    }

    public List<Estimate> readEstimateByParty(Date fromDate, Date toDate,String partyId,boolean withCancelledBill) {
        if (partyId.equalsIgnoreCase("All")) {
            return rEstimate.fetchAllEstimate(fromDate, toDate,false);
        } else {
            return rEstimate.fetchEstimateByParty(fromDate, toDate, Long.parseLong(partyId), withCancelledBill);
        }
    }

    public List<EstimateLineItem> readEstimateByProduct(Date fromDate, Date toDate, Product product) {
        return rEstimate.fetchEstimateByProduct(fromDate, toDate, product);
    }

    public List<Estimate> readEstimateAuditData(Date fromDate, Date toDate) {
        return rEstimate.fetchAllEstimateForAudit(fromDate, toDate);
    }

    public List<Estimate> fetchAllEstimateForAuditByUser(Date fromDate, Date toDate, AppUser user) {
            return rEstimate.fetchAllEstimateForAuditByUser(fromDate, toDate, user);    
    }
}
