/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.BalanceSheetCRUD;
import india.abs.gstonepro.api.models.BalanceSheet;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.ProfitLossAccount;
import india.abs.gstonepro.api.models.TradingAccount;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class BalanceSheetLogic {

    BalanceSheetCRUD bs = new BalanceSheetCRUD();

    public EventStatus updateTradingAccountLedgers(List<TradingAccount> tas) {
        return bs.updateTradingAccount(SessionDataUtil.getSelectedCompany(), tas);
    }

    public EventStatus updateProfitLossAccountLedgers(List<ProfitLossAccount> plas) {
        return bs.updateProfitLossAccount(SessionDataUtil.getSelectedCompany(), plas);
    }

    public EventStatus updateBalanceSheet(List<BalanceSheet> bss) {
        return bs.updateBalanceSheet(SessionDataUtil.getSelectedCompany(), bss);

    }

    public Set<Ledger> fetchTradingAccountProfitLossLedgers() {
        return bs.fetchProfitLossTradingAccountLedgers(SessionDataUtil.getSelectedCompany());
    }

    public List<TradingAccount> fetchLedgersPresentInTradingAccount() {
        return bs.fetchTradingAccountLedgers(SessionDataUtil.getSelectedCompany());
    }

    public List<ProfitLossAccount> fetchLedgersPresentInProfitLossAccount() {
        return bs.fetchProfitLossAccountLedgers(SessionDataUtil.getSelectedCompany());
    }

    public List<BalanceSheet> fetchBSLedgers() {
        return bs.fetchAllBSLedgers(SessionDataUtil.getSelectedCompany());
    }
    
    public BigDecimal getLedgerSum(Ledger ledger){
        return bs.getTransactionAmount(ledger);
    }
}
