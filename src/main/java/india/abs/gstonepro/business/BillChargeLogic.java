/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.BillChargeCRUD;
import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;



/**
 *
 * @author Admin
 */
public class BillChargeLogic {
    BillChargeCRUD b4 = new BillChargeCRUD();

    public boolean createBillCharge(BillCharge newBillCharge) {
        EventStatus result = b4.createBillCharge(newBillCharge);
        return result.isCreateDone();
    }

    public List<BillCharge> fetchAllBillCharges() {
        return b4.fetchAllBillCharges();
    }

    public boolean updateBillCharge(BillCharge updatedBillCharge) {
        EventStatus result = b4.updateBillCharge(updatedBillCharge);
        return result.isUpdateDone();
    }

    public boolean deleteBillCharge(Long billChargeId) {
        EventStatus result = b4.deleteBillCharge(billChargeId);
        return result.isDeleteDone();
    }

}
