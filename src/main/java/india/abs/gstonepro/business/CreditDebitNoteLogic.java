/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.CreditDebitNoteCRUD;
import india.abs.gstonepro.api.dao.PurchaseSaleCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class CreditDebitNoteLogic {

    CreditDebitNoteCRUD cdn4 = new CreditDebitNoteCRUD();

    public EventStatus createCDN(CreditDebitNote cdn, List<CreditDebitNoteLineItem> cdnLineItem) {
        EventStatus result = cdn4.createCDN(null, SessionDataUtil.getSelectedCompany(), cdn, cdnLineItem);
        return result;
    }

    public EventStatus updateCDN(CreditDebitNote cdn, List<CreditDebitNoteLineItem> cdnLineItems) {
        EventStatus result = cdn4.updateCreditDedbitNote(SessionDataUtil.getSelectedCompany(), cdn, cdnLineItems);
        return result;
    }

    public EventStatus deleteCDN(String cdnId) {
        return cdn4.deleteCreditDebitNote(null, cdnId);
    }

    public List<CreditDebitNote> fetchAllCDNote(String type, Date fromDate, Date toDate) {
        return cdn4.getAllCDNoteByDate(type, fromDate, toDate, SessionDataUtil.getSelectedCompany());
    }

    public CreditDebitNote fetchCDNote(String cdnId) {
        return cdn4.getCDNDetails(cdnId);
    }

    public List<CreditDebitNote> fetchAllCDNoteByLedger(Ledger ledger, String type, Date fromDate, Date toDate) {
        return cdn4.getAllCDNoteByLedger(ledger, type, fromDate, toDate, SessionDataUtil.getSelectedCompany());
    }

    public List<CreditDebitNoteLineItem> getAllCreditDebitNoteByProduct(String type, Date fromDate, Date toDate, Product product) {
        return cdn4.getAllCreditDebitNoteByProduct(type, fromDate, toDate, product);
    }
    
    public List<CreditDebitNote> getCreditDebitByAllAudits(String type,Date fromDate, Date toDate) {
        return cdn4.getCDNoteByAuditData(type, fromDate, toDate);
    }
    
    public List<CreditDebitNote> getAllCreditDebitNoteByType(String type,Ledger ledger){
        return cdn4.getAllCreditDebitNoteByType(type,ledger);
    }
    
   
}
