/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.JournalCRUD;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class JournalLogic {

    JournalCRUD j4 = new JournalCRUD();

    public EventStatus createJournal(Set<Transaction> transactions) {
        EventStatus result = new EventStatus();
        result = j4.createJournal(SessionDataUtil.getSelectedCompany(), transactions);

        return result;

    }
    
    public Journal fetchAJournal(String journalId){
        return j4.fetchAJournal(journalId);
    }
    
    public boolean updateJournal(Journal journal, Set<Transaction> transactions) {
        EventStatus result = new EventStatus();
        result = j4.updateJournal(SessionDataUtil.getSelectedCompany(), journal, transactions);

        return result.isUpdateDone();

    }

    public boolean deleteJournal(String journalId) {
        EventStatus result = new EventStatus();
        result = j4.deleteJournal(journalId);
        return result.isDeleteDone();
    }

}
