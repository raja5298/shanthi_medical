/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.TaxCRUD;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;

/**
 *
 * @author Admin
 */
public class TaxLogic {

    TaxCRUD t4 = new TaxCRUD();

    public boolean createTax(float tax) {
        EventStatus result = t4.createTax(tax);
        return result.isCreateDone();
    }

    public List<Tax> fetchAllTaxes() {
        return t4.fetchAllTaxes();
    }

    public boolean updateTax(Long taxId, float tax) {
        EventStatus result = t4.updateTax(taxId, tax);
        return result.isUpdateDone();
    }

    public boolean deleteTax(Long taxId) {
        EventStatus result = t4.deleteTax(taxId);
        return result.isDeleteDone();
    }

}
