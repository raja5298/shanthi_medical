/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.bos.CancelBOS;
import india.abs.gstonepro.api.dao.bos.CreateBOS;
import india.abs.gstonepro.api.dao.bos.ReadBOS;
import india.abs.gstonepro.api.dao.bos.UpdateBOS;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SGS
 */
public class BOSLogic {

    CreateBOS cBOS = new CreateBOS();
    ReadBOS rBOS = new ReadBOS();
    UpdateBOS uBOS = new UpdateBOS();
    CancelBOS cancelBOS = new CancelBOS();

    public EventStatus createBOS(BOS bos, List<BOSLineItem> bosLineItems) {
        EventStatus result = null;
        try {
            result = cBOS.createBOS(null, bos, bosLineItems, SessionDataUtil.getSelectedCompany());
        } catch (ParseException ex) {
            Logger.getLogger(BOSLogic.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
    
     public EventStatus updateBOS(BOS bos, List<BOSLineItem> bosLineItem) throws ParseException {
        EventStatus result = uBOS.updateBOS(bos, bosLineItem, SessionDataUtil.getSelectedCompany());
        return result;
    }

    public boolean cancelBOS(String bosId) {
        EventStatus result = cancelBOS.cancelBOS(null, bosId);
        System.out.println("result "+result.toString());
        return result.isDeleteDone();
    }
    
    public BOS readBOS(String bosId) {
        return rBOS.readBOS(bosId);
    }
    
    public List<BOS> readBOSByDuration(Date fromDate, Date toDate, boolean withCancelledBill) {
        return rBOS.fetchAllBOS(fromDate, toDate, withCancelledBill);
    }

    public List<BOS> readBOSByParty(Date fromDate, Date toDate,String partyId,boolean withCancelledBill) {
        if (partyId.equalsIgnoreCase("All")) {
            return rBOS.fetchAllBOS(fromDate, toDate,false);
        } else {
            return rBOS.fetchBOSByParty(fromDate, toDate, Long.parseLong(partyId), withCancelledBill);
        }
    }

    public List<BOSLineItem> readBOSByProduct(Date fromDate, Date toDate, Product product) {
        return rBOS.fetchBOSByProduct(fromDate, toDate, product);
    }

    public List<BOS> readBOSAuditData(Date fromDate, Date toDate) {
        return rBOS.fetchAllBOSForAudit(fromDate, toDate);
    }

    public List<BOS> fetchAllBOSForAuditByUser(Date fromDate, Date toDate, AppUser user) {
            return rBOS.fetchAllBOSForAuditByUser(fromDate, toDate, user);    
    }
}
