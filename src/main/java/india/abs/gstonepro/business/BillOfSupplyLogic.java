/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.billofsupply.CancelBOS;
import india.abs.gstonepro.api.dao.billofsupply.CreateBOS;
import india.abs.gstonepro.api.dao.billofsupply.ReadBOS;
import india.abs.gstonepro.api.dao.billofsupply.UpdateBOS;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SGS
 */
public class BillOfSupplyLogic {

    CreateBOS cBOS = new CreateBOS();
    ReadBOS rBOS = new ReadBOS();
    UpdateBOS uBOS = new UpdateBOS();
    CancelBOS cancelBOS = new CancelBOS();


    public EventStatus createBOS(BillOfSupply bos, List<BillOfSupplyLineItem> bosLis, List<BillOfSupplyChargeItem> bosCis) {
        EventStatus result = null;
        try {
            result = cBOS.createBOS(null, bos, bosLis, bosCis, SessionDataUtil.getSelectedCompany());
        } catch (ParseException ex) {
            Logger.getLogger(BillOfSupplyLogic.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public List<BillOfSupply> readBOSByDuration(Date fromDate, Date toDate, boolean withCancelledBill) {
        return rBOS.fetchAllBOS(fromDate, toDate, SessionDataUtil.getSelectedCompany(), withCancelledBill);
    }

    public List<BillOfSupply> readBOSByParty(Date fromDate, Date toDate, Ledger ledger, boolean withCancelledBill) {
        return rBOS.fetchBOSByParty(fromDate, toDate, SessionDataUtil.getSelectedCompany(), ledger, withCancelledBill);
    }

    public List<BillOfSupplyLineItem> readBOSByProduct(Date fromDate, Date toDate, Product product) {
        return rBOS.fetchBOSByProduct(fromDate, toDate, product);
    }

    public BillOfSupply readABOS(String bosId) {
        return rBOS.readABOS(bosId);
    }

    public EventStatus updateBOS(BillOfSupply bos, List<BillOfSupplyLineItem> bosLis, List<BillOfSupplyChargeItem> bosCis) throws ParseException {
        EventStatus result = uBOS.updateBOS(bos, bosLis, bosCis, SessionDataUtil.getSelectedCompany());
        return result;
    }

    public boolean cancelBOS(String bosId) {
        EventStatus result = cancelBOS.cancelBOS(null, bosId);
        return result.isDeleteDone();
    }

    public List<BillOfSupply> readBOSAuditData(Date fromDate, Date toDate) {
        return rBOS.fetchAllBOSForAudit(fromDate, toDate);
    }

    public List<BillOfSupply> readBOSAuditDataByUser(Date fromDate, Date toDate, AppUser user) {
        return rBOS.fetchAllBOSForAuditByUser(fromDate, toDate, user);
    }

}
