/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.PurchaseOrSaleCRUD;
import india.abs.gstonepro.api.dao.PurchaseSaleCRUD;
import india.abs.gstonepro.api.dao.TaxInvoiceCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class PurchaseSaleLogic {

    PurchaseSaleCRUD ps4 = new PurchaseSaleCRUD();

    PurchaseOrSaleCRUD pos4 = new PurchaseOrSaleCRUD();

    TaxInvoiceCRUD tx4 = new TaxInvoiceCRUD();

    //------------------------------------------------SALES-----------------------------------------------------------------------//
    public EventStatus createAPS(String type, Long ledgerId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        // EventStatus result = ps4.createPS("SALES", ledgerId, newPS, psItems, psTaxSummaries);
        // EventStatus result = pos4.createPS(type, ledgerId, newPS, psItems, psTaxSummaries);
        EventStatus result = tx4.createTaxInvoice(null, newPS, psItems, psTaxSummaries);
        return result;
    }

    public boolean updateAPurchaseOrSale(String type, Long ledgerId, PurchaseSale updatedPS, List<PurchaseSaleLineItem> updatdPSItems, List<PurchaseSaleTaxSummary> updatePSTaxSummaries) {
        // EventStatus result = ps4.createPS("SALES", ledgerId, newPS, psItems, psTaxSummaries);
        // EventStatus result = pos4.updatePS(type, ledgerId, updatedPS, updatdPSItems, updatePSTaxSummaries);
        EventStatus result = tx4.updateTaxInvoice(updatedPS, updatdPSItems, updatePSTaxSummaries);
        return result.isUpdateDone();
    }

    public boolean cancelASale(String psId) {
        EventStatus result = tx4.cancelTaxInvoice(null, psId);
        return result.isDeleteDone();
    }

    public List<PurchaseSale> getAllPS(String ledgerId, String type, Date fromDate, Date toDate, boolean withCancelledBill) {
        if (ledgerId.equalsIgnoreCase("ALL")) {
            return ps4.getAllPS(type, fromDate, toDate, withCancelledBill);
        }
        else {
            return ps4.getAllPSByLedger(Long.parseLong(ledgerId), type, fromDate, toDate, withCancelledBill);
        }
    }
    
    public List<PurchaseSale> getAllPSWithOutDate(String ledgerId, String type,  boolean withCancelledBill) {
        if (ledgerId.equalsIgnoreCase("ALL")) {
            return ps4.getAllPSWitOutDate(type,  withCancelledBill);
        }
        else {
            return ps4.getAllPSByLedgerWithOutDate(Long.parseLong(ledgerId), type, withCancelledBill);
        }
    }

    public List<PurchaseSaleLineItem> getAllTaxInvoiceByProduct(Date fromDate, Date toDate, Product product) {
        return ps4.getAllTaxInvoiceByProduct("TX-I", fromDate, toDate, product);
    }

    public List<PurchaseSale> getAllTaxInvoiceByAuditDate(Date fromDate, Date toDate) {
        return ps4.getAllTaxInvoiceByAuditData("TX-I", fromDate, toDate);
    }

    public PurchaseSale getPSDetail(String psId) {
        return ps4.getPSDetail(psId);
    }

    public List<PurchaseSale> getAllPurchaseByDate(Date fromDate, Date toDate) {
        return ps4.getAllPSByDate(fromDate, toDate);
    }

    public boolean updateASale(Long ledgerId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        return false;
    }

    public boolean cancelASale(Long ledgerId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        return false;
    }
    //============================================SALES END=====================================================================//

    public List<PurchaseSale> getAllPurchases() {
        return ps4.getAllPS("PURCHASE", new Date(), new Date(), false);
    }

    public boolean createASaleReturn(Long ledgerId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = ps4.createPS("SR", ledgerId, newPS, psItems, psTaxSummaries);
        return result.isCreateDone();
    }

    public List<PurchaseSale> getAllSalesReturns() {
        return ps4.getAllPS("SALE-RETURN", new Date(), new Date(), false);
    }

    public boolean createAPurchaseReturn(Long ledgerId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = ps4.createPS("PR", ledgerId, newPS, psItems, psTaxSummaries);
        return result.isCreateDone();
    }

    public List<PurchaseSale> getAllPurchaseReturns() {
        return ps4.getAllPS("PURCHASE-RETURN", new Date(), new Date(), false);
    }

    public List<PurchaseSale> getSalesByParty(Long ledgerId) {
        return null;
    }

    public List<PurchaseSale> getSalesByDateRange(Date from, Date to) {
        return null;
    }

    public List<PurchaseSale> getAllPSByLedgerWithoutDate(Ledger ledger, String Type) {
        return ps4.getAllPSByLedgerWithoutDate(ledger, Type, false);
    }

    public EventStatus updatePSLineItemsDescription() {
        return null;
    }

    public EventStatus updatePSPayment(List<PurchaseSale> psList, Ledger lg, List<CreditDebitNote> cdn, String str,Date date) {
        return ps4.updatePSPayment(psList, lg, cdn, str,date);
    }

    public EventStatus updatePurchasePayment(List<PurchaseInvoice> psList, Ledger lg, List<CreditDebitNote> cdn, String str,Date date) {
        return ps4.updatePurchasePayment(psList, cdn, str,date);
    }
}
