/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.LedgerCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class LedgerLogic {

    LedgerCRUD l4 = new LedgerCRUD();

    public boolean createPartyLedger(Long companyId, Ledger newLedger, boolean isSundryDebitor) {
        EventStatus result = l4.createPartyLedger(companyId, newLedger, isSundryDebitor);
        return result.isCreateDone();
    }

    public boolean createAgentLedger(Long companyId, Ledger newLedger) {
        EventStatus result = l4.createAgentLedger(companyId, newLedger);
        return result.isCreateDone();
    }

    public Ledger createLedger(Long companyId, Ledger newLedger) {
        Ledger resultLedger = l4.createLedger(companyId, newLedger);
        return resultLedger;
    }

    public Set<Ledger> fetchAllPartyLedgers(Long companyId) {
        return l4.fetchPartyLedgers(SessionDataUtil.getSelectedCompany());
    }

    public Set<Ledger> fetchAllAgentLedgers(Long companyId) {
        return l4.fetchAgentLedgers(SessionDataUtil.getSelectedCompany());
    }

    public Set<Ledger> fetchAllBankLedgers(Long companyId) {
        return l4.fetchBankLedgers(SessionDataUtil.getSelectedCompany());
    }

    public Set<Ledger> fetchAllPartyAndGOCLedgers() {
        return l4.fetchPartyAndGOCLedgers(SessionDataUtil.getSelectedCompany());
    }

    public Set<Ledger> fetchAllPurchaseLedgers() {
        return l4.fetchPurchaseLedgers(SessionDataUtil.getSelectedCompany());
    }

    public List<Ledger> fetchAllCompanyLedgers(Long companyId) {
        return l4.fetchAllLedgers(companyId);
    }

    public Ledger fetchCashLedger() {
        return l4.fetchLedgerByName(SessionDataUtil.getSelectedCompany(), "Cash In Hand");
    }

    public Ledger fetchCashAdjustmentLedger() {
        return l4.fetchLedgerByName(SessionDataUtil.getSelectedCompany(), "Cash Adjustment");
    }

    public boolean updateLedger(Ledger updatedLedger) {
        EventStatus result = l4.updateLedger(updatedLedger);
        return result.isUpdateDone();
    }

    public boolean updatePartyLedger(Ledger updatedLedger) {
        EventStatus result = l4.updatePartyLedger(updatedLedger);
        return result.isUpdateDone();
    }

    public EventStatus deleteLedger(Long ledgerId) {
        EventStatus result = l4.deleteLedger(ledgerId);
        return result;
    }

    public EventStatus deletePartyLedger(Long ledgerId) {
        EventStatus result = l4.deletePartyLedger(ledgerId);
        return result;
    }

    public List<Transaction> findTrailBalance(Date toDate) {
        return l4.findCompanyTrailBalance(SessionDataUtil.getSelectedCompany(), SessionDataUtil.getCompanyPolicyData().getFinancialYearStart(), toDate);
    }

    public List<Transaction> findLedgerGroupBalance(LedgerGroup lg, Date toDate) {
        return l4.findLedgerGroupBalance(SessionDataUtil.getSelectedCompany(), lg, SessionDataUtil.getCompanyPolicyData().getFinancialYearStart(), toDate);
    }

    public EventStatus updateOpeningBalance(Ledger updatedLedger) {
        return l4.updateLedgerOpeningBalance(updatedLedger);
    }

    public List<Ledger> fetchLedgerByGroup(long companyId, String group) {
        return l4.fetchLedgerByGroupName(companyId, group);
    }

    public Ledger fetchNoPartyLedger() {
        return l4.fetchLedgerByName(SessionDataUtil.getSelectedCompany(), "No Party");
    }
}
