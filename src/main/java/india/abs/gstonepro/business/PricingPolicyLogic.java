/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.business;

import india.abs.gstonepro.api.dao.PricingPolicyCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.api.utils.EventStatus;
import java.util.List;
import java.util.Set;

/**
 *
 * @author SGS
 */
public class PricingPolicyLogic {

    PricingPolicyCRUD pp4 = new PricingPolicyCRUD();

    public List<PricingPolicy> fetchAllPP(Long companyId) {
        return pp4.fetchAllPricingPolicies(companyId);
    }

    public Set<SingleProductPolicy> fetchAllSPP(PricingPolicy pp) {
        return pp4.fetchAllSingleProductPolicies(pp);
    }

    public boolean createPricingPolicy(Company company, String name) {
        EventStatus result = pp4.createPricingPolicy(company, name);
        return result.isCreateDone();
    }

    public boolean updatePricingPolicy(PricingPolicy pp) {
        EventStatus result = pp4.updatePricingPolicy(pp);
        return result.isUpdateDone();
    }

    public boolean deletePricingPolicy(Long ppId) {
        EventStatus result = pp4.deletePricingPolicy(ppId);
        return result.isDeleteDone();
    }

    public boolean updateProductPolicy(SingleProductPolicy spp) {
        EventStatus result = pp4.updateSingleProductPolicy(spp);
        return result.isUpdateDone();
    }

}
