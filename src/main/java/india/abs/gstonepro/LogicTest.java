/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.dao.PurchaseSaleCRUD;
import india.abs.gstonepro.api.models.PurchaseSale;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class LogicTest {

    public static void main(String[] args) {
        PurchaseSaleCRUD ps4 = new PurchaseSaleCRUD();
        ps4.getAllPS("SALES",new Date(),new Date(),true) .stream().map((ps) -> {
            
            return ps;
        }).map((ps) -> ps4.getPSDetail(ps.getPsId())).forEachOrdered((pos) -> {
            pos.printCollections();
        });
    }

}
