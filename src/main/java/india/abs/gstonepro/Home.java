/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.dao.TaxCRUD;

import org.hibernate.SessionFactory;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author ALPHA
 */
public class Home {

    private static SessionFactory factory;

    public static void main(String[] args) throws IOException {
        factory = HibernateUtil.getSessionFactory();

        try {
            Session session = factory.openSession();
            session.beginTransaction();
            session.save(new Product("Our very first PRODUCT!"));

            session.getTransaction().commit();
        } catch (HibernateException e) {
            System.err.print(e);
        }
        createTaxValues();
        createUQCCodes();
        readXLSXFile();
        List<Tax> results = new TaxCRUD().fetchAllTaxes();
        results.forEach((tax) -> {

        });
    }

    public static void createTaxValues() {

        Session session = factory.openSession();
        session.beginTransaction();
        float[] taxValues = {0.0f, 2.5f, 5.0f, 6.0f, 12.0f, 9.0f, 18.0f, 14.0f, 28.0f};

        for (float tax : taxValues) {
            session.save(new Tax(tax));
        }

        session.getTransaction().commit();

    }

    public static void createUQCCodes() {

        try (Session session = factory.openSession()) {
            session.beginTransaction();
            String[] quantity = {"BAGS", "BALE"};
            String[] type = {"Measure", "Measure"};
            String[] UQCCode = {"BAG", "Measure"};

            for (int counter = 0; counter < quantity.length; counter++) {
                session.save(new UQC(quantity[counter], type[counter], UQCCode[counter]));
            }

            session.getTransaction().commit();
            session.close();
        }

    }

    public static void readXLSXFile() throws IOException {
        InputStream ExcelFileToRead = new FileInputStream("C:\\Users\\ALPHA\\Desktop\\Sakthi Reference\\UQC_List.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

        XSSFWorkbook test = new XSSFWorkbook();

        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFRow row;
        XSSFCell cell;

        Iterator rows = sheet.rowIterator();

        while (rows.hasNext()) {
            row = (XSSFRow) rows.next();
            Iterator cells = row.cellIterator();
            XSSFCell cell1;
            cell1 = row.getCell(1);

            while (cells.hasNext()) {
                cell = (XSSFCell) cells.next();
                // 
                switch (cell.getCellType()) {
                    case XSSFCell.CELL_TYPE_STRING:
                        break;
                    case XSSFCell.CELL_TYPE_NUMERIC:
                        break;
                    //U Can Handel Boolean, Formula, Errors
                    default:
                        break;
                }
            }

        }

    }

}

//1       BAGS	Measure	BAG
//2	BALE	Measure	BAL
//3	BUNDLES	Measure	BDL
//4	BUCKLES	Measure	BKL
//5	BILLIONS OF UNITS	Measure	BOU
//6	BOX	Measure	BOX
//7	BOTTLES	Measure	BTL
//8	BUNCHES	Measure	BUN
//9	CANS	Measure	CAN
//10	CUBIC METER	Volume	CBM
//11	CUBIC CENTIMETER	Volume	CCM
//12	CENTIMETER	Length	CMS
//13	CARTONS	Measure	CTN
//14	DOZEN	Measure	DOZ
//15	DRUM	Measure	DRM
//16	GREAT GROSS	Measure	GGR
//17	GRAMS	Weight	GMS
//18	GROSS	Measure	GRS
//19	GROSS YARDS	Length	GYD
//20	KILOGRAMS	Weight	KGS
//21	KILOLITER	Volume	KLR
//22	KILOMETRE	Length	KME
//23	MILLILITRE	Volume	MLT
//24	METERS	Length	MTR
//25	NUMBERS	Measure	NOS
//26	PACKS	Measure	PAC
//27	PIECES	Measure	PCS
//28	PAIRS	Measure	PRS
//29	QUINTAL	Weight	QTL
//30	ROLLS	Measure	ROL
//31	SETS	Measure	SET
//32	SQUARE FEET	Area	SQF
//33	SQUARE METERS	Area	SQM
//34	SQUARE YARDS	Area	SQY
//35	TABLETS	Measure	TBS
//36	TEN GROSS	Measure	TGM
//37	THOUSANDS	Measure	THD
//38	TONNES	Weight	TON
//39	TUBES	Measure	TUB
//40	US GALLONS	Volume	UGS
//41	UNITS	Measure	UNT
//42	YARDS	Length	YDS
//43	OTHERS		OTH
