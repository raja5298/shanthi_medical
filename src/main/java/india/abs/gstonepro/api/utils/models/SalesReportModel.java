package india.abs.gstonepro.api.utils.models;

import java.math.BigDecimal;

public class SalesReportModel{
private BigDecimal totalAmount = null;
private BigDecimal totalAmountIncOfTax = null;
private BigDecimal IGSTAmt = null;
private BigDecimal CGSTAmt = null;
private BigDecimal SGSTAmt = null;
private BigDecimal profit = null;
private BigDecimal returnsValue = null;
private int returnsCount =0;

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmountIncOfTax() {
        return totalAmountIncOfTax;
    }

    public void setTotalAmountIncOfTax(BigDecimal totalAmountIncOfTax) {
        this.totalAmountIncOfTax = totalAmountIncOfTax;
    }

    public BigDecimal getIGSTAmt() {
        return IGSTAmt;
    }

    public void setIGSTAmt(BigDecimal IGSTAmt) {
        this.IGSTAmt = IGSTAmt;
    }

    public BigDecimal getCGSTAmt() {
        return CGSTAmt;
    }

    public void setCGSTAmt(BigDecimal CGSTAmt) {
        this.CGSTAmt = CGSTAmt;
    }

    public BigDecimal getSGSTAmt() {
        return SGSTAmt;
    }

    public void setSGSTAmt(BigDecimal SGSTAmt) {
        this.SGSTAmt = SGSTAmt;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getReturnsValue() {
        return returnsValue;
    }

    public void setReturnsValue(BigDecimal returnsValue) {
        this.returnsValue = returnsValue;
    }

    public int getReturnsCount() {
        return returnsCount;
    }

    public void setReturnsCount(int returnsCount) {
        this.returnsCount = returnsCount;
    }

}
