/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

/**
 *
 * @author SGS
 */
public class EventStatus {
    
    public Long createdId;

    private boolean createDone = false;

    private boolean updateDone = false;

    private boolean deleteDone = false;
    
    private String billNo;
    
    private String billID;
    
    private String message;

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }
    
    

    public boolean isCreateDone() {
        return createDone;
    }

    public boolean isUpdateDone() {
        return updateDone;
    }

    

    public boolean isDeleteDone() {
        return deleteDone;
    }

    public void setCreateDone(boolean createDone) {
        this.createDone = createDone;
    }

    public void setUpdateDone(boolean updateDone) {
        this.updateDone = updateDone;
    }

   

    public void setDeleteDone(boolean deleteDone) {
        this.deleteDone = deleteDone;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
    @Override
    public String toString() {
        return "EventStatus{" + "createdId=" + createdId + ", createDone=" + createDone + ", updateDone=" + updateDone +  ", deleteDone=" + deleteDone + 
                ", Bill Id =" + billID + ", message =" + message +'}';
    }
    
    

}
