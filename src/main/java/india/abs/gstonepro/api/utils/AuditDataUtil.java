/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

import india.abs.gstonepro.api.models.Audit;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class AuditDataUtil {

    public static Audit getAuditForCreate() {
        Audit auditData = new Audit();
        auditData.setCreatedBy(SessionDataUtil.getSelectedUser());
        auditData.setCreatedOn(new Date());
        return auditData;
    }

    public static Audit getAuditForUpdate() {
        Audit auditData = new Audit();
        auditData.setUpdatedBy(SessionDataUtil.getSelectedUser());
        auditData.setUpdatedOn(new Date());
        return auditData;
    }

}
