/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.SystemPolicy;

/**
 *
 * @author SGS
 */
public class SessionDataUtil {
    
    private static AppUser selectedUser;
    
    private static Company selectedCompany;
    
    private static CompanyPolicy companyPolicyData;
    
    private static CompanyUserRole companyUserRole;
    
   private static SystemPolicy systemPolicy;
    
    private static Boolean multipleCompany = false;

    public static Boolean isMultipleCompany() {
        return multipleCompany;
    }

    public static void setMultipleCompany(Boolean multipleCompany) {
        SessionDataUtil.multipleCompany = multipleCompany;
    }

    

    public static CompanyUserRole getCompanyUserRole() {
        
        return companyUserRole;
    }

    public static void setCompanyUserRole(CompanyUserRole companyUserRole) {
        SessionDataUtil.companyUserRole = companyUserRole;
        
    }
    
    
    
    public static AppUser getSelectedUser() {
        return selectedUser;
    }

    public static void setSelectedUser(AppUser selectedUser) {
        SessionDataUtil.selectedUser = selectedUser;
    }

    public static Company getSelectedCompany() {
        return selectedCompany;
    }

    public static void setSelectedCompany(Company selectedCompany) {
        SessionDataUtil.selectedCompany = selectedCompany;
    }

    public static CompanyPolicy getCompanyPolicyData() {
        return companyPolicyData;
    }

    public static void setCompanyPolicyData(CompanyPolicy companyPolicyData) {
        SessionDataUtil.companyPolicyData = companyPolicyData;
    }

    

    @Override
    public String toString() {
        return "SessionDataUtil{" +companyPolicyData.getPrintPaperSize() +'}';
    }

    public static SystemPolicy getSystemPolicy() {
        return systemPolicy;
    }

    public static void setSystemPolicy(SystemPolicy systemPolicy) {
        SessionDataUtil.systemPolicy = systemPolicy;
    }

    public static Boolean getMultipleCompany() {
        return multipleCompany;
    }
    
    
 }
