/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

import india.abs.gstonepro.api.models.SystemPolicy;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author SGS
 */
public class SystemPolicyUtil {

    static SystemPolicy sp = null;

    public static SystemPolicy getSystemPolicy() {
        return sp;
    }

    static {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "system.properties";
            input = SystemPolicyUtil.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {

            }

            //load a properties file from class path, inside static method
            prop.load(input);
            sp = new SystemPolicy();
            String packageName = prop.getProperty("package").toLowerCase();
            sp.setPackageName(prop.getProperty("package"));
            sp.setVersion(prop.getProperty("version"));
            sp.setReleaseDate(prop.getProperty("releasedate"));
            sp.setSerialNumber(prop.getProperty("serialno"));
            sp.setInventoryEnabled(Boolean.parseBoolean(prop.getProperty("inventory")));
            sp.setAccountsEnabled(Boolean.parseBoolean(prop.getProperty("accounts")));
            sp.setMasterUsername(prop.getProperty("masterusername"));
            sp.setMasterPassword(prop.getProperty("masterpassword"));
            
            sp.setTrialPackage(Boolean.parseBoolean(prop.getProperty("trailpackage")));
            sp.setBillOfSupply(Boolean.parseBoolean(prop.getProperty("billofsupply")));
            sp.setPlatinum(false);
            

            switch (packageName) {
                case "copper":
                    sp.setImageURL(prop.getProperty("copperurl"));
                    sp.setBalanceSheet(false);
                    break;
                case "silver":
                    sp.setImageURL(prop.getProperty("silverurl"));
                    sp.setBalanceSheet(false);
                    break;
                case "gold":
                    sp.setImageURL(prop.getProperty("goldurl"));
                    sp.setBalanceSheet(false);
                    break;
                case "diamond":
                    sp.setImageURL(prop.getProperty("diamondurl"));
                    sp.setBalanceSheet(true);
                    break;
                case "platinum":
                    sp.setImageURL(prop.getProperty("platinumurl"));
                    sp.setBalanceSheet(true);
                    sp.setPlatinum(true);
                    break;
                default:
                    sp.setImageURL(prop.getProperty(""));
                    break;
            }

//            String releaseDate = prop.getProperty("releasedate");
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//            LocalDate dateTime = LocalDate.parse(releaseDate, formatter);
//
//            sp.setReleaseDate(java.sql.Date.valueOf(dateTime));
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
