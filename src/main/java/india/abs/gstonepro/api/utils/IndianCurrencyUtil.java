/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

import static india.abs.gstonepro.api.utils.IndianCurrencyUtil.convertToIndianCurrency;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Currency;
import java.util.Locale;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author SGS
 */
public class IndianCurrencyUtil {

    public static void main(String[] args) {
        
        BigDecimal money;
        money = new BigDecimal("50000000000.78");
        convertToIndianCurrencyFormat(money);

    }

    public static String convertToIndianCurrencyFormat(BigDecimal money) {
        String result = "";
        StringBuilder t = new StringBuilder(money.toString());
        String[] moneys = t.toString().split("\\.");
        int count = 0;
        String money1 = new StringBuilder(moneys[0]).reverse().toString();
        int stringLength = money1.length();
        for (char c : money1.toCharArray()) {
            result += c;

            if (count + 1 != stringLength) {
                count++;
                switch (count) {
                    case 3:
                        result += ",";
                        break;
                    case 5:
                        result += ",";
                        break;
                    case 7:
                        result += ",";
                        break;
                    case 9:
                        result += ",";
                        break;
                    case 11:
                        result += ",";
                        break;
                }

            }
        }
        result = new StringBuilder(result).reverse().toString();
        if (moneys.length == 2) {
            result += "." + moneys[1];
        }
        result = '\u20B9' + result;
        return result;
    }

    public static String convertToIndianCurrency(String num) {
        BigDecimal bd = new BigDecimal(num);
        long number = bd.longValue();
        long no = bd.longValue();
        int decimal = (int) (bd.remainder(BigDecimal.ONE).doubleValue() * 100);
        int digits_length = String.valueOf(no).length();
        int i = 0;
        ArrayList<String> str = new ArrayList<>();
        HashMap<Integer, String> words = new HashMap<>();
        words.put(0, "");
        words.put(1, "One");
        words.put(2, "Two");
        words.put(3, "Three");
        words.put(4, "Four");
        words.put(5, "Five");
        words.put(6, "Six");
        words.put(7, "Seven");
        words.put(8, "Eight");
        words.put(9, "Nine");
        words.put(10, "Ten");
        words.put(11, "Eleven");
        words.put(12, "Twelve");
        words.put(13, "Thirteen");
        words.put(14, "Fourteen");
        words.put(15, "Fifteen");
        words.put(16, "Sixteen");
        words.put(17, "Seventeen");
        words.put(18, "Eighteen");
        words.put(19, "Nineteen");
        words.put(20, "Twenty");
        words.put(30, "Thirty");
        words.put(40, "Forty");
        words.put(50, "Fifty");
        words.put(60, "Sixty");
        words.put(70, "Seventy");
        words.put(80, "Eighty");
        words.put(90, "Ninety");
        String digits[] = {"", "Hundred", "Thousand", "Lakh", "Crore"};
        while (i < digits_length) {
            int divider = (i == 2) ? 10 : 100;
            number = no % divider;
            no = no / divider;
            i += divider == 10 ? 1 : 2;
            if (number > 0) {
                int counter = str.size();
                String plural = (counter > 0 && number > 9) ? "s" : "";
                String tmp = (number < 21) ? words.get(Integer.valueOf((int) number)) + " " + digits[counter] + plural : words.get(Integer.valueOf((int) Math.floor(number / 10) * 10)) + " " + words.get(Integer.valueOf((int) (number % 10))) + " " + digits[counter] + plural;
                str.add(tmp);
            } else {
                str.add("");
            }
        }

        Collections.reverse(str);
        String Rupees = String.join(" ", str).trim();

        String paise = (decimal) > 0 ? " And Paise " + words.get(Integer.valueOf((int) (decimal - decimal % 10))) + " " + words.get(Integer.valueOf((int) (decimal % 10))) : "";
        return "Rupees " + Rupees + paise + " Only";
    }

}
