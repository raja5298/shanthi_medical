/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils.models;

import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.Product;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class StockReportModel {

    private Product product;

    private GoDown godown;

    private Long openingStock;

    private Long closingStock;

    private Long stockIn;

    private Long stockOut;
    
    private Long manualStockIn;
    
    private Long purchase;
    
    private Long salesReturn;
    
    private Long manualStockout;
    
    private Long sales;
    
    private Long purchaseReturn;

    private Date reportDate;

    public StockReportModel() {

    }

    public StockReportModel(Product product, GoDown godown, Long openingStock, Long closingStock, int stockIn, int stockOut, Date reportDate) {
        this.product = product;
        this.godown = godown;
        this.openingStock = openingStock;
        this.closingStock = closingStock;
        this.reportDate = reportDate;
    }

    public StockReportModel(Long manualStockIn, Long purchase, Long salesReturn, Long manualStockout, Long sales, Long purchaseReturn) {
        this.manualStockIn = manualStockIn;
        this.purchase = purchase;
        this.salesReturn = salesReturn;
        this.manualStockout = manualStockout;
        this.sales = sales;
        this.purchaseReturn = purchaseReturn;
    }
    
    

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getOpeningStock() {
        return openingStock;
    }

    public void setOpeningStock(Long openingStock) {
        this.openingStock = openingStock;
    }

    public Long getClosingStock() {
        return closingStock;
    }

    public void setClosingStock(Long closingStock) {
        this.closingStock = closingStock;
    }

    public Long getStockIn() {
        return stockIn;
    }

    public void setStockIn(Long stockIn) {
        this.stockIn = stockIn;
    }

    public Long getStockOut() {
        return stockOut;
    }

    public void setStockOut(Long stockOut) {
        this.stockOut = stockOut;
    }

    public Long getManualStockIn() {
        return manualStockIn;
    }

    public void setManualStockIn(Long manualStockIn) {
        this.manualStockIn = manualStockIn;
    }

    public Long getPurchase() {
        return purchase;
    }

    public void setPurchase(Long purchase) {
        this.purchase = purchase;
    }

    public Long getSalesReturn() {
        return salesReturn;
    }

    public void setSalesReturn(Long salesReturn) {
        this.salesReturn = salesReturn;
    }

    public Long getManualStockout() {
        return manualStockout;
    }

    public void setManualStockout(Long manualStockout) {
        this.manualStockout = manualStockout;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Long getPurchaseReturn() {
        return purchaseReturn;
    }

    public void setPurchaseReturn(Long purchaseReturn) {
        this.purchaseReturn = purchaseReturn;
    }

 

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

   
    
    

    

}
