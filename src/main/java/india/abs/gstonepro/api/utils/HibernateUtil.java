/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.utils;

import india.abs.gstonepro.H2DBServer;
import india.abs.gstonepro.api.models.Company;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

/**
 * @author SGS
 */
//public class HibernateUtil {
//   private static StandardServiceRegistry registry;
//   private static SessionFactory sessionFactory;
//
//   public static SessionFactory getSessionFactory() {
//      if (sessionFactory == null) {
//         try {
//
//            // Create registry builder
//            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
//
//            // Hibernate settings equivalent to hibernate.cfg.xml's properties
//            Map<String, String> settings = new HashMap<>();
//            settings.put(Environment.DRIVER, "org.postgresql.Driver");
//            settings.put(Environment.URL, "jdbc:postgresql://localhost:5432/gstonepro");
//            settings.put(Environment.USER, "dev");
//            settings.put(Environment.PASS, "iamasuperhuman");
//            settings.put(Environment.POOL_SIZE, "10");
//            settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
//            settings.put(Environment.HBM2DDL_AUTO, "create");
//
//            // Apply settings
//            registryBuilder.applySettings(settings);
//
//            // Create registry
//            registry = registryBuilder.build();
//            
//          
//
//            // Create MetadataSources
//            MetadataSources sources = new MetadataSources(registry);
//
//            // Create Metadata
//            Metadata metadata = sources.getMetadataBuilder().build();
//
//            // Create SessionFactory
//            sessionFactory = metadata.getSessionFactoryBuilder().build();
//
//         } catch (Exception e) {
//            e.printStackTrace();
//            if (registry != null) {
//               StandardServiceRegistryBuilder.destroy(registry);
//            }
//         }
//      }
//      return sessionFactory;
//   }
//
//   public static void shutdown() {
//      if (registry != null) {
//         StandardServiceRegistryBuilder.destroy(registry);
//      }
//   }
//}




//public class HibernateUtil {
//private static final SessionFactory concreteSessionFactory;
//	static {
//		try {
//			Properties prop= new Properties();
//                        prop.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
//			prop.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/gstonepro");
//			prop.setProperty("hibernate.connection.username", "dev");
//			prop.setProperty("hibernate.connection.password", "iamasuperhuman");
//			prop.setProperty("dialect", "org.hibernate.dialect.PostgreSQLDialect");
//			
//			concreteSessionFactory = new Configuration()
//		   .addPackage("com.concretepage.persistence")
//				   .addProperties(prop)
//				   .addAnnotatedClass(Company.class)
//				   .buildSessionFactory();
//		} catch (Throwable ex) {
//			throw new ExceptionInInitializerError(ex);
//		}
//	}
//	public static Session getSessionFactory()
//			throws HibernateException {
//		return concreteSessionFactory.openSession();
//	}
//
//}























public class HibernateUtil {

    private static SessionFactory sessionFactory = null;

    static {
        try {
           
            StandardServiceRegistry standardRegistry
                    = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
            Metadata metaData
                    = new MetadataSources(standardRegistry).getMetadataBuilder().build();
            sessionFactory = metaData.getSessionFactoryBuilder().build();
         
        }catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {

        return sessionFactory;

    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
        
    }

}


