/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findAllCDNLinetems",
                    query = "from CreditDebitNoteLineItem cdl where cdl.creditDebitNote=:cdNote"
            )
            ,
                  @NamedQuery(
                    name = "findCDNByProduct",
                    query = "from CreditDebitNoteLineItem cdl where cdl.product=:product and cdl.creditDebitNote.company=:company and cdl.creditDebitNote.noteType = :type and cdl.creditDebitNote.noteDate between :fromDate and :toDate "
            )

        }
)
public class CreditDebitNoteLineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "note_line_item_id", updatable = false, nullable = false)
    private Long noteLineItemId;

    @ManyToOne
    @JoinColumn(name = "cd_note_id")
    private CreditDebitNote creditDebitNote;

    private int noteLineNumber;

    private String invoiceNumber;

    private Date invoiceDate;

    private int lineNumber;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private String productName;

    private String gstReason;

    private int uqc1Qty;

    private int uqc2Qty;

    private int stockQuantity;

    private float gstPercentage;

    private BigDecimal value;

    private BigDecimal valueInclusiveOfTax;

    private String particulars;

    @Column(name = "igst_percentage")
    private float igstPercentage;

    @Column(name = "cgst_percentage")
    private float cgstPercentage;

    @Column(name = "sgst_percentage")
    private float sgstPercentage;

    @Column(name = "igst_value")
    private BigDecimal igstValue;

    @Column(name = "cgst_value")
    private BigDecimal cgstValue;

    @Column(name = "sgst_value")
    private BigDecimal sgstValue;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger accountsLedger;

    @Column(name = "gst_pos")
    private String gstPOS;

    @Column(name = "cess_amount")
    private BigDecimal cessAmount;

    @OneToOne(mappedBy = "cdnli", cascade = CascadeType.ALL)
    private GoDownStockEntry godownStockEntry;

    @Embedded
    private Audit auditData;

    @Transient
    private GoDown godown;

    private Boolean isIGST;

    private String descriptionOne;
    private String descriptionTwo;
    private String descriptionThree;
    private String descriptionFour;
    private String descriptionFive;
    private String descriptionSix;
    private String descriptionSeven;
    private String descriptionEight;
    private String descriptionNine;
    private String descriptionTen;

    private int descriptionCount;

    public Boolean isIGST() {
        return isIGST;
    }

    public void setIsIGST(Boolean isIGST) {
        this.isIGST = isIGST;
    }

    public CreditDebitNote getCreditDebitNote() {
        return creditDebitNote;
    }

    public void setCreditDebitNote(CreditDebitNote creditDebitNote) {
        this.creditDebitNote = creditDebitNote;
    }

    public Long getNoteLineItemId() {
        return noteLineItemId;
    }

    public void setNoteLineItemId(Long noteLineItemId) {
        this.noteLineItemId = noteLineItemId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getGstReason() {
        return gstReason;
    }

    public void setGstReason(String gstReason) {
        this.gstReason = gstReason;
    }

    public int getUqc1Qty() {
        return uqc1Qty;
    }

    public void setUqc1Qty(int uqc1Qty) {
        this.uqc1Qty = uqc1Qty;
    }

    public int getUqc2Qty() {
        return uqc2Qty;
    }

    public void setUqc2Qty(int uqc2Qty) {
        this.uqc2Qty = uqc2Qty;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public float getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(float gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValueInclusiveOfTax() {
        return valueInclusiveOfTax;
    }

    public void setValueInclusiveOfTax(BigDecimal valueInclusiveOfTax) {
        this.valueInclusiveOfTax = valueInclusiveOfTax;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public Ledger getAccountsLedger() {
        return accountsLedger;
    }

    public void setAccountsLedger(Ledger accountsLedger) {
        this.accountsLedger = accountsLedger;
    }

    public String getGstPOS() {
        return gstPOS;
    }

    public void setGstPOS(String gstPOS) {
        this.gstPOS = gstPOS;
    }

    public int getNoteLineNumber() {
        return noteLineNumber;
    }

    public void setNoteLineNumber(int noteLineNumber) {
        this.noteLineNumber = noteLineNumber;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public GoDownStockEntry getGodownStockEntry() {
        return godownStockEntry;
    }

    public void setGodownStockEntry(GoDownStockEntry godownStockEntry) {
        this.godownStockEntry = godownStockEntry;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getCessAmount() {
        return cessAmount;
    }

    public void setCessAmount(BigDecimal cessAmount) {
        this.cessAmount = cessAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescriptionOne() {
        return descriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        this.descriptionOne = descriptionOne;
    }

    public String getDescriptionTwo() {
        return descriptionTwo;
    }

    public void setDescriptionTwo(String descriptionTwo) {
        this.descriptionTwo = descriptionTwo;
    }

    public String getDescriptionThree() {
        return descriptionThree;
    }

    public void setDescriptionThree(String descriptionThree) {
        this.descriptionThree = descriptionThree;
    }

    public String getDescriptionFour() {
        return descriptionFour;
    }

    public void setDescriptionFour(String descriptionFour) {
        this.descriptionFour = descriptionFour;
    }

    public String getDescriptionFive() {
        return descriptionFive;
    }

    public void setDescriptionFive(String descriptionFive) {
        this.descriptionFive = descriptionFive;
    }

    public String getDescriptionSix() {
        return descriptionSix;
    }

    public void setDescriptionSix(String descriptionSix) {
        this.descriptionSix = descriptionSix;
    }

    public String getDescriptionSeven() {
        return descriptionSeven;
    }

    public void setDescriptionSeven(String descriptionSeven) {
        this.descriptionSeven = descriptionSeven;
    }

    public String getDescriptionEight() {
        return descriptionEight;
    }

    public void setDescriptionEight(String descriptionEight) {
        this.descriptionEight = descriptionEight;
    }

    public String getDescriptionNine() {
        return descriptionNine;
    }

    public void setDescriptionNine(String descriptionNine) {
        this.descriptionNine = descriptionNine;
    }

    public String getDescriptionTen() {
        return descriptionTen;
    }

    public void setDescriptionTen(String descriptionTen) {
        this.descriptionTen = descriptionTen;
    }

    public int getDescriptionCount() {
        return descriptionCount;
    }

    public void setDescriptionCount(int descriptionCount) {
        this.descriptionCount = descriptionCount;
    }

}
