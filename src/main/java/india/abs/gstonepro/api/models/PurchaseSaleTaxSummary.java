/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "deleteAllPSTStems",
                    query = "delete PurchaseSaleTaxSummary p where p.purchaseSale=:ps"
            ),
              @NamedQuery(
                    name = "findPSTSByPS",
                    query = "from PurchaseSaleTaxSummary p where p.purchaseSale=:ps"
            )
        }
)
public class PurchaseSaleTaxSummary {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pstax_id", updatable = false, nullable = false)
    private Long psTaxId;

   @ManyToOne
    @JoinColumn(name = "ps_id")
    private PurchaseSale purchaseSale;
    
   @Column(name="hsn_sac")
    private String hsnSac;
   
   @Column(name="taxable_value")
    private BigDecimal taxableValue;
    
   @Column(name="igst_percentage")
    private float igstPercentage;
   
    @Column(name="cgst_percentage")
    private float cgstPercentage;
    
    @Column(name="sgst_percentage")
    private float sgstPercentage;
    
    @Column(name="igst_value")
    private BigDecimal igstValue;
    
    @Column(name="cgst_value")
    private BigDecimal cgstValue;
    
    @Column(name="sgst_value")
    private BigDecimal sgstValue;
    
    @Column(name="total_tax_value")
    private BigDecimal totalTaxValue;
    
    @Column(name="cess_value")
    private BigDecimal cessValue;
    
    @Column(name="igst_itc_value")
    private BigDecimal igstITCValue;
    
    @Column(name="cgst_itc_value")
    private BigDecimal cgstITCValue;
    
    @Column(name="sgst_itc_value")
    private BigDecimal sgstITCValue;
    
    @Column(name="cess_itc_value")
    private BigDecimal cessITCValue;
    
    @Column(name="total_itc_value")
    private BigDecimal totalITCValue;
    
     @Column(name = "cancelled")
    private boolean cancelled;
    
    
    
    @Embedded
    private Audit auditData;

    public Long getPsTaxId() {
        return psTaxId;
    }

    public void setPsTaxId(Long psTaxId) {
        this.psTaxId = psTaxId;
    }

    public PurchaseSale getPurchaseSale() {
        return purchaseSale;
    }

    public void setPurchaseSale(PurchaseSale purchaseSale) {
        this.purchaseSale = purchaseSale;
    }

   

  

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public BigDecimal getTaxableValue() {
        return taxableValue;
    }

    public void setTaxableValue(BigDecimal taxableValue) {
        this.taxableValue = taxableValue;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getTotalTaxValue() {
        return totalTaxValue;
    }

    public void setTotalTaxValue(BigDecimal totalTaxValue) {
        this.totalTaxValue = totalTaxValue;
    }

    @Override
    public String toString() {
        return "PurchaseSaleTaxSummary{" + "psTaxId=" + psTaxId + ", hsnSac=" + hsnSac + ", taxableValue=" + taxableValue + ", igstPercentage=" + igstPercentage + ", cgstPercentage=" + cgstPercentage + ", sgstPercentage=" + sgstPercentage + ", igstValue=" + igstValue + ", cgstValue=" + cgstValue + ", sgstValue=" + sgstValue + ", totalTaxValue=" + totalTaxValue + '}';
    }
    
     public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public BigDecimal getCessValue() {
        return cessValue;
    }

    public void setCessValue(BigDecimal cessValue) {
        this.cessValue = cessValue;
    }

    public BigDecimal getIgstITCValue() {
        return igstITCValue;
    }

    public void setIgstITCValue(BigDecimal igstITCValue) {
        this.igstITCValue = igstITCValue;
    }

    public BigDecimal getCgstITCValue() {
        return cgstITCValue;
    }

    public void setCgstITCValue(BigDecimal cgstITCValue) {
        this.cgstITCValue = cgstITCValue;
    }

    public BigDecimal getSgstITCValue() {
        return sgstITCValue;
    }

    public void setSgstITCValue(BigDecimal sgstITCValue) {
        this.sgstITCValue = sgstITCValue;
    }

    public BigDecimal getCessITCValue() {
        return cessITCValue;
    }

    public void setCessITCValue(BigDecimal cessITCValue) {
        this.cessITCValue = cessITCValue;
    }

    public BigDecimal getTotalITCValue() {
        return totalITCValue;
    }

    public void setTotalITCValue(BigDecimal totalITCValue) {
        this.totalITCValue = totalITCValue;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
    
    

}
