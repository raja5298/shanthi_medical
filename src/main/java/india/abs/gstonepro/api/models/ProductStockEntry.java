/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "getStockType",
            query = "from ProductStockEntry ps where stockUpdateType=:stockType")
    ,@NamedQuery(name = "getAllStockTypes",
            query = "from ProductStockEntry ps where company=:company")
    ,@NamedQuery(name = "getAllStockTypesTillDate",
            query = "from ProductStockEntry ps where company=:company and stockUpdateDate <=:tillDate")
    ,@NamedQuery(
            name = "getProductStock",
            query = "from ProductStockEntry ps where company=:company and product=:product"
    )
    ,@NamedQuery(
            name = "deleteAllProductStock",
            query = "delete ProductStockEntry p where p.psli=:psli"
    )
    ,@NamedQuery(
            name = "getStockUpdates",
            query = "from ProductStockEntry ps where stockUpdateDate between :fromDate and :toDate"
    )
    ,@NamedQuery(
            name = "getStockUpdatesforProduct",
            query = "from ProductStockEntry ps where product=:product and stockUpdateDate between :fromDate and :toDate"
    )})

public class ProductStockEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_stock_entry_id", updatable = false, nullable = false)
    private Long productStockEntryId;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;
    
    @Column(name="quantity")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @OneToOne
    @JoinColumn(name = "psli_id")
    private PurchaseSaleLineItem psli;
    
    @Column(name="stock_update_type")
    private String stockUpdateType;
    
    @Column(name="stock_value")
    private BigDecimal stockValue;

    @Temporal(TemporalType.DATE)
    @Column(name="stock_update_date")
    private Date stockUpdateDate;
    
    @Column(name="igst_percentage")
    private float igstPercentage;
    
    @Column(name="cgst_percentage")
    private float cgstPercentage;
    
    @Column(name="sgst_percentage")
    private float sgstPercentage;
    
    @Column(name="igst_amount")
    private BigDecimal igstAmount;
    
    @Column(name="cgst_amount")
    private BigDecimal cgstAmount;
    
    @Column(name="sgst_amount")
    private BigDecimal sgstAmount;

    @Embedded
    private Audit auditData;

    public ProductStockEntry() {
    }

    public ProductStockEntry(Long productStockEntryId, int quantity, Product product, String stockUpdateType, BigDecimal stockValue, Date stockUpdateDate, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal igstAmount, BigDecimal cgstAmount, BigDecimal sgstAmount) {
        this.productStockEntryId = productStockEntryId;
        this.quantity = quantity;
        this.product = product;
        this.stockUpdateType = stockUpdateType;
        this.stockValue = stockValue;
        this.stockUpdateDate = stockUpdateDate;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
        this.igstAmount = igstAmount;
        this.cgstAmount = cgstAmount;
        this.sgstAmount = sgstAmount;
    }

    public ProductStockEntry(int quantity, Product product, String stockUpdateType, BigDecimal stockValue, Date stockUpdateDate, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal igstAmount, BigDecimal cgstAmount, BigDecimal sgstAmount) {
        this.quantity = quantity;
        this.product = product;
        this.stockUpdateType = stockUpdateType;
        this.stockValue = stockValue;
        this.stockUpdateDate = stockUpdateDate;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
        this.igstAmount = igstAmount;
        this.cgstAmount = cgstAmount;
        this.sgstAmount = sgstAmount;
    }

    public ProductStockEntry(int quantity, String stockUpdateType, BigDecimal stockValue, Date stockUpdateDate, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal igstAmount, BigDecimal cgstAmount, BigDecimal sgstAmount) {
        this.quantity = quantity;
        this.stockUpdateType = stockUpdateType;
        this.stockValue = stockValue;
        this.stockUpdateDate = stockUpdateDate;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
        this.igstAmount = igstAmount;
        this.cgstAmount = cgstAmount;
        this.sgstAmount = sgstAmount;
    }

    public int getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public BigDecimal getStockValue() {
        return stockValue;
    }

    public Date getStockUpdateDate() {
        return stockUpdateDate;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public BigDecimal getIgstAmount() {
        return igstAmount;
    }

    public BigDecimal getCgstAmount() {
        return cgstAmount;
    }

    public BigDecimal getSgstAmount() {
        return sgstAmount;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setStockValue(BigDecimal stockValue) {
        this.stockValue = stockValue;
    }

    public void setStockUpdateDate(Date stockUpdateDate) {
        this.stockUpdateDate = stockUpdateDate;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public void setIgstAmount(BigDecimal igstAmount) {
        this.igstAmount = igstAmount;
    }

    public void setCgstAmount(BigDecimal cgstAmount) {
        this.cgstAmount = cgstAmount;
    }

    public void setSgstAmount(BigDecimal sgstAmount) {
        this.sgstAmount = sgstAmount;
    }

    public String getStockUpdateType() {
        return stockUpdateType;
    }

    public void setStockUpdateType(String stockUpdateType) {
        this.stockUpdateType = stockUpdateType;
    }

    public Long getProductStockEntryId() {
        return productStockEntryId;
    }

    public void setProductStockEntryId(Long productStockEntryId) {
        this.productStockEntryId = productStockEntryId;
    }

    @Override
    public String toString() {
        return "ProductStock{" + "productStockEntryId=" + productStockEntryId + ", quantity=" + quantity + ", product=" + product + ", stockUpdateType=" + stockUpdateType + ", stockValue=" + stockValue + ", stockUpdateDate=" + stockUpdateDate + ", igstPercentage=" + igstPercentage + ", cgstPercentage=" + cgstPercentage + ", sgstPercentage=" + sgstPercentage + ", igstAmount=" + igstAmount + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount + '}';
    }

    public PurchaseSaleLineItem getPsli() {
        return psli;
    }

    public void setPsli(PurchaseSaleLineItem psli) {
        this.psli = psli;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

}
