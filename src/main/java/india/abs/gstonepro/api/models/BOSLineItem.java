/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "find-BOSLineItemsByProduct",
                    query = "from BOSLineItem e where e.bos.bosDate between :fromDate and :toDate and e.product=:product and e.bos.cancelled=:cancelled"
            )
        }
)
public class BOSLineItem {

    private int lineNumber;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bos_line_item_id", updatable = false, nullable = false)
    private long bosLineItemId;

    @ManyToOne
    @JoinColumn(name = "bos_id")
    private BOS bos;

    @ManyToOne
    private Product product;

    @OneToOne(mappedBy = "bosli", cascade = CascadeType.ALL)
    private GoDownStockEntry godownStockEntry;

    @Transient
    private GoDown godown;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "uqc_one")
    private String uqcOne;

    @Column(name = "uqc_one_quantity")
    private int uqcOneQuantity;

    @Column(name = "uqc_one_rate")
    private BigDecimal uqcOneRate;

    @Column(name = "uqc_one_value")
    private BigDecimal uqcOneValue;

    @Column(name = "uqc_two")
    private String uqcTwo;

    @Column(name = "uqc_two_quantity")
    private int uqcTwoQuantity;

    @Column(name = "uqc_two_rate")
    private BigDecimal uqcTwoRate;

    @Column(name = "uqc_two_value")
    private BigDecimal uqcTwoValue;

    @Column(name = "stock_quantity")
    private int stockQuantity;

    @Column(name = "hsn_sac")
    private String hsnSac;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_value")
    private BigDecimal discountValue;

    @Column(name = "value")
    private BigDecimal value;

    //netValue = value - discount
    @Column(name = "net_value")
    private BigDecimal netValue;

    @Column(name = "uqc1_base_rate")
    private BigDecimal uqc1BaseRate;

    @Column(name = "uqc2_base_rate")
    private BigDecimal uqc2BaseRate;

    @Column(name = "profit")
    private BigDecimal profit;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Embedded
    private Audit auditData;
    @Column(name = "descriptionone")
    private String descriptionOne;

    @Column(name = "descriptiontwo")
    private String descriptionTwo;

    @Column(name = "descriptionthree")
    private String descriptionThree;

    @Column(name = "descriptionfour")
    private String descriptionFour;

    @Column(name = "descriptionfive")
    private String descriptionFive;

    @Column(name = "descriptionsix")
    private String descriptionSix;

    @Column(name = "descriptionseven")
    private String descriptionSeven;

    @Column(name = "descriptioneight")
    private String descriptionEight;

    @Column(name = "descriptionnine")
    private String descriptionNine;

    @Column(name = "descriptionoten")
    private String descriptionTen;

    @Column(name = "descriptioncount")
    private int descriptioncount;

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long getBOSLineItemId() {
        return bosLineItemId;
    }

    public void setBOSLineItemId(long bosLineItemId) {
        this.bosLineItemId = bosLineItemId;
    }

    public BOS getBOS() {
        return bos;
    }

    public void setBOS(BOS bos) {
        this.bos = bos;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUqcOne() {
        return uqcOne;
    }

    public void setUqcOne(String uqcOne) {
        this.uqcOne = uqcOne;
    }

    public int getUqcOneQuantity() {
        return uqcOneQuantity;
    }

    public void setUqcOneQuantity(int uqcOneQuantity) {
        this.uqcOneQuantity = uqcOneQuantity;
    }

    public BigDecimal getUqcOneRate() {
        return uqcOneRate;
    }

    public void setUqcOneRate(BigDecimal uqcOneRate) {
        this.uqcOneRate = uqcOneRate;
    }

    public BigDecimal getUqcOneValue() {
        return uqcOneValue;
    }

    public void setUqcOneValue(BigDecimal uqcOneValue) {
        this.uqcOneValue = uqcOneValue;
    }

    public String getUqcTwo() {
        return uqcTwo;
    }

    public void setUqcTwo(String uqcTwo) {
        this.uqcTwo = uqcTwo;
    }

    public int getUqcTwoQuantity() {
        return uqcTwoQuantity;
    }

    public void setUqcTwoQuantity(int uqcTwoQuantity) {
        this.uqcTwoQuantity = uqcTwoQuantity;
    }

    public BigDecimal getUqcTwoRate() {
        return uqcTwoRate;
    }

    public void setUqcTwoRate(BigDecimal uqcTwoRate) {
        this.uqcTwoRate = uqcTwoRate;
    }

    public BigDecimal getUqcTwoValue() {
        return uqcTwoValue;
    }

    public void setUqcTwoValue(BigDecimal uqcTwoValue) {
        this.uqcTwoValue = uqcTwoValue;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getNetValue() {
        return netValue;
    }

    public void setNetValue(BigDecimal netValue) {
        this.netValue = netValue;
    }

    public BigDecimal getUqc1BaseRate() {
        return uqc1BaseRate;
    }

    public void setUqc1BaseRate(BigDecimal uqc1BaseRate) {
        this.uqc1BaseRate = uqc1BaseRate;
    }

    public BigDecimal getUqc2BaseRate() {
        return uqc2BaseRate;
    }

    public void setUqc2BaseRate(BigDecimal uqc2BaseRate) {
        this.uqc2BaseRate = uqc2BaseRate;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public String getDescriptionOne() {
        return descriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        this.descriptionOne = descriptionOne;
    }

    public String getDescriptionTwo() {
        return descriptionTwo;
    }

    public void setDescriptionTwo(String descriptionTwo) {
        this.descriptionTwo = descriptionTwo;
    }

    public String getDescriptionThree() {
        return descriptionThree;
    }

    public void setDescriptionThree(String descriptionThree) {
        this.descriptionThree = descriptionThree;
    }

    public String getDescriptionFour() {
        return descriptionFour;
    }

    public void setDescriptionFour(String descriptionFour) {
        this.descriptionFour = descriptionFour;
    }

    public String getDescriptionFive() {
        return descriptionFive;
    }

    public void setDescriptionFive(String descriptionFive) {
        this.descriptionFive = descriptionFive;
    }

    public String getDescriptionSix() {
        return descriptionSix;
    }

    public void setDescriptionSix(String descriptionSix) {
        this.descriptionSix = descriptionSix;
    }

    public String getDescriptionSeven() {
        return descriptionSeven;
    }

    public void setDescriptionSeven(String descriptionSeven) {
        this.descriptionSeven = descriptionSeven;
    }

    public String getDescriptionEight() {
        return descriptionEight;
    }

    public void setDescriptionEight(String descriptionEight) {
        this.descriptionEight = descriptionEight;
    }

    public String getDescriptionNine() {
        return descriptionNine;
    }

    public void setDescriptionNine(String descriptionNine) {
        this.descriptionNine = descriptionNine;
    }

    public String getDescriptionTen() {
        return descriptionTen;
    }

    public void setDescriptionTen(String descriptionTen) {
        this.descriptionTen = descriptionTen;
    }

    public int getDescriptioncount() {
        return descriptioncount;
    }

    public void setDescriptioncount(int descriptioncount) {
        this.descriptioncount = descriptioncount;
    }

    public long getBosLineItemId() {
        return bosLineItemId;
    }

    public void setBosLineItemId(long bosLineItemId) {
        this.bosLineItemId = bosLineItemId;
    }

    public BOS getBos() {
        return bos;
    }

    public void setBos(BOS bos) {
        this.bos = bos;
    }

    public GoDownStockEntry getGodownStockEntry() {
        return godownStockEntry;
    }

    public void setGodownStockEntry(GoDownStockEntry godownStockEntry) {
        this.godownStockEntry = godownStockEntry;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

}
