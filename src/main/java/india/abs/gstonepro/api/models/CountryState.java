/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author SGS
 */
@Entity
@Table(name = "CountryState",
        uniqueConstraints = @UniqueConstraint(columnNames = {"stateName"}))
public class CountryState {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "state_id", updatable = false, nullable = false)
    private Long stateId;

    @Column(name = "state_code")
    private String stateCode;

    private String stateName;

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

}
