/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author SGS
 */
@Entity
public class Payment {
    @Id
    private String paymentId;
    
    @Column(name="type")
    private String type;
    
     @OneToMany(fetch = FetchType.LAZY, mappedBy = "payment")
    private Set<Transaction> transactions;
     
     @OneToMany(fetch = FetchType.EAGER, mappedBy = "payment")
    private Set<PaymentLineItem> plis;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Set<PaymentLineItem> getPlis() {
        return plis;
    }

    public void setPlis(Set<PaymentLineItem> plis) {
        this.plis = plis;
    }
     
     
    
}
