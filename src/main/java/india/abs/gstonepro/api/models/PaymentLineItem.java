/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author SGS
 */
@Entity
public class PaymentLineItem {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_line_item_id", updatable = false, nullable = false)
    private Long paymentLineItemId;
    
    @ManyToOne
    @JoinColumn(name = "payment_id")
    private Payment payment;
    
    @Column(name="total_bill_amount")
    private BigDecimal totalBillAmount;
    
    @Column(name="paid_bill_amount")
    private BigDecimal paidBillAmount;
    
    @Column(name="variation_amount")
    private BigDecimal variationAmount;

    public Long getPaymentLineItemId() {
        return paymentLineItemId;
    }

    public void setPaymentLineItemId(Long paymentLineItemId) {
        this.paymentLineItemId = paymentLineItemId;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public BigDecimal getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(BigDecimal totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public BigDecimal getPaidBillAmount() {
        return paidBillAmount;
    }

    public void setPaidBillAmount(BigDecimal paidBillAmount) {
        this.paidBillAmount = paidBillAmount;
    }

    public BigDecimal getVariationAmount() {
        return variationAmount;
    }

    public void setVariationAmount(BigDecimal variationAmount) {
        this.variationAmount = variationAmount;
    }
    
    
    
    
    
}
