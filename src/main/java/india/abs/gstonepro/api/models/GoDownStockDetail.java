/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author ceo
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findGoDownsProductDetail",
                    query = "from GoDownStockDetail g where product=:product and g.godown=:godown"
            ),
            @NamedQuery(
                    name = "findAllOpeningStocks",
                    query = "from GoDownStockDetail g where product=:product"
            ),
             @NamedQuery(
                    name = "findAllProductGSD",
                    query = "from GoDownStockDetail g where g.godown=:godown"
            )
        })
public class GoDownStockDetail {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "godown_stock_detail_id", updatable = false, nullable = false)
    private Long goDownStockDetailId;
    
    @ManyToOne
    @JoinColumn(name = "godown_id", nullable = false)
    private GoDown godown;
    
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;
    
    private Long currentStock;
    
    private Long openingStock;
    
    private Date openingStockDate;
    
    private BigDecimal openingStockValue;

    public Long getGoDownStockDetailId() {
        return goDownStockDetailId;
    }

    public void setGoDownStockDetailId(Long goDownStockDetailId) {
        this.goDownStockDetailId = goDownStockDetailId;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(Long currentStock) {
        this.currentStock = currentStock;
    }

    public Long getOpeningStock() {
        return openingStock;
    }

    public void setOpeningStock(Long openingStock) {
        this.openingStock = openingStock;
    }

    public Date getOpeningStockDate() {
        return openingStockDate;
    }

    public void setOpeningStockDate(Date openingStockDate) {
        this.openingStockDate = openingStockDate;
    }

    public BigDecimal getOpeningStockValue() {
        return openingStockValue;
    }

    public void setOpeningStockValue(BigDecimal openingStockValue) {
        this.openingStockValue = openingStockValue;
    }

    @Override
    public String toString() {
        return "GoDownStockDetail{" + "goDownStockDetailId=" + goDownStockDetailId + ", godown=" + godown + ", product=" + product + ", currentStock=" + currentStock + ", openingStock=" + openingStock + ", openingStockDate=" + openingStockDate + ", openingStockValue=" + openingStockValue + '}';
    }
    
    
    
}
