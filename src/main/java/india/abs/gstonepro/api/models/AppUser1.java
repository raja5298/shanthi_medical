package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author SGS
 */
@NamedQueries(
        {
            @NamedQuery(
                    name = "findUerByNameAndPassword",
                    query = "from AppUser u where u.userName = :userName and u.password = :password"
            )
        }
)
@Entity
@Table(name = "AppUser",
        uniqueConstraints = @UniqueConstraint(columnNames = {"user_name"}))
public class AppUser1 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", updatable = false, nullable = false)
    private Long userId;


    @Column(name="user_name")
    private String userName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "password")
    private String password;

    @Column(name = "logged_in")
    private Boolean loggedIn;

    @Column(name = "admin")
    private Boolean admin;

    public AppUser1(Long userId, String userName, String phoneNumber, String password, Boolean loggedIn) {
        this.userId = userId;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.loggedIn = loggedIn;
    }

    public AppUser1(String userName, String phoneNumber, String password, Boolean loggedIn) {
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.loggedIn = loggedIn;
    }

    public AppUser1() {

    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "AppUser{" + "userId=" + userId + ", userName=" + userName + ", phoneNumber=" + phoneNumber + ", password=" + password + ", loggedIn=" + loggedIn + ", admin=" + admin + '}';
    }

}
