/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author SGS
 */
@Entity
public class Journal implements Serializable {

    @Id
    @Column(name = "journal_id", updatable = false, nullable = false)
    private String journalId;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "journal",cascade = CascadeType.ALL)
    private Set<Transaction> transactions = new HashSet<>();
    
    @Embedded
    private Audit auditData;

    public String getJournalId() {
        return journalId;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setJournalId(String journalId) {
        this.journalId = journalId;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }
    

}
