/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author SGS
 */
@Entity
public class UQC {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "uqc_id", updatable = false, nullable = false)
    private Long id;
    
    @Column(name = "quantity_name")
    private String quantityName;
    
    @Column(name = "quantity_type")
    private String quantityType;
    
    @Column(name = "uqc_code")
    private String UQCCode;

    public UQC(String quantityName, String quantityType, String UQCCode) {
        this.quantityName = quantityName;
        this.quantityType = quantityType;
        this.UQCCode = UQCCode;
    }

    public UQC() {
        
    }

    public String getQuantityName() {
        return quantityName;
    }

    public String getQuantityType() {
        return quantityType;
    }

    public String getUQCCode() {
        return UQCCode;
    }

    public void setQuantityName(String quantityName) {
        this.quantityName = quantityName;
    }

    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    public void setUQCCode(String UQCCode) {
        this.UQCCode = UQCCode;
    }
    
   
    
    

    public Long getId() {
        return id;
    }

   

    public void setId(Long id) {
        this.id = id;
    }

   
    
    
}
