/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findAllPSByType",
                    query = "from PurchaseSale s where s.psType = :type and s.psDate between :fromDate and :toDate and s.company=:company"
            )
            ,
            @NamedQuery(
                    name = "findPSByType",
                    query = "from PurchaseSale s where s.psType = :type and s.psDate between :fromDate and :toDate and s.company=:company and s.cancelled=:withCancelledBill"
            )
            ,
                 @NamedQuery(
                    name = "findB2BPSByDate",
                    query = "from PurchaseSale s where  s.psType = :type and s.psDate between :fromDate and :toDate and s.company=:company and s.cancelled=:withCancelledBill and s.b2bInvoice=:isB2B"
            )
            ,
                @NamedQuery(
                    name = "findB2CPSByDate",
                    query = "from PurchaseSale s where  s.psType = :type and s.psDate between :fromDate and :toDate and s.company=:company and s.cancelled=:withCancelledBill and s.b2bInvoice=:isB2B"
            )
            ,
             @NamedQuery(
                    name = "findPSByLedger",
                    query = "from PurchaseSale s where s.ledger=:ledger and s.psType = :type and s.psDate between :fromDate and :toDate and s.company=:company and s.cancelled=:withCancelledBill"
            )
            ,
             @NamedQuery(
                    name = "findPSByLedgerWithoutDate",
                    query = "from PurchaseSale s where s.ledger=:ledger and s.psType = :type and  s.company=:company and s.cancelled=:withCancelledBill"
            )
            ,
             @NamedQuery(
                    name = "findPSByAudit",
                    query = "from PurchaseSale s where  s.psType = :type and  s.company=:company and s.auditData.createdOn between :fromDate and :toDate or s.auditData.updatedOn between :fromDate and :toDate or s.auditData.deletedOn between :fromDate and :toDate"
            )
            ,@NamedQuery(
                    name = "findAllPSByTypeWithOutDate",
                    query = "from PurchaseSale s where s.psType = :type and  s.company=:company"
            )
            , @NamedQuery(
                    name = "findPSByLedgerWitoutDate",
                    query = "from PurchaseSale s where s.ledger=:ledger and s.psType = :type and s.company=:company and s.cancelled=:withCancelledBill"
            )
            ,@NamedQuery(
                    name = "findPSByTypeWitOutDate",
                    query = "from PurchaseSale s where s.psType = :type and s.company=:company and s.cancelled=:withCancelledBill"
            )
            

        }
)
public class PurchaseSale {

    @Id
    @Column(name = "psId", updatable = false, nullable = false)
    private String psId;

    @Column(name = "ps_type")
    private String psType;

    @Temporal(TemporalType.DATE)
    @Column(name = "ps_date")
    private Date psDate;

    @Column(name = "bill_no")
    private String billNo;

    @Column(name = "bill_amount")
    private BigDecimal billAmount;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger ledger;

    @ManyToOne
    @JoinColumn(name = "emp_id")
    private Ledger employee;

    //  private Set<Transaction> transactions;
    @Column(name = "mode_of_delivery")
    private String modeOfDelivery;

    @Column(name = "remarks")
    private String remarks;


    @Column(name = "free")
    private BigDecimal free;

    //Difference between Bill Amount & Calculated Total Amount
    @Column(name = "variation_amount")
    private BigDecimal variationAmount;

    //Original Bill Amount
    @Column(name = "calculated_total_value")
    private BigDecimal calculatedTotalValue;

    //Cash or Credit
    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "total_amount_paid")
    private BigDecimal totalAmountPaid;

    @Column(name = "through")
    private String through;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "igst_value")
    private BigDecimal igstValue;

    @Column(name = "cgst_value")
    private BigDecimal cgstValue;

    @Column(name = "sgst_value")
    private BigDecimal sgstValue;

    @Column(name = "round_off_value")
    private BigDecimal roundOffValue;

    @Column(name = "is_igst")
    public boolean isIGST;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Column(name = "is_profit")
    private BigDecimal profit;

    @Column(name = "sms_message")
    private String smsMessage;

    @Column(name = "pricing_policy_name")
    private String pricingPolicyName;

    @Column(name = "reverse_charge_applicable")
    private boolean reverseChargeApplicable;

    @Column(name = "eligible_for_itc")
    private boolean eligibleForITC;

    @Column(name = "shipping_address_name")
    private String shippingAddressName;

    @Column(name = "shipping_address_one")
    private String shippingAddressOne;

    @Column(name = "shipping_address_two")
    private String shippingAddressTwo;

    @Column(name = "shipping_address_three")
    private String shippingAddressThree;

    @Column(name = "shipping_address_city")
    private String shippingAddressCity;

    @Column(name = "shipping_address_district")
    private String shippingAddressDistrict;

    @Column(name = "shipping_address_state")
    private String shippingAddressState;

    @Column(name = "shipping_address_phone")
    private String shippingAddressPhone;

    @Column(name = "shipping_address_mobile")
    private String shippingAddressMobile;

    @Column(name = "no_party")
    private Boolean noParty;

    @Column(name = "no_party_name")
    private String noPartyName;

    @Column(name = "no_party_line_one")
    private String noPartyLineOne;

    @Column(name = "no_party_city")
    private String noPartyCity;

    @Column(name = "no_party_district")
    private String noPartyDistrict;

    @Column(name = "no_party_state")
    private String noPartyState;

    @Column(name = "no_party_gstin")
    private String noPartyGSTIN;

    @Column(name = "b2b_invoice")
    private boolean b2bInvoice;

    private BigDecimal totalCessAmount;

    @Transient
    @Column(name = "current_payment_amount")
    private BigDecimal currentPaymentAmount;

    @OneToOne
    @JoinColumn(name = "journal_id")
    private Journal purchaseSaleJournal;

    @Embedded
    private Audit auditData;

    private String eWayBillNo;

    private Boolean eWayBill;

    private Date eWayBillDate;

    private String eWayBillVechileNo;

    private String eWayBillVechileName;

    private String eWayBillGeneratedBy;

    private Date eWayBillGeneratedAtTimeStamp;

    private String eWayBillTransportDcoument;

    private Date eWayBillTransportDate;

    private String eWayBillReasonForTransportation;

    //private Set<PurchaseSaleTaxSummary> taxPercentSummaries;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseSale", cascade = CascadeType.ALL)
    private List<PurchaseSaleLineItem> psLineItems = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchaseSale", cascade = CascadeType.ALL)
    private List<PurchaseSaleTaxSummary> psTaxSummaries = new ArrayList<>();

    public String getPsType() {
        return psType;
    }

    public void setPsType(String psType) {
        this.psType = psType;
    }

    public String getThrough() {
        return through;
    }

    public void setThrough(String through) {
        this.through = through;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getPsId() {
        return psId;
    }

    public void setPsId(String psId) {
        this.psId = psId;
    }

    public Date getPsDate() {
        return psDate;
    }

    public void setPsDate(Date psDate) {
        this.psDate = psDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Ledger getEmployee() {
        return employee;
    }

    public void setEmployee(Ledger employee) {
        this.employee = employee;
    }

    public String getModeOfDelivery() {
        return modeOfDelivery;
    }

    public void setModeOfDelivery(String modeOfDelivery) {
        this.modeOfDelivery = modeOfDelivery;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

   

    public BigDecimal getFreeAmount() {
        return free;
    }

    public void setFreeAmount(BigDecimal free) {
        this.free = free; 
    }

    public BigDecimal getVariationAmount() {
        return variationAmount;
    }

    public void setVariationAmount(BigDecimal variationAmount) {
        this.variationAmount = variationAmount;
    }

    public BigDecimal getCalculatedTotalValue() {
        return calculatedTotalValue;
    }

    public void setCalculatedTotalValue(BigDecimal calculatedTotalValue) {
        this.calculatedTotalValue = calculatedTotalValue;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public List<PurchaseSaleLineItem> getPsLineItems() {
        return psLineItems;
    }

    public void setPsLineItems(List<PurchaseSaleLineItem> psLineItems) {
        this.psLineItems = psLineItems;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getRoundOffValue() {
        return roundOffValue;
    }

    public void setRoundOffValue(BigDecimal roundOffValue) {
        this.roundOffValue = roundOffValue;
    }

    public boolean isIsIGST() {
        return isIGST;
    }

    public void setIsIGST(boolean isIGST) {
        this.isIGST = isIGST;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "PurchaseSale{" + "psId=" + psId + ", psType=" + psType + ", psDate=" + psDate + ", billNo=" + billNo + ", billAmount=" + billAmount + ", ledger=" + ledger + ",\n modeOfDelivery=" + modeOfDelivery + ", remarks=" + remarks + ", free=" + free + ", variationAmount=" + variationAmount + ", calculatedTotalValue=" + calculatedTotalValue + ", paymentType=" + paymentType + ", totalAmountPaid=" + totalAmountPaid + ", through=" + through + ", referenceNumber=" + referenceNumber + ",\n igstValue=" + igstValue + ", cgstValue=" + cgstValue + ", sgstValue=" + sgstValue + ", roundOffValue=" + roundOffValue + ", isIGST=" + isIGST + '}';
    }

    public void printCollections() {
        for (PurchaseSaleLineItem psli : this.getPsLineItems()) {

        }
        for (PurchaseSaleTaxSummary psts : this.getPsTaxSummaries()) {

        }

    }

    public List<PurchaseSaleTaxSummary> getPsTaxSummaries() {
        return psTaxSummaries;
    }

    public void setPsTaxSummaries(List<PurchaseSaleTaxSummary> psTaxSummaries) {
        this.psTaxSummaries = psTaxSummaries;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean isCancelled) {
        this.cancelled = isCancelled;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getPricingPolicyName() {
        return pricingPolicyName;
    }

    public void setPricingPolicyName(String pricingPolicyName) {
        this.pricingPolicyName = pricingPolicyName;
    }

    public Journal getPurchaseSaleJournal() {
        return purchaseSaleJournal;
    }

    public void setPurchaseSaleJournal(Journal purchaseSaleJournal) {
        this.purchaseSaleJournal = purchaseSaleJournal;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public BigDecimal getCurrentPaymentAmount() {
        return currentPaymentAmount;
    }

    public void setCurrentPaymentAmount(BigDecimal currentPaymentAmount) {
        this.currentPaymentAmount = currentPaymentAmount;
    }

    public boolean isReverseChargeApplicable() {
        return reverseChargeApplicable;
    }

    public void setReverseChargeApplicable(boolean reverseChargeApplicable) {
        this.reverseChargeApplicable = reverseChargeApplicable;
    }

    public boolean isEligibleForITC() {
        return eligibleForITC;
    }

    public void setEligibleForITC(boolean eligibleForITC) {
        this.eligibleForITC = eligibleForITC;
    }

    public String getShippingAddressName() {
        return shippingAddressName;
    }

    public void setShippingAddressName(String shippingAddressName) {
        this.shippingAddressName = shippingAddressName;
    }

    public String getShippingAddressOne() {
        return shippingAddressOne;
    }

    public void setShippingAddressOne(String shippingAddressOne) {
        this.shippingAddressOne = shippingAddressOne;
    }

    public String getShippingAddressTwo() {
        return shippingAddressTwo;
    }

    public String getShippingAddressThree() {
        return shippingAddressThree;
    }

    public void setShippingAddressThree(String shippingAddressThree) {
        this.shippingAddressThree = shippingAddressThree;
    }

    public String getShippingAddressCity() {
        return shippingAddressCity;
    }

    public void setShippingAddressCity(String shippingAddressCity) {
        this.shippingAddressCity = shippingAddressCity;
    }

    public String getShippingAddressState() {
        return shippingAddressState;
    }

    public void setShippingAddressState(String shippingAddressState) {
        this.shippingAddressState = shippingAddressState;
    }

    public String getNoPartyName() {
        return noPartyName;
    }

    public void setNoPartyName(String noPartyName) {
        this.noPartyName = noPartyName;
    }

    public String getNoPartyLineOne() {
        return noPartyLineOne;
    }

    public void setNoPartyLineOne(String noPartyLineOne) {
        this.noPartyLineOne = noPartyLineOne;
    }

    public String getNoPartyCity() {
        return noPartyCity;
    }

    public void setNoPartyCity(String noPartyCity) {
        this.noPartyCity = noPartyCity;
    }

    public String getNoPartyDistrict() {
        return noPartyDistrict;
    }

    public void setNoPartyDistrict(String noPartyDistrict) {
        this.noPartyDistrict = noPartyDistrict;
    }

    public String getNoPartyState() {
        return noPartyState;
    }

    public void setNoPartyState(String noPartyState) {
        this.noPartyState = noPartyState;
    }

    public String getNoPartyGSTIN() {
        return noPartyGSTIN;
    }

    public void setNoPartyGSTIN(String noPartyGSTIN) {
        this.noPartyGSTIN = noPartyGSTIN;
    }

    public boolean isB2bInvoice() {
        return b2bInvoice;
    }

    public void setB2bInvoice(boolean b2bInvoice) {
        this.b2bInvoice = b2bInvoice;
    }

    public void setShippingAddressTwo(String shippingAddressTwo) {
        this.shippingAddressTwo = shippingAddressTwo;
    }

    public String getShippingAddressDistrict() {
        return shippingAddressDistrict;
    }

    public void setShippingAddressDistrict(String shippingAddressDistrict) {
        this.shippingAddressDistrict = shippingAddressDistrict;
    }

    public String getShippingAddressPhone() {
        return shippingAddressPhone;
    }

    public void setShippingAddressPhone(String shippingAddressPhone) {
        this.shippingAddressPhone = shippingAddressPhone;
    }

    public String getShippingAddressMobile() {
        return shippingAddressMobile;
    }

    public void setShippingAddressMobile(String shippingAddressMobile) {
        this.shippingAddressMobile = shippingAddressMobile;
    }

    public Boolean isNoParty() {
        return noParty;
    }

    public void setNoParty(Boolean noParty) {
        this.noParty = noParty;
    }

    public BigDecimal getTotalCessAmount() {
        return totalCessAmount;
    }

    public void setTotalCessAmount(BigDecimal totalCessAmount) {
        this.totalCessAmount = totalCessAmount;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public Boolean geteWayBill() {
        return eWayBill;
    }

    public void seteWayBill(Boolean eWayBill) {
        this.eWayBill = eWayBill;
    }

    public Date geteWayBillDate() {
        return eWayBillDate;
    }

    public void seteWayBillDate(Date eWayBillDate) {
        this.eWayBillDate = eWayBillDate;
    }

    public String geteWayBillVechileNo() {
        return eWayBillVechileNo;
    }

    public void seteWayBillVechileNo(String eWayBillVechileNo) {
        this.eWayBillVechileNo = eWayBillVechileNo;
    }

    public String geteWayBillVechileName() {
        return eWayBillVechileName;
    }

    public void seteWayBillVechileName(String eWayBillVechileName) {
        this.eWayBillVechileName = eWayBillVechileName;
    }

    public String geteWayBillGeneratedBy() {
        return eWayBillGeneratedBy;
    }

    public void seteWayBillGeneratedBy(String eWayBillGeneratedBy) {
        this.eWayBillGeneratedBy = eWayBillGeneratedBy;
    }

    public Date geteWayBillGeneratedAtTimeStamp() {
        return eWayBillGeneratedAtTimeStamp;
    }

    public void seteWayBillGeneratedAtTimeStamp(Date eWayBillGeneratedAtTimeStamp) {
        this.eWayBillGeneratedAtTimeStamp = eWayBillGeneratedAtTimeStamp;
    }

    public String geteWayBillTransportDcoument() {
        return eWayBillTransportDcoument;
    }

    public void seteWayBillTransportDcoument(String eWayBillTransportDcoument) {
        this.eWayBillTransportDcoument = eWayBillTransportDcoument;
    }

    public Date geteWayBillTransportDate() {
        return eWayBillTransportDate;
    }

    public void seteWayBillTransportDate(Date eWayBillTransportDate) {
        this.eWayBillTransportDate = eWayBillTransportDate;
    }

    public String geteWayBillReasonForTransportation() {
        return eWayBillReasonForTransportation;
    }

    public void seteWayBillReasonForTransportation(String eWayBillReasonForTransportation) {
        this.eWayBillReasonForTransportation = eWayBillReasonForTransportation;
    }

}
