/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findPIByDate",
                    query = "from PurchaseInvoice p where p.purchaseInvoiceDate between :fromDate and :toDate and p.company=:company"
            )
            ,
             @NamedQuery(
                    name = "findPIByDateAndParty",
                    query = "from PurchaseInvoice p where p.purchaseInvoiceDate between :fromDate and :toDate and p.company=:company and p.ledger=:ledger"
            )
            ,
              @NamedQuery(
                    name = "findALLPIByDate",
                    query = "from PurchaseInvoice p where p.purchaseInvoiceDate between :fromDate and :toDate and p.company=:company and unregistered=:isRegistered"
            )
            ,
              
              @NamedQuery(
                    name = "findAllPIByParty",
                    query = "from PurchaseInvoice p where p.company=:company and p.ledger=:ledger"
            )
            ,@NamedQuery(
                    name = "findAllPI",
                    query = "from PurchaseInvoice p where p.company=:company"
            )})
public class PurchaseInvoice {

    @Id
    @Column(name = "purchase_invoice_id", updatable = false, nullable = false)
    public String purchaseInvoiceId;

    @Column(name = "purchase_invoice_no")
    public String purchaseInvoiceNo;

    @Column(name = "purchase_invoice_type")
    public String purchaseInvoiceType;

    @Column(name = "supplier_invoice_no")
    public String supplierInvoiceNo;

    @Temporal(TemporalType.DATE)
    @Column(name = "purchase_invoice_date")
    public Date purchaseInvoiceDate;

    @Column(name = "purchase_invoice_amount")
    public BigDecimal puchaseInvoiceAmount;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_amount")
    private BigDecimal discountAmount;

    @Column(name = "calculated_purchase_inovoice_amount")
    private BigDecimal calculatedPurchaseInvoiceAmount;

    @Column(name = "total_amount_paid")
    private BigDecimal totalAmountPaid;

    @Column(name = "eway_bill_no")
    private String eWayBillNo;

    @Column(name = "eway_bill_remarks")
    private String eWayBillRemarks;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "is_igst")
    public boolean isIGST;

    @Column(name = "igst_value")
    private BigDecimal igstValue;

    @Column(name = "cgst_value")
    private BigDecimal cgstValue;

    @Column(name = "sgst_value")
    private BigDecimal sgstValue;

    @Column(name = "round_off_value")
    private BigDecimal cessValue;

    @Column(name = "reverse_charge_applicable")
    private boolean reverseChargeApplicable;

    @Column(name = "eligible_for_itc")
    private boolean eligibleForITC;

    @Column(name = "itc_igst_value")
    private BigDecimal itcValueforIGST;

    @Column(name = "itc_cgst_value")
    private BigDecimal itcValueforCGST;

    @Column(name = "itc_sgst_value")
    private BigDecimal itcValueforSGST;

    @Column(name = "itc_cess_value")
    private BigDecimal itcValueforCess;

    @Column(name = "unregistered")
    private Boolean unregistered;

    @Column(name = "unregistered_name")
    private String unregisteredName;

    @Column(name = "unregistered_line_one")
    private String unregisteredLineOne;

    @Column(name = "unregistered_line_Two")
    private String unregisteredLineTwo;

    @Column(name = "unregistered_line_Three")
    private String unregisteredLineThree;

    @Column(name = "unregistered_city")
    private String unregisteredCity;

    @Column(name = "unregistered_district")
    private String unregisteredDistrict;

    @Column(name = "unregistered_state")
    private String unregisteredState;

    @Column(name = "unregistered_id")
    private String unregisteredId;

    @Column(name = "unregistered_mobile")
    private String unregisteredMobile;

    @Column(name = "unregistered_phone")
    private String unregisteredPhone;

    @Column(name = "unregistered_email")
    private String unregisteredEmail;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseInvoice", cascade = CascadeType.ALL)
    private List<PurchaseInvoiceLineItem> piLineItems = new ArrayList<>();

    @Column(name = "cancelled")
    private boolean cancelled;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger ledger;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToOne
    @JoinColumn(name = "journal_id")
    private Journal purchaseJournal;

    @Embedded
    private Audit auditData;

    public String getPurchaseInvoiceId() {
        return purchaseInvoiceId;
    }

    public void setPurchaseInvoiceId(String purchaseInvoiceId) {
        this.purchaseInvoiceId = purchaseInvoiceId;
    }

    public String getPurchaseInvoiceNo() {
        return purchaseInvoiceNo;
    }

    public void setPurchaseInvoiceNo(String purchaseInvoiceNo) {
        this.purchaseInvoiceNo = purchaseInvoiceNo;
    }

    public String getPurchaseInvoiceType() {
        return purchaseInvoiceType;
    }

    public void setPurchaseInvoiceType(String purchaseInvoiceType) {
        this.purchaseInvoiceType = purchaseInvoiceType;
    }

    public String getSupplierInvoiceNo() {
        return supplierInvoiceNo;
    }

    public void setSupplierInvoiceNo(String supplierInvoiceNo) {
        this.supplierInvoiceNo = supplierInvoiceNo;
    }

    public Date getPurchaseInvoiceDate() {
        return purchaseInvoiceDate;
    }

    public void setPurchaseInvoiceDate(Date purchaseInvoiceDate) {
        this.purchaseInvoiceDate = purchaseInvoiceDate;
    }

    public BigDecimal getPuchaseInvoiceAmount() {
        return puchaseInvoiceAmount;
    }

    public void setPuchaseInvoiceAmount(BigDecimal puchaseInvoiceAmount) {
        this.puchaseInvoiceAmount = puchaseInvoiceAmount;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getCalculatedPurchaseInvoiceAmount() {
        return calculatedPurchaseInvoiceAmount;
    }

    public void setCalculatedPurchaseInvoiceAmount(BigDecimal calculatedPurchaseInvoiceAmount) {
        this.calculatedPurchaseInvoiceAmount = calculatedPurchaseInvoiceAmount;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public String geteWayBillRemarks() {
        return eWayBillRemarks;
    }

    public void seteWayBillRemarks(String eWayBillRemarks) {
        this.eWayBillRemarks = eWayBillRemarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isIsIGST() {
        return isIGST;
    }

    public void setIsIGST(boolean isIGST) {
        this.isIGST = isIGST;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getCessValue() {
        return cessValue;
    }

    public void setCessValue(BigDecimal cessValue) {
        this.cessValue = cessValue;
    }

    public boolean isReverseChargeApplicable() {
        return reverseChargeApplicable;
    }

    public void setReverseChargeApplicable(boolean reverseChargeApplicable) {
        this.reverseChargeApplicable = reverseChargeApplicable;
    }

    public boolean isEligibleForITC() {
        return eligibleForITC;
    }

    public void setEligibleForITC(boolean eligibleForITC) {
        this.eligibleForITC = eligibleForITC;
    }

    public BigDecimal getItcValueforIGST() {
        return itcValueforIGST;
    }

    public void setItcValueforIGST(BigDecimal itcValueforIGST) {
        this.itcValueforIGST = itcValueforIGST;
    }

    public BigDecimal getItcValueforCGST() {
        return itcValueforCGST;
    }

    public void setItcValueforCGST(BigDecimal itcValueforCGST) {
        this.itcValueforCGST = itcValueforCGST;
    }

    public BigDecimal getItcValueforSGST() {
        return itcValueforSGST;
    }

    public void setItcValueforSGST(BigDecimal itcValueforSGST) {
        this.itcValueforSGST = itcValueforSGST;
    }

    public BigDecimal getItcValueforCess() {
        return itcValueforCess;
    }

    public void setItcValueforCess(BigDecimal itcValueforCess) {
        this.itcValueforCess = itcValueforCess;
    }

    public Boolean getUnregistered() {
        return unregistered;
    }

    public void setUnregistered(Boolean unregistered) {
        this.unregistered = unregistered;
    }

    public String getUnregisteredName() {
        return unregisteredName;
    }

    public void setUnregisteredName(String unregisteredName) {
        this.unregisteredName = unregisteredName;
    }

    public String getUnregisteredLineOne() {
        return unregisteredLineOne;
    }

    public void setUnregisteredLineOne(String unregisteredLineOne) {
        this.unregisteredLineOne = unregisteredLineOne;
    }

    public String getUnregisteredLineTwo() {
        return unregisteredLineTwo;
    }

    public void setUnregisteredLineTwo(String unregisteredLineTwo) {
        this.unregisteredLineTwo = unregisteredLineTwo;
    }

    public String getUnregisteredLineThree() {
        return unregisteredLineThree;
    }

    public void setUnregisteredLineThree(String unregisteredLineThree) {
        this.unregisteredLineThree = unregisteredLineThree;
    }

    public String getUnregisteredCity() {
        return unregisteredCity;
    }

    public void setUnregisteredCity(String unregisteredCity) {
        this.unregisteredCity = unregisteredCity;
    }

    public String getUnregisteredDistrict() {
        return unregisteredDistrict;
    }

    public void setUnregisteredDistrict(String unregisteredDistrict) {
        this.unregisteredDistrict = unregisteredDistrict;
    }

    public String getUnregisteredState() {
        return unregisteredState;
    }

    public void setUnregisteredState(String unregisteredState) {
        this.unregisteredState = unregisteredState;
    }

    public String getUnregisteredId() {
        return unregisteredId;
    }

    public void setUnregisteredId(String unregisteredId) {
        this.unregisteredId = unregisteredId;
    }

    public String getUnregisteredMobile() {
        return unregisteredMobile;
    }

    public void setUnregisteredMobile(String unregisteredMobile) {
        this.unregisteredMobile = unregisteredMobile;
    }

    public String getUnregisteredPhone() {
        return unregisteredPhone;
    }

    public void setUnregisteredPhone(String unregisteredPhone) {
        this.unregisteredPhone = unregisteredPhone;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public String getUnregisteredEmail() {
        return unregisteredEmail;
    }

    public void setUnregisteredEmail(String unregisteredEmail) {
        this.unregisteredEmail = unregisteredEmail;
    }

    public List<PurchaseInvoiceLineItem> getPiLineItems() {
        return piLineItems;
    }

    public void setPiLineItems(List<PurchaseInvoiceLineItem> piLineItems) {
        this.piLineItems = piLineItems;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Journal getPurchaseJournal() {
        return purchaseJournal;
    }

    public void setPurchaseJournal(Journal purchaseJournal) {
        this.purchaseJournal = purchaseJournal;
    }

}
