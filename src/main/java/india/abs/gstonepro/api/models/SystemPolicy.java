/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.Date;

/**
 *
 * @author SGS
 */
public class SystemPolicy {

    private boolean inventoryEnabled;

    private boolean accountsEnabled;

    private boolean platinum;

    private String packageName;

    private String releaseDate;

    private String serialNumber;

    private String version;

    private String database;

    private String masterUsername;

    private String masterPassword;

    private String imageURL;

    private Boolean trialPackage;

    private Boolean billOfSupply;

    private boolean balanceSheet;

    public boolean isInventoryEnabled() {
        return inventoryEnabled;
    }

    public void setInventoryEnabled(boolean inventoryEnabled) {
        this.inventoryEnabled = inventoryEnabled;
    }

    public boolean isAccountsEnabled() {
        return accountsEnabled;
    }

    public void setAccountsEnabled(boolean accountsEnabled) {
        this.accountsEnabled = accountsEnabled;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getMasterUsername() {
        return masterUsername;
    }

    public void setMasterUsername(String masterUsername) {
        this.masterUsername = masterUsername;
    }

    public String getMasterPassword() {
        return masterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Boolean isTrialPackage() {
        return trialPackage;
    }

    public void setTrialPackage(Boolean trialPackage) {
        this.trialPackage = trialPackage;
    }

    public Boolean isBillOfSupply() {
        return billOfSupply;
    }

    public void setBillOfSupply(Boolean billOfSupply) {
        this.billOfSupply = billOfSupply;
    }

    public boolean isPlatinum() {
        return platinum;
    }

    public void setPlatinum(boolean platinum) {
        this.platinum = platinum;
    }

    public boolean isBalanceSheet() {
        return balanceSheet;
    }

    public void setBalanceSheet(boolean balanceSheet) {
        this.balanceSheet = balanceSheet;
    }
}
