/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author SGS
 */
@Entity

public class CompanyPolicy {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false, nullable = false)
    private Long companyPolicyId;

    @OneToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "database")
    private String database;

    @Column(name = "under_stock_possible")
    private boolean underStockPossible;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date accountsExpiryDate;

    @Column(name = "sale_bill_count")
    private Long saleBillCount;

    @Column(name = "profit_enabled")
    private boolean profitEnabled;

    private boolean saleEditApproval;

    private boolean ledgerTransactionApproval;

//    private boolean amountTendered;
    @Column(name = "numeber_of_decimal_places")
    private int numeberOfDecimalPlaces;

    @Column(name = "print_paper_size")
    private String printPaperSize;

    private String footerHeading;

    private String footerLineOne;

    private String footerLineTwo;

    private String footerLineThree;

    private String footerLineFour;

    private String a4PAddressLineOne;

    private String a4PAddressLineTwo;

    private String a4PAddressLineThree;

    private String a5LAddressLineOne;

    private String a5LAddressLineTwo;

    private String a5LAddressLineThree;

    private String a5PAddressLineOne;

    private String a5PAddressLineTwo;

    private String a5PAddressLineThree;

    private String a7AddressLineOne;

    private String a7AddressLineTwo;

    private String a7AddressLineThree;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date financialYearStart;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date financialYearEnd;

    private int numberOfInvoiceCopies;

    private String firstInvoiceWord;

    private String secondInvoiceWord;

    private String thirdInvoiceWord;

    private String fourthInvoiceWord;

    private String fifthInvoiceWord;

    private String taxInvoiceWord;

    private String billOfSupplyWord;

    private String estimateBillWord;

    private Long taxInvoiceSequenceStart;

    private String taxInvoicePrefix;

    private String taxInvoiceSuffix;

    private Long purchaseSequenceStart;

    private String purchasePrefix;

    private String purchaseSuffix;

    private Long crDrSequenceStart;

    private String crDrPrefix;

    private String crDrSuffix;

    private Long estimateSequenceStart;

    private String estimatePrefix;

    private String estimateSuffix;

    private Long bosSequenceStart;

    private String bosPrefix;

    private String bosSuffix;

    public Long getCompanyPolicyId() {
        return companyPolicyId;
    }

    public void setCompanyPolicyId(Long companyPolicyId) {
        this.companyPolicyId = companyPolicyId;
    }

    public int getNumeberOfDecimalPlaces() {
        return numeberOfDecimalPlaces;
    }

    public void setNumeberOfDecimalPlaces(int numeberOfDecimalPlaces) {
        this.numeberOfDecimalPlaces = numeberOfDecimalPlaces;
    }

    public boolean isUnderStockPossible() {
        return underStockPossible;
    }

    public void setUnderStockPossible(boolean underStockPossible) {
        this.underStockPossible = underStockPossible;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getAccountsExpiryDate() {
        return accountsExpiryDate;
    }

    public void setAccountsExpiryDate(Date accountsExpiryDate) {
        this.accountsExpiryDate = accountsExpiryDate;
    }

    public Long getSaleBillCount() {
        return saleBillCount;
    }

    public void setSaleBillCount(Long saleBillCount) {
        this.saleBillCount = saleBillCount;
    }

    public boolean isProfitEnabled() {
        return profitEnabled;
    }

    public void setProfitEnabled(boolean profitEnabled) {
        this.profitEnabled = profitEnabled;
    }

    public Date getFinancialYearStart() {
        return financialYearStart;
    }

    public void setFinancialYearStart(Date financialYearStart) {
        this.financialYearStart = financialYearStart;
    }

    public Date getFinancialYearEnd() {
        return financialYearEnd;
    }

    public void setFinancialYearEnd(Date financialYearEnd) {
        this.financialYearEnd = financialYearEnd;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getPrintPaperSize() {
        return printPaperSize;
    }

    public void setPrintPaperSize(String printPaperSize) {
        this.printPaperSize = printPaperSize;
    }

    public int getNumberOfInvoiceCopies() {
        return numberOfInvoiceCopies;
    }

    public void setNumberOfInvoiceCopies(int numberOfInvoiceCopies) {
        this.numberOfInvoiceCopies = numberOfInvoiceCopies;
    }

    public String getFirstInvoiceWord() {
        return firstInvoiceWord;
    }

    public void setFirstInvoiceWord(String firstInvoiceWord) {
        this.firstInvoiceWord = firstInvoiceWord;
    }

    public String getSecondInvoiceWord() {
        return secondInvoiceWord;
    }

    public void setSecondInvoiceWord(String secondInvoiceWord) {
        this.secondInvoiceWord = secondInvoiceWord;
    }

    public String getThirdInvoiceWord() {
        return thirdInvoiceWord;
    }

    public void setThirdInvoiceWord(String thirdInvoiceWord) {
        this.thirdInvoiceWord = thirdInvoiceWord;
    }

    public String getFourthInvoiceWord() {
        return fourthInvoiceWord;
    }

    public void setFourthInvoiceWord(String fourthInvoiceWord) {
        this.fourthInvoiceWord = fourthInvoiceWord;
    }

    public String getFifthInvoiceWord() {
        return fifthInvoiceWord;
    }

    public void setFifthInvoiceWord(String fifthInvoiceWord) {
        this.fifthInvoiceWord = fifthInvoiceWord;
    }

    public String getTaxInvoiceWord() {
        return taxInvoiceWord;
    }

    public void setTaxInvoiceWord(String taxInvoiceWord) {
        this.taxInvoiceWord = taxInvoiceWord;
    }

    public String getBillOfSupplyWord() {
        return billOfSupplyWord;
    }

    public void setBillOfSupplyWord(String billOfSupplyWord) {
        this.billOfSupplyWord = billOfSupplyWord;
    }

    public String getEstimateBillWord() {
        return estimateBillWord;
    }

    public void setEstimateBillWord(String estimateBillWord) {
        this.estimateBillWord = estimateBillWord;
    }

    public String getFooterHeading() {
        return footerHeading;
    }

    public void setFooterHeading(String footerHeading) {
        this.footerHeading = footerHeading;
    }

    public String getFooterLineOne() {
        return footerLineOne;
    }

    public void setFooterLineOne(String footerLineOne) {
        this.footerLineOne = footerLineOne;
    }

    public String getFooterLineTwo() {
        return footerLineTwo;
    }

    public void setFooterLineTwo(String footerLineTwo) {
        this.footerLineTwo = footerLineTwo;
    }

    public String getFooterLineThree() {
        return footerLineThree;
    }

    public void setFooterLineThree(String footerLineThree) {
        this.footerLineThree = footerLineThree;
    }

    public String getFooterLineFour() {
        return footerLineFour;
    }

    public void setFooterLineFour(String footerLineFour) {
        this.footerLineFour = footerLineFour;
    }

    public String getA4PAddressLineOne() {
        return a4PAddressLineOne;
    }

    public void setA4PAddressLineOne(String a4PAddressLineOne) {
        this.a4PAddressLineOne = a4PAddressLineOne;
    }

    public String getA4PAddressLineTwo() {
        return a4PAddressLineTwo;
    }

    public void setA4PAddressLineTwo(String a4PAddressLineTwo) {
        this.a4PAddressLineTwo = a4PAddressLineTwo;
    }

    public String getA4PAddressLineThree() {
        return a4PAddressLineThree;
    }

    public void setA4PAddressLineThree(String a4PAddressLineThree) {
        this.a4PAddressLineThree = a4PAddressLineThree;
    }

    public String getA5LAddressLineOne() {
        return a5LAddressLineOne;
    }

    public void setA5LAddressLineOne(String a5LAddressLineOne) {
        this.a5LAddressLineOne = a5LAddressLineOne;
    }

    public String getA5LAddressLineTwo() {
        return a5LAddressLineTwo;
    }

    public void setA5LAddressLineTwo(String a5LAddressLineTwo) {
        this.a5LAddressLineTwo = a5LAddressLineTwo;
    }

    public String getA5LAddressLineThree() {
        return a5LAddressLineThree;
    }

    public void setA5LAddressLineThree(String a5LAddressLineThree) {
        this.a5LAddressLineThree = a5LAddressLineThree;
    }

    public String getA5PAddressLineOne() {
        return a5PAddressLineOne;
    }

    public void setA5PAddressLineOne(String a5PAddressLineOne) {
        this.a5PAddressLineOne = a5PAddressLineOne;
    }

    public String getA5PAddressLineTwo() {
        return a5PAddressLineTwo;
    }

    public void setA5PAddressLineTwo(String a5PAddressLineTwo) {
        this.a5PAddressLineTwo = a5PAddressLineTwo;
    }

    public String getA5PAddressLineThree() {
        return a5PAddressLineThree;
    }

    public void setA5PAddressLineThree(String a5PAddressLineThree) {
        this.a5PAddressLineThree = a5PAddressLineThree;
    }

    public String getA7AddressLineOne() {
        return a7AddressLineOne;
    }

    public void setA7AddressLineOne(String a7AddressLineOne) {
        this.a7AddressLineOne = a7AddressLineOne;
    }

    public String getA7AddressLineTwo() {
        return a7AddressLineTwo;
    }

    public void setA7AddressLineTwo(String a7AddressLineTwo) {
        this.a7AddressLineTwo = a7AddressLineTwo;
    }

    public String getA7AddressLineThree() {
        return a7AddressLineThree;
    }

    public void setA7AddressLineThree(String a7AddressLineThree) {
        this.a7AddressLineThree = a7AddressLineThree;
    }

    public boolean isSaleEditApproval() {
        return saleEditApproval;
    }

    public void setSaleEditApproval(boolean saleEditApproval) {
        this.saleEditApproval = saleEditApproval;
    }

    public boolean isLedgerTransactionApproval() {
        return ledgerTransactionApproval;
    }

    public void setLedgerTransactionApproval(boolean ledgerTransactionApproval) {
        this.ledgerTransactionApproval = ledgerTransactionApproval;
    }

    public Long getTaxInvoiceSequenceStart() {
        return taxInvoiceSequenceStart;
    }

    public void setTaxInvoiceSequenceStart(Long taxInvoiceSequenceStart) {
        this.taxInvoiceSequenceStart = taxInvoiceSequenceStart;
    }

    public String getTaxInvoicePrefix() {
        return taxInvoicePrefix;
    }

    public void setTaxInvoicePrefix(String taxInvoicePrefix) {
        this.taxInvoicePrefix = taxInvoicePrefix;
    }

    public String getTaxInvoiceSuffix() {
        return taxInvoiceSuffix;
    }

    public void setTaxInvoiceSuffix(String taxInvoiceSuffix) {
        this.taxInvoiceSuffix = taxInvoiceSuffix;
    }

    public Long getPurchaseSequenceStart() {
        return purchaseSequenceStart;
    }

    public void setPurchaseSequenceStart(Long purchaseSequenceStart) {
        this.purchaseSequenceStart = purchaseSequenceStart;
    }

    public String getPurchasePrefix() {
        return purchasePrefix;
    }

    public void setPurchasePrefix(String purchasePrefix) {
        this.purchasePrefix = purchasePrefix;
    }

    public String getPurchaseSuffix() {
        return purchaseSuffix;
    }

    public void setPurchaseSuffix(String purchaseSuffix) {
        this.purchaseSuffix = purchaseSuffix;
    }

    public Long getCrDrSequenceStart() {
        return crDrSequenceStart;
    }

    public void setCrDrSequenceStart(Long crDrSequenceStart) {
        this.crDrSequenceStart = crDrSequenceStart;
    }

    public String getCrDrPrefix() {
        return crDrPrefix;
    }

    public void setCrDrPrefix(String crDrPrefix) {
        this.crDrPrefix = crDrPrefix;
    }

    public String getCrDrSuffix() {
        return crDrSuffix;
    }

    public void setCrDrSuffix(String crDrSuffix) {
        this.crDrSuffix = crDrSuffix;
    }

    public Long getEstimateSequenceStart() {
        return estimateSequenceStart;
    }

    public void setEstimateSequenceStart(Long estimateSequenceStart) {
        this.estimateSequenceStart = estimateSequenceStart;
    }

    public String getEstimatePrefix() {
        return estimatePrefix;
    }

    public void setEstimatePrefix(String estimatePrefix) {
        this.estimatePrefix = estimatePrefix;
    }

    public String getEstimateSuffix() {
        return estimateSuffix;
    }

    public void setEstimateSuffix(String estimateSuffix) {
        this.estimateSuffix = estimateSuffix;
    }

    public Long getBosSequenceStart() {
        return bosSequenceStart;
    }

    public void setBosSequenceStart(Long bosSequenceStart) {
        this.bosSequenceStart = bosSequenceStart;
    }

    public String getBosPrefix() {
        return bosPrefix;
    }

    public void setBosPrefix(String bosPrefix) {
        this.bosPrefix = bosPrefix;
    }

    public String getBosSuffix() {
        return bosSuffix;
    }

    public void setBosSuffix(String bosSuffix) {
        this.bosSuffix = bosSuffix;
    }

    public boolean isAmountTendered() {
//        return amountTendered;
        return false;
    }

    public void setAmountTendered(boolean amountTendered) {
//        this.amountTendered = amountTendered;
    }

}
