/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findLedgerGroupByName",
                    query = "from LedgerGroup e where e.company = :company and e.ledgerGroupName IN (:ledgerGroupName,:ledgerGroupName2 )"
            )
            ,
                @NamedQuery(
                    name = "findLedgerGroupByNames",
                    query = "from LedgerGroup e where e.company = :company and e.ledgerGroupName IN (:ledgerGroupName,:ledgerGroupName2,:ledgerGroupName3)"
            )
            ,
             @NamedQuery(
                    name = "findLedgerGroupByCompany",
                    query = "from LedgerGroup e where e.company = :company"
            )
            ,
            @NamedQuery(
                    name = "findPurchaseLedgerByLedgerGroupName",
                    query = "from LedgerGroup e where e.company = :company and e.ledgerGroupName IN (:ledgerGroupName,:ledgerGroupName2 )"
            )
            ,
            @NamedQuery(
                    name = "findAgentLedgerGroupByName",
                    query = "from LedgerGroup e where e.company = :company and e.ledgerGroupName IN (:ledgerGroupName)"
            )
        }
)
public class LedgerGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ledger_group_id", updatable = false, nullable = false)
    private Long ledgerGroupId;

    @Column(name = "ledger_group_name")
    private String ledgerGroupName;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ledgerGroup")
    private Set<Ledger> ledgers = new HashSet<Ledger>();

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    private LedgerGroup parentLedgerGroup;

    @OneToMany(mappedBy = "parentLedgerGroup")
    private Set<LedgerGroup> childLedgerGroups = new HashSet<LedgerGroup>();

    @Column(name = "eligible_for_children")
    public boolean eligibleForChildren;

    @Column(name = "editable")
    public boolean editable;

    @Column(name = "deletable")
    public boolean deletable;

    @Column(name = "deleted")
    private boolean deleted;

    @Embedded
    private Audit auditData;

    public LedgerGroup(Company company, String ledgerGroupName) {
        this.company = company;
        this.ledgerGroupName = ledgerGroupName;
    }

    public LedgerGroup(String ledgerGroupName) {
        this.ledgerGroupName = ledgerGroupName;
    }

    public LedgerGroup() {
        //To change body of generated methods, choose Tools | Templates.
    }

    public Long getLedgerGroupId() {
        return ledgerGroupId;
    }

    public String getLedgerGroupName() {
        return ledgerGroupName;
    }

    public Set<Ledger> getLedgers() {
        return ledgers;
    }

    public void setLedgerGroupId(Long ledgerGroupId) {
        this.ledgerGroupId = ledgerGroupId;
    }

    public void setLedgerGroupName(String ledgerGroupName) {
        this.ledgerGroupName = ledgerGroupName;
    }

    public void setLedgers(Set<Ledger> ledgers) {
        this.ledgers = ledgers;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public LedgerGroup getParentLedgerGroup() {
        return parentLedgerGroup;
    }

    public void setParentLedgerGroup(LedgerGroup parentLedgerGroup) {
        this.parentLedgerGroup = parentLedgerGroup;
    }

    public Set<LedgerGroup> getChildLedgerGroups() {
        return childLedgerGroups;
    }

    public void setChildLedgerGroups(Set<LedgerGroup> childLedgerGroups) {
        this.childLedgerGroups = childLedgerGroups;
    }

    @Override
    public String toString() {
        return "LedgerGroup{" + "ledgerGroupId=" + ledgerGroupId + ", ledgerGroupName=" + ledgerGroupName + ", company=" + company + ", parentLedgerGroup=" + parentLedgerGroup + ", editable=" + editable + ", deletable=" + deletable + ", auditData=" + auditData + '}';
    }

    public void printLedgers() {
        for (Ledger l : this.ledgers) {

        }
    }

    public void printLedgerNames() {
        for (Ledger l : this.ledgers) {
            System.out.println(l.getLedgerName());
        }
    }

    public boolean isEligibleForChildren() {
        return eligibleForChildren;
    }

    public void setEligibleForChildren(boolean eligibleForChildren) {
        this.eligibleForChildren = eligibleForChildren;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
