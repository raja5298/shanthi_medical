/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
public class SaleEditLineItem {
    
    private int lineNumber;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "psli_id", updatable = false, nullable = false)
    private long psliId;

    @ManyToOne
    @JoinColumn(name = "ps_id")
    private PurchaseSale purchaseSale;

    @ManyToOne
    private Product product;
    
    @Column(name="product_name")
    private String productName;

    @OneToOne(mappedBy = "psli",cascade=CascadeType.ALL)
    private GoDownStockEntry godownStockEntry;
    
    @Column(name="uqc_one")
    private String uqcOne;
    
    @Column(name="uqc_one_quantity")
    private int uqcOneQuantity;
    
    @Column(name="uqc_one_rate")
    private BigDecimal uqcOneRate;
    
    @Column(name="uqc_one_value")
    private float uqcOneValue;
    
    @Column(name="uqc_two")
    private String uqcTwo;
    
    @Column(name="uqc_two_quantity")
    private int uqcTwoQuantity;
    
    @Column(name="uqc_two_rate")
    private BigDecimal uqcTwoRate;
    
    @Column(name="uqc_two_value")
    private float uqcTwoValue;
    
    @Column(name="stock_quantity")
    private int stockQuantity;
    
    @Column(name="hsn_sac")
    private String hsnSac;
    
    @Column(name="discount_percent")
    private float discountPercent;
    
    @Column(name="discount_value")
    private BigDecimal discountValue;
    
    @Column(name="value")
    private BigDecimal value;
    
    @Column(name="igst_percentage")
    private float igstPercentage;
    
    @Column(name="cgst_percentage")
    private float cgstPercentage;
    
    @Column(name="sgst_percentage")
    private float sgstPercentage;
    
    @Column(name="igst_value")
    private BigDecimal igstValue;
    
    @Column(name="cgst_value")
    private BigDecimal cgstValue;
    
    @Column(name="sgst_value")
    private BigDecimal sgstValue;
    
    @Column(name="uqc1_base_rate")
    private BigDecimal uqc1BaseRate;
    
    @Column(name="uqc2_base_rate")
    private BigDecimal uqc2BaseRate;
    
    @Column(name="profit")
    private BigDecimal profit;
    
    @Column(name="cess_amount")
    private BigDecimal cessAmount;
    
    
     @Column(name="cancelled")
    private boolean cancelled;
     
    @Column(name="service")
    private boolean service;
    
    @Column(name="line_description_one")
    private String lineDescriptionOne;
    
    @Column(name="line_description_two")
    private String lineDescriptionTwo;
    
    @Transient
    private GoDown godown;

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long getPsliId() {
        return psliId;
    }

    public void setPsliId(long psliId) {
        this.psliId = psliId;
    }

    public PurchaseSale getPurchaseSale() {
        return purchaseSale;
    }

    public void setPurchaseSale(PurchaseSale purchaseSale) {
        this.purchaseSale = purchaseSale;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public GoDownStockEntry getGodownStockEntry() {
        return godownStockEntry;
    }

    public void setGodownStockEntry(GoDownStockEntry godownStockEntry) {
        this.godownStockEntry = godownStockEntry;
    }

    public String getUqcOne() {
        return uqcOne;
    }

    public void setUqcOne(String uqcOne) {
        this.uqcOne = uqcOne;
    }

    public int getUqcOneQuantity() {
        return uqcOneQuantity;
    }

    public void setUqcOneQuantity(int uqcOneQuantity) {
        this.uqcOneQuantity = uqcOneQuantity;
    }

    public BigDecimal getUqcOneRate() {
        return uqcOneRate;
    }

    public void setUqcOneRate(BigDecimal uqcOneRate) {
        this.uqcOneRate = uqcOneRate;
    }

    public float getUqcOneValue() {
        return uqcOneValue;
    }

    public void setUqcOneValue(float uqcOneValue) {
        this.uqcOneValue = uqcOneValue;
    }

    public String getUqcTwo() {
        return uqcTwo;
    }

    public void setUqcTwo(String uqcTwo) {
        this.uqcTwo = uqcTwo;
    }

    public int getUqcTwoQuantity() {
        return uqcTwoQuantity;
    }

    public void setUqcTwoQuantity(int uqcTwoQuantity) {
        this.uqcTwoQuantity = uqcTwoQuantity;
    }

    public BigDecimal getUqcTwoRate() {
        return uqcTwoRate;
    }

    public void setUqcTwoRate(BigDecimal uqcTwoRate) {
        this.uqcTwoRate = uqcTwoRate;
    }

    public float getUqcTwoValue() {
        return uqcTwoValue;
    }

    public void setUqcTwoValue(float uqcTwoValue) {
        this.uqcTwoValue = uqcTwoValue;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getUqc1BaseRate() {
        return uqc1BaseRate;
    }

    public void setUqc1BaseRate(BigDecimal uqc1BaseRate) {
        this.uqc1BaseRate = uqc1BaseRate;
    }

    public BigDecimal getUqc2BaseRate() {
        return uqc2BaseRate;
    }

    public void setUqc2BaseRate(BigDecimal uqc2BaseRate) {
        this.uqc2BaseRate = uqc2BaseRate;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getCessAmount() {
        return cessAmount;
    }

    public void setCessAmount(BigDecimal cessAmount) {
        this.cessAmount = cessAmount;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public String getLineDescriptionOne() {
        return lineDescriptionOne;
    }

    public void setLineDescriptionOne(String lineDescriptionOne) {
        this.lineDescriptionOne = lineDescriptionOne;
    }

    public String getLineDescriptionTwo() {
        return lineDescriptionTwo;
    }

    public void setLineDescriptionTwo(String lineDescriptionTwo) {
        this.lineDescriptionTwo = lineDescriptionTwo;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }
    
    
    
}
