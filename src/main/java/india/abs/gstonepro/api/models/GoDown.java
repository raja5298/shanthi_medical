/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findAllGoDownsByCompany",
                    query = "from GoDown g where g.company=:company"
            ),
             @NamedQuery(
                    name = "findPrimaryGoDown",
                    query = "from GoDown g where g.company=:company and g.goDownName=:goDownName"
            )
        })
public class GoDown {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "godown_id", updatable = false, nullable = false)
    private Long goDownId;

    private String goDownName;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToMany(mappedBy = "godown")
    private Set<GoDownStockDetail> goDownStockDetails = new HashSet();

    @OneToMany(mappedBy = "godown")
    private Set<GoDownStockEntry> goDownStockEntries = new HashSet();
    
    private boolean editable;
    
    private boolean deletable;

    public Long getGoDownId() {
        return goDownId;
    }

    public void setGoDownId(Long goDownId) {
        this.goDownId = goDownId;
    }

    public String getGoDownName() {
        return goDownName;
    }

    public void setGoDownName(String goDownName) {
        this.goDownName = goDownName;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<GoDownStockDetail> getGoDownStockDetails() {
        return goDownStockDetails;
    }

    public void setGoDownStockDetails(Set<GoDownStockDetail> goDownStockDetails) {
        this.goDownStockDetails = goDownStockDetails;
    }

    public Set<GoDownStockEntry> getGoDownStockEntries() {
        return goDownStockEntries;
    }

    public void setGoDownStockEntries(Set<GoDownStockEntry> goDownStockEntries) {
        this.goDownStockEntries = goDownStockEntries;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }
    
    

}
