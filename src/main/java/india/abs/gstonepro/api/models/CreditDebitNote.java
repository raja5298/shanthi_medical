/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findCDNByDate",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteType = :type and cd.noteDate between :fromDate and :toDate "
            )
            ,
            @NamedQuery(
                    name = "findCDNByLedger",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteType = :type and cd.noteDate between :fromDate and :toDate and cd.ledger=:ledger"
            )
            ,
            @NamedQuery(
                    name = "findCDNByAuditData",
                    query = "from CreditDebitNote cd where  cd.company=:company and cd.noteType = :type and cd.auditData.createdOn between :fromDate and :toDate or cd.auditData.updatedOn between :fromDate and :toDate or cd.auditData.deletedOn between :fromDate and :toDate"
            )
            ,
            @NamedQuery(
                    name = "findAllCDNRByDate",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteDate between :fromDate and :toDate and isRegistered=:isRegistered "
            )
            ,@NamedQuery(
                    name = "findAllCDNRByDateAndType",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteDate between :fromDate and :toDate and cd.isRegistered=:isRegistered and cd.noteType = :noteType"
            )
            ,@NamedQuery(
                    name = "findAllCDNByType",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteType = :noteType and cd.ledger = :ledger"
            ),@NamedQuery(
                    name = "findAllCDNByTypeWithOutLedger",
                    query = "from CreditDebitNote cd where cd.company=:company and cd.noteType = :noteType "
            )
            
        }
)

public class CreditDebitNote {

    @Id
    @Column(name = "cd_note_id", updatable = false, nullable = false)
    private String cdNoteId;

    @Column(name = "cd_note_number")
    private String cdNoteNo;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger ledger;

    @Column(name = "note_type")
    private String noteType;

    @Column(name = "note_date")
    @Temporal(TemporalType.DATE)
    private Date noteDate;

    @Column(name = "note_value")
    private BigDecimal noteValue;

    @Column(name = "note_value_inclusive_of_tax")
    private BigDecimal noteValueInclusiveOfTax;

    @Column(name = "igst_value")
    private BigDecimal igstValue;

    @Column(name = "cgst_value")
    private BigDecimal cgstValue;

    @Column(name = "sgst_value")
    private BigDecimal sgstValue;

    @Column(name = "cess_value")
    private BigDecimal cessValue;

    @Column(name = "total_amount_paid")
    private BigDecimal totalAmountPaid;

    @Column(name = "isRegistered")
    private boolean isRegistered;

//    @Column(name = "cancelled")
//    private boolean cancelled;
    private String remarks;

    @OneToOne
    @JoinColumn(name = "journal_id")
    private Journal crDrJournal;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creditDebitNote", cascade = CascadeType.ALL)
    private List<CreditDebitNoteLineItem> cdLis = new ArrayList<>();

    @Embedded
    private Audit auditData;

    public String getCdNoteId() {
        return cdNoteId;
    }

    public void setCdNoteId(String cdNoteId) {
        this.cdNoteId = cdNoteId;
    }

    public String getCdNoteNo() {
        return cdNoteNo;
    }

    public void setCdNoteNo(String cdNoteNo) {
        this.cdNoteNo = cdNoteNo;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public Date getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public List<CreditDebitNoteLineItem> getCdLis() {
        return cdLis;
    }

    public void setCdLis(List<CreditDebitNoteLineItem> cdLis) {
        this.cdLis = cdLis;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public Journal getCrDrJournal() {
        return crDrJournal;
    }

    public void setCrDrJournal(Journal crDrJournal) {
        this.crDrJournal = crDrJournal;
    }

    public BigDecimal getNoteValue() {
        return noteValue;
    }

    public void setNoteValue(BigDecimal noteValue) {
        this.noteValue = noteValue;
    }

    public BigDecimal getNoteValueInclusiveOfTax() {
        return noteValueInclusiveOfTax;
    }

    public void setNoteValueInclusiveOfTax(BigDecimal noteValueInclusiveOfTax) {
        this.noteValueInclusiveOfTax = noteValueInclusiveOfTax;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getCessValue() {
        return cessValue;
    }

    public void setCessValue(BigDecimal cessValue) {
        this.cessValue = cessValue;
    }

    public boolean isIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

}
