/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findLedgerByName",
                    query = "from Ledger e where e.company = :company and e.ledgerName=:ledgerName"
            )
            ,
             @NamedQuery(
                    name = "findLedgerByLedgerGroupName",
                    query = "from Ledger e where e.company = :company and e.ledgerGroup.ledgerGroupName IN (:ledgerGroupName)"
            )
            ,
             @NamedQuery(
                    name = "findAgentLedgerByName",
                    query = "from Ledger e where e.company = :company and e.ledgerGroup.ledgerGroupName IN(:ledgerGroupName)")

        })
public class Ledger implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ledger_id", updatable = false, nullable = false)
    private Long ledgerId;

    @ManyToOne
    @JoinColumn(name = "ledger_group_id")
    private LedgerGroup ledgerGroup;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ledger")
    private Set<Transaction> transactions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ledger")
    private Set<PurchaseSale> purchaseOrsale;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ledger")
    private Set<PurchaseInvoice> purchaseInvoices;

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    private Ledger parentLedger;

    @OneToMany(mappedBy = "parentLedger", fetch = FetchType.EAGER)
    private Set<Ledger> childrenLedgers = new HashSet<Ledger>();

    @Column(name = "ledger_name")
    private String ledgerName;

    @Column(name = "address")
    private String address;

    @Column(name = "gstin")
    private String GSTIN;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;

    @Column(name = "state")
    private String state;

    @Column(name = "opening_balance")
    private BigDecimal openingBalance;

    @Column(name = "opening_balance_date")
    private Date openingBalanceDate;

    @Column(name = "is_goc")
    private boolean isGOC;

    @Column(name = "eligible_for_children")
    private boolean eligibleForChildren;

    @Column(name = "editable")
    private boolean editable;

    @Column(name = "deletable")
    private boolean deletable;

    @Column(name = "deleted")
    private boolean deleted;

    private boolean exemptFromApproval;

    private boolean transactionApprovalRequired;

    @Embedded
    private Audit auditData;

    @Transient
    private boolean visible;

    public Ledger(Company company, String ledgerName, String address, String GSTIN, String email, String phone, String mobile, String city, String district, String state, BigDecimal openingBalance, Date openingBalanceDate) {
        this.company = company;
        this.ledgerName = ledgerName;
        this.address = address;
        this.GSTIN = GSTIN;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.city = city;
        this.district = district;
        this.state = state;
        this.openingBalance = openingBalance;
        this.openingBalanceDate = openingBalanceDate;
    }

    public Ledger() {

    }

    public Ledger(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Long getLedgerId() {
        return ledgerId;
    }

    public LedgerGroup getLedgerGroup() {
        return ledgerGroup;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public String getAddress() {
        return address;
    }

    public String getGSTIN() {
        return GSTIN;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getMobile() {
        return mobile;
    }

    public String getCity() {
        return city;
    }

    public String getDistrict() {
        return district;
    }

    public String getState() {
        return state;
    }

    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public Date getOpeningBalanceDate() {
        return openingBalanceDate;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public void setLedgerGroup(LedgerGroup ledgerGroup) {
        this.ledgerGroup = ledgerGroup;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setGSTIN(String GSTIN) {
        this.GSTIN = GSTIN;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setState(String State) {
        this.state = State;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    public void setOpeningBalanceDate(Date openingBalanceDate) {
        this.openingBalanceDate = openingBalanceDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isIsGOC() {
        return isGOC;
    }

    public void setIsGOC(boolean isGOC) {
        this.isGOC = isGOC;
    }

    public Set<PurchaseSale> getPurchaseOrsale() {
        return purchaseOrsale;
    }

    public void setPurchaseOrsale(Set<PurchaseSale> purchaseOrsale) {
        this.purchaseOrsale = purchaseOrsale;
    }

    public Ledger getParentLedger() {
        return parentLedger;
    }

    public void setParentLedger(Ledger parentLedger) {
        this.parentLedger = parentLedger;
    }

    public Set<Ledger> getChildrenLedgers() {
        return childrenLedgers;
    }

    public void setChildrenLedgers(Set<Ledger> childrenLedgers) {
        this.childrenLedgers = childrenLedgers;
    }

    @Override
    public String toString() {
        return "Ledger{" + "ledgerId=" + ledgerId + ", ledgerGroup=" + ledgerGroup + ", ledgerName=" + ledgerName + ", address=" + address + ", GSTIN=" + GSTIN + ", email=" + email + ", phone=" + phone + ", mobile=" + mobile + ", city=" + city + ", district=" + district + ", State=" + state + ", openingBalance=" + openingBalance + ", openingBalanceDate=" + openingBalanceDate + '}';
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }

    public boolean isEligibleForChildren() {
        return eligibleForChildren;
    }

    public void setEligibleForChildren(boolean eligibleForChildren) {
        this.eligibleForChildren = eligibleForChildren;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public Set<PurchaseInvoice> getPurchaseInvoices() {
        return purchaseInvoices;
    }

    public void setPurchaseInvoices(Set<PurchaseInvoice> purchaseInvoices) {
        this.purchaseInvoices = purchaseInvoices;
    }

    public boolean isExemptFromApproval() {
        return exemptFromApproval;
    }

    public void setExemptFromApproval(boolean exemptFromApproval) {
        this.exemptFromApproval = exemptFromApproval;
    }

    public boolean isTransactionApprovalRequired() {
        return transactionApprovalRequired;
    }

    public void setTransactionApprovalRequired(boolean transactionApprovalRequired) {
        this.transactionApprovalRequired = transactionApprovalRequired;
    }

}
