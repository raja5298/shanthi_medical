/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findPIByProduct",
                    query = "from PurchaseInvoiceLineItem p where p.purchaseInvoice.purchaseInvoiceDate between :fromDate and :toDate and p.purchaseInvoice.company=:company and p.product=:product"
            ),})
public class PurchaseInvoiceLineItem {

    private int lineNumber;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pili_id", updatable = false, nullable = false)
    private long piLiId;

    @ManyToOne
    @JoinColumn(name = "pi_id")
    private PurchaseInvoice purchaseInvoice;

    @ManyToOne
    private Product product;

    @Column(name = "product_name")
    private String productName;

    @OneToOne(mappedBy = "pili", cascade = CascadeType.ALL)
    private GoDownStockEntry godownStockEntry;

    @Column(name = "uqc_one")
    private String uqcOne;

    @Column(name = "uqc_one_quantity")
    private int uqcOneQuantity;

    @Column(name = "uqc_one_rate")
    private BigDecimal uqcOneRate;

    @Column(name = "uqc_one_value")
    private float uqcOneValue;

    @Column(name = "uqc_two")
    private String uqcTwo;

    @Column(name = "uqc_two_quantity")
    private int uqcTwoQuantity;

    @Column(name = "uqc_two_rate")
    private BigDecimal uqcTwoRate;

    @Column(name = "uqc_two_value")
    private float uqcTwoValue;

    @Column(name = "stock_quantity")
    private int stockQuantity;

    @Column(name = "hsn_sac")
    private String hsnSac;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_value")
    private BigDecimal discountValue;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "igst_percentage")
    private float igstPercentage;

    @Column(name = "cgst_percentage")
    private float cgstPercentage;

    @Column(name = "sgst_percentage")
    private float sgstPercentage;

    @Column(name = "igst_value")
    private BigDecimal igstValue;

    @Column(name = "cgst_value")
    private BigDecimal cgstValue;

    @Column(name = "sgst_value")
    private BigDecimal sgstValue;

    @Column(name = "uqc1_base_rate")
    private BigDecimal uqc1BaseRate;

    @Column(name = "uqc2_base_rate")
    private BigDecimal uqc2BaseRate;

    @Column(name = "cess_amount")
    private BigDecimal cessAmount;

    @Column(name = "eligible_for_itc")
    private boolean eligibleForITC;

    @Column(name = "itc_igst_value")
    private BigDecimal itcValueforIGST;

    @Column(name = "itc_cgst_value")
    private BigDecimal itcValueforCGST;

    @Column(name = "itc_sgst_value")
    private BigDecimal itcValueforSGST;

    @Column(name = "itc_cess_value")
    private BigDecimal itcValueforCess;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Column(name = "service")
    private boolean service;

    @Column(name = "line_description_one")
    private String lineDescriptionOne;

    @Column(name = "line_description_two")
    private String lineDescriptionTwo;

    @Column(name = "ITC_Eligible")
    private String ITCEligible;

    @Column(name = "totalamountwithtax")
    private BigDecimal totalAmountWithTax;

    @Transient
    private GoDown godown;

    @Embedded
    private Audit auditData;

    private String descriptionOne;
    private String descriptionTwo;
    private String descriptionThree;
    private String descriptionFour;
    private String descriptionFive;
    private String descriptionSix;
    private String descriptionSeven;
    private String descriptionEight;
    private String descriptionNine;
    private String descriptionTen;

    private int descriptionCount;

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long getPiLiId() {
        return piLiId;
    }

    public void setPiLiId(long piLiId) {
        this.piLiId = piLiId;
    }

    public PurchaseInvoice getPurchaseInvoice() {
        return purchaseInvoice;
    }

    public void setPurchaseInvoice(PurchaseInvoice purchaseInvoice) {
        this.purchaseInvoice = purchaseInvoice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public GoDownStockEntry getGodownStockEntry() {
        return godownStockEntry;
    }

    public void setGodownStockEntry(GoDownStockEntry godownStockEntry) {
        this.godownStockEntry = godownStockEntry;
    }

    public String getUqcOne() {
        return uqcOne;
    }

    public void setUqcOne(String uqcOne) {
        this.uqcOne = uqcOne;
    }

    public int getUqcOneQuantity() {
        return uqcOneQuantity;
    }

    public void setUqcOneQuantity(int uqcOneQuantity) {
        this.uqcOneQuantity = uqcOneQuantity;
    }

    public BigDecimal getUqcOneRate() {
        return uqcOneRate;
    }

    public void setUqcOneRate(BigDecimal uqcOneRate) {
        this.uqcOneRate = uqcOneRate;
    }

    public float getUqcOneValue() {
        return uqcOneValue;
    }

    public void setUqcOneValue(float uqcOneValue) {
        this.uqcOneValue = uqcOneValue;
    }

    public String getUqcTwo() {
        return uqcTwo;
    }

    public void setUqcTwo(String uqcTwo) {
        this.uqcTwo = uqcTwo;
    }

    public int getUqcTwoQuantity() {
        return uqcTwoQuantity;
    }

    public void setUqcTwoQuantity(int uqcTwoQuantity) {
        this.uqcTwoQuantity = uqcTwoQuantity;
    }

    public BigDecimal getUqcTwoRate() {
        return uqcTwoRate;
    }

    public void setUqcTwoRate(BigDecimal uqcTwoRate) {
        this.uqcTwoRate = uqcTwoRate;
    }

    public float getUqcTwoValue() {
        return uqcTwoValue;
    }

    public void setUqcTwoValue(float uqcTwoValue) {
        this.uqcTwoValue = uqcTwoValue;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public BigDecimal getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(BigDecimal igstValue) {
        this.igstValue = igstValue;
    }

    public BigDecimal getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(BigDecimal cgstValue) {
        this.cgstValue = cgstValue;
    }

    public BigDecimal getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(BigDecimal sgstValue) {
        this.sgstValue = sgstValue;
    }

    public BigDecimal getUqc1BaseRate() {
        return uqc1BaseRate;
    }

    public void setUqc1BaseRate(BigDecimal uqc1BaseRate) {
        this.uqc1BaseRate = uqc1BaseRate;
    }

    public BigDecimal getUqc2BaseRate() {
        return uqc2BaseRate;
    }

    public void setUqc2BaseRate(BigDecimal uqc2BaseRate) {
        this.uqc2BaseRate = uqc2BaseRate;
    }

    public BigDecimal getCessAmount() {
        return cessAmount;
    }

    public void setCessAmount(BigDecimal cessAmount) {
        this.cessAmount = cessAmount;
    }

    public boolean isEligibleForITC() {
        return eligibleForITC;
    }

    public void setEligibleForITC(boolean eligibleForITC) {
        this.eligibleForITC = eligibleForITC;
    }

    public BigDecimal getItcValueforIGST() {
        return itcValueforIGST;
    }

    public void setItcValueforIGST(BigDecimal itcValueforIGST) {
        this.itcValueforIGST = itcValueforIGST;
    }

    public BigDecimal getItcValueforCGST() {
        return itcValueforCGST;
    }

    public void setItcValueforCGST(BigDecimal itcValueforCGST) {
        this.itcValueforCGST = itcValueforCGST;
    }

    public BigDecimal getItcValueforSGST() {
        return itcValueforSGST;
    }

    public void setItcValueforSGST(BigDecimal itcValueforSGST) {
        this.itcValueforSGST = itcValueforSGST;
    }

    public BigDecimal getItcValueforCess() {
        return itcValueforCess;
    }

    public void setItcValueforCess(BigDecimal itcValueforCess) {
        this.itcValueforCess = itcValueforCess;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public String getLineDescriptionOne() {
        return lineDescriptionOne;
    }

    public void setLineDescriptionOne(String lineDescriptionOne) {
        this.lineDescriptionOne = lineDescriptionOne;
    }

    public String getLineDescriptionTwo() {
        return lineDescriptionTwo;
    }

    public void setLineDescriptionTwo(String lineDescriptionTwo) {
        this.lineDescriptionTwo = lineDescriptionTwo;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public String getDescriptionOne() {
        return descriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        this.descriptionOne = descriptionOne;
    }

    public String getDescriptionTwo() {
        return descriptionTwo;
    }

    public void setDescriptionTwo(String descriptionTwo) {
        this.descriptionTwo = descriptionTwo;
    }

    public String getDescriptionThree() {
        return descriptionThree;
    }

    public void setDescriptionThree(String descriptionThree) {
        this.descriptionThree = descriptionThree;
    }

    public String getDescriptionFour() {
        return descriptionFour;
    }

    public void setDescriptionFour(String descriptionFour) {
        this.descriptionFour = descriptionFour;
    }

    public String getDescriptionFive() {
        return descriptionFive;
    }

    public void setDescriptionFive(String descriptionFive) {
        this.descriptionFive = descriptionFive;
    }

    public String getDescriptionSix() {
        return descriptionSix;
    }

    public void setDescriptionSix(String descriptionSix) {
        this.descriptionSix = descriptionSix;
    }

    public String getDescriptionSeven() {
        return descriptionSeven;
    }

    public void setDescriptionSeven(String descriptionSeven) {
        this.descriptionSeven = descriptionSeven;
    }

    public String getDescriptionEight() {
        return descriptionEight;
    }

    public void setDescriptionEight(String descriptionEight) {
        this.descriptionEight = descriptionEight;
    }

    public String getDescriptionNine() {
        return descriptionNine;
    }

    public void setDescriptionNine(String descriptionNine) {
        this.descriptionNine = descriptionNine;
    }

    public String getDescriptionTen() {
        return descriptionTen;
    }

    public void setDescriptionTen(String descriptionTen) {
        this.descriptionTen = descriptionTen;
    }

    public int getDescriptionCount() {
        return descriptionCount;
    }

    public void setDescriptionCount(int descriptionCount) {
        this.descriptionCount = descriptionCount;
    }

    public String getITCEligible() {
        return ITCEligible;
    }

    public void setITCEligible(String ITCEligible) {
        this.ITCEligible = ITCEligible;
    }

    public BigDecimal getTotalAmountWithTax() {
        return totalAmountWithTax;
    }

    public void setTotalAmountWithTax(BigDecimal totalAmountWithTax) {
        this.totalAmountWithTax = totalAmountWithTax;
    }

}
