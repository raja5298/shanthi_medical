/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author SGS
 */
@Entity
public class SingleProductPolicy implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "individual_product_policy_id", updatable = false, nullable = false)
    private Long individualProductPolicyId;

    @ManyToOne
    @JoinColumn(name = "pricing_policy_id")
    private PricingPolicy productPolicy;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    
    @Column(name="uqc1_rate")
    private BigDecimal uqc1Rate;
    
     @Column(name="uqc2_rate")
    private BigDecimal uqc2Rate;
     
     @Column(name="inclusive_of_gst")
    private boolean inclusiveOfGST;
     
    @Column(name="editable")
    private boolean editable;
    
    @Column(name="deletable")
    private boolean deletable;
    
    @Column(name="deleted")
    private boolean deleted;

    @Embedded
    private Audit auditData;

    public SingleProductPolicy(PricingPolicy productPolicy,Product product, BigDecimal uqc1Rate, BigDecimal uqc2Rate, boolean inclusiveOfGST, boolean editable, boolean deletable) {
        this.product = product;
        this.uqc1Rate = uqc1Rate;
        this.uqc2Rate = uqc2Rate;
        this.inclusiveOfGST = inclusiveOfGST;
        this.editable = editable;
        this.deletable = deletable;
        this.productPolicy=productPolicy;
    }

    public SingleProductPolicy() {
       
    }

    

    public Long getIndividualProductPolicyId() {
        return individualProductPolicyId;
    }

    public void setIndividualProductPolicyId(Long individualProductPolicyId) {
        this.individualProductPolicyId = individualProductPolicyId;
    }

    public PricingPolicy getProductPolicy() {
        return productPolicy;
    }

    public void setProductPolicy(PricingPolicy productPolicy) {
        this.productPolicy = productPolicy;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getUqc1Rate() {
        return uqc1Rate;
    }

    public void setUqc1Rate(BigDecimal uqc1Rate) {
        this.uqc1Rate = uqc1Rate;
    }

    public BigDecimal getUqc2Rate() {
        return uqc2Rate;
    }

    public void setUqc2Rate(BigDecimal uqc2Rate) {
        this.uqc2Rate = uqc2Rate;
    }

    public boolean isInclusiveOfGST() {
        return inclusiveOfGST;
    }

    public void setInclusiveOfGST(boolean inclusiveOfGST) {
        this.inclusiveOfGST = inclusiveOfGST;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }
    
    
      public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }
    

}
