/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Raja Abinesh
 */

@Entity
public class BillCharge {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bill_charge_id", updatable = false, nullable = false)
    private Long billChargeId;

    @Column(name = "name")
    private String name;
    
    @Column(name = "tax_applicable")
    private Boolean taxApplicable;
    
    @Column(name = "inclusive_of_tax")
    private Boolean inclusiveOfTax;
    
    @Column(name = "igst_percentage")
    private double igstPercentage;

    @Column(name = "cgst_percentage")
    private double cgstPercentage;

    @Column(name = "sgst_percentage")
    private double sgstPercentage;

    public BillCharge() {
    }
    
    public BillCharge(Long billChargeId, String name, Boolean taxApplicable, Boolean inclusiveOfTax, float igstPercentage, float cgstPercentage, float sgstPercentage) {
        this.billChargeId = billChargeId;
        this.name = name;
        this.taxApplicable = taxApplicable;
        this.inclusiveOfTax = inclusiveOfTax;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
    }

    public BillCharge(String name, Boolean taxApplicable, Boolean inclusiveOfTax, float igstPercentage, float cgstPercentage, float sgstPercentage) {
        this.name = name;
        this.taxApplicable = taxApplicable;
        this.inclusiveOfTax = inclusiveOfTax;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
    }

    public Long getBillChargeId() {
        return billChargeId;
    }

    public void setBillChargeId(Long billChargeId) {
        this.billChargeId = billChargeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isTaxApplicable() {
        return taxApplicable;
    }

    public void setTaxApplicable(Boolean taxApplicable) {
        this.taxApplicable = taxApplicable;
    }

    public Boolean isInclusiveOfTax() {
        return inclusiveOfTax;
    }

    public void setInclusiveOfTax(Boolean inclusiveOfTax) {
        this.inclusiveOfTax = inclusiveOfTax;
    }

    public double getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(double igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public double getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(double cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public double getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }
    
    
}
