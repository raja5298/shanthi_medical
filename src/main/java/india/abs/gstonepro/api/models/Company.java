/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author SGS
 */
@Entity
@Table(name = "company",
        uniqueConstraints = @UniqueConstraint(columnNames = {"company_name"}))
public class Company implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "company_id", updatable = false, nullable = false)
    private Long companyId;

    @Column(name = "company_name")
    private String companyName;

    private String companyDisplayName;

    @Column(name = "company_type")
    private String companyType;

    @Column(name = "address")
    private String address;

    @Column(name = "gstin")
    private String GSTIN;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;

    @Column(name = "state")
    private String state;

    @Column(name = "sale_count")
    private Long saleCount;

    @Column(name = "purchase_count")
    private Long purchaseCount;

    @Column(name = "sr_count")
    private Long srCount;

    @Column(name = "pr_count")
    private Long prCount;

    @Column(name = "bos_count")
    private Long bosCount;

    @Column(name = "estimate_count")
    private Long estimateCount;

    @OneToOne(mappedBy = "company", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CompanyPolicy companyPolicy;

    @OneToOne(mappedBy = "company", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CompanyBook companyBook;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<Product> products = new ArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<LedgerGroup> ledgerGroups = new ArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<Ledger> ledgers = new ArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<PurchaseSale> purchaseSales = new ArrayList();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<CreditDebitNote> creditDebitNote = new ArrayList();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
     private List<PurchaseInvoice> purchaseInvoices;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<BillOfSupply> billOfSupplies = new ArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private Set<ProductStockEntry> productStocks = new HashSet();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<PricingPolicy> pricingPolicies = new ArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<Transaction> transactions = new ArrayList();
    
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<ProductGroup> productGroups = new ArrayList();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private List<GoDown> godowns = new ArrayList();
    
    
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
//    private List<TradingAccount> tas = new ArrayList();

    @Column(name = "deleted")
    private boolean deleted;

    @Embedded
    private Audit auditData;

    public Company() {

    }

    public Company(String companyName, String companyType, String address, String city, String district, String state, String email, String phone, String mobile, String GSTIN, Long saleCount, Long purchaseCount, Long srCount, Long prCount) {
        this.companyName = companyName;
        this.companyType = companyType;
        this.address = address;
        this.city = city;
        this.district = district;
        this.state = state;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.GSTIN = GSTIN;
        this.saleCount = 0L;
        this.purchaseCount = 0L;
        this.srCount = 0L;
        this.prCount = 0L;
        this.bosCount = 0L;
        this.estimateCount = 0L;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getGSTIN() {
        return GSTIN;
    }

    public String getDistrict() {
        return district;
    }

    public void setGSTIN(String GSTIN) {
        this.GSTIN = GSTIN;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<LedgerGroup> getLedgerGroups() {
        return ledgerGroups;
    }

    public void setLedgerGroups(List<LedgerGroup> ledgerGroups) {
        this.ledgerGroups = ledgerGroups;
    }

    public List<Ledger> getLedgers() {
        return ledgers;
    }

    public void setLedgers(List<Ledger> ledgers) {
        this.ledgers = ledgers;
    }

    public Long getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Long saleCount) {
        this.saleCount = saleCount;
    }

    public List<PurchaseSale> getPurchaseSales() {
        return purchaseSales;
    }

    public void setPurchaseSales(List<PurchaseSale> purchaseSales) {
        this.purchaseSales = purchaseSales;
    }

    public Long getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Long purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Long getSrCount() {
        return srCount;
    }

    public void setSrCount(Long srCount) {
        this.srCount = srCount;
    }

    public Long getPrCount() {
        return prCount;
    }

    public void setPrCount(Long prCount) {
        this.prCount = prCount;
    }

    public CompanyPolicy getCompanyPolicy() {
        return companyPolicy;
    }

    public void setCompanyPolicy(CompanyPolicy companyPolicy) {
        this.companyPolicy = companyPolicy;
    }

    public List<PricingPolicy> getPricingPolicies() {
        return pricingPolicies;
    }

    public void setPricingPolicies(List<PricingPolicy> pricingPolicies) {
        this.pricingPolicies = pricingPolicies;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Set<ProductStockEntry> getProductStocks() {
        return productStocks;
    }

    public void setProductStocks(Set<ProductStockEntry> productStocks) {
        this.productStocks = productStocks;
    }

    @Override
    public String toString() {
        return "company{" + "companyID=" + companyId + ", companyName=" + companyName + ", companyType=" + companyType + ", address=" + address + ", GSTIN=" + GSTIN + ", email=" + email + ", phone=" + phone + ", mobile=" + mobile + ", city=" + city + ", district=" + district + ", State=" + state + '}';
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public Long getBosCount() {
        return bosCount;
    }

    public void setBosCount(Long bosCount) {
        this.bosCount = bosCount;
    }

    public Long getEstimateCount() {
        return estimateCount;
    }

    public void setEstimateCount(Long estimateCount) {
        this.estimateCount = estimateCount;
    }

    public String getCompanyDisplayName() {
        return companyDisplayName;
    }

    public void setCompanyDisplayName(String companyDisplayName) {
        this.companyDisplayName = companyDisplayName;
    }

    public List<BillOfSupply> getBillOfSupplies() {
        return billOfSupplies;
    }

    public void setBillOfSupplies(List<BillOfSupply> billOfSupplies) {
        this.billOfSupplies = billOfSupplies;
    }

    public CompanyBook getCompanyBook() {
        return companyBook;
    }

    public void setCompanyBook(CompanyBook companyBook) {
        this.companyBook = companyBook;
    }

    public List<ProductGroup> getProductGroups() {
        return productGroups;
    }

    public void setProductGroups(List<ProductGroup> productGroups) {
        this.productGroups = productGroups;
    }

    public List<GoDown> getGodowns() {
        return godowns;
    }

    public void setGodowns(List<GoDown> godowns) {
        this.godowns = godowns;
    }

    public List<CreditDebitNote> getCreditDebitNote() {
        return creditDebitNote;
    }

    public void setCreditDebitNote(List<CreditDebitNote> creditDebitNote) {
        this.creditDebitNote = creditDebitNote;
    }

    public List<PurchaseInvoice> getPurchaseInvoices() {
        return purchaseInvoices;
    }

    public void setPurchaseInvoices(List<PurchaseInvoice> purchaseInvoices) {
        this.purchaseInvoices = purchaseInvoices;
    }

//    public List<TradingAccount> getTas() {
//        return tas;
//    }
//
//    public void setTas(List<TradingAccount> tas) {
//        this.tas = tas;
//    }
//    
    

}
