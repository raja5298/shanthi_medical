/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findEstimateWithoutCancellBill",
                    query = "from Estimate e where e.estimateDate between :fromDate and :toDate and e.company=:company and e.cancelled=:withCancelledBill"
            )
            ,
            @NamedQuery(
                    name = "findEstimate",
                    query = "from Estimate e where e.estimateDate between :fromDate and :toDate and e.company=:company"
            )
            ,
            @NamedQuery(
                    name = "findEstimateByParty",
                    query = "from Estimate e where e.ledger=:ledger and e.estimateDate between :fromDate and :toDate and e.company=:company and e.cancelled=:withCancelledBill"
            )
            ,
            @NamedQuery(
                    name = "findEstimateAuditData",
                    query = "from Estimate e where e.auditData.createdOn between :fromDate and :toDate"
            )
            ,
                 @NamedQuery(
                    name = "findEstimateAuditDataByUser",
                    query = "from Estimate e where e.auditData.createdOn between :fromDate and :toDate and auditData.createdBy = :user"
            )

        }
)
public class Estimate {

    @Id
    @Column(name = "estimate_id", updatable = false, nullable = false)
    private String estimateId;

    @Temporal(TemporalType.DATE)
    @Column(name = "estimate_date")
    private Date estimateDate;

    @Column(name = "estimate_no")
    private String estimateNo;

    @Column(name = "bill_amount")
    private BigDecimal billAmount;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger ledger;

    @Column(name = "mode_of_delivery")
    private String modeOfDelivery;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_amount")
    private BigDecimal discountAmount;

    //Difference between Bill Amount & Calculated Total Amount
    @Column(name = "variation_amount")
    private BigDecimal variationAmount;

    //Original Bill Amount
    @Column(name = "calculated_total_value")
    private BigDecimal calculatedTotalValue;

    //Cash or Credit
    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "total_amount_paid")
    private BigDecimal totalAmountPaid;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "round_off_value")
    private BigDecimal roundOffValue;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Column(name = "profit")
    private BigDecimal profit;

    @Column(name = "sms_message")
    private String smsMessage;

    @Column(name = "pricing_policy_name")
    private String pricingPolicyName;

    @Column(name = "no_party_name")
    private String noPartyName;

    @Column(name = "no_party_line_one")
    private String noPartyLineOne;

    @Column(name = "no_party_city")
    private String noPartyCity;

    @Column(name = "no_party_district")
    private String noPartyDistrict;

    @Column(name = "no_party_state")
    private String noPartyState;

    @Column(name = "no_party_gstin")
    private String noPartyGSTIN;

    @Column(name = "descriptionone")
    private String descriptionOne;

    @Column(name = "descriptiontwo")
    private String descriptionTwo;

    @Column(name = "descriptionthree")
    private String descriptionThree;

    @Column(name = "descriptionfour")
    private String descriptionFour;

    @Column(name = "descriptionfive")
    private String descriptionFive;

    @Column(name = "descriptionsix")
    private String descriptionSix;

    @Column(name = "descriptionseven")
    private String descriptionSeven;

    @Column(name = "descriptioneight")
    private String descriptionEight;

    @Column(name = "descriptionnine")
    private String descriptionNine;

    @Column(name = "descriptionoten")
    private String descriptionTen;

    @Column(name = "descriptioncount")
    private int descriptioncount;

    @Column(name = "no_party")
    private Boolean noParty;

    @Transient
    @Column(name = "current_payment_amount")
    private BigDecimal currentPaymentAmount;

    @OneToOne
    @JoinColumn(name = "journal_id")
    private Journal estimateJournal;

    @Embedded
    private Audit auditData;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "estimate", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<EstimateLineItem> estimateLineItem = new ArrayList<>();

    public String getEstimateId() {
        return estimateId;
    }

    public void setEstimateId(String estimateId) {
        this.estimateId = estimateId;
    }

    public Date getEstimateDate() {
        return estimateDate;
    }

    public void setEstimateDate(Date estimateDate) {
        this.estimateDate = estimateDate;
    }

    public String getEstimateNo() {
        return estimateNo;
    }

    public void setEstimateNo(String estimateNo) {
        this.estimateNo = estimateNo;
    }

    public List<EstimateLineItem> getEstimateLineItem() {
        return estimateLineItem;
    }

    public void setEstimateLineItem(List<EstimateLineItem> estimateLineItem) {
        this.estimateLineItem = estimateLineItem;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public String getModeOfDelivery() {
        return modeOfDelivery;
    }

    public void setModeOfDelivery(String modeOfDelivery) {
        this.modeOfDelivery = modeOfDelivery;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getVariationAmount() {
        return variationAmount;
    }

    public void setVariationAmount(BigDecimal variationAmount) {
        this.variationAmount = variationAmount;
    }

    public BigDecimal getCalculatedTotalValue() {
        return calculatedTotalValue;
    }

    public void setCalculatedTotalValue(BigDecimal calculatedTotalValue) {
        this.calculatedTotalValue = calculatedTotalValue;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getRoundOffValue() {
        return roundOffValue;
    }

    public void setRoundOffValue(BigDecimal roundOffValue) {
        this.roundOffValue = roundOffValue;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getPricingPolicyName() {
        return pricingPolicyName;
    }

    public void setPricingPolicyName(String pricingPolicyName) {
        this.pricingPolicyName = pricingPolicyName;
    }

    public BigDecimal getCurrentPaymentAmount() {
        return currentPaymentAmount;
    }

    public void setCurrentPaymentAmount(BigDecimal currentPaymentAmount) {
        this.currentPaymentAmount = currentPaymentAmount;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public String getDescriptionOne() {
        return descriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        this.descriptionOne = descriptionOne;
    }

    public String getDescriptionTwo() {
        return descriptionTwo;
    }

    public void setDescriptionTwo(String descriptionTwo) {
        this.descriptionTwo = descriptionTwo;
    }

    public String getDescriptionThree() {
        return descriptionThree;
    }

    public void setDescriptionThree(String descriptionThree) {
        this.descriptionThree = descriptionThree;
    }

    public String getDescriptionFour() {
        return descriptionFour;
    }

    public void setDescriptionFour(String descriptionFour) {
        this.descriptionFour = descriptionFour;
    }

    public String getDescriptionFive() {
        return descriptionFive;
    }

    public void setDescriptionFive(String descriptionFive) {
        this.descriptionFive = descriptionFive;
    }

    public String getDescriptionSix() {
        return descriptionSix;
    }

    public void setDescriptionSix(String descriptionSix) {
        this.descriptionSix = descriptionSix;
    }

    public String getDescriptionSeven() {
        return descriptionSeven;
    }

    public void setDescriptionSeven(String descriptionSeven) {
        this.descriptionSeven = descriptionSeven;
    }

    public String getDescriptionEight() {
        return descriptionEight;
    }

    public void setDescriptionEight(String descriptionEight) {
        this.descriptionEight = descriptionEight;
    }

    public String getDescriptionNine() {
        return descriptionNine;
    }

    public void setDescriptionNine(String descriptionNine) {
        this.descriptionNine = descriptionNine;
    }

    public String getDescriptionTen() {
        return descriptionTen;
    }

    public void setDescriptionTen(String descriptionTen) {
        this.descriptionTen = descriptionTen;
    }

    public int getDescriptioncount() {
        return descriptioncount;
    }

    public void setDescriptioncount(int descriptioncount) {
        this.descriptioncount = descriptioncount;
    }

    public Journal getEstimateJournal() {
        return estimateJournal;
    }

    public void setEstimateJournal(Journal estimateJournal) {
        this.estimateJournal = estimateJournal;
    }

    public Boolean getNoParty() {
        return noParty;
    }

    public void setNoParty(Boolean noParty) {
        this.noParty = noParty;
    }

    public String getNoPartyName() {
        return noPartyName;
    }

    public void setNoPartyName(String noPartyName) {
        this.noPartyName = noPartyName;
    }

    public String getNoPartyLineOne() {
        return noPartyLineOne;
    }

    public void setNoPartyLineOne(String noPartyLineOne) {
        this.noPartyLineOne = noPartyLineOne;
    }

    public String getNoPartyCity() {
        return noPartyCity;
    }

    public void setNoPartyCity(String noPartyCity) {
        this.noPartyCity = noPartyCity;
    }

    public String getNoPartyDistrict() {
        return noPartyDistrict;
    }

    public void setNoPartyDistrict(String noPartyDistrict) {
        this.noPartyDistrict = noPartyDistrict;
    }

    public String getNoPartyState() {
        return noPartyState;
    }

    public void setNoPartyState(String noPartyState) {
        this.noPartyState = noPartyState;
    }

    public String getNoPartyGSTIN() {
        return noPartyGSTIN;
    }

    public void setNoPartyGSTIN(String noPartyGSTIN) {
        this.noPartyGSTIN = noPartyGSTIN;
    }
    
    

}
