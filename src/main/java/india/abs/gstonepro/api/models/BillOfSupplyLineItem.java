/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author SGS
 */

@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findBOSLineItemsByProduct",
                    query = "from BillOfSupplyLineItem b where b.billOfSupply.bosDate between :fromDate and :toDate and b.product=:product and b.billOfSupply.cancelled=:cancelled"
            )
            

        }
)
public class BillOfSupplyLineItem {

    private int lineNumber;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bosli_id", updatable = false, nullable = false)
    private long bosliId;

    @ManyToOne
    @JoinColumn(name = "bos_id")
    private BillOfSupply billOfSupply;

    @ManyToOne
    private Product product;

    @Column(name = "product_name")
    private String productName;

    @OneToOne(mappedBy = "psli", cascade = CascadeType.ALL)
    private ProductStockEntry productStock;

    @Column(name = "uqc_one")
    private String uqcOne;

    @Column(name = "uqc_one_quantity")
    private int uqcOneQuantity;

    @Column(name = "uqc_one_rate")
    private BigDecimal uqcOneRate;

    @Column(name = "uqc_one_value")
    private float uqcOneValue;

    @Column(name = "uqc_two")
    private String uqcTwo;

    @Column(name = "uqc_two_quantity")
    private int uqcTwoQuantity;

    @Column(name = "uqc_two_rate")
    private BigDecimal uqcTwoRate;

    @Column(name = "uqc_two_value")
    private float uqcTwoValue;

    @Column(name = "stock_quantity")
    private int stockQuantity;

    @Column(name = "hsn_sac")
    private String hsnSac;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_value")
    private BigDecimal discountValue;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "uqc1_base_rate")
    private BigDecimal uqc1BaseRate;

    @Column(name = "uqc2_base_rate")
    private BigDecimal uqc2BaseRate;

    @Column(name = "profit")
    private BigDecimal profit;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Embedded
    private Audit auditData;

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long getBosliId() {
        return bosliId;
    }

    public void setBosliId(long bosliId) {
        this.bosliId = bosliId;
    }

    public BillOfSupply getBillOfSupply() {
        return billOfSupply;
    }

    public void setBillOfSupply(BillOfSupply billOfSupply) {
        this.billOfSupply = billOfSupply;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductStockEntry getProductStock() {
        return productStock;
    }

    public void setProductStock(ProductStockEntry productStock) {
        this.productStock = productStock;
    }

    public String getUqcOne() {
        return uqcOne;
    }

    public void setUqcOne(String uqcOne) {
        this.uqcOne = uqcOne;
    }

    public int getUqcOneQuantity() {
        return uqcOneQuantity;
    }

    public void setUqcOneQuantity(int uqcOneQuantity) {
        this.uqcOneQuantity = uqcOneQuantity;
    }

    public BigDecimal getUqcOneRate() {
        return uqcOneRate;
    }

    public void setUqcOneRate(BigDecimal uqcOneRate) {
        this.uqcOneRate = uqcOneRate;
    }

    public float getUqcOneValue() {
        return uqcOneValue;
    }

    public void setUqcOneValue(float uqcOneValue) {
        this.uqcOneValue = uqcOneValue;
    }

    public String getUqcTwo() {
        return uqcTwo;
    }

    public void setUqcTwo(String uqcTwo) {
        this.uqcTwo = uqcTwo;
    }

    public int getUqcTwoQuantity() {
        return uqcTwoQuantity;
    }

    public void setUqcTwoQuantity(int uqcTwoQuantity) {
        this.uqcTwoQuantity = uqcTwoQuantity;
    }

    public BigDecimal getUqcTwoRate() {
        return uqcTwoRate;
    }

    public void setUqcTwoRate(BigDecimal uqcTwoRate) {
        this.uqcTwoRate = uqcTwoRate;
    }

    public float getUqcTwoValue() {
        return uqcTwoValue;
    }

    public void setUqcTwoValue(float uqcTwoValue) {
        this.uqcTwoValue = uqcTwoValue;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getUqc1BaseRate() {
        return uqc1BaseRate;
    }

    public void setUqc1BaseRate(BigDecimal uqc1BaseRate) {
        this.uqc1BaseRate = uqc1BaseRate;
    }

    public BigDecimal getUqc2BaseRate() {
        return uqc2BaseRate;
    }

    public void setUqc2BaseRate(BigDecimal uqc2BaseRate) {
        this.uqc2BaseRate = uqc2BaseRate;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public String toString() {
        return "BillOfSupplyLineItem{" + "lineNumber=" + lineNumber + ", bosliId=" + bosliId + ", product=" + product + ", productName=" + productName + ", uqcOne=" + uqcOne + ", uqcOneQuantity=" + uqcOneQuantity + ", uqcOneRate=" + uqcOneRate + ", uqcOneValue=" + uqcOneValue + ", uqcTwo=" + uqcTwo + ", uqcTwoQuantity=" + uqcTwoQuantity + ", uqcTwoRate=" + uqcTwoRate + ", uqcTwoValue=" + uqcTwoValue + ", stockQuantity=" + stockQuantity + ", hsnSac=" + hsnSac + ", discountPercent=" + discountPercent + ", discountValue=" + discountValue + ", value=" + value + ", uqc1BaseRate=" + uqc1BaseRate + ", uqc2BaseRate=" + uqc2BaseRate + ", profit=" + profit + ", cancelled=" + cancelled + '}';
    }

   
    
    

}
