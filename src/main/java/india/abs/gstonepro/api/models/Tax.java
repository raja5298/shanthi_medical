/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author SGS
 */
@Entity
public class Tax {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tax_id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "tax_rate")
    private float taxRate;
    
    public Tax() {
    }

    public Tax(float taxRate) {
        this.taxRate = taxRate;
    }

    public Long getId() {
        return id;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTaxRate(float taxRate) {
        this.taxRate = taxRate;
    }

}
