/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.Date;
import javax.persistence.Column;

/**
 *
 * @author SGS
 */
public class EditPurchaseSale {
    
    @Column(name="edit_psid")
    private Long editPSId;
    
    @Column(name="company")
    private Company company;
    
    @Column(name="type")
    private String type;
    
    @Column(name="bill_details")
    private String billDetails;
    
    @Column(name="user_name")
    private String userName;
    
    @Column(name="edit_time")
    private Date editTime;

    public Long getEditPSId() {
        return editPSId;
    }

    public void setEditPSId(Long editPSId) {
        this.editPSId = editPSId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(String billDetails) {
        this.billDetails = billDetails;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }
    
    
    
}
