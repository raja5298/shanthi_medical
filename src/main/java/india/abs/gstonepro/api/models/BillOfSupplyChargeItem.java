/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author SGS
 */
@Entity
public class BillOfSupplyChargeItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bosci_id", updatable = false, nullable = false)
    private long bosciId;

    @ManyToOne
    @JoinColumn(name = "bos_id")
    private BillOfSupply billOfSupply;

    @Column(name = "bill_charge_name")
    private String billChargeName;

    @Column(name = "value")
    private BigDecimal value;
    
    private boolean cancelled;
    
    @Embedded
    private Audit auditData;

    public BillOfSupply getBillOfSupply() {
        return billOfSupply;
    }

    public void setBillOfSupply(BillOfSupply billOfSupply) {
        this.billOfSupply = billOfSupply;
    }

    public String getBillChargeName() {
        return billChargeName;
    }

    public void setBillChargeName(String billChargeName) {
        this.billChargeName = billChargeName;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public long getBosciId() {
        return bosciId;
    }

    public void setBosciId(long bosciId) {
        this.bosciId = bosciId;
    }


    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }   
}
