/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author SGS
 */
@Entity

public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_role_id", updatable = false, nullable = false)
    private Long userRoleId;

    private Company company;

    private AppUser user;
    
    @Column(name = "can_read_accounts")
    private boolean canReadAccounts;
    
    @Column(name = "can_create_accounts")
    private boolean canCreateAccounts;
    
    @Column(name = "can_update_accounts")
    private boolean canUpdateAccounts;
    
    @Column(name = "can_delete_accounts")
    private boolean canDeleteAccounts;
    
    @Column(name = "can_read_inventory")
    private boolean canReadInventory;
    
    @Column(name = "can_create_inventory")
    private boolean canCreateInventory;
    
    @Column(name = "can_update_inventory")
    private boolean canUpdateInventory;
    
    @Column(name = "can_delete_inventory")
    private boolean canDeleteInventory;
    
    @Column(name = "can_read_billing")
    private boolean canReadBilling;

    @Column(name = "can_create_billing")
    private boolean canCreateBilling;
    
    @Column(name = "can_update_billing")
    private boolean canUpdateBilling;
    
    @Column(name = "can_delete_billing")
    private boolean canDeleteBilling;

}
