/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findTransactionsByLedger",
                    query = "from Transaction t where t.ledger=:ledger and t.transactionDate between :fromDate and :toDate"
            )
            ,
                @NamedQuery(
                    name = "findTransactionsByLedgers",
                    query = "from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate"
            )
            ,
             @NamedQuery(
                    name = "findTransactionsByDay",
                    query = "from Transaction t where t.transactionDate between :fromDate and :toDate"
            )
            ,@NamedQuery(
                    name = "findTransactionsByType",
                    query = "from Transaction t where t.transactionDate between :fromDate and :toDate and t.type =:type"
            )
        }
)
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id", updatable = false, nullable = false)
    private Long transactionId;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "type")
    private String type;

    @Temporal(TemporalType.DATE)
    private Date transactionDate;

    @ManyToOne
    @JoinColumn(name = "ledger_id", nullable = false)
    private Ledger ledger;

    @ManyToOne
    private Journal journal;

    @Column(name = "particulars")
    private String particulars;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "editable")
    private boolean editable;

    @ManyToOne
    @JoinColumn(name = "payment_id")
    private Payment payment;

    @Embedded
    private Audit auditData;

    public Transaction(String type, Date transactionDate, Ledger ledger, Journal journal, String particulars, BigDecimal amount) {
        this.type = type;
        this.transactionDate = transactionDate;
        this.ledger = ledger;
        this.journal = journal;
        this.particulars = particulars;
        this.amount = amount;
    }

    public Transaction() {

    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getType() {
        return type;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public Journal getJournal() {
        return journal;
    }

    public String getParticulars() {
        return particulars;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Transaction{" + "transactionId=" + transactionId + ", type=" + type + ", transactionDate=" + transactionDate + ", ledger=" + ledger + ", particulars=" + particulars + ", amount=" + amount + '}';
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

}
