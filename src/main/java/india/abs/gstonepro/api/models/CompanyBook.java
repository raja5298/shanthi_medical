/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
public class CompanyBook {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false, nullable = false)
    private Long companyBookId;

    @OneToOne
    @JoinColumn(name = "company_id")
    private Company company;
    
    

    @Column(name = "tax_invoice_id_count")
    private Long taxInvoiceIdCount;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "last_tax_invoice_date")
    private Date lastTaxInvoiceDate;

    @Column(name = "tax_invoice_count")
    private Long taxInvoiceCount;
    
    private Long taxInvoiceStart;
    
    private String taxInvoicePrefix;

    @Column(name = "bos_id_count")
    private Long bosIdCount;

    @Temporal(TemporalType.DATE)
    @Column(name = "last_bos_date")
    private Date lastBOSDate;

    @Column(name = "bos_count")
    private Long bosCount;
    
    private Long bosStart;
    
    private String bosPrefix;

    @Temporal(TemporalType.DATE)
    @Column(name = "last_estimate_date")
    private Date lastEstimateDate;

    @Column(name = "estimate_id_count")
    private Long estimateIdCount;

    @Column(name = "estimate_count")
    private Long estimateCount;
    
    private Long estimateStart;
    
    private String estimatePrefix;

    @Column(name = "purchase_id_count")
    private Long purchaseIdCount;

    @Column(name = "purchase_count")
    private Long purchaseCount;
    
    private Long purchaseStart;
    
    private String purchasePrefix;

    @Column(name = "cd_note_id_count")
    private Long cdNoteIdCount;

    @Column(name = "cd_note_count")
    private Long cdNoteCount;

    @Column(name = "last_cd_note_date")
    @Temporal(TemporalType.DATE)
    private Date lastCDNoteDate;
    
    private Long crDrStart;
    
    private String crDrPrefix;
    
    @Column(name = "journal_id_count")
    private Long journalIdCount;

    @Column(name = "journal_count")
    private Long journalCount;
    
    private BigDecimal netProfit;
    
    private BigDecimal grossProfit;
    
    private BigDecimal balance;

    public Long getCompanyBookId() {
        return companyBookId;
    }

    public void setCompanyBookId(Long companyBookId) {
        this.companyBookId = companyBookId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Long getTaxInvoiceIdCount() {
        return taxInvoiceIdCount;
    }

    public void setTaxInvoiceIdCount(Long taxInvoiceIdCount) {
        this.taxInvoiceIdCount = taxInvoiceIdCount;
    }

    public Date getLastTaxInvoiceDate() {
        return lastTaxInvoiceDate;
    }

    public void setLastTaxInvoiceDate(Date lastTaxInvoiceDate) {
        this.lastTaxInvoiceDate = lastTaxInvoiceDate;
    }

    public Long getTaxInvoiceCount() {
        return taxInvoiceCount;
    }

    public void setTaxInvoiceCount(Long taxInvoiceCount) {
        this.taxInvoiceCount = taxInvoiceCount;
    }

    public Long getBosIdCount() {
        return bosIdCount;
    }

    public void setBosIdCount(Long bosIdCount) {
        this.bosIdCount = bosIdCount;
    }

    public Date getLastBOSDate() {
        return lastBOSDate;
    }

    public void setLastBOSDate(Date lastBOSDate) {
        this.lastBOSDate = lastBOSDate;
    }

    public Long getBosCount() {
        return bosCount;
    }

    public void setBosCount(Long bosCount) {
        this.bosCount = bosCount;
    }

    public Date getLastEstimateDate() {
        return lastEstimateDate;
    }

    public void setLastEstimateDate(Date lastEstimateDate) {
        this.lastEstimateDate = lastEstimateDate;
    }

    public Long getEstimateIdCount() {
        return estimateIdCount;
    }

    public void setEstimateIdCount(Long estimateIdCount) {
        this.estimateIdCount = estimateIdCount;
    }

    public Long getEstimateCount() {
        return estimateCount;
    }

    public void setEstimateCount(Long estimateCount) {
        this.estimateCount = estimateCount;
    }

    public Long getCdNoteIdCount() {
        return cdNoteIdCount;
    }

    public void setCdNoteIdCount(Long cdNoteIdCount) {
        this.cdNoteIdCount = cdNoteIdCount;
    }

    public Long getCdNoteCount() {
        return cdNoteCount;
    }

    public void setCdNoteCount(Long cdNoteCount) {
        this.cdNoteCount = cdNoteCount;
    }

    public Date getLastCDNoteDate() {
        return lastCDNoteDate;
    }

    public void setLastCDNoteDate(Date lastCDNoteDate) {
        this.lastCDNoteDate = lastCDNoteDate;
    }

    public Long getPurchaseIdCount() {
        return purchaseIdCount;
    }

    public void setPurchaseIdCount(Long purchaseIdCount) {
        this.purchaseIdCount = purchaseIdCount;
    }

    public Long getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Long purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Long getJournalIdCount() {
        return journalIdCount;
    }

    public void setJournalIdCount(Long journalIdCount) {
        this.journalIdCount = journalIdCount;
    }

    public Long getJournalCount() {
        return journalCount;
    }

    public void setJournalCount(Long journalCount) {
        this.journalCount = journalCount;
    }

    public Long getTaxInvoiceStart() {
        return taxInvoiceStart;
    }

    public void setTaxInvoiceStart(Long taxInvoiceStart) {
        this.taxInvoiceStart = taxInvoiceStart;
    }

    public String getTaxInvoicePrefix() {
        return taxInvoicePrefix;
    }

    public void setTaxInvoicePrefix(String taxInvoicePrefix) {
        this.taxInvoicePrefix = taxInvoicePrefix;
    }

    public Long getBosStart() {
        return bosStart;
    }

    public void setBosStart(Long bosStart) {
        this.bosStart = bosStart;
    }

    public String getBosPrefix() {
        return bosPrefix;
    }

    public void setBosPrefix(String bosPrefix) {
        this.bosPrefix = bosPrefix;
    }

    public Long getEstimateStart() {
        return estimateStart;
    }

    public void setEstimateStart(Long estimateStart) {
        this.estimateStart = estimateStart;
    }

    public String getEstimatePrefix() {
        return estimatePrefix;
    }

    public void setEstimatePrefix(String estimatePrefix) {
        this.estimatePrefix = estimatePrefix;
    }

    public Long getPurchaseStart() {
        return purchaseStart;
    }

    public void setPurchaseStart(Long purchaseStart) {
        this.purchaseStart = purchaseStart;
    }

    public String getPurchasePrefix() {
        return purchasePrefix;
    }

    public void setPurchasePrefix(String purchasePrefix) {
        this.purchasePrefix = purchasePrefix;
    }

    public Long getCrDrStart() {
        return crDrStart;
    }

    public void setCrDrStart(Long crDrStart) {
        this.crDrStart = crDrStart;
    }

    public String getCrDrPrefix() {
        return crDrPrefix;
    }

    public void setCrDrPrefix(String crDrPrefix) {
        this.crDrPrefix = crDrPrefix;
    }

    public BigDecimal getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(BigDecimal netProfit) {
        this.netProfit = netProfit;
    }

    public BigDecimal getGrossProfit() {
        return grossProfit;
    }

    public void setGrossProfit(BigDecimal grossProfit) {
        this.grossProfit = grossProfit;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    
    
    
    

}
