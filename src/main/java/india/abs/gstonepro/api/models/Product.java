package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.swing.JLabel;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findAllProductsByCompany",
                    query = "from Product p where p.company=:company"
            )
            ,
            @NamedQuery(
                    name = "findAllProductsWithoutServiceByCompany",
                    query = "from Product p where p.company=:company and p.service=:isService"
            )
        })
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id", updatable = false, nullable = false)
    private Long productId;

    @Column(name = "name")
    private String name;

    @Column(name = "tamil_name")
    private String tamilName;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "productCode")
    private String productCode;

    @Column(name = "hsn_code")
    private String hsnCode;

    @Column(name = "igst_percentage")
    private float igstPercentage;

    @Column(name = "cgst_percentage")
    private float cgstPercentage;

    @Column(name = "sgst_percentage")
    private float sgstPercentage;

    @Column(name = "base_product_rate")
    private BigDecimal baseProductRate;

    @Column(name = "product_rate")
    private BigDecimal productRate;

    @Column(name = "inclusive_of_gst")
    private boolean inclusiveOfGST;

    @Column(name = "base_price_inclusive_of_gst")
    private boolean basePriceInclusiveOfGST;

    @Column(name = "uqc1")
    private String UQC1;

    @Column(name = "uqc2")
    private String UQC2;

    @Column(name = "uqc2_value")
    private int UQC2Value;

    @Column(name = "current_stock")
    private int currentStock;

    @Column(name = "bill_arithmetic")
    private boolean billArithmetic;

    @Column(name = "tax_applicable_for_bill_arithmetic")
    private boolean taxApplicableForBillArithmetic;

    @Column(name = "tax_inclusive_for_bill_arithmetic")
    private boolean taxInclusiveForBillArithmetic;

    @Column(name = "nill_rated")
    private boolean nillRated;

    @Column(name = "exempted")
    private boolean exempted;

    @Column(name = "non_gst")
    private boolean nonGST;

    @Column(name = "reverse_charge_applicable")
    private boolean reverseChargeApplicable;

    @Column(name = "cess_applicable")
    private boolean cessApplicable;

    @Column(name = "cess_type")
    private String cessType;

    @Column(name = "cess_percentage")
    private float cessPercentage;

    @Column(name = "cess_constant_value")
    private BigDecimal cessConstantValue;

    @Column(name = "cess_uqc")
    private String cessUQC;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "product_group_id")
    private ProductGroup productGroup;

    @OneToMany(mappedBy = "product")
    private List<ProductStockEntry> productStocks;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "service")
    private boolean service;

    @Embedded
    private Audit auditData;

//    @Transient
    @Column(name = "is_opening_stock_editable")
    private boolean isOpeningStockEditable;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "descriptionone")
    private String descriptionOne;

    @Column(name = "descriptiontwo")
    private String descriptionTwo;

    @Column(name = "descriptionthree")
    private String descriptionThree;

    @Column(name = "descriptionfour")
    private String descriptionFour;

    @Column(name = "descriptionfive")
    private String descriptionFive;

    @Column(name = "descriptionsix")
    private String descriptionSix;

    @Column(name = "descriptionseven")
    private String descriptionSeven;

    @Column(name = "descriptioneight")
    private String descriptionEight;

    @Column(name = "descriptionnine")
    private String descriptionNine;

    @Column(name = "descriptionoten")
    private String descriptionTen;

    @Column(name = "descriptioncount")
    private int descriptioncount;
    
    public Product() {
    }

    public Product(String name) {
        this.name = name;
    }
//
//    public Product(String name, String tamilName, String shortName, String hsnCode, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal productRate, boolean inclusiveOfGST, String UQC1, String UQC2, int UQC2Value) {
//        this.name = name;
//        this.tamilName = tamilName;
//        this.shortName = shortName;
//        this.hsnCode = hsnCode;
//        this.igstPercentage = igstPercentage;
//        this.cgstPercentage = cgstPercentage;
//        this.sgstPercentage = sgstPercentage;
//        this.productRate = productRate;
//        this.inclusiveOfGST = inclusiveOfGST;
//        this.UQC1 = UQC1;
//        this.UQC2 = UQC2;
//        this.UQC2Value = UQC2Value;
//    }
//
//    public Product(String name, String tamilName, String shortName, String hsnCode, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal productRate, boolean inclusiveOfGST, String UQC1, String UQC2, int UQC2Value, Company company, List<ProductStock> productStocks, boolean isOpeningStockEditable) {
//        this.name = name;
//        this.tamilName = tamilName;
//        this.shortName = shortName;
//        this.hsnCode = hsnCode;
//        this.igstPercentage = igstPercentage;
//        this.cgstPercentage = cgstPercentage;
//        this.sgstPercentage = sgstPercentage;
//        this.productRate = productRate;
//        this.inclusiveOfGST = inclusiveOfGST;
//        this.UQC1 = UQC1;
//        this.UQC2 = UQC2;
//        this.UQC2Value = UQC2Value;
//        this.company = company;
//        this.productStocks = productStocks;
//        this.isOpeningStockEditable = isOpeningStockEditable;
//    }

    public Product(String name, String tamilName, String shortName, String hsnCode, float igstPercentage, float cgstPercentage, float sgstPercentage, BigDecimal productRate, boolean inclusiveOfGST, String UQC1, String UQC2, int UQC2Value, int currentStock, boolean service) {
        this.name = name;
        this.tamilName = tamilName;
        this.shortName = shortName;
        this.hsnCode = hsnCode;
        this.igstPercentage = igstPercentage;
        this.cgstPercentage = cgstPercentage;
        this.sgstPercentage = sgstPercentage;
        this.productRate = productRate;
        this.inclusiveOfGST = inclusiveOfGST;
        this.UQC1 = UQC1;
        this.UQC2 = UQC2;
        this.UQC2Value = UQC2Value;
        this.currentStock = currentStock;
        this.baseProductRate = productRate;//same as product rate
        this.basePriceInclusiveOfGST = inclusiveOfGST;
        this.service = service;
        this.productCode="";
    }

    public BigDecimal getBaseProductRate() {
        return baseProductRate;
    }

    public void setBaseProductRate(BigDecimal baseProductRate) {
        this.baseProductRate = baseProductRate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTamilName(String tamilName) {
        this.tamilName = tamilName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public void setProductRate(BigDecimal productRate) {
        this.productRate = productRate;
    }

    public void setInclusiveOfGST(boolean inclusiveOfGST) {
        this.inclusiveOfGST = inclusiveOfGST;
    }

    public Long getProductId() {
        return productId;
    }

    public List<ProductStockEntry> getProductStocks() {
        return productStocks;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setProductStocks(List<ProductStockEntry> stockUpdates) {
        this.productStocks = stockUpdates;
    }

    public String getName() {
        return name;
    }

    public String getTamilName() {
        return tamilName;
    }

    public String getShortName() {
        return shortName;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public BigDecimal getProductRate() {
        return productRate;
    }

    public boolean isInclusiveOfGST() {
        return inclusiveOfGST;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public boolean isIsOpeningStockEditable() {
        return isOpeningStockEditable;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public void setIsOpeningStockEditable(boolean isOpeningStockEditable) {
        this.isOpeningStockEditable = isOpeningStockEditable;
    }

    public String getUQC1() {
        return UQC1;
    }

    public void setUQC1(String UQC1) {
        this.UQC1 = UQC1;
    }

    public String getUQC2() {
        return UQC2;
    }

    public void setUQC2(String UQC2) {
        this.UQC2 = UQC2;
    }

    public int getUQC2Value() {
        return UQC2Value;
    }

    public void setUQC2Value(int UQC2Value) {
        this.UQC2Value = UQC2Value;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public int getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(int currentStock) {
        this.currentStock = currentStock;
    }

    public void printStockValue() {

    }

    public boolean isBasePriceInclusiveOfGST() {
        return basePriceInclusiveOfGST;
    }

    public void setBasePriceInclusiveOfGST(boolean basePriceInclusiveOfGST) {
        this.basePriceInclusiveOfGST = basePriceInclusiveOfGST;
    }

    public String getStockValue() {
        int nCurrentStock = this.currentStock;
        int UQC1Count = nCurrentStock / this.UQC2Value;
        int UQC2Count = nCurrentStock % this.UQC2Value;
        String result = UQC1Count + " " + this.UQC1 + ", " + UQC2Count + " " + this.UQC2;
        if(this.UQC1.equals(this.UQC2)){
            result = UQC1Count + " " + this.UQC1;
        }
        return result;
    }
    
    
    public String getStockValueInTwoLines() {
        int nCurrentStock = this.currentStock;
        int UQC1Count = nCurrentStock / this.UQC2Value;
        int UQC2Count = nCurrentStock % this.UQC2Value;
        String result = "<html><b>" + UQC1Count + "</b> " + this.UQC1 + "<br><b>" + UQC2Count + "</b> " + this.UQC2 + "</html>";

        return result;
    }

    public int getFirstValue() {
        int currentStock = this.currentStock;
        int UQC1Count = currentStock / this.UQC2Value;
        return UQC1Count;
    }

    public int getSecondValue() {
        int currentStock = this.currentStock;
        int UQC2Count = currentStock % this.UQC2Value;
        return UQC2Count;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", name=" + name + ", tamilName=" + tamilName + ", shortName=" + shortName + ", hsnCode=" + hsnCode + ", igstPercentage=" + igstPercentage + ", cgstPercentage=" + cgstPercentage + ", sgstPercentage=" + sgstPercentage + ", baseProductRate=" + baseProductRate + ", productRate=" + productRate + ", inclusiveOfGST=" + inclusiveOfGST + ", basePriceInclusiveOfGST=" + basePriceInclusiveOfGST + ", UQC1=" + UQC1 + ", UQC2=" + UQC2 + ", UQC2Value=" + UQC2Value + ", currentStock=" + currentStock + ", company=" + company + ", isOpeningStockEditable=" + isOpeningStockEditable + '}';
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public boolean isNillRated() {
        return nillRated;
    }

    public void setNillRated(boolean nillRated) {
        this.nillRated = nillRated;
    }

    public boolean isExempted() {
        return exempted;
    }

    public void setExempted(boolean exempted) {
        this.exempted = exempted;
    }

    public boolean isNonGST() {
        return nonGST;
    }

    public void setNonGST(boolean nonGST) {
        this.nonGST = nonGST;
    }

    public boolean isCessApplicable() {
        return cessApplicable;
    }

    public void setCessApplicable(boolean cessApplicable) {
        this.cessApplicable = cessApplicable;
    }

    public String getCessType() {
        return cessType;
    }

    public void setCessType(String cessType) {
        this.cessType = cessType;
    }

    public float getCessPercentage() {
        return cessPercentage;
    }

    public void setCessPercentage(float cessPercentage) {
        this.cessPercentage = cessPercentage;
    }

    public BigDecimal getCessConstantValue() {
        return cessConstantValue;
    }

    public void setCessConstantValue(BigDecimal cessConstantValue) {
        this.cessConstantValue = cessConstantValue;
    }

    public String getCessUQC() {
        return cessUQC;
    }

    public void setCessUQC(String cessUQC) {
        this.cessUQC = cessUQC;
    }

    public boolean isReverseChargeApplicable() {
        return reverseChargeApplicable;
    }

    public void setReverseChargeApplicable(boolean reverseChargeApplicable) {
        this.reverseChargeApplicable = reverseChargeApplicable;
    }

    public boolean isBillArithmetic() {
        return billArithmetic;
    }

    public void setBillArithmetic(boolean billArithmetic) {
        this.billArithmetic = billArithmetic;
    }

    public boolean isTaxApplicableForBillArithmetic() {
        return taxApplicableForBillArithmetic;
    }

    public void setTaxApplicableForBillArithmetic(boolean taxApplicableForBillArithmetic) {
        this.taxApplicableForBillArithmetic = taxApplicableForBillArithmetic;
    }

    public boolean isTaxInclusiveForBillArithmetic() {
        return taxInclusiveForBillArithmetic;
    }

    public void setTaxInclusiveForBillArithmetic(boolean taxInclusiveForBillArithmetic) {
        this.taxInclusiveForBillArithmetic = taxInclusiveForBillArithmetic;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public ProductGroup getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(ProductGroup productGroup) {
        this.productGroup = productGroup;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDescriptionOne() {
        return descriptionOne;
    }

    public void setDescriptionOne(String descriptionOne) {
        this.descriptionOne = descriptionOne;
    }

    public String getDescriptionTwo() {
        return descriptionTwo;
    }

    public void setDescriptionTwo(String descriptionTwo) {
        this.descriptionTwo = descriptionTwo;
    }

    public String getDescriptionThree() {
        return descriptionThree;
    }

    public void setDescriptionThree(String descriptionThree) {
        this.descriptionThree = descriptionThree;
    }

    public String getDescriptionFour() {
        return descriptionFour;
    }

    public void setDescriptionFour(String descriptionFour) {
        this.descriptionFour = descriptionFour;
    }

    public String getDescriptionFive() {
        return descriptionFive;
    }

    public void setDescriptionFive(String descriptionFive) {
        this.descriptionFive = descriptionFive;
    }

    public String getDescriptionSix() {
        return descriptionSix;
    }

    public void setDescriptionSix(String descriptionSix) {
        this.descriptionSix = descriptionSix;
    }

    public String getDescriptionSeven() {
        return descriptionSeven;
    }

    public void setDescriptionSeven(String descriptionSeven) {
        this.descriptionSeven = descriptionSeven;
    }

    public String getDescriptionEight() {
        return descriptionEight;
    }

    public void setDescriptionEight(String descriptionEight) {
        this.descriptionEight = descriptionEight;
    }

    public String getDescriptionNine() {
        return descriptionNine;
    }

    public void setDescriptionNine(String descriptionNine) {
        this.descriptionNine = descriptionNine;
    }

    public String getDescriptionTen() {
        return descriptionTen;
    }

    public void setDescriptionTen(String descriptionTen) {
        this.descriptionTen = descriptionTen;
    }

    public int getDescriptioncount() {
        return descriptioncount;
    }

    public void setDescriptioncount(int descriptioncount) {
        this.descriptioncount = descriptioncount;
    }
    

}
