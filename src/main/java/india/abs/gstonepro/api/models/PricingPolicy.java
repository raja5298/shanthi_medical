/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author SGS
 */
@Entity
public class PricingPolicy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pricing_policy_id", updatable = false, nullable = false)
    private Long pricingPolicyId;

    @Column(name="policy_name")
    private String policyName;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;
    
    @Column(name="deleted")
    private boolean deleted;

    @Embedded
    private Audit auditData;

    @OneToMany(mappedBy = "productPolicy",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private Set<SingleProductPolicy> SingleProductPolicies = new HashSet<SingleProductPolicy>();

    public PricingPolicy(Company company, String name) {
        this.company=company;
        this.policyName=name;
    }

    public PricingPolicy() {
       
    }

    public Set<SingleProductPolicy> getSingleProductPolicies() {
        return SingleProductPolicies;
    }

    public void setSingleProductPolicies(Set<SingleProductPolicy> SingleProductPolicies) {
        this.SingleProductPolicies = SingleProductPolicies;
    }

    private boolean editable;

    private boolean deletable;

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    

    public Long getPricingPolicyId() {
        return pricingPolicyId;
    }

    public void setPricingPolicyId(Long pricingPolicyId) {
        this.pricingPolicyId = pricingPolicyId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }

    @Override
    public String toString() {
        return "PricingPolicy{" + "pricingPolicyId=" + pricingPolicyId + ", policyName=" + policyName + ", company=" + company + ", editable=" + editable + ", deletable=" + deletable + '}';
    }
      public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }
    

}
