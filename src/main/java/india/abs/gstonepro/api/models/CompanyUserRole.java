/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findCompanyUserRoleByUserAndCompany",
                    query = "from CompanyUserRole cur where cur.user=:user and cur.company = :company"
            )
            ,
             @NamedQuery(
                    name = "findCompanyUserRoleByUser",
                    query = "from CompanyUserRole cur where cur.user=:user"
            )
        }
)
public class CompanyUserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id", updatable = false, nullable = false)
    private Long roleId;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private AppUser user;

    @Column(name = "product_view")
    private boolean productView;

    @Column(name = "product_create")
    private boolean productCreate;

    @Column(name = "product_update")
    private boolean productUpdate;

    @Column(name = "product_delete")
    private boolean productDelete;

    @Column(name = "pricing_policy_view")
    private boolean pricingPolicyView;

    @Column(name = "pricing_policy_create")
    private boolean pricingPolicyCreate;

    @Column(name = "pricing_policy_update")
    private boolean pricingPolicyUpdate;

    @Column(name = "pricing_policy_delete")
    private boolean pricingPolicyDelete;

    @Column(name = "party_view")
    private boolean partyView;

    @Column(name = "party_create")
    private boolean partyCreate;

    @Column(name = "party_update")
    private boolean partyUpdate;

    @Column(name = "party_delete")
    private boolean partyDelete;

    @Column(name = "sales_view")
    private boolean salesView;

    @Column(name = "sales_create")
    private boolean salesCreate;

    @Column(name = "sales_update")
    private boolean salesUpdate;

    @Column(name = "sales_delete")
    private boolean salesDelete;

    @Column(name = "bos_view")
    private boolean bosView;

    @Column(name = "bos_create")
    private boolean bosCreate;

    @Column(name = "bos_update")
    private boolean bosUpdate;

    @Column(name = "bos_delete")
    private boolean bosDelete;

    @Column(name = "estimate_view")
    private boolean estimateView;

    @Column(name = "estimate_create")
    private boolean estimateCreate;

    @Column(name = "estimate_update")
    private boolean estimateUpdate;

    @Column(name = "estimate_delete")
    private boolean estimateDelete;

    @Column(name = "purchase_view")
    private boolean purchaseView;

    @Column(name = "purchase_create")
    private boolean purchaseCreate;

    @Column(name = "purchase_update")
    private boolean purchaseUpdate;

    @Column(name = "purchase_delete")
    private boolean purchaseDelete;

    @Column(name = "crdr_note_view")
    private boolean crdrNoteView;

    @Column(name = "crdr_note_create")
    private boolean crdrNoteCreate;

    @Column(name = "crdr_note_update")
    private boolean crdrNoteUpdate;

    @Column(name = "crdr_note_delete")
    private boolean crdrNoteDelete;

    @Column(name = "inventory_view")
    private boolean inventoryView;

    @Column(name = "inventory_add")
    private boolean inventoryAdd;

    @Column(name = "inventory_deduct")
    private boolean inventoryDeduct;

    @Column(name = "inventory_opening_balance")
    private boolean inventoryOpeningBalance;

    @Column(name = "gst_reports")
    private boolean gstReports;

    @Column(name = "ledger_view")
    private boolean ledgerView;

    @Column(name = "ledger_create")
    private boolean ledgerCreate;

    @Column(name = "ledger_update")
    private boolean ledgerUpdate;

    @Column(name = "ledger_delete")
    private boolean ledgerDelete;

    @Column(name = "ledger_opening_balance")
    private boolean ledgerOpeningBalance;

    @Column(name = "accounts_ledger_group_balance")
    private boolean accountsLedgerGroupBalance;

    @Column(name = "accounts_day_book")
    private boolean accountsDayBook;

    @Column(name = "accounts_journal")
    private boolean accountsJournal;

    @Column(name = "accounts_payment")
    private boolean accountsPayment;

    @Column(name = "accounts_cash")
    private boolean accountsCash;

    @Column(name = "accounts_bank")
    private boolean accountsBank;

    @Column(name = "accounts_trail_balance")
    private boolean accountsTrailBalance;

    @Column(name = "accounts_balance_sheet")
    private boolean accountsBalanceSheet;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }

    public boolean isProductView() {
        return productView;
    }

    public void setProductView(boolean productView) {
        this.productView = productView;
    }

    public boolean isProductCreate() {
        return productCreate;
    }

    public void setProductCreate(boolean productCreate) {
        this.productCreate = productCreate;
    }

    public boolean isProductUpdate() {
        return productUpdate;
    }

    public void setProductUpdate(boolean productUpdate) {
        this.productUpdate = productUpdate;
    }

    public boolean isProductDelete() {
        return productDelete;
    }

    public void setProductDelete(boolean productDelete) {
        this.productDelete = productDelete;
    }

    public boolean isPricingPolicyView() {
        return pricingPolicyView;
    }

    public void setPricingPolicyView(boolean pricingPolicyView) {
        this.pricingPolicyView = pricingPolicyView;
    }

    public boolean isPricingPolicyCreate() {
        return pricingPolicyCreate;
    }

    public void setPricingPolicyCreate(boolean pricingPolicyCreate) {
        this.pricingPolicyCreate = pricingPolicyCreate;
    }

    public boolean isPricingPolicyUpdate() {
        return pricingPolicyUpdate;
    }

    public void setPricingPolicyUpdate(boolean pricingPolicyUpdate) {
        this.pricingPolicyUpdate = pricingPolicyUpdate;
    }

    public boolean isPricingPolicyDelete() {
        return pricingPolicyDelete;
    }

    public void setPricingPolicyDelete(boolean pricingPolicyDelete) {
        this.pricingPolicyDelete = pricingPolicyDelete;
    }

    public boolean isPartyView() {
        return partyView;
    }

    public void setPartyView(boolean partyView) {
        this.partyView = partyView;
    }

    public boolean isPartyCreate() {
        return partyCreate;
    }

    public void setPartyCreate(boolean partyCreate) {
        this.partyCreate = partyCreate;
    }

    public boolean isPartyUpdate() {
        return partyUpdate;
    }

    public void setPartyUpdate(boolean partyUpdate) {
        this.partyUpdate = partyUpdate;
    }

    public boolean isPartyDelete() {
        return partyDelete;
    }

    public void setPartyDelete(boolean partyDelete) {
        this.partyDelete = partyDelete;
    }

    public boolean isSalesView() {
        return salesView;
    }

    public void setSalesView(boolean salesView) {
        this.salesView = salesView;
    }

    public boolean isSalesCreate() {
        return salesCreate;
    }

    public void setSalesCreate(boolean salesCreate) {
        this.salesCreate = salesCreate;
    }

    public boolean isSalesUpdate() {
        return salesUpdate;
    }

    public void setSalesUpdate(boolean salesUpdate) {
        this.salesUpdate = salesUpdate;
    }

    public boolean isSalesDelete() {
        return salesDelete;
    }

    public void setSalesDelete(boolean salesDelete) {
        this.salesDelete = salesDelete;
    }

    public boolean isBosView() {
        return bosView;
    }

    public void setBosView(boolean bosView) {
        this.bosView = bosView;
    }

    public boolean isBosCreate() {
        return bosCreate;
    }

    public void setBosCreate(boolean bosCreate) {
        this.bosCreate = bosCreate;
    }

    public boolean isBosUpdate() {
        return bosUpdate;
    }

    public void setBosUpdate(boolean bosUpdate) {
        this.bosUpdate = bosUpdate;
    }

    public boolean isBosDelete() {
        return bosDelete;
    }

    public void setBosDelete(boolean bosDelete) {
        this.bosDelete = bosDelete;
    }

    public boolean isEstimateView() {
        return estimateView;
    }

    public void setEstimateView(boolean estimateView) {
        this.estimateView = estimateView;
    }

    public boolean isEstimateCreate() {
        return estimateCreate;
    }

    public void setEstimateCreate(boolean estimateCreate) {
        this.estimateCreate = estimateCreate;
    }

    public boolean isEstimateUpdate() {
        return estimateUpdate;
    }

    public void setEstimateUpdate(boolean estimateUpdate) {
        this.estimateUpdate = estimateUpdate;
    }

    public boolean isEstimateDelete() {
        return estimateDelete;
    }

    public void setEstimateDelete(boolean estimateDelete) {
        this.estimateDelete = estimateDelete;
    }

    public boolean isPurchaseView() {
        return purchaseView;
    }

    public void setPurchaseView(boolean purchaseView) {
        this.purchaseView = purchaseView;
    }

    public boolean isPurchaseCreate() {
        return purchaseCreate;
    }

    public void setPurchaseCreate(boolean purchaseCreate) {
        this.purchaseCreate = purchaseCreate;
    }

    public boolean isPurchaseUpdate() {
        return purchaseUpdate;
    }

    public void setPurchaseUpdate(boolean purchaseUpdate) {
        this.purchaseUpdate = purchaseUpdate;
    }

    public boolean isPurchaseDelete() {
        return purchaseDelete;
    }

    public void setPurchaseDelete(boolean purchaseDelete) {
        this.purchaseDelete = purchaseDelete;
    }

    public boolean isCrdrNoteView() {
        return crdrNoteView;
    }

    public void setCrdrNoteView(boolean crdrNoteView) {
        this.crdrNoteView = crdrNoteView;
    }

    public boolean isCrdrNoteCreate() {
        return crdrNoteCreate;
    }

    public void setCrdrNoteCreate(boolean crdrNoteCreate) {
        this.crdrNoteCreate = crdrNoteCreate;
    }

    public boolean isCrdrNoteUpdate() {
        return crdrNoteUpdate;
    }

    public void setCrdrNoteUpdate(boolean crdrNoteUpdate) {
        this.crdrNoteUpdate = crdrNoteUpdate;
    }

    public boolean isCrdrNoteDelete() {
        return crdrNoteDelete;
    }

    public void setCrdrNoteDelete(boolean crdrNoteDelete) {
        this.crdrNoteDelete = crdrNoteDelete;
    }

    public boolean isInventoryView() {
        return inventoryView;
    }

    public void setInventoryView(boolean inventoryView) {
        this.inventoryView = inventoryView;
    }

    public boolean isInventoryAdd() {
        return inventoryAdd;
    }

    public void setInventoryAdd(boolean inventoryAdd) {
        this.inventoryAdd = inventoryAdd;
    }

    public boolean isInventoryDeduct() {
        return inventoryDeduct;
    }

    public void setInventoryDeduct(boolean inventoryDeduct) {
        this.inventoryDeduct = inventoryDeduct;
    }

    public boolean isAccountsDayBook() {
        return accountsDayBook;
    }

    public void setAccountsDayBook(boolean accountsDayBook) {
        this.accountsDayBook = accountsDayBook;
    }

    public boolean isAccountsJournal() {
        return accountsJournal;
    }

    public void setAccountsJournal(boolean accountsJournal) {
        this.accountsJournal = accountsJournal;
    }

    public boolean isAccountsPayment() {
        return accountsPayment;
    }

    public void setAccountsPayment(boolean accountsPayment) {
        this.accountsPayment = accountsPayment;
    }

    public boolean isAccountsCash() {
        return accountsCash;
    }

    public void setAccountsCash(boolean accountsCash) {
        this.accountsCash = accountsCash;
    }

    public boolean isAccountsBank() {
        return accountsBank;
    }

    public void setAccountsBank(boolean accountsBank) {
        this.accountsBank = accountsBank;
    }

    public boolean isAccountsTrailBalance() {
        return accountsTrailBalance;
    }

    public void setAccountsTrailBalance(boolean accountsTrailBalance) {
        this.accountsTrailBalance = accountsTrailBalance;
    }

    public boolean isInventoryOpeningBalance() {
        return inventoryOpeningBalance;
    }

    public void setInventoryOpeningBalance(boolean inventoryOpeningBalance) {
        this.inventoryOpeningBalance = inventoryOpeningBalance;
    }

    public boolean isGstReports() {
        return gstReports;
    }

    public void setGstReports(boolean gstReports) {
        this.gstReports = gstReports;
    }

    public boolean isLedgerView() {
        return ledgerView;
    }

    public void setLedgerView(boolean ledgerView) {
        this.ledgerView = ledgerView;
    }

    public boolean isLedgerCreate() {
        return ledgerCreate;
    }

    public void setLedgerCreate(boolean ledgerCreate) {
        this.ledgerCreate = ledgerCreate;
    }

    public boolean isLedgerUpdate() {
        return ledgerUpdate;
    }

    public void setLedgerUpdate(boolean ledgerUpdate) {
        this.ledgerUpdate = ledgerUpdate;
    }

    public boolean isLedgerDelete() {
        return ledgerDelete;
    }

    public void setLedgerDelete(boolean ledgerDelete) {
        this.ledgerDelete = ledgerDelete;
    }

    public boolean isAccountsLedgerGroupBalance() {
        return accountsLedgerGroupBalance;
    }

    public void setAccountsLedgerGroupBalance(boolean accountsLedgerGroupBalance) {
        this.accountsLedgerGroupBalance = accountsLedgerGroupBalance;
    }

    public boolean isLedgerOpeningBalance() {
        return ledgerOpeningBalance;
    }

    public void setLedgerOpeningBalance(boolean ledgerOpeningBalance) {
        this.ledgerOpeningBalance = ledgerOpeningBalance;
    }

    public boolean isAccountsBalanceSheet() {
//        return accountsBalanceSheet
        return false;
    }

    public void setAccountsBalanceSheet(boolean accountsBalanceSheet) {
//        this.accountsBalanceSheet = accountsBalanceSheet;
        this.accountsBalanceSheet = false;
    }

//    public CompanyUserRole(boolean productView, boolean productCreate, boolean productUpdate, boolean productDelete,
//            boolean pricingPolicyView, boolean pricingPolicyCreate, boolean pricingPolicyUpdate, boolean pricingPolicyDelete,
//            boolean partyView, boolean partyCreate, boolean partyUpdate, boolean partyDelete,
//            boolean salesView, boolean salesCreate, boolean salesUpdate, boolean salesDelete,
//            boolean bosView, boolean bosCreate, boolean bosUpdate, boolean bosDelete,
//            boolean estimateView, boolean estimateCreate, boolean estimateUpdate, boolean estimateDelete,
//            boolean purchaseView, boolean purchaseCreate, boolean purchaseUpdate, boolean purchaseDelete,
//            boolean crdrNoteView, boolean crdrNoteCreate, boolean crdrNoteUpdate, boolean crdrNoteDelete,
//            boolean inventoryView, boolean inventoryAdd, boolean inventoryDeduct, boolean inventoryOpeningBalance, boolean gstReports,
//            boolean ledgerView, boolean ledgerCreate, boolean ledgerUpdate, boolean ledgerDelete, boolean ledgerOpeningBalance,
//            boolean accountsLedgergroupBalance, boolean accountsDayBook, boolean accountsJournal, boolean accountsPayment, boolean accountsCash, boolean accountsBank, boolean accountsTrailBalance, boolean accountsBalanceSheet) {
//        this.productView = productView;
//        this.productCreate = productCreate;
//        this.productUpdate = productUpdate;
//        this.productDelete = productDelete;
//        this.pricingPolicyView = pricingPolicyView;
//        this.pricingPolicyCreate = pricingPolicyCreate;
//        this.pricingPolicyUpdate = pricingPolicyUpdate;
//        this.pricingPolicyDelete = pricingPolicyDelete;
//        this.partyView = partyView;
//        this.partyCreate = partyCreate;
//        this.partyUpdate = partyUpdate;
//        this.partyDelete = partyDelete;
//        this.salesView = salesView;
//        this.salesCreate = salesCreate;
//        this.salesUpdate = salesUpdate;
//        this.salesDelete = salesDelete;
//        this.bosView = bosView;
//        this.bosCreate = bosCreate;
//        this.bosUpdate = bosUpdate;
//        this.bosDelete = bosDelete;
//        this.estimateView = estimateView;
//        this.estimateCreate = estimateCreate;
//        this.estimateUpdate = estimateUpdate;
//        this.estimateDelete = estimateDelete;
//        this.purchaseView = purchaseView;
//        this.purchaseCreate = purchaseCreate;
//        this.purchaseUpdate = purchaseUpdate;
//        this.purchaseDelete = purchaseDelete;
//        this.crdrNoteView = crdrNoteView;
//        this.crdrNoteCreate = crdrNoteCreate;
//        this.crdrNoteUpdate = crdrNoteUpdate;
//        this.crdrNoteDelete = crdrNoteDelete;
//        this.inventoryView = inventoryView;
//        this.inventoryAdd = inventoryAdd;
//        this.inventoryDeduct = inventoryDeduct;
//        this.inventoryOpeningBalance = inventoryOpeningBalance;
//        this.gstReports = gstReports;
//        this.ledgerCreate = ledgerCreate;
//        this.ledgerView = ledgerView;
//        this.ledgerUpdate = ledgerUpdate;
//        this.ledgerDelete = ledgerDelete;
//        this.ledgerOpeningBalance = ledgerOpeningBalance;
//        this.accountsLedgerGroupBalance = accountsLedgergroupBalance;
//        this.accountsDayBook = accountsDayBook;
//        this.accountsJournal = accountsJournal;
//        this.accountsPayment = accountsPayment;
//        this.accountsCash = accountsCash;
//        this.accountsBank = accountsBank;
//        this.accountsTrailBalance = accountsTrailBalance;
//        this.accountsBalanceSheet =accountsBalanceSheet;
//    }
    public CompanyUserRole() {

    }

    public void makeAdmin() {
        Boolean isBOS = SystemPolicyUtil.getSystemPolicy().isBillOfSupply();
        Boolean isPlatinum = SystemPolicyUtil.getSystemPolicy().isPlatinum();

        this.productView = true;
        this.productCreate = true;
        this.productUpdate = true;
        this.productDelete = true;
        this.pricingPolicyView = true;
        this.pricingPolicyCreate = true;
        this.pricingPolicyUpdate = true;
        this.pricingPolicyDelete = true;
        this.partyView = true;
        this.partyCreate = true;
        this.partyUpdate = true;
        this.partyDelete = true;
        this.salesView = true;
        this.salesCreate = true;
        this.salesUpdate = true;
        this.salesDelete = true;
        this.bosView = isBOS;
        this.bosCreate = isBOS;
        this.bosUpdate = isBOS;
        this.bosDelete = isBOS;
        this.estimateView = true;
        this.estimateCreate = true;
        this.estimateUpdate = true;
        this.estimateDelete = true;
        this.purchaseView = true;
        this.purchaseCreate = true;
        this.purchaseUpdate = true;
        this.purchaseDelete = true;
        this.crdrNoteView = true;
        this.crdrNoteCreate = true;
        this.crdrNoteUpdate = true;
        this.crdrNoteDelete = true;
        this.inventoryView = true;
        this.inventoryAdd = true;
        this.inventoryDeduct = true;
        this.inventoryOpeningBalance = true;
        this.gstReports = true;
        this.ledgerCreate = true;
        this.ledgerView = true;
        this.ledgerUpdate = true;
        this.ledgerDelete = true;
        this.ledgerOpeningBalance = true;
        this.accountsLedgerGroupBalance = true;
        this.accountsDayBook = true;
        this.accountsJournal = true;
        this.accountsPayment = true;
        this.accountsCash = true;
        this.accountsBank = true;
        this.accountsTrailBalance = true;
        this.accountsBalanceSheet = isPlatinum;

    }

    @Override
    public String toString() {
        return "CompanyUserRole{" + "roleId=" + roleId + ", company=" + company + ", user=" + user + ", productView=" + productView + ", productCreate=" + productCreate + ", productUpdate=" + productUpdate + ", productDelete=" + productDelete + ", pricingPolicyView=" + pricingPolicyView + ", pricingPolicyCreate=" + pricingPolicyCreate + ", pricingPolicyUpdate=" + pricingPolicyUpdate + ", pricingPolicyDelete=" + pricingPolicyDelete + ", partyView=" + partyView + ", partyCreate=" + partyCreate + ", partyUpdate=" + partyUpdate + ", partyDelete=" + partyDelete + ", salesView=" + salesView + ", salesCreate=" + salesCreate + ", salesUpdate=" + salesUpdate + ", salesDelete=" + salesDelete + ", bosView=" + bosView + ", bosCreate=" + bosCreate + ", bosUpdate=" + bosUpdate + ", bosDelete=" + bosDelete + ", estimateView=" + estimateView + ", estimateCreate=" + estimateCreate + ", estimateUpdate=" + estimateUpdate + ", estimateDelete=" + estimateDelete + ", purchaseView=" + purchaseView + ", purchaseCreate=" + purchaseCreate + ", purchaseUpdate=" + purchaseUpdate + ", purchaseDelete=" + purchaseDelete + ", crdrNoteView=" + crdrNoteView + ", crdrNoteCreate=" + crdrNoteCreate + ", crdrNoteUpdate=" + crdrNoteUpdate + ", crdrNoteDelete=" + crdrNoteDelete + ", inventoryView=" + inventoryView + ", inventoryAdd=" + inventoryAdd + ", inventoryDeduct=" + inventoryDeduct + ", inventoryOpeningBalance=" + inventoryOpeningBalance + ", gstReports=" + gstReports + ", ledgerView=" + ledgerView + ", ledgerCreate=" + ledgerCreate + ", ledgerUpdate=" + ledgerUpdate + ", ledgerDelete=" + ledgerDelete + ", ledgerOpeningBalance=" + ledgerOpeningBalance + ", accountsLedgerGroupBalance=" + accountsLedgerGroupBalance + ", accountsDayBook=" + accountsDayBook + ", accountsJournal=" + accountsJournal + ", accountsPayment=" + accountsPayment + ", accountsCash=" + accountsCash + ", accountsBank=" + accountsBank + ", accountsTrailBalance=" + accountsTrailBalance + ", accountsBalanceSheet=" + accountsBalanceSheet + '}';
    }

}
