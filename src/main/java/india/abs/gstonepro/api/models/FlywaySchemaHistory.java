/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author SGS
 */
@Entity
@Table(name="flyway_schema_history")
public class FlywaySchemaHistory {

    @Id
    @Column(name = "installed_rank", nullable = false)
    private int installedRank;

    @Column(name = "version", nullable = false)
    private String version;

    @Column(name = "description", length = 200, nullable = false)
    private String description;

    @Column(name = "type", length = 20, nullable = false)
    private String type;

    @Column(name = "script", length = 1000, nullable = false)
    private String script;

    @Column(name = "checksum", nullable = false)
    private int checksum;

    @Column(name = "installed_by", length = 100, nullable = false)
    private String installedBy;

    @Column(name = "time_stamp", nullable = false)
    private Date timeStamp;

    @Column(name = "execution_time", nullable = false)
    private int executionTime;

    @Column(name = "success", nullable = false)
    private boolean success;

    public int getInstalledRank() {
        return installedRank;
    }

    public void setInstalledRank(int installedRank) {
        this.installedRank = installedRank;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public int getChecksum() {
        return checksum;
    }

    public void setChecksum(int checksum) {
        this.checksum = checksum;
    }

    public String getInstalledBy() {
        return installedBy;
    }

    public void setInstalledBy(String installedBy) {
        this.installedBy = installedBy;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(int executionTime) {
        this.executionTime = executionTime;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    
}
