/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Manjula
 */
public class GstReport {

    private String gstinOrUin;

    private String invoiceNumber;

    private Date invoiceDate;

    private BigDecimal invoiceValue;

    private String placeOfSupply;

    private String reverseCharge;

    private String invoiceType;

    private String eCommerceGSTIN;

    private BigDecimal gstRate;

    private BigDecimal taxableValue;

    private BigDecimal cessAmount;

    private String hsn;

    private String description;

    private String uqc;

    private BigDecimal totalQuantity;

    private BigDecimal totalValue;

    private BigDecimal integratedTaxAmount;

    private BigDecimal centralTaxAmount;

    private BigDecimal stateTaxAmount;

    private String noteType;

    private String noteNumber;

    private BigDecimal noteValue;

    private String noteReason;

    private Date noteDate;

    private String ITCeligible;

    private BigDecimal ITCIGST;

    private BigDecimal ITCSGST;

    private BigDecimal ITCCGST;

    private BigDecimal ITCCess;

    private String cdnNumber;

    public String getGstinOrUin() {
        return gstinOrUin;
    }

    public void setGstinOrUin(String gstinOrUin) {
        this.gstinOrUin = gstinOrUin;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public BigDecimal getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(BigDecimal invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getPlaceOfSupply() {
        return placeOfSupply;
    }

    public void setPlaceOfSupply(String placeOfSupply) {
        this.placeOfSupply = placeOfSupply;
    }

    public String getReverseCharge() {
        return reverseCharge;
    }

    public void setReverseCharge(String reverseCharge) {
        this.reverseCharge = reverseCharge;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String geteCommerceGSTIN() {
        return eCommerceGSTIN;
    }

    public void seteCommerceGSTIN(String eCommerceGSTIN) {
        this.eCommerceGSTIN = eCommerceGSTIN;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public BigDecimal getTaxableValue() {
        return taxableValue;
    }

    public void setTaxableValue(BigDecimal taxableValue) {
        this.taxableValue = taxableValue;
    }

    public BigDecimal getCessAmount() {
        return cessAmount;
    }

    public void setCessAmount(BigDecimal cessAmount) {
        this.cessAmount = cessAmount;
    }

    public String getHsn() {
        return hsn;
    }

    public void setHsn(String hsn) {
        this.hsn = hsn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUqc() {
        return uqc;
    }

    public void setUqc(String uqc) {
        this.uqc = uqc;
    }

    public BigDecimal getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(BigDecimal totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public BigDecimal getIntegratedTaxAmount() {
        return integratedTaxAmount;
    }

    public void setIntegratedTaxAmount(BigDecimal integratedTaxAmount) {
        this.integratedTaxAmount = integratedTaxAmount;
    }

    public BigDecimal getCentralTaxAmount() {
        return centralTaxAmount;
    }

    public void setCentralTaxAmount(BigDecimal centralTaxAmount) {
        this.centralTaxAmount = centralTaxAmount;
    }

    public BigDecimal getStateTaxAmount() {
        return stateTaxAmount;
    }

    public void setStateTaxAmount(BigDecimal stateTaxAmount) {
        this.stateTaxAmount = stateTaxAmount;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getNoteNumber() {
        return noteNumber;
    }

    public void setNoteNumber(String noteNumber) {
        this.noteNumber = noteNumber;
    }

    public BigDecimal getNoteValue() {
        return noteValue;
    }

    public void setNoteValue(BigDecimal noteValue) {
        this.noteValue = noteValue;
    }

    public String getNoteReason() {
        return noteReason;
    }

    public void setNoteReason(String noteReason) {
        this.noteReason = noteReason;
    }

    public Date getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public BigDecimal getITCIGST() {
        return ITCIGST;
    }

    public void setITCIGST(BigDecimal ITCIGST) {
        this.ITCIGST = ITCIGST;
    }

    public BigDecimal getITCSGST() {
        return ITCSGST;
    }

    public void setITCSGST(BigDecimal ITCSGST) {
        this.ITCSGST = ITCSGST;
    }

    public BigDecimal getITCCGST() {
        return ITCCGST;
    }

    public void setITCCGST(BigDecimal ITCCGST) {
        this.ITCCGST = ITCCGST;
    }

    public BigDecimal getITCCess() {
        return ITCCess;
    }

    public void setITCCess(BigDecimal ITCCess) {
        this.ITCCess = ITCCess;
    }

    public String getCdnNumber() {
        return cdnNumber;
    }

    public void setCdnNumber(String cdnNumber) {
        this.cdnNumber = cdnNumber;
    }

    public String getITCeligible() {
        return ITCeligible;
    }

    public void setITCeligible(String ITCeligible) {
        this.ITCeligible = ITCeligible;
    }

    
    @Override
    public String toString() {
        return "GstReport{" + "gstinOrUin=" + gstinOrUin + ", invoiceNumber=" + invoiceNumber + ", invoiceDate=" + invoiceDate + ", invoiceValue=" + invoiceValue + ", placeOfSupply=" + placeOfSupply + ", reverseCharge=" + reverseCharge + ", invoiceType=" + invoiceType + ", eCommerceGSTIN=" + eCommerceGSTIN + ", gstRate=" + gstRate + ", taxableValue=" + taxableValue + ", cessAmount=" + cessAmount + ", hsn=" + hsn + ", description=" + description + ", uqc=" + uqc + ", totalQuantity=" + totalQuantity + ", totalValue=" + totalValue + ", integratedTaxAmount=" + integratedTaxAmount + ", centralTaxAmount=" + centralTaxAmount + ", stateTaxAmount=" + stateTaxAmount + '}';
    }

}
