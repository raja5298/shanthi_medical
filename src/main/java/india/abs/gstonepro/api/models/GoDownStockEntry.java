/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findGoDownPurchaseStockEntry",
                    query = "from GoDownStockEntry g where g.pili.purchaseInvoice=:pi"
            )
            ,
            @NamedQuery(
                    name = "findGoDownBOSStockEntry",
                    query = "from GoDownStockEntry g where g.bosli.bos=:bos"
            )
            ,
            @NamedQuery(
                    name = "findGoDownSalesStockEntry",
                    query = "from GoDownStockEntry g where g.psli.purchaseSale=:ps"
            ),
            @NamedQuery(
                    name = "deleteGoDownSalesStockEntry",
                    query = "delete GoDownStockEntry g where g.psli.purchaseSale=:ps"
            )
            ,
            @NamedQuery(
                    name = "findGoDownCrDrStockEntry",
                    query = "from GoDownStockEntry g where g.cdnli.creditDebitNote=:cdn"
            )
            ,
            @NamedQuery(
                    name = "findGoDownProductStockEntry",
                    query = "from GoDownStockEntry g where product=:product and g.godown=:godown"
            ), @NamedQuery(
                    name = "findGoDownProductStockEntryByDateAndProduct",
                    query = "from GoDownStockEntry g where product=:product and  g.stockUpdateDate between :fromDate and :toDate"
            ),
                @NamedQuery(
                    name = "findGoDownProductStockEntryByDate",
                    query = "from GoDownStockEntry g where product=:product and g.godown=:godown and g.stockUpdateDate between :fromDate and :toDate"
            )
            ,
            @NamedQuery(
                    name = "findGoDownStockEntryTillDate",
                    query = "from GoDownStockEntry g where g.godown.company =:company and g.stockUpdateDate between :fromDate and :toDate"
            ),
            @NamedQuery(
                    name =  "findProductsSoldOut",
                    query = "from GoDownStockEntry g where g.godown.company=:company"
            )  
        })
public class GoDownStockEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "godown_product_stock_entry_id", updatable = false, nullable = false)
    private Long goDownProductStockEntryId;

    @ManyToOne
    @JoinColumn(name = "godown_id", nullable = false)
    private GoDown godown;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @Column(name = "quantity")
    private Long quantity;

    @OneToOne
    @JoinColumn(name = "psli_id")
    private PurchaseSaleLineItem psli;

    @OneToOne
    @JoinColumn(name = "pili_id")
    private PurchaseInvoiceLineItem pili;

    @OneToOne
    @JoinColumn(name = "cdnli_id")
    private CreditDebitNoteLineItem cdnli;
    
    @OneToOne
    @JoinColumn(name = "bosli_id")
    private BOSLineItem bosli;

    @Column(name = "stock_update_type")
    private String stockUpdateType;

    @Column(name = "stock_value")
    private BigDecimal stockValue;

    @Temporal(TemporalType.DATE)
    @Column(name = "stock_update_date")
    private Date stockUpdateDate;

    @Column(name = "igst_percentage")
    private float igstPercentage;

    @Column(name = "cgst_percentage")
    private float cgstPercentage;

    @Column(name = "sgst_percentage")
    private float sgstPercentage;

    @Column(name = "igst_amount")
    private BigDecimal igstAmount;

    @Column(name = "cgst_amount")
    private BigDecimal cgstAmount;

    @Column(name = "sgst_amount")
    private BigDecimal sgstAmount;

    @Embedded
    private Audit auditData;

    public Long getGoDownProductStockEntryId() {
        return goDownProductStockEntryId;
    }

    public void setGoDownProductStockEntryId(Long goDownProductStockEntryId) {
        this.goDownProductStockEntryId = goDownProductStockEntryId;
    }

    public GoDown getGodown() {
        return godown;
    }

    public void setGodown(GoDown godown) {
        this.godown = godown;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public PurchaseSaleLineItem getPsli() {
        return psli;
    }

    public void setPsli(PurchaseSaleLineItem psli) {
        this.psli = psli;
    }

    public String getStockUpdateType() {
        return stockUpdateType;
    }

    public void setStockUpdateType(String stockUpdateType) {
        this.stockUpdateType = stockUpdateType;
    }

    public BigDecimal getStockValue() {
        return stockValue;
    }

    public void setStockValue(BigDecimal stockValue) {
        this.stockValue = stockValue;
    }

    public Date getStockUpdateDate() {
        return stockUpdateDate;
    }

    public void setStockUpdateDate(Date stockUpdateDate) {
        this.stockUpdateDate = stockUpdateDate;
    }

    public float getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(float igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public float getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(float cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public float getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(float sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public BigDecimal getIgstAmount() {
        return igstAmount;
    }

    public void setIgstAmount(BigDecimal igstAmount) {
        this.igstAmount = igstAmount;
    }

    public BigDecimal getCgstAmount() {
        return cgstAmount;
    }

    public void setCgstAmount(BigDecimal cgstAmount) {
        this.cgstAmount = cgstAmount;
    }

    public BigDecimal getSgstAmount() {
        return sgstAmount;
    }

    public void setSgstAmount(BigDecimal sgstAmount) {
        this.sgstAmount = sgstAmount;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public PurchaseInvoiceLineItem getPili() {
        return pili;
    }

    public void setPili(PurchaseInvoiceLineItem pili) {
        this.pili = pili;
    }

    public CreditDebitNoteLineItem getCdnli() {
        return cdnli;
    }

    public void setCdnli(CreditDebitNoteLineItem cdnli) {
        this.cdnli = cdnli;
    }

    public BOSLineItem getBosli() {
        return bosli;
    }

    public void setBosli(BOSLineItem bosli) {
        this.bosli = bosli;
    }

    
    
}
