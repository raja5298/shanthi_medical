/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author SGS
 */
@Entity
@NamedQueries(
        {
            @NamedQuery(
                    name = "findBOS",
                    query = "from BillOfSupply b where b.bosDate between :fromDate and :toDate and b.company=:company and b.cancelled=:withCancelledBill"
            ),
            @NamedQuery(
                    name = "findBOSWithCancelled",
                    query = "from BillOfSupply b where b.bosDate between :fromDate and :toDate and b.company=:company"
            )
            ,
            @NamedQuery(
                    name = "findBOSByParty",
                    query = "from BillOfSupply b where b.bosDate between :fromDate and :toDate and b.company=:company and b.cancelled=:withCancelledBill and b.ledger=:ledger"
            )
            ,
            @NamedQuery(
                    name = "findBOSAuditData",
                    query = "from BillOfSupply b where b.auditData.createdOn between :fromDate and :toDate"
            )
            ,
                 @NamedQuery(
                    name = "findBOSAuditDataByUser",
                    query = "from BillOfSupply b where b.auditData.createdOn between :fromDate and :toDate and auditData.createdBy = :user"
            )

        }
)
public class BillOfSupply {

    @Id
    @Column(name = "bos_id", updatable = false, nullable = false)
    private String bosId;

    @Temporal(TemporalType.DATE)
    @Column(name = "bos_date")
    private Date bosDate;

    @Column(name = "bos_no")
    private String bosNo;

    @Column(name = "bill_amount")
    private BigDecimal billAmount;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private Ledger ledger;

    @Column(name = "mode_of_delivery")
    private String modeOfDelivery;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "discount_percent")
    private float discountPercent;

    @Column(name = "discount_amount")
    private BigDecimal discountAmount;

    //Difference between Bill Amount & Calculated Total Amount
    @Column(name = "variation_amount")
    private BigDecimal variationAmount;

    //Original Bill Amount
    @Column(name = "calculated_total_value")
    private BigDecimal calculatedTotalValue;

    //Cash or Credit
    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "total_amount_paid")
    private BigDecimal totalAmountPaid;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "round_off_value")
    private BigDecimal roundOffValue;

    @Column(name = "cancelled")
    private boolean cancelled;

    @Column(name = "profit")
    private BigDecimal profit;

    @Column(name = "sms_message")
    private String smsMessage;

    @Column(name = "pricing_policy_name")
    private String pricingPolicyName;

    @Transient
    @Column(name = "current_payment_amount")
    private BigDecimal currentPaymentAmount;

    @OneToOne
    @JoinColumn(name = "journal_id")
    private Journal bosJournal;

    @Embedded
    private Audit auditData;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "billOfSupply", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<BillOfSupplyLineItem> bosLineItems = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "billOfSupply", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<BillOfSupplyChargeItem> bosChargeItems = new ArrayList<>();

    public String getBosId() {
        return bosId;
    }

    public void setBosId(String bosId) {
        this.bosId = bosId;
    }

    public Date getBosDate() {
        return bosDate;
    }

    public void setBosDate(Date bosDate) {
        this.bosDate = bosDate;
    }

    public String getBosNo() {
        return bosNo;
    }

    public void setBosNo(String bosNo) {
        this.bosNo = bosNo;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    public String getModeOfDelivery() {
        return modeOfDelivery;
    }

    public void setModeOfDelivery(String modeOfDelivery) {
        this.modeOfDelivery = modeOfDelivery;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public float getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getVariationAmount() {
        return variationAmount;
    }

    public void setVariationAmount(BigDecimal variationAmount) {
        this.variationAmount = variationAmount;
    }

    public BigDecimal getCalculatedTotalValue() {
        return calculatedTotalValue;
    }

    public void setCalculatedTotalValue(BigDecimal calculatedTotalValue) {
        this.calculatedTotalValue = calculatedTotalValue;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getRoundOffValue() {
        return roundOffValue;
    }

    public void setRoundOffValue(BigDecimal roundOffValue) {
        this.roundOffValue = roundOffValue;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getPricingPolicyName() {
        return pricingPolicyName;
    }

    public void setPricingPolicyName(String pricingPolicyName) {
        this.pricingPolicyName = pricingPolicyName;
    }

    public BigDecimal getCurrentPaymentAmount() {
        return currentPaymentAmount;
    }

    public void setCurrentPaymentAmount(BigDecimal currentPaymentAmount) {
        this.currentPaymentAmount = currentPaymentAmount;
    }

    public Journal getBosJournal() {
        return bosJournal;
    }

    public void setBosJournal(Journal bosJournal) {
        this.bosJournal = bosJournal;
    }

    public Audit getAuditData() {
        return auditData;
    }

    public void setAuditData(Audit auditData) {
        this.auditData = auditData;
    }

    public List<BillOfSupplyLineItem> getBosLineItems() {
        return bosLineItems;
    }

    public void setBosLineItems(List<BillOfSupplyLineItem> bosLineItems) {
        this.bosLineItems = bosLineItems;
    }

    public List<BillOfSupplyChargeItem> getBosChargeItems() {
        return bosChargeItems;
    }

    public void setBosChargeItems(List<BillOfSupplyChargeItem> bosChargeItems) {
        this.bosChargeItems = bosChargeItems;
    }

//    @Override
//    public String toString() {
//        return "BillOfSupply{" + "bosId=" + bosId + ", bosDate=" + bosDate + ", bosNo=" + bosNo + ", billAmount=" + billAmount + ", company=" + company + ", ledger=" + ledger + ", modeOfDelivery=" + modeOfDelivery + ", remarks=" + remarks + ", discountPercent=" + discountPercent + ", discountAmount=" + discountAmount + ", variationAmount=" + variationAmount + ", calculatedTotalValue=" + calculatedTotalValue + ", paymentType=" + paymentType + ", totalAmountPaid=" + totalAmountPaid + ", referenceNumber=" + referenceNumber + ", roundOffValue=" + roundOffValue + ", cancelled=" + cancelled + ", profit=" + profit + ", smsMessage=" + smsMessage + ", pricingPolicyName=" + pricingPolicyName + ", currentPaymentAmount=" + currentPaymentAmount + ", bosJournal=" + bosJournal + ", auditData=" + auditData + ", bosLineItems=" + bosLineItems + ", bosChargeItems=" + bosChargeItems + '}';
//    }
}
