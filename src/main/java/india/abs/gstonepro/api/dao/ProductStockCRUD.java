///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package india.abs.gstonepro.api.dao;
//
//import india.abs.gstonepro.api.models.Company;
//import india.abs.gstonepro.api.models.GoDownStockDetail;
//import india.abs.gstonepro.api.models.GoDownStockEntry;
//import india.abs.gstonepro.api.models.Journal;
//import india.abs.gstonepro.api.models.Product;
//import india.abs.gstonepro.api.models.ProductStockEntry;
//import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
//import india.abs.gstonepro.api.models.Transaction;
//import india.abs.gstonepro.api.utils.AuditDataUtil;
//import india.abs.gstonepro.api.utils.EventStatus;
//import india.abs.gstonepro.api.utils.HibernateUtil;
//import india.abs.gstonepro.api.utils.models.StockReportModel;
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import org.hibernate.HibernateException;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.hibernate.resource.transaction.spi.TransactionStatus;
//
///**
// *
// * @author SGS
// */
//public class ProductStockCRUD {
//
//    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
//
//    public ProductStockEntry changeProductStock(Session passedSession, Product pro, int stocktQuantity, BigDecimal stockValue, Date stockUpdateDate, String updateType, float igstP, float cgstP, float sgstP, BigDecimal igstValue, BigDecimal cgstValue, BigDecimal sgstValue, PurchaseSaleLineItem newPSLI) {
//        EventStatus result = new EventStatus();
//        boolean sessionAvailable = (passedSession != null);
//        Session session = null;
//        if (sessionAvailable) {
//            session = passedSession;
//        } else {
//            session = FACTORY.openSession();
//
//        }
//        ProductStockEntry psStock = null;
//        try {
//            if (!sessionAvailable) {
//                session.beginTransaction();
//            }
//
//            ProductStockEntry newPStock = new ProductStockEntry();
//            Product product = (Product) session.get(Product.class, pro.getProductId());
//            int currentStock = product.getCurrentStock() + stocktQuantity;
//            
//           
//
//            newPStock.setProduct(pro);
//            newPStock.setStockUpdateDate(stockUpdateDate);
//            newPStock.setQuantity(stocktQuantity);
//            newPStock.setStockValue(stockValue);
//            newPStock.setStockUpdateType(updateType);
//            newPStock.setCgstAmount(cgstValue);
//            newPStock.setCgstPercentage(cgstP);
//            newPStock.setIgstAmount(igstValue);
//            newPStock.setIgstPercentage(igstP);
//            newPStock.setSgstAmount(sgstValue);
//            newPStock.setSgstPercentage(sgstP);
//            newPStock.setPsli(newPSLI);
//
//            newPStock.setAuditData(AuditDataUtil.getAuditForCreate());
//            newPStock.setCompany(pro.getCompany());
//
//            session.save(newPStock);
//            product.setCurrentStock(currentStock);
//            session.save(product);
//
//            if (!sessionAvailable) {
//                session.getTransaction().commit();
//            }
//
//            psStock = newPStock;
//            return psStock;
//
//        } catch (HibernateException hx) {
//
//            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
//                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
//                session.getTransaction().rollback();
//                return psStock;
//            }
//
//        } finally {
//            if (!sessionAvailable) {
//                session.close();
//            }
//        }
//        return psStock;
//
//    }
//
//    public EventStatus updateProductStockEntry(Long productId, ProductStockEntry updatedStockEntry) {
//
//        EventStatus createProduct = new EventStatus();
//        try (Session session = FACTORY.openSession()) {
//            session.beginTransaction();
//            Product product = (Product) session.get(Product.class, productId);
//            ProductStockEntry updatedStock = (ProductStockEntry) session.get(ProductStockEntry.class, updatedStockEntry.getProductStockEntryId());
//
//            updatedStock.setQuantity(updatedStockEntry.getQuantity());
//            updatedStock.setStockValue(updatedStockEntry.getStockValue());
//            updatedStock.setStockUpdateType(updatedStockEntry.getStockUpdateType());
//            updatedStock.setStockUpdateDate(updatedStockEntry.getStockUpdateDate());
//            updatedStock.setCgstAmount(updatedStockEntry.getCgstAmount());
//            updatedStock.setCgstPercentage(updatedStockEntry.getCgstPercentage());
//            updatedStock.setIgstAmount(updatedStockEntry.getIgstAmount());
//            updatedStock.setIgstPercentage(updatedStockEntry.getIgstPercentage());
//            updatedStock.setSgstAmount(updatedStockEntry.getSgstAmount());
//            updatedStock.setSgstPercentage(updatedStockEntry.getSgstPercentage());
//            updatedStock.setProduct(product);
//            session.save(updatedStock);
//
//            session.getTransaction().commit();
//            createProduct.setUpdateDone(true);
//            session.close();
//        }
//
//        return createProduct;
//    }
//
//    public EventStatus manualStockUpdate(Set<ProductStockEntry> stockUpdates, String message, Date updateDate, Company company) {
//
//        EventStatus result = new EventStatus();
//        Session session = FACTORY.openSession();
//        try {
//
//            session.beginTransaction();
//            for (ProductStockEntry ps : stockUpdates) {
//
//                Product product = (Product) session.get(Product.class, ps.getProduct().getProductId());
//                int currentStock = product.getCurrentStock() + ps.getQuantity();
//                product.setCurrentStock(currentStock);
//                session.save(product);
//
//                ProductStockEntry newPS = new ProductStockEntry();
//
//                newPS.setProduct(product);
//                newPS.setCompany(company);
//
//                newPS.setQuantity(ps.getQuantity());
//                newPS.setStockValue(ps.getStockValue());
//                newPS.setStockUpdateDate(updateDate);
//                newPS.setStockUpdateType("MANUAL-STOCK-UPDATE");
//
//                newPS.setCgstPercentage(ps.getCgstPercentage());
//                newPS.setCgstAmount(ps.getCgstAmount());
//                newPS.setSgstPercentage(ps.getSgstPercentage());
//                newPS.setSgstAmount(ps.getSgstAmount());
//                newPS.setIgstPercentage(ps.getIgstPercentage());
//                newPS.setIgstAmount(ps.getIgstAmount());
//
//                session.save(newPS);
//
//            }
//            Journal newJournal = new Journal();
//            LedgerCRUD l4 = new LedgerCRUD();
//            Transaction transactionOne = new Transaction();
//
//            transactionOne.setLedger(l4.fetchAGeneralLedger(company, "Stock Adjustment"));
//            transactionOne.setEditable(false);
//            transactionOne.setType("MANUAL-STOCK-UPDATE");
//            transactionOne.setAmount(new BigDecimal(0));
//            transactionOne.setParticulars(message);
//            transactionOne.setTransactionDate(updateDate);
//            transactionOne.setCompany(company);
//            session.save(transactionOne);
//            newJournal.setJournalId(company.getCompanyId() + "MANUAL-STOCK-UPDATE" + transactionOne.getTransactionId());
//            newJournal.getTransactions().add(transactionOne);
//
//            session.save(newJournal);
//
//            session.getTransaction().commit();
//
//            session.close();
//            result.setUpdateDone(true);
//
//            return result;
//        } catch (HibernateException hx) {
//
//            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
//                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
//                session.getTransaction().rollback();
//            }
//
//        } finally {
//
//            session.close();
//        }
//
//        return result;
//    }
//
//    public Map<Product, StockReportModel> closingStockReport(Date stockDate, Company company) {

//
//        List<ProductStockEntry> pss = null;
//        Map<Product, StockReportModel> stockReport = new HashMap();
//        Session session = FACTORY.openSession();
//        try {
//            session.beginTransaction();
////
////            //Hibernate Named Query  
////            Query query = session.getNamedQuery("getAllStockTypesTillDate");
////            query.setParameter("company", company);
////            query.setParameter("tillDate", stockDate);
////
////            pss = query.list();
////
////            for (ProductStockEntry ps : pss) {
////                
////                if (stockReport.containsKey(ps.getProduct())) {
////                    StockReportModel srm = stockReport.get(ps.getProduct());
////                    srm.setReportDate(ps.getStockUpdateDate());
////                    int cs = srm.getClosingStock() + ps.getQuantity();
////                    
////                    srm.setClosingStock(cs);
////                } else {
////
////                    StockReportModel srm = new StockReportModel();
////                    srm.setReportDate(ps.getStockUpdateDate());
////                    int cs = srm.getClosingStock() + ps.getQuantity();
////                    
////                    srm.setClosingStock(cs);
////                    stockReport.put(ps.getProduct(), srm);
////                }
////
//    //        }
//
//            session.getTransaction().commit();
//
//            session.close();
//            return stockReport;
//        } catch (HibernateException hx) {
//
//            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
//                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
//                session.getTransaction().rollback();
//            }
//
//        } finally {
//            session.close();
//        }
//        return stockReport;
//
//    }
//
//    public Set<StockReportModel> productStockReport(Company company, Product product, Date fromDate, Date toDate) {
//
//        List<ProductStockEntry> pss = null;
//        Map<Date, StockReportModel> stockReport = new HashMap();
//        Session session = FACTORY.openSession();
//        Set<StockReportModel> productStocks = new HashSet();
//        try {
//            session.beginTransaction();
//            Query query = session.getNamedQuery("getProductStock");
//            query.setParameter("company", company);
//            query.setParameter("product", product);
//
//            pss = query.list();
//
//            for (ProductStockEntry ps : pss) {
//                
//                Date psDate = ps.getStockUpdateDate();
//                boolean isDateAfterFromDate = psDate.compareTo(fromDate) >= 0;
//                boolean isDateBeforeDate = psDate.compareTo(toDate) <= 0;
//
//                if (isDateAfterFromDate && isDateBeforeDate) {
//                    if (stockReport.containsKey(psDate)) {
//                        StockReportModel srm = stockReport.get(psDate);
//                        int cs = srm.getClosingStock() + ps.getQuantity();
//                        if (ps.getQuantity() < 0) {
//                            srm.setStockOut(srm.getStockOut() + ps.getQuantity());
//                        } else {
//                            srm.setStockIn(srm.getStockIn() + ps.getQuantity());
//                        }
//                        srm.setClosingStock(cs);
//                        stockReport.put(psDate, srm);
//                    } else {
//                        StockReportModel srm = new StockReportModel();
//                        int cs = srm.getClosingStock() + ps.getQuantity();
//                        if (ps.getQuantity() < 0) {
//                            srm.setStockOut(srm.getStockOut() + ps.getQuantity());
//                        } else {
//                            srm.setStockIn(srm.getStockIn() + ps.getQuantity());
//                        }
//                        srm.setClosingStock(cs);
//                        srm.setReportDate(psDate);
//                        stockReport.put(psDate, srm);
//                    }
//                }
//            }
//            for (Date psDate : stockReport.keySet()) {
//                StockReportModel srm = stockReport.get(psDate);
//                
//                productStocks.add(srm);
//            }
//            session.getTransaction().commit();
//
//            session.close();
//            return productStocks;
//        } catch (HibernateException hx) {
//
//            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
//                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
//                session.getTransaction().rollback();
//            }
//
//        } finally {
//            session.close();
//        }
//        return productStocks;
//
//    }
//
//}
