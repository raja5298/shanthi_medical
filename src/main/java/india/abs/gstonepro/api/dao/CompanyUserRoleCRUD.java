/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CompanyUserRoleCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<CompanyUserRole> fetchCURByUser(AppUser user) {

        List<CompanyUserRole> curs;
        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findCompanyUserRoleByUser");
        query.setParameter("user", user);

        curs = query.list();
        session.getTransaction().commit();

        session.close();

        return curs;
    }

    public CompanyUserRole fetchCURByUserAndCompany(Company company, AppUser user) {
        
        CompanyUserRole cur;
        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findCompanyUserRoleByUserAndCompany");
        query.setParameter("company", company);
        query.setParameter("user", user);

        cur = (CompanyUserRole) query.getSingleResult();
        session.getTransaction().commit();

        session.close();

        return cur;
    }

    public EventStatus updateCUR(CompanyUserRole updatedCUR) {
        
       
        EventStatus updateCUR = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(updatedCUR);
            session.getTransaction().commit();
            session.close();
            updateCUR.setUpdateDone(true);
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();

        }

        return updateCUR;

    }

}
