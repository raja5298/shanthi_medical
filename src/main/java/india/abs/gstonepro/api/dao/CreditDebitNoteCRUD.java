package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.SystemPolicy;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/*
 *
 * @author Raja
 */
public class CreditDebitNoteCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    JournalCRUD j4 = new JournalCRUD();

    public EventStatus createCDN(Session passedSession, Company company, CreditDebitNote newCDN, List<CreditDebitNoteLineItem> cdLinLineItems) {
        EventStatus result = new EventStatus();

        GoDownStockCRUD gos4 = new GoDownStockCRUD();
        Audit ad = new Audit();
        ad.setCreatedBy(SessionDataUtil.getSelectedUser());
        ad.setCreatedOn(new Date());

        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }

        try {
            Audit piAudit = new Audit();
            if (!sessionAvailable) {
                session.beginTransaction();

                Company co = (Company) session.get(Company.class, company.getCompanyId());

                newCDN.setCompany(co);

                Long cdnCount = co.getCompanyBook().getCdNoteCount() + 1;
                newCDN.setCdNoteNo(co.getCompanyPolicy().getCrDrPrefix() + cdnCount + co.getCompanyPolicy().getCrDrSuffix());

                Long cdnIdCount = co.getCompanyBook().getCdNoteIdCount() + 1;
                newCDN.setCdNoteId(co.getCompanyId() + "CDN" + cdnIdCount);

                co.getCompanyBook().setCdNoteCount(cdnCount);
                co.getCompanyBook().setCdNoteIdCount(cdnIdCount);
                session.save(co);

                piAudit = AuditDataUtil.getAuditForCreate();

            } else {

                Audit temp = AuditDataUtil.getAuditForUpdate();
//                piAudit.setCreatedBy(newPI.getAuditData().getCreatedBy());
//                piAudit.setCreatedOn(newPI.getAuditData().getCreatedOn());
                piAudit.setUpdatedBy(temp.getUpdatedBy());
                piAudit.setUpdatedOn(temp.getUpdatedOn());
            }
            newCDN.setAuditData(piAudit);
            newCDN.setCdLis(new ArrayList<>());
            session.save(newCDN);

            for (CreditDebitNoteLineItem cdLi : cdLinLineItems) {
                Long stockQuantityToChange = 0L;
//                int uqc2Value = cdLi.getProduct().getUQC2Value();
                int totalStockInUQC2 = cdLi.getStockQuantity();
                stockQuantityToChange = new Long(totalStockInUQC2);

                cdLi.setAuditData(piAudit);

                session.save(cdLi);

                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {

                    GoDownStockEntry newGStock = gos4.changeProductStock(session, cdLi.getGodown(), cdLi.getProduct(), stockQuantityToChange, cdLi.getValue(), cdLi.getInvoiceDate(), newCDN.getNoteType(), cdLi.getIgstPercentage(), cdLi.getCgstPercentage(), cdLi.getSgstPercentage(), cdLi.getIgstValue(), cdLi.getCgstValue(), cdLi.getSgstValue(), null, null, cdLi, null);
                    if (newGStock == null) {
                        throw new HibernateException("Product Stock Update Failed during Credit and Debit Note");
                    }
                    cdLi.setGodownStockEntry(newGStock);

                }
                cdLi.setCreditDebitNote(newCDN);

                newCDN.getCdLis().add(cdLi);
                session.save(cdLi);
            }

            if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {

                Journal crDrJournal = j4.createACrDrNoteJournal(session, newCDN.getCompany(), newCDN.getLedger(), newCDN.getCdNoteNo(), newCDN.getNoteType(), newCDN.getNoteValueInclusiveOfTax(), newCDN.getNoteDate());
                if (crDrJournal == null) {
                    throw new HibernateException("Accounts Failed");
                }
                newCDN.setCrDrJournal(crDrJournal);
            }

            session.save(newCDN);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setCreateDone(true);

            result.setBillID(newCDN.getCdNoteId());

            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception ex) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
            }

        } finally {

            if (!sessionAvailable) {
                session.close();
            }
        }
        return result;
    }

    public List<CreditDebitNote> getAllCDNoteByDate(String type, Date fromDate, Date toDate, Company selectedCompany) {
        List<CreditDebitNote> creditNotes = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findCDNByDate");
            query.setParameter("company", selectedCompany);
            query.setParameter("type", type);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            creditNotes = query.list();
            session.getTransaction().commit();
            return creditNotes;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

                return creditNotes;
            }

        } finally {

            session.close();
            return creditNotes;
        }
    }

    public List<CreditDebitNote> getAllCDNoteByLedger(Ledger ledger, String type, Date fromDate, Date toDate, Company selectedCompany) {
        List<CreditDebitNote> creditNotes = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findCDNByLedger");
            query.setParameter("company", selectedCompany);
            query.setParameter("type", type);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("ledger", ledger);

            creditNotes = query.list();
            session.getTransaction().commit();

            return creditNotes;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return creditNotes;
            }

        } finally {
            session.close();
            return creditNotes;
        }
    }

    public CreditDebitNote getCDNDetails(String cdLinId) {

        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            CreditDebitNote cdn = (CreditDebitNote) session.get(CreditDebitNote.class, cdLinId);
            List<CreditDebitNoteLineItem> cdLis = null;

            Query cdLiQuery = session.getNamedQuery("findAllCDNLinetems");

            cdLiQuery.setParameter("cdNote", cdn);

            cdLis = cdLiQuery.list();

            cdn.setCdLis(cdLis);
            session.getTransaction().commit();
            session.close();
            return cdn;
        }
    }

    public EventStatus deleteCreditDebitNote(Session passedSession, String cdLinId) {
        EventStatus result = new EventStatus();
        GoDownStockCRUD gos4 = new GoDownStockCRUD();
        boolean sessionAvailable = (passedSession != null);
        String errorMessage = "Name\t\tCurrent Stock\t\tStock in Purchase\t\tResult";
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            boolean canDeleteCDN = true;
            CreditDebitNote cdLin = (CreditDebitNote) session.get(CreditDebitNote.class, cdLinId);
            Hibernate.initialize(cdLin.getCdLis());
            for (CreditDebitNoteLineItem cdLili : cdLin.getCdLis()) {
//                if (cdLin.getNoteType().equalsIgnoreCase("DEBIT-NOTE")) {
                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    GoDownStockDetail gsd = gos4.getGoDownStockDetail(cdLili.getProduct().getProductId(), cdLili.getGodownStockEntry().getGodown().getGoDownId());
                    Long stockAfterDeduction = gsd.getCurrentStock() - cdLili.getStockQuantity();
                    if (stockAfterDeduction < 0) {
                        canDeleteCDN = false;
                        errorMessage += cdLili.getProduct().getName() + "\t" + gsd.getCurrentStock() + "\t" + "(-)" + cdLili.getStockQuantity() + "\t" + stockAfterDeduction;
                    } else {
                        EventStatus removeStockEntries = gos4.removeCrDrNoteEntries(session, cdLin);
                    }
                } else {
                    EventStatus removeStockEntries = gos4.removeCrDrNoteEntries(session, cdLin);
                }
//                }
            }
            if (canDeleteCDN) {
                if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {
                    EventStatus deleteJournalResult = j4.deleteACrDrJournal(session, cdLin);
                    if (!deleteJournalResult.isDeleteDone()) {
                        throw new HibernateException("Journal Delete Failed");
                    }
                }
                session.delete(cdLin);

                if (!sessionAvailable) {
                    session.getTransaction().commit();
                }
                result.setDeleteDone(true);

                return result;
            } else {
                result.setMessage(errorMessage);

                return result;
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                result.setMessage("Cannot Update");

                return result;
            }

        } finally {
            if (!sessionAvailable) {

                session.close();
            }
        }

        return result;
    }

    public EventStatus updateCreditDedbitNote(Company company, CreditDebitNote updatedCDN, List<CreditDebitNoteLineItem> cdnLis) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();

        try {
            session.getTransaction().begin();
            EventStatus deleteResult = deleteCreditDebitNote(session, updatedCDN.getCdNoteId());

            if (!deleteResult.isDeleteDone()) {
                result.setMessage(deleteResult.getMessage());
                return result;
            }
            EventStatus createResult = createCDN(session, company, updatedCDN, cdnLis);

            session.getTransaction().commit();
            if (deleteResult.isDeleteDone() && createResult.isCreateDone()) {
                result.setUpdateDone(true);

                return result;
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();

                return result;
            }

        } catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();

                return result;
            }

        } finally {
            session.close();
        }

        return result;

    }

    public List<CreditDebitNote> getCDByAudit(String type, Date fromDate, Date toDate) {
        List<CreditDebitNote> cdnNotes = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findCDNByAudit");
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        query.setParameter("company", SessionDataUtil.getSelectedCompany());

        cdnNotes = query.list();
        session.getTransaction().commit();

        session.close();

        return cdnNotes;

    }

    public List<CreditDebitNote> getCDNoteByAuditData(String type, Date fromDate, Date toDate) {
        List<CreditDebitNote> cdnNotes = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findCDNByAuditData");
        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        cdnNotes = query.list();
        session.getTransaction().commit();

        session.close();

        return cdnNotes;

    }

//    public List<CreditDebitNote> getCreditDebitByAllAudits(String type,Date fromDate, Date toDate) {
//       
//    }
//    public List<CreditDebitNote> getCreditDebitByAudit(AppUser audit,String type,Date fromDate, Date toDate) {
//        
//    }
    public List<CreditDebitNoteLineItem> getAllCreditDebitNoteByProduct(String type, Date fromDate, Date toDate, Product product) {
        List<CreditDebitNoteLineItem> cdnNoteLineItems = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findCDNByProduct");
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("product", product);

        cdnNoteLineItems = query.list();
        session.getTransaction().commit();

        session.close();

        return cdnNoteLineItems;

    }

    public List<CreditDebitNote> getAllCreditDebitNoteByType(String type, Ledger ledger) {
        List<CreditDebitNote> cdNotes = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        Query query = null;
        if (ledger != null) {

            query = session.getNamedQuery("findAllCDNByType");
            query.setParameter("noteType", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("ledger", ledger);
        } else {
            query = session.getNamedQuery("findAllCDNByTypeWithOutLedger");
            query.setParameter("noteType", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
//            query.setParameter("ledger", ledger);
        }
        cdNotes = query.list();
        session.getTransaction().commit();

        session.close();

        return cdNotes;
    }
}
