/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.utils.models.StockReportModel;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class GoDownStockCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public GoDownStockEntry changeProductStock(Session passedSession, GoDown go, Product pro, Long stockQuantity, BigDecimal stockValue, Date stockUpdateDate, String updateType, float igstP, float cgstP, float sgstP, BigDecimal igstValue, BigDecimal cgstValue, BigDecimal sgstValue, PurchaseSaleLineItem newPSLI, PurchaseInvoiceLineItem newPILI, CreditDebitNoteLineItem newCDNLI, BOSLineItem newBOSLI) {

        EventStatus result = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        GoDownStockEntry goDownStockEntry = null;
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        GoDownStockEntry goDownStock = null;
        GoDownStockDetail gsd = null;
        try {

            if (!sessionAvailable) {
                session.beginTransaction();
            }

            GoDownStockEntry newGSEntry = new GoDownStockEntry();
            GoDownStockDetail gStockDetail = null;
            if (newPSLI != null || newPILI != null || newCDNLI != null || newBOSLI != null) {
                gStockDetail = getGoDownStockDetail(pro.getProductId(), go.getGoDownId());
                gsd = session.get(GoDownStockDetail.class, gStockDetail.getGoDownStockDetailId());

            } else {
                gStockDetail = new GoDownStockDetail();
                gStockDetail.setProduct(pro);
                gStockDetail.setGodown(go);
            }
            Long currentStock = 0L;
            if (updateType.equalsIgnoreCase("OPENING")) {
                currentStock = stockQuantity;
                gStockDetail.setOpeningStockDate(stockUpdateDate);
                gStockDetail.setOpeningStock(stockQuantity);
                gStockDetail.setOpeningStockValue(stockValue);

            } else {
                currentStock = (gsd.getCurrentStock() + stockQuantity);
            }

            newGSEntry.setProduct(pro);

            newGSEntry.setStockUpdateDate(stockUpdateDate);
            newGSEntry.setQuantity(stockQuantity);
            newGSEntry.setStockValue(stockValue);
            newGSEntry.setStockUpdateType(updateType);
            newGSEntry.setCgstAmount(cgstValue);
            newGSEntry.setCgstPercentage(cgstP);
            newGSEntry.setIgstAmount(igstValue);
            newGSEntry.setIgstPercentage(igstP);
            newGSEntry.setSgstAmount(sgstValue);
            newGSEntry.setSgstPercentage(sgstP);
            newGSEntry.setGodown(go);
            if (newPSLI != null) {
                newGSEntry.setPsli(newPSLI);
            } else if (newPILI != null) {

                newGSEntry.setPili(newPILI);

            } else if (newCDNLI != null) {
                newGSEntry.setCdnli(newCDNLI);
            } else if (newBOSLI != null) {
                newGSEntry.setBosli(newBOSLI);
            }

            newGSEntry.setAuditData(AuditDataUtil.getAuditForCreate());
            // newGSEntry.setCompany(pro.getCompany());

            session.save(newGSEntry);

            if (updateType.equalsIgnoreCase("OPENING")) {
                gStockDetail.setCurrentStock(currentStock);
                session.save(gStockDetail);
            } else {

                gsd.setCurrentStock(currentStock);
                session.update(gsd);

            }

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }

            goDownStockEntry = newGSEntry;
            return goDownStockEntry;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                return goDownStockEntry;
            }

        } catch (Exception ex) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }
        }
        return goDownStockEntry;

    }

    public GoDownStockDetail getGoDownStockDetail(Long productId, Long goDownId) {
        Product pro = new Product();
        pro.setProductId(productId);
        GoDown go = new GoDown();
        go.setGoDownId(goDownId);
        GoDownStockDetail gsd = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findGoDownsProductDetail");
            query.setParameter("product", pro);
            query.setParameter("godown", go);
            gsd = (GoDownStockDetail) query.getSingleResult();
            session.getTransaction().commit();
            return gsd;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                return gsd;
            }

        } finally {

            session.close();

        }
        return gsd;

    }

    public List<GoDownStockDetail> fetchAllOpeningStocks(Long productId) {

        List<GoDownStockDetail> opgsds = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllOpeningStocks");
            Product pro = (Product) session.get(Product.class, productId);
            query.setParameter("product", pro);

            opgsds = query.list();
            session.getTransaction().commit();
            return opgsds;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return opgsds;
            }

        } finally {

            session.close();
            return opgsds;

        }

    }

    public EventStatus updateOpeningStock(GoDownStockDetail gsd) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findGoDownProductStockEntryByDate");

            query.setParameter("product", gsd.getProduct());
            query.setParameter("godown", gsd.getGodown());
            query.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            query.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());

            List<GoDownStockEntry> gses = query.list();
            if (gses.size() == 1) {

                GoDownStockDetail egsd = session.get(GoDownStockDetail.class, gsd.getGoDownStockDetailId());
                egsd.setOpeningStockDate(gsd.getOpeningStockDate());
                egsd.setOpeningStock(gsd.getOpeningStock());
                egsd.setOpeningStockValue(gsd.getOpeningStockValue());
                egsd.setCurrentStock(gsd.getOpeningStock());

                GoDownStockEntry gse = gses.get(0);
                gse.setStockUpdateDate(gsd.getOpeningStockDate());
                gse.setQuantity(gsd.getOpeningStock());
                gse.setStockValue(gsd.getOpeningStockValue());
                session.save(gse);

                session.save(egsd);

            } else if (gses.size() > 1) {
                result.setMessage("Please remove any Sales or Purchases before editing Opening Stock");
                return result;
            }
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return result;
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {

            session.close();
            return result;

        }

    }

    public List<GoDownStockDetail> fetchProductStockDetailsByGoDown(Long godownId) {
        List<GoDownStockDetail> opgsds = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllProductGSD");
            GoDown god = new GoDown();
            god.setGoDownId(godownId);
            query.setParameter("godown", god);

            opgsds = query.list();
            session.getTransaction().commit();
            return opgsds;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return opgsds;
            }

        } finally {

            session.close();
            return opgsds;

        }
    }

    public EventStatus removePurchaseStockEntries(Session passedSession, PurchaseInvoice pi) {
        EventStatus result = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        List<GoDownStockEntry> gses;
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Query query = null;

            query = session.getNamedQuery("findGoDownPurchaseStockEntry");
            query.setParameter("pi", pi);

            gses = query.list();
            for (GoDownStockEntry gse : gses) {
                Query gsdQuery = session.getNamedQuery("findGoDownsProductDetail");
                gsdQuery.setParameter("godown", gse.getGodown());
                gsdQuery.setParameter("product", gse.getProduct());
                GoDownStockDetail gsd = (GoDownStockDetail) gsdQuery.getSingleResult();

                Long updatedStock = gsd.getCurrentStock() - gse.getQuantity();
                gsd.setCurrentStock(updatedStock);
                session.delete(gse);
                session.saveOrUpdate(gsd);
            }
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return result;
            }

        } finally {

            if (!sessionAvailable) {

                session.close();
            }

        }
        return result;

    }

    public EventStatus removeBOSStockEntries(Session passedSession, BOS bos) {
        EventStatus result = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        List<GoDownStockEntry> gses;

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Query query = null;

            query = session.getNamedQuery("findGoDownBOSStockEntry");
            query.setParameter("bos", bos);

            gses = query.list();
            for (GoDownStockEntry gse : gses) {
                Query gsdQuery = session.getNamedQuery("findGoDownsProductDetail");
                gsdQuery.setParameter("godown", gse.getGodown());
                gsdQuery.setParameter("product", gse.getProduct());
                GoDownStockDetail gsd = (GoDownStockDetail) gsdQuery.getSingleResult();

                Long updatedStock = gsd.getCurrentStock() - gse.getQuantity();
                gsd.setCurrentStock(updatedStock);
                gse.setQuantity(new Long(0));
                gse.setStockUpdateType("BOS CANCEL");
                session.update(gse);
                session.saveOrUpdate(gsd);
            }

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return result;
            }
            hx.printStackTrace();

        } catch (Exception hx) {

            hx.printStackTrace();

        } finally {

            if (!sessionAvailable) {

                session.close();
            }

        }
        return result;

    }

    public EventStatus removeSalesStockEntries(Session passedSession, PurchaseSale ps) {
        EventStatus result = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        List<GoDownStockEntry> gses;

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Query query = null;

            query = session.getNamedQuery("findGoDownSalesStockEntry");
            query.setParameter("ps", ps);

            gses = query.list();
            for (GoDownStockEntry gse : gses) {
                Query gsdQuery = session.getNamedQuery("findGoDownsProductDetail");
                gsdQuery.setParameter("godown", gse.getGodown());
                gsdQuery.setParameter("product", gse.getProduct());
                GoDownStockDetail gsd = (GoDownStockDetail) gsdQuery.getSingleResult();

                Long updatedStock = gsd.getCurrentStock() - gse.getQuantity();
                gsd.setCurrentStock(updatedStock);
                gse.setQuantity(new Long(0));
                gse.setStockUpdateType("TX-I CANCEL");
                session.update(gse);
                session.saveOrUpdate(gsd);
            }

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return result;
            }
            hx.printStackTrace();

        } catch (Exception hx) {

            hx.printStackTrace();

        } finally {

            if (!sessionAvailable) {

                session.close();
            }

        }
        return result;

    }

    public EventStatus removeCrDrNoteEntries(Session passedSession, CreditDebitNote cdn) {
        EventStatus result = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        List<GoDownStockEntry> gses;
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Query query = null;

            query = session.getNamedQuery("findGoDownCrDrStockEntry");
            query.setParameter("cdn", cdn);

            gses = query.list();
            for (GoDownStockEntry gse : gses) {
                Query gsdQuery = session.getNamedQuery("findGoDownsProductDetail");
                gsdQuery.setParameter("godown", gse.getGodown());
                gsdQuery.setParameter("product", gse.getProduct());
                GoDownStockDetail gsd = (GoDownStockDetail) gsdQuery.getSingleResult();

                Long updatedStock = gsd.getCurrentStock() - gse.getQuantity();
                gsd.setCurrentStock(updatedStock);
                session.delete(gse);
                session.saveOrUpdate(gsd);
            }
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return result;
            }

        } finally {

            if (!sessionAvailable) {

                session.close();
            }

        }
        return result;

    }

    public EventStatus manualGoDownStockUpdate(Set<GoDownStockEntry> gses, String message, Date updateDate, Company company) {

        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();
            for (GoDownStockEntry gse : gses) {

                gse.setAuditData(AuditDataUtil.getAuditForCreate());
                session.save(gse);

                Query gsdQuery = session.getNamedQuery("findGoDownsProductDetail");
                gsdQuery.setParameter("godown", gse.getGodown());
                gsdQuery.setParameter("product", gse.getProduct());
                GoDownStockDetail gsd = (GoDownStockDetail) gsdQuery.getSingleResult();
                //  session.delete(gse);

                Long updatedStock = gsd.getCurrentStock() + gse.getQuantity();

                gsd.setCurrentStock(updatedStock);
                session.save(gsd);

            }

            session.getTransaction().commit();

            session.close();
            result.setUpdateDone(true);

            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }

        return result;
    }

    public Set<StockReportModel> fetchClosingStock(Date fromDate, Date toDate, Company company) {
        Set<StockReportModel> srms = new HashSet<StockReportModel>();

        List<GoDownStockEntry> gses;

        List<Product> pros;
        List<GoDown> gos;
        Session session = FACTORY.openSession();
        try {
            Query productQuery = session.getNamedQuery("findAllProductsWithoutServiceByCompany");
            productQuery.setParameter("company", company);
            productQuery.setParameter("isService", false);

            pros = productQuery.list();

            Query goDownQuery = session.getNamedQuery("findAllGoDownsByCompany");
            goDownQuery.setParameter("company", company);
            gos = goDownQuery.list();

            for (GoDown go : gos) {

                for (Product pro : pros) {

                    Query sumQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and godown=:go and stockUpdateDate between :fromDate and :toDate");
                    sumQuery.setParameter("fromDate", fromDate);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(toDate);
                    cal.add(Calendar.DATE, -1);
                    Date dateBefore1Day = cal.getTime();
                    sumQuery.setParameter("toDate", dateBefore1Day);
                    sumQuery.setParameter("pro", pro);
                    sumQuery.setParameter("go", go);
                    Long stock = (Long) sumQuery.getSingleResult();
                    StockReportModel srm;
                    if (stock != null) {
                        srm = new StockReportModel(pro, go, stock, stock, 0, 0, toDate);
                    } else {
                        srm = new StockReportModel(pro, go, new Long(0), new Long(0), 0, 0, toDate);
                    }

                    sumQuery.setParameter("toDate", toDate);
                    Long closingStock = (Long) sumQuery.getSingleResult();
                    if (closingStock != null) {
                        srm.setClosingStock(closingStock);
                    } else {
                        srm.setClosingStock(new Long(0));
                    }

                    srms.add(srm);

                }
            }
            return srms;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }
        return null;

    }

    public Set<StockReportModel> fetchTransactionSummary(Company company, Date fromDate, Date toDate, GoDown godown) {

//        
        Set<StockReportModel> srms = new HashSet<StockReportModel>();
        Long salesCount, purchaseCount, srCount, prCount, manualInCount, manualOutCount;
        List<Product> pros;
        Session session = FACTORY.openSession();
        try {

            Query productQuery = session.getNamedQuery("findAllProductsWithoutServiceByCompany");
            productQuery.setParameter("company", company);
            productQuery.setParameter("isService", false);
            pros = productQuery.list();

            for (Product pro : pros) {

                Query sumQuery, saleCountQuery, manualAddCountQuery, manualDeductCountQuery;
                if (godown == null) {
                    sumQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and stockUpdateDate between :fromDate and :toDate");
                    saleCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type");
                    manualAddCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type and quantity>0");
                    manualDeductCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type and quantity<0");
                } else {
                    sumQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and godown=:go and stockUpdateDate between :fromDate and :toDate");
                    saleCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and godown=:go and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type");
                    manualAddCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and godown=:go and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type and quantity>0");
                    manualDeductCountQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where product=:pro and godown=:go and stockUpdateDate between :fromDate and :toDate and stockUpdateType=:type and quantity<0");
//                    productQuery.setParameter("go", godown);
                    saleCountQuery.setParameter("go", godown);
                    manualAddCountQuery.setParameter("go", godown);
                    manualDeductCountQuery.setParameter("go", godown);
                    sumQuery.setParameter("go", godown);

                }

                sumQuery.setParameter("pro", pro);
                sumQuery.setParameter("fromDate", fromDate);
                sumQuery.setParameter("toDate", toDate);

                Long totalStock = (Long) sumQuery.getSingleResult();

                saleCountQuery.setParameter("pro", pro);
                saleCountQuery.setParameter("fromDate", fromDate);
                saleCountQuery.setParameter("toDate", toDate);

                saleCountQuery.setParameter("type", "TX-I");
                salesCount = (Long) saleCountQuery.getSingleResult();

                saleCountQuery.setParameter("type", "P-I");
                purchaseCount = (Long) saleCountQuery.getSingleResult();

                saleCountQuery.setParameter("type", "CREDIT-NOTE");
                srCount = (Long) saleCountQuery.getSingleResult();

                saleCountQuery.setParameter("type", "DEBIT-NOTE");
                prCount = (Long) saleCountQuery.getSingleResult();

                manualAddCountQuery.setParameter("pro", pro);
                manualAddCountQuery.setParameter("type", "MANUAL-ADD");
                manualAddCountQuery.setParameter("fromDate", fromDate);
                manualAddCountQuery.setParameter("toDate", toDate);
                manualInCount = (Long) manualAddCountQuery.getSingleResult();

                manualDeductCountQuery.setParameter("pro", pro);
                manualDeductCountQuery.setParameter("type", "MANUAL-DEDUCT");
                manualDeductCountQuery.setParameter("fromDate", fromDate);
                manualDeductCountQuery.setParameter("toDate", toDate);
                manualOutCount = (Long) manualDeductCountQuery.getSingleResult();

                StockReportModel srm = new StockReportModel();
                srm.setProduct(pro);
                srm.setSales(salesCount);
                srm.setPurchase(purchaseCount);
                srm.setSalesReturn(srCount);
                srm.setPurchaseReturn(prCount);
                srm.setManualStockIn(manualInCount);
                srm.setManualStockout(manualOutCount);
                if (godown != null) {
                    srm.setGodown(godown);
                }

                srms.add(srm);

            }
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }

        return srms;
    }

    public Map<Date, StockReportModel> fetchProductTransactionSummary(Company company, Date fromDate, Date toDate, Product pro) {

        //       Set<StockReportModel> srms = new HashSet<StockReportModel>();
        Map<Date, StockReportModel> proTransSum = new HashMap<>();
        Long salesCount, purchaseCount, srCount, prCount, manualInCount, manualOutCount;
        List<GoDownStockEntry> gses;
        Session session = FACTORY.openSession();
        try {

            Query productQuery = session.getNamedQuery("findGoDownProductStockEntryByDateAndProduct");
            productQuery.setParameter("fromDate", fromDate);
            productQuery.setParameter("toDate", toDate);
            productQuery.setParameter("product", pro);

            gses = productQuery.list();
            for (GoDownStockEntry gse : gses) {
                Date gseDate = gse.getStockUpdateDate();
                String gseType = gse.getStockUpdateType();
                if (proTransSum.containsKey(gseDate)) {
                    StockReportModel srm = proTransSum.get(gseDate);
                    if (gseType.equalsIgnoreCase("TX-I")) {
                        Long incrementSales = srm.getSales() + gse.getQuantity();
                        srm.setSales(incrementSales);
                    } else if (gseType.equalsIgnoreCase("P-I")) {
                        Long incrementPurchase = srm.getPurchase() + gse.getQuantity();
                        srm.setPurchase(incrementPurchase);
                    } else if (gseType.equalsIgnoreCase("DEBIT-NOTE")) {
                        Long incrementPurchaseR = srm.getPurchaseReturn() + gse.getQuantity();
                        srm.setPurchaseReturn(incrementPurchaseR);
                    } else if (gseType.equalsIgnoreCase("CREDIT-NOTE")) {
                        Long incrementSalesR = srm.getSalesReturn() + gse.getQuantity();
                        srm.setSalesReturn(incrementSalesR);
                    } else if (gseType.equalsIgnoreCase("MANUAL-ADD")) {
                        Long manualCount = gse.getQuantity();
                        srm.setManualStockIn(srm.getManualStockIn() + manualCount);
                    } else if (gseType.equalsIgnoreCase("MANUAL-DEDUCT")) {
                        Long manualCount = gse.getQuantity();
                        srm.setManualStockout(srm.getManualStockout() + manualCount);
                    }
                } else {
                    Long zero = new Long(0);
                    StockReportModel srm = new StockReportModel(zero, zero, zero, zero, zero, zero);

                    if (gseType.equalsIgnoreCase("TX-I")) {
                        Long incrementSales = gse.getQuantity();
                        srm.setSales(incrementSales);
                    } else if (gseType.equalsIgnoreCase("P-I")) {
                        Long incrementPurchase = gse.getQuantity();
                        srm.setPurchase(incrementPurchase);
                    } else if (gseType.equalsIgnoreCase("DEBIT-NOTE")) {
                        Long incrementPurchaseR = gse.getQuantity();
                        srm.setPurchaseReturn(incrementPurchaseR);
                    } else if (gseType.equalsIgnoreCase("CREDIT-NOTE")) {
                        Long incrementSalesR = gse.getQuantity();
                        srm.setSalesReturn(incrementSalesR);
                    } else if (gseType.equalsIgnoreCase("MANUAL-ADD")) {
                        Long manualCount = gse.getQuantity();
                        srm.setManualStockIn(manualCount);
                    } else if (gseType.equalsIgnoreCase("MANUAL-DEDUCT")) {
                        Long manualCount = gse.getQuantity();
                        srm.setManualStockout(manualCount);
                    }
                    proTransSum.put(gseDate, srm);
                }
            }

        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }

        return proTransSum;
    }

    public Map<String, StockReportModel> fetchProductsSoldOut() {
        Map<String, StockReportModel> productExp = new TreeMap<>();
        List<GoDownStockEntry> gses;
        Session session = FACTORY.openSession();
        Company company = SessionDataUtil.getSelectedCompany();
        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Query productQuery = session.getNamedQuery("findProductsSoldOut");
            productQuery.setParameter("company", company);
            gses = productQuery.list();
            for (GoDownStockEntry gse : gses) {
                String productName = gse.getProduct().getName();
                String gseType = gse.getStockUpdateType();
                if (productExp.containsKey(productName)) {
                    StockReportModel srm = productExp.get(gse.getProduct().getName());
                    long stockOut = 0;
                    long stockIn = 0;
                    if (srm.getStockOut() != null) {
                        stockOut = srm.getStockOut();
                    }
                    if (srm.getStockIn() != null) {
                        stockIn = srm.getStockIn();
                    }
                    if ((gseType.equalsIgnoreCase("TX-I")) || (gseType.equalsIgnoreCase("CREDIT-NOTE")) || (gseType.equalsIgnoreCase("MANUAL-DEDUCT"))) {
                        srm.setStockOut(stockOut + gse.getQuantity());
                        if (gseType.equalsIgnoreCase("TX-I")) {
                            srm.setReportDate(gse.getStockUpdateDate());
                        }
                    } else if ((gseType.equalsIgnoreCase("P-I")) || (gseType.equalsIgnoreCase("DEBIT-NOTE")) || (gseType.equalsIgnoreCase("MANUAL-ADD") || (gseType.equalsIgnoreCase("OPENING")))) {
                        srm.setStockIn(stockIn + gse.getQuantity());
                    }
                    srm.setGodown(gse.getGodown());
                    srm.setProduct(gse.getProduct());
                } else {
                    Long zero = new Long(0);
                    StockReportModel srm = new StockReportModel(zero, zero, zero, zero, zero, zero);
                    if ((gseType.equalsIgnoreCase("TX-I")) || (gseType.equalsIgnoreCase("CREDIT-NOTE")) || (gseType.equalsIgnoreCase("MANUAL-DEDUCT"))) {
                        srm.setStockOut(gse.getQuantity());
                    } else if ((gseType.equalsIgnoreCase("P-I")) || (gseType.equalsIgnoreCase("DEBIT-NOTE")) || (gseType.equalsIgnoreCase("MANUAL-ADD"))||gseType.equalsIgnoreCase("OPENING")) {
                        srm.setStockIn(gse.getQuantity());
                    }
//                    srm.setBatchNo(gse.getBatchNo());
//                    srm.setExpiryDate(gse.getExpiryDate());
                    srm.setGodown(gse.getGodown());
                    srm.setProduct(gse.getProduct());

                    productExp.put(productName, srm);

                }
            }
        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
        } finally {
            session.close();
        }
        return productExp;
    }

}
