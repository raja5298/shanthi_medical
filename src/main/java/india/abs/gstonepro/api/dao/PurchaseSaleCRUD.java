package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.business.LedgerLogic;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SGS
 */
public class PurchaseSaleCRUD {

    PurchaseSaleLineItemCRUD psli4 = new PurchaseSaleLineItemCRUD();
    PurchaseSaleTaxSummaryCRUD psts4 = new PurchaseSaleTaxSummaryCRUD();

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createPS(String type, Long partyId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = new EventStatus();
        try (Session session = FACTORY.openSession()) {

            session.beginTransaction();

            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);
            if (ledger == null) {
                ledger = new LedgerLogic().fetchNoPartyLedger();
            }
            newPS.setLedger(ledger);
            newPS.setPsType(type);
            session.save(newPS);
            session.getTransaction().commit();
            result.setCreateDone(true);
            session.close();
        }
        for (PurchaseSaleLineItem psLI : psItems) {
            psli4.createPSLineItem(newPS.getPsId(), psLI);
        }
        for (PurchaseSaleTaxSummary psTS : psTaxSummaries) {
            psts4.createPSTaxSummary(newPS.getPsId(), psTS);
        }

        return result;
    }

    public EventStatus updatePSPayment(List<PurchaseSale> updatedPS, Ledger lg, List<CreditDebitNote> cdnList, String notes, Date paymentDate) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            Set<Transaction> transList = new HashSet<>();
            Journal js = new Journal();
            js.setJournalId(Calendar.getInstance().getTime() + "Sales Payments");
            for (PurchaseSale ps : updatedPS) {
                PurchaseSale uPS = (PurchaseSale) session.get(PurchaseSale.class, ps.getPsId());
                uPS.setTotalAmountPaid(uPS.getTotalAmountPaid().add(ps.getTotalAmountPaid()));
                session.save(uPS);
                Transaction ts = new Transaction();
                ts.setAmount(ps.getTotalAmountPaid());
                if (uPS.getLedger() != null) {
                    ts.setLedger(uPS.getLedger());
                } else {
                    Ledger newLg = new LedgerLogic().fetchNoPartyLedger();
                    ts.setLedger(newLg);
                }
//                ts.setTransactionDate(Calendar.getInstance().getTime());
                ts.setJournal(js);
                ts.setTransactionDate(paymentDate);
                ts.setType("Payment Sales - " + uPS.getBillNo());
                Audit ad = new Audit();

                ts.setParticulars(notes);
                transList.add(ts);
                session.save(ts);
            }
            for (CreditDebitNote cdn : cdnList) {
                CreditDebitNote cdnUp = (CreditDebitNote) session.get(CreditDebitNote.class, cdn.getCdNoteId());
                cdnUp.setTotalAmountPaid(cdnUp.getTotalAmountPaid().add(cdn.getTotalAmountPaid()));
                session.save(cdnUp);
                Transaction ts = new Transaction();
                ts.setAmount(cdn.getTotalAmountPaid());

                if (cdnUp.getLedger() != null) {
                    ts.setLedger(cdnUp.getLedger());
                } else {
                    Ledger cdnLg = new LedgerLogic().fetchNoPartyLedger();
                    ts.setLedger(cdnLg);
                }
                ts.setJournal(js);

                ts.setType("Payment CreditNote - " + cdn.getCdNoteNo());

//                ts.setType("Payment-CreditNote - ")
                ts.setParticulars(notes);
                transList.add(ts);
                ts.setTransactionDate(paymentDate);
                session.save(ts);
            }
            js.setTransactions(transList);
            session.save(js);
            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }
        return updateLedger;
    }

    public EventStatus updatePurchasePayment(List<PurchaseInvoice> updatedPS, List<CreditDebitNote> cdnList, String notes, Date date) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            Set<Transaction> transList = new HashSet<>();
            Journal js = new Journal();
            js.setJournalId(Calendar.getInstance().getTime() + "Purchase Payments");
            for (PurchaseInvoice ps : updatedPS) {
                PurchaseInvoice uPS = (PurchaseInvoice) session.get(PurchaseInvoice.class, ps.getPurchaseInvoiceId());
                uPS.setTotalAmountPaid(uPS.getTotalAmountPaid().add(ps.getTotalAmountPaid()));
                session.save(uPS);
                Transaction ts = new Transaction();
                ts.setAmount(ps.getTotalAmountPaid());
                if (uPS.getLedger() != null) {
                    ts.setLedger(uPS.getLedger());
                } else {
                    Ledger newLg = new LedgerLogic().fetchNoPartyLedger();
                    ts.setLedger(newLg);
                }
                ts.setTransactionDate(date);
                ts.setJournal(js);
                ts.setType("Payment Purchase - " + uPS.getPurchaseInvoiceNo());
                ts.setParticulars(notes);
                transList.add(ts);
                session.save(ts);
            }
            for (CreditDebitNote cdn : cdnList) {
                CreditDebitNote cdnUp = (CreditDebitNote) session.get(CreditDebitNote.class, cdn.getCdNoteId());
                cdnUp.setTotalAmountPaid(cdnUp.getTotalAmountPaid().add(cdn.getTotalAmountPaid()));
                session.save(cdnUp);
                Transaction ts = new Transaction();
                ts.setAmount(cdn.getTotalAmountPaid());
                if (cdnUp.getLedger() != null) {
                    ts.setLedger(cdnUp.getLedger());
                } else {
                    Ledger cdnLg = new LedgerLogic().fetchNoPartyLedger();
                    ts.setLedger(cdnLg);
                }
                ts.setTransactionDate(date);
                ts.setJournal(js);
                ts.setType("Payment DebitNote - " + cdnUp.getCdNoteNo());
                ts.setParticulars(notes);
                transList.add(ts);
                session.save(ts);
            }
            js.setTransactions(transList);
            session.save(js);
            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }

        return updateLedger;
    }

    public EventStatus updatePS(Long psId, PurchaseSale updatedPS) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            PurchaseSale uPS = (PurchaseSale) session.get(PurchaseSale.class, updatedPS.getPsId());

            uPS.setBillNo(updatedPS.getBillNo());
            uPS.setBillAmount(updatedPS.getBillAmount());
            uPS.setCalculatedTotalValue(updatedPS.getCalculatedTotalValue());
            uPS.setPsDate(updatedPS.getPsDate());
            uPS.setRemarks(updatedPS.getRemarks());
            uPS.setVariationAmount(updatedPS.getVariationAmount());
            // uPS.set(updatedPS.get());

            session.save(uPS);
            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }

        return updateLedger;
    }

    public List<PurchaseSale> getAllPS(String type, Date fromDate, Date toDate, boolean withCancelledBill) {
        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;
        if (withCancelledBill) {
            query = session.getNamedQuery("findAllPSByType");
            query.setParameter("type", type);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            query.setParameter("company", SessionDataUtil.getSelectedCompany());
        } else {
            query = session.getNamedQuery("findPSByType");
            query.setParameter("type", type);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("withCancelledBill", false);
        }

        purchaseSalesS = query.list();
        session.getTransaction().commit();

        session.close();

        return purchaseSalesS;

    }

    public List<PurchaseSaleLineItem> getAllTaxInvoiceByProduct(String type, Date fromDate, Date toDate, Product product) {
        List<PurchaseSaleLineItem> psLis = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findPSLIByProduct");
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("cancelled", false);
        query.setParameter("product", product);

        psLis = query.list();
        session.getTransaction().commit();

        session.close();

        return psLis;

    }

    public List<PurchaseSale> getAllTaxInvoiceByAuditData(String type, Date fromDate, Date toDate) {
        List<PurchaseSale> pss = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findPSByAudit");
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        query.setParameter("company", SessionDataUtil.getSelectedCompany());

        pss = query.list();
        session.getTransaction().commit();

        session.close();

        return pss;

    }

    public List<PurchaseSale> getAllPSByLedger(Long ledgerId, String type, Date fromDate, Date toDate, boolean withCancelledBill) {
        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();
        Ledger ledger = (Ledger) session.get(Ledger.class, ledgerId);

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findPSByLedger");
        query.setParameter("ledger", ledger);
        query.setParameter("type", type);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("withCancelledBill", false);

        purchaseSalesS = query.list();
        for (PurchaseSale ps : purchaseSalesS) {

        }
        session.getTransaction().commit();

        session.close();

        return purchaseSalesS;

    }

    public List<PurchaseSale> getAllPSByLedgerWithoutDate(Ledger ledger, String type, boolean withCancelledBill) {

        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();
        if (ledger != null) {
            Query query = session.getNamedQuery("findPSByLedgerWithoutDate");
            query.setParameter("ledger", ledger);
            query.setParameter("type", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("withCancelledBill", false);
            purchaseSalesS = query.list();
            session.close();
        } else {
            Query query = session.getNamedQuery("findPSByTypeWitOutDate");
//            query.setParameter("ledger", ledger);
            query.setParameter("type", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("withCancelledBill", false);
            purchaseSalesS = query.list();
            session.close();
        }
        return purchaseSalesS;

    }

    public PurchaseSale getPSDetail(String psId) {

        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class,
                    psId);

            List<PurchaseSaleLineItem> psLis = null;
            List<PurchaseSaleTaxSummary> psTs = null;

            Query psLiQuery = session.getNamedQuery("findAllPSLItems");

            psLiQuery.setParameter("ps", ps);

            psLis = psLiQuery.list();

            Query psTsQuery = session.getNamedQuery("findPSTSByPS");

            psTsQuery.setParameter("ps", ps);

            psTs = psTsQuery.list();
            ps.setPsLineItems(psLis);
            ps.setPsTaxSummaries(psTs);

            session.getTransaction().commit();
            session.close();
            return ps;
        }
    }

    public List<PurchaseSaleLineItem> getPSLIs(String psId) {
        List<PurchaseSaleLineItem> psLIs = null;

        try (Session session = FACTORY.openSession()) {
            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class,
                    psId);

            Hibernate.initialize(ps.getPsTaxSummaries());
            if (ps.getPsLineItems() != null) {
                psLIs = ps.getPsLineItems();
            }
            session.getTransaction().commit();
            session.close();
            return psLIs;
        }
    }

    public EventStatus deletePS(Long psLineItemId) {
        EventStatus deletePSLineItem = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                PurchaseSaleLineItem psli = new PurchaseSaleLineItem();
                psli.setPsliId(psLineItemId);

                session.delete(psli);

                session.getTransaction().commit();
                deletePSLineItem.setDeleteDone(true);
            }

            return deletePSLineItem;
        } catch (ConstraintViolationException ex) {
            return deletePSLineItem;

        } catch (HibernateException ex) {
            return deletePSLineItem;
        }
    }

    public List<PurchaseSale> getAllPSByDate(Date fromDate, Date toDate) {
        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;
        query = session.getNamedQuery("findPSByType");
        query.setParameter("type", "TX-I");
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("withCancelledBill", false);

        purchaseSalesS = query.list();

        session.getTransaction()
                .commit();

        session.close();

        return purchaseSalesS;

    }

    public List<PurchaseSale> getAllPSWitOutDate(String type, boolean withCancelledBill) {
        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;
        if (withCancelledBill) {
            query = session.getNamedQuery("findAllPSByTypeWithOutDate");
            query.setParameter("type", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
        } else {
            query = session.getNamedQuery("findPSByTypeWitOutDate");
            query.setParameter("type", type);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("withCancelledBill", false);
        }

        purchaseSalesS = query.list();
        session.getTransaction().commit();

        session.close();

        return purchaseSalesS;

    }

    public List<PurchaseSale> getAllPSByLedgerWithOutDate(Long ledgerId, String type, boolean withCancelledBill) {
        List<PurchaseSale> purchaseSalesS = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();
        Ledger ledger = (Ledger) session.get(Ledger.class,
                ledgerId);

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findPSByLedgerWitoutDate");
        query.setParameter("ledger", ledger);
        query.setParameter("type", type);

        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("withCancelledBill", false);

        purchaseSalesS = query.list();
        for (PurchaseSale ps : purchaseSalesS) {

        }
        session.getTransaction().commit();

        session.close();

        return purchaseSalesS;

    }

}
