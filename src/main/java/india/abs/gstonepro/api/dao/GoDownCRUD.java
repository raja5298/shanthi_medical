/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;

import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.models.StockReportModel;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class GoDownCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createGoDown(GoDown newGoDown, Company company) {
        EventStatus result = new EventStatus();
        ProductCRUD p4 = new ProductCRUD();
        GoDownStockCRUD gse4 = new GoDownStockCRUD();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Company co = (Company) session.get(Company.class, company.getCompanyId());
            newGoDown.setCompany(co);
            session.save(newGoDown);
            co.getGodowns().add(newGoDown);
            session.save(co);
            for (Product po : p4.fetchAllProducts(co.getCompanyId())) {
                GoDownStockDetail gsd = new GoDownStockDetail();
                gsd.setGodown(newGoDown);
                gsd.setProduct(po);
                GoDownStockEntry openingGoDownStockEntry = gse4.changeProductStock(session,newGoDown, po, new Long(0), BigDecimal.ZERO, new Date(), "OPENING", 0, 0, 0, null, null, null, null, null,null,null);
                gsd.setCurrentStock(new Long(0));
            }
            session.getTransaction().commit();
            result.setCreateDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
            return result;
        }
    }

    public List<GoDown> fetchAllGodowns(Long companyId) {
        List<GoDown> godowns = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllGoDownsByCompany");
            Company co = (Company) session.get(Company.class, companyId);
            query.setParameter("company", co);

            godowns = query.list();
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return godowns;
            }

        } finally {
            session.close();
            return godowns;
        }
    }

    public GoDown getPrimaryGoDown(Long companyId) {
        
        GoDown primaryGoDown = null;
        
         Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findPrimaryGoDown");
            Company co = (Company) session.get(Company.class, companyId);
            query.setParameter("company", co);
            query.setParameter("goDownName", "Primary");

            primaryGoDown = (GoDown) query.getSingleResult();
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return primaryGoDown;
            }

        } finally {

            session.close();
            return primaryGoDown;

        }
    }
    
    
}
