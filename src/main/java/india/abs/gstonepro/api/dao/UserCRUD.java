/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class UserCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
   

    public EventStatus createUser(String userName, String phoneNumber, String password, boolean isAdmin) {
        EventStatus createUser = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();
            AppUser user = new AppUser();
            user.setLoggedIn(Boolean.FALSE);
            user.setPassword(password);
            user.setPhoneNumber(phoneNumber);
            user.setUserName(userName);
            user.setAdmin(isAdmin);
            session.save(user);

            CompanyCRUD c4 = new CompanyCRUD();

            for (Company com : c4.fetchAllCompaniesWithLedgerGroups()) {
                CompanyUserRole cur = new CompanyUserRole();
                cur.setUser(user);
                cur.setCompany(com);
                if(isAdmin){
                    cur.makeAdmin();
                }
                session.save(cur);
                

            }
             session.getTransaction().commit();
            createUser.setCreateDone(true);
            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return createUser;
        } finally {

            session.close();

        }

        return createUser;
    }

    public EventStatus updateUser(Long userId, String userName, String phoneNumber, String password, boolean isAdmin) {
        EventStatus updateUser = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            AppUser user = (AppUser) session.get(AppUser.class, userId);
            user.setUserName(userName);
            user.setPhoneNumber(phoneNumber);
            user.setPassword(password);
            user.setAdmin(isAdmin);
            session.save(user);
            session.getTransaction().commit();
            updateUser.setUpdateDone(true);
            session.close();
        }

        return updateUser;
    }

    public List<AppUser> fetchAllUsers() {
        List<AppUser> users = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            // session.createCriteria(Company.class).list();

            users = session.createQuery("from AppUser").list();
            session.getTransaction().commit();
            session.close();

        }
        return users;

    }

    public AppUser fetchAUser(Long userId) {
        AppUser user;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            user = (AppUser) session.get(AppUser.class, userId);
            session.getTransaction().commit();
            session.close();

        }

        return user;
    }

    public AppUser fetchAuthorisedUser(String userName, String password) {
        AppUser user = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            Query query = session.getNamedQuery("findUerByNameAndPassword");
            query.setString("userName", userName);
            query.setString("password", password);
            List<AppUser> users = query.list();
            if (users.size() == 1) {
                user = users.get(0);
            }

            session.getTransaction().commit();
            session.close();

        }

        return user;
    }

    public EventStatus deleteUser(Long userId) {
        EventStatus deleteUser = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                AppUser user = new AppUser();
                user.setUserId(userId);

                session.delete(user);

                session.getTransaction().commit();
                deleteUser.setDeleteDone(true);
            }

            return deleteUser;
        } catch (ConstraintViolationException ex) {
            return deleteUser;

        } catch (HibernateException ex) {
            return deleteUser;
        }
    }

}
