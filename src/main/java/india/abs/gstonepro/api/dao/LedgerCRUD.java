/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.LedgerGroupLogic;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class LedgerCRUD {
    
    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    
    public Ledger createLedger(Long companyId, Ledger newLedger) {
        EventStatus createUser = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            
            session.beginTransaction();
            
            Company company = (Company) session.get(Company.class, companyId);
            
            newLedger.setCompany(company);
            newLedger.setEditable(true);
            newLedger.setDeletable(true);
            
            session.save(newLedger);
            
            Journal openingBalance = new Journal();
            openingBalance.setJournalId(companyId + "LEDGER" + newLedger.getLedgerId());
            Transaction newPSTransaction = new Transaction();
            newPSTransaction.setJournal(openingBalance);
            newPSTransaction.setTransactionDate(newLedger.getOpeningBalanceDate());
            newPSTransaction.setParticulars("Opening Balance");
            newPSTransaction.setType("OP");
            newPSTransaction.setAmount(newLedger.getOpeningBalance());
            newPSTransaction.setLedger(newLedger);
            session.save(newPSTransaction);
            openingBalance.getTransactions().add(newPSTransaction);
            
            session.save(openingBalance);
            
            session.getTransaction().commit();
            createUser.setCreateDone(true);
            session.close();
            return newLedger;
        }
    }
    
    public EventStatus createPartyLedger(Long companyId, Ledger newLedger, boolean isSundryDebitor) {
        EventStatus createUser = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            
            session.beginTransaction();
            
            Company company = (Company) session.get(Company.class, companyId);
            
            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", company);
            if (isSundryDebitor) {
                query.setParameter("ledgerGroupName", "Sundry Debtors");
            } else {
                query.setParameter("ledgerGroupName", "Sundry Creditors");
            }
            query.setParameter("ledgerGroupName2", "");
            
            LedgerGroup lg = (LedgerGroup) query.uniqueResult();
            
            newLedger.setCompany(company);
            newLedger.setEditable(false);
            newLedger.setDeletable(false);
            newLedger.setLedgerGroup(lg);
            
            session.save(newLedger);
            boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
            
            if (isAccountEnabled) {
                
                Journal openingBalance = new Journal();
                openingBalance.setJournalId(companyId + "PARTY" + newLedger.getLedgerId());
                Transaction newPSTransaction = new Transaction();
                newPSTransaction.setJournal(openingBalance);
                
                boolean isOpeningBalancePresent = newLedger.getOpeningBalance().compareTo(BigDecimal.ZERO) == 1;
                if (isOpeningBalancePresent) {
                    newLedger.setOpeningBalanceDate(newLedger.getOpeningBalanceDate());
                    newLedger.setOpeningBalance(newLedger.getOpeningBalance());
                    newPSTransaction.setAmount(newLedger.getOpeningBalance());
                    newPSTransaction.setTransactionDate(newLedger.getOpeningBalanceDate());
                } else {
                    newPSTransaction.setAmount(BigDecimal.ZERO);
                    newPSTransaction.setTransactionDate(SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
                    newLedger.setOpeningBalance(BigDecimal.ZERO);
                    newLedger.setOpeningBalanceDate(SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
                }
                newPSTransaction.setParticulars("Opening Balance");
                newPSTransaction.setType("OP");
                
                newPSTransaction.setLedger(newLedger);
                session.save(newPSTransaction);
                openingBalance.getTransactions().add(newPSTransaction);
                
                session.save(openingBalance);
                session.save(newLedger);
                
            }
            
            session.getTransaction().commit();
            createUser.setCreateDone(true);
            
            session.close();
        }
        return createUser;
    }
    
    public List<Ledger> fetchLedgerByGroupName(long companyId, String groupName) {
        List<Ledger> ledgerList = new ArrayList<>();
        Session session = FACTORY.openSession();
        
        try {
            Company company = session.get(Company.class,
                    companyId);
            Query query = session.getNamedQuery("findLedgerByLedgerGroupName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Suspense");
            ledgerList = query.getResultList();
        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
        } finally {
            session.close();
        }
        return ledgerList;
    }
    
    public EventStatus updatePartyLedger(Ledger updatedLedger) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class, updatedLedger.getLedgerId());
            
            ledger.setLedgerName(updatedLedger.getLedgerName());
            ledger.setAddress(updatedLedger.getAddress());
            ledger.setGSTIN(updatedLedger.getGSTIN());
            ledger.setEmail(updatedLedger.getEmail());
            ledger.setPhone(updatedLedger.getPhone());
            ledger.setMobile(updatedLedger.getMobile());
            ledger.setCity(updatedLedger.getCity());
            ledger.setDistrict(updatedLedger.getDistrict());
            ledger.setState(updatedLedger.getState());
            ledger.setLedgerGroup(updatedLedger.getLedgerGroup());
            ledger.setParentLedger(updatedLedger.getParentLedger());
            boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
            boolean isOpeningBalancePresent = !(updatedLedger.getOpeningBalance() == null) && !(updatedLedger.getOpeningBalanceDate() == null);
            if (isAccountEnabled && isOpeningBalancePresent) {
                ledger.setOpeningBalance(updatedLedger.getOpeningBalance());
                ledger.setOpeningBalanceDate(updatedLedger.getOpeningBalanceDate());
            }
            
            session.save(ledger);
//            if (isAccountEnabled && isOpeningBalancePresent) {
//                String journalId = SessionDataUtil.getSelectedCompany().getCompanyId() + "PARTY" + updatedLedger.getLedgerId();
//
//                Journal openingJournal = (Journal) session.get(Journal.class, journalId);
//                for (Transaction openingTransaction : openingJournal.getTransactions()) {
//                    Transaction trans = (Transaction) session.get(Transaction.class, openingTransaction.getTransactionId());
//                    trans.setAmount(updatedLedger.getOpeningBalance());
//                    trans.setTransactionDate(updatedLedger.getOpeningBalanceDate());
//                    session.update(trans);
//                }
//            }

            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }
        
        return updateLedger;
    }
    
    public EventStatus updateLedger(Ledger updatedLedger) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class, updatedLedger.getLedgerId());
            
            ledger.setLedgerName(updatedLedger.getLedgerName());
            ledger.setAddress(updatedLedger.getAddress());
            ledger.setGSTIN(updatedLedger.getGSTIN());
            ledger.setEmail(updatedLedger.getEmail());
            ledger.setPhone(updatedLedger.getPhone());
            ledger.setMobile(updatedLedger.getMobile());
            ledger.setCity(updatedLedger.getCity());
            ledger.setDistrict(updatedLedger.getDistrict());
            ledger.setState(updatedLedger.getState());
            ledger.setLedgerGroup(updatedLedger.getLedgerGroup());
            ledger.setOpeningBalance(updatedLedger.getOpeningBalance());
            ledger.setOpeningBalanceDate(updatedLedger.getOpeningBalanceDate());
            ledger.setChildrenLedgers(updatedLedger.getChildrenLedgers());
            ledger.setParentLedger(updatedLedger.getParentLedger());
            
            session.save(ledger);
            if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {
                
                String journalId = SessionDataUtil.getSelectedCompany().getCompanyId() + "LEDGER" + updatedLedger.getLedgerId();
                
                Journal openingJournal = (Journal) session.get(Journal.class, journalId);
                for (Transaction openingTransaction : openingJournal.getTransactions()) {
                    Transaction trans = (Transaction) session.get(Transaction.class, openingTransaction.getTransactionId());
                    trans.setAmount(updatedLedger.getOpeningBalance());
                    trans.setTransactionDate(updatedLedger.getOpeningBalanceDate());
                    session.update(trans);
                }
            }
            
            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }
        
        return updateLedger;
    }
    
    public EventStatus updateLedgerOpeningBalance(Ledger updatedLedger) {
        EventStatus updateLedger = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class, updatedLedger.getLedgerId());
            
            Query query = session.getNamedQuery("findTransactionsByLedger");
            
            query.setParameter("ledger", ledger);
            query.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            query.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());
            List<Transaction> trans = query.list();
            
            if (trans.size() == 1) {
                
                Transaction t = trans.get(0);
                t.setAmount(updatedLedger.getOpeningBalance());
                t.setTransactionDate(updatedLedger.getOpeningBalanceDate());
                session.save(t);
                ledger.setOpeningBalance(updatedLedger.getOpeningBalance());
                ledger.setOpeningBalanceDate(updatedLedger.getOpeningBalanceDate());
                session.save(ledger);
                session.getTransaction().commit();
                updateLedger.setUpdateDone(true);
                
                return updateLedger;
            } else {
                updateLedger.setMessage("Not permission to update this Ledger");
            }
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            
            return updateLedger;
        } finally {
            
            session.close();
            
        }
        
        return updateLedger;
    }
    
    public List<Ledger> fetchAllLedgers(Long companyId) {
        List<Ledger> filteredLedgers = new ArrayList<Ledger>();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            Company company = (Company) session.get(Company.class, companyId);
            
            Hibernate.initialize(company.getLedgers());
            filteredLedgers = company.getLedgers();
            
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return filteredLedgers;
        } finally {
            
            session.close();
            
        }
        return filteredLedgers;
        
    }
    
    public Set<Ledger> fetchPartyLedgers(Company company) {
        
        Set<Ledger> ledgers = new HashSet<>();
        List<LedgerGroup> ledgerGroups = null;
        Session session;
        
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Sundry Debtors");
            query.setParameter("ledgerGroupName2", "Sundry Creditors");
            
            ledgerGroups = query.list();
            session.getTransaction().commit();
            
            session.close();
            for (LedgerGroup lg : ledgerGroups) {
                for (Ledger ld : lg.getLedgers()) {
                    ledgers.add(ld);
                }
            }
            return ledgers;
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return ledgers;
        } finally {
            
            session.close();
            
        }
        return ledgers;
    }
    
    public Ledger fetchLedgerByName(Company company, String ledgerName) {
        
        Session session;
        Ledger lg = null;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findLedgerByName");
            query.setParameter("company", company);
            query.setParameter("ledgerName", ledgerName);
            lg = (Ledger) query.uniqueResult();
            session.getTransaction().commit();
            
            session.close();
            
            return lg;
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return lg;
        } finally {
            
            session.close();
            
        }
        return lg;
    }
    
    public Set<Ledger> fetchBankLedgers(Company company) {
        
        List<LedgerGroup> ledgerGroups = null;
        Session session;
        LedgerGroup lg = new LedgerGroup();
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Bank");
            query.setParameter("ledgerGroupName2", "");
            lg = (LedgerGroup) query.uniqueResult();
            session.getTransaction().commit();
            
            session.close();
            Set<Ledger> ledgers = lg.getLedgers();
            lg.printLedgers();
            return ledgers;
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return lg.getLedgers();
        } finally {
            
            session.close();
            
        }
        return lg.getLedgers();
    }
    
    public Set<Ledger> fetchPartyAndGOCLedgers(Company company) {
        
        List<LedgerGroup> ledgerGroups = null;
        Set<Ledger> ledgers = new HashSet<Ledger>();
        
        Session session;
        LedgerGroup lg = new LedgerGroup();
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findLedgerGroupByNames");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Sundry Debtors");
            query.setParameter("ledgerGroupName3", "Sundry Creditors");
            query.setParameter("ledgerGroupName2", "Group of Companies");
            
            ledgerGroups = query.list();
            for (LedgerGroup lg1 : ledgerGroups) {
                ledgers.addAll(lg1.getLedgers());
            }
            
            session.getTransaction().commit();
            
            session.close();
            
            lg.printLedgers();
            return ledgers;
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return lg.getLedgers();
        } finally {
            
            session.close();
            
        }
        return lg.getLedgers();
    }
    
    public Set<Ledger> fetchPurchaseLedgers(Company company) {
        
        List<LedgerGroup> ledgerGroups = null;
        Set<Ledger> ledgers = new HashSet<Ledger>();
        
        Session session;
        LedgerGroup lg = new LedgerGroup();
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findPurchaseLedgerByLedgerGroupName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Sundry Debtors");
            query.setParameter("ledgerGroupName2", "Group of Companies");
            
            ledgerGroups = query.list();
            if (ledgerGroups.size() > 0) {
                for (LedgerGroup lg1 : ledgerGroups) {
                    ledgers.addAll(lg1.getLedgers());
                }
            }
            
            session.getTransaction().commit();
            
            session.close();
            
            lg.printLedgers();
            return ledgers;
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return lg.getLedgers();
        } finally {
            
            session.close();
            
        }
        return lg.getLedgers();
    }
    
    public Ledger fetchAGeneralLedger(Company company, String ledgerName) {
        Ledger ledger = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "General");
            LedgerGroup lg = (LedgerGroup) query.uniqueResult();
            for (Ledger l : lg.getLedgers()) {
                if (l.getLedgerName().equalsIgnoreCase(ledgerName)) {
                    return l;
                }
            }
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return ledger;
        } finally {
            
            session.close();
            
        }
        return ledger;
    }
    
    public EventStatus deleteLedger(Long ledgerId) {
        
        EventStatus deleteLedger = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class, ledgerId);
            
            Query transQuery = session.createQuery("select count(amount) from  Transaction t where t.ledger=:ledger");
            transQuery.setParameter("ledger", ledger);
            Long transCount = (Long) transQuery.getSingleResult();
            
            if (transCount == 1) {
                
                Journal j = new Journal();
                j.setJournalId(ledger.getCompany().getCompanyId() + "LEDGER" + ledger.getLedgerId());
                
                Query deleteProductFromPricingPolicy = session.createQuery("delete from Transaction t  where t.journal =:j");
                deleteProductFromPricingPolicy.setParameter("j", j);
                
                int opDelete = deleteProductFromPricingPolicy.executeUpdate();
                
                session.delete(j);
                session.delete(ledger);
                session.getTransaction().commit();
                deleteLedger.setDeleteDone(true);
                
            } else {
                deleteLedger.setMessage("Please remove any transactions and try again!");
            }
            
            return deleteLedger;
        } catch (HibernateException px) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                px.printStackTrace();
                deleteLedger.setMessage("Error during ledger Delete. Please try again!");
                return deleteLedger;
            }
            
        } finally {
            session.close();
        }
        deleteLedger.setMessage("Error during ledger Delete. Please try again!");
        return deleteLedger;
    }
    
    public EventStatus deletePartyLedger(Long ledgerId) {
        
        EventStatus deleteLedger = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class, ledgerId);
            
            Query transQuery = session.createQuery("select count(amount) from  Transaction t where t.ledger=:ledger");
            transQuery.setParameter("ledger", ledger);
            Long transCount = (Long) transQuery.getSingleResult();
            
            if (transCount <= 1) {
                Query saleQuery = session.createQuery("select count(remarks) from  PurchaseSale s where s.ledger=:ledger");
                saleQuery.setParameter("ledger", ledger);
                Long saleCount = (Long) saleQuery.getSingleResult();
                
                Query purchaseQuery = session.createQuery("select count(remarks) from  PurchaseInvoice s where s.ledger=:ledger");
                purchaseQuery.setParameter("ledger", ledger);
                Long purchaseCount = (Long) purchaseQuery.getSingleResult();
                
                Query crDrNoteQuery = session.createQuery("select count(remarks) from  CreditDebitNote s where s.ledger=:ledger");
                crDrNoteQuery.setParameter("ledger", ledger);
                Long crDrNoteCount = (Long) crDrNoteQuery.getSingleResult();
                
                Query estimateQuery = session.createQuery("select count(remarks) from  Estimate s where s.ledger=:ledger");
                estimateQuery.setParameter("ledger", ledger);
                Long estimateCount = (Long) estimateQuery.getSingleResult();
                
                Long totalCount = saleCount + purchaseCount + crDrNoteCount + estimateCount;
                
                if (totalCount == 0) {
                    
                    Journal j = new Journal();
                    j.setJournalId(ledger.getCompany().getCompanyId() + "PARTY" + ledger.getLedgerId());
                    
                    Query deleteOPJournalFromTransation = session.createQuery("delete from Transaction t  where t.journal =:j");
                    deleteOPJournalFromTransation.setParameter("j", j);
                    
                    int opTransactionDelete = deleteOPJournalFromTransation.executeUpdate();
                    
                    Query deleteOPJournal = session.createQuery("delete from Journal j  where j =:j");
                    deleteOPJournal.setParameter("j", j);
                    
                    int opJournalDelete = deleteOPJournal.executeUpdate();
                    
                    if (opTransactionDelete < 0 || opJournalDelete < 0) {
                        deleteLedger.setMessage("Party Delete Error!");
                    } else {
                        
                        session.delete(ledger);
                        session.getTransaction().commit();
                        deleteLedger.setDeleteDone(true);
                    }
                } else {
                    deleteLedger.setMessage("The party has Purchase, Sale or Cr Dr Note");
                }
                
            } else {
                deleteLedger.setMessage("Please remove any transactions and try again!");
            }
            
            return deleteLedger;
        } catch (HibernateException px) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                px.printStackTrace();
                deleteLedger.setMessage("Error during ledger Delete. Please try again!");
                return deleteLedger;
            }
            
        } finally {
            session.close();
        }
        deleteLedger.setMessage("Error during ledger Delete. Please try again!");
        return deleteLedger;
    }
    
    public List<Transaction> findCompanyTrailBalance(Company company, Date fromDate, Date toDate) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        List<Ledger> companyLedgers = fetchAllLedgers(company.getCompanyId());
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            for (Ledger ledger : companyLedgers) {
                Transaction trans = new Transaction();
                trans.setAmount(BigDecimal.ZERO);
                trans.setLedger(ledger);
                Query sumQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger=:ledger and t.transactionDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", fromDate);
                sumQuery.setParameter("toDate", toDate);
                sumQuery.setParameter("ledger", ledger);
                BigDecimal amount = (BigDecimal) sumQuery.getSingleResult();
                trans.setAmount(amount);
                transactions.add(trans);
                
            }
            
            session.getTransaction().commit();
            session.close();
            return transactions;
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            
        } finally {
            
            session.close();
            
        }
        return transactions;
        
    }
    
    public List<Transaction> findLedgerGroupBalance(Company company, LedgerGroup lg, Date fromDate, Date toDate) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        List<Ledger> lgLedgers;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            Query query = session.getNamedQuery("findLedgerByLedgerGroupName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", lg.getLedgerGroupName());
            
            lgLedgers = query.list();
            
            for (Ledger ledger : lgLedgers) {
                Transaction trans = new Transaction();
                trans.setAmount(BigDecimal.ZERO);
                trans.setLedger(ledger);
                Query sumQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger=:ledger and t.transactionDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", fromDate);
                sumQuery.setParameter("toDate", toDate);
                sumQuery.setParameter("ledger", ledger);
                BigDecimal amount = (BigDecimal) sumQuery.getSingleResult();
                trans.setAmount(amount);
                transactions.add(trans);
            }
            
            session.getTransaction().commit();
            session.close();
            return transactions;
        } catch (HibernateException hx) {
            hx.printStackTrace();
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            
        } finally {
            
            session.close();
            
        }
        return transactions;
        
    }
    
    public Set<Ledger> fetchAgentLedgers(Company company) {
        
        Set<Ledger> ledgers = new HashSet<>();
        List<Ledger> ledgerGroups = null;
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findAgentLedgerByName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Agent");
            
            ledgerGroups = query.list();
            session.getTransaction().commit();
            session.close();
            for (Ledger ld : ledgerGroups) {
                ledgers.add(ld);
            }
            return ledgers;
        } catch (HibernateException hx) {
            
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return ledgers;
        } finally {
            
            session.close();
            
        }
        return ledgers;
    }
    
    public EventStatus createAgentLedger(Long companyId, Ledger newLedger) {
        EventStatus createUser = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            
            session.beginTransaction();
            
            Company company = (Company) session.get(Company.class,
                    companyId);
            
            Query query = session.getNamedQuery("findAgentLedgerGroupByName");
            query.setParameter("company", company);
            query.setParameter("ledgerGroupName", "Agent");
            
            LedgerGroup lg = (LedgerGroup) query.uniqueResult();
            
            newLedger.setCompany(company);
            newLedger.setEditable(false);
            newLedger.setDeletable(false);
            newLedger.setLedgerGroup(lg);
            
            session.save(newLedger);
            boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
            
            if (isAccountEnabled) {
                Journal openingBalance = new Journal();
                openingBalance.setJournalId(companyId + "AGENT" + newLedger.getLedgerId());
                Transaction newPSTransaction = new Transaction();
                newPSTransaction.setJournal(openingBalance);
                
                newPSTransaction.setAmount(BigDecimal.ZERO);
                newPSTransaction.setTransactionDate(SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
                newLedger.setOpeningBalance(BigDecimal.ZERO);
                newLedger.setOpeningBalanceDate(SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
                
                newPSTransaction.setParticulars("Opening Balance");
                newPSTransaction.setType("OP");
                
                newPSTransaction.setLedger(newLedger);
                session.save(newPSTransaction);
                openingBalance.getTransactions().add(newPSTransaction);
                
                session.save(openingBalance);
                session.save(newLedger);
                
            }
            
            session.getTransaction().commit();
            createUser.setCreateDone(true);
            
            session.close();
        }
        return createUser;
    }
    
    public EventStatus updateAgentLedger(Ledger updatedLedger) {
        EventStatus updateLedger = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            
            Ledger ledger = (Ledger) session.get(Ledger.class,
                    updatedLedger.getLedgerId());
            
            ledger.setLedgerName(updatedLedger.getLedgerName());
            ledger.setAddress(updatedLedger.getAddress());
            ledger.setGSTIN(updatedLedger.getGSTIN());
            ledger.setEmail(updatedLedger.getEmail());
            ledger.setPhone(updatedLedger.getPhone());
            ledger.setMobile(updatedLedger.getMobile());
            ledger.setCity(updatedLedger.getCity());
            ledger.setDistrict(updatedLedger.getDistrict());
            ledger.setState(updatedLedger.getState());
            ledger.setLedgerGroup(updatedLedger.getLedgerGroup());
            boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
            boolean isOpeningBalancePresent = !(updatedLedger.getOpeningBalance() == null) && !(updatedLedger.getOpeningBalanceDate() == null);
            if (isAccountEnabled && isOpeningBalancePresent) {
                ledger.setOpeningBalance(updatedLedger.getOpeningBalance());
                ledger.setOpeningBalanceDate(updatedLedger.getOpeningBalanceDate());
            }
            
            session.save(ledger);
            session.getTransaction().commit();
            updateLedger.setUpdateDone(true);
            session.close();
        }
        
        return updateLedger;
    }
    
}
