package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyBook;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.SystemPolicy;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CompanyCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    LedgerCRUD l4 = new LedgerCRUD();
    LedgerGroupCRUD lg4 = new LedgerGroupCRUD();
    UserCRUD u4 = new UserCRUD();

    public EventStatus createCompany(Company newCompany, Date financialYearStart, Date financialYearEnd) {
        EventStatus createCompany = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.save(newCompany);
            CompanyPolicy newCompanyPolicy = new CompanyPolicy();
            newCompanyPolicy.setCompany(newCompany);

            newCompanyPolicy.setUnderStockPossible(false);
            newCompanyPolicy.setAccountsExpiryDate(new Date());
            newCompanyPolicy.setSaleBillCount(100L);
            newCompanyPolicy.setProfitEnabled(true);
            newCompanyPolicy.setSaleEditApproval(true);
            newCompanyPolicy.setLedgerTransactionApproval(true);
            newCompanyPolicy.setNumeberOfDecimalPlaces(2);
            newCompanyPolicy.setPrintPaperSize("A4_P");

            //Amount Tendred
            newCompanyPolicy.setAmountTendered(true);

            newCompanyPolicy.setNumberOfInvoiceCopies(1);
            newCompanyPolicy.setFirstInvoiceWord("Original for Recipient");
            newCompanyPolicy.setSecondInvoiceWord("Duplicate for Supplier");
            newCompanyPolicy.setThirdInvoiceWord("Triplicate for Transporter");
            newCompanyPolicy.setFourthInvoiceWord("");
            newCompanyPolicy.setFifthInvoiceWord("");
            newCompanyPolicy.setTaxInvoiceWord("Tax Invoice");
            newCompanyPolicy.setBillOfSupplyWord("Bill Of Supply");
            newCompanyPolicy.setEstimateBillWord("Estimate Order");
            newCompanyPolicy.setTaxInvoiceSequenceStart(new Long(1));
            newCompanyPolicy.setPurchaseSequenceStart(new Long(1));
            newCompanyPolicy.setCrDrSequenceStart(new Long(1));
            newCompanyPolicy.setEstimateSequenceStart(new Long(1));
            newCompanyPolicy.setBosSequenceStart(new Long(1));
            newCompanyPolicy.setTaxInvoicePrefix("");
            newCompanyPolicy.setPurchasePrefix("");
            newCompanyPolicy.setCrDrPrefix("");
            newCompanyPolicy.setEstimatePrefix("");
            newCompanyPolicy.setBosPrefix("");
            newCompanyPolicy.setTaxInvoiceSuffix("");
            newCompanyPolicy.setPurchaseSuffix("");
            newCompanyPolicy.setCrDrSuffix("");
            newCompanyPolicy.setEstimateSuffix("");
            newCompanyPolicy.setBosSuffix("");
            String[] words = newCompany.getAddress().split("/~/");

            newCompanyPolicy.setA4PAddressLineOne(words[0]);
            newCompanyPolicy.setA4PAddressLineTwo(words[1]);
            newCompanyPolicy.setA4PAddressLineThree(words[2]);

            if (SystemPolicyUtil.getSystemPolicy().isTrialPackage()) {
                Calendar ex = Calendar.getInstance();
                ex.add(Calendar.DATE, 7);
                Date accoutntExpiryDate = ex.getTime();
                newCompanyPolicy.setAccountsExpiryDate(accoutntExpiryDate);
                newCompanyPolicy.setFinancialYearStart(financialYearStart);
                newCompanyPolicy.setFinancialYearEnd(financialYearEnd);
            } else {
                Calendar ex = Calendar.getInstance();
                ex.add(Calendar.YEAR, 25);
                Date accoutntExpiryDate = ex.getTime();
                newCompanyPolicy.setAccountsExpiryDate(accoutntExpiryDate);
                newCompanyPolicy.setFinancialYearStart(financialYearStart);
                newCompanyPolicy.setFinancialYearEnd(financialYearEnd);
            }

            session.save(newCompanyPolicy);

            CompanyBook newCompanyBook = new CompanyBook();
            newCompanyBook.setBosCount(new Long(0));
            newCompanyBook.setBosIdCount(new Long(0));
            newCompanyBook.setTaxInvoiceCount(new Long(0));
            newCompanyBook.setTaxInvoiceIdCount(new Long(0));
            newCompanyBook.setEstimateCount(new Long(0));
            newCompanyBook.setEstimateIdCount(new Long(0));
            newCompanyBook.setCdNoteCount(new Long(0));
            newCompanyBook.setCdNoteIdCount(new Long(0));
            newCompanyBook.setPurchaseCount(new Long(0));
            newCompanyBook.setPurchaseIdCount(new Long(0));
            newCompanyBook.setJournalCount(new Long(0));
            newCompanyBook.setJournalIdCount(new Long(0));

            newCompanyBook.setLastTaxInvoiceDate(newCompanyPolicy.getFinancialYearStart());
            newCompanyBook.setLastBOSDate(newCompanyPolicy.getFinancialYearStart());
            newCompanyBook.setLastEstimateDate(newCompanyPolicy.getFinancialYearStart());
            newCompanyBook.setLastCDNoteDate(newCompanyPolicy.getFinancialYearStart());

            newCompanyBook.setBalance(BigDecimal.ZERO);
            newCompanyBook.setGrossProfit(BigDecimal.ZERO);
            newCompanyBook.setNetProfit(BigDecimal.ZERO);

            newCompanyBook.setCompany(newCompany);

            //-----------Create a Primary Godown------------------//
            GoDown newGodown = new GoDown();
            newGodown.setGoDownName("Primary");
            newGodown.setEditable(true);
            newGodown.setCompany(newCompany);
            newCompany.getGodowns().add(newGodown);
            session.save(newGodown);

//            GoDown secondGodown = new GoDown();
//            secondGodown.setGoDownName("Secondary");
//            secondGodown.setEditable(true);
//            secondGodown.setCompany(newCompany);
//            newCompany.getGodowns().add(secondGodown);
//            session.save(secondGodown);
            ProductGroup newPG = new ProductGroup();
            newPG.setCompany(newCompany);
            newPG.setProductGroupName("No Group");
            session.save(newPG);

            session.save(newCompanyBook);

            String[] defaultLedgerGroups = {"Fixed Assets", "Branch and Division", "Profit and Loss Account", "Current Assets", "Current Liabilities", "Investments & Advances", "Misc. Expenses", "Capital", "Loans and Borrowing", "Suspense", "Revenue Account", "Employees"};

            for (String ledgerGroupName : defaultLedgerGroups) {
                LedgerGroup lg = new LedgerGroup(ledgerGroupName);
                lg.setCompany(newCompany);
                session.save(lg);

                if (!ledgerGroupName.equalsIgnoreCase("Party")) {
                    lg.setEligibleForChildren(true);
                }
                if (!ledgerGroupName.equalsIgnoreCase("Employees")) {
                    lg.setEligibleForChildren(true);
                    Ledger newCIHLedger= new Ledger();
                    newCIHLedger.setLedgerGroup(lg);
                    newCIHLedger.setLedgerName("No Party");
                    newCIHLedger.setEligibleForChildren(true);
                    newCIHLedger.setCompany(newCompany);
                    newCIHLedger.setExemptFromApproval(true);
                    newCIHLedger.setTransactionApprovalRequired(false);
                    newCIHLedger.setOpeningBalance(BigDecimal.ZERO);
                    newCIHLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newCIHLedger);
                    lg.getLedgers().add(newCIHLedger);
                }

                if (ledgerGroupName.equalsIgnoreCase("Current Assets")) {

                    LedgerGroup calg1 = new LedgerGroup("Sundry Debtors");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    LedgerGroup calg7 = new LedgerGroup("Trade Debtors");
                    calg7.setCompany(newCompany);
                    calg7.setParentLedgerGroup(calg1);
                    calg7.setEligibleForChildren(true);
                    session.save(calg7);
                    calg1.getChildLedgerGroups().add(calg7);
                    session.save(calg1);

                    LedgerGroup calg2 = new LedgerGroup("Bank");
                    calg2.setCompany(newCompany);
                    calg2.setParentLedgerGroup(lg);
                    calg2.setEligibleForChildren(true);
                    session.save(calg2);

                    LedgerGroup calg8 = new LedgerGroup("Term Loan");
                    calg8.setCompany(newCompany);
                    calg8.setParentLedgerGroup(calg2);
                    calg8.setEligibleForChildren(true);
                    session.save(calg8);
                    calg2.getChildLedgerGroups().add(calg8);
                    session.save(calg2);

                    LedgerGroup calg3 = new LedgerGroup("Cash");
                    calg3.setCompany(newCompany);
                    calg3.setParentLedgerGroup(lg);
                    calg3.setEligibleForChildren(true);
                    session.save(calg3);

                    Ledger newCIHLedger = new Ledger("Cash In Hand");
                    newCIHLedger.setLedgerGroup(calg3);
                    newCIHLedger.setEligibleForChildren(true);
                    newCIHLedger.setCompany(newCompany);
                    newCIHLedger.setExemptFromApproval(true);
                    newCIHLedger.setTransactionApprovalRequired(false);
                    newCIHLedger.setOpeningBalance(BigDecimal.ZERO);
                    newCIHLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newCIHLedger);
                    calg3.getLedgers().add(newCIHLedger);
                    session.save(calg3);

                    LedgerGroup calg4 = new LedgerGroup("Deposits");
                    calg4.setCompany(newCompany);
                    calg4.setParentLedgerGroup(lg);
                    calg4.setEligibleForChildren(true);
                    session.save(calg4);

                    LedgerGroup calg5 = new LedgerGroup("Loans and Advances");
                    calg5.setCompany(newCompany);
                    calg5.setParentLedgerGroup(lg);
                    calg5.setEligibleForChildren(true);
                    session.save(calg5);

                    LedgerGroup calg6 = new LedgerGroup("Stock in Hand");
                    calg6.setCompany(newCompany);
                    calg6.setParentLedgerGroup(lg);
                    calg6.setEligibleForChildren(true);
                    session.save(calg6);

                    LedgerGroup calg9 = new LedgerGroup("Opening Stock");
                    calg9.setCompany(newCompany);
                    calg9.setParentLedgerGroup(calg6);
                    calg9.setEligibleForChildren(true);
                    session.save(calg9);
                    calg6.getChildLedgerGroups().add(calg9);
                    session.save(calg6);

                    Ledger newOSLedger = new Ledger("Opening Stock");
                    newOSLedger.setLedgerGroup(calg9);
                    newOSLedger.setEligibleForChildren(true);
                    newOSLedger.setCompany(newCompany);
                    newOSLedger.setExemptFromApproval(true);
                    newOSLedger.setTransactionApprovalRequired(false);
                    newOSLedger.setOpeningBalance(BigDecimal.ZERO);
                    newOSLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newOSLedger);
                    calg9.getLedgers().add(newOSLedger);
                    session.save(calg9);

                    LedgerGroup calg10 = new LedgerGroup("Closing Stock");
                    calg10.setCompany(newCompany);
                    calg10.setParentLedgerGroup(calg6);
                    calg10.setEligibleForChildren(true);
                    session.save(calg10);
                    calg6.getChildLedgerGroups().add(calg10);
                    session.save(calg6);

                    Ledger newCSLedger = new Ledger("Closing Stock");
                    newCSLedger.setLedgerGroup(calg10);
                    newCSLedger.setEligibleForChildren(true);
                    newCSLedger.setCompany(newCompany);
                    newCSLedger.setExemptFromApproval(true);
                    newCSLedger.setTransactionApprovalRequired(false);
                    newCSLedger.setOpeningBalance(BigDecimal.ZERO);
                    newCSLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newCSLedger);
                    calg10.getLedgers().add(newCSLedger);
                    session.save(calg10);

                    lg.getChildLedgerGroups().add(calg1);
                    lg.getChildLedgerGroups().add(calg2);
                    lg.getChildLedgerGroups().add(calg3);
                    lg.getChildLedgerGroups().add(calg4);
                    lg.getChildLedgerGroups().add(calg5);
                    lg.getChildLedgerGroups().add(calg6);

                }
                if (ledgerGroupName.equalsIgnoreCase("Capital")) {

                    LedgerGroup calg1 = new LedgerGroup("Equity");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    LedgerGroup calg2 = new LedgerGroup("Reserves and Surplus");
                    calg2.setCompany(newCompany);
                    calg2.setParentLedgerGroup(lg);
                    calg2.setEligibleForChildren(true);
                    session.save(calg2);

                    LedgerGroup calg3 = new LedgerGroup("Retained Earnings");
                    calg3.setCompany(newCompany);
                    calg3.setParentLedgerGroup(lg);
                    calg3.setEligibleForChildren(true);
                    session.save(calg3);

                    lg.getChildLedgerGroups().add(calg1);
                    lg.getChildLedgerGroups().add(calg2);
                    lg.getChildLedgerGroups().add(calg3);

                }
                if (ledgerGroupName.equalsIgnoreCase("Loans and Borrowing")) {

                    LedgerGroup calg1 = new LedgerGroup("Bank OD Accounts");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    LedgerGroup calg2 = new LedgerGroup("Secured Loan");
                    calg2.setCompany(newCompany);
                    calg2.setParentLedgerGroup(lg);
                    calg2.setEligibleForChildren(true);
                    session.save(calg2);

                    LedgerGroup calg3 = new LedgerGroup("Unsecured Loan");
                    calg3.setCompany(newCompany);
                    calg3.setParentLedgerGroup(lg);
                    calg3.setEligibleForChildren(true);
                    session.save(calg3);

                    lg.getChildLedgerGroups().add(calg1);
                    lg.getChildLedgerGroups().add(calg2);
                    lg.getChildLedgerGroups().add(calg3);

                }
                if (ledgerGroupName.equalsIgnoreCase("Profit and Loss Account")) {

                    Ledger newNPLedger = new Ledger("Net Profit");
                    newNPLedger.setLedgerGroup(lg);
                    newNPLedger.setEligibleForChildren(true);
                    newNPLedger.setCompany(newCompany);
                    newNPLedger.setExemptFromApproval(true);
                    newNPLedger.setTransactionApprovalRequired(false);
                    newNPLedger.setOpeningBalance(BigDecimal.ZERO);
                    newNPLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newNPLedger);
                    lg.getLedgers().add(newNPLedger);
                    session.save(lg);

                }
                if (ledgerGroupName.equalsIgnoreCase("Investments & Advances")) {

                    LedgerGroup calg1 = new LedgerGroup("Trade Advances");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    lg.getChildLedgerGroups().add(calg1);

                }
                if (ledgerGroupName.equalsIgnoreCase("Current Liabilities")) {

                    LedgerGroup calg1 = new LedgerGroup("Sundry Creditors");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    LedgerGroup calg5 = new LedgerGroup("Agent");
                    calg5.setCompany(newCompany);
                    calg5.setParentLedgerGroup(calg1);
                    calg5.setEligibleForChildren(true);
                    session.save(calg5);
                    calg1.getChildLedgerGroups().add(calg5);
                    session.save(calg1);

                    LedgerGroup calg2 = new LedgerGroup("Duties and Taxes");
                    calg2.setCompany(newCompany);
                    calg2.setParentLedgerGroup(lg);
                    calg2.setEligibleForChildren(true);
                    session.save(calg2);

                    LedgerGroup calg3 = new LedgerGroup("Provision");
                    calg3.setCompany(newCompany);
                    calg3.setParentLedgerGroup(lg);
                    calg3.setEligibleForChildren(true);
                    session.save(calg3);

                    LedgerGroup calg4 = new LedgerGroup("Relative Deposits");
                    calg4.setCompany(newCompany);
                    calg4.setParentLedgerGroup(lg);
                    calg4.setEligibleForChildren(true);
                    session.save(calg4);

                    lg.getChildLedgerGroups().add(calg1);
                    lg.getChildLedgerGroups().add(calg2);
                    lg.getChildLedgerGroups().add(calg3);
                    lg.getChildLedgerGroups().add(calg4);

                }
                if (ledgerGroupName.equalsIgnoreCase("Revenue Account")) {

                    LedgerGroup calg1 = new LedgerGroup("Direct Expenses");
                    calg1.setCompany(newCompany);
                    calg1.setParentLedgerGroup(lg);
                    calg1.setEligibleForChildren(true);
                    session.save(calg1);

                    LedgerGroup calg2 = new LedgerGroup("Indirect Expenses");
                    calg2.setCompany(newCompany);
                    calg2.setParentLedgerGroup(lg);
                    calg2.setEligibleForChildren(true);
                    session.save(calg2);

                    Ledger newBCLedger = new Ledger("Bank Commission");
                    newBCLedger.setLedgerGroup(calg2);
                    newBCLedger.setEligibleForChildren(true);
                    newBCLedger.setExemptFromApproval(true);
                    newBCLedger.setTransactionApprovalRequired(false);
                    newBCLedger.setOpeningBalance(BigDecimal.ZERO);
                    newBCLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    newBCLedger.setCompany(newCompany);

                    session.save(newBCLedger);
                    calg2.getLedgers().add(newBCLedger);
                    session.save(calg2);

                    Ledger newBILedger = new Ledger("Bank Interest");
                    newBILedger.setLedgerGroup(calg2);
                    newBILedger.setEligibleForChildren(true);
                    newBILedger.setCompany(newCompany);
                    newBILedger.setExemptFromApproval(true);
                    newBILedger.setTransactionApprovalRequired(false);
                    newBILedger.setOpeningBalance(BigDecimal.ZERO);
                    newBILedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newBILedger);
                    calg2.getLedgers().add(newBILedger);
                    session.save(calg2);

                    LedgerGroup calg8 = new LedgerGroup("Interest");
                    calg8.setCompany(newCompany);
                    calg8.setParentLedgerGroup(calg2);
                    calg8.setEligibleForChildren(true);
                    session.save(calg8);
                    calg2.getChildLedgerGroups().add(calg8);
                    session.save(calg2);

                    Ledger newILedger = new Ledger("Interest");
                    newILedger.setLedgerGroup(calg8);
                    newILedger.setEligibleForChildren(true);
                    newILedger.setCompany(newCompany);
                    newILedger.setExemptFromApproval(true);
                    newILedger.setTransactionApprovalRequired(false);
                    newILedger.setOpeningBalance(BigDecimal.ZERO);
                    newILedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newILedger);
                    calg8.getLedgers().add(newILedger);
                    session.save(calg8);

                    LedgerGroup calg3 = new LedgerGroup("Direct Income");
                    calg3.setCompany(newCompany);
                    calg3.setParentLedgerGroup(lg);
                    calg3.setEligibleForChildren(true);
                    session.save(calg3);

                    LedgerGroup calg4 = new LedgerGroup("Indirect Income");
                    calg4.setCompany(newCompany);
                    calg4.setParentLedgerGroup(lg);
                    calg4.setEligibleForChildren(true);
                    session.save(calg4);

                    LedgerGroup calg5 = new LedgerGroup("Purchase");
                    calg5.setCompany(newCompany);
                    calg5.setParentLedgerGroup(lg);
                    calg5.setEligibleForChildren(true);
                    session.save(calg5);

                    Ledger newILFLedger = new Ledger("Inward Lorry Frieght");
                    newILFLedger.setLedgerGroup(calg5);
                    newILFLedger.setEligibleForChildren(true);
                    newILFLedger.setCompany(newCompany);
                    newILFLedger.setExemptFromApproval(true);
                    newILFLedger.setTransactionApprovalRequired(false);
                    newILFLedger.setOpeningBalance(BigDecimal.ZERO);
                    newILFLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newILFLedger);
                    calg5.getLedgers().add(newILFLedger);
                    session.save(calg5);

                    Ledger newPLedger = new Ledger("Purchase");
                    newPLedger.setLedgerGroup(calg5);
                    newPLedger.setEligibleForChildren(true);
                    newPLedger.setCompany(newCompany);
                    newPLedger.setExemptFromApproval(true);
                    newPLedger.setTransactionApprovalRequired(false);
                    newPLedger.setOpeningBalance(BigDecimal.ZERO);
                    newPLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newPLedger);
                    calg5.getLedgers().add(newPLedger);
                    session.save(calg5);

                    LedgerGroup calg6 = new LedgerGroup("Sales");
                    calg6.setCompany(newCompany);
                    calg6.setParentLedgerGroup(lg);
                    calg6.setEligibleForChildren(true);
                    session.save(calg6);

                    Ledger newSLedger = new Ledger("Sales");
                    newSLedger.setLedgerGroup(calg6);
                    newSLedger.setEligibleForChildren(true);
                    newSLedger.setCompany(newCompany);
                    newSLedger.setExemptFromApproval(true);
                    newSLedger.setTransactionApprovalRequired(false);
                    newSLedger.setOpeningBalance(BigDecimal.ZERO);
                    newSLedger.setOpeningBalanceDate(newCompanyPolicy.getFinancialYearStart());
                    session.save(newSLedger);
                    calg6.getLedgers().add(newSLedger);
                    session.save(calg6);

                    LedgerGroup calg7 = new LedgerGroup("Income");
                    calg7.setCompany(newCompany);
                    calg7.setParentLedgerGroup(lg);
                    calg7.setEligibleForChildren(true);
                    session.save(calg7);

                    lg.getChildLedgerGroups().add(calg1);
                    lg.getChildLedgerGroups().add(calg2);
                    lg.getChildLedgerGroups().add(calg3);
                    lg.getChildLedgerGroups().add(calg4);
                    lg.getChildLedgerGroups().add(calg5);
                    lg.getChildLedgerGroups().add(calg6);

                }

                session.save(lg);
                if (ledgerGroupName.equalsIgnoreCase("General")) {

                    String[] defaultLedgers = {"Bank Commission",
                        "Bank Interest",
                        "Cash",
                        "Cash Adjustment",
                        "Closing Stock",
                        "Interest",
                        "Inward Lorry Freight",
                        "Net Profit",
                        "Opening Stock",
                        "Purchase",
                        "Sales", "Stock Adjustment"};
                    for (String defaultLedger : defaultLedgers) {
                        Ledger newLedger = new Ledger(defaultLedger);
                        newLedger.setLedgerGroup(lg);
                        newLedger.setEligibleForChildren(true);
                        newLedger.setCompany(newCompany);
                        session.save(newLedger);
                        lg.getLedgers().add(newLedger);
                    }
                }

                session.save(lg);

            }

            LedgerGroup gocLG = new LedgerGroup("Group of Companies");
            gocLG.setCompany(newCompany);
            session.save(gocLG);

            //New Company ---> Exisiting Companies
            for (Company com : fetchAllCompaniesWithLedgerGroups()) {

                Ledger newLedger = new Ledger();
                newLedger.setLedgerName(newCompany.getCompanyName());
                newLedger.setAddress(newCompany.getAddress());
                newLedger.setPhone(newCompany.getPhone());
                newLedger.setEmail(newCompany.getEmail());
                newLedger.setMobile(newCompany.getMobile());
                newLedger.setDistrict(newCompany.getDistrict());
                newLedger.setState(newCompany.getState());
                newLedger.setGSTIN(newCompany.getGSTIN());
                newLedger.setCity(newCompany.getCity());
                LedgerGroup companyGOCLG = lg4.fetchLGByName(com, "Group of Companies");

                newLedger.setLedgerGroup(companyGOCLG);
                newLedger.setCompany(companyGOCLG.getCompany());

                session.save(newLedger);
                Journal openingBalance = new Journal();
                openingBalance.setJournalId(com.getCompanyId() + "LEDGER" + newLedger.getLedgerId());
                Transaction newPSTransaction = new Transaction();
                newPSTransaction.setJournal(openingBalance);
                newPSTransaction.setTransactionDate(newCompanyPolicy.getFinancialYearStart());
                newPSTransaction.setParticulars("Opening Balance");
                newPSTransaction.setType("OP");
                newPSTransaction.setAmount(BigDecimal.ZERO);
                newPSTransaction.setLedger(newLedger);
                session.save(newPSTransaction);
                openingBalance.getTransactions().add(newPSTransaction);

                session.save(openingBalance);
            }
            //Existing Company ---> New Company
            for (Company com : fetchAllCompaniesWithLedgerGroups()) {

                Ledger newLedger = new Ledger();
                newLedger.setLedgerName(com.getCompanyName());
                newLedger.setAddress(com.getAddress());
                newLedger.setPhone(com.getPhone());
                newLedger.setEmail(com.getEmail());
                newLedger.setMobile(com.getMobile());
                newLedger.setDistrict(com.getDistrict());
                newLedger.setState(com.getState());
                newLedger.setGSTIN(com.getGSTIN());
                newLedger.setCity(com.getCity());
                newLedger.setLedgerGroup(gocLG);

                newLedger.setCompany(newCompany);
                session.save(newLedger);

                Journal openingBalance = new Journal();
                openingBalance.setJournalId(com.getCompanyId() + "LEDGER" + newLedger.getLedgerId());
                Transaction newPSTransaction = new Transaction();
                newPSTransaction.setJournal(openingBalance);
                newPSTransaction.setTransactionDate(newCompanyPolicy.getFinancialYearStart());
                newPSTransaction.setParticulars("Opening Balance");
                newPSTransaction.setType("OP");
                newPSTransaction.setAmount(BigDecimal.ZERO);
                newPSTransaction.setLedger(newLedger);
                session.save(newPSTransaction);
                openingBalance.getTransactions().add(newPSTransaction);

                session.save(openingBalance);

            }

            for (AppUser user : u4.fetchAllUsers()) {
                CompanyUserRole cur = new CompanyUserRole();
                cur.setUser(user);
                cur.setCompany(newCompany);
                if (user.isAdmin()) {
                    cur.makeAdmin();
                }
                session.save(cur);
            }
            session.saveOrUpdate(newCompany);
            session.getTransaction().commit();
            session.close();
            createCompany.setCreateDone(true);
        }

        return createCompany;
    }

    public EventStatus updateCompany(Company updatedCompany) {
        EventStatus updateCompany = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            session.update(updatedCompany);
            session.getTransaction().commit();
            session.close();
            updateCompany.setUpdateDone(true);
        }

        return updateCompany;
    }

    public List<Company> fetchAllCompanies() {
        List<Company> companies = new ArrayList<>();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            // session.createCriteria(Company.class).list();

            Query query = session.createQuery("from Company");
            companies = query.list();
            session.getTransaction().commit();
            session.close();

        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
        }
        return companies;

    }

    public List<Company> fetchAllCompaniesWithLedgerGroups() {
        List<Company> companies = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            // session.createCriteria(Company.class).list();

            companies = session.createQuery("from Company").list();
            for (Company com : companies) {
                Hibernate.initialize(com.getLedgerGroups());
            }

            session.getTransaction().commit();
            session.close();

        }
        return companies;

    }

    public Company fetchACompany(Long companyId) {
        Company company;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            company = (Company) session.get(Company.class, companyId);
            session.getTransaction().commit();
            session.close();

        }
        return company;
    }

}
