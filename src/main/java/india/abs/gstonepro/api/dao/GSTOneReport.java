/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.GstReport;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.ui.utils.StateCode;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class GSTOneReport {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<GstReport> getB2BDetails(Company co, Date fromDate, Date toDate) {

        List<GstReport> b2b = new ArrayList<GstReport>();
        List<PurchaseSale> pss = new ArrayList<PurchaseSale>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findB2BPSByDate");
            query.setParameter("type", "TX-I");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
            query.setParameter("withCancelledBill", false);
            query.setParameter("isB2B", true);

            pss = query.list();

            for (PurchaseSale ps : pss) {
                Hibernate.initialize(ps.getPsLineItems());

                for (PurchaseSaleLineItem psLi : ps.getPsLineItems()) {
                    GstReport gsr = new GstReport();
                    gsr.setGstinOrUin(ps.getLedger().getGSTIN());
                    gsr.setInvoiceNumber(ps.getBillNo());
                    gsr.setInvoiceDate(ps.getPsDate());
                    gsr.setInvoiceValue(ps.getBillAmount());
                    gsr.setPlaceOfSupply(ps.getLedger().getState());
                    if (ps.isReverseChargeApplicable()) {
                        gsr.setReverseCharge("Y");
                    } else {
                        gsr.setReverseCharge("N");
                    }
                    gsr.setInvoiceType("Regular");
                    gsr.seteCommerceGSTIN(null);
                    gsr.setCessAmount(psLi.getCessAmount());
                    gsr.setGstRate(new BigDecimal(String.valueOf(psLi.getIgstPercentage())));
                    gsr.setTaxableValue(psLi.getValue());
                    b2b.add(gsr);

                }

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2b;
    }

    public List<GstReport> getB2CDetails(Company co, Date fromDate, Date toDate) {

        List<GstReport> b2c = new ArrayList<GstReport>();
        List<PurchaseSale> pss = new ArrayList<PurchaseSale>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findB2BPSByDate");
            query.setParameter("type", "TX-I");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
            query.setParameter("withCancelledBill", false);
            query.setParameter("isB2B", false);

            pss = query.list();

            for (PurchaseSale ps : pss) {
                if (ps.getBillAmount().intValue() < 250000) {
                    Hibernate.initialize(ps.getPsLineItems());

                    for (PurchaseSaleLineItem psLi : ps.getPsLineItems()) {
                        GstReport gsr = new GstReport();
                        gsr.setInvoiceType("OE");
                        if (ps.getLedger() == null) {
                            gsr.setPlaceOfSupply(ps.getNoPartyState());
                        } else {
                            gsr.setPlaceOfSupply(ps.getLedger().getState());
                        }
                        gsr.seteCommerceGSTIN(null);
                        gsr.setGstRate(new BigDecimal(String.valueOf(psLi.getIgstPercentage())));
                        gsr.setTaxableValue(psLi.getValue());
                        gsr.setCessAmount(psLi.getCessAmount());
                        b2c.add(gsr);
                    }
                }

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2c;
    }

    public List<GstReport> getB2CLDetails(Company co, Date fromDate, Date toDate) throws ParseException {

        List<GstReport> b2cl = new ArrayList<GstReport>();
        List<PurchaseSale> pss = new ArrayList<PurchaseSale>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findB2BPSByDate");
            query.setParameter("type", "TX-I");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
            query.setParameter("withCancelledBill", false);
            query.setParameter("isB2B", false);

            pss = query.list();

            for (PurchaseSale ps : pss) {
                if (ps.getBillAmount().intValue() >= 250000) {
                    Hibernate.initialize(ps.getPsLineItems());

                    for (PurchaseSaleLineItem psLi : ps.getPsLineItems()) {
                        GstReport gsr = new GstReport();

                        Date invDate = ps.getPsDate();
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MMM-yy");
                        String strDate = sft.format(invDate);
                        gsr.setInvoiceDate(sft.parse(strDate));
                        gsr.setInvoiceNumber(ps.getBillNo());
                        gsr.setInvoiceDate(ps.getPsDate());
                        gsr.setInvoiceValue(ps.getBillAmount());
                        gsr.setGstRate(new BigDecimal(String.valueOf(psLi.getIgstPercentage())));
                        if (ps.getLedger() == null) {
                            gsr.setPlaceOfSupply(ps.getNoPartyState());
                        } else {
                            gsr.setPlaceOfSupply(ps.getLedger().getState());
                        }
                        gsr.setCessAmount(ps.getTotalCessAmount());
                        gsr.setCessAmount(psLi.getCessAmount());
                        gsr.seteCommerceGSTIN(null);
//                        gsr.setGstRate(psLi.getIgstPercentage());
                        gsr.setTaxableValue(psLi.getValue());
                        b2cl.add(gsr);
                    }
                }

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2cl;
    }

    public List<GstReport> getHSNSummary(Company co, Date fromDate, Date toDate) {
        ProductCRUD p4 = new ProductCRUD();
        List<GstReport> hsn = new ArrayList<GstReport>();
        List<PurchaseSaleLineItem> psLis = new ArrayList<PurchaseSaleLineItem>();
        List<Product> products = p4.fetchAllProducts(co.getCompanyId());
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            for (Product pro : products) {
                Query query = session.getNamedQuery("findPSLIByProduct");
                query.setParameter("type", "TX-I");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", co);
                query.setParameter("cancelled", false);
                query.setParameter("product", pro);

                psLis = query.list();
                GstReport gsr = new GstReport();
                if (pro.getHsnCode() == null) {
                    gsr.setDescription(pro.getName());

                } else {
                    gsr.setHsn(pro.getHsnCode());
                    gsr.setDescription("-");
                }
                gsr.setUqc(pro.getUQC2());
                gsr.setTotalQuantity(BigDecimal.ZERO);
                gsr.setTotalValue(BigDecimal.ZERO);
                gsr.setTaxableValue(BigDecimal.ZERO);
                gsr.setIntegratedTaxAmount(BigDecimal.ZERO);
                gsr.setCentralTaxAmount(BigDecimal.ZERO);
                gsr.setStateTaxAmount(BigDecimal.ZERO);
                gsr.setCessAmount(BigDecimal.ZERO);

                for (PurchaseSaleLineItem psLi : psLis) {

                    gsr.setTotalQuantity(gsr.getTotalQuantity().add(BigDecimal.valueOf(psLi.getStockQuantity())));
                    gsr.setTotalValue(gsr.getTotalValue().add(psLi.getValue()));
                    gsr.setTaxableValue(gsr.getTaxableValue().add(psLi.getValue()));
                    gsr.setIntegratedTaxAmount(gsr.getIntegratedTaxAmount().add(psLi.getIgstValue()));
                    gsr.setCentralTaxAmount(gsr.getCentralTaxAmount().add(psLi.getCgstValue()));
                    gsr.setStateTaxAmount(gsr.getStateTaxAmount().add(psLi.getSgstValue()));
                    String strCess = "0";
                    if (!(gsr.getCessAmount() == null)) {
                        strCess = gsr.getCessAmount() + "";
                    }
                    String strPiCess = "0";
                    if (!(psLi.getCessAmount() == null)) {
                        strPiCess = psLi.getCessAmount() + "";
                    }
                    BigDecimal gsrCess = new BigDecimal(strCess);
                    BigDecimal piCess = new BigDecimal(strPiCess);
                    gsr.setCessAmount(gsrCess.add(piCess));
                }
                hsn.add(gsr);
            }

            session.getTransaction().commit();
            return hsn;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return null;
    }

    public List<GstReport> getB2B(Company co, Date fromDate, Date toDate) {

        List<GstReport> b2b = new ArrayList<GstReport>();
        List<PurchaseSale> pss = new ArrayList<PurchaseSale>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findB2BPSByDate");
            query.setParameter("type", "TX-I");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
            query.setParameter("withCancelledBill", false);
            query.setParameter("isB2B", true);

            pss = query.list();

            for (PurchaseSale ps : pss) {

                GstReport gsr = new GstReport();
                gsr.setGstinOrUin(ps.getLedger().getGSTIN());
                gsr.setInvoiceNumber(ps.getBillNo());
                gsr.setInvoiceDate(ps.getPsDate());
                gsr.setInvoiceValue(ps.getCalculatedTotalValue());
                String code = new StateCode().getStateCode(ps.getLedger().getState());
                gsr.setPlaceOfSupply(code + "-" + ps.getLedger().getState());
                if (ps.isReverseChargeApplicable()) {
                    gsr.setReverseCharge("Y");
                } else {
                    gsr.setReverseCharge("N");
                }
                gsr.setInvoiceType("Regular");
                gsr.seteCommerceGSTIN(null);
                gsr.setCessAmount(ps.getTotalCessAmount());
                if (ps.isIGST) {
                    gsr.setGstRate(ps.getIgstValue());
                } else {
                    gsr.setGstRate(ps.getCgstValue().add(ps.getSgstValue()));
                }

                gsr.setTaxableValue(ps.getBillAmount());
                b2b.add(gsr);

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2b;
    }

    public List<GstReport> getCdnr(Company co, Date fromDate, Date toDate, String noteType) {

        List<GstReport> cdnr = new ArrayList<GstReport>();
        List<CreditDebitNote> cdnList = new ArrayList<CreditDebitNote>();
        List<CreditDebitNoteLineItem> cdLineItem = new ArrayList<CreditDebitNoteLineItem>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllCDNRByDateAndType");
            query.setParameter("company", co);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("isRegistered", true);
            query.setParameter("noteType", noteType);
            cdnList = query.list();

            for (CreditDebitNote cdn : cdnList) {
                Query cdLiQuery = session.getNamedQuery("findAllCDNLinetems");
                cdLiQuery.setParameter("cdNote", cdn);
                cdLineItem = cdLiQuery.list();
                for (CreditDebitNoteLineItem cdlt : cdLineItem) {
                    GstReport gsr = new GstReport();
                    gsr.setGstinOrUin(cdn.getLedger().getGSTIN());
                    gsr.setInvoiceNumber(cdlt.getInvoiceNumber());
                    gsr.setInvoiceDate(cdlt.getInvoiceDate());
                    gsr.setNoteDate(cdn.getNoteDate());
                    gsr.setNoteNumber(cdn.getCdNoteNo());
                    gsr.setNoteValue(cdn.getNoteValue());
                    gsr.setNoteReason(cdlt.getGstReason());
                    gsr.setPlaceOfSupply(cdlt.getGstPOS());
                    gsr.setNoteType(cdn.getNoteType());
                    gsr.setGstRate(new BigDecimal(cdlt.getIgstPercentage()));
                    gsr.setTaxableValue(cdlt.getValue());
                    gsr.setCessAmount(cdlt.getCessAmount());
                    gsr.setCdnNumber(cdn.getCdNoteNo());
                    gsr.setIntegratedTaxAmount(cdlt.getIgstValue());
                    gsr.setCentralTaxAmount(cdlt.getCgstValue());
                    gsr.setStateTaxAmount(cdlt.getSgstValue());
                    cdnr.add(gsr);
                }
            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return cdnr;
    }

    public List<GstReport> getCdnUr(Company co, Date fromDate, Date toDate) {

        List<GstReport> cdnr = new ArrayList<GstReport>();
        List<CreditDebitNote> cdnList = new ArrayList<CreditDebitNote>();
        List<CreditDebitNoteLineItem> cdLineItem = new ArrayList<CreditDebitNoteLineItem>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllCDNRByDate");
            query.setParameter("company", co);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("isRegistered", false);
            cdnList = query.list();

            for (CreditDebitNote cdn : cdnList) {
                Query cdLiQuery = session.getNamedQuery("findAllCDNLinetems");
                cdLiQuery.setParameter("cdNote", cdn);
                cdLineItem = cdLiQuery.list();
                for (CreditDebitNoteLineItem cdlt : cdLineItem) {
                    GstReport gsr = new GstReport();
                    gsr.setNoteNumber(cdn.getCdNoteNo());
                    gsr.setNoteDate(cdn.getNoteDate());
                    gsr.setNoteType(cdn.getNoteType());
                    gsr.setInvoiceNumber(cdlt.getInvoiceNumber());
                    gsr.setInvoiceDate(cdlt.getInvoiceDate());
                    gsr.setNoteReason(cdlt.getGstReason());
                    gsr.setPlaceOfSupply(cdlt.getGstPOS());
                    gsr.setNoteValue(cdn.getNoteValue());
                    gsr.setGstRate(new BigDecimal(cdlt.getIgstPercentage()));
                    gsr.setTaxableValue(cdlt.getValue());
                    gsr.setCessAmount(cdlt.getCessAmount());
                    cdnr.add(gsr);
                }
            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return cdnr;
    }

    public List<GstReport> getGst2B2BDetails(Company co, Date fromDate, Date toDate) {

        List<GstReport> b2b = new ArrayList<>();
        List<PurchaseInvoice> pss = new ArrayList<>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findPIByDate");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
//            query.setParameter("isRegistered", false);
            pss = query.list();

            for (PurchaseInvoice ps : pss) {
                Hibernate.initialize(ps.getPiLineItems());
                for (PurchaseInvoiceLineItem piLi : ps.getPiLineItems()) {
                    GstReport gsr = new GstReport();
                    gsr.setGstinOrUin(ps.getLedger().getGSTIN());
                    gsr.setInvoiceNumber(ps.getPurchaseInvoiceNo());
                    gsr.setInvoiceDate(ps.getPurchaseInvoiceDate());
                    gsr.setInvoiceValue(ps.getPuchaseInvoiceAmount());
                    gsr.setPlaceOfSupply(ps.getLedger().getState());
                    if (ps.isReverseChargeApplicable()) {
                        gsr.setReverseCharge("Y");
                    } else {
                        gsr.setReverseCharge("N");
                    }
                    gsr.setInvoiceType("Regular");
                    gsr.setCessAmount(piLi.getCessAmount());
                    gsr.setGstRate(new BigDecimal(String.valueOf(piLi.getIgstPercentage())));
                    gsr.setTaxableValue(piLi.getValue());
                    gsr.setIntegratedTaxAmount(piLi.getIgstValue());
                    gsr.setCentralTaxAmount(piLi.getCgstValue());
                    gsr.setStateTaxAmount(piLi.getSgstValue());
                    gsr.setITCeligible(piLi.getITCEligible());
                    gsr.setITCIGST(piLi.getItcValueforIGST());
                    gsr.setITCCGST(piLi.getItcValueforCGST());
                    gsr.setITCSGST(piLi.getItcValueforSGST());
                    gsr.setITCCess(piLi.getItcValueforCess());

                    b2b.add(gsr);
                }

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2b;
    }

    public List<GstReport> getGst2CdnrDetails(Company co, Date fromDate, Date toDate) {

        List<GstReport> b2b = new ArrayList<>();
        List<PurchaseInvoice> pss = new ArrayList<>();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findPIByDate");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", co);
            query.setParameter("isRegistered", false);
            query.setParameter("noteType", "DEBIT");
            pss = query.list();

            for (PurchaseInvoice ps : pss) {
                Hibernate.initialize(ps.getPiLineItems());
                for (PurchaseInvoiceLineItem piLi : ps.getPiLineItems()) {
                    GstReport gsr = new GstReport();
                    gsr.setGstinOrUin(ps.getLedger().getGSTIN());
                    gsr.setInvoiceNumber(ps.getPurchaseInvoiceNo());
                    gsr.setInvoiceDate(ps.getPurchaseInvoiceDate());
                    gsr.setInvoiceValue(ps.getPuchaseInvoiceAmount());
                    gsr.setPlaceOfSupply(ps.getLedger().getState());
                    if (ps.isReverseChargeApplicable()) {
                        gsr.setReverseCharge("Y");
                    } else {
                        gsr.setReverseCharge("N");
                    }
                    gsr.setInvoiceType("Regular");
                    gsr.setCessAmount(piLi.getCessAmount());
                    gsr.setGstRate(new BigDecimal(String.valueOf(piLi.getIgstPercentage())));
                    gsr.setTaxableValue(piLi.getValue());
                    gsr.setIntegratedTaxAmount(piLi.getIgstValue());
                    gsr.setCentralTaxAmount(piLi.getCgstValue());
                    gsr.setStateTaxAmount(piLi.getSgstValue());
                    gsr.setITCeligible(piLi.getITCEligible());
                    gsr.setITCIGST(piLi.getItcValueforIGST());
                    gsr.setITCCGST(piLi.getItcValueforCGST());
                    gsr.setITCSGST(piLi.getItcValueforSGST());
                    gsr.setITCCess(piLi.getItcValueforCess());

                    b2b.add(gsr);
                }

            }
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return b2b;
    }
    
    public List<GstReport> getPurchaseHSNSummary(Company co, Date fromDate, Date toDate) {
        ProductCRUD p4 = new ProductCRUD();
        List<GstReport> hsn = new ArrayList<GstReport>();
        List<PurchaseInvoiceLineItem> piLis = new ArrayList<PurchaseInvoiceLineItem>();
        List<Product> products = p4.fetchAllProducts(co.getCompanyId());
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            for (Product pro : products) {
                Query query = session.getNamedQuery("findPIByProduct");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", co);
                query.setParameter("product", pro);

                piLis = query.list();
                GstReport gsr = new GstReport();
                if (pro.getHsnCode() == null) {
                    gsr.setDescription(pro.getName());

                } else {
                    gsr.setHsn(pro.getHsnCode());
                    gsr.setDescription("-");
                }
                gsr.setUqc(pro.getUQC2());
                gsr.setTotalQuantity(BigDecimal.ZERO);
                gsr.setTotalValue(BigDecimal.ZERO);
                gsr.setTaxableValue(BigDecimal.ZERO);
                gsr.setIntegratedTaxAmount(BigDecimal.ZERO);
                gsr.setCentralTaxAmount(BigDecimal.ZERO);
                gsr.setStateTaxAmount(BigDecimal.ZERO);
                gsr.setCessAmount(BigDecimal.ZERO);

                for (PurchaseInvoiceLineItem piLi : piLis) {

                    gsr.setTotalQuantity(gsr.getTotalQuantity().add(BigDecimal.valueOf(piLi.getStockQuantity())));
                    gsr.setTotalValue(gsr.getTotalValue().add(piLi.getValue()));
                    gsr.setTaxableValue(gsr.getTaxableValue().add(piLi.getValue()));
                    gsr.setIntegratedTaxAmount(gsr.getIntegratedTaxAmount().add(piLi.getIgstValue()));
                    gsr.setCentralTaxAmount(gsr.getCentralTaxAmount().add(piLi.getCgstValue()));
                    gsr.setStateTaxAmount(gsr.getStateTaxAmount().add(piLi.getSgstValue()));
                    String strCess = "0";
                    if (!(gsr.getCessAmount() == null)) {
                        strCess = gsr.getCessAmount() + "";
                    }
                    String strPiCess = "0";
                    if (!(piLi.getCessAmount() == null)) {
                        strPiCess = piLi.getCessAmount() + "";
                    }
                    BigDecimal gsrCess = new BigDecimal(strCess);
                    BigDecimal piCess = new BigDecimal(strPiCess);
                    gsr.setCessAmount(gsrCess.add(piCess));
                }
                hsn.add(gsr);
            }

            session.getTransaction().commit();
            return hsn;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return null;
            }

        } finally {

            session.close();

        }

        return null;
    }
}
