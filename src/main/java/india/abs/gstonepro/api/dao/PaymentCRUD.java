/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.text.DecimalFormat;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class PaymentCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");

    public EventStatus createPayment(Company company, Ledger partyLedger, Ledger bankLedger, String particulars, float amount, Date transactionDate) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();

            Journal newJournal = new Journal();

            newJournal.setJournalId(SessionDataUtil.getSelectedCompany().getCompanyId() + "PAYMENT" + new Date().toString());

            session.save(newJournal);

            Transaction transactionOne = new Transaction();
            transactionOne.setLedger(partyLedger);
            transactionOne.setEditable(true);
            transactionOne.setType("PAYMENT");
            transactionOne.setAmount(convertDecimal.Currency(currency.format(amount)).negate());
            transactionOne.setParticulars(particulars);
            transactionOne.setTransactionDate(transactionDate);
            transactionOne.setCompany(company);
            transactionOne.setJournal(newJournal);

            Transaction transactionTwo = new Transaction();
            transactionTwo.setLedger(bankLedger);
            transactionTwo.setEditable(false);
            transactionTwo.setType("PAYMENT");
            transactionTwo.setAmount(convertDecimal.Currency(currency.format(amount)));
            transactionTwo.setParticulars(particulars);
            transactionTwo.setTransactionDate(transactionDate);
            transactionTwo.setCompany(company);
            transactionTwo.setJournal(newJournal);

            session.save(transactionOne);
            session.save(transactionTwo);

            newJournal.getTransactions().add(transactionOne);
            newJournal.getTransactions().add(transactionTwo);

            session.save(newJournal);

            session.getTransaction().commit();

            session.close();
            result.setCreateDone(true);

            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }

        return result;
    }

    public EventStatus updatePayment(Transaction updatedTransaction) {
        EventStatus result = new EventStatus();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Transaction oldTransaction = (Transaction) session.get(Transaction.class, updatedTransaction.getTransactionId());
            oldTransaction.setAmount(updatedTransaction.getAmount());
            oldTransaction.setParticulars(updatedTransaction.getParticulars());
            oldTransaction.setTransactionDate(updatedTransaction.getTransactionDate());
            session.save(oldTransaction);
            session.getTransaction().commit();

            session.close();
            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }
        return result;
    }

    public EventStatus deletePayment(Transaction transaction) {
        EventStatus result = new EventStatus();
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Transaction oldTransaction = (Transaction) session.get(Transaction.class, transaction.getTransactionId());
            session.delete(oldTransaction);
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }
        return result;
    }

}
