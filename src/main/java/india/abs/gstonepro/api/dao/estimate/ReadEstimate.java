/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.estimate;

import india.abs.gstonepro.api.dao.billofsupply.*;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.models.EstimateLineItem;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class ReadEstimate {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<Estimate> fetchAllEstimate(Date fromDate, Date toDate, boolean withCancelledBill) {
        List<Estimate> estimates = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = null;

            if (withCancelledBill) {
                query = session.getNamedQuery("findEstimate");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
            } else {
                query = session.getNamedQuery("findEstimateWithoutCancellBill");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
                query.setParameter("withCancelledBill", false);
            }

            estimates = query.list();
            
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return estimates;

    }

    public List<Estimate> fetchEstimateByParty(Date fromDate, Date toDate, Long partyId, boolean withCancelledBill) {
        List<Estimate> estimates = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);
            //Hibernate Named Query  
            Query query;

            query = session.getNamedQuery("findEstimateByParty");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("ledger", ledger);
            query.setParameter("withCancelledBill", withCancelledBill);

            estimates = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return estimates;

    }

    public List<EstimateLineItem> fetchEstimateByProduct(Date fromDate, Date toDate, Product product) {
        List<EstimateLineItem> estimateLineItems = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findEstimateLineItemsByProduct");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("cancelled", false);

            query.setParameter("product", product);

            estimateLineItems = query.list();

            session.getTransaction().commit();

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return estimateLineItems;
    }

    public Estimate readEstimate(String estimateId) {
        Estimate estimate = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            estimate = (Estimate) session.get(Estimate.class, estimateId);
            Hibernate.initialize(estimate.getEstimateLineItem());
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return estimate;
    }

    public List<Estimate> fetchAllEstimateForAudit(Date fromDate, Date toDate) {
        List<Estimate> estimates = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findEstimateAuditData");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            estimates = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
        }
        return estimates;

    }

    public List<Estimate> fetchAllEstimateForAuditByUser(Date fromDate, Date toDate,AppUser user) {
        List<Estimate> estimates = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            //Hibernate Named Query  
            Query query = session.getNamedQuery("findEstimateAuditDataByUser");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("user",user);

            estimates = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return estimates;

    }

}
