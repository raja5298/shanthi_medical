/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author SGS
 */
public class PurchaseSaleTaxSummaryCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createPSTaxSummary(String newPSId, PurchaseSaleTaxSummary newPSTaxSummary) {
        EventStatus result = new EventStatus();
        try (Session session = FACTORY.openSession()) {

            session.beginTransaction();
            
            

            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class, newPSId);

            newPSTaxSummary.setPurchaseSale(ps);

            session.save(newPSTaxSummary);

            session.getTransaction().commit();
            result.setCreateDone(true);
            session.close();
        }

        return result;
    }



    public EventStatus deletePSTaxSummary(Long psTaxSummaryId) {
        EventStatus deletePSLineItem = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                PurchaseSaleTaxSummary psts = new PurchaseSaleTaxSummary();
                psts.setPsTaxId(psTaxSummaryId);

                session.delete(psts);

                session.getTransaction().commit();
                deletePSLineItem.setDeleteDone(true);
            }

            return deletePSLineItem;
        } catch (ConstraintViolationException ex) {
            return deletePSLineItem;

        } catch (HibernateException ex) {
            return deletePSLineItem;
        }
    }

}
