/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

/**
 *
 * @author Admin
 */
public class TransactionCRUD {

    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createTransaction(String type, Date transactionDate, Ledger ledger, Journal journal, String particulars, float amount) {
        EventStatus createTransaction = new EventStatus();
        try (Session session = FACTORY.openSession()) {

            session.beginTransaction();
            Transaction transaction = new Transaction();
            transaction.setAmount(convertDecimal.Currency(currency.format(amount)));
            transaction.setJournal(journal);
            transaction.setLedger(ledger);
            transaction.setParticulars(particulars);
            transaction.setType(type);
            session.save(transaction);
            session.getTransaction().commit();
            createTransaction.setCreateDone(true);
            session.close();
        }

        return createTransaction;
    }

    public EventStatus updateTransaction(Long transactionId, String type, Date transactionDate, Ledger ledger, Journal journal, String particulars, float amount) {
        EventStatus updateTransaction = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            Transaction transaction = (Transaction) session.get(Transaction.class, transactionId);
            transaction.setAmount(convertDecimal.Currency(currency.format(amount)));
            transaction.setJournal(journal);
            transaction.setLedger(ledger);
            transaction.setParticulars(particulars);
            transaction.setType(type);
            session.save(transaction);
            session.save(transaction);
            session.getTransaction().commit();
            updateTransaction.setUpdateDone(true);
            session.close();
        }

        return updateTransaction;
    }

    public List<Transaction> fetchAllTransactions() {
        List<Transaction> transactions = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            // session.createCriteria(Company.class).list();

            transactions = session.createQuery("from Transaction").list();
            session.getTransaction().commit();
            session.close();

        }
        return transactions;

    }

    public List<Transaction> getAllTransactionsByLedger(Ledger ledger, java.util.Date fromDate, java.util.Date toDate) {

        List<Transaction> transactions = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findTransactionsByLedger");
        query.setParameter("ledger", ledger);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        transactions = query.list();
        session.getTransaction().commit();

        session.close();

        return transactions;

    }

    public List<Transaction> getAllTransactionsByDay(java.util.Date fromDate, java.util.Date toDate) {

        List<Transaction> transactions = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findTransactionsByDay");

        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        transactions = query.list();
        session.getTransaction().commit();

        session.close();

        return transactions;

    }
    
     public List<Transaction> getAllTransactionsByType(java.util.Date fromDate, java.util.Date toDate,String type) {

        List<Transaction> transactions = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = session.getNamedQuery("findTransactionsByType");

        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        query.setParameter("type","%"+type+"%");

        transactions = query.list();
        session.getTransaction().commit();

        session.close();

        return transactions;

    }

    public Transaction fetchATransaction(Long transactionId) {
        Transaction transaction = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            transaction = (Transaction) session.get(Transaction.class, transactionId);
            session.close();
        }
        return transaction;

    }

    public EventStatus deleteTransaction(Long transactionId) {
        EventStatus deleteTransaction = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                Transaction transaction = new Transaction();
                transaction.setTransactionId(transactionId);

                session.delete(transaction);

                session.getTransaction().commit();
                deleteTransaction.setDeleteDone(true);
            }

            return deleteTransaction;
        } catch (ConstraintViolationException ex) {
            return deleteTransaction;

        } catch (HibernateException ex) {
            return deleteTransaction;
        }
    }

}
