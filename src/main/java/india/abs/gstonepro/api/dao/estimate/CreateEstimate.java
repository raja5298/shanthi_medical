/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.estimate;

import india.abs.gstonepro.api.dao.billofsupply.*;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.models.EstimateLineItem;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CreateEstimate {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createEstimate(Session passedSession, Estimate estimate, List<EstimateLineItem> estimateLineItems, Company selectedCompany) throws ParseException {
        EventStatus createdEstimate = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }

            Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());

            Date lastEstimateDate = co.getCompanyBook().getLastEstimateDate();

            LocalDate lastLocalEstimateDate = Instant.ofEpochMilli(lastEstimateDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            LocalDate estimateDate = Instant.ofEpochMilli(estimate.getEstimateDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
            
            if (lastLocalEstimateDate.isAfter(estimateDate)) {

                createdEstimate.setMessage("Please check the Date. Last Invoice Date is " + lastLocalEstimateDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)).toString());
                
                return createdEstimate;
            }

            co.getCompanyBook().setLastEstimateDate(estimate.getEstimateDate());
            session.save(co);
            
            Estimate newEstimate = createNewEstimate(session, estimate, selectedCompany);
            
            for (EstimateLineItem estimateLine : estimateLineItems) {
                session.save(estimateLine);
                estimateLine.setEstimate(newEstimate);
                newEstimate.getEstimateLineItem().add(createNewEstimateLineItem(session, estimateLine));
            }
            
            session.save(newEstimate);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            
            createdEstimate.setCreateDone(true);
            createdEstimate.setBillNo(newEstimate.getEstimateNo());
            createdEstimate.setBillID(newEstimate.getEstimateId());

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }
        }
        
        return createdEstimate;
    }

    private Estimate createNewEstimate(Session session, Estimate newEstimate, Company selectedCompany) {
        
        Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());

        Long estimateCount = co.getCompanyBook().getEstimateCount() + 1;
        newEstimate.setEstimateNo(co.getCompanyPolicy().getEstimatePrefix()+estimateCount + co.getCompanyPolicy().getEstimateSuffix());

        Long estimateIdCount = co.getCompanyBook().getEstimateIdCount() + 1;
        newEstimate.setEstimateId(co.getCompanyId() + "Estimate" + estimateIdCount);

        newEstimate.setAuditData(AuditDataUtil.getAuditForCreate());
        newEstimate.setEstimateLineItem(new ArrayList<EstimateLineItem>());
        session.save(newEstimate);

        co.getCompanyBook().setEstimateCount(estimateCount);
        co.getCompanyBook().setEstimateIdCount(estimateIdCount);
        co.getCompanyBook().setLastEstimateDate(newEstimate.getEstimateDate());

        session.save(co);
        
        return newEstimate;
    }

    private EstimateLineItem createNewEstimateLineItem(Session session, EstimateLineItem estimateLineItem) {
        
        estimateLineItem.setAuditData(AuditDataUtil.getAuditForCreate());
        session.save(estimateLineItem);
        return estimateLineItem;
    }

}
