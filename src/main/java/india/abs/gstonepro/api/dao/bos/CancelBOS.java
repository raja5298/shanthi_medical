/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.bos;

import india.abs.gstonepro.api.dao.GoDownStockCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CancelBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus cancelBOS(Session passedSession, String bosId) {

        boolean sessionAvailable = (passedSession != null);
        EventStatus result = new EventStatus();
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            BOS bos = (BOS) session.get(BOS.class, bosId);
            EventStatus deleteResult = new GoDownStockCRUD().removeBOSStockEntries(session, bos);
            System.out.println("deleteResult "+deleteResult.toString());
            if (deleteResult.isDeleteDone()) {
                bos.setCancelled(true);
                session.save(bos);

                int lineItemsResult = -1;

                Query query = session.createQuery("update BOSLineItem e set e.cancelled=:isCancelled where e.bos =:bos");
               
                query.setParameter("isCancelled", true);
                query.setParameter("bos", bos);
                lineItemsResult = query.executeUpdate();
                System.out.println("lineItemsResult "+lineItemsResult);

                if (lineItemsResult < 0) {
                    throw new HibernateException("Bill of Supply isCancelled Update during cancel TX-I Failed");
                }
                Company c = (Company) session.get(Company.class, bos.getCompany().getCompanyId());
                Query maxDateQuery = session.createQuery("select max(bosDate) from BOS where company = :company and cancelled = :isCancelled");
                maxDateQuery.setParameter("company", c);
                maxDateQuery.setParameter("isCancelled", false);
                Date lastDate = (Date) maxDateQuery.getSingleResult();
                if (lastDate != null) {
                    c.getCompanyBook().setLastBOSDate(lastDate);
                } else {
                    c.getCompanyBook().setLastBOSDate(c.getCompanyPolicy().getFinancialYearStart());
                }
                session.save(c);
                if (!sessionAvailable) {
                    session.getTransaction().commit();
                }
                result.setDeleteDone(true);

            } else {
                result.setMessage("Sale Delete Failed");
                System.out.println("failed return");
                return result;
            }
            System.out.println("correct return");
            return result;

        } catch (HibernateException hx) {
            System.out.println("hx "+hx);
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }
        System.out.println("final return");
        return result;

    }

}
