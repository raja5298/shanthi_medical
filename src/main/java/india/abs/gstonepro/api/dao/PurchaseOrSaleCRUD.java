package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class PurchaseOrSaleCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    
//    ProductStockCRUD ps4 = new ProductStockCRUD();

    public EventStatus createPS(String type, Long partyId, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = new EventStatus();
        
        Audit ad = new Audit();
        ad.setCreatedBy(SessionDataUtil.getSelectedUser());
        ad.setCreatedOn(new Date());

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            
            
            if(partyId==null){
            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);
             newPS.setLedger(ledger);
            }
            
            Company company = (Company) session.get(Company.class, newPS.getCompany().getCompanyId());

           
            newPS.setPsType(type);
            newPS.setAuditData(ad);
            if (type.equalsIgnoreCase("SALES")) {
                Long billNo = company.getSaleCount() + 1;
                company.setSaleCount(billNo);
                session.save(company);
                newPS.setCompany(company);
                newPS.setPsId(company.getCompanyId() + "SALE" + billNo);
                newPS.setBillNo(billNo + "");
            } else if (type.equalsIgnoreCase("PURCHASE")) {
                Long purchaseCount = company.getPurchaseCount() + 1;
                company.setPurchaseCount(purchaseCount);
                session.save(company);
                newPS.setCompany(company);
                newPS.setPsId(company.getCompanyId() + "PURCHASE" + purchaseCount);

            } else if (type.equalsIgnoreCase("SR")) {
                Long saleReturnCount = company.getSrCount() + 1;
                company.setSrCount(saleReturnCount);
                session.save(company);
                newPS.setCompany(company);
                newPS.setPsId(company.getCompanyId() + "SR" + saleReturnCount);

            } else if (type.equalsIgnoreCase("PR")) {
                Long purchaseReturnCount = company.getPrCount() + 1;
                company.setPrCount(purchaseReturnCount);
                session.save(company);
                newPS.setCompany(company);
                newPS.setPsId(company.getCompanyId() + "PR" + purchaseReturnCount);

            }

            for (PurchaseSaleLineItem psLI : psItems) {
                PurchaseSaleLineItem newPSLI = new PurchaseSaleLineItem();

                int stockQuantityToChange = 0;
                int uqc2Value = psLI.getProduct().getUQC2Value();
                int totalStockInUQC2 = (psLI.getUqcOneQuantity() * uqc2Value) + psLI.getUqcTwoQuantity();
                //---------------------------------------------//
                if (newPS.getPsType().equalsIgnoreCase("SALES") || newPS.getPsType().equalsIgnoreCase("PR")) {
                    stockQuantityToChange = -(totalStockInUQC2);
                } else {
                    stockQuantityToChange = totalStockInUQC2;
                }

                newPSLI.setPurchaseSale(newPS);
                newPSLI.setCgstPercentage(psLI.getCgstPercentage());
                newPSLI.setCgstValue(psLI.getCgstValue());
                newPSLI.setIgstPercentage(psLI.getIgstPercentage());
                newPSLI.setIgstValue(psLI.getIgstValue());
                newPSLI.setSgstPercentage(psLI.getSgstPercentage());
                newPSLI.setSgstValue(psLI.getSgstValue());
                newPSLI.setDiscountPercent(psLI.getDiscountPercent());
                newPSLI.setDiscountValue(psLI.getDiscountValue());
                newPSLI.setLineNumber(psLI.getLineNumber());
                newPSLI.setHsnSac(psLI.getHsnSac());
                newPSLI.setUqcOne(psLI.getUqcOne());
                newPSLI.setUqcTwo(psLI.getUqcTwo());
                newPSLI.setUqcOneQuantity(psLI.getUqcOneQuantity());
                newPSLI.setUqcTwoQuantity(psLI.getUqcTwoQuantity());
                newPSLI.setUqcOneRate(psLI.getUqcOneRate());
                newPSLI.setUqcTwoRate(psLI.getUqcTwoRate());
                newPSLI.setUqcOneValue(psLI.getUqcOneValue());
                newPSLI.setUqcTwoValue(psLI.getUqcTwoValue());
                newPSLI.setProductName(psLI.getProductName());
                newPSLI.setProduct(psLI.getProduct());
                newPSLI.setValue(psLI.getValue());
                newPSLI.setProfit(psLI.getProfit());
                newPSLI.setUqc1BaseRate(psLI.getUqc1BaseRate());
                newPSLI.setUqc2BaseRate(psLI.getUqc2BaseRate());
                newPSLI.setStockQuantity(psLI.getStockQuantity());
                newPSLI.setFree(psLI.getFree());
                newPSLI.setFree(psLI.getFree());
                newPSLI.setAuditData(ad);
                newPS.getPsLineItems().add(newPSLI);

                session.save(newPSLI);

                //==============================================//
                ProductStockEntry newPStock = new ProductStockEntry();
                Product product = (Product) session.get(Product.class, psLI.getProduct().getProductId());
                int currentStock = product.getCurrentStock() + stockQuantityToChange;
                product.setCurrentStock(currentStock);
                newPStock.setProduct(psLI.getProduct());
                newPStock.setStockUpdateDate(newPS.getPsDate());
                newPStock.setQuantity(stockQuantityToChange);
                newPStock.setStockUpdateType(newPS.getPsType());
                newPStock.setCgstAmount(psLI.getCgstValue());
                newPStock.setCgstPercentage(psLI.getCgstPercentage());
                newPStock.setIgstAmount(psLI.getIgstValue());
                newPStock.setIgstPercentage(psLI.getIgstPercentage());
                newPStock.setSgstAmount(psLI.getSgstValue());
                newPStock.setSgstPercentage(psLI.getSgstPercentage());
                newPStock.setAuditData(ad);
                newPStock.setCompany(company);
                newPStock.setPsli(newPSLI);
                
                session.save(newPStock);
                session.save(product);

                
          //      newPSLI.setProductStock(newPStock);
                session.save(newPSLI);
                //===========================================================//
            }

            for (PurchaseSaleTaxSummary psTS : psTaxSummaries) {
                PurchaseSaleTaxSummary newPSTS = new PurchaseSaleTaxSummary();
                newPSTS.setPurchaseSale(newPS);
                newPSTS.setHsnSac(psTS.getHsnSac());
                newPSTS.setIgstPercentage(psTS.getIgstPercentage());
                newPSTS.setIgstValue(psTS.getIgstValue());
                newPSTS.setCgstPercentage(psTS.getCgstPercentage());
                newPSTS.setCgstValue(psTS.getCgstValue());
                newPSTS.setSgstPercentage(psTS.getSgstPercentage());
                newPSTS.setSgstValue(psTS.getSgstValue());
                newPSTS.setAuditData(ad);
                newPS.getPsTaxSummaries().add(newPSTS);
                newPSTS.setTaxableValue(psTS.getTaxableValue());
                newPSTS.setTotalTaxValue(psTS.getTotalTaxValue());
                session.save(newPSTS);
            }

            session.save(newPS);

            Journal newJournal = new Journal();
            newJournal.setJournalId(newPS.getCompany().getCompanyId() +type+ new Date().toString());
            newJournal.setAuditData(ad);
            Transaction newPSTransaction = new Transaction();
            if (newPS.getPsType().equalsIgnoreCase("SALES") || newPS.getPsType().equalsIgnoreCase("PR")) {
                newPSTransaction.setAmount((newPS.getCalculatedTotalValue().negate()));
            } else {
                newPSTransaction.setAmount((newPS.getCalculatedTotalValue()));
            }
            newPSTransaction.setJournal(newJournal);
            newPSTransaction.setTransactionDate(newPS.getPsDate());
            newPSTransaction.setParticulars(type + "-" + newPS.getBillNo() + "-" + newPS.getLedger().getLedgerName());
            newPSTransaction.setType(type);
            newPSTransaction.setLedger(newPS.getLedger());
            newPSTransaction.setCompany(company);
            newPSTransaction.setAuditData(ad);
            session.save(newPSTransaction);
            newJournal.getTransactions().add(newPSTransaction);
            newPS.setPurchaseSaleJournal(newJournal);
            session.save(newJournal);
            // Transaction trans1 = new Transaction();
            session.save(newPS);

            session.getTransaction().commit();
            result.setCreateDone(true);
            result.setBillID(newPS.getPsId());
            
            boolean isProduction = SessionDataUtil.getSelectedCompany().getCompanyType().equalsIgnoreCase("PRODUCTION");
            
            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();
        }
        return result;
    }

    public EventStatus updatePS(String type, Long partyId, PurchaseSale updatedPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        
        try {
            
            session.beginTransaction();

            updatedPS.setPsLineItems(new ArrayList<PurchaseSaleLineItem>());

            updatedPS.setPsTaxSummaries(new ArrayList<PurchaseSaleTaxSummary>());

            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);

            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class, updatedPS.getPsId());

            Hibernate.initialize(ps.getPsLineItems());
            List<PurchaseSaleLineItem> existingPSLI = ps.getPsLineItems();
            for (PurchaseSaleLineItem epsli : existingPSLI) {
                Product product = (Product) session.get(Product.class, epsli.getProduct().getProductId());
                int updatedProductStock = 0;
                if (type.equalsIgnoreCase("SALES") || type.equalsIgnoreCase("PR")) {
                    updatedProductStock = product.getCurrentStock() + epsli.getStockQuantity();
                } else if (type.equalsIgnoreCase("PURCHASE") || type.equalsIgnoreCase("SR")) {
                    updatedProductStock = product.getCurrentStock() - epsli.getStockQuantity();
                }
                product.setCurrentStock(updatedProductStock);
                session.save(product);
            }

            session.delete(ps);

            PurchaseSale newPS = new PurchaseSale();
            newPS.setCompany(updatedPS.getCompany());
            newPS.setPsId(updatedPS.getPsId());
            newPS.setPsType(updatedPS.getPsType());
            newPS.setBillNo(updatedPS.getBillNo());
            newPS.setPsDate(updatedPS.getPsDate());
            newPS.setBillAmount(updatedPS.getBillAmount());
            newPS.setIsIGST(updatedPS.isIsIGST());
            newPS.setIgstValue(updatedPS.getIgstValue());
            newPS.setCgstValue(updatedPS.getCgstValue());
            newPS.setSgstValue(updatedPS.getSgstValue());
            newPS.setRoundOffValue(updatedPS.getRoundOffValue());
            newPS.setCalculatedTotalValue(updatedPS.getCalculatedTotalValue());
            newPS.setCancelled(updatedPS.isCancelled());
            newPS.setThrough(updatedPS.getThrough());
            newPS.setLedger(ledger);
            newPS.setPaymentType(updatedPS.getPaymentType());
            newPS.setRemarks(updatedPS.getRemarks());
            newPS.setModeOfDelivery(updatedPS.getModeOfDelivery());
            newPS.setVariationAmount(updatedPS.getVariationAmount());
            newPS.setTotalAmountPaid(updatedPS.getTotalAmountPaid());
            newPS.setReferenceNumber(updatedPS.getReferenceNumber());
            newPS.setProfit(updatedPS.getProfit());
            newPS.setPricingPolicyName(updatedPS.getPricingPolicyName());
            newPS.setSmsMessage(updatedPS.getSmsMessage());
            newPS.setPurchaseSaleJournal(updatedPS.getPurchaseSaleJournal());
            

            session.save(newPS);

            for (PurchaseSaleLineItem psLI : psItems) {
                PurchaseSaleLineItem newPSLI = new PurchaseSaleLineItem();

                int stockQuantityToChange = 0;
                int uqc2Value = psLI.getProduct().getUQC2Value();
                int totalStockInUQC2 = (psLI.getUqcOneQuantity() * uqc2Value) + psLI.getUqcTwoQuantity();
                //---------------------------------------------//
                if (newPS.getPsType().equalsIgnoreCase("SALES") || newPS.getPsType().equalsIgnoreCase("PURCHASE-RETURN")) {
                    stockQuantityToChange = -(totalStockInUQC2);
                } else {
                    stockQuantityToChange = totalStockInUQC2;
                }

                newPSLI.setPurchaseSale(newPS);
                newPSLI.setCgstPercentage(psLI.getCgstPercentage());
                newPSLI.setCgstValue(psLI.getCgstValue());
                newPSLI.setIgstPercentage(psLI.getIgstPercentage());
                newPSLI.setIgstValue(psLI.getIgstValue());
                newPSLI.setSgstPercentage(psLI.getSgstPercentage());
                newPSLI.setSgstValue(psLI.getSgstValue());
                newPSLI.setDiscountPercent(psLI.getDiscountPercent());
                newPSLI.setDiscountValue(psLI.getDiscountValue());
                newPSLI.setLineNumber(psLI.getLineNumber());
                newPSLI.setHsnSac(psLI.getHsnSac());
                newPSLI.setUqcOne(psLI.getUqcOne());
                newPSLI.setUqcTwo(psLI.getUqcTwo());
                newPSLI.setUqcOneQuantity(psLI.getUqcOneQuantity());
                newPSLI.setUqcTwoQuantity(psLI.getUqcTwoQuantity());
                newPSLI.setUqcOneRate(psLI.getUqcOneRate());
                newPSLI.setUqcTwoRate(psLI.getUqcTwoRate());
                newPSLI.setUqcOneValue(psLI.getUqcOneValue());
                newPSLI.setUqcTwoValue(psLI.getUqcTwoValue());
                newPSLI.setProductName(psLI.getProductName());
                newPSLI.setFree(psLI.getFree());
                newPSLI.setProduct(psLI.getProduct());
                newPSLI.setValue(psLI.getValue());
                newPSLI.setProfit(psLI.getProfit());
                newPSLI.setStockQuantity(psLI.getStockQuantity());
                newPSLI.setUqc1BaseRate(psLI.getUqc1BaseRate());
                newPSLI.setUqc2BaseRate(psLI.getUqc2BaseRate());

                newPS.getPsLineItems().add(newPSLI);

                session.save(newPSLI);

                //==============================================//
                ProductStockEntry newPStock = new ProductStockEntry();
                newPStock.setProduct(psLI.getProduct());
                newPStock.setStockUpdateDate(newPS.getPsDate());
                newPStock.setQuantity(stockQuantityToChange);
                newPStock.setStockUpdateType(newPS.getPsType());
                newPStock.setCgstAmount(psLI.getCgstValue());
                newPStock.setCgstPercentage(psLI.getCgstPercentage());
                newPStock.setIgstAmount(psLI.getIgstValue());
                newPStock.setIgstPercentage(psLI.getIgstPercentage());
                newPStock.setSgstAmount(psLI.getSgstValue());
                newPStock.setSgstPercentage(psLI.getSgstPercentage());
                newPStock.setPsli(newPSLI);
                Product product = (Product) session.get(Product.class, psLI.getProduct().getProductId());
                product.setCurrentStock(product.getCurrentStock() + stockQuantityToChange);
                session.save(product);
                session.save(newPStock);
            //    newPSLI.setProductStock(newPStock);
                session.save(newPSLI);
                //===========================================================//
            }

            for (PurchaseSaleTaxSummary psTS : psTaxSummaries) {
                PurchaseSaleTaxSummary newPSTS = new PurchaseSaleTaxSummary();
                newPSTS.setPurchaseSale(newPS);
                newPSTS.setHsnSac(psTS.getHsnSac());
                newPSTS.setIgstPercentage(psTS.getIgstPercentage());
                newPSTS.setIgstValue(psTS.getIgstValue());
                newPSTS.setCgstPercentage(psTS.getCgstPercentage());
                newPSTS.setCgstValue(psTS.getCgstValue());
                newPSTS.setSgstPercentage(psTS.getSgstPercentage());
                newPSTS.setSgstValue(psTS.getSgstValue());
                newPS.getPsTaxSummaries().add(newPSTS);
                newPSTS.setTaxableValue(psTS.getTaxableValue());
                newPSTS.setTotalTaxValue(psTS.getTotalTaxValue());
                session.save(newPSTS);
            }
            
            Journal psJournal = (Journal) session.get(Journal.class, ps.getPurchaseSaleJournal().getJournalId());
            Set<Transaction> psTransaction = psJournal.getTransactions();
            for (Transaction trans : psTransaction) {
                if (newPS.getPsType().equalsIgnoreCase("SALES") || newPS.getPsType().equalsIgnoreCase("PR")) {
                    trans.setAmount((newPS.getCalculatedTotalValue().negate()));
                } else {
                    trans.setAmount((newPS.getCalculatedTotalValue()));
                }
                trans.setJournal(psJournal);
                trans.setTransactionDate(newPS.getPsDate());
                trans.setParticulars(type + "-" + newPS.getBillNo() + "-" + newPS.getLedger().getLedgerName());
                trans.setType(type);
                trans.setLedger(newPS.getLedger());
                session.save(trans);
                psJournal.getTransactions().add(trans);
                session.save(psJournal);
            }

            session.save(newPS);
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;
            
        } catch (HibernateException hx) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public EventStatus cancelPS(String psId) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class, psId);

            PurchaseSale newPS = new PurchaseSale();
            newPS.setPsId(ps.getPsId());
            newPS.setPsType(ps.getPsType());
            newPS.setPsDate(ps.getPsDate());
            newPS.setBillAmount(ps.getBillAmount());
            newPS.setBillNo(ps.getBillNo());
            newPS.setCompany(ps.getCompany());
            newPS.setLedger(ps.getLedger());
            newPS.setModeOfDelivery(ps.getModeOfDelivery());
            newPS.setRemarks(ps.getRemarks());
            newPS.setVariationAmount(ps.getVariationAmount());
            newPS.setCalculatedTotalValue(ps.getCalculatedTotalValue());
            newPS.setPaymentType(ps.getPaymentType());
            newPS.setTotalAmountPaid(ps.getTotalAmountPaid());
            newPS.setThrough(ps.getThrough());
            newPS.setReferenceNumber(ps.getReferenceNumber());
            newPS.setIgstValue(ps.getIgstValue());
            newPS.setCgstValue(ps.getCgstValue());
            newPS.setSgstValue(ps.getSgstValue());
            newPS.setRoundOffValue(ps.getRoundOffValue());
            newPS.setIsIGST(ps.isIsIGST());
            newPS.setCancelled(true);

            session.delete(ps);

            session.save(newPS);
            session.getTransaction().commit();
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return result;
    }
}
