/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

/**
 *
 * @author ALPHA
 */
public class TaxCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<Tax> fetchAllTaxes() {
        List<Tax> taxes;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            taxes = session.createQuery("from Tax").list();
            session.getTransaction().commit();
            session.close();
        }
        return taxes;

    }

    public EventStatus createTax(float tax) {
        EventStatus createTax = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.save(new Tax(tax));
            session.getTransaction().commit();
            createTax.setCreateDone(true);
            session.close();
        }

        return createTax;
    }

    public EventStatus updateTax(Long taxId, float taxValue) {
        EventStatus updateTax = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            Tax tax = (Tax) session.get(Tax.class, taxId);
            tax.setTaxRate(taxValue);
            session.save(tax);
            session.getTransaction().commit();
            updateTax.setUpdateDone(true);
            session.close();
        }

        return updateTax;
    }

    public EventStatus deleteTax(Long taxId) {
        EventStatus deleteTax = new EventStatus();
        try {
            Session session = FACTORY.openSession();
            session.beginTransaction();
            Tax tax = new Tax();
            tax.setId(taxId);

            session.delete(tax);

            session.getTransaction().commit();
            deleteTax.setDeleteDone(true);
            session.close();

            return deleteTax;
        } catch (ConstraintViolationException ex) {
            return deleteTax;

        } catch (HibernateException ex) {
            return deleteTax;
        }
    }
}
