/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.bos;

import india.abs.gstonepro.api.dao.GoDownStockCRUD;
import india.abs.gstonepro.api.dao.bos.*;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CreateBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    GoDownStockCRUD gos4 = new GoDownStockCRUD();

    public EventStatus createBOS(Session passedSession, BOS bos, List<BOSLineItem> bosLineItems, Company selectedCompany) throws ParseException {
        EventStatus createdBOS = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }

            Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());

            Date lastBOSDate = co.getCompanyBook().getLastBOSDate();

            LocalDate lastLocalBOSDate = Instant.ofEpochMilli(lastBOSDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            LocalDate bosDate = Instant.ofEpochMilli(bos.getBOSDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
            
            if (lastLocalBOSDate.isAfter(bosDate)) {

                createdBOS.setMessage("Please check the Date. Last Invoice Date is " + lastLocalBOSDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)).toString());
                
                return createdBOS;
            }

            co.getCompanyBook().setLastBOSDate(bos.getBOSDate());
            session.save(co);
            
            BOS newBOS = createNewBOS(session, bos, selectedCompany);
            
            for (BOSLineItem bosLine : bosLineItems) {
                
                
                int uqc2Value = bosLine.getProduct().getUQC2Value();
                int totalStockInUQC2 = (bosLine.getUqcOneQuantity() * uqc2Value) + bosLine.getUqcTwoQuantity();
                
                Long stockQuantityToChange = new Long(-(totalStockInUQC2));
                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    
                    if (!bosLine.getProduct().isService()) {
                         GoDownStockEntry newGStock = gos4.changeProductStock(session, bosLine.getGodown(), bosLine.getProduct(), stockQuantityToChange, bosLine.getValue(), newBOS.getBOSDate(), "BOS", 0, 0, 0, new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), null, null, null,bosLine);
                        if (newGStock == null) {
                            throw new HibernateException("Product Stock Update Failed during Bill of Supply");
                        }
                        bosLine.setGodownStockEntry(newGStock);
                    }
                    
                }
                session.save(bosLine);
                bosLine.setBOS(newBOS);
                newBOS.getBOSLineItem().add(createNewBOSLineItem(session, bosLine));
            }
            
            session.save(newBOS);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            
            createdBOS.setCreateDone(true);
            createdBOS.setBillNo(newBOS.getBOSNo());
            createdBOS.setBillID(newBOS.getBOSId());
            

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }
        }
        
        return createdBOS;
    }

    private BOS createNewBOS(Session session, BOS newBOS, Company selectedCompany) {
        
        Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());

        Long bosCount = co.getCompanyBook().getBosCount() + 1;
        newBOS.setBOSNo(co.getCompanyPolicy().getBosPrefix()+bosCount + co.getCompanyPolicy().getBosSuffix());

        Long bosIdCount = co.getCompanyBook().getBosIdCount() + 1;
        newBOS.setBOSId(co.getCompanyId() + "BOS" + bosIdCount);

        newBOS.setAuditData(AuditDataUtil.getAuditForCreate());
        newBOS.setBOSLineItem(new ArrayList<BOSLineItem>());
        session.save(newBOS);

        co.getCompanyBook().setBosCount(bosCount);
        co.getCompanyBook().setBosIdCount(bosIdCount);
        co.getCompanyBook().setLastBOSDate(newBOS.getBOSDate());

        session.save(co);
        
        return newBOS;
    }

    private BOSLineItem createNewBOSLineItem(Session session, BOSLineItem bosLineItem) {
        
        bosLineItem.setAuditData(AuditDataUtil.getAuditForCreate());
        session.save(bosLineItem);
        return bosLineItem;
    }

}
