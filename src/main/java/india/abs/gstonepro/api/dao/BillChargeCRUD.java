/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Admin
 */
public class BillChargeCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createBillCharge(BillCharge newBillCharge) {
        EventStatus createBillCharge = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.save(newBillCharge);
            session.getTransaction().commit();
            session.close();
            createBillCharge.setCreateDone(true);
        }
        return createBillCharge;
    }
    
    public EventStatus updateBillCharge(BillCharge updatedBillCharge) {
        EventStatus updateBillCharge = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(updatedBillCharge);
            session.getTransaction().commit();
            session.close();
            updateBillCharge.setUpdateDone(true);
        }
        return updateBillCharge;
    }
    public List<BillCharge> fetchAllBillCharges() {
        List<BillCharge> billCharges = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            billCharges = (List<BillCharge>) session.createQuery("from BillCharge").list();
            session.getTransaction().commit();
            session.close();
        }
        return billCharges;

    }
    public EventStatus deleteBillCharge(Long billChargeId) {
        EventStatus deleteBillCharge = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                BillCharge billCharge = new BillCharge();
                billCharge.setBillChargeId(billChargeId);

                session.delete(billCharge);

                session.getTransaction().commit();
                deleteBillCharge.setDeleteDone(true);
            }

            return deleteBillCharge;
        } catch (ConstraintViolationException ex) {
            return deleteBillCharge;

        } catch (HibernateException ex) {
            return deleteBillCharge;
        }
    }
}
