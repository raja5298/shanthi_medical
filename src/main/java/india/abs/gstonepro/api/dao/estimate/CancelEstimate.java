/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.estimate;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CancelEstimate {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus cancelEstimate(Session passedSession, String estimateId) {

        boolean sessionAvailable = (passedSession != null);
        EventStatus result = new EventStatus();
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Estimate estimate = (Estimate) session.get(Estimate.class, estimateId);

            estimate.setCancelled(true);
            session.save(estimate);

            int lineItemsResult = -1;

            Query query = session.createQuery("update EstimateLineItem e set e.cancelled=:isCancelled where e.estimate =:estimate");
            query.setParameter("estimate", estimate);
            query.setParameter("isCancelled", true);

            lineItemsResult = query.executeUpdate();

            if (lineItemsResult < 0) {
                throw new HibernateException("Esimate isCancelled Update during cancel TX-I Failed");
            }
            Company c = (Company) session.get(Company.class, estimate.getCompany().getCompanyId());
                Query maxDateQuery = session.createQuery("select max(estimateDate) from Estimate where company = :company and cancelled = :isCancelled");
                maxDateQuery.setParameter("company", c);
                maxDateQuery.setParameter("isCancelled", false);
                Date lastDate = (Date) maxDateQuery.getSingleResult();
                if(lastDate!=null){
                c.getCompanyBook().setLastEstimateDate(lastDate);
                }
                else{
                    c.getCompanyBook().setLastEstimateDate(c.getCompanyPolicy().getFinancialYearStart());
                }
                session.save(c);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
            session.close();
            }

        }
        return result;

    }

}
