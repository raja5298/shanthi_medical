/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author ALPHA
 */
public class uqcCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createUQC(String quantityName, String quantityType, String uqcCode) {
        EventStatus createUQC = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.save(new UQC(quantityName, quantityType, uqcCode));
            session.getTransaction().commit();
            createUQC.setCreateDone(true);
            session.close();
        }

        return createUQC;
    }

    public EventStatus updateUQC(Long id, String quantityName, String quantityType, String uqcCode) {
        EventStatus updateUQC = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            UQC uqc = (UQC) session.get(UQC.class, id);
            uqc.setQuantityName(quantityName);
            uqc.setQuantityType(quantityType);
            uqc.setUQCCode(uqcCode);
            session.save(uqc);
            session.getTransaction().commit();
            updateUQC.setUpdateDone(true);
            session.close();
        }

        return updateUQC;
    }

    public List<UQC> fetchAllUQCs() {
        List<UQC> companies = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            // session.createCriteria(Company.class).list();

            companies = session.createQuery("from UQC").list();
            session.getTransaction().commit();
            session.close();

        }
        return companies;

    }

    public EventStatus deleteUQC(Long uqcId) {
        EventStatus deleteUQC = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                UQC uqc = new UQC();
                uqc.setId(uqcId);

                session.delete(uqc);

                session.getTransaction().commit();
                deleteUQC.setDeleteDone(true);
            }

            return deleteUQC;
        } catch (ConstraintViolationException ex) {
            return deleteUQC;

        } catch (HibernateException ex) {
            return deleteUQC;
        }
    }
}
