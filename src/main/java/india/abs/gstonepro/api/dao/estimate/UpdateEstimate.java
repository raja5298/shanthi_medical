/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.estimate;

import india.abs.gstonepro.api.dao.billofsupply.*;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.models.EstimateLineItem;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class UpdateEstimate {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    CreateEstimate c4 = new CreateEstimate();
    CancelEstimate can4 = new CancelEstimate();

    public EventStatus updateEstimate(Estimate estimate, List<EstimateLineItem> estimateLineItems, Company selectedCompany) throws ParseException {
        EventStatus updatedEstimate = new EventStatus();
        Session session = FACTORY.openSession();

        try {
            session.beginTransaction();
            EventStatus deleteResult=can4.cancelEstimate(session, estimate.getEstimateId());
            if(!deleteResult.isDeleteDone()){
                 session.getTransaction().rollback();
                 deleteResult.setMessage("Update Failed");
                return deleteResult;
            }
            Company co = (Company) session.get(Company.class, estimate.getCompany().getCompanyId());
            
            estimate.setEstimateDate(co.getCompanyBook().getLastEstimateDate());
            EventStatus createResult = c4.createEstimate(session, estimate, estimateLineItems,selectedCompany);
            if(!createResult.isCreateDone()){
                session.getTransaction().rollback();
                createResult.setMessage("Update Failed");
                return createResult;
            }

            session.getTransaction().commit();
            if (deleteResult.isDeleteDone() && createResult.isCreateDone()) {
                updatedEstimate.setUpdateDone(true);
                return updatedEstimate;
            }
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                hx.printStackTrace();
                session.getTransaction().rollback();
            }

        } catch (ParseException ex) {
            Logger.getLogger(UpdateEstimate.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            session.close();
        }

        return updatedEstimate;
    }

}
