/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.bos;

import india.abs.gstonepro.api.dao.bos.*;
import india.abs.gstonepro.api.dao.billofsupply.*;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class ReadBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<BOS> fetchAllBOS(Date fromDate, Date toDate, boolean withCancelledBill) {
        List<BOS> boss = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = null;

            if (withCancelledBill) {
                query = session.getNamedQuery("find-BOS");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
            }
            else {
                query = session.getNamedQuery("find-BOSWithoutCancellBill");
                query.setParameter("fromDate", fromDate);
                query.setParameter("toDate", toDate);
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
                query.setParameter("withCancelledBill", false);
            }

            boss = query.list();

            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return boss;

    }

    public List<BOS> fetchBOSByParty(Date fromDate, Date toDate, Long partyId, boolean withCancelledBill) {
        List<BOS> boss = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);
            //Hibernate Named Query  
            Query query;

            query = session.getNamedQuery("find-BOSByParty");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", SessionDataUtil.getSelectedCompany());
            query.setParameter("ledger", ledger);
            query.setParameter("withCancelledBill", withCancelledBill);

            boss = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return boss;

    }

    public List<BOSLineItem> fetchBOSByProduct(Date fromDate, Date toDate, Product product) {
        List<BOSLineItem> bosLineItems = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("find-BOSLineItemsByProduct");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("cancelled", false);

            query.setParameter("product", product);

            bosLineItems = query.list();

            session.getTransaction().commit();

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return bosLineItems;
    }

    public BOS readBOS(String bosId) {
        BOS bos = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            bos = (BOS) session.get(BOS.class, bosId);
            Hibernate.initialize(bos.getBOSLineItem());
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return bos;
    }

    public List<BOS> fetchAllBOSForAudit(Date fromDate, Date toDate) {
        List<BOS> boss = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("find-BOSAuditData");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            boss = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
        }
        return boss;

    }

    public List<BOS> fetchAllBOSForAuditAll(Date fromDate, Date toDate, String partyId) {
        List<BOS> boss = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("find-BOSAuditData");
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            boss = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
        }
        return boss;

    }

    public List<BOS> fetchAllBOSForAuditByUser(Date fromDate, Date toDate, AppUser user) {
        List<BOS> boss = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("find-BOSAuditDataByUser");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("user", user);

            boss = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return boss;

    }

}
