/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.billofsupply;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class ReadBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public List<BillOfSupply> fetchAllBOS(Date fromDate, Date toDate, Company company, boolean withCancelledBill) {
        List<BillOfSupply> billOfSupplies = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findBOS");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", company);
            query.setParameter("withCancelledBill", withCancelledBill);

            billOfSupplies = query.list();
            for (BillOfSupply bos : billOfSupplies) {
                
            }
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return billOfSupplies;

    }

    public List<BillOfSupply> fetchBOSByParty(Date fromDate, Date toDate, Company company, Ledger ledger, boolean withCancelledBill) {
        List<BillOfSupply> billOfSupplies = null;
        
        
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query;

            if (withCancelledBill) {
                query = session.getNamedQuery("findBOSWithCancelled");
            } else {
                query = session.getNamedQuery("findBOSByParty");
            }

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("company", company);
            query.setParameter("ledger", ledger);
            if (!withCancelledBill) {
                query.setParameter("withCancelledBill", withCancelledBill);
            }

            billOfSupplies = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return billOfSupplies;

    }

    public List<BillOfSupplyLineItem> fetchBOSByProduct(Date fromDate, Date toDate, Product product) {
        List<BillOfSupplyLineItem> bosLineItems = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findBOSLineItemsByProduct");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("cancelled", false);
            
            query.setParameter("product", product);

            bosLineItems = query.list();

            session.getTransaction().commit();

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return bosLineItems;

    }

    public BillOfSupply readABOS(String bosId) {
        BillOfSupply bos = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            bos = (BillOfSupply) session.get(BillOfSupply.class, bosId);
            Hibernate.initialize(bos.getBosChargeItems());
            Hibernate.initialize(bos.getBosLineItems());
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }
        return bos;
    }

    public List<BillOfSupply> fetchAllBOSForAudit(Date fromDate, Date toDate) {
        List<BillOfSupply> billOfSupplies = null;
        
        

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findBOSAuditData");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            billOfSupplies = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return billOfSupplies;

    }

    public List<BillOfSupply> fetchAllBOSForAuditByUser(Date fromDate, Date toDate, AppUser user) {
        List<BillOfSupply> billOfSupplies = null;

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findBOSAuditDataByUser");

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("user", user);

            billOfSupplies = query.list();
            session.getTransaction().commit();

            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return billOfSupplies;

    }

}
