/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class JournalCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createJournal(Company company, Set<Transaction> transactions) {
        EventStatus createJournal = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();

            Company co = (Company) session.get(Company.class, company.getCompanyId());
            Long journalIdCount = co.getCompanyBook().getJournalIdCount() + 1;
            co.getCompanyBook().setJournalIdCount(journalIdCount);
            session.save(co);

            Journal newJournal = new Journal();
            String journalId = company.getCompanyId() + "JOURNAL" + journalIdCount;
            newJournal.setJournalId(journalId);
            session.save(newJournal);

            for (Transaction transactionOne : transactions) {

                transactionOne.setEditable(true);
                if (transactionOne.getType() == null) {
                    transactionOne.setType("JOURNAL");
                }
                transactionOne.setCompany(company);
                transactionOne.setJournal(newJournal);
                session.save(transactionOne);
                newJournal.getTransactions().add(transactionOne);
            }

            session.save(newJournal);
            session.getTransaction().commit();
            createJournal.setCreateDone(true);

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return createJournal;
        } finally {

            session.close();

        }

        return createJournal;
    }

    public EventStatus updateJournal(Company company, Journal journal, Set<Transaction> transactions) {
        EventStatus updateJournal = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();

            Journal existingJournal = (Journal) session.get(Journal.class, journal.getJournalId());

            for (Transaction trans : existingJournal.getTransactions()) {
                session.delete(trans);
            }

            session.delete(existingJournal);

            Journal newJournal = new Journal();

            newJournal.setJournalId(journal.getJournalId());
            session.save(newJournal);

            for (Transaction transactionOne : transactions) {

                transactionOne.setEditable(true);
                if (transactionOne.getType() == null) {
                    transactionOne.setType("JOURNAL");
                }
                transactionOne.setCompany(company);
                transactionOne.setJournal(newJournal);
                session.save(transactionOne);
                newJournal.getTransactions().add(transactionOne);
            }

            session.save(newJournal);

            session.getTransaction().commit();

            updateJournal.setUpdateDone(true);
            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return updateJournal;
        } finally {

            session.close();

        }

        return updateJournal;
    }

    public Journal fetchAJournal(String journalId) {

        Session session = FACTORY.openSession();
        Journal existingJournal = null;
        try {

            session.beginTransaction();

            existingJournal = (Journal) session.get(Journal.class, journalId);

            session.getTransaction().commit();

            session.close();
            return existingJournal;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return existingJournal;
        } finally {

            session.close();

        }

        return existingJournal;

    }

    public EventStatus deleteJournal(String journalId) {
        EventStatus deleteJournal = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();

            Journal existingJournal = (Journal) session.get(Journal.class, journalId);

            for (Transaction trans : existingJournal.getTransactions()) {
                session.delete(trans);
            }

            session.delete(existingJournal);

            session.getTransaction().commit();

            deleteJournal.setDeleteDone(true);
            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return deleteJournal;
        } finally {

            session.close();

        }

        return deleteJournal;
    }

    public Journal createASaleJournal(Session passedSession, Company company, Ledger partyLedger, String billNo, BigDecimal amount, Date saleDate) {

        LedgerCRUD l4 = new LedgerCRUD();
        Journal saleJournal = null;
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {

            if (!sessionAvailable) {
                session.beginTransaction();
            }

            Ledger purchaseLedger = l4.fetchLedgerByName(company, "Sales");

            saleJournal = new Journal();
            String journalId = company.getCompanyId() + "SALE" + billNo;
            saleJournal.setJournalId(journalId);
            saleJournal.setAuditData(AuditDataUtil.getAuditForCreate());
            session.save(saleJournal);

            Transaction saleTransaction = new Transaction();
            saleTransaction.setAmount(amount);
            saleTransaction.setTransactionDate(saleDate);
            saleTransaction.setType("SALE");
            saleTransaction.setParticulars("Sale Entry - Bill No" + billNo);
            saleTransaction.setLedger(purchaseLedger);
            saleTransaction.setJournal(saleJournal);
            session.save(saleTransaction);
            saleJournal.getTransactions().add(saleTransaction);

            Transaction partyTransaction = new Transaction();
            partyTransaction.setAmount(amount.negate());
            partyTransaction.setTransactionDate(saleDate);
            partyTransaction.setType("SALE");
            partyTransaction.setParticulars("Sale Entry - Bill No" + billNo);
            if (partyLedger != null) {
                partyTransaction.setLedger(partyLedger);
            } else {
                Ledger cashLedger = l4.fetchLedgerByName(company, "Cash In Hand");
                partyTransaction.setLedger(cashLedger);
            }
            partyTransaction.setJournal(saleJournal);
            session.save(partyTransaction);
            saleJournal.getTransactions().add(partyTransaction);
            session.save(saleJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }

            return saleJournal;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return saleJournal;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }

        return saleJournal;
    }

    public EventStatus deleteASaleJournal(Session passedSession, PurchaseSale ps) {
        EventStatus deleteJournalResult = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Journal psJournal = ps.getPurchaseSaleJournal();
            ps.setPurchaseSaleJournal(null);
            session.save(ps);
            session.delete(psJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            deleteJournalResult.setDeleteDone(true);
            return deleteJournalResult;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return deleteJournalResult;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }
        return deleteJournalResult;
    }

    public Journal createAPurchaseJournal(Session passedSession, Company company, Ledger partyLedger, String billNo, BigDecimal amount, Date purchaseDate) {

        LedgerCRUD l4 = new LedgerCRUD();
        Journal purchaseJournal = null;
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {

            if (!sessionAvailable) {
                session.beginTransaction();
            }

            Ledger purchaseLedger = l4.fetchLedgerByName(company, "Purchase");

            purchaseJournal = new Journal();
            String journalId = company.getCompanyId() + "PURCHASE" + billNo;
            purchaseJournal.setJournalId(journalId);
            purchaseJournal.setAuditData(AuditDataUtil.getAuditForCreate());
            session.save(purchaseJournal);

            Transaction saleTransaction = new Transaction();
            saleTransaction.setAmount(amount.negate());
            saleTransaction.setTransactionDate(purchaseDate);
            saleTransaction.setType("PURCHASE");
            saleTransaction.setParticulars("Purchase Entry - Bill No" + billNo);
            saleTransaction.setLedger(purchaseLedger);
            saleTransaction.setJournal(purchaseJournal);
            session.save(saleTransaction);
            purchaseJournal.getTransactions().add(saleTransaction);

            Transaction partyTransaction = new Transaction();
            partyTransaction.setAmount(amount);
            partyTransaction.setTransactionDate(purchaseDate);
            partyTransaction.setType("PURCHASE");
            partyTransaction.setParticulars("Purchase Entry - Bill No" + billNo);
            if (partyLedger != null) {
                partyTransaction.setLedger(partyLedger);
            } else {
                Ledger cashLedger = l4.fetchLedgerByName(company, "Cash In Hand");
                partyTransaction.setLedger(cashLedger);
            }
            partyTransaction.setJournal(purchaseJournal);
            session.save(partyTransaction);
            purchaseJournal.getTransactions().add(partyTransaction);
            session.save(purchaseJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }

            return purchaseJournal;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return purchaseJournal;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }

        return purchaseJournal;
    }

    public EventStatus deleteAPurchaseJournal(Session passedSession, PurchaseInvoice pi) {
        EventStatus deleteJournalResult = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Journal piJournal = pi.getPurchaseJournal();
            pi.setPurchaseJournal(null);
            session.save(pi);
            session.delete(piJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            deleteJournalResult.setDeleteDone(true);
            return deleteJournalResult;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return deleteJournalResult;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }
        return deleteJournalResult;
    }

    public Journal createACrDrNoteJournal(Session passedSession, Company company, Ledger partyLedger, String noteNo, String type, BigDecimal amount, Date purchaseDate) {

        LedgerCRUD l4 = new LedgerCRUD();
        Journal crDrJournal = null;
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {

            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Ledger crDrLedger = null;
            if (type.equalsIgnoreCase("DEBIT-NOTE")) {
                crDrLedger = l4.fetchLedgerByName(company, "Purchase");
            } else if (type.equalsIgnoreCase("CREDIT-NOTE")) {
                crDrLedger = l4.fetchLedgerByName(company, "Sales");
            }

            crDrJournal = new Journal();
            String journalId = company.getCompanyId() + "CRDR" + noteNo;
            crDrJournal.setJournalId(journalId);
            crDrJournal.setAuditData(AuditDataUtil.getAuditForCreate());
            session.save(crDrJournal);

            Transaction crDrTransaction = new Transaction();

            crDrTransaction.setTransactionDate(purchaseDate);
            crDrTransaction.setType(type);
            crDrTransaction.setParticulars(type + "No :" + noteNo);
            crDrTransaction.setLedger(crDrLedger);
            crDrTransaction.setJournal(crDrJournal);

            Transaction partyTransaction = new Transaction();
            partyTransaction.setAmount(amount);
            partyTransaction.setTransactionDate(purchaseDate);
            partyTransaction.setType(type);
            partyTransaction.setParticulars(type + " No :" + noteNo);
            if (partyLedger != null) {
                partyTransaction.setLedger(partyLedger);
            } else {
                Ledger cashLedger = l4.fetchLedgerByName(company, "Cash In Hand");
                partyTransaction.setLedger(cashLedger);
            }
            partyTransaction.setJournal(crDrJournal);

            if (type.equalsIgnoreCase("DEBIT-NOTE")) {
                crDrTransaction.setAmount(amount);
                partyTransaction.setAmount(amount.negate());
            } else if (type.equalsIgnoreCase("CREDIT-NOTE")) {
                crDrTransaction.setAmount(amount.negate());
                partyTransaction.setAmount(amount);
            }

            session.save(crDrTransaction);
            crDrJournal.getTransactions().add(crDrTransaction);
            session.save(partyTransaction);
            crDrJournal.getTransactions().add(partyTransaction);
            session.save(crDrJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }

            return crDrJournal;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return crDrJournal;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }

        return crDrJournal;
    }

    public EventStatus deleteACrDrJournal(Session passedSession, CreditDebitNote cdn) {
        EventStatus deleteJournalResult = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Journal cdnJournal = cdn.getCrDrJournal();
            cdn.setCrDrJournal(null);
            session.save(cdn);
            session.delete(cdnJournal);
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            deleteJournalResult.setDeleteDone(true);
            return deleteJournalResult;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return deleteJournalResult;
        } finally {
            if (!sessionAvailable) {
                session.close();
            }

        }
        return deleteJournalResult;
    }

}
