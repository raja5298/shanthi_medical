/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.ProfitLossAccount;
import india.abs.gstonepro.api.models.BalanceSheet;
import india.abs.gstonepro.api.models.TradingAccount;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class BalanceSheetCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus updateTradingAccount(Company company, List<TradingAccount> tas) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query sumQuery = session.createQuery("select count(type) from TradingAccount where company=:company");
            sumQuery.setParameter("company", company);
            Long existingRecords = (Long) sumQuery.getSingleResult();

            if (existingRecords > 0) {

                Query deleteProductFromGoDownEntry = session.createQuery("delete from TradingAccount t  where t.company =:company");
                deleteProductFromGoDownEntry.setParameter("company", company);
                int tradingAccountDelete = deleteProductFromGoDownEntry.executeUpdate();
            }
            List<Ledger> income = new ArrayList<Ledger>();
            List<Ledger> expense = new ArrayList<Ledger>();
            for (TradingAccount ta : tas) {
                ta.setCompany(company);
                if (ta.getType().equals("EXPENSE")) {
                    expense.add(ta.getLedger());
                } else if (ta.getType().equals("INCOME")) {
                    income.add(ta.getLedger());
                }
                session.save(ta);
            }

            Query incomeLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            incomeLedgersQuery.setParameterList("ledgers", income);
            incomeLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            incomeLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());

            BigDecimal incomeSum = (BigDecimal) incomeLedgersQuery.getSingleResult();

            Query expenseLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            expenseLedgersQuery.setParameterList("ledgers", expense);

            expenseLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            expenseLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());
            BigDecimal expenseSum = (BigDecimal) expenseLedgersQuery.getSingleResult();

            BigDecimal net = new BigDecimal(0);
            if (incomeSum != null) {
                net = incomeSum;
            } else {
                incomeSum = net;
            }
            if (expenseSum != null) {
                net = incomeSum.subtract(expenseSum);
            }

            company.getCompanyBook().setNetProfit(net);

//           session.save(company);
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

            }
            hx.printStackTrace();

        } catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

            }
            hx.printStackTrace();

        } finally {
            session.close();
            return result;
        }

    }

    public List<TradingAccount> fetchTradingAccountLedgers(Company company) {

        List<TradingAccount> taLedgers = null;
        Session session;

        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findTALedgers");
            query.setParameter("company", company);
            taLedgers = query.list();
            session.getTransaction().commit();

            return taLedgers;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return taLedgers;
        } finally {

            session.close();

        }
        return taLedgers;

    }

    public EventStatus updateProfitLossAccount(Company company, List<ProfitLossAccount> plas) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query sumQuery = session.createQuery("select count(type) from ProfitLossAccount where company=:company");
            sumQuery.setParameter("company", company);
            Long existingRecords = (Long) sumQuery.getSingleResult();

            if (existingRecords > 0) {

                Query deleteProductFromGoDownEntry = session.createQuery("delete from ProfitLossAccount t  where t.company =:company");
                deleteProductFromGoDownEntry.setParameter("company", company);
                int tradingAccountDelete = deleteProductFromGoDownEntry.executeUpdate();
            }
            List<Ledger> income = new ArrayList<Ledger>();
            List<Ledger> expense = new ArrayList<Ledger>();
            for (ProfitLossAccount pla : plas) {
                pla.setCompany(company);
                if (pla.getType().equals("EXPENSE")) {
                    expense.add(pla.getLedger());
                } else if (pla.getType().equals("INCOME")) {
                    income.add(pla.getLedger());
                }
                session.save(pla);
            }

            Query incomeLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            incomeLedgersQuery.setParameterList("ledgers", income);
            incomeLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            incomeLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());

            BigDecimal incomeSum = (BigDecimal) incomeLedgersQuery.getSingleResult();

            Query expenseLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            expenseLedgersQuery.setParameterList("ledgers", expense);

            expenseLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            expenseLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());
            BigDecimal expenseSum = (BigDecimal) expenseLedgersQuery.getSingleResult();

            BigDecimal gross = new BigDecimal(0);
            if (incomeSum != null) {
                gross = incomeSum;
            } else {
                incomeSum = gross;
            }
            if (expenseSum != null) {
                gross = incomeSum.subtract(expenseSum);
            }

            company.getCompanyBook().setGrossProfit(gross);

//            session.save(company);
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } finally {
            session.close();
            return result;
        }

    }

    public List<ProfitLossAccount> fetchProfitLossAccountLedgers(Company company) {

        List<ProfitLossAccount> plaLedgers = null;
        Session session;

        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findPLALedgers");
            query.setParameter("company", company);
            plaLedgers = query.list();
            session.getTransaction().commit();

            return plaLedgers;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return plaLedgers;
        } finally {

            session.close();

        }
        return plaLedgers;

    }

    public Set<Ledger> fetchProfitLossTradingAccountLedgers(Company company) {

        List<ProfitLossAccount> plLedgers = null;
        List<TradingAccount> taLedgers = null;
        Set<Ledger> taPlLedgers = new HashSet();

        Session session;

        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query plQuery = session.getNamedQuery("findPLALedgers");
            plQuery.setParameter("company", company);
            plLedgers = plQuery.list();
            for (ProfitLossAccount pl : plLedgers) {
                taPlLedgers.add(pl.getLedger());
            }

            Query taQuery = session.getNamedQuery("findTALedgers");
            taQuery.setParameter("company", company);
            taLedgers = taQuery.list();

            for (TradingAccount ta : taLedgers) {
                taPlLedgers.add(ta.getLedger());
            }
            session.getTransaction().commit();

            return taPlLedgers;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return taPlLedgers;
        } finally {

            session.close();

        }
        return taPlLedgers;

    }

    public EventStatus updateBalanceSheet(Company company, List<BalanceSheet> bas) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query sumQuery = session.createQuery("select count(type) from BalanceSheet where company=:company");
            sumQuery.setParameter("company", company);
            Long existingRecords = (Long) sumQuery.getSingleResult();

            if (existingRecords > 0) {

                Query deleteProductFromGoDownEntry = session.createQuery("delete from BalanceSheet t  where t.company =:company");
                deleteProductFromGoDownEntry.setParameter("company", company);
                int tradingAccountDelete = deleteProductFromGoDownEntry.executeUpdate();
            }
            List<Ledger> income = new ArrayList<Ledger>();
            List<Ledger> expense = new ArrayList<Ledger>();
            for (BalanceSheet ta : bas) {
                ta.setCompany(company);
                if (ta.getType().equals("EXPENSE")) {
                    expense.add(ta.getLedger());
                } else if (ta.getType().equals("INCOME")) {
                    income.add(ta.getLedger());
                }
                session.save(ta);
            }

            Query incomeLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            incomeLedgersQuery.setParameterList("ledgers", income);
            incomeLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            incomeLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());

            BigDecimal incomeSum = (BigDecimal) incomeLedgersQuery.getSingleResult();

            Query expenseLedgersQuery = session.createQuery("select sum(amount) from Transaction t where t.ledger in (:ledgers) and t.transactionDate between :fromDate and :toDate");
            expenseLedgersQuery.setParameterList("ledgers", expense);

            expenseLedgersQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            expenseLedgersQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());
            BigDecimal expenseSum = (BigDecimal) expenseLedgersQuery.getSingleResult();
            BigDecimal net = new BigDecimal(0);
            if (incomeSum != null) {
                net = incomeSum;
            } else {
                incomeSum = net;
            }
            if (expenseSum != null) {
                net = incomeSum.subtract(expenseSum);
            }
            company.getCompanyBook().setBalance(net);

//           session.save(company);
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

            }
            hx.printStackTrace();

        } catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

            }
            hx.printStackTrace();

        } finally {
            session.close();
            return result;
        }

    }

    public List<BalanceSheet> fetchAllBSLedgers(Company company) {
        List<BalanceSheet> bsLedgers = null;
        Session session;

        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findBSLedgers");
            query.setParameter("company", company);
            bsLedgers = query.list();
            session.getTransaction().commit();
            return bsLedgers;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return bsLedgers;
        } finally {
            session.close();
        }
        return bsLedgers;
    }

    public BigDecimal getTransactionAmount(Ledger ledger) {
        BigDecimal ledgerSum = new BigDecimal(0);
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Query ledgerSumQuery = session.createQuery("select sum(amount) from Transaction where ledger = :ledger and transactionDate between :fromDate and :toDate");
            ledgerSumQuery.setParameter("ledger", ledger);
            ledgerSumQuery.setParameter("fromDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearStart());
            ledgerSumQuery.setParameter("toDate", SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd());

            ledgerSum = (BigDecimal) ledgerSumQuery.getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return ledgerSum;
    }
}
