/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.billofsupply;

import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CancelBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus cancelBOS(Session passedSession, String bosId) {

        boolean sessionAvailable = (passedSession != null);
        EventStatus result = new EventStatus();
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            BillOfSupply bos = (BillOfSupply) session.get(BillOfSupply.class, bosId);

            bos.setCancelled(true);
            session.save(bos);

            int chargeItemsResult = -1, lineItemsResult = -1;

            Query query = session.createQuery("update BOSLineItem b set b.cancelled=:isCancelled where b.billOfSupply =:bos");
            query.setParameter("bos", bos);
            query.setParameter("isCancelled", true);

            chargeItemsResult = query.executeUpdate();

            lineItemsResult = query.executeUpdate();

            if (lineItemsResult < 0 || chargeItemsResult < 0) {
                throw new HibernateException("Bill  of Supply is not Canceled");
            }
            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setDeleteDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
            session.close();
            }

        }
        return result;

    }

}
