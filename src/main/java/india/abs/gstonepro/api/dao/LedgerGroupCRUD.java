/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class LedgerGroupCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public LedgerGroup createLedgerGroup(Company company, LedgerGroup newLedgerGroup) {
        EventStatus createLG = new EventStatus();
        Session session = FACTORY.openSession();

        try {

            session.beginTransaction();

            newLedgerGroup.setCompany(company);
            session.save(newLedgerGroup);

            if (newLedgerGroup.getParentLedgerGroup() != null) {

                LedgerGroup lg = (LedgerGroup) session.get(LedgerGroup.class, newLedgerGroup.getParentLedgerGroup().getLedgerGroupId());
                lg.getChildLedgerGroups().add(newLedgerGroup);
                session.save(lg);

            }

            session.getTransaction().commit();
            createLG.setCreateDone(true);
            session.close();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return newLedgerGroup;
        } finally {

            session.close();
            return newLedgerGroup;
        }

    }

    public EventStatus updateLedgerGroup(Long ledgerGroupId, String ledgerGroupName, Set<Ledger> ledgers) {
        EventStatus updateUser = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            LedgerGroup ledgerGroup = (LedgerGroup) session.get(LedgerGroup.class, ledgerGroupId);
            ledgerGroup.setLedgerGroupName(ledgerGroupName);
            if (ledgers != null && ledgers.size() > 0) {
                ledgerGroup.setLedgers(ledgers);
            }
            session.save(ledgerGroup);
            session.getTransaction().commit();
            updateUser.setUpdateDone(true);
            session.close();
        }

        return updateUser;
    }

    public List<LedgerGroup> fetchAllLedgerGroups(Company company) {
        List<LedgerGroup> ledgerGroups = null;
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            //Hibernate Named Query  
            Query query = session.getNamedQuery("findLedgerGroupByCompany");
            query.setParameter("company", company);

            ledgerGroups = query.list();
            session.getTransaction().commit();

            session.close();
            return ledgerGroups;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return ledgerGroups;
        } finally {

            session.close();

        }
        return ledgerGroups;
    }

    public List<LedgerGroup> fetchChildLedgerGroups(LedgerGroup lg) {
        List<LedgerGroup> ledgerGroups = null;
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Hibernate.initialize(lg.getChildLedgerGroups());
            if (lg.getChildLedgerGroups() != null) {
                ledgerGroups = (List<LedgerGroup>) lg.getChildLedgerGroups();
            }
            session.getTransaction().commit();

            session.close();
            return ledgerGroups;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return ledgerGroups;
        } finally {

            session.close();

        }
        return ledgerGroups;
    }
    public LedgerGroup fetchLedgerGroupByName(Long companyId, String name) {
        Session session = FACTORY.openSession();
        LedgerGroup lg = null;
        try {

            Company com = session.get(Company.class, companyId);
            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", com);
            query.setParameter("ledgerGroupName", name);

            lg = (LedgerGroup) query.uniqueResult();

            return lg;
        } catch (Exception ex) {
            return lg;
        } finally {

            session.close();

        }

    }

    public LedgerGroup fetchLGByName(Company com, String name) {
        Session session = FACTORY.openSession();
        LedgerGroup lg = null;
        try {

            Query query = session.getNamedQuery("findLedgerGroupByName");
            query.setParameter("company", com);
            query.setParameter("ledgerGroupName", name);
            query.setParameter("ledgerGroupName2","");

            lg = (LedgerGroup) query.uniqueResult();

            return lg;
        } catch (Exception ex) {
            return lg;
        } finally {

            session.close();

        }

    }

    public LedgerGroup fetchALedgerGroup(Long id) {
        LedgerGroup ledgerGroup = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            ledgerGroup = (LedgerGroup) session.get(LedgerGroup.class, id);

            session.getTransaction().commit();

            session.close();
        }
        return ledgerGroup;

    }

    public EventStatus deleteLedgerGroup(Long ledgerGroupId) {
        
        EventStatus deleteLedgerGroup = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                LedgerGroup ledgerGroup = new LedgerGroup();
                ledgerGroup.setLedgerGroupId(ledgerGroupId);

                session.delete(ledgerGroup);

                session.getTransaction().commit();
                deleteLedgerGroup.setDeleteDone(true);
            }

            return deleteLedgerGroup;
        } catch (ConstraintViolationException ex) {
            return deleteLedgerGroup;

        } catch (HibernateException ex) {
            return deleteLedgerGroup;
        }
    }

}
