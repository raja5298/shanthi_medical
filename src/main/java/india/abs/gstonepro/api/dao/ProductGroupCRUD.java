/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.models.SalesReportModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.PersistenceException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class ProductGroupCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createProductGroup(ProductGroup newProductGroup, Company company,boolean editable, boolean deletable) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Company co = (Company) session.get(Company.class, company.getCompanyId());
            newProductGroup.setCompany(co);
            newProductGroup.setEditable(editable);
            newProductGroup.setDeletable(deletable);
            session.save(newProductGroup);
            co.getProductGroups().add(newProductGroup);
            session.save(co);
            session.getTransaction().commit();
            result.setCreateDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
            return result;
        }

    }

    public List<ProductGroup> getCompanyProductGroups(Company company) {

        List<ProductGroup> pgs = null;
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Query query = session.getNamedQuery("findAllPGByCompany");
            query.setParameter("company", company);

            pgs = query.list();
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
            return pgs;
        }

    }

    public EventStatus updateProductGroup(ProductGroup updatedPG) {

        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            ProductGroup pg = (ProductGroup) session.get(ProductGroup.class, updatedPG.getProductGroupId());
            pg.setProductGroupName(updatedPG.getProductGroupName());
            session.save(pg);
            session.getTransaction().commit();
            result.setUpdateDone(true);
            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
            return result;
        }

    }

    public EventStatus deleteProductGroup(Long productGroupId) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            ProductGroup pg = new ProductGroup();
            pg.setProductGroupId(productGroupId);
            session.delete(pg);
            session.getTransaction().commit();

            result.setDeleteDone(true);
            return result;

        } catch (PersistenceException px) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                px.printStackTrace();
                session.getTransaction().rollback();
                result.setMessage("Please remove the products from product group");
                return result;
            }

        } finally {
            session.close();
            result.setMessage("Please remove the products from product group");
            return result;
        }

    }

    public ProductGroup getAPG(Long productGroupId) {
        ProductGroup pg = null;
        Session session = FACTORY.openSession();
        try {
            pg = (ProductGroup) session.get(ProductGroup.class, productGroupId);
            return pg;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return pg;
            }

        } finally {
            session.close();
            return pg;
        }
    }

    public Map<ProductGroup, SalesReportModel> getProductGroupSalesReport(Date fromDate, Date toDate) {

        List<ProductGroup> pgs = getCompanyProductGroups(SessionDataUtil.getSelectedCompany());
        Map<ProductGroup, SalesReportModel> pgSalesReport = new HashMap<ProductGroup, SalesReportModel>();
        List<PurchaseSaleLineItem> psLis;
        Session session;
        session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Query query = session.getNamedQuery("findPSLIByProductGroups");
            query.setParameterList("productGroups", pgs);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("cancelled", false);
            

            psLis = query.list();

            for (PurchaseSaleLineItem psLi : psLis) {
                if (pgSalesReport.containsKey(psLi.getProduct().getProductGroup())) {
                    SalesReportModel srm = pgSalesReport.get(psLi.getProduct().getProductGroup());
                    srm.setTotalAmount(psLi.getValue().add(srm.getTotalAmount()));
                    srm.setProfit(psLi.getProfit().add(srm.getProfit()));
                    srm.setCGSTAmt(psLi.getCgstValue().add(srm.getCGSTAmt()));
                    srm.setIGSTAmt(psLi.getIgstValue().add(srm.getIGSTAmt()));
                    srm.setSGSTAmt(psLi.getSgstValue().add(srm.getSGSTAmt()));
                    pgSalesReport.put(psLi.getProduct().getProductGroup(), srm);
                } else {
                    SalesReportModel srm = new SalesReportModel();
                    srm.setTotalAmount(psLi.getValue());
                    srm.setProfit(psLi.getProfit());
                    srm.setCGSTAmt(psLi.getCgstValue());
                    srm.setIGSTAmt(psLi.getIgstValue());
                    srm.setSGSTAmt(psLi.getSgstValue());
                    pgSalesReport.put(psLi.getProduct().getProductGroup(), srm);
                }
            }

            session.getTransaction().commit();
            return pgSalesReport;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
            return pgSalesReport;
        }

    }

}
