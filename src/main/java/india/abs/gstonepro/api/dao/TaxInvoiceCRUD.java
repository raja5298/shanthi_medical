/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 *
 * @author SGS
 */
public class TaxInvoiceCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    GoDownStockCRUD gos4 = new GoDownStockCRUD();
    JournalCRUD j4 = new JournalCRUD();

    public EventStatus createTaxInvoice(Session passedSession, PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = new EventStatus();

        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();
        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            Company co = (Company) session.get(Company.class, newPS.getCompany().getCompanyId());

            Date lastTaxInvoiceDate = co.getCompanyBook().getLastTaxInvoiceDate();

            LocalDate lastTaxIDate = Instant.ofEpochMilli(lastTaxInvoiceDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            LocalDate txInvoiceDate = Instant.ofEpochMilli(newPS.getPsDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            if (lastTaxIDate.isAfter(txInvoiceDate)) {

                result.setMessage("Please check the Date. Last Invoice Date is " + lastTaxIDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)).toString());
                return result;
            }

            newPS.setPsType("TX-I");

            newPS.setCompany(co);

            Long txCount = co.getCompanyBook().getTaxInvoiceCount() + 1;
            newPS.setBillNo(co.getCompanyPolicy().getTaxInvoicePrefix() + txCount + co.getCompanyPolicy().getTaxInvoiceSuffix());

            Long txIdCount = co.getCompanyBook().getTaxInvoiceIdCount() + 1;
            newPS.setPsId(co.getCompanyId() + "TX-I" + txIdCount);

            co.getCompanyBook().setTaxInvoiceCount(txCount);
            co.getCompanyBook().setTaxInvoiceIdCount(txIdCount);
            co.getCompanyBook().setLastTaxInvoiceDate(newPS.getPsDate());

            session.save(co);

            Audit txAudit = AuditDataUtil.getAuditForCreate();
            newPS.setAuditData(txAudit);
            newPS.setPsLineItems(new ArrayList<PurchaseSaleLineItem>());
            newPS.setPsTaxSummaries(new ArrayList<PurchaseSaleTaxSummary>());
            session.save(newPS);

            for (PurchaseSaleLineItem psLI : psItems) {
                PurchaseSaleLineItem newPSLI = new PurchaseSaleLineItem();

                Long stockQuantityToChange = 0L;
                int uqc2Value = psLI.getProduct().getUQC2Value();
                int totalStockInUQC2 = (psLI.getUqcOneQuantity() * uqc2Value) + psLI.getUqcTwoQuantity();
                stockQuantityToChange = new Long(-(totalStockInUQC2));

                newPSLI.setPurchaseSale(newPS);
                newPSLI.setCgstPercentage(psLI.getCgstPercentage());
                newPSLI.setCgstValue(psLI.getCgstValue());
                newPSLI.setIgstPercentage(psLI.getIgstPercentage());
                newPSLI.setIgstValue(psLI.getIgstValue());
                newPSLI.setSgstPercentage(psLI.getSgstPercentage());
                newPSLI.setSgstValue(psLI.getSgstValue());
                newPSLI.setDiscountPercent(psLI.getDiscountPercent());
                newPSLI.setDiscountValue(psLI.getDiscountValue());
                newPSLI.setCessAmount(psLI.getCessAmount());
                newPSLI.setLineNumber(psLI.getLineNumber());
                newPSLI.setHsnSac(psLI.getHsnSac());
                newPSLI.setUqcOne(psLI.getUqcOne());
                newPSLI.setUqcTwo(psLI.getUqcTwo());
                newPSLI.setUqcOneQuantity(psLI.getUqcOneQuantity());
                newPSLI.setUqcTwoQuantity(psLI.getUqcTwoQuantity());
                newPSLI.setUqcOneRate(psLI.getUqcOneRate());
                newPSLI.setUqcTwoRate(psLI.getUqcTwoRate());
                newPSLI.setUqcOneValue(psLI.getUqcOneValue());
                newPSLI.setUqcTwoValue(psLI.getUqcTwoValue());
                newPSLI.setProductName(psLI.getProductName());
                newPSLI.setProduct(psLI.getProduct());
                newPSLI.setValue(psLI.getValue());
                newPSLI.setProfit(psLI.getProfit());
                newPSLI.setUqc1BaseRate(psLI.getUqc1BaseRate());
                newPSLI.setUqc2BaseRate(psLI.getUqc2BaseRate());
                newPSLI.setStockQuantity(psLI.getStockQuantity());
                newPSLI.setDescriptionOne(psLI.getDescriptionOne());
                newPSLI.setDescriptionTwo(psLI.getDescriptionTwo());
                newPSLI.setDescriptionThree(psLI.getDescriptionThree());
                newPSLI.setDescriptionFour(psLI.getDescriptionFour());
                newPSLI.setDescriptionFive(psLI.getDescriptionFive());
                newPSLI.setDescriptionSix(psLI.getDescriptionSix());
                newPSLI.setDescriptionSeven(psLI.getDescriptionSeven());
                newPSLI.setDescriptionEight(psLI.getDescriptionEight());
                newPSLI.setDescriptionNine(psLI.getDescriptionNine());
                newPSLI.setDescriptionTen(psLI.getDescriptionTen());
                newPSLI.setDescriptionCount(psLI.getDescriptionCount());
                newPSLI.setService(psLI.isService());
                newPSLI.setUqcOneRateWithTax(psLI.getUqcOneRateWithTax());
                newPSLI.setUqcTwoRateWithTax(psLI.getUqcTwoRateWithTax());

                newPSLI.setAuditData(txAudit);
                newPSLI.setGodown(psLI.getGodown());
                newPS.getPsLineItems().add(newPSLI);
                
                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    if (!newPSLI.getProduct().isService()) {
                        GoDownStockEntry newGStock = gos4.changeProductStock(session, newPSLI.getGodown(), newPSLI.getProduct(), stockQuantityToChange, psLI.getValue(), newPS.getPsDate(), "TX-I", psLI.getIgstPercentage(), psLI.getCgstPercentage(), psLI.getSgstPercentage(), psLI.getIgstValue(), psLI.getCgstValue(), psLI.getSgstValue(), newPSLI, null, null,null);
                        if (newGStock == null) {
                            throw new HibernateException("Product Stock Update Failed during Tax Invoice");
                        }
                        newPSLI.setGodownStockEntry(newGStock);
                    }
                }
                session.save(newPSLI);

            }

            for (PurchaseSaleTaxSummary psTS : psTaxSummaries) {
                PurchaseSaleTaxSummary newPSTS = new PurchaseSaleTaxSummary();
                newPSTS.setPurchaseSale(newPS);
                newPSTS.setHsnSac(psTS.getHsnSac());
                newPSTS.setIgstPercentage(psTS.getIgstPercentage());
                newPSTS.setIgstValue(psTS.getIgstValue());
                newPSTS.setCgstPercentage(psTS.getCgstPercentage());
                newPSTS.setCgstValue(psTS.getCgstValue());
                newPSTS.setSgstPercentage(psTS.getSgstPercentage());
                newPSTS.setSgstValue(psTS.getSgstValue());
                newPSTS.setAuditData(txAudit);
                newPS.getPsTaxSummaries().add(newPSTS);
                newPSTS.setTaxableValue(psTS.getTaxableValue());
                newPSTS.setTotalTaxValue(psTS.getTotalTaxValue());
                session.save(newPSTS);
            }

            if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {

                Journal saleJournal = j4.createASaleJournal(passedSession, newPS.getCompany(), newPS.getLedger(), newPS.getBillNo(), newPS.getCalculatedTotalValue(), newPS.getPsDate());
                if (saleJournal == null) {
                    throw new HibernateException("Accounts Failed");
                }
                newPS.setPurchaseSaleJournal(saleJournal);
            }
            session.save(newPS);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setCreateDone(true);
            result.setBillID(newPS.getPsId());

            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }
        }
        return result;
    }

    public EventStatus cancelTaxInvoice(Session passedSession, String psId) {
        EventStatus result = new EventStatus();

        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class, psId);
            EventStatus deleteResult = new GoDownStockCRUD().removeSalesStockEntries(session, ps);

            if (deleteResult.isDeleteDone()) {

                Query deletePSLIQuery = session.createQuery("update PurchaseSaleLineItem p set p.cancelled=:isCancelled where p.purchaseSale =:ps");
                deletePSLIQuery.setParameter("ps", ps);
                deletePSLIQuery.setParameter("isCancelled", true);

                int taxSummaryResult = -1, lineItemsResult = -1;

                lineItemsResult = deletePSLIQuery.executeUpdate();

                Query deletePSTXSQuery = session.createQuery("update PurchaseSaleTaxSummary p set p.cancelled=:isCancelled where p.purchaseSale =:ps");
                deletePSTXSQuery.setParameter("ps", ps);
                deletePSTXSQuery.setParameter("isCancelled", true);

                taxSummaryResult = deletePSTXSQuery.executeUpdate();

                if (lineItemsResult < 0 || taxSummaryResult < 0) {
                    throw new HibernateException("Tax Invoice isCancelled Update during cancel TX-I Failed");
                }

                if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {
                    EventStatus deleteJournalResult = j4.deleteASaleJournal(session, ps);
                    if (!deleteJournalResult.isDeleteDone()) {
                        throw new HibernateException("Journal Delete Failed");
                    }
                }
                ps.setCancelled(true);
                ps.getAuditData().setDeletedBy(SessionDataUtil.getSelectedUser());
                ps.getAuditData().setDeletedOn(new Date());

                session.save(ps);
                Company c = (Company) session.get(Company.class, ps.getCompany().getCompanyId());
                Query maxDateQuery = session.createQuery("select max(psDate) from PurchaseSale where company = :company and cancelled = :isCancelled");
                maxDateQuery.setParameter("company", c);
                maxDateQuery.setParameter("isCancelled", false);
                Date lastDate = (Date) maxDateQuery.getSingleResult();
                if (lastDate != null) {
                    c.getCompanyBook().setLastTaxInvoiceDate(lastDate);
                } else {
                    c.getCompanyBook().setLastTaxInvoiceDate(c.getCompanyPolicy().getFinancialYearStart());
                }
                session.save(c);
                if (!sessionAvailable) {
                    session.getTransaction().commit();
                }
                result.setDeleteDone(true);
            } else {
                result.setMessage("Sale Delete Failed");
                return result;
            }

            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            hx.printStackTrace();

        }catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            hx.printStackTrace();

        }
        
        finally {
            if (!sessionAvailable) {
                session.close();
            }

        }
        return result;
    }

    public EventStatus updateTaxInvoice(PurchaseSale newPS, List<PurchaseSaleLineItem> psItems, List<PurchaseSaleTaxSummary> psTaxSummaries) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            EventStatus cancelTXIResult;
            session.beginTransaction();
            Company co = (Company) session.get(Company.class, newPS.getCompany().getCompanyId());
            newPS.setPsDate(co.getCompanyBook().getLastTaxInvoiceDate());
            cancelTXIResult = cancelTaxInvoice(session, newPS.getPsId());
            if (newPS.getLedger() == null || newPS.getLedger().getLedgerId() == null) {
                newPS.setLedger(null);
            }

            if (cancelTXIResult.isDeleteDone()) {
                EventStatus insertTXIResult = createTaxInvoice(session, newPS, psItems, psTaxSummaries);
                if (insertTXIResult.isCreateDone()) {
                    result.setUpdateDone(true);
                    session.getTransaction().commit();
                    return result;
                } else {
                    throw new HibernateException("Create after Delete Tax Invoice Failed");
                }
            } else {
                throw new HibernateException("Delete Tax Invoice Failed");
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {

            session.close();

        }

        return result;
    }

}
