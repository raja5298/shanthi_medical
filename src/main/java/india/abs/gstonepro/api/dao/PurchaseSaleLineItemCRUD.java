/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author SGS
 */
public class PurchaseSaleLineItemCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createPSLineItem(String newPSId, PurchaseSaleLineItem newPSLineItem) {
        EventStatus result = new EventStatus();
        try (Session session = FACTORY.openSession()) {

            session.beginTransaction();

            PurchaseSale ps = (PurchaseSale) session.get(PurchaseSale.class, newPSId);

            newPSLineItem.setPurchaseSale(ps);

            session.save(newPSLineItem);

            session.getTransaction().commit();
            result.setCreateDone(true);
            session.close();
        }

        return result;
    }

    public EventStatus deletePSLineItem(Long psLineItemId) {
        EventStatus deletePSLineItem = new EventStatus();
        try {
            try (Session session = FACTORY.openSession()) {
                session.beginTransaction();
                PurchaseSaleLineItem psli = new PurchaseSaleLineItem();
                psli.setPsliId(psLineItemId);

                session.delete(psli);

                session.getTransaction().commit();
                deletePSLineItem.setDeleteDone(true);
            }

            return deletePSLineItem;
        } catch (ConstraintViolationException ex) {
            return deletePSLineItem;

        } catch (HibernateException ex) {
            return deletePSLineItem;
        }
    }

}
