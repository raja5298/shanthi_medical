/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.billofsupply;

import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CreateBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus createBOS(Session passedSession, BillOfSupply bos, List<BillOfSupplyLineItem> bosLis, List<BillOfSupplyChargeItem> bosCis, Company selectedCompany) throws ParseException {
        EventStatus createdBOS = new EventStatus();
        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }

        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }

            Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());
          

            Date lastBOSDate = co.getCompanyBook().getLastBOSDate();

            LocalDate lastBosDate = Instant.ofEpochMilli(lastBOSDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            LocalDate bosDate = Instant.ofEpochMilli(bos.getBosDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

            if (lastBosDate.isAfter(bosDate)) {

                createdBOS.setMessage("Please check the Date. Last Invoice Date is " + lastBosDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)).toString());
                return createdBOS;
            }

            BillOfSupply newBOS = createNewBOS(session, bos, selectedCompany);
            for (BillOfSupplyLineItem bosLi : bosLis) {
                bosLi.setBillOfSupply(newBOS);
                newBOS.getBosLineItems().add(createNewBOSLI(session, bosLi));
            }

            for (BillOfSupplyChargeItem bosCi : bosCis) {
                bosCi.setBillOfSupply(newBOS);
                newBOS.getBosChargeItems().add(createNewBOSCI(session, bosCi));
            }
            session.save(newBOS);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }

            createdBOS.setCreateDone(true);
            createdBOS.setBillNo(newBOS.getBosNo());
            createdBOS.setBillID(newBOS.getBosId());

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } finally {
            if (!sessionAvailable) {
                session.close();
            }
        }

        return createdBOS;
    }

    private BillOfSupply createNewBOS(Session session, BillOfSupply newBOS, Company selectedCompany) {
        Company co = (Company) session.get(Company.class, selectedCompany.getCompanyId());

        Long bosCount = co.getCompanyBook().getBosCount() + 1;
        newBOS.setBosNo(bosCount + "");
        
        Long bosIdCount = co.getCompanyBook().getBosIdCount()+1;
        newBOS.setBosId(co.getCompanyId() + "BOS" + bosIdCount);
        
        newBOS.setAuditData(AuditDataUtil.getAuditForCreate());
        newBOS.setBosChargeItems(new ArrayList<BillOfSupplyChargeItem>());
        newBOS.setBosLineItems(new ArrayList<BillOfSupplyLineItem>());
        session.save(newBOS);

        co.getCompanyBook().setBosCount(bosCount);
        co.getCompanyBook().setBosIdCount(bosIdCount);
        co.getCompanyBook().setLastBOSDate(newBOS.getBosDate());

        session.save(co);
        return newBOS;
    }

    private BillOfSupplyLineItem createNewBOSLI(Session session, BillOfSupplyLineItem bosLi) {
        bosLi.setAuditData(AuditDataUtil.getAuditForCreate());
        session.save(bosLi);
        return bosLi;
    }

    private BillOfSupplyChargeItem createNewBOSCI(Session session, BillOfSupplyChargeItem bosCi) {
        bosCi.setAuditData(AuditDataUtil.getAuditForCreate());
        session.save(bosCi);
        return bosCi;
    }

}
