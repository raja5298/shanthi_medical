
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import org.flywaydb.core.Flyway;
import org.h2.message.DbException;
import org.h2.tools.DeleteDbFiles;
import org.h2.tools.Restore;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class BackupRestoreCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public boolean backupH2DB(String filePath) {

        Session session = FACTORY.getCurrentSession();
        // Begin of a sql query n the session
        try {
            session.beginTransaction();
            // create data consistent call of backup command of h2 and execute it directly
            // session.createSQLQuery("BACKUP TO \'"+filePath+"\'").executeUpdate();
//            session.createNamedQuery("SCRIPT TO \'" + filePath + "\'").executeUpdate();
            session.createNativeQuery("BACKUP TO \'" + filePath + "\'").executeUpdate();
            // finalize sql query, flush session
            session.getTransaction().commit();
            return true;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {
            return false;
        } finally {

            session.close();

        }
        return false;
    }

    public boolean restoreH2DB(String parentDir, String fileName) {

        try {
            HibernateUtil.shutdown();
            DeleteDbFiles.execute("./.gst1pro/", "gstonepro", false);

            Restore.execute(parentDir + "/" + fileName, "./.gst1pro/", "gstonepro");

            return true;
        } catch (DbException ex) {
            ex.printStackTrace();

            return false;

        } finally {

        }
    }

    public void migrateDB() {
        // Create the Flyway instance
        Flyway flyway = new Flyway();

        // Point it to the database
        flyway.setDataSource("jdbc:h2:./.gst1prod/gstonepro;AUTO_SERVER=TRUE", null, null);

        // Start the migration
        flyway.migrate();
    }

    public EventStatus purge(Company selectedCompany) {
        EventStatus resultStatus = new EventStatus();
        Session session = FACTORY.getCurrentSession();
        // session.createSQLQuery("truncate table MyTable").executeUpdate();
        try {
            session.beginTransaction();
            int result = 0;

            Query deleteAllGoDownStockEntries = session.createQuery("delete from GoDownStockEntry t  where  stockUpdateType!=:type");
            // deleteAllGoDownStockEntries.setParameter("company", selectedCompany);
            deleteAllGoDownStockEntries.setParameter("type", "OPENING");
            int deleteStockEntriesResult = deleteAllGoDownStockEntries.executeUpdate();
            GoDownStockCRUD gos4 = new GoDownStockCRUD();
            for (Product pro : new ProductCRUD().fetchAllProductsWithoutService(selectedCompany.getCompanyId())) {
                for (GoDown go : new GoDownCRUD().fetchAllGodowns(selectedCompany.getCompanyId())) {
                    GoDownStockDetail gsd = gos4.getGoDownStockDetail(pro.getProductId(), go.getGoDownId());
                    gsd.setCurrentStock(gsd.getOpeningStock());
                    session.update(gsd);
                }
            }

            Query deleteAllTransactions = session.createQuery("delete from Transaction t  where t.type!=:type");
            //  deleteAllTransactions.setParameter("company", selectedCompany);
            deleteAllTransactions.setParameter("type", "OP");
            int deleteAllTransactionsResult = deleteAllTransactions.executeUpdate();

            Query deleteAllTaxSummary = session.createQuery("delete from PurchaseSaleTaxSummary t");
            //deleteAllTaxSummary.setParameter("company", selectedCompany);
            //int deleteAllTaxSummaryResult=0;
            int deleteAllTaxSummaryResult = deleteAllTaxSummary.executeUpdate();

            Query deleteAllSalesLineItems = session.createQuery("delete from PurchaseSaleLineItem");
            // deleteAllSalesLineItems.setParameter("company", selectedCompany);
            int deleteAllSalesLineItemsResult = deleteAllSalesLineItems.executeUpdate();

            Query deleteAllSales = session.createQuery("delete from PurchaseSale ");
            //deleteAllSales.setParameter("company", selectedCompany);
            int deleteAllSalesResult = deleteAllSales.executeUpdate();

            Query deleteAllPurchaseLineItems = session.createQuery("delete from PurchaseInvoiceLineItem ");
            // deleteAllPurchaseLineItems.setParameter("company", selectedCompany);
            int deleteAllPurchaseLineItemsResult = deleteAllPurchaseLineItems.executeUpdate();

            Query deleteAllPurchases = session.createQuery("delete from PurchaseInvoice ");
            // deleteAllPurchases.setParameter("company", selectedCompany);
            int deleteAllPurchasesResult = deleteAllPurchases.executeUpdate();

            Query deleteAllCrDrNoteLineItems = session.createQuery("delete from CreditDebitNoteLineItem ");
            // deleteAllCrDrNoteLineItems.setParameter("company", selectedCompany);
            int deleteAllCrDrNoteLineItemsResult = deleteAllCrDrNoteLineItems.executeUpdate();

            Query deleteAllCrDrNotes = session.createQuery("delete from CreditDebitNote");
            //deleteAllCrDrNotes.setParameter("company", selectedCompany);
            int deleteAllCrDrResult = deleteAllCrDrNotes.executeUpdate();

            Query deleteAllEstimateLineItem = session.createQuery("delete from EstimateLineItem");
            int deleteAllEstimateLineItemResult = deleteAllEstimateLineItem.executeUpdate();

            Query deleteAllEstimate = session.createQuery("delete from Estimate");
            int deleteAllEstimateResult = deleteAllEstimate.executeUpdate();

            result = deleteAllTaxSummaryResult + deleteAllSalesLineItemsResult + deleteAllSalesResult + deleteAllPurchaseLineItemsResult + deleteAllPurchasesResult;
            result += deleteAllCrDrNoteLineItemsResult + deleteAllCrDrResult + deleteStockEntriesResult + deleteAllTransactionsResult + deleteAllEstimateLineItemResult + deleteAllEstimateResult;

            Company company = (Company) session.get(Company.class, selectedCompany.getCompanyId());
            CompanyPolicy cp = company.getCompanyPolicy();

            company.getCompanyBook().setTaxInvoiceCount(new Long(0));
            company.getCompanyBook().setPurchaseCount(new Long(0));
            company.getCompanyBook().setCdNoteCount(new Long(0));
            company.getCompanyBook().setEstimateCount(new Long(0));
            company.getCompanyBook().setBosCount(new Long(0));
            company.getCompanyBook().setLastTaxInvoiceDate(cp.getFinancialYearStart());
            company.getCompanyBook().setLastEstimateDate(cp.getFinancialYearStart());
            company.getCompanyBook().setLastBOSDate(cp.getFinancialYearStart());

            company.setBosCount(new Long(0));
            company.setEstimateCount(new Long(0));
            session.save(company);
            cp.setTaxInvoiceSequenceStart(new Long(1));
            cp.setPurchaseSequenceStart(new Long(1));
            cp.setCrDrSequenceStart(new Long(1));
            cp.setEstimateSequenceStart(new Long(1));
            cp.setBosSequenceStart(new Long(1));
            session.save(cp);

            session.getTransaction().commit();
            if (result >= 0) {
                resultStatus.setDeleteDone(true);
                return resultStatus;
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }
            hx.printStackTrace();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            session.close();

        }
        return resultStatus;

    }

}
