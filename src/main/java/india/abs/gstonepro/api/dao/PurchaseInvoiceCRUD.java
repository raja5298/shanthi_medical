/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.utils.AuditDataUtil;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class PurchaseInvoiceCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();
    JournalCRUD j4 = new JournalCRUD();

    public EventStatus createPurchaseInvoice(Session passedSession, Company company, PurchaseInvoice newPI, List<PurchaseInvoiceLineItem> piLis) {
        EventStatus result = new EventStatus();
        GoDownStockCRUD gos4 = new GoDownStockCRUD();
        Audit ad = new Audit();
        ad.setCreatedBy(SessionDataUtil.getSelectedUser());
        ad.setCreatedOn(new Date());

        boolean sessionAvailable = (passedSession != null);
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            Audit piAudit = new Audit();
            if (!sessionAvailable) {
                session.beginTransaction();

                Company co = (Company) session.get(Company.class, company.getCompanyId());

                newPI.setCompany(co);

                Long piCount = co.getCompanyBook().getPurchaseCount() + 1;
                newPI.setPurchaseInvoiceNo(co.getCompanyPolicy().getPurchasePrefix() + piCount + co.getCompanyPolicy().getPurchaseSuffix());

                Long piIdCount = co.getCompanyBook().getPurchaseIdCount() + 1;
                newPI.setPurchaseInvoiceId(co.getCompanyId() + "P-I" + piIdCount);

                co.getCompanyBook().setPurchaseCount(piCount);
                co.getCompanyBook().setPurchaseIdCount(piIdCount);
                session.save(co);

                piAudit = AuditDataUtil.getAuditForCreate();

            } else {
                Audit temp = AuditDataUtil.getAuditForUpdate();
//                piAudit.setCreatedBy(newPI.getAuditData().getCreatedBy());
//                piAudit.setCreatedOn(newPI.getAuditData().getCreatedOn());
                piAudit.setUpdatedBy(temp.getUpdatedBy());
                piAudit.setUpdatedOn(temp.getUpdatedOn());
            }
            newPI.setAuditData(piAudit);
            session.save(newPI);

            for (PurchaseInvoiceLineItem piLI : piLis) {

                Long stockQuantityToChange = 0L;
                int uqc2Value = piLI.getProduct().getUQC2Value();
                int totalStockInUQC2 = (piLI.getUqcOneQuantity() * uqc2Value) + piLI.getUqcTwoQuantity();
                stockQuantityToChange = new Long(totalStockInUQC2);

                piLI.setPurchaseInvoice(newPI);
                piLI.setAuditData(piAudit);
                newPI.getPiLineItems().add(piLI);
                session.save(piLI);
                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    GoDownStockEntry newGStock = gos4.changeProductStock(session, piLI.getGodown(), piLI.getProduct(), stockQuantityToChange, piLI.getValue(), newPI.getPurchaseInvoiceDate(), "P-I", piLI.getIgstPercentage(), piLI.getCgstPercentage(), piLI.getSgstPercentage(), piLI.getIgstValue(), piLI.getCgstValue(), piLI.getSgstValue(), null, piLI, null, null);
                    if (newGStock == null) {
                        throw new HibernateException("Product Stock Update Failed during Tax Invoice");
                    }
                    piLI.setGodownStockEntry(newGStock);
                }
                session.save(piLI);

            }

            if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {

                Journal purchaseJournal = j4.createAPurchaseJournal(passedSession, newPI.getCompany(), newPI.getLedger(), newPI.getSupplierInvoiceNo(), newPI.getCalculatedPurchaseInvoiceAmount(), newPI.getPurchaseInvoiceDate());
                if (purchaseJournal == null) {
                    throw new HibernateException("Accounts Failed");
                }
                newPI.setPurchaseJournal(purchaseJournal);
            }

            session.save(newPI);

            if (!sessionAvailable) {
                session.getTransaction().commit();
            }
            result.setCreateDone(true);
            result.setBillID(newPI.getPurchaseInvoiceNo());

            return result;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception ex) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
            }

        } finally {

            if (!sessionAvailable) {

                session.close();
            }
        }

        return result;
    }

    public PurchaseInvoice getPurchaseInvoice(String piId) {
        PurchaseInvoice pi = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            pi = (PurchaseInvoice) session.get(PurchaseInvoice.class, piId);
            Hibernate.initialize(pi.getPiLineItems());

            session.getTransaction().commit();
            return pi;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                return pi;
            }

        } finally {

            session.close();
            return pi;
        }
    }

    public List<PurchaseInvoice> getAllPurchaseInvoiceByDate(Date fromDate, Date toDate, Company selectedCompany) {
        List<PurchaseInvoice> pis = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Query query = session.getNamedQuery("findPIByDate");
            query.setParameter("company", selectedCompany);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            pis = query.list();
            session.getTransaction().commit();
            return pis;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

                return pis;
            }

        } finally {

            session.close();
            return pis;
        }
    }

    public List<PurchaseInvoiceLineItem> getAllPurchaseByProduct(Date fromDate, Date toDate, Product product, Company selectedCompnay) {
        List<PurchaseInvoiceLineItem> piLineItems = null;

        Session session;
        session = FACTORY.openSession();
        session.beginTransaction();

        //Hibernate Named Query  
        Query query = null;

        query = session.getNamedQuery("findPIByProduct");
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        query.setParameter("company", SessionDataUtil.getSelectedCompany());
        query.setParameter("product", product);

        piLineItems = query.list();
        session.getTransaction().commit();

        session.close();

        return piLineItems;
    }

    public List<PurchaseInvoice> getAllPurchaseInvoiceByDateAndLedger(Long partyId, Date fromDate, Date toDate, Company selectedCompany) {
        List<PurchaseInvoice> pis = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();

            Ledger ledger = (Ledger) session.get(Ledger.class, partyId);

            Query query = session.getNamedQuery("findPIByDateAndParty");
            query.setParameter("company", selectedCompany);
            query.setParameter("ledger", ledger);
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            pis = query.list();
            session.getTransaction().commit();
            return pis;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

                return pis;
            }

        } finally {

            session.close();
            return pis;
        }
    }

    public EventStatus deletePurchaseInvoice(Session passedSession, String piId) {
        EventStatus result = new EventStatus();
        GoDownStockCRUD gos4 = new GoDownStockCRUD();
        boolean sessionAvailable = (passedSession != null);
        String errorMessage = "Name\t\tCurrent Stock\t\tStock in Purchase\t\tResult";
        Session session = null;
        if (sessionAvailable) {
            session = passedSession;
        } else {
            session = FACTORY.openSession();

        }
        try {
            if (!sessionAvailable) {
                session.beginTransaction();
            }
            boolean canDeletePurchaseBill = true;
            PurchaseInvoice pi = (PurchaseInvoice) session.get(PurchaseInvoice.class, piId);
            Hibernate.initialize(pi.getPiLineItems());
            for (PurchaseInvoiceLineItem pili : pi.getPiLineItems()) {
                if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    GoDownStockDetail gsd = gos4.getGoDownStockDetail(pili.getProduct().getProductId(), pili.getGodownStockEntry().getGodown().getGoDownId());
                    Long stockAfterDeduction = gsd.getCurrentStock() - pili.getStockQuantity();
                    if (stockAfterDeduction < 0) {
                        canDeletePurchaseBill = false;
//                    errorMessage += pili.getProduct().getName() + "\t" + gsd.getCurrentStock() + "\t" + "(-)" + pili.getStockQuantity() + "\t" + stockAfterDeduction;
                        errorMessage = "Name : " + pili.getProduct().getName() + "\nCurrent Stock : " + gsd.getCurrentStock() + "\nStock in Purchase : " + pili.getStockQuantity() + "\nResult : " + stockAfterDeduction;
                    } else {
                        EventStatus removeStockEntries = gos4.removePurchaseStockEntries(session, pi);
                    }
                } else {
                    EventStatus removeStockEntries = gos4.removePurchaseStockEntries(session, pi);
                }
            }
            if (canDeletePurchaseBill) {

                if (SystemPolicyUtil.getSystemPolicy().isAccountsEnabled()) {
                    EventStatus deleteJournalResult = j4.deleteAPurchaseJournal(session, pi);
                    if (!deleteJournalResult.isDeleteDone()) {
                        throw new HibernateException("Journal Delete Failed");
                    }
                }

                session.delete(pi);

                if (!sessionAvailable) {
                    session.getTransaction().commit();
                }
                result.setDeleteDone(true);

                return result;
            } else {
                result.setMessage(errorMessage);

                return result;
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } finally {
            if (!sessionAvailable) {

                session.close();
            }
        }

        return result;
    }

    public EventStatus updatePurchaseInvoice(Company company, PurchaseInvoice updatedPI, List<PurchaseInvoiceLineItem> piLis) {
        EventStatus result = new EventStatus();
        Session session = FACTORY.openSession();

        try {
            session.getTransaction().begin();
            EventStatus deleteResult = deletePurchaseInvoice(session, updatedPI.getPurchaseInvoiceId());
            if (!deleteResult.isDeleteDone()) {
                result.setMessage(deleteResult.getMessage());
                return result;
            }
            EventStatus createResult = createPurchaseInvoice(session, company, updatedPI, piLis);

            session.getTransaction().commit();

            if (deleteResult.isDeleteDone() && createResult.isCreateDone()) {
                result.setUpdateDone(true);
                return result;
            }

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                return result;
            }

        } finally {

            session.close();

        }

        return result;
    }

    public List<PurchaseInvoice> getAllPurchaseInvoiceByParty(Ledger ledger) {
        List<PurchaseInvoice> pis = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            if (ledger != null) {
                Query query = session.getNamedQuery("findAllPIByParty");
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
                query.setParameter("ledger", ledger);
                pis = query.list();
            } else {
                Query query = session.getNamedQuery("findAllPI");
                query.setParameter("company", SessionDataUtil.getSelectedCompany());
//                query.setParameter("ledger", ledger);
                pis = query.list();
            }
            return pis;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

                return pis;
            }

        } finally {

            session.close();
            return pis;
        }
    }
}
