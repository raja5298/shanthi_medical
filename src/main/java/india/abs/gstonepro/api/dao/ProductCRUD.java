/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.GoDownStockEntry;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class ProductCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

//    ProductStockCRUD ps4 = new ProductStockCRUD();
    GoDownCRUD go4 = new GoDownCRUD();
    GoDownStockCRUD gse4 = new GoDownStockCRUD();

    public EventStatus createProduct(Long companyId, Product newProduct, List<GoDownStockDetail> gsds) {
        EventStatus createProduct = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Company company = (Company) session.get(Company.class, companyId);

            newProduct.setCompany(company);
            newProduct.setDescriptionOne("");
            newProduct.setDescriptionTwo("");
            newProduct.setDescriptionThree("");
            newProduct.setDescriptionFour("");
            newProduct.setDescriptionFive("");
            newProduct.setDescriptionSix("");
            newProduct.setDescriptionSeven("");
            newProduct.setDescriptionEight("");
            newProduct.setDescriptionNine("");
            newProduct.setDescriptionTen("");
            newProduct.setDescriptioncount(0);

            session.save(newProduct);

            GoDown primaryGoDown = go4.getPrimaryGoDown(companyId);
            PricingPolicyCRUD pp4 = new PricingPolicyCRUD();

            for (PricingPolicy pp : pp4.fetchAllPricingPolicies(companyId)) {
                SingleProductPolicy spp = new SingleProductPolicy(pp, newProduct, new BigDecimal(newProduct.getUQC2Value()).multiply(newProduct.getProductRate()), newProduct.getProductRate(), newProduct.isInclusiveOfGST(), true, true);
                pp.getSingleProductPolicies().add(spp);
                session.save(spp);
            }

            if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled() && gsds != null && (!newProduct.isService())) {

                for (GoDownStockDetail gsd : gsds) {

                    gsd.setProduct(newProduct);

                    GoDownStockEntry openingGoDownStockEntry = gse4.changeProductStock(session, gsd.getGodown(), newProduct, gsd.getOpeningStock(), gsd.getOpeningStockValue(), gsd.getOpeningStockDate(), "OPENING", 0.0f, 0.0f, 0.0f, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null, null, null,null);
                    if (openingGoDownStockEntry == null) {

                        createProduct.setMessage("Error during opening stock creation");
                        return createProduct;

                    }

                }
            }
            session.getTransaction().commit();
            session.close();
            createProduct.setCreateDone(true);
            return createProduct;

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
                return createProduct;
            }

        } catch (Exception ex) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
                return createProduct;
            }

        } finally {

            session.close();

        }

        return createProduct;
    }

    public Product getProduct(Long proudctId) {
        Product getProduct = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            getProduct = (Product) session.get(Product.class, proudctId);
            session.getTransaction().commit();
            session.close();
        }
        return getProduct;
    }

    public EventStatus updateProductDescription(Product updatedProduct) {
        EventStatus updateProduct = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.update(updatedProduct);
            session.getTransaction().commit();
            updateProduct.setUpdateDone(true);
            session.close();
        }

        return updateProduct;

    }

    public EventStatus updateProduct(Long companyId, Product updatedProduct) {

        EventStatus updateProduct = new EventStatus();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            Company company = (Company) session.get(Company.class, companyId);
            Product product = (Product) session.get(Product.class, updatedProduct.getProductId());

            product.setName(updatedProduct.getName());
            product.setTamilName(updatedProduct.getTamilName());
            product.setShortName(updatedProduct.getShortName());
            product.setProductRate(updatedProduct.getProductRate());
            product.setHsnCode(updatedProduct.getHsnCode());
            product.setUQC1(updatedProduct.getUQC1());
            product.setUQC2Value(updatedProduct.getUQC2Value());
            product.setUQC2(updatedProduct.getUQC2());
            product.setIgstPercentage(updatedProduct.getIgstPercentage());
            product.setCgstPercentage(updatedProduct.getCgstPercentage());
            product.setSgstPercentage(updatedProduct.getSgstPercentage());
            product.setInclusiveOfGST(updatedProduct.isInclusiveOfGST());
            product.setBaseProductRate(updatedProduct.getBaseProductRate());
            product.setBasePriceInclusiveOfGST(updatedProduct.isBasePriceInclusiveOfGST());
            product.setNillRated(updatedProduct.isNillRated());
            product.setExempted(updatedProduct.isExempted());
            product.setNonGST(updatedProduct.isNonGST());
            product.setService(updatedProduct.isService());
            product.setProductCode(updatedProduct.getProductCode());
//            product.setDescriptionOne(updatedProduct.getDescriptionOne());
//            product.setDescriptionTwo(updatedProduct.getDescriptionTwo());
//            product.setDescriptionThree(updatedProduct.getDescriptionThree());
//            product.setDescriptionFour(updatedProduct.getDescriptionFour());
//            product.setDescriptionFive(updatedProduct.getDescriptionFive());
//            product.setDescriptionSix(updatedProduct.getDescriptionSix());
//            product.setDescriptionSeven(updatedProduct.getDescriptionSeven());
//            product.setDescriptionEight(updatedProduct.getDescriptionEight());
//            product.setDescriptionNine(updatedProduct.getDescriptionNine());
//            product.setDescriptionTen(updatedProduct.getDescriptionTen());
//            product.setDescriptioncount(updatedProduct.getDescriptioncount());

            product.setCompany(company);
            if (updatedProduct.getProductGroup() != null) {
                ProductGroup pg = (ProductGroup) session.get(ProductGroup.class, updatedProduct.getProductGroup().getProductGroupId());
                product.setProductGroup(pg);
                session.save(product);
                pg.getProducts().add(product);
                session.update(pg);
            } else {
                product.setProductGroup(null);
                session.update(product);
            }

            session.getTransaction().commit();
            updateProduct.setUpdateDone(true);
            session.close();
        }

        return updateProduct;
    }

    public List<Product> fetchAllProducts(Long companyId) {
        List<Product> products = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllProductsByCompany");
            Company co = (Company) session.get(Company.class, companyId);
            query.setParameter("company", co);

            products = query.list();
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return products;
            }

        } finally {

            session.close();
            return products;

        }

    }

    public List<Product> fetchAllProductsWithoutService(Long companyId) {
        List<Product> products = null;
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("findAllProductsWithoutServiceByCompany");
            Company co = (Company) session.get(Company.class, companyId);
            query.setParameter("company", co);
            query.setParameter("isService", false);

            products = query.list();
            session.getTransaction().commit();
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                return products;
            }

        } finally {

            session.close();
            return products;

        }

    }

    public List<Product> fetchAllProductsWithStockEntries(Long companyId) {
        List<Product> products = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            //Company company = (Company) session.get(Company.class, companyId);
            Company company = (Company) session.get(Company.class, companyId);

            Hibernate.initialize(company.getProducts());

            if (company.getProducts() != null) {
                products = company.getProducts();
            }
            session.getTransaction().commit();
            session.close();

        }
        return products;

    }

    public List<ProductStockEntry> fetchAllProductStockEntries(Long productId) {
        List<ProductStockEntry> productStockEntries = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            //Company company = (Company) session.get(Company.class, companyId);
            Product product = (Product) session.get(Product.class, productId);

            Hibernate.initialize(product.getProductStocks());

            if (product.getProductStocks() != null) {
                productStockEntries = product.getProductStocks();
            }
            session.getTransaction().commit();
            session.close();

        }
        return productStockEntries;

    }

    public EventStatus deleteProduct(Long productId) {
        EventStatus deleteProduct = new EventStatus();
        Session session = FACTORY.openSession();
        try {

            session.beginTransaction();
            Product product = new Product();
            product.setProductId(productId);

            Query saleQuery = session.createQuery("select count(productName) from  PurchaseSaleLineItem s where s.product=:product");
            saleQuery.setParameter("product", product);
            Long saleCount = (Long) saleQuery.getSingleResult();

            Query purchaseQuery = session.createQuery("select count(productName) from  PurchaseInvoiceLineItem s where s.product=:product");
            purchaseQuery.setParameter("product", product);
            Long purchaseCount = (Long) purchaseQuery.getSingleResult();

            Query crDrNoteQuery = session.createQuery("select count(productName) from  CreditDebitNoteLineItem s where s.product=:product");
            crDrNoteQuery.setParameter("product", product);
            Long crDrNoteCount = (Long) crDrNoteQuery.getSingleResult();

            Query estimateQuery = session.createQuery("select count(productName) from  EstimateLineItem s where s.product=:product");
            estimateQuery.setParameter("product", product);
            Long estimateCount = (Long) estimateQuery.getSingleResult();

            Long total = saleCount + purchaseCount + crDrNoteCount + estimateCount;

            if (total == 0) {

                Query deleteProductFromGoDownEntry = session.createQuery("delete from GoDownStockEntry g  where g.product =:product");
                deleteProductFromGoDownEntry.setParameter("product", product);

                int goDownEntryDelete = deleteProductFromGoDownEntry.executeUpdate();

                Query deleteProductFromGoDown = session.createQuery("delete from GoDownStockDetail g  where g.product =:product");
                deleteProductFromGoDown.setParameter("product", product);

                int goDownDelete = deleteProductFromGoDown.executeUpdate();

                Query deleteProductFromPricingPolicy = session.createQuery("delete from SingleProductPolicy s  where s.product =:product");
                deleteProductFromPricingPolicy.setParameter("product", product);

                int pricingPolicyDelete = deleteProductFromPricingPolicy.executeUpdate();

                if (goDownEntryDelete < 0 || goDownDelete < 0 || pricingPolicyDelete < 0) {
                    throw new HibernateException("Product Delete Failed");
                }

                session.delete(product);
                session.getTransaction().commit();

                deleteProduct.setDeleteDone(true);
                return deleteProduct;

            } else {
                deleteProduct.setMessage("Please remove the product from Bills and try again");
                return deleteProduct;
            }

        } catch (HibernateException ex) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
                deleteProduct.setMessage("Product Delete Failed");
                return deleteProduct;
            }
        } finally {
            session.close();
        }
        deleteProduct.setMessage("Product Delete Failed");
        return deleteProduct;
    }

    public ProductStockEntry getOpeningStock(Long productId) {
        ProductStockEntry openingStock = new ProductStockEntry();
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            //Company company = (Company) session.get(Company.class, companyId);
            Product product = (Product) session.get(Product.class, productId);

            Hibernate.initialize(product.getProductStocks());

            for (ProductStockEntry ps : product.getProductStocks()) {
                if (ps.getStockUpdateType().equalsIgnoreCase("OPENING")) {
                    openingStock = ps;
                }
            }
            session.getTransaction().commit();
            session.close();

        }

        return openingStock;
    }

    public void updateProductStock(Long productId) {

    }

}
