/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao.bos;

import india.abs.gstonepro.api.dao.bos.*;
import india.abs.gstonepro.api.dao.billofsupply.*;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.BOS;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class UpdateBOS {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    CreateBOS c4 = new CreateBOS();
    CancelBOS can4 = new CancelBOS();

    public EventStatus updateBOS(BOS bos, List<BOSLineItem> bosLineItems, Company selectedCompany) throws ParseException {
        EventStatus updatedBOS = new EventStatus();
        Session session = FACTORY.openSession();

        try {
            session.beginTransaction();
            EventStatus deleteResult=can4.cancelBOS(session, bos.getBOSId());
            if(!deleteResult.isDeleteDone()){
                 session.getTransaction().rollback();
                 deleteResult.setMessage("Update Failed");
                return deleteResult;
            }
            Company co = (Company) session.get(Company.class, bos.getCompany().getCompanyId());
            
            bos.setBOSDate(co.getCompanyBook().getLastBOSDate());
            EventStatus createResult = c4.createBOS(session, bos, bosLineItems,selectedCompany);
            if(!createResult.isCreateDone()){
                session.getTransaction().rollback();
                createResult.setMessage("Update Failed");
                return createResult;
            }

            session.getTransaction().commit();
            if (deleteResult.isDeleteDone() && createResult.isCreateDone()) {
                updatedBOS.setUpdateDone(true);
                return updatedBOS;
            }
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                hx.printStackTrace();
                session.getTransaction().rollback();
            }

        } finally {
            session.close();
        }

        return updatedBOS;
    }

}
