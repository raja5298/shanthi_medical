/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

/**
 *
 * @author SGS
 */
public class PricingPolicyCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    ProductCRUD p4 = new ProductCRUD();

    public EventStatus createPricingPolicy(Company company, String name) {
        EventStatus createPricingPolicy = new EventStatus();
        List<Product> products = p4.fetchAllProducts(company.getCompanyId());
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            PricingPolicy pp = new PricingPolicy(company, name);
            pp.setEditable(true);
            pp.setDeletable(true);
            session.save(pp);

            for (Product p : products) {
                SingleProductPolicy spp = new SingleProductPolicy(pp, p,new BigDecimal(p.getUQC2Value()).multiply(p.getProductRate()),  p.getProductRate(), p.isInclusiveOfGST(), true, true);
                pp.getSingleProductPolicies().add(spp);
                session.save(spp);
            }
            session.save(pp);
            session.getTransaction().commit();
            session.close();
            createPricingPolicy.setCreateDone(true);

        }

        return createPricingPolicy;
    }

    public List<PricingPolicy> fetchAllPricingPolicies(Long companyId) {
        List<PricingPolicy> pricingPolicies = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            //Company company = (Company) session.get(Company.class, companyId);
            Company company = (Company) session.get(Company.class, companyId);
            
            Hibernate.initialize(company.getPricingPolicies());

            if (company.getPricingPolicies() != null) {
                pricingPolicies = company.getPricingPolicies();
            }
            session.getTransaction().commit();
            session.close();

        }
        return pricingPolicies;

    }

    public Set<SingleProductPolicy> fetchAllSingleProductPolicies(PricingPolicy pp) {
        Set<SingleProductPolicy> singleProductPolicies = null;
        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();

            //Company company = (Company) session.get(Company.class, companyId);
            PricingPolicy ppfromDB = (PricingPolicy) session.get(PricingPolicy.class, pp.getPricingPolicyId());
            

            Hibernate.initialize(ppfromDB.getSingleProductPolicies());

            if (ppfromDB.getSingleProductPolicies() != null) {
                singleProductPolicies = ppfromDB.getSingleProductPolicies();
                
            }
            session.getTransaction().commit();
            session.close();

        }
        return singleProductPolicies;

    }

    public EventStatus updateSingleProductPolicy(SingleProductPolicy spp) {
        EventStatus updateSPP = new EventStatus();

        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.update(spp);
            session.getTransaction().commit();
            session.close();
            updateSPP.setUpdateDone(true);

        }
        return updateSPP;

    }

    public EventStatus updatePricingPolicy(PricingPolicy pp) {
        EventStatus updateSPP = new EventStatus();

        try (Session session = FACTORY.openSession()) {
            session.beginTransaction();
            session.update(pp);
            session.getTransaction().commit();
            session.close();
            updateSPP.setUpdateDone(true);

        }
        return updateSPP;
    }

 public EventStatus deletePricingPolicy(Long ppId) {
        EventStatus deletePP = new EventStatus();
        try {
            Session session = FACTORY.openSession();
            session.beginTransaction();
            PricingPolicy pp = new PricingPolicy();
            pp.setPricingPolicyId(ppId);
            
            Query deleteProductFromGoDownEntry = session.createQuery("delete from SingleProductPolicy s  where s.productPolicy =:productPolicy");
                deleteProductFromGoDownEntry.setParameter("productPolicy", pp);

           int goDownEntryDelete = deleteProductFromGoDownEntry.executeUpdate();

            session.delete(pp);

            session.getTransaction().commit();
            deletePP.setDeleteDone(true);
            session.close();

            return deletePP;
        } catch (ConstraintViolationException ex) {
            return deletePP;

        } catch (HibernateException ex) {
            return deletePP;
        }
    }
}
