/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.api.dao;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.SystemPolicy;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.HibernateUtil;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import java.util.Calendar;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 *
 * @author SGS
 */
public class CompanyPolicyCRUD {

    private static final SessionFactory FACTORY = HibernateUtil.getSessionFactory();

    public EventStatus updateCompanyPolicy(CompanyPolicy updatedCP) {
        EventStatus updateCP = new EventStatus();
        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            CompanyPolicy cp = (CompanyPolicy) session.get(CompanyPolicy.class, updatedCP.getCompanyPolicyId());

            cp.setAccountsExpiryDate(updatedCP.getAccountsExpiryDate());
            cp.setNumeberOfDecimalPlaces(updatedCP.getNumeberOfDecimalPlaces());
            cp.setSaleBillCount(updatedCP.getSaleBillCount());
            cp.setProfitEnabled(updatedCP.isProfitEnabled());
            cp.setAmountTendered(updatedCP.isAmountTendered());

            cp.setSaleEditApproval(updatedCP.isSaleEditApproval());
            cp.setLedgerTransactionApproval(updatedCP.isLedgerTransactionApproval());
            cp.setUnderStockPossible(updatedCP.isUnderStockPossible());
            cp.setPrintPaperSize(updatedCP.getPrintPaperSize());

            cp.setNumberOfInvoiceCopies(updatedCP.getNumberOfInvoiceCopies());
            cp.setFirstInvoiceWord(updatedCP.getFirstInvoiceWord());
            cp.setSecondInvoiceWord(updatedCP.getSecondInvoiceWord());
            cp.setThirdInvoiceWord(updatedCP.getThirdInvoiceWord());
            cp.setFourthInvoiceWord(updatedCP.getFourthInvoiceWord());
            cp.setFifthInvoiceWord(updatedCP.getFifthInvoiceWord());
            cp.setTaxInvoiceWord(updatedCP.getTaxInvoiceWord());
            cp.setBillOfSupplyWord(updatedCP.getBillOfSupplyWord());
            cp.setEstimateBillWord(updatedCP.getEstimateBillWord());
            cp.setFooterHeading(updatedCP.getFooterHeading());
            cp.setFooterLineOne(updatedCP.getFooterLineOne());
            cp.setFooterLineTwo(updatedCP.getFooterLineTwo());
            cp.setFooterLineThree(updatedCP.getFooterLineThree());
            cp.setFooterLineFour(updatedCP.getFooterLineFour());

            cp.setA4PAddressLineOne(updatedCP.getA4PAddressLineOne());
            cp.setA4PAddressLineTwo(updatedCP.getA4PAddressLineTwo());
            cp.setA4PAddressLineThree(updatedCP.getA4PAddressLineThree());

            cp.setA5PAddressLineOne(updatedCP.getA5PAddressLineOne());
            cp.setA5PAddressLineTwo(updatedCP.getA5PAddressLineTwo());
            cp.setA5PAddressLineThree(updatedCP.getA5PAddressLineThree());

            cp.setA5LAddressLineOne(updatedCP.getA5LAddressLineOne());
            cp.setA5LAddressLineTwo(updatedCP.getA5LAddressLineTwo());
            cp.setA5LAddressLineThree(updatedCP.getA5LAddressLineThree());

            cp.setA7AddressLineOne(updatedCP.getA7AddressLineOne());
            cp.setA7AddressLineTwo(updatedCP.getA7AddressLineTwo());
            cp.setA7AddressLineThree(updatedCP.getA7AddressLineThree());

            if (updatedCP.getTaxInvoicePrefix() != null || updatedCP.getTaxInvoiceSuffix() != null || updatedCP.getTaxInvoiceSequenceStart().compareTo(new Long(1)) > 0) {

                Company co = (Company) session.get(Company.class, updatedCP.getCompany().getCompanyId());
                Query sumQuery = session.createQuery("select count(billNo) from PurchaseSale where company=:company and psDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", cp.getFinancialYearStart());

                sumQuery.setParameter("toDate", cp.getFinancialYearEnd());
                sumQuery.setParameter("company", co);
                Long stock = (Long) sumQuery.getSingleResult();
                if (stock.equals(new Long(0))) {
                    co.getCompanyBook().setTaxInvoiceCount(updatedCP.getTaxInvoiceSequenceStart() - 1);
                    cp.setTaxInvoicePrefix(updatedCP.getTaxInvoicePrefix());
                    cp.setTaxInvoiceSuffix(updatedCP.getTaxInvoiceSuffix());
                    cp.setTaxInvoiceSequenceStart(updatedCP.getTaxInvoiceSequenceStart());
                    session.save(co);
                }
            }

            if (updatedCP.getPurchaseSequenceStart().compareTo(new Long(1)) > 0) {

                Company co = (Company) session.get(Company.class, updatedCP.getCompany().getCompanyId());
                Query sumQuery = session.createQuery("select count(purchaseInvoiceNo) from PurchaseInvoice where company=:company and purchaseInvoiceDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", cp.getFinancialYearStart());

                sumQuery.setParameter("toDate", cp.getFinancialYearEnd());
                sumQuery.setParameter("company", co);
                Long stock = (Long) sumQuery.getSingleResult();
                if (stock.equals(new Long(0))) {
                    co.getCompanyBook().setPurchaseCount(updatedCP.getPurchaseSequenceStart() - 1);
                    cp.setPurchasePrefix(updatedCP.getPurchasePrefix());
                    cp.setPurchaseSuffix(updatedCP.getPurchaseSuffix());
                    cp.setPurchaseSequenceStart(updatedCP.getPurchaseSequenceStart());
                    session.save(co);
                }
            }

            if (updatedCP.getCrDrSequenceStart().compareTo(new Long(1)) > 0) {

                Company co = (Company) session.get(Company.class, updatedCP.getCompany().getCompanyId());
                Query sumQuery = session.createQuery("select count(cdNoteNo) from CreditDebitNote where company=:company and noteDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", cp.getFinancialYearStart());

                sumQuery.setParameter("toDate", cp.getFinancialYearEnd());
                sumQuery.setParameter("company", co);
                Long stock = (Long) sumQuery.getSingleResult();
                if (stock.equals(new Long(0))) {
                    co.getCompanyBook().setCdNoteCount(updatedCP.getCrDrSequenceStart() - 1);
                    cp.setCrDrPrefix(updatedCP.getCrDrPrefix());
                    cp.setCrDrSuffix(updatedCP.getCrDrSuffix());
                    cp.setCrDrSequenceStart(updatedCP.getCrDrSequenceStart());
                    session.save(co);
                }
            }
            if (updatedCP.getEstimateSequenceStart().compareTo(new Long(1)) > 0) {

                Company co = (Company) session.get(Company.class, updatedCP.getCompany().getCompanyId());
                Query sumQuery = session.createQuery("select count(estimateNo) from Estimate where company=:company and estimateDate between :fromDate and :toDate");
                sumQuery.setParameter("fromDate", cp.getFinancialYearStart());

                sumQuery.setParameter("toDate", cp.getFinancialYearEnd());
                sumQuery.setParameter("company", co);
                Long stock = (Long) sumQuery.getSingleResult();
                if (stock.equals(new Long(0))) {
                    co.getCompanyBook().setEstimateCount(updatedCP.getEstimateSequenceStart() - 1);
                    cp.setEstimatePrefix(updatedCP.getEstimatePrefix());
                    cp.setEstimateSuffix(updatedCP.getEstimateSuffix());
                    cp.setEstimateSequenceStart(updatedCP.getEstimateSequenceStart());
                    session.save(co);
                }
            }

            session.save(cp);
            session.getTransaction().commit();
            updateCP.setUpdateDone(true);

        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } finally {
            session.close();
            return updateCP;
        }

    }

    public EventStatus changeFinancialYear(CompanyPolicy cpWithDates) {

        EventStatus updateFY = new EventStatus();

        Date currentFinancialYearStart = SessionDataUtil.getCompanyPolicyData().getFinancialYearStart();
        Date currentFinancialYearEnd = SessionDataUtil.getCompanyPolicyData().getFinancialYearEnd();

        Date newFinancialYearStart = cpWithDates.getFinancialYearStart();
        Date newFinancialYarEnd = cpWithDates.getFinancialYearEnd();

        Session session = FACTORY.openSession();
        try {
            session.beginTransaction();
            CompanyPolicy cp = (CompanyPolicy) session.get(CompanyPolicy.class, SessionDataUtil.getSelectedCompany().getCompanyPolicy().getCompanyPolicyId());
            Company company = (Company) session.get(Company.class, SessionDataUtil.getSelectedCompany().getCompanyId());

            company.getCompanyBook().setTaxInvoiceCount(new Long(0));
            company.getCompanyBook().setPurchaseCount(new Long(0));
            company.getCompanyBook().setCdNoteCount(new Long(0));
            company.getCompanyBook().setEstimateCount(new Long(0));
            company.getCompanyBook().setBosCount(new Long(0));
            company.getCompanyBook().setLastTaxInvoiceDate(newFinancialYearStart);
            company.getCompanyBook().setLastEstimateDate(newFinancialYearStart);
            company.getCompanyBook().setLastBOSDate(newFinancialYearStart);

            company.setBosCount(new Long(0));
            company.setEstimateCount(new Long(0));
            session.save(company);
            cp.setFinancialYearStart(newFinancialYearStart);
            cp.setFinancialYearEnd(newFinancialYarEnd);
            cp.setTaxInvoiceSequenceStart(new Long(1));
            cp.setPurchaseSequenceStart(new Long(1));
            cp.setCrDrSequenceStart(new Long(1));
            cp.setEstimateSequenceStart(new Long(1));
            cp.setBosSequenceStart(new Long(1));
            session.save(cp);

            EventStatus carryForwardStockEvent = null;
            if (SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                carryForwardStockEvent
                        = carryForwardStock(session, currentFinancialYearStart, currentFinancialYearEnd, newFinancialYarEnd, newFinancialYarEnd, company);

            }
            if (carryForwardStockEvent == null || carryForwardStockEvent.isUpdateDone()) {
                session.getTransaction().commit();
                updateFY.setUpdateDone(true);
                return updateFY;

            }

        } catch (HibernateException hx) {
            System.out.println("hx " + hx);
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                hx.printStackTrace();
            }

        } catch (Exception ex) {
            System.out.println("ex " + ex);
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
                ex.printStackTrace();
            }

        } finally {
            session.close();
            return updateFY;
        }
    }

    public EventStatus carryForwardStock(Session passedSession, Date oldStartDate, Date oldEndDate, Date newStartDate, Date newEndDate, Company co) {
        GoDownCRUD go4 = new GoDownCRUD();
        ProductCRUD p4 = new ProductCRUD();
        EventStatus result = new EventStatus();
//        Session session = passedSession;
        Date start = oldStartDate;
        Date end;
        Session session = FACTORY.openSession();
        if (oldEndDate.before(newStartDate)) {
            end = newStartDate;
        }
        else {
            end = oldEndDate;
        }
        try {
            for (GoDown go : go4.fetchAllGodowns(co.getCompanyId())) {
                for (Product pro : p4.fetchAllProductsWithoutService(co.getCompanyId())) {

                    Query stockQuery = session.createQuery("select sum(quantity) from GoDownStockEntry where godown=:godown and product=:product and stockUpdateDate between :fromDate and :toDate");
                    stockQuery.setParameter("fromDate", start);

                    stockQuery.setParameter("toDate", end);
                    stockQuery.setParameter("godown", go);
                    stockQuery.setParameter("product", pro);
                    Long stock = (Long) stockQuery.getSingleResult();
                    GoDownStockDetail gsd;
                    Query query = session.getNamedQuery("findGoDownsProductDetail");
                    query.setParameter("product", pro);
                    query.setParameter("godown", go);
                    gsd = (GoDownStockDetail) query.getSingleResult();
                    gsd.setCurrentStock(stock);
                    gsd.setOpeningStock(stock);
                    gsd.setOpeningStockDate(newStartDate);
                    session.save(gsd);

                }

            }

            result.setUpdateDone(true);
            return result;
        } catch (HibernateException hx) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();
            }

        } catch (Exception ex) {

            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE
                    || session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                session.getTransaction().rollback();

            }

        } finally {
            session.close();
            return result;
        }
    }
}
