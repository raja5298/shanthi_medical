/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.dao.TaxCRUD;
import india.abs.gstonepro.api.dao.UserCRUD;
import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.business.CompanyLogic;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class InsertDataVigneshIndustries {

    public static void main(String[] args) {
        insertCompany();
    }

    public static void insertCompany() {
        System.out.println("START");

        CompanyLogic co4 = new CompanyLogic();
        Date now = new Date(1522540800 * 1000L);
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        c.add(Calendar.YEAR, 1);
        c.add(Calendar.HOUR, -24);
        Date nowPlus1YearMinus1Day = c.getTime();

//        co4.createCompany(new Company("SYMA BUILDING SOLUTIONS","Regular","1A,Wonderful Complex,S.T.C Road/~/Perumalpuram/~/ ","TIRUNELVELI-627007", "TIRUNELVELI","Tamil Nadu", "symabuildtechtvl@gmail.com","0462 4971977","9629888852","33FFQPS4807K1ZT",0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
        co4.createCompany(new Company("SRI VIGNESH INDUSTRIES", "Regular", "14/97, Nainarkulam Market Road,/~//~/ ", "Tirunelveli - 627 006", "TIRUNELVELI", "Tamil Nadu", "", "0462-2337534", "93667 03834", "", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);

        TaxCRUD t4 = new TaxCRUD();
        t4.createTax((float) 0.0);
        t4.createTax((float) 5.0);
        t4.createTax((float) 2.5);
        t4.createTax((float) 12.0);
        t4.createTax((float) 6.0);
        t4.createTax((float) 18.0);
        t4.createTax((float) 9.0);
        t4.createTax((float) 28.0);
        t4.createTax((float) 14.0);

        UserCRUD u4 = new UserCRUD();
        u4.createUser("admin", "9629888852", "onepro", true);

        uqcCRUD uqc = new uqcCRUD();

        uqc.createUQC("BAGS", "Measure", "BAG");
        uqc.createUQC("BALE", "Measure", "BAL");
        uqc.createUQC("BUNDLES", "Measure", "BDL");
        uqc.createUQC("BUCKLES", "Measure", "BKL");
        uqc.createUQC("BILLION OF UNITS", "Measure", "BOU");
        uqc.createUQC("BOX", "Measure", "BOX");
        uqc.createUQC("BOTTLES", "Measure", "BTL");
        uqc.createUQC("BUNCHES", "Measure", "BUN");
        uqc.createUQC("CANS", "Measure", "CAN");
        uqc.createUQC("CUBIC METERS", "Volume", "CBM");
        uqc.createUQC("CUBIC CENTIMETERS", "Volume", "CCM");
        uqc.createUQC("CENTIMETERS", "Length", "CMS");
        uqc.createUQC("CARTONS", "Measure", "CTN");
        uqc.createUQC("DOZENS", "Measure", "DOZ");
        uqc.createUQC("DRUMS", "Measure", "DRM");
        uqc.createUQC("GREAT GROSS", "Measure", "GGK");
        uqc.createUQC("GRAMMES", "Weight", "GMS");
        uqc.createUQC("GROSS", "Measure", "GRS");
        uqc.createUQC("GROSS YARDS", "Length", "GYD");
        uqc.createUQC("KILOGRAMS", "Weight", "KGS");
        uqc.createUQC("KILOLITRE", "Measure", "KLR");
        uqc.createUQC("KILOMETRE", "Length", "KME");
        uqc.createUQC("MILILITRE", "Volume", "MLT");
        uqc.createUQC("METERS", "Length", "MTR");
        uqc.createUQC("NUMBERS", "Measure", "NOS");
        uqc.createUQC("PACKS", "Measure", "PAC");
        uqc.createUQC("PIECES", "Measure", "PCS");
        uqc.createUQC("PAIRS", "Measure", "PRS");
        uqc.createUQC("QUINTAL", "Measure", "QTL");
        uqc.createUQC("ROLLS", "Measure", "ROL");
        uqc.createUQC("SETS", "Measure", "SET");
        uqc.createUQC("SQUARE FEET", "Area", "SQF");
        uqc.createUQC("SQUARE METERS", "Area", "SQM");
        uqc.createUQC("SQUARE YARDS", "Area", "SQY");
        uqc.createUQC("TABLETS", "Measure", "TBS");
        uqc.createUQC("TEN GROSS", "Measure", "TGM");
        uqc.createUQC("THOUSANDS", "Measure", "THD");
        uqc.createUQC("TONNES", "Measure", "TON");
        uqc.createUQC("TUBES", "Measure", "TUB");
        uqc.createUQC("US GALLONS", "Measure", "UGS");
        uqc.createUQC("UNITS", "Measure", "UNT");
        uqc.createUQC("YARDS", "Measure", "YDS");
        uqc.createUQC("OTHERS", "", "OTH");

        System.out.println("END");

    }
}
