package india.abs.gstonepro;

import india.abs.gstonepro.api.dao.CompanyCRUD;
import india.abs.gstonepro.api.dao.JournalCRUD;
import india.abs.gstonepro.api.dao.LedgerCRUD;
import india.abs.gstonepro.api.dao.LedgerGroupCRUD;
import india.abs.gstonepro.api.dao.ProductCRUD;

import india.abs.gstonepro.api.dao.TaxCRUD;
import india.abs.gstonepro.api.dao.TransactionCRUD;
import india.abs.gstonepro.api.dao.UserCRUD;
import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Journal;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.models.UQC;
import java.sql.Date;

/**
 *
 * @author SGS
 */
public class CRUDTest {

    public static void main(String[] args) {

        ProductCRUD p4 = new ProductCRUD();
      
         CompanyCRUD c4 = new CompanyCRUD();
      
    //   c4.createCompany("My Dear", "Distribution");
       // p4.createProduct(new Long(1),new Product( "SAKTHI SOAP 12", "சிவகுரு ஸ்ரீனிவாஸ் ", "SGS", "HSN", new Float(4.0), new Float(4.0), new Float(8.0), new Float(100.0), true, "BAGS", "PIECES", 12));
       // ps4.createProductStockEntry(4, new Long(2), "OPENING", new Float(10934.90), new Date(12 - 01 - 1992), new Float(10934.90), new Float(10934.90), new Float(10934.90), new Float(10934.90), new Float(10934.90), new Float(10934.90));

        for (Product p : p4.fetchAllProducts(new Long(1))) {
           // 
            p.printStockValue();
           for(ProductStockEntry ps:p4.fetchAllProductStockEntries(p.getProductId())){
               
           }
        }
        
    }

    private static void testCompanyCRUD() {
        CompanyCRUD c4 = new CompanyCRUD();
//        c4.createCompany("My Dear", "Distribution");
//        c4.updateCompany(new Long(25), "Hola1", "Production1");
        for (Company co : c4.fetchAllCompanies()) {
            
        }

    }

    private static void testTaxCRUD() {
        TaxCRUD t4 = new TaxCRUD();
        t4.createTax((float) 120.5);

        t4.updateTax(new Long(31), 9);
        for (Tax tax : t4.fetchAllTaxes()) {
            
        }
        t4.deleteTax(new Long(31));
        for (Tax tax : t4.fetchAllTaxes()) {
            
        }
    }

    private static void testUQCCRUD() {
        uqcCRUD uqc = new uqcCRUD();

        // uqc.createUQC("BAGz", "Measere", "BAK");
        uqc.updateUQC(new Long(35), "Bags", "Measure", "BAG");
        uqc.deleteUQC(new Long(36));
        for (UQC uqcO : uqc.fetchAllUQCs()) {
            
        }
    }

    private static void testUserCRUD() {
        UserCRUD u4 = new UserCRUD();
        u4.createUser("si", "77088896", "AG",true);
        u4.updateUser(new Long(2), "Siva", "7708889688", "SUBBU",true);
        for (AppUser user : u4.fetchAllUsers()) {
            
        }
        u4.deleteUser(new Long(2));
        for (AppUser user : u4.fetchAllUsers()) {
            
        }
    }

//    private static void testLedgerCRUD() {
//        LedgerGroupCRUD lg4 = new LedgerGroupCRUD();
//        lg4.createLedgerGroup("Sundry Deptors");
//
//        for (LedgerGroup lg : lg4.fetchAllLedgerGroups()) {
//            
//        }
//        lg4.updateLedgerGroup(new Long(7), "Cash-At-HAND", null);
//        for (LedgerGroup lg : lg4.fetchAllLedgerGroups()) {
//            
//        }
//        // lg4.deleteLedgerGroup(new Long(6));
//
//        for (LedgerGroup lg : lg4.fetchAllLedgerGroups()) {
//            
//        }
//
//        LedgerGroup ledgerGroup = null;
//        LedgerCRUD l4 = new LedgerCRUD();
//     //   l4.createLedger(ledgerGroup, "ABS", "Adress", "GSTIN", "email", "7708889688", "7598889688", "KVP", "TVL", "TN", (float) 12.4, new Date(12 - 01 - 1992));
//
////        for (Ledger ledger : l4.fetchAllLedgers()) {
////            
////        }
//    }

//    private static void ledgerGroupCRUDTest() {
//        LedgerGroup ledgerGroup = null;
//        LedgerCRUD l4 = new LedgerCRUD();
//
//        LedgerGroupCRUD lg4 = new LedgerGroupCRUD();
//        lg4.createLedgerGroup("WoW");
////        l4.createLedger(lg4.fetchALedgerGroup(new Long(2)), "ABS", "Adress", "GSTIN", "email", "7708889688", "7598889688", "KVP", "TVL", "TN", (float) 12.4, new Date(12 - 01 - 1992));
//
//        for (Ledger ledger : lg4.fetchALedgerGroup(new Long(2)).getLedgers()) {
//            
//        }
//    }

//    private static void testBasicAccountsCR() {
//        JournalCRUD j4 = new JournalCRUD();
//        LedgerCRUD l4 = new LedgerCRUD();
//        TransactionCRUD t4 = new TransactionCRUD();
//        j4.createJournal("PD1123");
//        for (Journal jo : j4.fetchAllJournals()) {
//            
//        }
//
//        t4.createTransaction("Purchase", new Date(12 - 01 - 1992), l4.fetchALedger(new Long(1)), j4.fetchAJournal("PD123"), "To Purchase", -1000);
//        t4.createTransaction("Sales", new Date(12 - 01 - 1992), l4.fetchALedger(new Long(3)), j4.fetchAJournal("PD123"), "To Sales", 1000);
//        for (Transaction t : j4.fetchAJournal("PD123").getTransactions()) {
//            
//        }
//        for (Transaction t : l4.fetchALedger(new Long(3)).getTransactions()) {
//            t4.deleteTransaction(t.getTransactionId());
//            
//        }
//    }
}
