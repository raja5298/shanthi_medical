/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.dao.TaxCRUD;
import india.abs.gstonepro.api.dao.UserCRUD;
import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.business.CompanyLogic;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SGS
 */
public class InsertData2 {

    public static void main(String[] args) {
        insertCompany();
    }

    public static void insertCompany() {
        System.out.println("START");

        CompanyLogic co4 = new CompanyLogic();

        Date now = new Date(1522540800 * 1000L);
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        c.add(Calendar.YEAR, 1);
        c.add(Calendar.HOUR, -24);
        Date nowPlus1YearMinus1Day = c.getTime();
        System.out.println(nowPlus1YearMinus1Day);

        co4.createCompany(new Company("VINS COMMUNICATION", "Regular", "12/A-1, SALAI STREET/~/VANNARPETTAI/~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", "vinsecshop@gmail.com", " ", "9486704606", " ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("VINS HARDWARE", "Regular", "102C-/1 /~/Nainarkulam Market Road/~/ ", "TIRUNELVELI-627006", "TIRUNELVELI", "Tamil Nadu", " ", "0462-2332974", " ", " ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("SAAMY BATTERIES - PRIYA POWER SYSTEMS", "Regular", "21/1 Vinayagar Nagar,NGO A Colony /~/A1/3 Trivandrum Road, NGO B Colony /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462 2552801", "9442286801", "33AGJPU3470E1ZC", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);

//        co4.createCompany(new Company("VINS COMMUNICATION", "Regular", "12/A-1, SALAI STREET/~/VANNARPETTAI/~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", "vinsecshop@gmail.com", " ", "9486704606", " ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("VINS HARDWARE", "Regular", "102C-/1 /~/Nainarkulam Market Road/~/ ", "TIRUNELVELI-627006", "TIRUNELVELI", "Tamil Nadu", " ", "0462-2332974", " ", "33AEAPV0716N1ZF", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("SAAMY BATTERIES - PRIYA POWER SYSTEMS", "Regular", "21/1 Vinayagar Nagar,NGO A Colony /~/A1/3 Trivandrum Road, NGO B Colony /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462 2552801", "9442286801", "33AGJPU3470E1ZC", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 2", "Regular", "10F3 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627004", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4221234", "8531886262", "33BBBBB0000B1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Guru Krupa Agencies", "Regular", "9/2 Madurai Road /~/Tvl junction /~/ ", "Tirunelveli 627001", "TIRUNELVELI", "Tamil Nadu", "gurukrupaagenciestvl@gmail.com", "", "7418425752", "33BFJPS5695J1ZQ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Saradha Silks", "Regular", "8/9, Gandhi Market /~/ /~/ ", "Palayamkottai", "TIRUNELVELI", "Tamil Nadu", "solomonajam@gmail.com ", " ", "9442468846", "33CCOPSO822E1ZQ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Chandra Store", "Regular", "21/10, Market East /~/ /~/ ", "Palayamkottai", "TIRUNELVELI", "Tamil Nadu", " ", " ", "8903877286", "33AJDPS6088G1ZY", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("OM Balaji Electrical", "Regular", "21/4, Market West /~/ /~/ ", "Palayamkottai", "TIRUNELVELI", "Tamil Nadu", "talk2bhuvanbl@gmail.com ", " ", "9894315169", " ", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("J.S Hardwares ", "Regular", "30,East Car Street /~/Town /~/ ", "TIRUNELVELI", "TIRUNELVELI", "Tamil Nadu", "", " ", "9092857776", "33AMBPT1580F1Z8", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Alpha Business Solutions Private Limited", "Regular", "10F3 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627002", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220008", "9150754631", "", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Talento Academy", "Regular", "10F3 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627002", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220008", "9150754631", "", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("GST One Pro", "Regular", "10F3 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627002", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220008", "9150754631", "", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 1", "Regular", "10F2 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220008", "8531887272", "33AAAAA0000A1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 2", "Regular", "10F2 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220009", "8531887273", "33AAAAA0001A1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 3", "Regular", "10F2 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220010", "8531887274", "33AAAAA0002A1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 4", "Regular", "10F2 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220011", "8531887275", "33AAAAA0003A1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
//        co4.createCompany(new Company("Demo Company 5", "Regular", "10F2 Trivandrum Road/~/Palayamkottai /~/ ", "TIRUNELVELI-627003", "TIRUNELVELI", "Tamil Nadu", " ", "0462-4220012", "8531887276", "33AAAAA0004A1Z5", 0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);
          co4.createCompany(new Company("SYMA BUILDING SOLUTIONS","Regular","1A,Wonderful Complex,S.T.C Road/~/Perumalpuram/~/ ","TIRUNELVELI-627007", "TIRUNELVELI","Tamil Nadu", "symabuildtechtvl@gmail.com","0462 4971977","9629888852","33FFQPS4807K1ZT",0L, 0L, 0L, 0L), now, nowPlus1YearMinus1Day);


        TaxCRUD t4 = new TaxCRUD();
        t4.createTax((float) 0.0);
        t4.createTax((float) 5.0);
        t4.createTax((float) 2.5);
        t4.createTax((float) 12.0);
        t4.createTax((float) 6.0);
        t4.createTax((float) 18.0);
        t4.createTax((float) 9.0);
        t4.createTax((float) 28.0);
        t4.createTax((float) 14.0);

        UserCRUD u4 = new UserCRUD();
        u4.createUser("admin", "7708889688", "onepro", true);

        uqcCRUD uqc = new uqcCRUD();

        uqc.createUQC("BAGS", "Measure", "BAG");
        uqc.createUQC("BALE", "Measure", "BAL");
        uqc.createUQC("BUNDLES", "Measure", "BDL");
        uqc.createUQC("BUCKLES", "Measure", "BKL");
        uqc.createUQC("BILLION OF UNITS", "Measure", "BOU");
        uqc.createUQC("BOX", "Measure", "BOX");
        uqc.createUQC("BOTTLES", "Measure", "BTL");
        uqc.createUQC("BUNCHES", "Measure", "BUN");
        uqc.createUQC("CANS", "Measure", "CAN");
        uqc.createUQC("CUBIC METERS", "Volume", "CBM");
        uqc.createUQC("CUBIC CENTIMETERS", "Volume", "CCM");
        uqc.createUQC("CENTIMETERS", "Length", "CMS");
        uqc.createUQC("CARTONS", "Measure", "CTN");
        uqc.createUQC("DOZENS", "Measure", "DOZ");
        uqc.createUQC("DRUMS", "Measure", "DRM");
        uqc.createUQC("GREAT GROSS", "Measure", "GGK");
        uqc.createUQC("GRAMMES", "Weight", "GMS");
        uqc.createUQC("GROSS", "Measure", "GRS");
        uqc.createUQC("GROSS YARDS", "Length", "GYD");
        uqc.createUQC("KILOGRAMS", "Weight", "KGS");
        uqc.createUQC("KILOLITRE", "Measure", "KLR");
        uqc.createUQC("KILOMETRE", "Length", "KME");
        uqc.createUQC("MILILITRE", "Volume", "MLT");
        uqc.createUQC("METERS", "Length", "MTR");
        uqc.createUQC("NUMBERS", "Measure", "NOS");
        uqc.createUQC("PACKS", "Measure", "PAC");
        uqc.createUQC("PIECES", "Measure", "PCS");
        uqc.createUQC("PAIRS", "Measure", "PRS");
        uqc.createUQC("QUINTAL", "Measure", "QTL");
        uqc.createUQC("ROLLS", "Measure", "ROL");
        uqc.createUQC("SETS", "Measure", "SET");
        uqc.createUQC("SQUARE FEET", "Area", "SQF");
        uqc.createUQC("SQUARE METERS", "Area", "SQM");
        uqc.createUQC("SQUARE YARDS", "Area", "SQY");
        uqc.createUQC("TABLETS", "Measure", "TBS");
        uqc.createUQC("TEN GROSS", "Measure", "TGM");
        uqc.createUQC("THOUSANDS", "Measure", "THD");
        uqc.createUQC("TONNES", "Measure", "TON");
        uqc.createUQC("TUBES", "Measure", "TUB");
        uqc.createUQC("US GALLONS", "Measure", "UGS");
        uqc.createUQC("UNITS", "Measure", "UNT");
        uqc.createUQC("YARDS", "Measure", "YDS");
        uqc.createUQC("OTHERS", "", "OTH");

        System.out.println("END");

    }
}
