/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.ui.*;
import india.abs.gstonepro.ui.billofsupply.BillOfSupplyProductDateReportUI;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.BOSLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BOSLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author CEO
 */
public class BOSProductDateReportUI extends javax.swing.JFrame {

    /**
     * Creates new form SalesProductReportUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    DecimalFormat currency = new DecimalFormat("0.000");
    int fTotalUQC1 = 0;
    int fTotalUQC2 = 0;
    String strUQC2 = "";
    String strUQC1 = "";
    List<Product> products = new ProductLogic().fetchAllProducts(company.getCompanyId());
    String quantity;
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();

    public BOSProductDateReportUI() {
//        try {
        initComponents();
        //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        setMenuRoles();
        comboProduct.requestFocus();
        setExtendedState(this.MAXIMIZED_BOTH);

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        setProduct();
        setRoles();
        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        labelCompanyName.setText(company.getCompanyName());
        labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
        dateFrom.setDate(new Date());
        dateTo.setDate(new Date());

        ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                char vChar = evt.getKeyChar();
                boolean dot = false;

                if (dot == false) {
                    if (vChar == '.') {
                        dot = true;
                    } else if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();
                    }
                } else {
                    if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();

                    }
                }
                if (Character.isDigit(vChar)) {
                    String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                    int len = strDate.length();
                    if (len == 1) {
                        ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                        evt.consume();
                    } else if (len == 4) {
                        ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                        evt.consume();
                    }
                }
            }
        });
        ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                        sft.setLenient(false);
                        String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                        if (!(strDate.trim().equals(""))) {
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                dateTo.requestFocusInWindow();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } else {
                            JOptionPane.showMessageDialog(null, "Please Select the Date");
                        }
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                        Logger.getLogger(BOSProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                char vChar = evt.getKeyChar();
                boolean dot = false;

                if (dot == false) {
                    if (vChar == '.') {
                        dot = true;
                    } else if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();
                    }
                } else {
                    if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();

                    }
                }
                if (Character.isDigit(vChar)) {
                    String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                    int len = strDate.length();
                    if (len == 1) {
                        ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                        evt.consume();
                    } else if (len == 4) {
                        ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                        evt.consume();
                    }
                }
            }
        });
        ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                        sft.setLenient(false);
                        String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                        if (!(strDate.trim().equals(""))) {
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                btnFetch.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } else {
                            JOptionPane.showMessageDialog(null, "Please Select the Date");
                        }
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                        Logger.getLogger(BOSProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        JTextComponent editor1 = (JTextComponent) comboProduct.getEditor().getEditorComponent();
        editor1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboProduct.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();

                        for (Product product : products) {
                            if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(product.getName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem(val);
                        comboProduct.setPopupVisible(true);
                    }
                }
            }

            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                    ArrayList<String> scripts = new ArrayList<>();
                    for (Product product : products) {
                        scripts.add(product.getName());
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboProduct.setModel(model);
                    comboProduct.getEditor().setItem("");
                    comboProduct.setPopupVisible(true);
                } else {
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboProduct.setPopupVisible(false);
                       
                        comboProduct.getEditor().setItem(selectedObj);
                        dateFrom.requestFocusInWindow();
                    }
                }
            }
        });
//        } catch (IOException ex) {
//            Logger.getLogger(BOSProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isCrdrNoteView()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        comboProduct = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        dateTo = new com.toedter.calendar.JDateChooser();
        btnFetch = new javax.swing.JButton();
        btnPrintReport = new javax.swing.JButton();
        btnDownloadPdf = new javax.swing.JButton();
        btnDownloadExcel = new javax.swing.JButton();
        labelTotal = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bill of Supply - Product Date Report");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Quantity", "ProductAmount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("Product");

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N

        jLabel2.setText("From");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        jLabel3.setText("To");

        dateTo.setDateFormatString("dd-MM-yyyy");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });
        btnFetch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnFetchKeyPressed(evt);
            }
        });

        btnPrintReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnPrintReport.setText("Print Report");
        btnPrintReport.setPreferredSize(new java.awt.Dimension(160, 23));
        btnPrintReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintReportActionPerformed(evt);
            }
        });
        btnPrintReport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPrintReportKeyPressed(evt);
            }
        });

        btnDownloadPdf.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDownloadPdf.setText("Download Report (PDF)");
        btnDownloadPdf.setPreferredSize(new java.awt.Dimension(160, 23));
        btnDownloadPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadPdfActionPerformed(evt);
            }
        });
        btnDownloadPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDownloadPdfKeyPressed(evt);
            }
        });

        btnDownloadExcel.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDownloadExcel.setText("Download Report (Excel)");
        btnDownloadExcel.setPreferredSize(new java.awt.Dimension(160, 23));
        btnDownloadExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadExcelActionPerformed(evt);
            }
        });

        labelTotal.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTotal.setText("Total Bill Amount : 0.00 , Total Product Amount :0.00");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-------");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel7.setText("BILL OF SUPPLY - PRODUCT DATE REPORT");

        jLabel12.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout1.setMnemonic('l');
        btnLogout1.setText("Logout");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(labelCompanyName)
                        .addComponent(jLabel7))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel12)
                        .addComponent(jButton1))))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(380, 380, 380)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 576, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDownloadPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDownloadExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDownloadExcel, btnDownloadPdf, btnPrintReport});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1))
                    .addComponent(btnFetch, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelTotal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDownloadPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnDownloadExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(49, 49, 49))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDownloadExcel, btnDownloadPdf, btnPrintReport});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        try {
            fetchProducts();
        } catch (ParseException ex) {
            ex.printStackTrace();
            Logger.getLogger(BOSProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnFetchActionPerformed

    private void btnPrintReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(true);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintReportActionPerformed

    private void btnDownloadPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadPdfActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(false);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDownloadPdfActionPerformed

    private void btnDownloadExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadExcelActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            try {
                generateExcelReport();
            } catch (WriteException ex) {
                Logger.getLogger(PurchaseReportUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDownloadExcelActionPerformed

    public void generateExcelReport() throws WriteException {
        try {
            java.util.Date from = dateFrom.getDate();
            java.util.Date to = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(from);
            String formatdateTo = sdf.format(to);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            String product = comboProduct.getSelectedItem().toString();
            String fileName = product + " Bill of Supply Product Date Report " + sdf.format(from) + " to " + sdf.format(to);
            JFrame parentFrame = new JFrame();

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Bill of Supply Product Date Excel Report");
            fileChooser.setSelectedFile(new File(fileName));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                String excelpath = fileToSave.getAbsolutePath() + ".xls";
                File f = new File(excelpath);
                WritableWorkbook myexcel;
                myexcel = Workbook.createWorkbook(f);
                WritableSheet mysheet = myexcel.createSheet("mysheet", 0);
                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);
                WritableCellFormat rupeeFormat = new WritableCellFormat();
                rupeeFormat.setAlignment(Alignment.RIGHT);
                rupeeFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat rightFormat = new WritableCellFormat(wf2);
                rightFormat.setAlignment(Alignment.RIGHT);
                rightFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat centerFormat = new WritableCellFormat(wf2);
                centerFormat.setAlignment(Alignment.CENTRE);
                centerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableFont wf4 = new WritableFont(WritableFont.TIMES);
                mysheet.setColumnView(0, 20);
                mysheet.setColumnView(1, 25);
                mysheet.setColumnView(2, 25);
                mysheet.setColumnView(3, 30);
                mysheet.setColumnView(4, 30);
                //mysheet.setAutobreaks(false);
                WritableCellFormat cellFormat = new WritableCellFormat(wf2);
                cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat cellFormat1 = new WritableCellFormat(wf3);
                cellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
                mysheet.setName("Bill of Supply Product Date Report");

                mysheet.addCell(new Label(0, 0, "GSTIN/UIN: " + company.getGSTIN(), cellFormat));
                mysheet.mergeCells(0, 0, 1, 0);
                mysheet.addCell(new Label(2, 0, product + " Bill of Supply Product Date Reports " + formatdateFrom + " to " + formatdateTo + "", rightFormat));
                mysheet.mergeCells(2, 0, 3, 0);
                mysheet.addCell(new Label(0, 1, company.getCompanyName(), centerFormat));
                mysheet.mergeCells(0, 1, 2, 1);
                if (!company.getAddress().equals("")) {
                    mysheet.addCell(new Label(0, 2, company.getAddress().replaceAll("/~/", ""), centerFormat));
                    mysheet.mergeCells(0, 2, 2, 2);
                }
                mysheet.addCell(new Label(0, 3, "City : " + company.getCity(), centerFormat));
                mysheet.mergeCells(0, 3, 2, 3);
                mysheet.addCell(new Label(0, 4, "State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), centerFormat));
                mysheet.mergeCells(0, 4, 2, 4);
                mysheet.addCell(new Label(0, 5, "Phone : " + company.getPhone(), centerFormat));
                mysheet.mergeCells(0, 5, 2, 5);
                mysheet.addCell(new Label(0, 6, "Mobile : " + company.getMobile(), centerFormat));
                mysheet.mergeCells(0, 6, 2, 6);
                mysheet.addCell(new Label(0, 7, "Email : " + company.getEmail(), centerFormat));
                mysheet.mergeCells(0, 7, 2, 7);
                mysheet.addCell(new Label(0, 8, "Date", cellFormat));
                mysheet.addCell(new Label(1, 8, "Quantity", cellFormat));
                mysheet.addCell(new Label(2, 8, "product Amount", cellFormat));

                float totalAmt = 0;
                int totalPostion = 9 + model.getRowCount();

                for (int i = 0; i < model.getRowCount(); i++) {
                    totalAmt = totalAmt + Float.parseFloat(model.getValueAt(i, 2).toString());
                    int rowValue = 9 + i;
                    mysheet.addCell(new Label(0, rowValue, jTable1.getValueAt(i, 0).toString(), cellFormat1));
                    mysheet.addCell(new Label(1, rowValue, jTable1.getValueAt(i, 1).toString(), cellFormat1));
                    String strAmount = "";
                    strAmount = currency.format(Float.parseFloat(model.getValueAt(i, 2).toString()));
                    mysheet.addCell(new Label(2, rowValue, strAmount, rupeeFormat));
                }

                mysheet.addCell(new Label(0, totalPostion, "Total", centerFormat));
                mysheet.addCell(new Label(2, totalPostion, currency.format(totalAmt), rightFormat));
                mysheet.addCell(new Label(1, totalPostion, quantity, cellFormat));

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String d = dtf.format(now);
                String date1 = "Report generated as on " + d;
                int datePosition = totalPostion + 2;
                mysheet.addCell(new Label(0, datePosition, date1, centerFormat));
                mysheet.mergeCells(0, datePosition, 2, datePosition + 1);
                myexcel.write();
                myexcel.close();

                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(excelpath);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, "Document not to be Opened");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Excel File Download At " + excelpath);
                }
            }

            com.alee.laf.WebLookAndFeel.install();
        } catch (WriteException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void btnFetchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnFetchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                fetchProducts();
            } catch (ParseException ex) {
                Logger.getLogger(BOSProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnFetchKeyPressed

    private void btnDownloadPdfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDownloadPdfKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            pdfReportGeneration(false);
        }
    }//GEN-LAST:event_btnDownloadPdfKeyPressed

    private void btnPrintReportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPrintReportKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            pdfReportGeneration(true);
        }
    }//GEN-LAST:event_btnPrintReportKeyPressed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
          if (isInventoryEnabled && isAccountEnabled) {
                        new DashBoardUI().setVisible(true);
                        dispose();
                    } else if (isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardSilverUI().setVisible(true);
                        dispose();
                    } else if (!isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardCopperUI().setVisible(true);
                        dispose();
                    }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void fetchProducts() throws ParseException {

        String strQuantity = "";
        int nUQC1 = 0;
        int nUQC2 = 0;
        int nTotalUQC1 = 0;
        int nTotalUQC2 = 0;
        float fTotalAmount = 0.000f;
        float fProductAmount = 0.000f;
        fTotalUQC1 = 0;
        fTotalUQC2 = 0;
        Boolean isFound = false;
        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");

        try {
            strUQC2 = "";
            strUQC1 = "";
            String product = (String) comboProduct.getSelectedItem();
            Product selProdut = new Product();
            for (Product list : products) {
                if (list.getName().equals(product)) {
                    selProdut = list;
                    isFound = true;
                    break;
                }
            }
            Date fromDate = dateFrom.getDate();
            fromDate.setHours(0);
            fromDate.setMinutes(0);
            fromDate.setSeconds(0);
            if (isFound) {
                List<BOSLineItem> BOSLineItems = new BOSLogic().readBOSByProduct(fromDate, dateTo.getDate(), selProdut);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                if (model.getRowCount() > 0) {
                    model.setRowCount(0);
                }
                Date startDate = sft.parse(sft.format(dateFrom.getDate()));
                Date endDate = sft.parse(sft.format(dateTo.getDate()));
                while (endDate.compareTo(startDate) >= 0) {
                    nUQC1 = 0;
                    nUQC2 = 0;
                    fProductAmount = 0.000f;
                    for (BOSLineItem lineItem : BOSLineItems) {
                        strUQC1 = lineItem.getUqcOne();
                        strUQC2 = lineItem.getUqcTwo();
                        if (startDate.compareTo(lineItem.getBOS().getBOSDate()) == 0) {
                            nUQC1 = nUQC1 + lineItem.getUqcOneQuantity();
                            nUQC2 = nUQC2 + lineItem.getUqcTwoQuantity();
                            fProductAmount = fProductAmount + lineItem.getValue().floatValue();
                            nTotalUQC1 = nTotalUQC1 + lineItem.getUqcOneQuantity();

                            nTotalUQC2 = nTotalUQC2 + lineItem.getUqcTwoQuantity();
                            fTotalAmount = fTotalAmount + lineItem.getValue().floatValue();
                        }
                    }
                    fTotalUQC1 = fTotalUQC1 + nTotalUQC1;
                    fTotalUQC2 = fTotalUQC2 + nTotalUQC2;
                    String strDate = sft.format(startDate);
                    strQuantity = Integer.toString(nUQC1) + " " + strUQC1 + ", " + Integer.toString(nUQC2) + " " + strUQC2;
                    if (nUQC1 != 0 || nUQC2 != 0) {
                        Object[] row = {strDate, strQuantity, fProductAmount};
                        model.addRow(row);
                    }
                    startDate = new Date(startDate.getTime() + TimeUnit.DAYS.toMillis(1));
                }
                labelTotal.setText("Total Quantity: " + nTotalUQC1 + " " + strUQC1 + ", " + nTotalUQC2 + " " + strUQC2 + ",   Total Amount: " + fTotalAmount);
                quantity = nTotalUQC1 + " " + strUQC1 + ", " + nTotalUQC2 + " " + strUQC2;

            } else {
                JOptionPane.showMessageDialog(null, "Please Select Product in the list", "Alert", JOptionPane.WARNING_MESSAGE);
            }
            jTable1.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());

        } catch (Exception ex) {
            Logger.getLogger(BillOfSupplyProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfReportGeneration(Boolean isPrint) {
        try {
            Date datefrom = dateFrom.getDate();
            Date dateto = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(datefrom);
            String formatdateTo = sdf.format(dateto);
            Document document = new Document(PageSize.A4);
            String path = "";
            Boolean isApproved = false;
            String selectProduct = (String) comboProduct.getSelectedItem();
            if (isPrint) {

                path = AppConstants.getTempDocPath() + selectProduct + formatdateFrom + " to " + formatdateTo + ".pdf";
                isApproved = true;
            } else {
//                path = AppConstants.getDocPath() +"Sales Report " + formatdateFrom + " to " + formatdateTo  + ".pdf";

                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill of Supply Report");
                fileChooser.setSelectedFile(new File("Product " + selectProduct + formatdateFrom + " to " + formatdateTo));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                Phrase p;
                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(2);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell;

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN: " + company.getGSTIN(), reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                String reportdate = selectProduct + " Reports " + '\n' + formatdateFrom + " to " + formatdateTo + "";
                taxcell = new PdfPCell(new Phrase(reportdate, reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);
                document.add(taxTable);

                PdfPTable companyTable = new PdfPTable(1);
                companyTable.setWidthPercentage(100);
                PdfPCell cell = null;

                cell = new PdfPCell(new Phrase(company.getCompanyName(), boldFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                if (!company.getAddress().equals("")) {
                    cell = new PdfPCell(new Phrase(company.getAddress().replaceAll("/~/", ","), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                cell = new PdfPCell(new Phrase("City : " + company.getCity(), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                cell = new PdfPCell(new Phrase("State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);
                if (!company.getPhone().equals("")) {
                    cell = new PdfPCell(new Phrase("Phone : " + company.getPhone(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getMobile().equals("")) {
                    cell = new PdfPCell(new Phrase("Mobile : " + company.getMobile(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getEmail().equals("")) {
                    cell = new PdfPCell(new Phrase("Email : " + company.getEmail(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }

                document.add(companyTable);

                PdfPTable thirdTable = new PdfPTable(3);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{25, 30, 25});

                taxcell = new PdfPCell(new Phrase("Date", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Quantity", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Amount", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                float totalAmount = 0;
                String totalProduct = "";
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                for (int rows = 0; rows < jTable1.getRowCount(); rows++) {
                    totalAmount = totalAmount + Float.parseFloat(model.getValueAt(rows, 2).toString());
                    totalProduct = fTotalUQC1 + " " + strUQC1 + ", " + fTotalUQC2 + " " + strUQC2;
                    for (int cols = 1; cols <= jTable1.getColumnCount(); cols++) {
                        String tablevalue = "";
                        if (cols == 1) {
                            String billDate = model.getValueAt(rows, cols - 1).toString();
                            String[] s2 = billDate.split("-");
                            tablevalue = s2[2] + "-" + s2[1] + "-" + s2[0];
                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                            thirdTable.addCell(taxcell);
                        } else if (cols == 3) {
                            tablevalue = currency.format(model.getValueAt(rows, cols - 1)).toString();
                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                            thirdTable.addCell(taxcell);
                        } else {
                            tablevalue = model.getValueAt(rows, cols - 1).toString();
                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                            thirdTable.addCell(taxcell);
                        }
                    }
                }
                taxcell = new PdfPCell(new Phrase("Total ", reportdateFont));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                taxcell.setColspan(3);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase((quantity), totalFont));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(totalAmount), totalFont));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

//                taxcell = new PdfPCell(new Phrase(" ", totalFont));
//                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                thirdTable.addCell(taxcell);
//                taxcell = new PdfPCell(new Phrase(currency.format(totalProfit), totalFont));
//                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                thirdTable.addCell(taxcell);
                document.add(thirdTable);
                document.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);
//                        JOptionPane.showMessageDialog(null, "Product Report is Printing....... " + path);
                    } catch (PrintException ex) {
                        Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }

                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        com.alee.laf.WebLookAndFeel.install();
    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BOSProductDateReportUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BOSProductDateReportUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BOSProductDateReportUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BOSProductDateReportUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        com.alee.laf.WebLookAndFeel.install();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BOSProductDateReportUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDownloadExcel;
    private javax.swing.JButton btnDownloadPdf;
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout1;
    private javax.swing.JButton btnPrintReport;
    private javax.swing.JComboBox<String> comboProduct;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    // End of variables declaration//GEN-END:variables
}
