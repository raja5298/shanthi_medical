/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.sun.java.swing.plaf.windows.WindowsButtonUI;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.utils.models.StockReportModel;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.unusedui.PaymentGrpUI;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Admin
 */
public class DashBoardUI extends javax.swing.JFrame {

    /**
     * Creates new form DashBoardUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
    Map<String, StockReportModel> gseEntry = new HashMap<>();

    SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");

    public DashBoardUI() {
//        try {

        initComponents();
        fetchProductDetails();
        lableDaysToExpire.setText("");
        lableExpiryAlert.setVisible(false);
        lableDaysToExpire.setVisible(false);
        alertExpiryMessage();
        //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        setMenuRoles();
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        menuSettings.setVisible(SessionDataUtil.getSelectedUser().isAdmin());
        setRoles();
        this.setExtendedState(this.MAXIMIZED_BOTH);
        labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
        labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
        setButtonRoles();
//        menuTransactionSummary.setVisible(false);
//        menuProductTransaction.setVisible(false);
        //  setCompanyDetails();
        setButtonUI();
        setBtnB2BMnemonics();
        setBtnB2CMnemonics();
        setBtnEstimateMnemonics();
//        setBtnBillReportsMnemonics();
        setBtnPurchaseMnemonics();
//        setBtnDebitMnemonics();
//        setBtnCreditMnemonics();
//        setBtnInvReportMnemonics();
        setBtnPaymentMnemonics();
        setBtnCashEntryMnemonics();
        setBtnBankEntryMnemonics();
//        setBtnAccountsReportsMnemonics();
        btnB2B.requestFocus();
//        } catch (IOException ex) {
//            Logger.getLogger(DashBoardUI.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    private void fetchProductDetails() {
        DefaultTableModel modelExp = (DefaultTableModel) tblProductExp.getModel();
        if (modelExp.getRowCount() > 0) {
            modelExp.setRowCount(0);
        }
        Calendar cal = Calendar.getInstance();
        gseEntry = new GoDownLogic().fetchProductsSoldOut();
        int count = 1;
        if (gseEntry.size() > 0) {
            for (Map.Entry<String, StockReportModel> entry : gseEntry.entrySet()) {
                StockReportModel srm = entry.getValue();
                Product product = srm.getProduct();
                long stockOut = 0;
                if (srm.getStockOut() != null) {
                    stockOut = srm.getStockOut();
                }
                long quantity = 0;
                if (srm.getStockIn() != null) {
                    quantity = srm.getStockIn() + stockOut;
                }
                String godown = srm.getGodown().getGoDownName();
                String productName = product.getName();
                Date soldDate = null;
                String strSoldDate = "";

                if (srm.getReportDate() != null) {
                    cal.setTime(srm.getReportDate());
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    month = month + 1;
                    int day = cal.get(Calendar.DATE);
                    LocalDate dateSold = LocalDate.of(year, month, day);
                    LocalDate today = LocalDate.now();
                    Period p = Period.between(dateSold, today);
                    String strYear = "";
                    String strMonth = "";
                    String strDays = "";
                    if (p.getYears() > 0) {
                        if ((p.getYears() == 1)) {
                            strYear = p.getYears() + " Year ";
                        } else {
                            strYear = p.getYears() + " Years ";
                        }
                        if ((p.getMonths() == 0) && (p.getDays() == 0)) {
                            strYear = strYear + " ago";
                        }
                    }
                    if (p.getMonths() > 0) {
                        if (p.getMonths() == 1) {
                            strMonth = p.getMonths() + " Month ";
                        } else {
                            strMonth = p.getMonths() + " Months ";
                        }
                        if (p.getDays() == 0) {
                            strMonth = strMonth + " ago";
                        }
                    }
                    if (p.getDays() > 0) {
                        if (p.getDays() == 1) {
                            strDays = p.getDays() + " Day ago";
                        } else {
                            strDays = p.getDays() + " Days ago";
                        }
                    }
                    if ((p.getYears() == 0) && (p.getMonths() == 0) && (p.getDays() == 0)) {
                        strSoldDate = "Today";
                    } else {
                        strSoldDate = strYear + strMonth + strDays;
                    }
                } else {
                    strSoldDate = "Not Sold";
                }
                String Stockquantity = setStockConverstion(quantity, product.getUQC1(), product.getUQC2(), product.getUQC2Value());
                Object[] row = {
                    count++,
                    productName,
                    Stockquantity,
                    godown,
                    strSoldDate};
                modelExp.addRow(row);
            }
        }
    }

    private String setStockConverstion(Long qty, String UQC1, String UQC2, int UQC2Value) {
        Long UQC1Count = qty / UQC2Value;
        Long UQC2Count = qty % UQC2Value;
        String result = "";
        if (UQC1.equals(UQC2)) {
            result = UQC1Count + " " + UQC1;
        } else {
            result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        }
        return result;
    }

// function to check date difference
    public void alertExpiryMessage() {
        Boolean isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();
        CompanyPolicy companyPolicy = company.getCompanyPolicy();
        if (isTrialPackage) {
            float daysBetween = 0;
            SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date dateAfter = companyPolicy.getAccountsExpiryDate();
            Date dateBefore = new Date();
            long difference = dateAfter.getTime() - dateBefore.getTime();
            daysBetween = (difference / (1000 * 60 * 60 * 24));
            if (daysBetween <= 7) {
                lableExpiryAlert.setVisible(true);
                lableDaysToExpire.setText(daysBetween + " Days");
                lableDaysToExpire.setVisible(true);
            }
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
//            menuBar.remove(menuGodown);
        }
    }
//    public void setCompanyDetails() {
//        labelName.setText(company.getCompanyName());
//        String address = company.getAddress();
//        String[] words = address.split("/~/");
//        labelAddr1.setText(words[0]);
//        labelType.setText(company.getCompanyType());
//        String Addr2 = words[1].replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String Addr3 = words[2].replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        if (Addr2.equals("")) {
//            labelAddr2.setVisible(false);
//        } else {
//            labelAddr2.setText(Addr2);
//        }
//        if (Addr3.equals("")) {
//            labelAddr3.setVisible(false);
//        } else {
//            labelAddr3.setText(Addr3);
//        }
//        labelCity.setText(company.getCity());
//        labelDistrict.setText(company.getDistrict());
//        labelState.setText(company.getState());
//        labelPhone.setText(company.getPhone());
//        labelMobile.setText(company.getMobile());
//        labelEmail.setText(company.getEmail());
//        labelGSTIN.setText(company.getGSTIN());
//    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuGSTR2.setVisible(true);
        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

        //btn
        btnB2B.setEnabled(companyUserRole.isSalesCreate());
        btnB2C.setEnabled(companyUserRole.isSalesCreate());
        btnEstimate.setEnabled(companyUserRole.isEstimateCreate());
//        btnBillReport.setEnabled(companyUserRole.isSalesView() || companyUserRole.isEstimateView());
        btnPurchase.setEnabled(companyUserRole.isPurchaseCreate());
//        btnDebit.setEnabled(companyUserRole.isCrdrNoteCreate());
//        btnCredit.setEnabled(companyUserRole.isCrdrNoteCreate());
//        btnInvReport.setEnabled(companyUserRole.isInventoryView());
//        btnAccReports.setEnabled(companyUserRole.isLedgerView() || companyUserRole.isAccountsDayBook() || companyUserRole.isAccountsCash()
//                || companyUserRole.isAccountsBank() || companyUserRole.isAccountsLedgerGroupBalance() || companyUserRole.isAccountsTrailBalance() || companyUserRole.isAccountsBalanceSheet());
        btnBankEntry.setEnabled(companyUserRole.isAccountsBank());
        btnCashEntry.setEnabled(companyUserRole.isAccountsCash());
        btnJournalEntry.setEnabled(companyUserRole.isAccountsJournal());
    }

    public void setButtonRoles() {
        if (isInventoryEnabled && isAccountEnabled) {
//            jPanel4.add(jPanel5);
//            jPanel4.add(jPanel3);
//            jPanel4.add(jPanel7);
//            jPanel4.add(jPanel8);
        } else if (isInventoryEnabled) {
//            jPanel4.remove(jPanel5);
//            jPanel4.remove(jPanel7);
        } else if (isAccountEnabled) {
//            jPanel4.remove(jPanel3);
//            jPanel4.remove(jPanel7);
        } else if (!isInventoryEnabled && !isAccountEnabled) {
//            jPanel4.remove(jPanel5);
//            jPanel4.remove(jPanel3);
//            jPanel4.remove(jPanel7);
//            jPanel4.remove(jPanel8);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jLabel13 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnB2B = new JButton("B2B");
        btnB2C = new javax.swing.JButton();
        btnPurchase = new javax.swing.JButton();
        btnEstimate = new javax.swing.JButton();
        btnJournalEntry = new javax.swing.JButton();
        btnBankEntry = new javax.swing.JButton();
        btnCashEntry = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProductExp = new javax.swing.JTable();
        lableExpiryAlert = new javax.swing.JLabel();
        lableDaysToExpire = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        jMenuItem5.setText("jMenuItem5");

        jMenuItem1.setText("jMenuItem1");

        jMenu3.setText("jMenu3");

        jMenuItem4.setText("jMenuItem4");

        jMenuItem8.setText("jMenuItem8");

        jMenuItem9.setText("jMenuItem9");

        jMenuItem10.setText("jMenuItem10");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Welcome");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-------");

        jLabel11.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jPanel4.setPreferredSize(new java.awt.Dimension(1000, 693));

        jPanel1.setPreferredSize(new java.awt.Dimension(0, 120));
        jPanel1.setLayout(new java.awt.GridLayout(1, 0, 20, 0));

        btnB2B.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnB2B.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tax_invoice_b2b.png"))); // NOI18N
        btnB2B.setText("TAX INVOICE B2B  (F1)");
        btnB2B.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnB2B.setIconTextGap(3);
        btnB2B.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB2BActionPerformed(evt);
            }
        });
        btnB2B.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnB2BKeyPressed(evt);
            }
        });
        jPanel1.add(btnB2B);

        btnB2C.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnB2C.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tax_invoice_b2c.png"))); // NOI18N
        btnB2C.setText("TAX INVOICE B2C  (F2)");
        btnB2C.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnB2C.setIconTextGap(3);
        btnB2C.setMargin(new java.awt.Insets(2, 14, 3, 14));
        btnB2C.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB2CActionPerformed(evt);
            }
        });
        btnB2C.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnB2CKeyPressed(evt);
            }
        });
        jPanel1.add(btnB2C);

        btnPurchase.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnPurchase.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/purchase.png"))); // NOI18N
        btnPurchase.setText("PURCHASE  (F5)");
        btnPurchase.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPurchase.setIconTextGap(3);
        btnPurchase.setMaximumSize(new java.awt.Dimension(146, 113));
        btnPurchase.setMinimumSize(new java.awt.Dimension(146, 113));
        btnPurchase.setPreferredSize(new java.awt.Dimension(146, 113));
        btnPurchase.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPurchaseActionPerformed(evt);
            }
        });
        btnPurchase.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPurchaseKeyPressed(evt);
            }
        });
        jPanel1.add(btnPurchase);

        btnEstimate.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnEstimate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/estimate.png"))); // NOI18N
        btnEstimate.setText("ESTIMATE  (F3)");
        btnEstimate.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEstimate.setIconTextGap(3);
        btnEstimate.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstimateActionPerformed(evt);
            }
        });
        btnEstimate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEstimateKeyPressed(evt);
            }
        });
        jPanel1.add(btnEstimate);

        btnJournalEntry.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnJournalEntry.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/journal_entry.png"))); // NOI18N
        btnJournalEntry.setText("JOURNAL ENTRY  (F9)");
        btnJournalEntry.setToolTipText("");
        btnJournalEntry.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnJournalEntry.setIconTextGap(3);
        btnJournalEntry.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJournalEntryActionPerformed(evt);
            }
        });
        btnJournalEntry.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnJournalEntryKeyPressed(evt);
            }
        });
        jPanel1.add(btnJournalEntry);

        btnBankEntry.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnBankEntry.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bank_entry.png"))); // NOI18N
        btnBankEntry.setText("BANK ENTRY  (F11)");
        btnBankEntry.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBankEntry.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBankEntryActionPerformed(evt);
            }
        });
        btnBankEntry.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnBankEntryKeyPressed(evt);
            }
        });
        jPanel1.add(btnBankEntry);

        btnCashEntry.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        btnCashEntry.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cash_entry.png"))); // NOI18N
        btnCashEntry.setText("CASH ENTRY  (F10)");
        btnCashEntry.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCashEntry.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCashEntryActionPerformed(evt);
            }
        });
        btnCashEntry.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCashEntryKeyPressed(evt);
            }
        });
        jPanel1.add(btnCashEntry);

        tblProductExp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product", "Stock Quantity", "Godown", "Last Sold"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Long.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblProductExp);
        if (tblProductExp.getColumnModel().getColumnCount() > 0) {
            tblProductExp.getColumnModel().getColumn(0).setMinWidth(40);
            tblProductExp.getColumnModel().getColumn(0).setPreferredWidth(40);
            tblProductExp.getColumnModel().getColumn(0).setMaxWidth(40);
            tblProductExp.getColumnModel().getColumn(1).setMinWidth(300);
            tblProductExp.getColumnModel().getColumn(1).setPreferredWidth(300);
            tblProductExp.getColumnModel().getColumn(1).setMaxWidth(300);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(174, 174, 174))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(58, Short.MAX_VALUE))
        );

        lableExpiryAlert.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lableExpiryAlert.setText("Your trial pack will be expired on :");

        lableDaysToExpire.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lableDaysToExpire.setText("jLabel2");

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        jMenuItem14.setText("Agent");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        menuFile.add(jMenuItem14);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        jMenuItem12.setText("Sales Transactions Report");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem12);

        jMenuItem13.setText("Purchase Transactions Reprot");
        menuAccountsReport.add(jMenuItem13);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelCompanyName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lableExpiryAlert)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lableDaysToExpire)
                        .addGap(208, 208, 208)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelUserName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLogout))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 1116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogout)
                    .addComponent(labelUserName)
                    .addComponent(jLabel13)
                    .addComponent(labelCompanyName)
                    .addComponent(jLabel11)
                    .addComponent(lableExpiryAlert)
                    .addComponent(lableDaysToExpire))
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed


    private void menuGodownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGodownActionPerformed
        new GodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGodownActionPerformed

    private void btnCashEntryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCashEntryKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new LedgerUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnCashEntryKeyPressed

    private void btnCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCashEntryActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnCashEntryActionPerformed

    private void btnBankEntryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnBankEntryKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new DayBookUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnBankEntryKeyPressed

    private void btnBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBankEntryActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnBankEntryActionPerformed

    private void btnJournalEntryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnJournalEntryKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new BankEntryUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnJournalEntryKeyPressed

    private void btnJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnJournalEntryActionPerformed

    private void btnEstimateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEstimateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new EstimateUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnEstimateKeyPressed

    private void btnEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnEstimateActionPerformed

    private void btnPurchaseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPurchaseKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new PurchaseUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnPurchaseKeyPressed

    private void btnPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnPurchaseActionPerformed

    private void btnB2CKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnB2CKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new SalesB2CUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnB2CKeyPressed

    private void btnB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnB2CActionPerformed

    private void btnB2BKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnB2BKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            new SalesB2BUI().setVisible(true);
            dispose();
            evt.consume();
        }
    }//GEN-LAST:event_btnB2BKeyPressed

    private void btnB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnB2BActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        new SalesTransactionReportUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        new AgentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void setButtonUI() {

        btnB2B.setUI(new WindowsButtonUI());
        btnB2C.setUI(new WindowsButtonUI());
        btnEstimate.setUI(new WindowsButtonUI());
//        btnBillReport.setUI(new WindowsButtonUI());
        btnPurchase.setUI(new WindowsButtonUI());
//        btnDebit.setUI(new WindowsButtonUI());
//        btnCredit.setUI(new WindowsButtonUI());
//        btnInvReport.setUI(new WindowsButtonUI());
        btnJournalEntry.setUI(new WindowsButtonUI());
        btnCashEntry.setUI(new WindowsButtonUI());
        btnBankEntry.setUI(new WindowsButtonUI());
//        btnAccReports.setUI(new WindowsButtonUI());
    }

    private void setBtnB2BMnemonics() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SalesB2BUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
        InputMap inputMap = btnB2B.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnB2B.getActionMap();
        actionMap.put("Action", actionListener);
        btnB2B.setActionMap(actionMap);
    }

    private void setBtnB2CMnemonics() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new SalesB2CUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
        InputMap inputMap = btnB2C.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnB2C.getActionMap();
        actionMap.put("Action", actionListener);
        btnB2C.setActionMap(actionMap);
    }

    private void setBtnEstimateMnemonics() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new EstimateUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0);
        InputMap inputMap = btnEstimate.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnEstimate.getActionMap();
        actionMap.put("Action", actionListener);
        btnEstimate.setActionMap(actionMap);
    }

//    private void setBtnBillReportsMnemonics() {
//        Action actionListener = new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent actionEvent) {
//                new SalesReportUI().setVisible(true);
//                dispose();
//            }
//        };
//        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0);
////        InputMap inputMap = btnBillReport.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        inputMap.put(ks, "Action");
////        ActionMap actionMap = btnBillReport.getActionMap();
//        actionMap.put("Action", actionListener);
////        btnBillReport.setActionMap(actionMap);
//    }
    private void setBtnPurchaseMnemonics() {
        Action actionListener = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                new PurchaseUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
        InputMap inputMap = btnPurchase.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnPurchase.getActionMap();
        actionMap.put("Action", actionListener);
        btnPurchase.setActionMap(actionMap);
    }

//    private void setBtnDebitMnemonics() {
//        Action actionListener = new AbstractAction() {
//            public void actionPerformed(ActionEvent actionEvent) {
//                new DebitNoteUI().setVisible(true);
//                dispose();
//
//            }
//        };
//        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0);
//        InputMap inputMap = btnDebit.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        inputMap.put(ks, "Action");
//        ActionMap actionMap = btnDebit.getActionMap();
//        actionMap.put("Action", actionListener);
//        btnDebit.setActionMap(actionMap);
//    }
//
//    private void setBtnCreditMnemonics() {
//        Action actionListener = new AbstractAction() {
//            public void actionPerformed(ActionEvent actionEvent) {
//                new CreditNoteUI().setVisible(true);
//                dispose();
//            }
//        };
//        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0);
//        InputMap inputMap = btnCredit.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        inputMap.put(ks, "Action");
//        ActionMap actionMap = btnCredit.getActionMap();
//        actionMap.put("Action", actionListener);
//        btnCredit.setActionMap(actionMap);
//    }
//
//    private void setBtnInvReportMnemonics() {
//        Action actionListener = new AbstractAction() {
//            public void actionPerformed(ActionEvent actionEvent) {
//                new InventoryReportGrpUI().setVisible(true);
//                dispose();
//            }
//        };
//        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0);
//        InputMap inputMap = btnInvReport.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        inputMap.put(ks, "Action");
//        ActionMap actionMap = btnInvReport.getActionMap();
//        actionMap.put("Action", actionListener);
//        btnInvReport.setActionMap(actionMap);
//    }
    private void setBtnPaymentMnemonics() {
        Action actionListener = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                new JournalEntryUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0);
        InputMap inputMap = btnJournalEntry.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnJournalEntry.getActionMap();
        actionMap.put("Action", actionListener);
        btnJournalEntry.setActionMap(actionMap);
    }

    private void setBtnCashEntryMnemonics() {
        Action actionListener = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                new CashEntryUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0);
        InputMap inputMap = btnCashEntry.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnCashEntry.getActionMap();
        actionMap.put("Action", actionListener);
        btnCashEntry.setActionMap(actionMap);
    }

    private void setBtnBankEntryMnemonics() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new BankEntryUI().setVisible(true);
                dispose();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0);
        InputMap inputMap = btnBankEntry.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnBankEntry.getActionMap();
        actionMap.put("Action", actionListener);
        btnBankEntry.setActionMap(actionMap);
    }

//    private void setBtnAccountsReportsMnemonics() {
//        Action actionListener = new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent actionEvent) {
//                new AccountReportsGroupUI().setVisible(true);
//                dispose();
//            }
//        };
//        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0);
//        InputMap inputMap = btnAccReports.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
//        inputMap.put(ks, "Action");
//        ActionMap actionMap = btnAccReports.getActionMap();
//        actionMap.put("Action", actionListener);
//        btnAccReports.setActionMap(actionMap);
//    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DashBoardUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DashBoardUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DashBoardUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DashBoardUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        com.alee.laf.WebLookAndFeel.install();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DashBoardUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnB2B;
    private javax.swing.JButton btnB2C;
    private javax.swing.JButton btnBankEntry;
    private javax.swing.JButton btnCashEntry;
    private javax.swing.JButton btnEstimate;
    private javax.swing.JButton btnJournalEntry;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPurchase;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lableDaysToExpire;
    private javax.swing.JLabel lableExpiryAlert;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTable tblProductExp;
    // End of variables declaration//GEN-END:variables
}
