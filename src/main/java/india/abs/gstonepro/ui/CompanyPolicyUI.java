/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CompanyPolicyLogic;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Raja Abinesh
 */
public class CompanyPolicyUI extends javax.swing.JFrame {

    /**
     * Creates new form CompanyPolicyUI
     */
    Company company;
    CompanyPolicy companyPolicy;
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();

    public CompanyPolicyUI() {
        initComponents();
        btnRemoveImage.setEnabled(false);
        btnUploadImage.setEnabled(false);
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }
        dateAccountExpiry.setEnabled(!SystemPolicyUtil.getSystemPolicy().isTrialPackage());
        dateFYstart.setEnabled(false);
        dateFYend.setEnabled(false);
        btnChangeFY.setEnabled(false);
        A4AddressLinePanel.setVisible(true);
        this.setExtendedState(this.MAXIMIZED_BOTH);
    }

    public void setCompanyPolicy(Company getCompany) {

        try {
            company = getCompany;
            companyPolicy = getCompany.getCompanyPolicy();
//            btnRemoveImage.setEnabled(false);
            String imgPath = ".\\assets\\" + company.getCompanyName() + "-logo.jpg";

            File file = new File(imgPath);
            System.out.println("FILE-->" + file);
//            InputStream in = new FileInputStream(imgPath);
//            OutputStream outputStream = new FileOutputStream(file);
//            IOUtils.copy(in, outputStream);
//            outputStream.close();
            if (file.exists()) {
                URI uri = file.toURI();
                URL imgURL = uri.toURL();
                Image image = new ImageIcon(imgURL).getImage();
                ImageIcon icon = new ImageIcon(image);
                lblimg.setIcon(icon);
                btnRemoveImage.setEnabled(true);
                btnUploadImage.setEnabled(false);
//                    btnRemoveImage.setEnabled(true);
            }
            else {
                btnRemoveImage.setEnabled(false);
                btnUploadImage.setEnabled(true);
            }

//        String imgPath = company.getCompanyName() + "-logo.jpg";
//        URL imgURL = getClass().getResource("/images/" + imgPath);
//        System.out.println("imgPath-->" + imgPath + "-->imgURL--->" + imgURL);
//        if (imgURL != null) {
//            Image image = new ImageIcon(imgURL).getImage();
//            ImageIcon icon = new ImageIcon(image);
//            lblimg.setIcon(icon);
//            btnRemoveImage.setEnabled(true);
//        }
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            labelCompanyName.setText(company.getCompanyName());
            Boolean isUnderStockPossible = companyPolicy.isUnderStockPossible();
            radioUSPyes.setSelected(isUnderStockPossible);
            radioUSPno.setSelected(!isUnderStockPossible);
            Boolean isProfitNeed = companyPolicy.isProfitEnabled();
            radioPNyes.setSelected(isProfitNeed);
            radioPNno.setSelected(!isProfitNeed);
            radioAmountyes.setSelected(companyPolicy.isAmountTendered());
            radioAmountno.setSelected(!companyPolicy.isAmountTendered());
            txtSaleBillCount.setText(String.valueOf(companyPolicy.getSaleBillCount()));
            dateAccountExpiry.setDate(companyPolicy.getAccountsExpiryDate());

            radioAmountyes.setSelected(companyPolicy.isAmountTendered());
            radioAmountno.setSelected(!companyPolicy.isAmountTendered());

            dateFYend.setDate(companyPolicy.getFinancialYearEnd());
            dateFYstart.setDate(companyPolicy.getFinancialYearStart());
            comboNoCopies.setSelectedItem(String.valueOf(companyPolicy.getNumberOfInvoiceCopies()));
            txtFirstCopyName.setText(companyPolicy.getFirstInvoiceWord());
            txtSecondCopyName.setText(companyPolicy.getSecondInvoiceWord());
            txtThirdCopyName.setText(companyPolicy.getThirdInvoiceWord());
            txtFourthCopyName.setText(companyPolicy.getFourthInvoiceWord());
            txtFifthCopyName.setText(companyPolicy.getFifthInvoiceWord());
            txtTaxInvoiceName.setText(companyPolicy.getTaxInvoiceWord());
            txtBillOfSupplyName.setText(companyPolicy.getBillOfSupplyWord());
            txtEstimateBillName.setText(companyPolicy.getEstimateBillWord());
            txtFooterHeading.setText(companyPolicy.getFooterHeading());
            txtFooterLineOne.setText(companyPolicy.getFooterLineOne());
            txtFooterLineTwo.setText(companyPolicy.getFooterLineTwo());
            txtFooterLineThree.setText(companyPolicy.getFooterLineThree());
            txtFooterLineFour.setText(companyPolicy.getFooterLineFour());
            txtA4AddressLine1.setText(companyPolicy.getA4PAddressLineOne());
            txtA4AddressLine2.setText(companyPolicy.getA4PAddressLineTwo());
            txtA4AddressLine3.setText(companyPolicy.getA4PAddressLineThree());
            txtA5LAddrLine1.setText(companyPolicy.getA5LAddressLineOne());
            txtA5LAddrLine2.setText(companyPolicy.getA5LAddressLineTwo());
            txtA5LAddrLine3.setText(companyPolicy.getA5LAddressLineThree());
            txtA5PAddrLine1.setText(companyPolicy.getA5PAddressLineOne());
            txtA5PAddrLine2.setText(companyPolicy.getA5PAddressLineTwo());
            txtA5PAddrLine3.setText(companyPolicy.getA5PAddressLineThree());
            txtA7AddrLine1.setText(companyPolicy.getA7AddressLineOne());
            txtA7AddrLine2.setText(companyPolicy.getA7AddressLineTwo());
            txtA7AddrLine3.setText(companyPolicy.getA7AddressLineThree());
            radioEBAno.setSelected(!companyPolicy.isSaleEditApproval());
            radioEBAyes.setSelected(companyPolicy.isSaleEditApproval());
            radioLTAno.setSelected(!companyPolicy.isLedgerTransactionApproval());
            radioLTAyes.setSelected(companyPolicy.isLedgerTransactionApproval());
//        radioAmountyes.setSelected(companyPolicy.isAmountTendered());
//        radioAmountno.setSelected(!companyPolicy.isAmountTendered());
            txtSalesPrefix.setText(companyPolicy.getTaxInvoicePrefix());
            txtSalesSeq.setText(String.valueOf(companyPolicy.getTaxInvoiceSequenceStart()));
            txtSalesSuffix.setText(companyPolicy.getTaxInvoiceSuffix());
            txtPurchasePrefix.setText(companyPolicy.getPurchasePrefix());
            txtPurchaseSeq.setText(String.valueOf(companyPolicy.getPurchaseSequenceStart()));
            txtPurchaseSuffix.setText(companyPolicy.getPurchaseSuffix());
            txtCDnotePrefix.setText(companyPolicy.getCrDrPrefix());
            txtCDnoteSeq.setText(String.valueOf(companyPolicy.getCrDrSequenceStart()));
            txtCDnoteSuffix.setText(companyPolicy.getCrDrSuffix());
            txtEstimatePrefix.setText(companyPolicy.getEstimatePrefix());
            txtEstimateSeq.setText(String.valueOf(companyPolicy.getEstimateSequenceStart()));
            txtEstimateSuffix.setText(companyPolicy.getEstimateSuffix());
            txtBOSPrefix.setText(companyPolicy.getBosPrefix());
            txtBOSSeq.setText(String.valueOf(companyPolicy.getBosSequenceStart()));
            txtBOSSuffix.setText(companyPolicy.getBosSuffix());

            //        printPaperSizeGroup.getSelection().setActionCommand(companyPolicy.getPrintPaperSize());
            String type = companyPolicy.getPrintPaperSize();

            switch (type) {
                case "A4_P":
                    radioA4Portrait.setSelected(true);
                    break;
                case "A5_P":
                    radioA5Portrait.setSelected(true);
                    break;
                case "A5_L":
                    radioA5Landscape.setSelected(true);
                    break;
                case "A7_P":
                    radioA7Portrait.setSelected(true);
                    break;
                default:
                    break;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(CompanyPolicyUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CompanyPolicyUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        underStockPossibleButtonGroup = new javax.swing.ButtonGroup();
        profitNeddButtonGroup = new javax.swing.ButtonGroup();
        printPaperSizeGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        labelUserName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        A4AddressLinePanel = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txtA4AddressLine3 = new javax.swing.JTextField();
        txtA4AddressLine2 = new javax.swing.JTextField();
        txtA4AddressLine1 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtA5LAddrLine1 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txtA5LAddrLine2 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtA5LAddrLine3 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtA5PAddrLine1 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtA5PAddrLine2 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtA5PAddrLine3 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txtA7AddrLine1 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtA7AddrLine2 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txtA7AddrLine3 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        txtSalesPrefix = new javax.swing.JTextField();
        txtPurchasePrefix = new javax.swing.JTextField();
        txtCDnotePrefix = new javax.swing.JTextField();
        txtEstimatePrefix = new javax.swing.JTextField();
        txtBOSPrefix = new javax.swing.JTextField();
        txtSalesSeq = new javax.swing.JTextField();
        txtPurchaseSeq = new javax.swing.JTextField();
        txtCDnoteSeq = new javax.swing.JTextField();
        txtEstimateSeq = new javax.swing.JTextField();
        txtSalesSuffix = new javax.swing.JTextField();
        txtBOSSeq = new javax.swing.JTextField();
        txtPurchaseSuffix = new javax.swing.JTextField();
        txtCDnoteSuffix = new javax.swing.JTextField();
        txtEstimateSuffix = new javax.swing.JTextField();
        txtBOSSuffix = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        comboNoCopies = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        txtFirstCopyName = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtSecondCopyName = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtFourthCopyName = new javax.swing.JTextField();
        txtThirdCopyName = new javax.swing.JTextField();
        txtFifthCopyName = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        radioUSPyes = new javax.swing.JRadioButton();
        radioUSPno = new javax.swing.JRadioButton();
        radioPNno = new javax.swing.JRadioButton();
        radioPNyes = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        radioLTAyes = new javax.swing.JRadioButton();
        radioLTAno = new javax.swing.JRadioButton();
        radioEBAyes = new javax.swing.JRadioButton();
        radioEBAno = new javax.swing.JRadioButton();
        jLabel46 = new javax.swing.JLabel();
        radioAmountyes = new javax.swing.JRadioButton();
        radioAmountno = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        dateAccountExpiry = new com.toedter.calendar.JDateChooser();
        txtSaleBillCount = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        dateFYstart = new com.toedter.calendar.JDateChooser();
        dateFYend = new com.toedter.calendar.JDateChooser();
        btnChangeFY = new javax.swing.JButton();
        PwdFinancialYr = new javax.swing.JPasswordField();
        jLabel11 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtTaxInvoiceName = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtBillOfSupplyName = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtEstimateBillName = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtFooterHeading = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtFooterLineOne = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtFooterLineTwo = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtFooterLineThree = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtFooterLineFour = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        radioA4Portrait = new javax.swing.JRadioButton();
        radioA5Portrait = new javax.swing.JRadioButton();
        radioA5Landscape = new javax.swing.JRadioButton();
        radioA7Portrait = new javax.swing.JRadioButton();
        lblimg = new javax.swing.JLabel();
        btnUploadImage = new javax.swing.JButton();
        btnRemoveImage = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        goHomebtn = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        Home = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuBackUP = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Company Policy");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        jLabel1.setText("User : ");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-");

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        A4AddressLinePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Address Format", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        jLabel20.setText("A4 Address Line 1");

        jLabel27.setText("A4 Address Line 2");

        jLabel28.setText("A4 Address Line 3");

        txtA4AddressLine3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA4AddressLine3KeyReleased(evt);
            }
        });

        txtA4AddressLine2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA4AddressLine2ActionPerformed(evt);
            }
        });
        txtA4AddressLine2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA4AddressLine2KeyReleased(evt);
            }
        });

        txtA4AddressLine1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA4AddressLine1ActionPerformed(evt);
            }
        });
        txtA4AddressLine1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA4AddressLine1KeyReleased(evt);
            }
        });

        jLabel36.setText("A5 Landscape Address Line 1");

        txtA5LAddrLine1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA5LAddrLine1ActionPerformed(evt);
            }
        });
        txtA5LAddrLine1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtA5LAddrLine1KeyPressed(evt);
            }
        });

        jLabel37.setText("A5 Landscape Address Line 2");

        txtA5LAddrLine2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA5LAddrLine2ActionPerformed(evt);
            }
        });
        txtA5LAddrLine2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA5LAddrLine2KeyReleased(evt);
            }
        });

        jLabel35.setText("A5 Landscape Address Line 3");

        txtA5LAddrLine3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA5LAddrLine3KeyReleased(evt);
            }
        });

        jLabel29.setText("A5 Potrait Address Line 1");

        txtA5PAddrLine1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA5PAddrLine1ActionPerformed(evt);
            }
        });
        txtA5PAddrLine1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA5PAddrLine1KeyReleased(evt);
            }
        });

        jLabel30.setText("A5 Potrait Address Line 2");

        txtA5PAddrLine2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA5PAddrLine2ActionPerformed(evt);
            }
        });
        txtA5PAddrLine2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA5PAddrLine2KeyReleased(evt);
            }
        });

        jLabel31.setText("A5 Potrait Address Line 3");

        txtA5PAddrLine3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA5PAddrLine3KeyReleased(evt);
            }
        });

        jLabel33.setText("A7 Address Line 1");

        txtA7AddrLine1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA7AddrLine1ActionPerformed(evt);
            }
        });
        txtA7AddrLine1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA7AddrLine1KeyReleased(evt);
            }
        });

        jLabel34.setText("A7 Address Line 2");

        txtA7AddrLine2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtA7AddrLine2ActionPerformed(evt);
            }
        });
        txtA7AddrLine2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA7AddrLine2KeyReleased(evt);
            }
        });

        jLabel32.setText("A7 Address Line 3");

        txtA7AddrLine3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtA7AddrLine3KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout A4AddressLinePanelLayout = new javax.swing.GroupLayout(A4AddressLinePanel);
        A4AddressLinePanel.setLayout(A4AddressLinePanelLayout);
        A4AddressLinePanelLayout.setHorizontalGroup(
            A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtA4AddressLine1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtA4AddressLine2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtA4AddressLine3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtA5PAddrLine1)
                            .addComponent(txtA5PAddrLine2)
                            .addComponent(txtA5PAddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtA7AddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtA7AddrLine1)
                            .addComponent(txtA7AddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel35)
                            .addComponent(jLabel36)
                            .addComponent(jLabel37))
                        .addGap(18, 18, 18)
                        .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtA5LAddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtA5LAddrLine1)
                            .addComponent(txtA5LAddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        A4AddressLinePanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel20, jLabel27, jLabel28, jLabel29, jLabel30, jLabel31, jLabel32, jLabel33, jLabel34, jLabel35, jLabel36, jLabel37});

        A4AddressLinePanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtA4AddressLine1, txtA4AddressLine2, txtA4AddressLine3, txtA5LAddrLine1, txtA5LAddrLine2, txtA5LAddrLine3, txtA5PAddrLine1, txtA5PAddrLine2, txtA5PAddrLine3, txtA7AddrLine1, txtA7AddrLine2, txtA7AddrLine3});

        A4AddressLinePanelLayout.setVerticalGroup(
            A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel20)
                    .addComponent(txtA4AddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(txtA4AddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(txtA4AddressLine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel35))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(txtA5LAddrLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA5LAddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA5LAddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(txtA5PAddrLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA5PAddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA5PAddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel31)))
                .addGap(18, 18, 18)
                .addGroup(A4AddressLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(txtA7AddrLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA7AddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtA7AddrLine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(A4AddressLinePanelLayout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel32)))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        A4AddressLinePanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel20, jLabel27, jLabel28, jLabel29, jLabel30, jLabel31, jLabel32, jLabel33, jLabel34, jLabel35, jLabel36, jLabel37});

        A4AddressLinePanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtA4AddressLine1, txtA4AddressLine2, txtA4AddressLine3, txtA5LAddrLine1, txtA5LAddrLine2, txtA5LAddrLine3, txtA5PAddrLine1, txtA5PAddrLine2, txtA5PAddrLine3, txtA7AddrLine1, txtA7AddrLine2, txtA7AddrLine3});

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Bill Format", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12)), "Bill Format", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        jLabel7.setText("Sales");

        jLabel39.setText("Purchase");

        jLabel40.setText("Credit /Debit Note");

        jLabel41.setText("Estimate");

        jLabel42.setText("Bill OfSupply");

        jLabel43.setText("Prefix");

        jLabel44.setText("Sequence Start");

        jLabel45.setText("Suffix");

        txtCDnoteSeq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCDnoteSeqActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42)
                    .addComponent(jLabel41)
                    .addComponent(jLabel40)
                    .addComponent(jLabel39)
                    .addComponent(jLabel7))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPurchasePrefix)
                    .addComponent(txtCDnotePrefix)
                    .addComponent(txtEstimatePrefix)
                    .addComponent(txtBOSPrefix)
                    .addComponent(txtSalesPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtEstimateSeq, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                    .addComponent(txtBOSSeq, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                    .addComponent(txtSalesSeq, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                    .addComponent(txtPurchaseSeq, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                    .addComponent(txtCDnoteSeq, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                    .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtBOSSuffix, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtPurchaseSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSalesSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCDnoteSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtEstimateSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel45))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel39, jLabel40, jLabel41, jLabel42, jLabel7});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel43, jLabel45, txtBOSPrefix, txtBOSSuffix, txtCDnotePrefix, txtCDnoteSuffix, txtEstimatePrefix, txtEstimateSuffix, txtPurchasePrefix, txtPurchaseSuffix, txtSalesPrefix, txtSalesSuffix});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtBOSSeq, txtCDnoteSeq, txtEstimateSeq, txtPurchaseSeq, txtSalesSeq});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43)
                    .addComponent(jLabel45))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtSalesPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalesSeq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalesSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txtPurchasePrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPurchaseSeq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPurchaseSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(txtCDnotePrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCDnoteSeq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCDnoteSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(txtEstimatePrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEstimateSeq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEstimateSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txtBOSPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBOSSeq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBOSSuffix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel39, jLabel40, jLabel41, jLabel42, jLabel7});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtBOSPrefix, txtBOSSuffix, txtCDnotePrefix, txtCDnoteSuffix, txtEstimatePrefix, txtEstimateSuffix, txtPurchasePrefix, txtPurchaseSuffix, txtSalesPrefix, txtSalesSuffix});

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Bill Print", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        jLabel12.setText("No of Copies");

        comboNoCopies.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
        comboNoCopies.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboNoCopiesActionPerformed(evt);
            }
        });

        jLabel13.setText("1st Copy Name");

        jLabel14.setText("2nd Copy Name");

        jLabel15.setText("3rd Copy Name");

        jLabel16.setText("4th Copy Name");

        jLabel17.setText("5th Copy Name");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtThirdCopyName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFourthCopyName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFifthCopyName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSecondCopyName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFirstCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboNoCopies, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(17, 17, 17))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtFifthCopyName, txtFirstCopyName, txtFourthCopyName, txtSecondCopyName, txtThirdCopyName});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel12, jLabel13, jLabel14, jLabel15, jLabel16, jLabel17});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(comboNoCopies, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtFirstCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtSecondCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtThirdCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(txtFourthCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(txtFifthCopyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtFifthCopyName, txtFirstCopyName, txtFourthCopyName, txtSecondCopyName, txtThirdCopyName});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel12, jLabel13, jLabel14, jLabel15, jLabel16, jLabel17});

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Configuration", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel4.setText("Under Stock Possible");

        underStockPossibleButtonGroup.add(radioUSPyes);
        radioUSPyes.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        radioUSPyes.setText("Yes");

        underStockPossibleButtonGroup.add(radioUSPno);
        radioUSPno.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        radioUSPno.setText("No");

        profitNeddButtonGroup.add(radioPNno);
        radioPNno.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        radioPNno.setText("No");

        profitNeddButtonGroup.add(radioPNyes);
        radioPNyes.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        radioPNyes.setText("Yes");

        jLabel8.setText("Show Profit");

        jLabel9.setText("Edit Bill Approval");

        jLabel38.setText("Ledger Transaction Approval");

        buttonGroup2.add(radioLTAyes);
        radioLTAyes.setText("Yes");

        buttonGroup2.add(radioLTAno);
        radioLTAno.setText("No");

        buttonGroup1.add(radioEBAyes);
        radioEBAyes.setText("Yes");

        buttonGroup1.add(radioEBAno);
        radioEBAno.setText("No");

        jLabel46.setText("Amount Tendered");

        buttonGroup3.add(radioAmountyes);
        radioAmountyes.setText("Yes");

        buttonGroup3.add(radioAmountno);
        radioAmountno.setText("No");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(50, 50, 50)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(radioPNyes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioPNno))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(radioUSPyes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioUSPno))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(radioLTAyes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioLTAno))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(radioEBAyes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioEBAno))))
                    .addComponent(jLabel38)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGap(158, 158, 158)
                            .addComponent(radioAmountyes)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(radioAmountno))
                        .addComponent(jLabel46)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(radioUSPyes)
                    .addComponent(radioUSPno))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(radioPNyes)
                    .addComponent(radioPNno))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(radioEBAyes)
                        .addComponent(radioEBAno))
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioLTAyes)
                    .addComponent(radioLTAno))
                .addGap(1, 1, 1)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioAmountyes)
                    .addComponent(radioAmountno)))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Account Expiry Date");

        dateAccountExpiry.setDateFormatString("dd-MM-yyy");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("Sale Bill Count");

        jLabel10.setText("Financial Year Start");

        dateFYstart.setDateFormatString("dd-MM-yyyy");

        dateFYend.setDateFormatString("dd-MM-yyyy");

        btnChangeFY.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnChangeFY.setForeground(new java.awt.Color(204, 0, 0));
        btnChangeFY.setText("Change");
        btnChangeFY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeFYActionPerformed(evt);
            }
        });

        PwdFinancialYr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PwdFinancialYrKeyPressed(evt);
            }
        });

        jLabel11.setText("Financial Year End");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(PwdFinancialYr, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnChangeFY))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(dateAccountExpiry, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtSaleBillCount)
                            .addComponent(dateFYstart, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                            .addComponent(dateFYend, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel11, jLabel5, jLabel6});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateAccountExpiry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtSaleBillCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateFYstart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dateFYend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PwdFinancialYr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChangeFY))
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Paper Format", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        jLabel18.setText("Tax Invoice Name");

        txtTaxInvoiceName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTaxInvoiceNameKeyReleased(evt);
            }
        });

        jLabel19.setText("Bill Of Supply Name");

        txtBillOfSupplyName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillOfSupplyNameKeyReleased(evt);
            }
        });

        jLabel21.setText("Estimate Bill Name");

        txtEstimateBillName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEstimateBillNameKeyReleased(evt);
            }
        });

        jLabel24.setText("Footer Heading");

        txtFooterHeading.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFooterHeadingActionPerformed(evt);
            }
        });
        txtFooterHeading.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFooterHeadingKeyReleased(evt);
            }
        });

        jLabel22.setText("Footer Line One");

        txtFooterLineOne.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFooterLineOneKeyReleased(evt);
            }
        });

        jLabel23.setText("Footer Line Two");

        txtFooterLineTwo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFooterLineTwoActionPerformed(evt);
            }
        });
        txtFooterLineTwo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFooterLineTwoKeyReleased(evt);
            }
        });

        jLabel25.setText("Footer Line Three");

        txtFooterLineThree.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFooterLineThreeKeyReleased(evt);
            }
        });

        jLabel26.setText("Footer Line Four");

        txtFooterLineFour.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFooterLineFourKeyReleased(evt);
            }
        });

        jLabel3.setText("Bill Print Format");

        printPaperSizeGroup.add(radioA4Portrait);
        radioA4Portrait.setText("A4 Portrait");
        radioA4Portrait.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioA4PortraitActionPerformed(evt);
            }
        });

        printPaperSizeGroup.add(radioA5Portrait);
        radioA5Portrait.setText("A5 Portrait");
        radioA5Portrait.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioA5PortraitActionPerformed(evt);
            }
        });

        printPaperSizeGroup.add(radioA5Landscape);
        radioA5Landscape.setText("A5 Landscape");
        radioA5Landscape.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioA5LandscapeActionPerformed(evt);
            }
        });

        printPaperSizeGroup.add(radioA7Portrait);
        radioA7Portrait.setText("A7 Potrait");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioA4Portrait)
                            .addComponent(radioA5Portrait)
                            .addComponent(radioA5Landscape)
                            .addComponent(radioA7Portrait))
                        .addGap(0, 161, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFooterHeading, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtFooterLineOne, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtFooterLineTwo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtFooterLineThree, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtFooterLineFour, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTaxInvoiceName, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                                    .addComponent(txtBillOfSupplyName)
                                    .addComponent(txtEstimateBillName))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel18, jLabel19, jLabel21, jLabel22, jLabel23, jLabel24, jLabel25, jLabel26, jLabel3});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioA4Portrait)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioA5Portrait)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioA5Landscape)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioA7Portrait)
                .addGap(1, 1, 1)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(40, 40, 40)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(txtTaxInvoiceName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtBillOfSupplyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEstimateBillName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFooterHeading, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFooterLineOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFooterLineTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFooterLineThree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFooterLineFour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        btnUploadImage.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnUploadImage.setText("Upload Logo (100x100)");
        btnUploadImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadImageActionPerformed(evt);
            }
        });

        btnRemoveImage.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnRemoveImage.setText("Remove Logo");
        btnRemoveImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveImageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnUploadImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(lblimg, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnRemoveImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(A4AddressLinePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(lblimg, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnUploadImage)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnRemoveImage)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(A4AddressLinePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Update");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnCancel.setMnemonic('s');
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        goHomebtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        goHomebtn.setMnemonic('h');
        goHomebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goHomebtnActionPerformed(evt);
            }
        });

        Home.setMnemonic('f');
        Home.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        Home.add(menuDashboard);

        menuBar.add(Home);

        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuBackUP.setText("Maintenance");
        menuBackUP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBackUPActionPerformed(evt);
            }
        });
        menuSettings.add(menuBackUP);

        menuBar.add(menuSettings);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(goHomebtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelUserName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnLogout))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelCompanyName)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(641, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 95, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 953, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogout)
                    .addComponent(labelUserName)
                    .addComponent(jLabel1)
                    .addComponent(goHomebtn))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelCompanyName))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(77, 77, 77))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    public void getRadioValue() {
        radioA4Portrait.setActionCommand("A4_P");
        radioA5Portrait.setActionCommand("A5_P");
        radioA5Landscape.setActionCommand("A5_L");
        radioA7Portrait.setActionCommand("A7_P");
    }

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        getRadioValue();
        companyPolicy.setAccountsExpiryDate(dateAccountExpiry.getDate());
        companyPolicy.setFinancialYearEnd(dateFYend.getDate());
        companyPolicy.setFinancialYearStart(dateFYstart.getDate());
        companyPolicy.setProfitEnabled(radioPNyes.isSelected());
        companyPolicy.setSaleBillCount(Long.parseLong(txtSaleBillCount.getText()));
        companyPolicy.setUnderStockPossible(radioUSPyes.isSelected());
        companyPolicy.setAmountTendered(radioAmountyes.isSelected());

        String paparSize = "";
        if (radioA4Portrait.isSelected()) {
            paparSize = "A4_P";
        }
        else if (radioA5Portrait.isSelected()) {
            paparSize = "A5_P";
        }
        else if (radioA5Landscape.isSelected()) {
            paparSize = "A5_L";
        }
        else if (radioA7Portrait.isSelected()) {
            paparSize = "A7_P";
        }

        companyPolicy.setPrintPaperSize(paparSize);
        companyPolicy.setNumberOfInvoiceCopies(Integer.parseInt((String) comboNoCopies.getSelectedItem()));
        companyPolicy.setFirstInvoiceWord(txtFirstCopyName.getText());
        companyPolicy.setSecondInvoiceWord(txtSecondCopyName.getText());
        companyPolicy.setThirdInvoiceWord(txtThirdCopyName.getText());
        companyPolicy.setFourthInvoiceWord(txtFourthCopyName.getText());
        companyPolicy.setFifthInvoiceWord(txtFifthCopyName.getText());
        companyPolicy.setBillOfSupplyWord(txtBillOfSupplyName.getText());
        companyPolicy.setTaxInvoiceWord(txtTaxInvoiceName.getText());
        companyPolicy.setEstimateBillWord(txtEstimateBillName.getText());
        companyPolicy.setFooterHeading(txtFooterHeading.getText());
        companyPolicy.setFooterLineOne(txtFooterLineOne.getText());
        companyPolicy.setFooterLineTwo(txtFooterLineTwo.getText());
        companyPolicy.setFooterLineThree(txtFooterLineThree.getText());
        companyPolicy.setFooterLineFour(txtFooterLineFour.getText());
        companyPolicy.setA4PAddressLineOne(txtA4AddressLine1.getText());
        companyPolicy.setA4PAddressLineTwo(txtA4AddressLine2.getText());
        companyPolicy.setA4PAddressLineThree(txtA4AddressLine3.getText());

        companyPolicy.setA5LAddressLineOne(txtA5LAddrLine1.getText());
        companyPolicy.setA5LAddressLineTwo(txtA5LAddrLine2.getText());
        companyPolicy.setA5LAddressLineThree(txtA5LAddrLine3.getText());

        companyPolicy.setA5PAddressLineOne(txtA5PAddrLine1.getText());
        companyPolicy.setA5PAddressLineTwo(txtA5PAddrLine2.getText());
        companyPolicy.setA5PAddressLineThree(txtA5PAddrLine3.getText());

        companyPolicy.setA7AddressLineOne(txtA7AddrLine1.getText());
        companyPolicy.setA7AddressLineTwo(txtA7AddrLine2.getText());
        companyPolicy.setA7AddressLineThree(txtA7AddrLine3.getText());

        companyPolicy.setSaleEditApproval(radioEBAyes.isSelected());
        companyPolicy.setLedgerTransactionApproval(radioLTAyes.isSelected());

        companyPolicy.setTaxInvoicePrefix(txtSalesPrefix.getText());
        companyPolicy.setTaxInvoiceSequenceStart(Long.parseLong(txtSalesSeq.getText()));
        companyPolicy.setTaxInvoiceSuffix(txtSalesSuffix.getText());

        companyPolicy.setPurchasePrefix(txtPurchasePrefix.getText());
        companyPolicy.setPurchaseSequenceStart(Long.parseLong(txtPurchaseSeq.getText()));
        companyPolicy.setPurchaseSuffix(txtPurchaseSuffix.getText());

        companyPolicy.setCrDrPrefix(txtCDnotePrefix.getText());
        companyPolicy.setCrDrSequenceStart(Long.parseLong(txtCDnoteSeq.getText()));
        companyPolicy.setCrDrSuffix(txtCDnoteSuffix.getText());

        companyPolicy.setEstimatePrefix(txtEstimatePrefix.getText());
        companyPolicy.setEstimateSequenceStart(Long.parseLong(txtEstimateSeq.getText()));
        companyPolicy.setEstimateSuffix(txtEstimateSuffix.getText());

        companyPolicy.setBosPrefix(txtBOSPrefix.getText());
        companyPolicy.setBosSequenceStart(Long.parseLong(txtBOSSeq.getText()));
        companyPolicy.setBosSuffix(txtBOSSuffix.getText());

        Boolean isSuccess = new CompanyPolicyLogic().updateCompanyPolicy(companyPolicy);

        Object[] options = {"Yes", "No"};
        int p = JOptionPane.showOptionDialog(null, "The Software will LOGOUT after updating Company Policy, Proceed?", "CONFIRMATION", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
        if (p == JOptionPane.YES_OPTION) {
            if (isSuccess) {
                JOptionPane.showMessageDialog(null, company.getCompanyName() + " Policy is updated", "Message", JOptionPane.WARNING_MESSAGE);
                new LogInUI().setVisible(true);
                dispose();
            }
            else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Update has been cancelled", "Message", JOptionPane.INFORMATION_MESSAGE);
        }


    }//GEN-LAST:event_btnSaveActionPerformed

    public void uploadImage() {

        javax.swing.JFileChooser filechooser = new javax.swing.JFileChooser();
        filechooser.setDialogTitle("Choose Your File");
        filechooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
        // below code selects the file 
        int returnval = filechooser.showOpenDialog(this);
        if (returnval == javax.swing.JFileChooser.APPROVE_OPTION) {
            File file = filechooser.getSelectedFile();
            try {
                OutputStream out = null;
                // display the image in a Jlabel
                BufferedImage bi = ImageIO.read(file);
                int imageHt = bi.getHeight();
                int imagewd = bi.getWidth();

                if (!(imageHt == 100 && imagewd == 100)) {
                    JOptionPane.showMessageDialog(null, "Please Select (100x100) image", "Alert", JOptionPane.WARNING_MESSAGE);
                }
                else {
                    BufferedImage newimg = getScaledImage(bi, imagewd, imageHt);
//                lblImg.setIcon(new ImageIcon(newimg));
                    lblimg.setIcon(new ImageIcon(newimg));

                    String name = company.getCompanyName() + "-logo.jpg";
//                    ".\\temp\\"
//                    out = new FileOutputStream(".\\src\\main\\resources\\images\\" + name);
                    out = new FileOutputStream(".\\assets\\" + name);
                    ImageIO.write(newimg, "jpg", out);
                    btnUploadImage.setEnabled(false);
                    btnRemoveImage.setEnabled(true);
                }

            } catch (IOException e) {
                e.printStackTrace(); // todo: implement proper error handeling
            }
            this.pack();
        }
    }

    public static BufferedImage getScaledImage(BufferedImage image, int width, int height) throws IOException {
        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();

        double scaleX = (double) width / imageWidth;
        double scaleY = (double) height / imageHeight;
        AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
        AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

        return bilinearScaleOp.filter(
                image,
                new BufferedImage(width, height, image.getType()));
    }

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
        }
        else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
        }
        else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
        }
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuBackUPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBackUPActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBackUPActionPerformed

    private void radioA5PortraitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioA5PortraitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioA5PortraitActionPerformed

    private void radioA5LandscapeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioA5LandscapeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioA5LandscapeActionPerformed

    private void radioA4PortraitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioA4PortraitActionPerformed
//        if (radioA4Portrait.isSelected()) {
//            A4AddressLinePanel.setVisible(true);
//            MainAddressPanel.add(A4AddressLinePanel);
//            MainAddressPanel.add(jLabel20);
//            MainAddressPanel.add(jLabel27);
//            MainAddressPanel.add(jLabel28);
//            MainAddressPanel.add(txtA4AddressLine1);
//            MainAddressPanel.add(txtA4AddressLine2);
//            MainAddressPanel.add(txtA4AddressLine3);
//            
//
//        }

    }//GEN-LAST:event_radioA4PortraitActionPerformed

    private void txtTaxInvoiceNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTaxInvoiceNameKeyReleased
        String taxInvoiceName = txtTaxInvoiceName.getText();
        if (taxInvoiceName.length() <= 30) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 30 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            taxInvoiceName = taxInvoiceName.substring(0, 30);
            txtTaxInvoiceName.setText(taxInvoiceName);
        }
    }//GEN-LAST:event_txtTaxInvoiceNameKeyReleased

    private void txtBillOfSupplyNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillOfSupplyNameKeyReleased
        String BillOfSupplyName = txtBillOfSupplyName.getText();
        if (BillOfSupplyName.length() <= 30) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 30 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            BillOfSupplyName = BillOfSupplyName.substring(0, 30);
            txtBillOfSupplyName.setText(BillOfSupplyName);
        }
    }//GEN-LAST:event_txtBillOfSupplyNameKeyReleased

    private void txtEstimateBillNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEstimateBillNameKeyReleased
        String EstimateBillName = txtEstimateBillName.getText();
        if (EstimateBillName.length() <= 30) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 30 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            EstimateBillName = EstimateBillName.substring(0, 30);
            txtEstimateBillName.setText(EstimateBillName);
        }
    }//GEN-LAST:event_txtEstimateBillNameKeyReleased

    private void txtFooterLineOneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFooterLineOneKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterLineOneKeyReleased

    private void txtFooterLineTwoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFooterLineTwoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterLineTwoKeyReleased

    private void txtFooterHeadingKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFooterHeadingKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterHeadingKeyReleased

    private void txtFooterLineThreeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFooterLineThreeKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterLineThreeKeyReleased

    private void txtFooterLineFourKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFooterLineFourKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterLineFourKeyReleased

    private void PwdFinancialYrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PwdFinancialYrKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (PwdFinancialYr.getText().equals("9688")) {
                dateFYstart.setEnabled(true);
                dateFYend.setEnabled(true);
                btnChangeFY.setEnabled(true);
            }
            else {
//                J/OptionPane.showMessageDialog(null, "Please enter the correct Password", "Alert", JOptionPane.WARNING_MESSAGE);
                dateFYstart.setEnabled(false);
                dateFYend.setEnabled(false);
            }
        }
    }//GEN-LAST:event_PwdFinancialYrKeyPressed

    private void txtA7AddrLine1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA7AddrLine1KeyReleased
        String A7AddrLine1 = txtA7AddrLine1.getText();
        if (A7AddrLine1.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A7AddrLine1 = A7AddrLine1.substring(0, 60);
            txtA7AddrLine1.setText(A7AddrLine1);
        }
    }//GEN-LAST:event_txtA7AddrLine1KeyReleased

    private void txtA7AddrLine2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA7AddrLine2KeyReleased
        String A7AddrLine2 = txtA7AddrLine2.getText();
        if (A7AddrLine2.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A7AddrLine2 = A7AddrLine2.substring(0, 60);
            txtA7AddrLine2.setText(A7AddrLine2);
        }
    }//GEN-LAST:event_txtA7AddrLine2KeyReleased

    private void txtA7AddrLine3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA7AddrLine3KeyReleased
        String A7AddrLine3 = txtA7AddrLine3.getText();
        if (A7AddrLine3.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A7AddrLine3 = A7AddrLine3.substring(0, 60);
            txtA7AddrLine3.setText(A7AddrLine3);
        }
    }//GEN-LAST:event_txtA7AddrLine3KeyReleased

    private void txtA5PAddrLine1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5PAddrLine1KeyReleased
        String APAddrLine1 = txtA5PAddrLine1.getText();
        if (APAddrLine1.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            APAddrLine1 = APAddrLine1.substring(0, 60);
            txtA5PAddrLine1.setText(APAddrLine1);
        }
    }//GEN-LAST:event_txtA5PAddrLine1KeyReleased

    private void txtA5PAddrLine2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5PAddrLine2KeyReleased
        String APAddrLine2 = txtA5PAddrLine2.getText();
        if (APAddrLine2.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            APAddrLine2 = APAddrLine2.substring(0, 60);
            txtA5PAddrLine2.setText(APAddrLine2);
        }
    }//GEN-LAST:event_txtA5PAddrLine2KeyReleased

    private void txtA5PAddrLine3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5PAddrLine3KeyReleased
        String APAddrLine3 = txtA5PAddrLine3.getText();
        if (APAddrLine3.length() <= 60) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 60 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            APAddrLine3 = APAddrLine3.substring(0, 60);
            txtA5PAddrLine3.setText(APAddrLine3);
        }
    }//GEN-LAST:event_txtA5PAddrLine3KeyReleased

    private void btnChangeFYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeFYActionPerformed

        companyPolicy.setFinancialYearStart(dateFYstart.getDate());
        companyPolicy.setFinancialYearEnd(dateFYend.getDate());

//        System.out.println(company)
        Boolean isSuccess = new CompanyPolicyLogic().updateFinancialYear(companyPolicy);
        if (isSuccess) {
            JOptionPane.showMessageDialog(null, company.getCompanyName() + " Policy Financial Year is updated", "Message", JOptionPane.WARNING_MESSAGE);
//            new CompanyUI().setVisible(true);
//            dispose();
        }
        else {
            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnChangeFYActionPerformed

    private void txtFooterLineTwoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFooterLineTwoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterLineTwoActionPerformed

    private void txtA4AddressLine1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA4AddressLine1KeyReleased
        String A4AddressLine1 = txtA4AddressLine1.getText();
        if (A4AddressLine1.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A4AddressLine1 = A4AddressLine1.substring(0, 35);
            txtA4AddressLine1.setText(A4AddressLine1);
        }
    }//GEN-LAST:event_txtA4AddressLine1KeyReleased

    private void txtA4AddressLine2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA4AddressLine2KeyReleased
        String A4AddressLine2 = txtA4AddressLine2.getText();
        if (A4AddressLine2.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A4AddressLine2 = A4AddressLine2.substring(0, 35);
            txtA4AddressLine2.setText(A4AddressLine2);
        }
    }//GEN-LAST:event_txtA4AddressLine2KeyReleased

    private void txtA4AddressLine3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA4AddressLine3KeyReleased
        String A4AddressLine3 = txtA4AddressLine3.getText();
        if (A4AddressLine3.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A4AddressLine3 = A4AddressLine3.substring(0, 35);
            txtA4AddressLine3.setText(A4AddressLine3);
        }
    }//GEN-LAST:event_txtA4AddressLine3KeyReleased

    private void txtA5LAddrLine1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5LAddrLine1KeyPressed
        String A5LAddressLine1 = txtA5LAddrLine1.getText();
        if (A5LAddressLine1.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A5LAddressLine1 = A5LAddressLine1.substring(0, 35);
            txtA5LAddrLine1.setText(A5LAddressLine1);
        }
    }//GEN-LAST:event_txtA5LAddrLine1KeyPressed

    private void txtA5LAddrLine2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5LAddrLine2KeyReleased
        String A5LAddressLine2 = txtA5LAddrLine2.getText();
        if (A5LAddressLine2.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A5LAddressLine2 = A5LAddressLine2.substring(0, 35);
            txtA5LAddrLine2.setText(A5LAddressLine2);
        }
    }//GEN-LAST:event_txtA5LAddrLine2KeyReleased

    private void txtA5LAddrLine3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtA5LAddrLine3KeyReleased
        String A5LAddressLine3 = txtA5LAddrLine3.getText();
        if (A5LAddressLine3.length() <= 35) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Enter 35 Characters only", "Alert", JOptionPane.WARNING_MESSAGE);
            A5LAddressLine3 = A5LAddressLine3.substring(0, 35);
            txtA5LAddrLine3.setText(A5LAddressLine3);
        }
    }//GEN-LAST:event_txtA5LAddrLine3KeyReleased

    private void txtA4AddressLine2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA4AddressLine2ActionPerformed
        txtA4AddressLine3.requestFocus();
    }//GEN-LAST:event_txtA4AddressLine2ActionPerformed

    private void txtCDnoteSeqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCDnoteSeqActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCDnoteSeqActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        new CompanyUI().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtA4AddressLine1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA4AddressLine1ActionPerformed
        txtA4AddressLine2.requestFocus();
    }//GEN-LAST:event_txtA4AddressLine1ActionPerformed

    private void txtA5LAddrLine1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA5LAddrLine1ActionPerformed
        // TODO add your handling code here:
        txtA5LAddrLine2.requestFocus();
    }//GEN-LAST:event_txtA5LAddrLine1ActionPerformed

    private void txtA5LAddrLine2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA5LAddrLine2ActionPerformed
        // TODO add your handling code here:
        txtA5LAddrLine3.requestFocus();
    }//GEN-LAST:event_txtA5LAddrLine2ActionPerformed

    private void txtA5PAddrLine1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA5PAddrLine1ActionPerformed
        // TODO add your handling code here:
        txtA5PAddrLine2.requestFocus();
    }//GEN-LAST:event_txtA5PAddrLine1ActionPerformed

    private void txtA5PAddrLine2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA5PAddrLine2ActionPerformed
        // TODO add your handling code here:
        txtA5PAddrLine3.requestFocus();
    }//GEN-LAST:event_txtA5PAddrLine2ActionPerformed

    private void txtA7AddrLine1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA7AddrLine1ActionPerformed
        // TODO add your handling code here:
        txtA7AddrLine2.requestFocus();
    }//GEN-LAST:event_txtA7AddrLine1ActionPerformed

    private void txtA7AddrLine2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtA7AddrLine2ActionPerformed
        // TODO add your handling code here:
        txtA7AddrLine3.requestFocus();
    }//GEN-LAST:event_txtA7AddrLine2ActionPerformed

    private void txtFooterHeadingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFooterHeadingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFooterHeadingActionPerformed

    private void goHomebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goHomebtnActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        }
        else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_goHomebtnActionPerformed

    private void comboNoCopiesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboNoCopiesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboNoCopiesActionPerformed

    private void btnUploadImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadImageActionPerformed
        uploadImage();
    }//GEN-LAST:event_btnUploadImageActionPerformed

    private void btnRemoveImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveImageActionPerformed
        removeImage();
    }//GEN-LAST:event_btnRemoveImageActionPerformed

    public void removeImage() {
        String imgPath = ".\\assets\\" + company.getCompanyName() + "-logo.jpg";
        File file = new File(imgPath);

//        String imgPath = company.getCompanyName() + "-logo.jpg";
//        System.out.println("*********" + getClass().getResource("/images/" + imgPath));
////        File file = new File(getClass().getResource("/images/" + imgPath).getFile());
//        File file = new File(("C:/Users/Aryan/Desktop/Projects/gstonepro/target/classes/images/" + imgPath));
        System.out.println(file);
        if (file.delete()) {
            System.out.println("file deleted111111111");

        } else {

            System.out.println("file not deleted111111111");
        }
        if (!file.exists()) {
            System.out.println("file deleted");
            lblimg.setIcon(null);
            btnRemoveImage.setEnabled(false);
            btnUploadImage.setEnabled(true);
        }
        else {
            System.out.println("file not deleted");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CompanyPolicyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CompanyPolicyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CompanyPolicyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CompanyPolicyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CompanyPolicyUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel A4AddressLinePanel;
    private javax.swing.JMenu Home;
    private javax.swing.JPasswordField PwdFinancialYr;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnChangeFY;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnRemoveImage;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUploadImage;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JComboBox<String> comboNoCopies;
    private com.toedter.calendar.JDateChooser dateAccountExpiry;
    private com.toedter.calendar.JDateChooser dateFYend;
    private com.toedter.calendar.JDateChooser dateFYstart;
    private javax.swing.JButton goHomebtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lblimg;
    private javax.swing.JMenuItem menuBackUP;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.ButtonGroup printPaperSizeGroup;
    private javax.swing.ButtonGroup profitNeddButtonGroup;
    private javax.swing.JRadioButton radioA4Portrait;
    private javax.swing.JRadioButton radioA5Landscape;
    private javax.swing.JRadioButton radioA5Portrait;
    private javax.swing.JRadioButton radioA7Portrait;
    private javax.swing.JRadioButton radioAmountno;
    private javax.swing.JRadioButton radioAmountyes;
    private javax.swing.JRadioButton radioEBAno;
    private javax.swing.JRadioButton radioEBAyes;
    private javax.swing.JRadioButton radioLTAno;
    private javax.swing.JRadioButton radioLTAyes;
    private javax.swing.JRadioButton radioPNno;
    private javax.swing.JRadioButton radioPNyes;
    private javax.swing.JRadioButton radioUSPno;
    private javax.swing.JRadioButton radioUSPyes;
    private javax.swing.JTextField txtA4AddressLine1;
    private javax.swing.JTextField txtA4AddressLine2;
    private javax.swing.JTextField txtA4AddressLine3;
    private javax.swing.JTextField txtA5LAddrLine1;
    private javax.swing.JTextField txtA5LAddrLine2;
    private javax.swing.JTextField txtA5LAddrLine3;
    private javax.swing.JTextField txtA5PAddrLine1;
    private javax.swing.JTextField txtA5PAddrLine2;
    private javax.swing.JTextField txtA5PAddrLine3;
    private javax.swing.JTextField txtA7AddrLine1;
    private javax.swing.JTextField txtA7AddrLine2;
    private javax.swing.JTextField txtA7AddrLine3;
    private javax.swing.JTextField txtBOSPrefix;
    private javax.swing.JTextField txtBOSSeq;
    private javax.swing.JTextField txtBOSSuffix;
    private javax.swing.JTextField txtBillOfSupplyName;
    private javax.swing.JTextField txtCDnotePrefix;
    private javax.swing.JTextField txtCDnoteSeq;
    private javax.swing.JTextField txtCDnoteSuffix;
    private javax.swing.JTextField txtEstimateBillName;
    private javax.swing.JTextField txtEstimatePrefix;
    private javax.swing.JTextField txtEstimateSeq;
    private javax.swing.JTextField txtEstimateSuffix;
    private javax.swing.JTextField txtFifthCopyName;
    private javax.swing.JTextField txtFirstCopyName;
    private javax.swing.JTextField txtFooterHeading;
    private javax.swing.JTextField txtFooterLineFour;
    private javax.swing.JTextField txtFooterLineOne;
    private javax.swing.JTextField txtFooterLineThree;
    private javax.swing.JTextField txtFooterLineTwo;
    private javax.swing.JTextField txtFourthCopyName;
    private javax.swing.JTextField txtPurchasePrefix;
    private javax.swing.JTextField txtPurchaseSeq;
    private javax.swing.JTextField txtPurchaseSuffix;
    private javax.swing.JTextField txtSaleBillCount;
    private javax.swing.JTextField txtSalesPrefix;
    private javax.swing.JTextField txtSalesSeq;
    private javax.swing.JTextField txtSalesSuffix;
    private javax.swing.JTextField txtSecondCopyName;
    private javax.swing.JTextField txtTaxInvoiceName;
    private javax.swing.JTextField txtThirdCopyName;
    private javax.swing.ButtonGroup underStockPossibleButtonGroup;
    // End of variables declaration//GEN-END:variables
}
