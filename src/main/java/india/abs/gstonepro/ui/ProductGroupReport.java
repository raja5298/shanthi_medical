/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.ui.billofsupply.BillOfSupplyProductDateReportUI;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.utils.models.SalesReportModel;
import india.abs.gstonepro.business.ProductGroupLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author CEO
 */
public class ProductGroupReport extends javax.swing.JFrame {

    /**
     * Creates new form ProductGroupReport
     */
    Company company = SessionDataUtil.getSelectedCompany();
    DecimalFormat currency = new DecimalFormat("0.000");
    List<ProductGroup> lProductGroup = new ProductGroupLogic().fetchAllPG();
    boolean isProfitEnabled = false;
    String groupName = "All";
    long ProductGrpId = 0;
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
    String strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
    String strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
    double dTotalProfit = 0;
    double dTotalInvoice = 0;
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();

    public ProductGroupReport() {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            setRoles();
//        jMenuItem3.setEnabled(false);
//        jMenuItem9.setEnabled(false);
//        jMenuItem11.setEnabled(false);
//        jMenuItem12.setEnabled(false);
            setExtendedState(this.MAXIMIZED_BOTH);

//        Collections.sort(productsGroup, new Comparator<ProductGroup>() {
//            public int compare(ProductGroup o1, ProductGroup o2) {
//                return o1.getName().compareTo(o2.getName());
//            }
//        });
            setTable();

            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            labelCompanyName.setText(company.getCompanyName());
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            setExtendedState(this.MAXIMIZED_BOTH);
            dateFrom.setDate(new Date());
            dateTo.setDate(new Date());
            dateFrom.requestFocusInWindow();
            
            ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
                
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    dateTo.requestFocusInWindow();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            

            ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
                
                 public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    btnFetch.requestFocus();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            
        } catch (Exception ex) {
            Logger.getLogger(ProductGroupReport.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        dateTo = new com.toedter.calendar.JDateChooser();
        btnFetch = new javax.swing.JButton();
        btnPrintReport = new javax.swing.JButton();
        btnDownloadPdf = new javax.swing.JButton();
        btnDownloadExcel = new javax.swing.JButton();
        labelTotal = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tax Invoice - Product Group Report");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Group", "Invoice Value", "CGST", "IGST", "SGST", "Total Amount (Incl Tax)", "Return Count", "Return Value", "Profit"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Double.class, java.lang.Float.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(200);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(200);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
        }

        jLabel2.setText("From");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        jLabel3.setText("To");

        dateTo.setDateFormatString("dd-MM-yyyy");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });
        btnFetch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnFetchKeyPressed(evt);
            }
        });

        btnPrintReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnPrintReport.setText("Print Report");
        btnPrintReport.setPreferredSize(new java.awt.Dimension(160, 23));
        btnPrintReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintReportActionPerformed(evt);
            }
        });
        btnPrintReport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPrintReportKeyPressed(evt);
            }
        });

        btnDownloadPdf.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDownloadPdf.setText("Download Report (PDF)");
        btnDownloadPdf.setPreferredSize(new java.awt.Dimension(160, 23));
        btnDownloadPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadPdfActionPerformed(evt);
            }
        });
        btnDownloadPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDownloadPdfKeyPressed(evt);
            }
        });

        btnDownloadExcel.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDownloadExcel.setText("Download Report (Excel)");
        btnDownloadExcel.setPreferredSize(new java.awt.Dimension(160, 23));
        btnDownloadExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadExcelActionPerformed(evt);
            }
        });

        labelTotal.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTotal.setText("Total Invoice Amount: 0.00 ");

        jLabel26.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout2.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout2.setMnemonic('l');
        btnLogout2.setText("Logout");
        btnLogout2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout2ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel8.setText("TAX INVOICE - PRODUCT GROUP REPORT");

        jLabel9.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(83, 83, 83)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel26)
                        .addComponent(jLabel8)
                        .addComponent(jButton1))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(labelCompanyName))
                    .addComponent(btnLogout2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addGap(14, 14, 14)
                                .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnDownloadPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnDownloadExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 576, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnFetch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateTo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTotal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownloadPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownloadExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        fetchData();
    }//GEN-LAST:event_btnFetchActionPerformed

    private void btnFetchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnFetchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fetchData();
        }
    }//GEN-LAST:event_btnFetchKeyPressed

    private void btnPrintReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(true);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintReportActionPerformed

    private void btnPrintReportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPrintReportKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            //    pdfReportGeneration(true);
        }
    }//GEN-LAST:event_btnPrintReportKeyPressed

    private void btnDownloadPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadPdfActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(false);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDownloadPdfActionPerformed

    public void pdfReportGeneration(Boolean isPrint) {
        try {
            Date datefrom = dateFrom.getDate();
            Date dateto = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(datefrom);
            String formatdateTo = sdf.format(dateto);
            Document document = new Document(PageSize.A4);
            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "ProductGroup" + formatdateFrom + " to " + formatdateTo + ".pdf";
                isApproved = true;
            } else {
//                path = AppConstants.getDocPath() +"Sales Report " + formatdateFrom + " to " + formatdateTo  + ".pdf";

                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Product Group Report");
                fileChooser.setSelectedFile(new File("Product Group Report " + formatdateFrom + " to " + formatdateTo));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                Phrase p;
                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(2);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell;

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN: " + company.getGSTIN(), reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                String reportdate = "Product Group Reports " + formatdateFrom + " to " + formatdateTo + "";
                taxcell = new PdfPCell(new Phrase(reportdate, reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);
                document.add(taxTable);

                PdfPTable companyTable = new PdfPTable(1);
                companyTable.setWidthPercentage(100);
                PdfPCell cell = null;

                cell = new PdfPCell(new Phrase(company.getCompanyName(), boldFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                if (!company.getAddress().equals("")) {
                    cell = new PdfPCell(new Phrase(company.getAddress().replaceAll("/~/", ","), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                cell = new PdfPCell(new Phrase("City : " + company.getCity(), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                cell = new PdfPCell(new Phrase("State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);
                if (!company.getPhone().equals("")) {
                    cell = new PdfPCell(new Phrase("Phone : " + company.getPhone(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getMobile().equals("")) {
                    cell = new PdfPCell(new Phrase("Mobile : " + company.getMobile(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getEmail().equals("")) {
                    cell = new PdfPCell(new Phrase("Email : " + company.getEmail(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }

                document.add(companyTable);

                if (isProfitEnabled) {
                    PdfPTable thirdTable = new PdfPTable(7);
                    thirdTable.setWidthPercentage(100);
                    thirdTable.setWidths(new float[]{20, 12, 12, 12, 12, 20, 12});

                    taxcell = new PdfPCell(new Phrase("Product Group", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Invoice Value", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("CGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("IGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("SGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Total Amount (Incl. Tax)", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Profit", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    float totalAmount = 0, totalProfit = 0;
                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    for (int rows = 0; rows < jTable1.getRowCount(); rows++) {
                        totalAmount = totalAmount + Float.parseFloat(model.getValueAt(rows, 5).toString());
                        totalProfit = totalProfit + Float.parseFloat(model.getValueAt(rows, 8).toString());
                        for (int cols = 0; cols < jTable1.getColumnCount(); cols++) {
                            String tablevalue = "";
                            if (cols == 1 || cols == 2 || cols == 3 || cols == 4 || cols == 5 || cols == 8) {
                                tablevalue = currency.format(model.getValueAt(rows, cols)).toString();
                                taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                                thirdTable.addCell(taxcell);
                            } else if (cols == 6 || cols == 7) {
                            } else {
                                tablevalue = model.getValueAt(rows, cols).toString();
                                taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                                thirdTable.addCell(taxcell);
                            }
                        }
                    }
                    taxcell = new PdfPCell(new Phrase("Total ", reportdateFont));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                    taxcell.setColspan(5);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(dTotalInvoice), totalFont));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(dTotalProfit), totalFont));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);

                    document.add(thirdTable);

                } else {
                    PdfPTable thirdTable = new PdfPTable(6);
                    thirdTable.setWidthPercentage(100);
                    thirdTable.setWidths(new float[]{20, 15, 15, 15, 15, 20});

                    taxcell = new PdfPCell(new Phrase("Product Group", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Invoice Value", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("CGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("IGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("SGST", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Total Amount (Incl. Tax)", reportdateFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    float totalAmount = 0, totalProfit = 0;
                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    for (int rows = 0; rows < jTable1.getRowCount(); rows++) {
                        totalAmount = totalAmount + Float.parseFloat(model.getValueAt(rows, 5).toString());
                        for (int cols = 0; cols < jTable1.getColumnCount() - 3; cols++) {
                            String tablevalue = "";
                            
                            if (cols == 1 || cols == 2 || cols == 3 || cols == 4 || cols == 5) {
                                tablevalue = currency.format(model.getValueAt(rows, cols)).toString();
                                taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                                thirdTable.addCell(taxcell);
                            } else {
                                tablevalue = model.getValueAt(rows, cols).toString();
                                taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                                thirdTable.addCell(taxcell);
                            }
                        }
                    }
                    taxcell = new PdfPCell(new Phrase("Total ", reportdateFont));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                    taxcell.setColspan(5);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(dTotalInvoice), totalFont));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);

                    document.add(thirdTable);

                }

                document.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);
                        JOptionPane.showMessageDialog(null, "Product Group Report is Printing....... " + path);
                    } catch (PrintException ex) {
                        Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }

                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (FileNotFoundException | DocumentException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }

    private void btnDownloadPdfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDownloadPdfKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            //   pdfReportGeneration(false);
        }
    }//GEN-LAST:event_btnDownloadPdfKeyPressed


    private void btnDownloadExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadExcelActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            try {
                generateExcelReport();
            } catch (WriteException ex) {
                Logger.getLogger(ProductGroupUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProductGroupReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDownloadExcelActionPerformed

    public void generateExcelReport() throws WriteException, IOException {
        try {
            java.util.Date from = dateFrom.getDate();
            java.util.Date to = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(from);
            String formatdateTo = sdf.format(to);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            Boolean isApproved = false;
            String path = "";
            Boolean isPrint = false;
//            String fileName = "Tax Invoice Report " + sdf.format(from) + " to " + sdf.format(to);
//            JFrame parentFrame = new JFrame();

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Product Group Report" + " " + sdf.format(from) + " to " + sdf.format(to) + ".xls";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Report");
                fileChooser.setSelectedFile(new File("Product Group Report" + " " + sdf.format(from) + " to " + sdf.format(to)));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".xls";
                    isApproved = true;
                }
            }

            if (isApproved) {
                File f = new File(path);
                WritableWorkbook myexcel;
                myexcel = Workbook.createWorkbook(f);
                WritableSheet mysheet = myexcel.createSheet("mysheet", 0);
                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);
                WritableCellFormat rupeeFormat = new WritableCellFormat();
                rupeeFormat.setAlignment(Alignment.RIGHT);
                rupeeFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat rightFormat = new WritableCellFormat(wf2);
                rightFormat.setAlignment(Alignment.RIGHT);
                rightFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat centerFormat = new WritableCellFormat(wf2);
                centerFormat.setAlignment(Alignment.CENTRE);
                centerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableFont wf4 = new WritableFont(WritableFont.TIMES);
                mysheet.setColumnView(0, 25);
                mysheet.setColumnView(1, 15);
                mysheet.setColumnView(2, 15);
                mysheet.setColumnView(3, 15);
                mysheet.setColumnView(4, 15);
                mysheet.setColumnView(5, 25);
                mysheet.setColumnView(6, 15);
                //mysheet.setAutobreaks(false);
                WritableCellFormat cellFormat = new WritableCellFormat(wf2);
                cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat cellFormat1 = new WritableCellFormat(wf3);
                cellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
                mysheet.setName("Product Group Report");

                if (isProfitEnabled) {
                    mysheet.addCell(new Label(0, 0, "GSTIN/UIN: " + company.getGSTIN(), cellFormat));
                    mysheet.mergeCells(0, 0, 2, 0);
                    mysheet.addCell(new Label(3, 0, "Product Group Report " + formatdateFrom + " to " + formatdateTo + "", rightFormat));
                    mysheet.mergeCells(3, 0, 6, 0);
                    mysheet.addCell(new Label(0, 1, company.getCompanyName(), centerFormat));
                    mysheet.mergeCells(0, 1, 6, 1);
                    if (!company.getAddress().equals("")) {
                        mysheet.addCell(new Label(0, 2, company.getAddress().replaceAll("/~/", ""), centerFormat));
                        mysheet.mergeCells(0, 2, 6, 2);
                    }
                    mysheet.addCell(new Label(0, 3, "City : " + company.getCity(), centerFormat));
                    mysheet.mergeCells(0, 3, 6, 3);
                    mysheet.addCell(new Label(0, 4, "State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), centerFormat));
                    mysheet.mergeCells(0, 4, 6, 4);
                    mysheet.addCell(new Label(0, 5, "Phone : " + company.getPhone(), centerFormat));
                    mysheet.mergeCells(0, 5, 6, 5);
                    mysheet.addCell(new Label(0, 6, "Mobile : " + company.getMobile(), centerFormat));
                    mysheet.mergeCells(0, 6, 6, 6);
                    mysheet.addCell(new Label(0, 7, "Email : " + company.getEmail(), centerFormat));
                    mysheet.mergeCells(0, 7, 6, 7);
                    mysheet.addCell(new Label(0, 8, "Product Group", cellFormat));
                    mysheet.addCell(new Label(1, 8, "Invoice Value", cellFormat));
                    mysheet.addCell(new Label(2, 8, "CGST", cellFormat));
                    mysheet.addCell(new Label(3, 8, "IGST", cellFormat));
                    mysheet.addCell(new Label(4, 8, "SGST", cellFormat));
                    mysheet.addCell(new Label(5, 8, "Total Amount(Incl Tax)", cellFormat));
                    mysheet.addCell(new Label(6, 8, "Profit", cellFormat));

                    float totalAmt = 0, totalProfit = 0;
                    int totalPostion = 9 + model.getRowCount();
                    

                    for (int i = 0; i < model.getRowCount(); i++) {
                        totalAmt = totalAmt + Float.parseFloat(model.getValueAt(i, 5).toString());
                        totalProfit = totalProfit + Float.parseFloat(model.getValueAt(i, 8).toString());
                        int rowValue = 9 + i;
                        mysheet.addCell(new Label(0, rowValue, jTable1.getValueAt(i, 0).toString(), cellFormat1));
                        mysheet.addCell(new Label(1, rowValue, jTable1.getValueAt(i, 1).toString(), rupeeFormat));
                        mysheet.addCell(new Label(2, rowValue, jTable1.getValueAt(i, 2).toString(), rupeeFormat));
                        mysheet.addCell(new Label(3, rowValue, jTable1.getValueAt(i, 3).toString(), rupeeFormat));
                        mysheet.addCell(new Label(4, rowValue, jTable1.getValueAt(i, 4).toString(), rupeeFormat));
                        String strAmount = "", strProfit = "";
                        strAmount = currency.format(Float.parseFloat(model.getValueAt(i, 5).toString()));
                        strProfit = currency.format(Float.parseFloat(model.getValueAt(i, 8).toString()));
                        mysheet.addCell(new Label(5, rowValue, strAmount, rupeeFormat));
                        mysheet.addCell(new Label(6, rowValue, strProfit, rupeeFormat));
                    }

                    mysheet.addCell(new Label(0, totalPostion, "Total", centerFormat));
                    mysheet.mergeCells(0, totalPostion, 4, totalPostion);
                    mysheet.addCell(new Label(5, totalPostion, currency.format(totalAmt), rightFormat));
                    mysheet.addCell(new Label(6, totalPostion, currency.format(totalProfit), rightFormat));

                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    LocalDateTime now = LocalDateTime.now();
                    String d = dtf.format(now);
                    String date1 = "Report generated as on " + d;
                    int datePosition = totalPostion + 2;
                    mysheet.addCell(new Label(0, datePosition, date1, centerFormat));
                    mysheet.mergeCells(0, datePosition, 6, datePosition + 1);
                    myexcel.write();
                    myexcel.close();
                } else {
                    mysheet.addCell(new Label(0, 0, "GSTIN/UIN: " + company.getGSTIN(), cellFormat));
                    mysheet.mergeCells(0, 0, 2, 0);
                    mysheet.addCell(new Label(3, 0, "Product Group Report " + formatdateFrom + " to " + formatdateTo + "", rightFormat));
                    mysheet.mergeCells(3, 0, 5, 0);
                    mysheet.addCell(new Label(0, 1, company.getCompanyName(), centerFormat));
                    mysheet.mergeCells(0, 1, 5, 1);
                    if (!company.getAddress().equals("")) {
                        mysheet.addCell(new Label(0, 2, company.getAddress().replaceAll("/~/", ""), centerFormat));
                        mysheet.mergeCells(0, 2, 5, 2);
                    }
                    mysheet.addCell(new Label(0, 3, "City : " + company.getCity(), centerFormat));
                    mysheet.mergeCells(0, 3, 5, 3);
                    mysheet.addCell(new Label(0, 4, "State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), centerFormat));
                    mysheet.mergeCells(0, 4, 5, 4);
                    mysheet.addCell(new Label(0, 5, "Phone : " + company.getPhone(), centerFormat));
                    mysheet.mergeCells(0, 5, 5, 5);
                    mysheet.addCell(new Label(0, 6, "Mobile : " + company.getMobile(), centerFormat));
                    mysheet.mergeCells(0, 6, 5, 6);
                    mysheet.addCell(new Label(0, 7, "Email : " + company.getEmail(), centerFormat));
                    mysheet.mergeCells(0, 7, 5, 7);
                    mysheet.addCell(new Label(0, 8, "Product Group", cellFormat));
                    mysheet.addCell(new Label(1, 8, "Invoice Value", cellFormat));
                    mysheet.addCell(new Label(2, 8, "CGST", cellFormat));
                    mysheet.addCell(new Label(3, 8, "IGST", cellFormat));
                    mysheet.addCell(new Label(4, 8, "SGST", cellFormat));
                    mysheet.addCell(new Label(5, 8, "Total Amount(Incl Tax)", cellFormat));

                    float totalAmt = 0;
                    int totalPostion = 9 + model.getRowCount();
                    
                    for (int i = 0; i < model.getRowCount(); i++) {
                        totalAmt = totalAmt + Float.parseFloat(model.getValueAt(i, 5).toString());
                        int rowValue = 9 + i;
                        mysheet.addCell(new Label(0, rowValue, jTable1.getValueAt(i, 0).toString(), cellFormat1));
                        mysheet.addCell(new Label(1, rowValue, jTable1.getValueAt(i, 1).toString(), rupeeFormat));
                        mysheet.addCell(new Label(2, rowValue, jTable1.getValueAt(i, 2).toString(), rupeeFormat));
                        mysheet.addCell(new Label(3, rowValue, jTable1.getValueAt(i, 3).toString(), rupeeFormat));
                        mysheet.addCell(new Label(4, rowValue, jTable1.getValueAt(i, 4).toString(), rupeeFormat));
                        String strAmount = "";
                        strAmount = currency.format(Float.parseFloat(model.getValueAt(i, 5).toString()));
                        mysheet.addCell(new Label(5, rowValue, strAmount, rupeeFormat));
                    }

                    mysheet.addCell(new Label(0, totalPostion, "Total", centerFormat));
                    mysheet.mergeCells(0, totalPostion, 4, totalPostion);
                    mysheet.addCell(new Label(5, totalPostion, currency.format(totalAmt), rightFormat));

                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    LocalDateTime now = LocalDateTime.now();
                    String d = dtf.format(now);
                    String date1 = "Report generated as on " + d;
                    int datePosition = totalPostion + 2;
                    mysheet.addCell(new Label(0, datePosition, date1, centerFormat));
                    mysheet.mergeCells(0, datePosition, 5, datePosition + 1);
                    myexcel.write();
                    myexcel.close();
                }
                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(path);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        // no application registered for PDFs
                    }
                }
            }

            com.alee.laf.WebLookAndFeel.install();
        } catch (WriteException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ProductGroupReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void btnLogout2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout2ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
                        new DashBoardUI().setVisible(true);
                        dispose();
                    } else if (isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardSilverUI().setVisible(true);
                        dispose();
                    } else if (!isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardCopperUI().setVisible(true);
                        dispose();
                    }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void setTable() {
        isProfitEnabled = SessionDataUtil.getCompanyPolicyData().isProfitEnabled();
        if (!isProfitEnabled) {
            jTable1.getColumnModel().getColumn(8).setHeaderValue("");
            jTable1.getColumnModel().getColumn(8).setResizable(false);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
        } else {
            jTable1.getColumnModel().getColumn(8).setHeaderValue("Profit");
            jTable1.getColumnModel().getColumn(8).setResizable(true);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(8).setMinWidth(100);
        }
        if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setHeaderValue("");
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setHeaderValue("");
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
        } else {
            jTable1.getColumnModel().getColumn(6).setResizable(true);
            jTable1.getColumnModel().getColumn(6).setHeaderValue("Return Count");
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(6).setMinWidth(100);
            jTable1.getColumnModel().getColumn(7).setResizable(true);
            jTable1.getColumnModel().getColumn(7).setHeaderValue("Return Value");
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(7).setMinWidth(100);
        }
    }

    public void fetchData() {
        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
        double dTotalAmount = 0;
        double dTotalAmountInclTax = 0;
        double dIGST = 0;
        double dCGST = 0;
        double dSGST = 0;
        double dReturnValue = 0;
        double dProfit = 0;
        dTotalProfit = 0;
        dTotalInvoice = 0;
        int nReturnCount = 0;
        try {
            Boolean isInBetweeFinancialYearStart = new CheckFinancialYear().toCheck(companyPolicy, dateFrom.getDate(), false);
            Boolean isInBetweeFinancialYearEnd = new CheckFinancialYear().toCheck(companyPolicy, dateTo.getDate(), false);
            if (isInBetweeFinancialYearStart && isInBetweeFinancialYearEnd) {
                Map<ProductGroup, SalesReportModel> productGroupLineItem = new ProductGroupLogic().getProductGroupSalesReport(dateFrom.getDate(), dateTo.getDate());
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                if (model.getRowCount() > 0) {
                    model.setRowCount(0);
                }
                if (!productGroupLineItem.isEmpty()) {
                    for (Map.Entry<ProductGroup, SalesReportModel> entry : productGroupLineItem.entrySet()) {
                        ProductGroup pGroup = entry.getKey();
                        SalesReportModel salesReportModel = entry.getValue();
                        if (salesReportModel != null) {
                            if (salesReportModel.getTotalAmount() != null) {
                                dTotalAmount = salesReportModel.getTotalAmount().doubleValue();
                            } else {
                                dTotalAmount = 0;
                            }

                            if (salesReportModel.getIGSTAmt() != null) {
                                dIGST = salesReportModel.getIGSTAmt().doubleValue();
                            }
                            if (salesReportModel.getCGSTAmt() != null) {
                                dCGST = salesReportModel.getCGSTAmt().doubleValue();
                            } else {
                                dCGST = 0;
                            }
                            if (salesReportModel.getSGSTAmt() != null) {
                                dSGST = salesReportModel.getSGSTAmt().doubleValue();
                            } else {
                                dSGST = 0;
                            }
                            if (salesReportModel.getReturnsValue() != null) {
                                dReturnValue = salesReportModel.getReturnsValue().doubleValue();
                            } else {
                                dReturnValue = 0;
                            }
                            if (salesReportModel.getReturnsCount() != 0) {
                                nReturnCount = salesReportModel.getReturnsCount();
                            }
                            if (salesReportModel.getProfit() != null) {
                                dProfit = salesReportModel.getProfit().doubleValue();
                            } else {
                                dProfit = 0;
                            }
                            if (salesReportModel.getTotalAmountIncOfTax() != null) {
                                dTotalAmountInclTax = salesReportModel.getTotalAmountIncOfTax().doubleValue();
                            } else {
                                dTotalAmountInclTax = dTotalAmount + dCGST + dSGST + dIGST;
                            }
                            Object[] row = {
                                pGroup.getProductGroupName(),
                                dTotalAmount,
                                dCGST,
                                dIGST,
                                dSGST,
                                dTotalAmountInclTax,
                                nReturnCount,
                                dReturnValue,
                                dProfit
                            };
                            model.addRow(row);
                            dTotalInvoice = dTotalInvoice + dTotalAmountInclTax;
                            dTotalProfit = dTotalProfit + dProfit;

                        }
                    }
                    if (isProfitEnabled) {
                        labelTotal.setText("Total Amount = " + currency.format(dTotalInvoice) + ", Total Profit = " + currency.format(dTotalProfit));
                    } else {
                        labelTotal.setText("Total Amount = " + currency.format(dTotalInvoice));
                    }
                }
                jTable1.getColumnModel().getColumn(1).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(4).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(5).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(7).setCellRenderer(new NumberTableCellRenderer());
                jTable1.getColumnModel().getColumn(8).setCellRenderer(new NumberTableCellRenderer());
            } else {
                dateFrom.setDate(new Date());
                dateTo.setDate(new Date());
                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                        + " to <b>" + strFinancialYearEnd + "</b></html>";
                JLabel label = new JLabel(msg);
                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception ex) {
            Logger.getLogger(BillOfSupplyProductDateReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProductGroupReport.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProductGroupReport.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProductGroupReport.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProductGroupReport.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProductGroupReport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDownloadExcel;
    private javax.swing.JButton btnDownloadPdf;
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout2;
    private javax.swing.JButton btnPrintReport;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    // End of variables declaration//GEN-END:variables
}
