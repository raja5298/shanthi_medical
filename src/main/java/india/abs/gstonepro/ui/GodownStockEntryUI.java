/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.sun.glass.events.KeyEvent;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ABS
 */
public class GodownStockEntryUI extends javax.swing.JFrame {

    /**
     * Creates new form GodownStockEntryUI
     */
    Product product = new Product();
    Long productId, godownStockDetailId, godownId;
    Company company = SessionDataUtil.getSelectedCompany();
    Long companyId = company.getCompanyId();
    List<Product> newproduct = new ProductLogic().fetchAllProducts(companyId);
    GoDownStockDetail gsd = new GoDownStockDetail();
    int uqc2Value;
    String uqc1, uqc2, productName;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public GodownStockEntryUI(Long productId, String productName, int UQC2Value, String UQC1, String UQC2) {
//        try {
        this.productId = productId;
        this.uqc2Value = UQC2Value;
        this.uqc1 = UQC1;
        this.uqc2 = UQC2;
        this.productName = productName;
        initComponents();
        //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
        labelCompanyName.setText(company.getCompanyName());
        fetch();
        labelUQC1.setText(uqc1);
        labelUQC2.setText(uqc2);
        dateStock.setDate(new Date());
        setGodown();
        comboGodown.requestFocus();
        btnUpdate.setEnabled(false);

        labelProductName.setText(productName);
        labelUQC1fromProduct.setText(uqc1);
        labelUQC2fromProduct.setText(uqc2);
        labelUQC2ValuefromProduct1.setText(String.valueOf(uqc2Value));
        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
//        } catch (IOException ex) {
//            Logger.getLogger(GodownStockEntryUI.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    private GodownStockEntryUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setGodown() {
        List<GoDown> goDowns = new GoDownLogic().fetchAllGodowns(companyId);
        for (GoDown goDown : goDowns) {
            comboGodown.addItem(goDown.getGoDownName());
        }
    }

    public void fetch() {
        List<GoDownStockDetail> goDownStockDetails = new GoDownLogic().fetchAllOpeningStocks(productId);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (GoDownStockDetail goDownStockDetail : goDownStockDetails) {
            godownStockDetailId = goDownStockDetail.getGoDownStockDetailId();
            Object[] row = {
                goDownStockDetail.getGodown().getGoDownName(),
                goDownStockDetail.getOpeningStock(),
                sdf.format(goDownStockDetail.getOpeningStockDate()),
                goDownStockDetail.getOpeningStockValue(),
                godownStockDetailId,
                goDownStockDetail.getGodown().getGoDownId()};
            model.addRow(row);
        }
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
    }
//    public void fetch() {
////        System.err.println("#######$$$$$$$$2 " + productId);
////        List<GoDownStockDetail> goDownStockDetails = new GoDownLogic().fetchAllOpeningStocks(productId);
//        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
//        if (model.getRowCount() > 0) {
//            model.setRowCount(0);
//        }
////        for (GoDownStockDetail goDownStockDetail : goDownStockDetails) {
//        Object[] row = {
//            "Primary",
//            100,
//            "23-02-2018",
//            400.00,
//            "BOXES",
//            "BOXES",
//            10};
//        model.addRow(row);
////        }
//    }

    public void goDownDetails(String UQC1, String UQC2, int UQC2Value) {
        uqc2Value = UQC2Value;
//        UQC1 = comboUQC1.getSelectedItem().toString();
//        UQC2 = comboUQC2.getSelectedItem().toString();

    }

    public Long getStock(int num) {
        Long wholePic = null, Pic = null, convertValue;

        if (!txtStockUQC1Value.getText().equals("")) {
            wholePic = Long.parseLong(txtStockUQC1Value.getText());
        } else {
            wholePic = 0L;
        }
        if (!txtStockUQC2Value.getText().equals("")) {
            Pic = Long.parseLong(txtStockUQC2Value.getText());
        } else {
            Pic = 0L;
        }
        convertValue = (num * wholePic) + Pic;
        return convertValue;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        comboGodown = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtStockUQC1Value = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        txtStockUQC2Value = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        dateStock = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtStockAmount = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        btnCancel = new javax.swing.JButton();
        labelProductName = new javax.swing.JLabel();
        labelUQC1fromProduct = new javax.swing.JLabel();
        labelUQC2ValuefromProduct1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelUQC2fromProduct = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();

        jLabel5.setText("jLabel5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Godown Stock Entry");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Godown", "Opening Stock ", "Opening Date", "Stock Value", "Godown Stock Detail ID", "Godown Id"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        comboGodown.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGodownKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("Godown");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Opening Stock");

        txtStockUQC1Value.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStockUQC1ValueActionPerformed(evt);
            }
        });
        txtStockUQC1Value.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockUQC1ValueKeyTyped(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setText("UQC1");

        txtStockUQC2Value.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStockUQC2ValueActionPerformed(evt);
            }
        });
        txtStockUQC2Value.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockUQC2ValueKeyTyped(evt);
            }
        });

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setText("UQC2");

        dateStock.setDateFormatString("dd-MM-yyyy");
        dateStock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateStockKeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Date");

        jLabel23.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel23.setText("Amount");

        txtStockAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStockAmountActionPerformed(evt);
            }
        });
        txtStockAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockAmountKeyTyped(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setText("GODOWN STOCK ENTRY");

        btnCancel.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        labelProductName.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelProductName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelProductName.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        labelUQC1fromProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelUQC1fromProduct.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        labelUQC2ValuefromProduct1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelUQC2ValuefromProduct1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jLabel4.setText("=");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jLabel6.setText("1");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        labelUQC2fromProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelUQC2fromProduct.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCompanyName))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 624, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(labelProductName, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(33, 33, 33)
                                                .addComponent(labelUQC1fromProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(339, 339, 339)
                                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC2ValuefromProduct1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(txtStockUQC1Value, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(labelUQC1)
                                                .addGap(14, 14, 14)
                                                .addComponent(txtStockUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(labelUQC2)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(dateStock, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel15))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtStockAmount)
                                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(labelUQC2fromProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancel, btnUpdate});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(labelCompanyName))
                .addGap(1, 1, 1)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(labelUQC2ValuefromProduct1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelUQC1fromProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelProductName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelUQC2fromProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStockUQC1Value, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC1)
                            .addComponent(txtStockUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC2)))
                    .addComponent(txtStockAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dateStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtStockUQC1ValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStockUQC1ValueActionPerformed
        txtStockUQC2Value.requestFocus();
    }//GEN-LAST:event_txtStockUQC1ValueActionPerformed

    private void txtStockUQC1ValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockUQC1ValueKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtStockUQC1ValueKeyTyped

    private void txtStockUQC2ValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStockUQC2ValueActionPerformed
        txtStockAmount.requestFocus();
    }//GEN-LAST:event_txtStockUQC2ValueActionPerformed

    private void txtStockUQC2ValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockUQC2ValueKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtStockUQC2ValueKeyTyped

    private void dateStockKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateStockKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            txtStockAmount.requestFocus();
        }
    }//GEN-LAST:event_dateStockKeyPressed

    private void txtStockAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStockAmountActionPerformed
        btnUpdate.requestFocus();
    }//GEN-LAST:event_txtStockAmountActionPerformed

    private void txtStockAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockAmountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

//        if (txtPriceUQC1.getText().equals("")) {
//            dot = false;
//        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtStockAmountKeyTyped

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        Product productDetails = new Product();
        for (int i = 0; i < newproduct.size(); i++) {
            if (productId.equals(newproduct.get(i).getProductId())) {
                productDetails.setProductId(newproduct.get(i).getProductId());
                productDetails.setName(newproduct.get(i).getName());
                productDetails.setTamilName(newproduct.get(i).getTamilName());
                productDetails.setShortName(newproduct.get(i).getShortName());
                productDetails.setHsnCode(newproduct.get(i).getHsnCode());
                productDetails.setIgstPercentage(newproduct.get(i).getIgstPercentage());
                productDetails.setCgstPercentage(newproduct.get(i).getCgstPercentage());
                productDetails.setSgstPercentage(newproduct.get(i).getSgstPercentage());
                productDetails.setBaseProductRate(newproduct.get(i).getBaseProductRate());
                productDetails.setProductRate(newproduct.get(i).getProductRate());
                productDetails.setUQC1(newproduct.get(i).getUQC1());
                productDetails.setUQC2(newproduct.get(i).getUQC2());
                productDetails.setUQC2Value(newproduct.get(i).getUQC2Value());
                productDetails.setProductGroup(newproduct.get(i).getProductGroup());

            }
        }
        gsd.setProduct(productDetails);
        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }
            Long wholeStock = getStock(uqc2Value);
//        List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(companyId);

            GoDown godown = new GoDown();
            godown.setGoDownId((Long) jTable1.getModel().getValueAt(i, 5));
            godown.setCompany(company);
//        godown.setGoDownName(comboGodown.getSelectedItem().toString());
            godown.setGoDownName(jTable1.getModel().getValueAt(i, 0).toString());
            gsd.setGodown(godown);

            gsd.setGoDownStockDetailId((Long) jTable1.getModel().getValueAt(i, 4));
            gsd.setOpeningStockDate(dateStock.getDate());
            gsd.setOpeningStock(wholeStock);
            gsd.setOpeningStockValue(new BigDecimal(txtStockAmount.getText()));

            EventStatus result = new GoDownLogic().updateOpeningStock(gsd);
            if (result.isUpdateDone()) {
                JOptionPane.showMessageDialog(null, "Opening Stock Added", "Message", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select any Godown", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        fetch();
        clear();
//        this.dispose();
    }//GEN-LAST:event_btnUpdateActionPerformed

    public void clear() {
        comboGodown.setSelectedIndex(0);
        txtStockUQC1Value.setText("");
        txtStockUQC2Value.setText("");
        dateStock.setDate(new Date());
        txtStockAmount.setText("");
    }

    private void comboGodownKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGodownKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtStockUQC1Value.requestFocus();
        }
    }//GEN-LAST:event_comboGodownKeyPressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        try {
            int i = jTable1.getSelectedRow();
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }

            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            comboGodown.setSelectedItem(model.getValueAt(i, 0));
            dateStock.setDate(sdf.parse(model.getValueAt(i, 2).toString()));
            txtStockAmount.setText(model.getValueAt(i, 3).toString());
            int OpeningStock = Integer.parseInt(model.getValueAt(i, 1).toString());

            int UQC1Count = OpeningStock / uqc2Value;
            int UQC2Count = OpeningStock % uqc2Value;
            if (labelUQC1.getText() == labelUQC2.getText()) {
                txtStockUQC1Value.setText(String.valueOf(UQC1Count));
                txtStockUQC2Value.setVisible(false);
                labelUQC2.setVisible(false);
            } else {
                txtStockUQC1Value.setText(String.valueOf(UQC1Count));
                txtStockUQC2Value.setText(String.valueOf(UQC2Count));
            }
            btnUpdate.setEnabled(true);
        } catch (ParseException ex) {
            Logger.getLogger(GodownStockEntryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jTable1MouseClicked

    public List getGodownList() {
        return (List) gsd;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GodownStockEntryUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GodownStockEntryUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GodownStockEntryUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GodownStockEntryUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GodownStockEntryUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> comboGodown;
    private com.toedter.calendar.JDateChooser dateStock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelProductName;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1fromProduct;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2ValuefromProduct1;
    private javax.swing.JLabel labelUQC2fromProduct;
    private javax.swing.JTextField txtStockAmount;
    private javax.swing.JTextField txtStockUQC1Value;
    private javax.swing.JTextField txtStockUQC2Value;
    // End of variables declaration//GEN-END:variables
}
