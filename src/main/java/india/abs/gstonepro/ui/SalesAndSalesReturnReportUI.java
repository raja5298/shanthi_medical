/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.toedter.calendar.JDateChooserCellEditor;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CreditDebitNoteLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Admin
 */
public class SalesAndSalesReturnReportUI extends javax.swing.JFrame {

    /**
     * Creates new form SalesAndSalesReturnReportUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    AppUser user = SessionDataUtil.getSelectedUser();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();
    BigDecimal GrandTotal = new BigDecimal(0);
//    BigDecimal totalSaleReturnAmount = new BigDecimal(0);
//    BigDecimal totalSaleAmount = new BigDecimal(0);

    public SalesAndSalesReturnReportUI() {
        try {
            initComponents();
            tableSales1.setDefaultEditor(Date.class, new JDateChooserCellEditor());
            salesSingleBtn.setEnabled(false);
            salesWholeBtn.setEnabled(false);
            salesReturnSingle.setEnabled(false);
            salesReturnWhole.setEnabled(false);
            cdnSingle.setEnabled(false);
            cdnReturnSingle.setEnabled(false);
            cdnWhole.setEnabled(false);
            cdnReturnWhiole.setEnabled(false);
            datePayment.setDate(new Date());
            //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            labelCompanyName.setText(company.getCompanyName());
            labelUserName.setText(user.getUserName());
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });
            tableSales.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            tableSalesReturn.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            this.setExtendedState(this.MAXIMIZED_BOTH);
            setParty();
            comboParty.setSelectedItem("All");
            fetch();
            comboParty.setSelectedItem("All");

            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
            editor1.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboParty.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();
                        scripts.add("All");
                        for (Ledger ledger : sortedLedgers) {
                            if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(ledger.getLedgerName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);

                        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                            comboParty.setPopupVisible(false);
                            comboParty.setSelectedItem(comboParty.getSelectedItem());
                            btnFetch.requestFocus();
                        } else {
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    public void setParty() {
        comboParty.addItem("All");
        for (Ledger ledger : ledgers) {
            comboParty.addItem(ledger.getLedgerName());
        }
        comboParty.setSelectedItem("All");
    }

    public void fetch() {
        String partyName = (String) comboParty.getSelectedItem();
        DefaultTableModel modelSales = (DefaultTableModel) tableSales.getModel();
        DefaultTableModel modelSalesReturn = (DefaultTableModel) tableSalesReturn.getModel();

        DefaultTableModel modelSales1 = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel modelSalesReturn1 = (DefaultTableModel) tableSalesReturn1.getModel();

        if (modelSales.getRowCount() > 0) {
            modelSales.setRowCount(0);
        }
        if (modelSalesReturn.getRowCount() > 0) {
            modelSalesReturn.setRowCount(0);
        }
        if (modelSales1.getRowCount() > 0) {
            modelSales1.setRowCount(0);
        }
        if (modelSalesReturn1.getRowCount() > 0) {
            modelSalesReturn1.setRowCount(0);
        }
        Ledger ledger = null;
        for (Ledger l : ledgers) {
            if (l.getLedgerName().equals(partyName)) {
                ledger = l;
                break;
            }
        }
//        if (ledger != null) {
        List<PurchaseSale> Sales = new PurchaseSaleLogic().getAllPSByLedgerWithoutDate(ledger, "TX-I");
        BigDecimal totalSaleAmount = new BigDecimal(0), totalSalePaidAmount = new BigDecimal(0);
        for (PurchaseSale ps : Sales) {
            if (ps.getCalculatedTotalValue().compareTo(ps.getTotalAmountPaid()) > 0) {
                Object[] row = {ps.getPsDate(), ps.getBillNo(), ps.getCalculatedTotalValue(), ps.getTotalAmountPaid(), ps.getPsId()};
                totalSaleAmount = totalSaleAmount.add(ps.getCalculatedTotalValue());
                totalSalePaidAmount = totalSalePaidAmount.add(ps.getTotalAmountPaid());
                modelSales.addRow(row);
            }
        }
        BigDecimal RemaingSaleAmount = totalSalePaidAmount.subtract(totalSaleAmount);

        if (RemaingSaleAmount.compareTo(BigDecimal.ZERO) < 0) {
        } else if (RemaingSaleAmount.compareTo(BigDecimal.ZERO) > 0) {
            labelRemainingSaleBalance.setText(RemaingSaleAmount.toString() + " Dr");
        } else {
            labelRemainingSaleBalance.setText("0.00 Cr");
        }
        tableSales.getColumnModel().getColumn(0).setCellRenderer(new DateTableCellRenderer());
        tableSales.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
        tableSales.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());

        List<CreditDebitNote> SaleReturns = new CreditDebitNoteLogic().getAllCreditDebitNoteByType("CREDIT-NOTE", ledger);

        BigDecimal totalSRAmount = new BigDecimal(0), totalSRPaidAmount = new BigDecimal(0);

        for (CreditDebitNote cdn : SaleReturns) {
            if (cdn.getNoteValueInclusiveOfTax().compareTo(cdn.getTotalAmountPaid()) > 0) {
                Object[] row = {cdn.getNoteDate(), cdn.getCdNoteNo(), cdn.getNoteValueInclusiveOfTax(), cdn.getTotalAmountPaid(), cdn.getCdNoteId()};
                totalSRAmount = totalSRAmount.add(cdn.getNoteValueInclusiveOfTax());
                totalSRPaidAmount = totalSRPaidAmount.add(cdn.getTotalAmountPaid());
                modelSalesReturn.addRow(row);
            }
        };
        BigDecimal RemaingSRAmount = totalSRAmount.subtract(totalSRPaidAmount);
        tableSalesReturn.getColumnModel().getColumn(0).setCellRenderer(new DateTableCellRenderer());
        tableSalesReturn.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
        tableSalesReturn.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());
        datePayment.setDate(new Date());
        jTextArea.setText("");
        tableSales.requestFocus();
//        }
//        else {
//            JOptionPane.showMessageDialog(null, "Please Select Correct Party", "Alert", JOptionPane.WARNING_MESSAGE);
//        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboParty = new javax.swing.JComboBox<>();
        btnFetch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableSalesReturn = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableSales = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        labelStrSale = new javax.swing.JLabel();
        labelStrSR = new javax.swing.JLabel();
        labelRemainingSaleBalance = new javax.swing.JLabel();
        labelRemainingSRBalance = new javax.swing.JLabel();
        labelFinal = new javax.swing.JLabel();
        labelFinalAmount = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableSales1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableSalesReturn1 = new javax.swing.JTable();
        salesSingleBtn = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        salesReturnSingle = new javax.swing.JButton();
        salesWholeBtn = new javax.swing.JButton();
        salesReturnWhole = new javax.swing.JButton();
        cdnSingle = new javax.swing.JButton();
        cdnReturnSingle = new javax.swing.JButton();
        cdnWhole = new javax.swing.JButton();
        cdnReturnWhiole = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        labelSaleBillTot = new javax.swing.JLabel();
        Cr = new javax.swing.JLabel();
        Cr3 = new javax.swing.JLabel();
        Cr4 = new javax.swing.JLabel();
        lblSaleTotal = new javax.swing.JLabel();
        labelSaleRvdAmt = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblSRBillAmt = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblSRAmtPd = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblSRTotal = new javax.swing.JLabel();
        lblgrandTotal = new javax.swing.JLabel();
        Cr1 = new javax.swing.JLabel();
        Cr6 = new javax.swing.JLabel();
        Cr5 = new javax.swing.JLabel();
        Cr2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jButton9 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        datePayment = new com.toedter.calendar.JDateChooser();
        jLabel12 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sales Payment");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        comboParty.setEditable(true);

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setMnemonic('f');
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        tableSalesReturn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill ID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSalesReturn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesReturnMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableSalesReturn);
        if (tableSalesReturn.getColumnModel().getColumnCount() > 0) {
            tableSalesReturn.getColumnModel().getColumn(0).setResizable(false);
            tableSalesReturn.getColumnModel().getColumn(1).setResizable(false);
            tableSalesReturn.getColumnModel().getColumn(2).setResizable(false);
            tableSalesReturn.getColumnModel().getColumn(3).setResizable(false);
            tableSalesReturn.getColumnModel().getColumn(4).setMinWidth(0);
            tableSalesReturn.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSalesReturn.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("SALES");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("SALES RETURN");

        tableSales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill ID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tableSales);
        if (tableSales.getColumnModel().getColumnCount() > 0) {
            tableSales.getColumnModel().getColumn(0).setResizable(false);
            tableSales.getColumnModel().getColumn(2).setResizable(false);
            tableSales.getColumnModel().getColumn(4).setMinWidth(0);
            tableSales.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSales.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(265, 98));

        labelStrSale.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStrSale.setText("Sale ");

        labelStrSR.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStrSR.setText("Sales Return");

        labelRemainingSaleBalance.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelRemainingSaleBalance.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelRemainingSaleBalance.setText("0.00");

        labelRemainingSRBalance.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelRemainingSRBalance.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelRemainingSRBalance.setText("0.00");

        labelFinal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelFinal.setText("Balance");

        labelFinalAmount.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelFinalAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelFinalAmount.setText("0.00");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelFinal)
                    .addComponent(labelStrSR)
                    .addComponent(labelStrSale))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(labelRemainingSRBalance, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelRemainingSaleBalance, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelFinalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(87, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStrSale)
                    .addComponent(labelRemainingSaleBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStrSR)
                    .addComponent(labelRemainingSRBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelFinal)
                    .addComponent(labelFinalAmount))
                .addContainerGap())
        );

        tableSales1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill ID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSales1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSales1MouseClicked(evt);
            }
        });
        tableSales1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableSales1KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tableSales1);
        if (tableSales1.getColumnModel().getColumnCount() > 0) {
            tableSales1.getColumnModel().getColumn(1).setMinWidth(80);
            tableSales1.getColumnModel().getColumn(1).setPreferredWidth(80);
            tableSales1.getColumnModel().getColumn(1).setMaxWidth(80);
            tableSales1.getColumnModel().getColumn(4).setMinWidth(0);
            tableSales1.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSales1.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        tableSalesReturn1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill ID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSalesReturn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesReturn1MouseClicked(evt);
            }
        });
        tableSalesReturn1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableSalesReturn1KeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tableSalesReturn1);
        if (tableSalesReturn1.getColumnModel().getColumnCount() > 0) {
            tableSalesReturn1.getColumnModel().getColumn(1).setMinWidth(80);
            tableSalesReturn1.getColumnModel().getColumn(1).setPreferredWidth(80);
            tableSalesReturn1.getColumnModel().getColumn(1).setMaxWidth(80);
            tableSalesReturn1.getColumnModel().getColumn(4).setMinWidth(0);
            tableSalesReturn1.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSalesReturn1.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        salesSingleBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        salesSingleBtn.setText(">>");
        salesSingleBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesSingleBtnActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel10.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-");

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        salesReturnSingle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        salesReturnSingle.setText("<<");
        salesReturnSingle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesReturnSingleActionPerformed(evt);
            }
        });

        salesWholeBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        salesWholeBtn.setText(">>|");
        salesWholeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesWholeBtnActionPerformed(evt);
            }
        });

        salesReturnWhole.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        salesReturnWhole.setText("|<<");
        salesReturnWhole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesReturnWholeActionPerformed(evt);
            }
        });

        cdnSingle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cdnSingle.setText(">>");
        cdnSingle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnSingleActionPerformed(evt);
            }
        });

        cdnReturnSingle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cdnReturnSingle.setText("<<");
        cdnReturnSingle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnReturnSingleActionPerformed(evt);
            }
        });

        cdnWhole.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cdnWhole.setText(">>|");
        cdnWhole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnWholeActionPerformed(evt);
            }
        });

        cdnReturnWhiole.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cdnReturnWhiole.setText("|<<");
        cdnReturnWhiole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnReturnWhioleActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Balance ");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Received     ");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Receivable ");

        labelSaleBillTot.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelSaleBillTot.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelSaleBillTot.setText("0.00");

        Cr.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr.setText("Cr");

        Cr3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr3.setText("Cr");

        Cr4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cr4.setText("Cr");

        lblSaleTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblSaleTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSaleTotal.setText("0.00");

        labelSaleRvdAmt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelSaleRvdAmt.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelSaleRvdAmt.setText("0.00");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel5.setText("Total");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Payable      ");

        lblSRBillAmt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblSRBillAmt.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRBillAmt.setText("0.00");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Paid     ");

        lblSRAmtPd.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblSRAmtPd.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRAmtPd.setText("0.00");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Balance ");

        lblSRTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblSRTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRTotal.setText("0.00");

        lblgrandTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblgrandTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblgrandTotal.setText("0.00");

        Cr1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cr1.setText("Dr");

        Cr6.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cr6.setText("Dr");

        Cr5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr5.setText("Dr");

        Cr2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr2.setText("Dr");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton9.setMnemonic('h');
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jTextArea.setColumns(20);
        jTextArea.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jTextArea.setRows(5);
        jScrollPane5.setViewportView(jTextArea);

        jLabel11.setText("Remarks");

        jLabel12.setText("Payment Date");

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator10);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jScrollPane1)
                        .addComponent(jScrollPane3))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(datePayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(345, 345, 345))
                            .addComponent(jScrollPane5)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(salesReturnSingle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(salesSingleBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(salesWholeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(salesReturnWhole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cdnReturnSingle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cdnSingle, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cdnWhole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cdnReturnWhiole, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelSaleBillTot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelSaleRvdAmt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSaleTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Cr3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Cr4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Cr, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblgrandTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cr1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSRAmtPd, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSRBillAmt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSRTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Cr2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Cr5, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(Cr6, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cdnReturnSingle, cdnSingle, salesReturnSingle, salesReturnWhole, salesSingleBtn, salesWholeBtn});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jScrollPane1, jScrollPane2, jScrollPane3, jScrollPane4});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel11, jLabel12});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton9)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(labelCompanyName)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel24)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFetch))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Cr)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Cr3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Cr4))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(labelSaleBillTot)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(labelSaleRvdAmt)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblSaleTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(18, 18, 18)
                            .addComponent(jLabel1)
                            .addGap(4, 4, 4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(73, 73, 73)
                            .addComponent(salesSingleBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(salesReturnSingle)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(salesWholeBtn)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(salesReturnWhole))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cdnSingle, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cdnReturnSingle)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cdnWhole)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cdnReturnWhiole)
                                .addGap(29, 29, 29))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9))
                            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRBillAmt)
                                    .addComponent(Cr2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRAmtPd)
                                    .addComponent(Cr5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRTotal)
                                    .addComponent(Cr6))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblgrandTotal)
                            .addComponent(Cr1)
                            .addComponent(jLabel5))))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(datePayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(85, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        fetch();
    }//GEN-LAST:event_btnFetchActionPerformed

    private void salesSingleBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesSingleBtnActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        int i = tableSales.getSelectedRow();
        int count = tableSales1.getRowCount();
        String getBillNo = tableSales.getValueAt(i, 1).toString();
        Boolean isHere = false;
        for (int j = 0; j < count; j++) {
            if (getBillNo != null && getBillNo.equals(tableSales1.getValueAt(j, 1).toString())) {
                JOptionPane.showMessageDialog(null, "Bill is Already Here", "Alert", JOptionPane.WARNING_MESSAGE);
                isHere = true;
            }
        }
        BigDecimal amountfromSales = new BigDecimal(BigInteger.ZERO);
        BigDecimal paidAmountfromSales = new BigDecimal(BigInteger.ZERO);
        BigDecimal amounttoSales = new BigDecimal(BigInteger.ZERO);
        String amounttoSalesString;
        paidAmountfromSales = new BigDecimal(tableSales.getValueAt(i, 3).toString());
        amountfromSales = new BigDecimal(tableSales.getValueAt(i, 2).toString());
        if ("0.00".equals(tableSales.getValueAt(i, 3).toString())) {
            amounttoSalesString = tableSales.getValueAt(i, 2).toString();
        } else {
            amounttoSales = amountfromSales.subtract(paidAmountfromSales);
            amounttoSalesString = String.valueOf(amounttoSales);
        }

        if (!isHere) {
            Object[] row = {tableSales.getValueAt(i, 0).toString(), tableSales.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSales.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);
//        }
            BigDecimal amountAfterAdd = new BigDecimal(0);
            BigDecimal receivedAfterAdd = new BigDecimal(0);
            BigDecimal balanceAfterAdd = new BigDecimal(0);
            int countAfterInsertRow = tableSales1.getRowCount();
            BigDecimal totSaleAmtLoc = new BigDecimal(0);
            labelSaleBillTot.setText("0.00");
            labelSaleRvdAmt.setText("0.00");
            for (int j = 0; j < countAfterInsertRow; j++) {
                amountAfterAdd = new BigDecimal(labelSaleBillTot.getText()).add(new BigDecimal(tableSales1.getValueAt(j, 2).toString()));
                labelSaleBillTot.setText(String.valueOf(amountAfterAdd));

                receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSales1.getValueAt(j, 3).toString()));
                labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
            }

            balanceAfterAdd = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText()));
            lblSaleTotal.setText(String.valueOf(balanceAfterAdd));
            labelRemainingSaleBalance.setText(String.valueOf(balanceAfterAdd));

            lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));

            if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
                Cr1.setText("Cr");
            } else {
                Cr1.setText("Dr");
                lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
                labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            }
        }
        tableSales.clearSelection();
        salesSingleBtn.setEnabled(false);
        salesWholeBtn.setEnabled(false);

    }//GEN-LAST:event_salesSingleBtnActionPerformed

    private void tableSales1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableSales1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            BigDecimal totSaleAmtPaid = new BigDecimal(0);
            int countAfterInsertRow = tableSales1.getRowCount();
            for (int j = 0; j < countAfterInsertRow; j++) {
                totSaleAmtPaid = totSaleAmtPaid.add(new BigDecimal(tableSales1.getValueAt(j, 3).toString()));
            }
            labelSaleRvdAmt.setText(totSaleAmtPaid.toString());
        }

        lblSaleTotal.setText(new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText())).toString());
        labelRemainingSaleBalance.setText(new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText())).toString());

        GrandTotal = new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()));
        lblgrandTotal.setText(GrandTotal.toString());
        labelFinalAmount.setText(GrandTotal.toString());

        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
    }//GEN-LAST:event_tableSales1KeyPressed

    private void tableSalesReturn1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableSalesReturn1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            BigDecimal totSaleReturnAmtPaid = new BigDecimal(0);
            int countAfterInsertRow = tableSalesReturn1.getRowCount();
            for (int j = 0; j < countAfterInsertRow; j++) {
                totSaleReturnAmtPaid = totSaleReturnAmtPaid.add(new BigDecimal(tableSalesReturn1.getValueAt(j, 3).toString()));
            }
            lblSRAmtPd.setText(totSaleReturnAmtPaid.toString());
        }

        lblSRTotal.setText(new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText())).toString());
        labelRemainingSRBalance.setText(new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText())).toString());

        GrandTotal = new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()));
        lblgrandTotal.setText(GrandTotal.toString());
        labelFinalAmount.setText(GrandTotal.toString());

        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
    }//GEN-LAST:event_tableSalesReturn1KeyPressed

    public void newJournal() throws ParseException {
        List<PurchaseSale> purchasesale = new ArrayList<>();
        List<CreditDebitNote> cdnList = new ArrayList<>();
        DefaultTableModel model = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel model1 = (DefaultTableModel) tableSalesReturn1.getModel();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date;
        for (int i = 0; i < model.getRowCount(); i++) {
            PurchaseSale ps = new PurchaseSale();
            ps.setPsId(model.getValueAt(i, 4).toString());
            ps.setTotalAmountPaid(new BigDecimal(model.getValueAt(i, 3).toString()));
            purchasesale.add(ps);
        }

        for (int i = 0; i < model1.getRowCount(); i++) {
            CreditDebitNote ps1 = new CreditDebitNote();
//            date = sdf.parse(model1.getValueAt(i, 0).toString());;
            ps1.setCdNoteId(model1.getValueAt(i, 4).toString());
            ps1.setTotalAmountPaid(new BigDecimal(model1.getValueAt(i, 3).toString()));
            cdnList.add(ps1);
        }
        Ledger selectedLedger = null;
        String lgName = comboParty.getSelectedItem().toString();
        for (Ledger lg : ledgers) {
            if (lg.getLedgerName().equals(lgName)) {
                selectedLedger = lg;
                break;
            }
        }
        String remarks = jTextArea.getText();
        EventStatus isSuccess = new PurchaseSaleLogic().updatePSPayment(purchasesale, selectedLedger, cdnList, remarks, datePayment.getDate());
        if (isSuccess.isUpdateDone()) {
            JOptionPane.showMessageDialog(null, "Payment is Successfull", "Message", JOptionPane.INFORMATION_MESSAGE);
            fetch();
            resettable();
        } else {
            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void resettable() {
        DefaultTableModel model = (DefaultTableModel) tableSales1.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        DefaultTableModel model1 = (DefaultTableModel) tableSalesReturn1.getModel();
        int rowCount1 = model1.getRowCount();
        for (int i = rowCount1 - 1; i >= 0; i--) {
            model1.removeRow(i);
        }
    }

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            newJournal();
        } catch (ParseException ex) {
            Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
        }
        dispose();
    }

    // private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {                                                  
    //     new SetCompanyUI().setVisible(true);
    //     dispose();
    // }                                                 
    // private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {                                          
    //     new PartyDetailsUI().setVisibmenuBankEntryActionPerformedle(true);
    //     dispose();
    // }                                         
    // private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {                                                 
    //     new ProductGroupUI().setVisible(true);
    //     dispose();
    // }                                                
    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }

    // private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {                                            
    //     new ServiceUI().setVisible(true);
    //     dispose();
    // }
    // private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {                                                  
    //     new PricingPolicyUI().setVisible(true);
    //     dispose();
    // }
    // private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {                                            
    //     new SalesB2BUI().setVisible(true);
    //     dispose();
    // }
    // private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {                                            
    //     new SalesB2CUI().setVisible(true);
    //     dispose();
    // }
    // private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {                                             
    //     new EstimateUI().setVisible(true);
    //     dispose();
    // }
    // private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {                                               
    //     new CreditNoteUI().setVisible(true);
    //     dispose();
    // }                                              
    // private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {                                             
    //     new PurchaseUI().setVisible(true);
    //     dispose();
    // }                                            
    // private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {                                              
    //     new DebitNoteUI().setVisible(true);
    //     dispose();
    // }                                             
    // private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {                                             
    //     new AddStockUI().setVisible(true);
    //     dispose();
    // }                                            
    // private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {                                                   
    //     new DeductionStockUI().setVisible(true);
    //     dispose();
    // }                                                  
    // private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {                                                 
    //     new OpeningStockUI().setVisible(true);
    //     dispose();
    // }                                                
    // private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {                                           
    //     new LedgerUI().setVisible(true);
    //     dispose();
    // }                                          
    // private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {                                                   
    //     new OpeningBalanceUI().setVisible(true);
    //     dispose();
    // }                                                  
    // private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {                                                 
    //     new JournalEntryUI().setVisible(true);
    //     dispose();
    // }                                                
    // private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {                                              
    //     new CashEntryUI().setVisible(true);
    //     dispose();
    // }                                             
    // private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {                                              
    //     new BankEntryUI().setVisible(true);
    //     dispose();
    // }                                             
    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {
        new ClosingStockUI().setVisible(true);
        dispose();
    }

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {
        new DayBookUI().setVisible(true);
        dispose();
    }

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {
        new CashBookUI().setVisible(true);
        dispose();
    }

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {
        new BankBookUI().setVisible(true);
        dispose();
    }

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {
        new LedgerBookUI().setVisible(true);
        dispose();
    }

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {
        new TrialBalanceUI().setVisible(true);
        dispose();
    }

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {
        new GSTR1UI().setVisible(true);
        dispose();
    }

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {
        new TaxUI().setVisible(true);
        dispose();
    }

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {
        new UQCUI().setVisible(true);
        dispose();
    }

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {
        new CompanyUI().setVisible(true);
        dispose();
    }

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {
        new UserUI().setVisible(true);
        dispose();
    }

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {
        new BackupUI().setVisible(true);
        dispose();
    }

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {
        new ContactUsUI().setVisible(true);
        dispose();
    }

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }

    private void cdnSingleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdnSingleActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();

        int i = tableSalesReturn.getSelectedRow();

        int count = tableSalesReturn1.getRowCount();

        String getBillNo = tableSalesReturn.getValueAt(i, 1).toString();

        Boolean isHere = false;
        for (int j = 0; j < count; j++) {
            if (getBillNo == tableSalesReturn1.getValueAt(j, 1).toString()) {
                JOptionPane.showMessageDialog(null, "Bill is Already Here", "Alert", JOptionPane.WARNING_MESSAGE);
                isHere = true;
            }
        }

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);
        String amounttoSalesString;
        paidAmountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 3).toString());
        amountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 2).toString());
        if (tableSalesReturn.getValueAt(i, 3).toString() == "0.00") {
            amounttoSalesString = tableSalesReturn.getValueAt(i, 2).toString();
        } else {
            amounttoSales = amountfromSales.subtract(paidAmountfromSales);
            amounttoSalesString = String.valueOf(amounttoSales);
        }

        if (!isHere) {
            Object[] row = {tableSalesReturn.getValueAt(i, 0).toString(), tableSalesReturn.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSalesReturn.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            BigDecimal amountAfterAdd = new BigDecimal(0);
            BigDecimal receivedAfterAdd = new BigDecimal(0);
            BigDecimal balanceAfterAdd = new BigDecimal(0);
            int countAfterInsertRow = tableSalesReturn1.getRowCount();
            BigDecimal totSaleAmtLoc = new BigDecimal(0);
            lblSRBillAmt.setText("0.00");
            lblSRBillAmt.setText("0.00");
            for (int j = 0; j < countAfterInsertRow; j++) {
//                totSaleAmtLoc = totSaleAmtLoc.add(new BigDecimal(tableSalesReturn1.getValueAt(j, 2).toString()));
                amountAfterAdd = new BigDecimal(lblSRBillAmt.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(j, 2).toString()));
                lblSRBillAmt.setText(String.valueOf(amountAfterAdd));

                receivedAfterAdd = new BigDecimal(lblSRAmtPd.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(j, 3).toString()));
                lblSRAmtPd.setText(String.valueOf(receivedAfterAdd));
            }

            balanceAfterAdd = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText()));
            lblSRTotal.setText(String.valueOf(balanceAfterAdd));
            labelRemainingSRBalance.setText(String.valueOf(balanceAfterAdd));

            lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
                Cr1.setText("Cr");
            } else {
                Cr1.setText("Dr");
                lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
                labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            }
        }
        tableSalesReturn.clearSelection();
        salesSingleBtn.setEnabled(false);
        salesWholeBtn.setEnabled(false);
    }//GEN-LAST:event_cdnSingleActionPerformed

    private void salesWholeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesWholeBtnActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel salesTable = (DefaultTableModel) tableSales.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        labelSaleBillTot.setText("0.00");
        labelSaleRvdAmt.setText("0.00");
        lblSaleTotal.setText("0.00");
        labelRemainingSaleBalance.setText("0.00");

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);

        BigDecimal amountAfterAdd = new BigDecimal(0);
        BigDecimal receivedAfterAdd = new BigDecimal(0);
        BigDecimal balanceAfterAdd = new BigDecimal(0);
        String amounttoSalesString;

        String getBillIdfromSales = "", getBillIdfromSales1;

        for (int i = 0; i < tableSales.getRowCount(); i++) {
            paidAmountfromSales = new BigDecimal(tableSales.getValueAt(i, 3).toString());
            amountfromSales = new BigDecimal(tableSales.getValueAt(i, 2).toString());
            if (tableSales.getValueAt(i, 3).toString() == "0.00") {
                amounttoSalesString = tableSales.getValueAt(i, 2).toString();

            } else {
                amounttoSales = amountfromSales.subtract(paidAmountfromSales);
                amounttoSalesString = String.valueOf(amounttoSales);
            }

            Object[] row = {tableSales.getValueAt(i, 0).toString(), tableSales.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSales.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            amountAfterAdd = amountAfterAdd.add(new BigDecimal(tableSales1.getValueAt(i, 2).toString()));

            receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSales1.getValueAt(i, 3).toString()));
            labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
        }

        labelSaleBillTot.setText(String.valueOf(amountAfterAdd));
        balanceAfterAdd = new BigDecimal(labelSaleBillTot.getText()).add(new BigDecimal(labelSaleRvdAmt.getText()));
        lblSaleTotal.setText(String.valueOf(balanceAfterAdd));
        labelRemainingSaleBalance.setText(String.valueOf(balanceAfterAdd));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        tableSales.clearSelection();
        salesSingleBtn.setEnabled(false);
        salesWholeBtn.setEnabled(false);
    }//GEN-LAST:event_salesWholeBtnActionPerformed

    private void salesReturnSingleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesReturnSingleActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        int i = tableSales1.getSelectedRow();

        BigDecimal amountAfterDelete = new BigDecimal(0);
        BigDecimal receivedAfterDelete = new BigDecimal(0);
        BigDecimal balanceAfterDelete = new BigDecimal(0);

        amountAfterDelete = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(tableSales1.getValueAt(i, 2).toString()));
        labelSaleBillTot.setText(String.valueOf(amountAfterDelete));

        receivedAfterDelete = new BigDecimal(labelSaleRvdAmt.getText()).subtract(new BigDecimal(tableSales1.getValueAt(i, 3).toString()));
        labelSaleRvdAmt.setText(String.valueOf(receivedAfterDelete));

        balanceAfterDelete = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText()));
        lblSaleTotal.setText(String.valueOf(balanceAfterDelete));
        labelRemainingSaleBalance.setText(String.valueOf(balanceAfterDelete));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        salesTable1.removeRow(i);
        tableSales1.clearSelection();
        salesReturnSingle.setEnabled(false);
        salesReturnWhole.setEnabled(false);
    }//GEN-LAST:event_salesReturnSingleActionPerformed

    private void tableSalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesMouseClicked
        salesSingleBtn.setEnabled(true);
        salesWholeBtn.setEnabled(true);
        salesReturnSingle.setEnabled(false);
        salesReturnWhole.setEnabled(false);
    }//GEN-LAST:event_tableSalesMouseClicked

    private void tableSales1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSales1MouseClicked
        salesSingleBtn.setEnabled(false);
        salesWholeBtn.setEnabled(false);
        salesReturnSingle.setEnabled(true);
        salesReturnWhole.setEnabled(true);
    }//GEN-LAST:event_tableSales1MouseClicked

    private void salesReturnWholeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesReturnWholeActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        labelSaleBillTot.setText("0.00");
        labelSaleRvdAmt.setText("0.00");
        lblSaleTotal.setText("0.00");
        labelRemainingSaleBalance.setText("0.00");
        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        tableSales1.clearSelection();
        salesReturnSingle.setEnabled(false);
        salesReturnWhole.setEnabled(false);
    }//GEN-LAST:event_salesReturnWholeActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        tableSales.clearSelection();
        tableSales1.clearSelection();
        salesReturnSingle.setEnabled(false);
        salesReturnWhole.setEnabled(false);
        salesSingleBtn.setEnabled(false);
        salesWholeBtn.setEnabled(false);

        tableSalesReturn.clearSelection();
        tableSalesReturn1.clearSelection();
        cdnSingle.setEnabled(false);
        cdnReturnSingle.setEnabled(false);
        cdnWhole.setEnabled(false);
        cdnReturnWhiole.setEnabled(false);
    }//GEN-LAST:event_formMouseClicked

    private void cdnReturnSingleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdnReturnSingleActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        int i = tableSalesReturn1.getSelectedRow();

        BigDecimal amountAfterDelete = new BigDecimal(0);
        BigDecimal receivedAfterDelete = new BigDecimal(0);
        BigDecimal balanceAfterDelete = new BigDecimal(0);

        amountAfterDelete = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(tableSalesReturn1.getValueAt(i, 2).toString()));
        lblSRBillAmt.setText(String.valueOf(amountAfterDelete));

        receivedAfterDelete = new BigDecimal(lblSRAmtPd.getText()).subtract(new BigDecimal(tableSalesReturn1.getValueAt(i, 3).toString()));
        lblSRAmtPd.setText(String.valueOf(receivedAfterDelete));

        balanceAfterDelete = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText()));
        lblSRTotal.setText(String.valueOf(balanceAfterDelete));
        labelRemainingSRBalance.setText(String.valueOf(balanceAfterDelete));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        salesTable1.removeRow(i);
        tableSalesReturn1.clearSelection();
        salesReturnSingle.setEnabled(false);
        salesReturnWhole.setEnabled(false);
    }//GEN-LAST:event_cdnReturnSingleActionPerformed

    private void cdnWholeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdnWholeActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        DefaultTableModel salesTable = (DefaultTableModel) tableSalesReturn.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        lblSRBillAmt.setText("0.00");
        lblSRAmtPd.setText("0.00");
        lblSRTotal.setText("0.00");
        labelRemainingSRBalance.setText("0.00");

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);

        BigDecimal amountAfterAdd = new BigDecimal(0);
        BigDecimal receivedAfterAdd = new BigDecimal(0);
        BigDecimal balanceAfterAdd = new BigDecimal(0);
        String amounttoSalesString;

        String getBillIdfromSales = "", getBillIdfromSales1;

        for (int i = 0; i < tableSalesReturn.getRowCount(); i++) {
            paidAmountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 3).toString());
            amountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 2).toString());
            if (tableSalesReturn.getValueAt(i, 3).toString() == "0.00") {
                amounttoSalesString = tableSalesReturn.getValueAt(i, 2).toString();

            } else {
                amounttoSales = amountfromSales.subtract(paidAmountfromSales);
                amounttoSalesString = String.valueOf(amounttoSales);
            }

            Object[] row = {tableSalesReturn.getValueAt(i, 0).toString(), tableSalesReturn.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSalesReturn.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            amountAfterAdd = amountAfterAdd.add(new BigDecimal(tableSalesReturn1.getValueAt(i, 2).toString()));

            receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(i, 3).toString()));
            labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
        }

        lblSRBillAmt.setText(String.valueOf(amountAfterAdd));
        balanceAfterAdd = new BigDecimal(lblSRBillAmt.getText()).add(new BigDecimal(lblSRAmtPd.getText()));
        lblSRTotal.setText(String.valueOf(balanceAfterAdd));
        labelRemainingSRBalance.setText(String.valueOf(balanceAfterAdd));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        tableSalesReturn.clearSelection();
        cdnSingle.setEnabled(false);
        cdnWhole.setEnabled(false);
    }//GEN-LAST:event_cdnWholeActionPerformed

    private void cdnReturnWhioleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdnReturnWhioleActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblSRBillAmt.setText("0.00");
        lblSRAmtPd.setText("0.00");
        lblSRTotal.setText("0.00");
        labelRemainingSRBalance.setText("0.00");
        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        tableSalesReturn1.clearSelection();
        cdnReturnSingle.setEnabled(false);
        cdnReturnWhiole.setEnabled(false);
    }//GEN-LAST:event_cdnReturnWhioleActionPerformed

    private void tableSalesReturnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesReturnMouseClicked
        cdnSingle.setEnabled(true);
        cdnReturnSingle.setEnabled(false);
        cdnWhole.setEnabled(true);
        cdnReturnWhiole.setEnabled(false);
    }//GEN-LAST:event_tableSalesReturnMouseClicked

    private void tableSalesReturn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesReturn1MouseClicked
        cdnSingle.setEnabled(false);
        cdnReturnSingle.setEnabled(true);
        cdnWhole.setEnabled(false);
        cdnReturnWhiole.setEnabled(true);
    }//GEN-LAST:event_tableSalesReturn1MouseClicked

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    // private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
    //     new DashBoardUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    // private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed
    //     //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
    //         //            new ProductUI().setVisible(true);
    //         //            dispose();
    //         //        } else {
    //         new ProductUI().setVisible(true);
    //         dispose();
    //         //        }
    // }//GEN-LAST:event_menuProductActionPerformed
    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {
        new ServiceUI().setVisible(true);
        dispose();
    }

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {
        new PricingPolicyUI().setVisible(true);
        dispose();
    }

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {
        new SalesB2BUI().setVisible(true);
        dispose();
    }

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {
        new SalesB2CUI().setVisible(true);
        dispose();
    }

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {
        new EstimateUI().setVisible(true);
        dispose();
    }

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    // private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
    //     new TaxInvoiceReportsGroupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    // private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
    //     new EstimateReportsGroupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    // private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
    //     new CreditNoteReportsGroupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    // private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
    //     new PurchaseReportGroupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    // private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
    //     new DebitNoteReportsGroupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    // private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
    //     new CurrentStockReportwithGodownUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuCurrentStockActionPerformed

    // private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
    //     new ClosingStockUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    // private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
    //     new DayBookUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuDayBookActionPerformed

    // private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
    //     new CashBookUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuCashBookActionPerformed

    // private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
    //     new BankBookUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuBankBookActionPerformed

    // private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
    //     new LedgerBookUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuLedgerBookActionPerformed

    // private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
    //     new LedgerGroupTrialBalanceUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    // private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
    //     new TrialBalanceUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    // private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
    //     new GSTR1UI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    // private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
    //     new TaxUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuTaxActionPerformed

    // private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
    //     new UQCUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuUQCActionPerformed

    // private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
    //     new CompanyUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuCompanyActionPerformed

    // private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
    //     new UserUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuUsersActionPerformed

    // private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
    //     new BackupUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuMaintenanceActionPerformed

    // private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
    //     if (isAccountEnabled) {
    //         new KeyBoardShortcutsGOLDUI().setVisible(true);
    //         dispose();
    //     }
    //     else if (isInventoryEnabled) {
    //         new KeyBoardShortcutsSILVERUI().setVisible(true);
    //         dispose();
    //     }
    //     else {
    //         new KeyBoardShortcutsCOPPERUI().setVisible(true);
    //         dispose();
    //     }
    // }//GEN-LAST:event_menuHelpActionPerformed

    // private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
    //     new ContactUsUI().setVisible(true);
    //     dispose();
    // }//GEN-LAST:event_menuContactUsActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SalesAndSalesReturnReportUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Cr;
    private javax.swing.JLabel Cr1;
    private javax.swing.JLabel Cr2;
    private javax.swing.JLabel Cr3;
    private javax.swing.JLabel Cr4;
    private javax.swing.JLabel Cr5;
    private javax.swing.JLabel Cr6;
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton cdnReturnSingle;
    private javax.swing.JButton cdnReturnWhiole;
    private javax.swing.JButton cdnSingle;
    private javax.swing.JButton cdnWhole;
    private javax.swing.JComboBox<String> comboParty;
    private com.toedter.calendar.JDateChooser datePayment;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextArea jTextArea;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelFinal;
    private javax.swing.JLabel labelFinalAmount;
    private javax.swing.JLabel labelRemainingSRBalance;
    private javax.swing.JLabel labelRemainingSaleBalance;
    private javax.swing.JLabel labelSaleBillTot;
    private javax.swing.JLabel labelSaleRvdAmt;
    private javax.swing.JLabel labelStrSR;
    private javax.swing.JLabel labelStrSale;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lblSRAmtPd;
    private javax.swing.JLabel lblSRBillAmt;
    private javax.swing.JLabel lblSRTotal;
    private javax.swing.JLabel lblSaleTotal;
    private javax.swing.JLabel lblgrandTotal;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JButton salesReturnSingle;
    private javax.swing.JButton salesReturnWhole;
    private javax.swing.JButton salesSingleBtn;
    private javax.swing.JButton salesWholeBtn;
    private javax.swing.JTable tableSales;
    private javax.swing.JTable tableSales1;
    private javax.swing.JTable tableSalesReturn;
    private javax.swing.JTable tableSalesReturn1;
    // End of variables declaration//GEN-END:variables
}
