/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.BarcodePDF417;
import com.itextpdf.text.pdf.PdfPCell;
import com.sun.glass.events.KeyEvent;
import india.abs.gstonepro.api.dao.GoDownCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CompanyUserRoleLogic;
import india.abs.gstonepro.business.ProductGroupLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.TaxLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.pdf.CreateBarcodePdf;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.RowFilter;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.poi.EncryptedDocumentException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author ABS
 */
public class ProductUI extends javax.swing.JFrame {

    /**
     * Creates new form ProductUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    AppUser user = SessionDataUtil.getSelectedUser();
    Long companyId = company.getCompanyId();
    CompanyUserRole companyUserRole = new CompanyUserRoleLogic().getCompanyUserRole(company, user);
//    GodownStockEntryUI godownStockEntryUI = new GodownStockEntryUI();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    ProductDescriptionUI productDescriptionUI = new ProductDescriptionUI();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isProfitEnable = company.getCompanyPolicy().isProfitEnabled();
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isService = false;
    String strSecondValue;
    GoDownCRUD g4 = new GoDownCRUD();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);

    public ProductUI() {
//        try {
        initComponents();
        //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        btnProductDescription.setEnabled(false);
        setMenuRoles();
        txtSearch.requestFocus();
        txtUQC2Value.setText("1");
        txtUQC2Value.setEnabled(false);
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        setExtendedState(ProductUI.MAXIMIZED_BOTH);
        labelCompanyName.setText(company.getCompanyName());
        labelUserName.setText(user.getUserName());
        labelBasePerUQC1.setVisible(isProfitEnable);
        labelBasePerUQC2.setVisible(isProfitEnable);
        txtBasePriceUQC1.setVisible(isProfitEnable);
        txtBasePriceUQC2.setVisible(isProfitEnable);
        labelBasePrice.setVisible(isProfitEnable);
        checkBasePriceTaxInclusive.setVisible(isProfitEnable);
        checkBasePriceTaxExclusive.setVisible(isProfitEnable);
        checkBasePriceTaxExclusive.setSelected(true);
        checkTaxExclusive.setSelected(true);
        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        Font tamilFont = new Font("Baamini", Font.PLAIN, 12);
        txtTamilName.setFont(tamilFont);
        setGroup();
        setUQCvalue();
        setTaxValue();
        fetch();
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());

    }

    private void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    private void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    private void fetch() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }

        for (Product product : products) {
            ProductStockEntry productStock = new ProductLogic().getOpeningStock(product.getProductId());
            String ProductValue = setStockConverstion(productStock.getQuantity(), product.getUQC1(), product.getUQC2(), product.getUQC2Value());
            String productGroupName;
            if (product.getProductGroup() == null) {
                productGroupName = null;
            } else {
                productGroupName = product.getProductGroup().getProductGroupName();
            }
            if (!product.isService()) {
                String name = product.getTamilName();
                String tamilName = new String(name.getBytes(), Charset.forName("UTF-8"));
                Object[] row = {
                    product.getProductId(),
                    product.getName(),
                    tamilName,
                    product.getShortName(),
                    product.getHsnCode(),
                    product.getUQC1(),
                    product.getUQC2Value(),
                    product.getUQC2(),
                    "1" + product.getUQC1() + "=" + product.getUQC2Value() + " " + product.getUQC2(),
                    product.getBaseProductRate(),
                    product.isBasePriceInclusiveOfGST(),
                    product.getProductRate(),
                    product.isInclusiveOfGST(),
                    product.getIgstPercentage(),
                    product.getCgstPercentage(),
                    product.getSgstPercentage(),
                    product.isIsOpeningStockEditable(),
                    productStock.getStockUpdateDate(),
                    productStock.getQuantity(),
                    ProductValue,
                    productStock.getStockValue(),
                    productStock.getIgstPercentage(),
                    productStock.getIgstAmount(),
                    productStock.getCgstPercentage(),
                    productStock.getCgstAmount(),
                    productStock.getSgstPercentage(),
                    productStock.getSgstAmount(),
                    productStock.getProductStockEntryId(),
                    product.isNillRated(),
                    product.isExempted(),
                    product.isNonGST(),
                    product.isService(),
                    productGroupName,
                    product.getProductCode()
                };
                model.addRow(row);
            }
        }

        jTable1.getColumnModel().getColumn(11).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(20).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(22).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(24).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(26).setCellRenderer(new NumberTableCellRenderer());
        if (isProfitEnable) {
            jTable1.getColumnModel().getColumn(9).setCellRenderer(new NumberTableCellRenderer());
        } else {
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
        }
        searchClear();
    }

    private void setGroup() {
        List<ProductGroup> productGroups = new ProductGroupLogic().fetchAllPG();
        for (ProductGroup productGroup : productGroups) {
            comboGroup.addItem(productGroup.getProductGroupName());
        }
    }

    private void setUQCvalue() {
        List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
        for (UQC uqc : UQCs) {
            comboUQC1.addItem(uqc.getQuantityName());
            comboUQC2.addItem(uqc.getQuantityName());
        }
    }

    public String setStockConverstion(int qty, String UQC1, String UQC2, int UQC2Value) {
        int UQC1Count = qty / UQC2Value;
        int UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    private void setTaxValue() {
        List<Tax> taxes = new ArrayList<Tax>();
        taxes = new TaxLogic().fetchAllTaxes();
        Collections.sort(taxes, new Comparator<Tax>() {
            @Override
            public int compare(Tax o1, Tax o2) {
                return o1.getTaxRate() < o2.getTaxRate() ? -1 : (o1.getTaxRate() > o2.getTaxRate()) ? 1 : 0;
            }
        });
        for (Tax tax : taxes) {
            String s = Float.toString(tax.getTaxRate());
            comboCGST.addItem(s);
            comboIGST.addItem(s);
            comboSGST.addItem(s);

//            comboStockIGST.addItem(s);
//            comboStockCGST.addItem(s);
//            comboStockSGST.addItem(s);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem3 = new javax.swing.JMenuItem();
        dialogBarcode = new javax.swing.JDialog();
        jLabel15 = new javax.swing.JLabel();
        txtBarcodeCount = new javax.swing.JTextField();
        btnDownloadBarcode = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtTamilName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtHsncode = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        txtCode = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        comboGroup = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        CheckExempted = new javax.swing.JCheckBox();
        ChecknonGst = new javax.swing.JCheckBox();
        comboIGST = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        comboCGST = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        comboSGST = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        comboUQC1 = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtUQC2Value = new javax.swing.JTextField();
        comboUQC2 = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        txtBasePriceUQC2 = new javax.swing.JTextField();
        labelBasePerUQC2 = new javax.swing.JLabel();
        txtBasePriceUQC1 = new javax.swing.JTextField();
        labelBasePerUQC1 = new javax.swing.JLabel();
        labelBasePrice = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPriceUQC1 = new javax.swing.JTextField();
        txtPriceUQC2 = new javax.swing.JTextField();
        labelPerUQC1 = new javax.swing.JLabel();
        labelPerUQC2 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        checkBasePriceTaxExclusive = new javax.swing.JCheckBox();
        checkTaxExclusive = new javax.swing.JCheckBox();
        checkBasePriceTaxInclusive = new javax.swing.JCheckBox();
        checkTaxInclusive = new javax.swing.JCheckBox();
        txtShortName = new javax.swing.JTextField();
        btnProductDescription = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnExcelReport = new javax.swing.JButton();
        btnExcelReport1 = new javax.swing.JButton();
        btnExcelReport2 = new javax.swing.JButton();
        btnGenerate = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        jMenuItem3.setText("jMenuItem3");

        dialogBarcode.setTitle("Barcode Generation");
        dialogBarcode.setFocusTraversalPolicyProvider(true);
        dialogBarcode.setPreferredSize(new java.awt.Dimension(298, 274));
        dialogBarcode.setResizable(false);

        jLabel15.setText("Enter the Count");

        txtBarcodeCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBarcodeCountActionPerformed(evt);
            }
        });
        txtBarcodeCount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBarcodeCountKeyTyped(evt);
            }
        });

        btnDownloadBarcode.setText("Download");
        btnDownloadBarcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadBarcodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dialogBarcodeLayout = new javax.swing.GroupLayout(dialogBarcode.getContentPane());
        dialogBarcode.getContentPane().setLayout(dialogBarcodeLayout);
        dialogBarcodeLayout.setHorizontalGroup(
            dialogBarcodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogBarcodeLayout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addGroup(dialogBarcodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addGroup(dialogBarcodeLayout.createSequentialGroup()
                        .addComponent(txtBarcodeCount, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDownloadBarcode)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        dialogBarcodeLayout.setVerticalGroup(
            dialogBarcodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogBarcodeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dialogBarcodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBarcodeCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownloadBarcode))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        dialogBarcode.getAccessibleContext().setAccessibleParent(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Product");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Product Name", "Tamil Name", "Short Name", "HSN Code", "UQC1", "UQCValue2", "UQC2", "UQC", "Base Price", "Base Price Tax Inclusive", "Price", "Tax Inclusive", "IGST%", "CGST%", "SGST%", "isEditable ", "Date", "Quantity", "Opening Balance", "Amount", "Stock IGST %", "IGST Amount", "Stock CGST %", "CGST Amount", "Stock SGST %", "SGST Amount", "ProductStockId", "NillRated", "Exempted", "nonGST", "isService", "Group", "Product Code"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Long.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(0);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(2).setMinWidth(0);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(100);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(200);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(200);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(11).setMinWidth(100);
            jTable1.getColumnModel().getColumn(11).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(11).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(70);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(70);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(70);
            jTable1.getColumnModel().getColumn(14).setMinWidth(0);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(14).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(15).setMinWidth(0);
            jTable1.getColumnModel().getColumn(15).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(15).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(22).setMinWidth(0);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(23).setMinWidth(0);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(25).setMinWidth(0);
            jTable1.getColumnModel().getColumn(25).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(25).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(26).setMinWidth(0);
            jTable1.getColumnModel().getColumn(26).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(26).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(27).setMinWidth(0);
            jTable1.getColumnModel().getColumn(27).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(27).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(28).setMinWidth(0);
            jTable1.getColumnModel().getColumn(28).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(28).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(29).setMinWidth(0);
            jTable1.getColumnModel().getColumn(29).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(29).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(30).setMinWidth(0);
            jTable1.getColumnModel().getColumn(30).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(30).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(31).setMinWidth(0);
            jTable1.getColumnModel().getColumn(31).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(31).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(32).setMinWidth(0);
            jTable1.getColumnModel().getColumn(32).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(32).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(33).setMinWidth(100);
            jTable1.getColumnModel().getColumn(33).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(33).setMaxWidth(100);
        }

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N

        txtSearch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel4.setText("Product Name");

        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        txtTamilName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTamilNameActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Short Name");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("HSN Code");

        txtHsncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHsncodeActionPerformed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel26.setText("Product Code");

        txtCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodeActionPerformed(evt);
            }
        });
        txtCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodeKeyTyped(evt);
            }
        });

        jLabel27.setText("Product Group");

        comboGroup.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGroupKeyPressed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Product Name in Regional Language (Optional)");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        CheckExempted.setText("Exempted");
        CheckExempted.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckExemptedActionPerformed(evt);
            }
        });

        ChecknonGst.setText("nonGST");
        ChecknonGst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChecknonGstActionPerformed(evt);
            }
        });

        comboIGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboIGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboIGSTFocusGained(evt);
            }
        });
        comboIGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboIGSTActionPerformed(evt);
            }
        });
        comboIGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboIGSTKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel11.setText("IGST %");

        comboCGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboCGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboCGSTFocusGained(evt);
            }
        });
        comboCGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCGSTActionPerformed(evt);
            }
        });
        comboCGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboCGSTKeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("CGST %");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("SGST %");

        comboSGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboSGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboSGSTFocusGained(evt);
            }
        });
        comboSGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSGSTActionPerformed(evt);
            }
        });
        comboSGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboSGSTKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChecknonGst)
                            .addComponent(CheckExempted))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CheckExempted)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChecknonGst)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("1");

        comboUQC1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboUQC1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUQC1FocusGained(evt);
            }
        });
        comboUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUQC1ActionPerformed(evt);
            }
        });
        comboUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboUQC1KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel9.setText("UQC Conversion");

        jLabel8.setText("=");

        txtUQC2Value.setText("1");
        txtUQC2Value.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2ValueActionPerformed(evt);
            }
        });
        txtUQC2Value.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2ValueKeyTyped(evt);
            }
        });

        comboUQC2.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboUQC2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUQC2FocusGained(evt);
            }
        });
        comboUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUQC2ActionPerformed(evt);
            }
        });
        comboUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboUQC2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(comboUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtBasePriceUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasePriceUQC2ActionPerformed(evt);
            }
        });
        txtBasePriceUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC2KeyTyped(evt);
            }
        });

        labelBasePerUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePerUQC2.setText("per UQC2");

        txtBasePriceUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasePriceUQC1ActionPerformed(evt);
            }
        });
        txtBasePriceUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyTyped(evt);
            }
        });

        labelBasePerUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePerUQC1.setText("per UQC1");

        labelBasePrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePrice.setText("Base Price");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("Price");

        txtPriceUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUQC1ActionPerformed(evt);
            }
        });
        txtPriceUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyTyped(evt);
            }
        });

        txtPriceUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUQC2ActionPerformed(evt);
            }
        });
        txtPriceUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPriceUQC2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceUQC2KeyTyped(evt);
            }
        });

        labelPerUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelPerUQC1.setText("per UQC1");

        labelPerUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelPerUQC2.setText("per UQC2");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        checkBasePriceTaxExclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkBasePriceTaxExclusive.setText("Tax Exclusive");
        checkBasePriceTaxExclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkBasePriceTaxExclusiveItemStateChanged(evt);
            }
        });
        checkBasePriceTaxExclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBasePriceTaxExclusiveActionPerformed(evt);
            }
        });
        checkBasePriceTaxExclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxExclusiveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxExclusiveKeyReleased(evt);
            }
        });

        checkTaxExclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkTaxExclusive.setText("Tax Exclusive");
        checkTaxExclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxExclusiveItemStateChanged(evt);
            }
        });
        checkTaxExclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTaxExclusiveActionPerformed(evt);
            }
        });
        checkTaxExclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxExclusiveKeyPressed(evt);
            }
        });

        checkBasePriceTaxInclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkBasePriceTaxInclusive.setText("Tax Inclusive");
        checkBasePriceTaxInclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkBasePriceTaxInclusiveItemStateChanged(evt);
            }
        });
        checkBasePriceTaxInclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxInclusiveKeyPressed(evt);
            }
        });

        checkTaxInclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkTaxInclusive.setText("Tax Inclusive");
        checkTaxInclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxInclusiveItemStateChanged(evt);
            }
        });
        checkTaxInclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTaxInclusiveActionPerformed(evt);
            }
        });
        checkTaxInclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxInclusiveKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtBasePriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBasePerUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelBasePrice)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtBasePriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBasePerUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(checkBasePriceTaxExclusive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(checkBasePriceTaxInclusive)))
                .addGap(18, 18, 18)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtPriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPerUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtPriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPerUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(checkTaxExclusive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(checkTaxInclusive)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(labelBasePrice)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelBasePerUQC1)
                                    .addComponent(txtBasePriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelBasePerUQC2)
                                    .addComponent(txtBasePriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtPriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelPerUQC1))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelPerUQC2)
                                    .addComponent(txtPriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(checkBasePriceTaxExclusive)
                                .addComponent(checkBasePriceTaxInclusive))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(checkTaxExclusive)
                                .addComponent(checkTaxInclusive))))
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtShortName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtShortNameActionPerformed(evt);
            }
        });

        btnProductDescription.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnProductDescription.setText("Product Description");
        btnProductDescription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductDescriptionActionPerformed(evt);
            }
        });

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("PRODUCT");

        jLabel2.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(131, 131, 131)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel24)
                        .addComponent(jLabel1))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(labelCompanyName))
                    .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnExcelReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnExcelReport.setText("Download Product(s)  (Excel)");
        btnExcelReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelReportActionPerformed(evt);
            }
        });

        btnExcelReport1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnExcelReport1.setText("Upload  Product(s) (Excel)");
        btnExcelReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelReport1ActionPerformed(evt);
            }
        });

        btnExcelReport2.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnExcelReport2.setText("Download Template");
        btnExcelReport2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelReport2ActionPerformed(evt);
            }
        });

        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem4.setText("Payment");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem4);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtName)
                                                    .addComponent(comboGroup, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(txtTamilName)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel6)
                                                                .addGap(82, 82, 82)
                                                                .addComponent(jLabel26))
                                                            .addComponent(jLabel27)
                                                            .addComponent(jLabel14)
                                                            .addComponent(jLabel5)
                                                            .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(jLabel4))
                                                        .addGap(0, 0, Short.MAX_VALUE)))
                                                .addGap(81, 81, 81))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnGenerate)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(45, 45, 45)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGap(72, 72, 72)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnProductDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnExcelReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnExcelReport1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnExcelReport2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTamilName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel26))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnGenerate))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProductDescription)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExcelReport, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(btnExcelReport1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(btnExcelReport2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        txtTamilName.requestFocus();
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtTamilNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTamilNameActionPerformed
        txtHsncode.requestFocus();
    }//GEN-LAST:event_txtTamilNameActionPerformed

    private void txtShortNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtShortNameActionPerformed
        int i = jTable1.getSelectedRow();
        if (btnAdd.isEnabled()) {
            if (i == -1) {
                comboUQC1.requestFocus();
            }
        } else {
            txtBasePriceUQC1.requestFocus();
        }

    }//GEN-LAST:event_txtShortNameActionPerformed

    private void txtHsncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHsncodeActionPerformed

        txtCode.requestFocus();

    }//GEN-LAST:event_txtHsncodeActionPerformed

    private void comboUQC1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUQC1FocusGained
        comboUQC1.showPopup();
    }//GEN-LAST:event_comboUQC1FocusGained

    private void comboUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboUQC1KeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {

            comboUQC2.requestFocus();
        }
    }//GEN-LAST:event_comboUQC1KeyPressed

    private void txtUQC2ValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2ValueActionPerformed
        txtBasePriceUQC1.requestFocus();
    }//GEN-LAST:event_txtUQC2ValueActionPerformed

    private void comboUQC2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUQC2FocusGained
        comboUQC2.showPopup();
    }//GEN-LAST:event_comboUQC2FocusGained

    private void comboUQC2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboUQC2KeyPressed
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            if (comboUQC1.getSelectedItem() == comboUQC2.getSelectedItem()) {
                txtUQC2Value.setEnabled(false);
                txtBasePriceUQC2.setEnabled(false);
                labelBasePerUQC2.setEnabled(false);
                txtPriceUQC2.setEnabled(false);
                labelPerUQC2.setEnabled(false);
                txtUQC2Value.setText("1");
                txtBasePriceUQC1.requestFocus();
            } else {
                txtUQC2Value.setEnabled(true);
                txtBasePriceUQC2.setEnabled(true);
                labelBasePerUQC2.setEnabled(true);
                txtPriceUQC2.setEnabled(true);
                labelPerUQC2.setEnabled(true);
                txtUQC2Value.setText("");
                txtUQC2Value.requestFocus();
            }
        }
    }//GEN-LAST:event_comboUQC2KeyPressed

    private void comboIGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboIGSTActionPerformed

    }//GEN-LAST:event_comboIGSTActionPerformed

    private void comboCGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCGSTActionPerformed

    }//GEN-LAST:event_comboCGSTActionPerformed

    private void comboSGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSGSTActionPerformed

    }//GEN-LAST:event_comboSGSTActionPerformed

    private void comboIGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboIGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            float value = Float.parseFloat((String) comboIGST.getSelectedItem());
            String divValue = String.valueOf(value / 2);

            comboCGST.setSelectedItem(divValue);
            comboSGST.setSelectedItem(divValue);

//            comboStockIGST.setSelectedItem(comboIGST.getSelectedItem());
//            comboStockCGST.setSelectedItem(comboCGST.getSelectedItem());
//            comboStockSGST.setSelectedItem(comboSGST.getSelectedItem());
            comboCGST.requestFocus();
        }
    }//GEN-LAST:event_comboIGSTKeyPressed

    private void comboCGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboCGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            comboSGST.requestFocus();
        }
    }//GEN-LAST:event_comboCGSTKeyPressed

    private void comboSGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboSGSTKeyPressed
        int i = jTable1.getSelectedRow();
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
//            if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
//            else {
//                txtStockUQC1Value.requestFocus();
//            }
//        }

    }//GEN-LAST:event_comboSGSTKeyPressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        addProduct();
    }//GEN-LAST:event_btnAddActionPerformed

    public Boolean checkSameProductName(String name, int pos) {
        Boolean isFound = false;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if ((pos != i) && (model.getValueAt(i, 1).toString().equals(name))) {
                isFound = true;
                break;
            }
        }
        return isFound;
    }

    public Boolean checkSameProductCode(String name, int pos) {
        Boolean isFound = false;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if ((pos != i) && (model.getValueAt(i, 33).toString().equals(name))) {
                isFound = true;
                break;
            }
        }
        return isFound;
    }

    public void updateProduct() {
        int i = jTable1.getSelectedRow();

        if (i >= 0) {
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }

            String strProductGroup;
            if (comboGroup.getSelectedItem().toString().equals("No Product Group")) {
                strProductGroup = null;
            } else {
                strProductGroup = comboGroup.getSelectedItem().toString();
            }
            String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String productNamefromTable = jTable1.getModel().getValueAt(i, 1).toString();
            String strHsnName = txtHsncode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strSecondValue = txtUQC2Value.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strAmount = txtPriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strBaseAmount = txtBasePriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateStock.getDate(), true);

            Boolean isAlreadyHere = checkSameProductName(strProductName, i);

            Boolean isCodeAlreadyHere = false;
            String strProductCode = txtCode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strProductCode.equals("")) {
                isCodeAlreadyHere = checkSameProductCode(strProductCode, i);
            }
            if (strProductName.equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter the Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
                txtName.requestFocus();
//            } else if (strHsnName.equals("")) {
//                JOptionPane.showMessageDialog(null, "Please enter the HSN Code", "Alert", JOptionPane.WARNING_MESSAGE);
//                txtHsncode.requestFocus();
            } else if (strSecondValue.equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter the UQC Value", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC2Value.requestFocus();
//            } else if (strAmount.equals("")) {
//                JOptionPane.showMessageDialog(null, "Please enter the Amount", "Alert", JOptionPane.WARNING_MESSAGE);
//                txtPriceUQC1.requestFocus();
            } else if (isAlreadyHere) {
                JOptionPane.showMessageDialog(null, strProductName + " is already here. Please Enter another Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
                txtName.requestFocus();
//            } else if (!isInBetweenFinancialYear) {
//                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
//                        + " to <b>" + strFinancialYearEnd + "</b></html>";
//                JLabel label = new JLabel(msg);
//                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            } else if (isCodeAlreadyHere) {
                JOptionPane.showMessageDialog(null, strProductName + " Product Code is already here. Please Enter another Product Code", "Alert", JOptionPane.WARNING_MESSAGE);
                txtName.requestFocus();
            } else {
                Long id = (Long) jTable1.getModel().getValueAt(i, 0);

                if (strBaseAmount.equals("")) {
                    strBaseAmount = "0.00";
                }

                Long productGroupId = getProductGroupIDbyName();
                ProductGroup newProductGroup = new ProductGroup();

                newProductGroup.setCompany(company);
                newProductGroup.setProductGroupId(productGroupId);
                newProductGroup.setProductGroupName(strProductGroup);

                Product newProduct = new Product();
                newProduct.setProductGroup(newProductGroup);

                newProduct.setProductId(id);
                newProduct.setName(strProductName);
                newProduct.setTamilName(txtTamilName.getText());
                newProduct.setShortName(txtShortName.getText());
                newProduct.setProductCode(txtCode.getText());
                newProduct.setHsnCode(strHsnName);
                newProduct.setIgstPercentage(Float.parseFloat((String) comboIGST.getSelectedItem()));
                newProduct.setCgstPercentage(Float.parseFloat((String) comboCGST.getSelectedItem()));
                newProduct.setSgstPercentage(Float.parseFloat((String) comboSGST.getSelectedItem()));
                if (isProfitEnable) {
                    newProduct.setBaseProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strBaseAmount))));
                    newProduct.setBasePriceInclusiveOfGST(checkBasePriceTaxInclusive.isSelected());
                }
                newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strAmount))));
                newProduct.setInclusiveOfGST(checkTaxInclusive.isSelected());
                newProduct.setUQC1(comboUQC1.getSelectedItem().toString());
                newProduct.setUQC2(comboUQC2.getSelectedItem().toString());
                newProduct.setUQC2Value(Integer.parseInt(strSecondValue));
                newProduct.setIsOpeningStockEditable(true);

                newProduct.setService(isService);

                Boolean isEditable = (Boolean) jTable1.getModel().getValueAt(i, 16);

                int getWholeStock = getStock(Integer.parseInt(strSecondValue));
                //        String strStock = newProduct.getStockValue(getWholeStock);

                float stockAmount = 0;
//                String strStockAmount = txtStockAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                if (!strStockAmount.equals("")) {
//                    stockAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockAmount)));
//                }
//
//                float stockIGSTAmount = 0;
//                String strStockIGSTAmount = txtStockIGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                if (!strStockIGSTAmount.equals("")) {
//                    stockIGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockIGSTAmount)));
//                }
//                float stockCGSTAmount = 0;
//                String strStockCGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                if (!strStockCGSTAmount.equals("")) {
//                    stockCGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockCGSTAmount)));
//                }
//                float stockSGSTAmount = 0;
//                String strStockSGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                if (!strStockSGSTAmount.equals("")) {
//                    stockSGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockSGSTAmount)));
//                }

//                newProduct.setNillRated(CheckNillRated.isSelected());
                newProduct.setNonGST(ChecknonGst.isSelected());
                newProduct.setExempted(CheckExempted.isSelected());

                ProductStockEntry newProductStock = new ProductStockEntry();

                newProductStock.setQuantity(getWholeStock);
                newProductStock.setProduct(newProduct);
                newProductStock.setStockValue(convertDecimal.Currency(currency.format(stockAmount)));
//                newProductStock.setStockUpdateDate(dateStock.getDate());
//                newProductStock.setIgstPercentage(Float.parseFloat((String) comboStockIGST.getSelectedItem()));
//                newProductStock.setCgstPercentage(Float.parseFloat((String) comboStockCGST.getSelectedItem()));
//                newProductStock.setSgstPercentage(Float.parseFloat((String) comboStockSGST.getSelectedItem()));
//                newProductStock.setIgstAmount(convertDecimal.Currency(currency.format(stockIGSTAmount)));
//                newProductStock.setCgstAmount(convertDecimal.Currency(currency.format(stockCGSTAmount)));
//                newProductStock.setSgstAmount(convertDecimal.Currency(currency.format(stockSGSTAmount)));
                newProductStock.setStockUpdateType("OPENING");
                Long productStockId = (Long) jTable1.getModel().getValueAt(i, 27);
                newProductStock.setProductStockEntryId(productStockId);
                Boolean isSuccess;
                if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                    isSuccess = new ProductLogic().updateProduct(newProduct, null, companyId);
                } else {
                    isSuccess = new ProductLogic().updateProduct(newProduct, newProductStock, companyId);
                }

                if (isSuccess) {
                    JOptionPane.showMessageDialog(null, productNamefromTable + " is Successfully Updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clear();
                    fetch();
                } else {
                    JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public long getProductGroupIDbyName() {

        String strProductGroup = comboGroup.getSelectedItem().toString();
        Long productGroupId = null;
//        Company company = null;

        List<ProductGroup> productGroups = new ProductGroupLogic().fetchAllPG();
        for (ProductGroup productGroup : productGroups) {

            if (productGroup.getProductGroupName().equals(strProductGroup)) {
//                if (productGroup.getProductGroupName() == null) {
//                    productGroupId = null;
//                } else {
                productGroupId = productGroup.getProductGroupId();
//                }

//                company = productGroup.getCompany();
            }

        }
        return productGroupId;
    }

    public void addProduct() {
        String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strProductGroup;
        String strHsnName = "";
        if (comboGroup.getSelectedItem().toString().equals("No Product Group")) {
            strProductGroup = null;
        } else {
            strProductGroup = comboGroup.getSelectedItem().toString();
        }
        strHsnName = txtHsncode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strSecondValue = txtUQC2Value.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strAmount = txtPriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strBaseAmount = txtBasePriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateStock.getDate(), false);
        Boolean isAlreadyHere = checkSameProductName(strProductName, -1);
        Boolean isCodeAlreadyHere = false;
        String strProductCode = txtCode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (!strProductCode.equals("")) {
            isCodeAlreadyHere = checkSameProductCode(strProductCode, -1);
            System.out.println("isCodeAlreadyHere code " + isCodeAlreadyHere);
        }
        System.out.println("isCodeAlreadyHere " + isCodeAlreadyHere);

        if (strProductName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
//        } else if (strHsnName.equals("")) {
//            JOptionPane.showMessageDialog(null, "Please enter the HSN Code", "Alert", JOptionPane.WARNING_MESSAGE);
//            txtHsncode.requestFocus();
        } else if (strSecondValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the UQC Value", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC2Value.requestFocus();
//        } else if (strAmount.equals("")) {
//            JOptionPane.showMessageDialog(null, "Please enter the Amount", "Alert", JOptionPane.WARNING_MESSAGE);
//            txtPriceUQC1.requestFocus();
        } else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strProductName + " is already here. Please Enter another Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
//        } else if (!isInBetweenFinancialYear) {
//            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
//                    + " to <b>" + strFinancialYearEnd + "</b></html>";
//            JLabel label = new JLabel(msg);
//            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        } else if (isCodeAlreadyHere) {
            JOptionPane.showMessageDialog(null, strProductName + " Product Code is already here. Please Enter another Product Code", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
        } else {

            int getWholeStock = getStock(Integer.parseInt(strSecondValue));
            if (strBaseAmount.equals("")) {
                strBaseAmount = "0.00";
            }
            Long productGroupId = getProductGroupIDbyName();
            ProductGroup newProductGroup = new ProductGroup();

            newProductGroup.setCompany(company);
            newProductGroup.setProductGroupId(productGroupId);
            newProductGroup.setProductGroupName(strProductGroup);

            Product newProduct = new Product();
            newProduct.setProductGroup(newProductGroup);
            newProduct.setName(strProductName);

            String name = txtTamilName.getText();
            newProduct.setTamilName(name);

            newProduct.setShortName(txtShortName.getText());
            newProduct.setProductCode(txtCode.getText());
            newProduct.setHsnCode(strHsnName);

            newProduct.setIgstPercentage(Float.parseFloat((String) comboIGST.getSelectedItem()));
            newProduct.setCgstPercentage(Float.parseFloat((String) comboCGST.getSelectedItem()));
            newProduct.setSgstPercentage(Float.parseFloat((String) comboSGST.getSelectedItem()));
            newProduct.setDescriptionOne(
                    "");
            newProduct.setDescriptionTwo(
                    "");
            newProduct.setDescriptionThree(
                    "");
            newProduct.setDescriptionFour(
                    "");
            newProduct.setDescriptionFive(
                    "");
            newProduct.setDescriptionSix(
                    "");
            newProduct.setDescriptionSeven(
                    "");
            newProduct.setDescriptionEight(
                    "");
            newProduct.setDescriptionNine(
                    "");
            newProduct.setDescriptionTen(
                    "");
            if (strAmount.equals(
                    "")) {
                strAmount = "0.00";
                newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strAmount))));
            } else {
                newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strAmount))));
            }

            if (isProfitEnable) {
                newProduct.setBaseProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strBaseAmount))));
                newProduct.setBasePriceInclusiveOfGST(checkBasePriceTaxInclusive.isSelected());
            }

            newProduct.setInclusiveOfGST(checkTaxInclusive.isSelected());
            newProduct.setBasePriceInclusiveOfGST(checkBasePriceTaxInclusive.isSelected());
            newProduct.setUQC1(comboUQC1.getSelectedItem().toString());
            newProduct.setUQC2(comboUQC2.getSelectedItem().toString());
            newProduct.setUQC2Value(Integer.parseInt(strSecondValue));
            newProduct.setCurrentStock(getWholeStock);
//            newProduct.setNillRated(CheckNillRated.isSelected());

            newProduct.setNonGST(ChecknonGst.isSelected());
            newProduct.setExempted(CheckExempted.isSelected());

            newProduct.setService(isService);

            // String strStock = newProduct.getStockValue(getWholeStock);
            float stockAmount = 0;
//            String strStockAmount = txtStockAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockAmount.equals("")) {
//                stockAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockAmount)));
//            }
//
//            float stockIGSTAmount = 0;
//            String strStockIGSTAmount = txtStockIGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockIGSTAmount.equals("")) {
//                stockIGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockIGSTAmount)));
//            }
//            float stockCGSTAmount = 0;
//            String strStockCGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockCGSTAmount.equals("")) {
//                stockCGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockCGSTAmount)));
//            }
//            float stockSGSTAmount = 0;
//            String strStockSGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockSGSTAmount.equals("")) {
//                stockSGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockSGSTAmount)));
//            }
            List<GoDownStockDetail> gsds = new ArrayList<GoDownStockDetail>();
            for (GoDown god
                    : g4.fetchAllGodowns(company.getCompanyId())) {
                GoDownStockDetail gsd = new GoDownStockDetail();
                gsd.setGodown(god);

                gsd.setOpeningStock(Long.decode("0"));
//                gsd.setOpeningStockDate(new Date());

                gsd.setOpeningStockDate(SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFinancialYearStart());
                gsd.setOpeningStockValue(new BigDecimal(0));
                gsds.add(gsd);
            }

            ProductStockEntry newProductStock = new ProductStockEntry();

            newProductStock.setQuantity(getWholeStock);

            newProductStock.setProduct(newProduct);

            newProductStock.setStockValue(convertDecimal.Currency(currency.format(stockAmount)));
//            GodownStockEntryUI godownStockEntry = new GodownStockEntryUI();

//            newProductStock.setStockUpdateDate(dateStock.getDate());
//            newProductStock.setIgstPercentage(Float.parseFloat((String) comboStockIGST.getSelectedItem()));
//            newProductStock.setCgstPercentage(Float.parseFloat((String) comboStockCGST.getSelectedItem()));
//            newProductStock.setSgstPercentage(Float.parseFloat((String) comboStockSGST.getSelectedItem()));
//            newProductStock.setIgstAmount(convertDecimal.Currency(currency.format(stockIGSTAmount)));
//            newProductStock.setCgstAmount(convertDecimal.Currency(currency.format(stockCGSTAmount)));
//            newProductStock.setSgstAmount(convertDecimal.Currency(currency.format(stockSGSTAmount)));
            newProductStock.setStockUpdateType(
                    "OPENING");
            Boolean isSuccess;

            if (!SystemPolicyUtil.getSystemPolicy()
                    .isInventoryEnabled()) {
                isSuccess = new ProductLogic().createProduct(companyId, newProduct, null);
            } else {
                isSuccess = new ProductLogic().createProduct(companyId, newProduct, gsds);
            }

            if (isSuccess) { //Check this

                JOptionPane.showMessageDialog(null, strProductName + " is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                clear();
                fetch();
            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                clear();
                fetch();
            }
        }
    }

    public int getStock(int num) {
        int wholePic = 0, Pic = 0, convertValue = 0;

//        if (!txtStockUQC1Value.getText().equals("")) {
//            wholePic = Integer.parseInt(txtStockUQC1Value.getText());
//        }
//        if (!txtStockUQC2Value.getText().equals("")) {
//            Pic = Integer.parseInt(txtStockUQC2Value.getText());
//        }
        convertValue = (num * wholePic) + Pic;
        return convertValue;
    }

    public String getUQC2ValuefromProduct() {
        System.err.println("$$$$$$$$$ " + strSecondValue);
        return strSecondValue;
    }

    private void comboCGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboCGSTFocusGained
        comboCGST.showPopup();
    }//GEN-LAST:event_comboCGSTFocusGained

    private void comboSGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboSGSTFocusGained
        comboSGST.showPopup();
    }//GEN-LAST:event_comboSGSTFocusGained

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
        txtSearch.requestFocus();
    }//GEN-LAST:event_btnClearActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        try {
            btnAdd.setEnabled(false);
            btnProductDescription.setEnabled(true);
            btnUpdate.setEnabled(companyUserRole.isProductUpdate());
            btnDelete.setEnabled(companyUserRole.isProductDelete());
            int i = jTable1.getSelectedRow();
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            txtName.setText(model.getValueAt(i, 1).toString());
            txtTamilName.setText(model.getValueAt(i, 2).toString());
            txtShortName.setText(model.getValueAt(i, 3).toString());
            txtHsncode.setText(model.getValueAt(i, 4).toString());
            comboUQC1.setSelectedItem(model.getValueAt(i, 5).toString());
            txtUQC2Value.setText(model.getValueAt(i, 6).toString());
            comboUQC2.setSelectedItem(model.getValueAt(i, 7).toString());

            if (isProfitEnable) {
                float QUC1BasePrice = Float.parseFloat(model.getValueAt(i, 9).toString()) * Integer.parseInt(model.getValueAt(i, 6).toString());
                txtBasePriceUQC2.setText(currency.format(model.getValueAt(i, 9)));
                txtBasePriceUQC1.setText(currency.format(QUC1BasePrice));
                checkBasePriceTaxInclusive.setSelected((Boolean) model.getValueAt(i, 10));
            }

            float QUC1Price = Float.parseFloat(model.getValueAt(i, 11).toString()) * Integer.parseInt(model.getValueAt(i, 6).toString());
            txtPriceUQC2.setText(currency.format(model.getValueAt(i, 11)));
            txtPriceUQC1.setText(currency.format(QUC1Price));

            Boolean isTaxInclusive = (Boolean) model.getValueAt(i, 12);
            checkTaxInclusive.setSelected(isTaxInclusive);
            comboIGST.setSelectedItem(model.getValueAt(i, 13).toString());
            comboCGST.setSelectedItem(model.getValueAt(i, 14).toString());
            comboSGST.setSelectedItem(model.getValueAt(i, 15).toString());
            Boolean isEditable = (Boolean) model.getValueAt(i, 16);

            System.err.println("########## " + model.getValueAt(i, 6).toString());
            txtUQC2Value.setText(model.getValueAt(i, 6).toString());
            comboUQC1.setEnabled(isEditable);
            txtUQC2Value.setEnabled(isEditable);
            comboUQC2.setEnabled(isEditable);
//            txtStockUQC1Value.setEnabled(isEditable);
//            txtStockUQC2Value.setEnabled(isEditable);

            int Quantity = Integer.parseInt(model.getValueAt(i, 18).toString());
            int QUC2value = Integer.parseInt(model.getValueAt(i, 6).toString());
            int stock1 = Quantity / QUC2value, stock2 = Quantity % QUC2value;

//            dateStock.setDate((Date) model.getValueAt(i, 17));
//            txtStockUQC1Value.setText("" + stock1);
//            txtStockUQC2Value.setText("" + stock2);
            Boolean isNillRated = (Boolean) model.getValueAt(i, 28);
//            CheckNillRated.setSelected(isNillRated);
            Boolean isExempted = (Boolean) model.getValueAt(i, 29);
            CheckExempted.setSelected(isExempted);
            Boolean isNonGST = (Boolean) model.getValueAt(i, 30);
            ChecknonGst.setSelected(isNonGST);
            Boolean isService = (Boolean) model.getValueAt(i, 31);
            String tablevalue = null;
            try {
                tablevalue = model.getValueAt(i, 32).toString();
            } catch (NullPointerException ex) {
                if (tablevalue == null) {
                    tablevalue = "";
                }
            }
            System.err.println("tablevalue " + tablevalue);
            if (!"".equals(tablevalue)) {

                comboGroup.setSelectedItem(model.getValueAt(i, 32).toString());
            } else {

                comboGroup.setSelectedItem("No Product Group");
            }

            if (model.getValueAt(i, 33) != null) {
                txtCode.setText(model.getValueAt(i, 33).toString());
            }
            comboGroup.requestFocus();
//            btnOpeningStock.setEnabled(true);

//        if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
//            txtStockAmount.setText(null);
//            comboStockIGST.setSelectedItem(null);
//            txtStockIGSTAmount.setText(null);
//            comboStockCGST.setSelectedItem(null);
//            txtStockCGSTAmount.setText(null);
//            comboStockSGST.setSelectedItem(null);
//            txtStockSGSTAmount.setText(null);
//        } else {
//            txtStockAmount.setText(model.getValueAt(i, 20).toString());
//            comboStockIGST.setSelectedItem(model.getValueAt(i, 21).toString());
//            txtStockIGSTAmount.setText(currency.format(model.getValueAt(i, 22)));
//            comboStockCGST.setSelectedItem(model.getValueAt(i, 23).toString());
//            txtStockCGSTAmount.setText(currency.format(model.getValueAt(i, 24)));
//            comboStockSGST.setSelectedItem(model.getValueAt(i, 25).toString());
//            txtStockSGSTAmount.setText(currency.format(model.getValueAt(i, 26)));
//        }
        } catch (NullPointerException ex) {

            Logger.getLogger(ProductUI.class.getName()).log(Level.SEVERE, null, ex);

        }


    }//GEN-LAST:event_jTable1MouseClicked

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == java.awt.event.KeyEvent.VK_ENTER) {
            addProduct();
            evt.consume();
            comboGroup.requestFocus();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProduct();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();

        if (i >= 0) {
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }
            Long id = (Long) jTable1.getModel().getValueAt(i, 0);

            Object[] options = {"Yes", "No"};
            int p = JOptionPane.showOptionDialog(null, "SURE YOU WANT TO DELETE ?", "CONFIRMATION", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
            if (p == JOptionPane.YES_OPTION) {
                String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                EventStatus result = new ProductLogic().deleteProduct(id);
                if (result.isDeleteDone()) {
                    JOptionPane.showMessageDialog(null, strProductName + " is Sucessfully Deleted", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clear();
                    fetch();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Delete has been cancelled", "Message", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select any Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void comboUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUQC1ActionPerformed
        txtUQC2Value.setEnabled(false);
        txtBasePriceUQC2.setEnabled(false);
        labelBasePerUQC2.setEnabled(false);
        txtPriceUQC2.setEnabled(false);
        labelPerUQC2.setEnabled(false);
//        labelUQC1.setText((String) comboUQC1.getSelectedItem());
        labelPerUQC1.setText((String) comboUQC1.getSelectedItem());
        labelBasePerUQC1.setText((String) comboUQC1.getSelectedItem());

    }//GEN-LAST:event_comboUQC1ActionPerformed

    private void comboUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUQC2ActionPerformed
        if (comboUQC1.getSelectedItem() == comboUQC2.getSelectedItem()) {
            txtUQC2Value.setEnabled(false);
            txtBasePriceUQC2.setEnabled(false);
            labelBasePerUQC2.setEnabled(false);
            txtPriceUQC2.setEnabled(false);
            labelPerUQC2.setEnabled(false);
            txtUQC2Value.setText("1");
        } else {
            txtUQC2Value.setEnabled(true);
            txtBasePriceUQC2.setEnabled(true);
            labelBasePerUQC2.setEnabled(true);
            txtPriceUQC2.setEnabled(true);
            labelPerUQC2.setEnabled(true);
            txtUQC2Value.setText("");
        }
//        labelUQC2.setText((String) comboUQC2.getSelectedItem());
        labelPerUQC2.setText((String) comboUQC2.getSelectedItem());
        labelBasePerUQC2.setText((String) comboUQC2.getSelectedItem());

    }//GEN-LAST:event_comboUQC2ActionPerformed

    private void txtUQC2ValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2ValueKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtUQC2ValueKeyTyped

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(((DefaultTableModel) jTable1.getModel()));
        sorter.setRowFilter(RowFilter.regexFilter("(?i)" + txtSearch.getText(), 1));
        jTable1.setRowSorter(sorter);
        clearWithoutSearchBox();
    }//GEN-LAST:event_txtSearchKeyReleased

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();

    }//GEN-LAST:event_btnLogoutActionPerformed

    private void comboIGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboIGSTFocusGained
        comboIGST.showPopup();
    }//GEN-LAST:event_comboIGSTFocusGained

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        updateProduct();
        txtSearch.requestFocus();
        evt.consume();
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void CheckExemptedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckExemptedActionPerformed
        if (CheckExempted.isSelected()) {
//            CheckNillRated.setSelected(false);
            ChecknonGst.setSelected(false);
        }
    }//GEN-LAST:event_CheckExemptedActionPerformed

    private void ChecknonGstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChecknonGstActionPerformed
        if (ChecknonGst.isSelected()) {
//            CheckNillRated.setSelected(false);
            CheckExempted.setSelected(false);
        }
    }//GEN-LAST:event_ChecknonGstActionPerformed


    private void comboGroupKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGroupKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtName.requestFocus();
        }
    }//GEN-LAST:event_comboGroupKeyPressed

    private void txtCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodeActionPerformed
        txtShortName.requestFocus();
    }//GEN-LAST:event_txtCodeActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboGroup.requestFocus();
        }
    }//GEN-LAST:event_txtSearchKeyPressed


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void checkBasePriceTaxInclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkBasePriceTaxInclusive.setSelected(true);
            checkBasePriceTaxExclusive.setSelected(false);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkBasePriceTaxInclusive.setSelected(false);
            checkBasePriceTaxExclusive.setSelected(true);
        } else if (vChar == KeyEvent.VK_ENTER) {
            txtPriceUQC1.requestFocus();
        }
    }//GEN-LAST:event_checkBasePriceTaxInclusiveKeyPressed

    private void txtBasePriceUQC2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBasePriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBasePriceUQC2KeyTyped

    private void txtBasePriceUQC2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC2Price = Float.parseFloat(txtBasePriceUQC2.getText());
        float UQC1Price = UQC2Price * Value;
        txtBasePriceUQC1.setText(currency.format(UQC1Price));
    }//GEN-LAST:event_txtBasePriceUQC2KeyReleased

    private void txtBasePriceUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2ActionPerformed
        checkBasePriceTaxInclusive.requestFocus();
    }//GEN-LAST:event_txtBasePriceUQC2ActionPerformed

    private void txtBasePriceUQC1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBasePriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBasePriceUQC1KeyTyped

    private void txtBasePriceUQC1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC1Price = Float.parseFloat(txtBasePriceUQC1.getText());
        float UQC2Price = UQC1Price / Value;
        txtBasePriceUQC2.setText(currency.format(UQC2Price));
    }//GEN-LAST:event_txtBasePriceUQC1KeyReleased

    private void txtBasePriceUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1ActionPerformed
//        txtBasePriceUQC2.requestFocus();
    }//GEN-LAST:event_txtBasePriceUQC1ActionPerformed

    private void txtPriceUQC2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC2KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPriceUQC2KeyTyped

    private void txtPriceUQC2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC2KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC2Price = Float.parseFloat(txtPriceUQC2.getText());
        float UQC1Price = UQC2Price * Value;
        txtPriceUQC1.setText(currency.format(UQC1Price));
    }//GEN-LAST:event_txtPriceUQC2KeyReleased

    private void txtPriceUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUQC2ActionPerformed
        checkTaxInclusive.requestFocus();
    }//GEN-LAST:event_txtPriceUQC2ActionPerformed

    private void txtPriceUQC1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPriceUQC1KeyTyped

    private void txtPriceUQC1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC1Price = Float.parseFloat(txtPriceUQC1.getText());
        float UQC2Price = UQC1Price / Value;
        txtPriceUQC2.setText(currency.format(UQC2Price));
    }//GEN-LAST:event_txtPriceUQC1KeyReleased

    private void txtPriceUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtBasePriceUQC2.isEnabled()) {
                checkTaxInclusive.requestFocus();
            } else {
                txtPriceUQC2.requestFocus();
            }
        }
    }//GEN-LAST:event_txtPriceUQC1KeyPressed

    private void txtPriceUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUQC1ActionPerformed
//        txtPriceUQC2.requestFocus();
    }//GEN-LAST:event_txtPriceUQC1ActionPerformed

    private void checkTaxInclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxInclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkTaxInclusive.setSelected(true);
            checkTaxExclusive.setSelected(false);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkTaxInclusive.setSelected(false);
            checkTaxExclusive.setSelected(true);
        } else if (vChar == KeyEvent.VK_ENTER) {
            comboIGST.requestFocus();
        }
    }//GEN-LAST:event_checkTaxInclusiveKeyPressed

    private void checkTaxInclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTaxInclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxInclusiveActionPerformed

    private void txtBasePriceUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtBasePriceUQC2.isEnabled()) {
                checkBasePriceTaxInclusive.requestFocus();
            } else {
                txtBasePriceUQC2.requestFocus();
            }
        }
    }//GEN-LAST:event_txtBasePriceUQC1KeyPressed

    private void btnProductDescriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductDescriptionActionPerformed
        int i = jTable1.getSelectedRow();
        if (i > -1) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

            Long id = Long.parseLong(model.getValueAt(i, 0).toString());
            Product getProduct = new ProductLogic().getProduct(id);
            ProductDescriptionUI productDescriptionUI = new ProductDescriptionUI(getProduct);
            productDescriptionUI.setVisible(true);
        } else {

        }

    }//GEN-LAST:event_btnProductDescriptionActionPerformed

    private void btnExcelReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            try {
                generateExcelReport();
                JOptionPane.showMessageDialog(null, "Download Completed", "Alert", JOptionPane.INFORMATION_MESSAGE);

            } catch (WriteException ex) {
                Logger.getLogger(PurchaseReportUI.class
                        .getName()).log(Level.SEVERE, null, ex);

            } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(ProductUI.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnExcelReportActionPerformed

    private void btnExcelReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelReport1ActionPerformed
        UploadProductsviaExcel();
        JOptionPane.showMessageDialog(null, "Product Uploaded Successfully", "Alert", JOptionPane.INFORMATION_MESSAGE);
//        Thread t = new Thread(new Runnable() {
//            public void run() {
//                JOptionPane.showMessageDialog(null, "Uploading...");
//            }
//        });
//        t.start();
    }//GEN-LAST:event_btnExcelReport1ActionPerformed

    private void btnExcelReport2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelReport2ActionPerformed
//        File source = new File("C:\\Users\\CEO\\gstonepro\\temp");
//        File dest = new File("C:\\Users\\CEO\\Desktop\\New folder");
//        FileUtils.copyDirectory(source, dest);
        Boolean isApproved = false;
        String path = "";
        InputStream inStream = null;
        OutputStream outStream = null;

        try {

            File afile = new File(".\\template\\ProductImportTemplate.xls");
            JFrame parentFrame = new JFrame();
            com.alee.laf.WebLookAndFeel.install();
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Template");
            fileChooser.setSelectedFile(new File("ProductImportTemplate"));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                path = fileToSave.getAbsolutePath() + ".xls";
                isApproved = true;
            }
            if (isApproved) {
                File bfile = new File(path);

                inStream = new FileInputStream(afile);
                outStream = new FileOutputStream(bfile);

                byte[] buffer = new byte[1024];

                int length;
                //copy the file content in bytes 
                while ((length = inStream.read(buffer)) > 0) {

                    outStream.write(buffer, 0, length);

                }

                inStream.close();
                outStream.close();

                //delete the original file
//            afile.delete();
                JOptionPane.showMessageDialog(null, "Template Download Successfully", "Alert", JOptionPane.WARNING_MESSAGE);
//                

            }
        } catch (IOException e) {
            e.printStackTrace();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnExcelReport2ActionPerformed

    private void checkBasePriceTaxExclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkBasePriceTaxExclusiveActionPerformed

    private void checkBasePriceTaxExclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkBasePriceTaxInclusive.setSelected(false);
            checkBasePriceTaxExclusive.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkBasePriceTaxInclusive.setSelected(true);
            checkBasePriceTaxExclusive.setSelected(false);
        } else if (vChar == KeyEvent.VK_ENTER) {
            checkBasePriceTaxInclusive.requestFocus();
        }
    }//GEN-LAST:event_checkBasePriceTaxExclusiveKeyPressed

    private void checkBasePriceTaxExclusiveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_checkBasePriceTaxExclusiveKeyReleased

    private void checkTaxExclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTaxExclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxExclusiveActionPerformed

    private void checkTaxExclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxExclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkTaxInclusive.setSelected(false);
            checkTaxExclusive.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkTaxInclusive.setSelected(true);
            checkTaxExclusive.setSelected(false);
        } else if (vChar == KeyEvent.VK_ENTER) {
            checkTaxInclusive.requestFocus();
        }
    }//GEN-LAST:event_checkTaxExclusiveKeyPressed

    private void checkBasePriceTaxExclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveItemStateChanged
        checkBasePriceTaxInclusive.setSelected(!checkBasePriceTaxExclusive.isSelected());
    }//GEN-LAST:event_checkBasePriceTaxExclusiveItemStateChanged

    private void checkBasePriceTaxInclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveItemStateChanged
        checkBasePriceTaxExclusive.setSelected(!checkBasePriceTaxInclusive.isSelected());
    }//GEN-LAST:event_checkBasePriceTaxInclusiveItemStateChanged

    private void checkTaxExclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxExclusiveItemStateChanged
        checkTaxInclusive.setSelected(!checkTaxExclusive.isSelected());
    }//GEN-LAST:event_checkTaxExclusiveItemStateChanged

    private void checkTaxInclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxInclusiveItemStateChanged
        checkTaxExclusive.setSelected(!checkTaxInclusive.isSelected());
    }//GEN-LAST:event_checkTaxInclusiveItemStateChanged

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        String code = txtCode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (code.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter the Product code", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            BarcodeEAN barcode = new BarcodeEAN();
            barcode.setCodeType(Barcode.EAN8);
            barcode.setCode(String.format("%08d", Integer.parseInt(code)));
            Image img = barcode.createAwtImage(Color.BLACK, Color.WHITE);
//            BufferedImage bImg = (BufferedImage) img;
//            Image dimg = bImg.getScaledInstance(jLabel16.getWidth(), jLabel16.getHeight(),Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(img);
            jLabel16.setIcon(imageIcon);
            dialogBarcode.setVisible(true);
            dialogBarcode.setSize(300, 200);
            dialogBarcode.setLocationRelativeTo(null);

        }
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void btnDownloadBarcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadBarcodeActionPerformed
        generateBarcode();
    }//GEN-LAST:event_btnDownloadBarcodeActionPerformed
    private void generateBarcode() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
            JFrame parentFrame = new JFrame();
            Boolean isApproved = false;
            String path = "";
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Bar Code");
            fileChooser.setSelectedFile(new File(txtName.getText() + " Barcode"));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                path = fileToSave.getAbsolutePath() + ".pdf";
                isApproved = true;
            }
            if (isApproved) {
                new CreateBarcodePdf().createPdf(path, Integer.parseInt(txtCode.getText()), Integer.parseInt(txtBarcodeCount.getText()));
                dialogBarcode.dispose();
                txtBarcodeCount.setText("");
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ProductUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ProductUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ProductUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        com.alee.laf.WebLookAndFeel.install();
    }
    private void txtCodeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodeKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCodeKeyTyped

    private void txtBarcodeCountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBarcodeCountKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtBarcodeCountKeyTyped

    private void txtBarcodeCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBarcodeCountActionPerformed
        generateBarcode();
    }//GEN-LAST:event_txtBarcodeCountActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    private void UploadProductsviaExcel() {
        try {
            JFrame parentFrame = new JFrame();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String strDate = sdf.format(new Date());
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Upload Product");
            int userSelection = fileChooser.showOpenDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();

//                Path path = Paths.get(fileToSave.getAbsolutePath());
                com.alee.laf.WebLookAndFeel.install();

                Workbook workbook = Workbook.getWorkbook(fileToSave);
//Workbook workbook = Workbook.getWorkbook(fileToSave);
                Sheet sheet = workbook.getSheet(0);
                String strProductGroup;
                strProductGroup = null;
                int productCount = 0;
                String strProductName = null, strHsnName = null, strUQC1 = null, strUQC2Value = null,
                        strUQC2 = null, strPrice = null, igst_percentage = null, cgst_percentage = null, sgst_percentage = null;
                for (int i = 1; i < sheet.getRows(); i++) {
                    if (sheet.getCell(0, i).getContents() != "") {
                        productCount++;
                    }
                }

//                SwingWorkerProgress progress = new SwingWorkerProgress();
//                SwingWorkerProgress.MySwingWorker nested = new SwingWorkerProgress.MySwingWorker();
//                nested.doInBackground(productCount);
                for (int i = 1; i <= productCount; i++) {

                    strProductName = sheet.getCell(0, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                    Boolean isAlreadyHere = checkSameProductName(strProductName, -1);
                    if (!isAlreadyHere) {

                        strProductName = sheet.getCell(0, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        strHsnName = sheet.getCell(1, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        strUQC1 = sheet.getCell(2, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        strUQC2Value = sheet.getCell(3, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        strUQC2 = sheet.getCell(4, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        strPrice = sheet.getCell(5, i).getContents().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        igst_percentage = sheet.getCell(6, i).getContents();
                        cgst_percentage = sheet.getCell(7, i).getContents();
                        sgst_percentage = sheet.getCell(8, i).getContents();
                        
                        System.out.println("igst_percentage "+igst_percentage);

                        Long productGroupId = getProductGroupIDbyName();
                        ProductGroup newProductGroup = new ProductGroup();

                        newProductGroup.setCompany(company);
                        newProductGroup.setProductGroupId(productGroupId);
                        newProductGroup.setProductGroupName(strProductGroup);

                        Product newProduct = new Product();
                        newProduct.setProductGroup(newProductGroup);
                        newProduct.setName(strProductName);
                        newProduct.setTamilName("");
                        newProduct.setShortName("");
                        newProduct.setProductCode("");
                        newProduct.setHsnCode(strHsnName);
                        newProduct.setBaseProductRate(BigDecimal.ZERO);

                        newProduct.setIgstPercentage(Float.parseFloat(igst_percentage));
                        newProduct.setCgstPercentage(Float.parseFloat(cgst_percentage));
                        newProduct.setSgstPercentage(Float.parseFloat(sgst_percentage));
                        newProduct.setDescriptionOne("");
                        newProduct.setDescriptionTwo("");
                        newProduct.setDescriptionThree("");
                        newProduct.setDescriptionFour("");
                        newProduct.setDescriptionFive("");
                        newProduct.setDescriptionSix("");
                        newProduct.setDescriptionSeven("");
                        newProduct.setDescriptionEight("");
                        newProduct.setDescriptionNine("");
                        newProduct.setDescriptionTen("");
                        if (strPrice.equals("")) {
                            strPrice = "0.00";
                            newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strPrice))));
                        } else {
                            newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strPrice))));
                        }

                        newProduct.setInclusiveOfGST(false);
                        newProduct.setBasePriceInclusiveOfGST(false);
                        newProduct.setUQC1(strUQC1);
                        newProduct.setUQC2(strUQC2);
                        newProduct.setUQC2Value(Integer.parseInt(strUQC2Value));
                        newProduct.setCurrentStock(0);
                        newProduct.setNonGST(false);
                        newProduct.setExempted(false);

                        newProduct.setService(false);

                        float stockAmount = 0;
                        List<GoDownStockDetail> gsds = new ArrayList<GoDownStockDetail>();
                        for (GoDown god : g4.fetchAllGodowns(company.getCompanyId())) {
                            GoDownStockDetail gsd = new GoDownStockDetail();
                            gsd.setGodown(god);

                            gsd.setOpeningStock(Long.decode("0"));

                            gsd.setOpeningStockDate(SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFinancialYearStart());
                            gsd.setOpeningStockValue(new BigDecimal(0));
                            gsds.add(gsd);
                        }

                        ProductStockEntry newProductStock = new ProductStockEntry();

                        newProductStock.setQuantity(0);
                        newProductStock.setProduct(newProduct);
                        newProductStock.setStockValue(convertDecimal.Currency(currency.format(stockAmount)));
                        newProductStock.setStockUpdateType("OPENING");
                        Boolean isSuccess;
                        if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                            isSuccess = new ProductLogic().createProduct(companyId, newProduct, null);
                        } else {
                            isSuccess = new ProductLogic().createProduct(companyId, newProduct, gsds);
                        }

                        fetch();

//                        if (isSuccess) { //Check this
//                            
//                            
//                            clear();
//                            fetch();
//                        } else {
//                            
//                            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
//                            clear();
//                            fetch();
//                        }
                    }
//            }
//                    prograssBar.setValue((productCount / i) * 100);
                }

            }
        } catch (EncryptedDocumentException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (BiffException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            Logger.getLogger(ProductUI.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateExcelReport() throws WriteException, UnsupportedLookAndFeelException {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            Boolean isApproved = false;
            String path = "";
            Boolean isPrint = false;
//            String fileName = "Tax Invoice Report " + sdf.format(from) + " to " + sdf.format(to);
//            JFrame parentFrame = new JFrame();

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Product Report " + ".xls";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Report");
                fileChooser.setSelectedFile(new File("Product "));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".xls";
                    isApproved = true;
                }
            }

            if (isApproved) {
                File f = new File(path);
                WritableWorkbook myexcel;
                myexcel = Workbook.createWorkbook(f);
                WritableSheet mysheet = myexcel.createSheet("mysheet", 0);
                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);
                WritableCellFormat rupeeFormat = new WritableCellFormat();
                rupeeFormat.setAlignment(Alignment.RIGHT);
                rupeeFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat rightFormat = new WritableCellFormat(wf2);
                rightFormat.setAlignment(Alignment.RIGHT);
                rightFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat centerFormat = new WritableCellFormat(wf2);
                centerFormat.setAlignment(Alignment.CENTRE);
                centerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableFont wf4 = new WritableFont(WritableFont.TIMES);
                mysheet.setColumnView(0, 20);
                mysheet.setColumnView(1, 40);
                mysheet.setColumnView(2, 30);
                mysheet.setColumnView(3, 30);
                mysheet.setColumnView(4, 20);
                mysheet.setColumnView(5, 20);
                mysheet.setColumnView(6, 20);
                //mysheet.setAutobreaks(false);
                WritableCellFormat cellFormat = new WritableCellFormat(wf2);
                cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat cellFormat1 = new WritableCellFormat(wf3);
                cellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
                mysheet.setName("ProductReport");

                mysheet.addCell(new Label(0, 0, "GSTIN/UIN: " + company.getGSTIN(), cellFormat));
                mysheet.mergeCells(0, 0, 2, 0);
                mysheet.addCell(new Label(3, 0, "Product Report ", rightFormat));
                mysheet.mergeCells(3, 0, 6, 0);
                mysheet.addCell(new Label(0, 1, company.getCompanyName(), centerFormat));
                mysheet.mergeCells(0, 1, 6, 1);
                if (!company.getAddress().equals("")) {
                    mysheet.addCell(new Label(0, 2, company.getAddress().replaceAll("/~/", ""), centerFormat));
                    mysheet.mergeCells(0, 2, 6, 2);
                }
                mysheet.addCell(new Label(0, 3, "City : " + company.getCity(), centerFormat));
                mysheet.mergeCells(0, 3, 6, 3);
                mysheet.addCell(new Label(0, 4, "State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), centerFormat));
                mysheet.mergeCells(0, 4, 6, 4);
                mysheet.addCell(new Label(0, 5, "Phone : " + company.getPhone(), centerFormat));
                mysheet.mergeCells(0, 5, 6, 5);
                mysheet.addCell(new Label(0, 6, "Mobile : " + company.getMobile(), centerFormat));
                mysheet.mergeCells(0, 6, 6, 6);
                mysheet.addCell(new Label(0, 7, "Email : " + company.getEmail(), centerFormat));
                mysheet.mergeCells(0, 7, 6, 7);
                mysheet.addCell(new Label(0, 8, "Product Name", cellFormat));
                mysheet.addCell(new Label(1, 8, "HSN", cellFormat));
//                mysheet.addCell(new Label(2, 8, "UQC", cellFormat));
                mysheet.addCell(new Label(2, 8, "UQC1", cellFormat));
                mysheet.addCell(new Label(3, 8, "UQC2 Value per UQC1", cellFormat));
                mysheet.addCell(new Label(4, 8, "UQC2", cellFormat));
                mysheet.addCell(new Label(5, 8, "Price", cellFormat));
                mysheet.addCell(new Label(6, 8, "IGST %", cellFormat));
                mysheet.addCell(new Label(7, 8, "CGST %", cellFormat));
                mysheet.addCell(new Label(8, 8, "SGST %", cellFormat));

                float totalAmt = 0, totalProfit = 0;
                int totalPostion = 9 + model.getRowCount();
                for (int i = 0; i < model.getRowCount(); i++) {
//                    totalAmt = totalAmt + Float.parseFloat(model.getValueAt(i, 4).toString());
//                    totalProfit = totalProfit + Float.parseFloat(model.getValueAt(i, 5).toString());
                    int rowValue = 9 + i;
                    mysheet.addCell(new Label(0, rowValue, jTable1.getValueAt(i, 1).toString(), cellFormat1));
                    mysheet.addCell(new Label(1, rowValue, jTable1.getValueAt(i, 4).toString(), cellFormat1));
//                    mysheet.addCell(new Label(2, rowValue, jTable1.getValueAt(i, 8).toString(), cellFormat1));
                    mysheet.addCell(new Label(2, rowValue, jTable1.getValueAt(i, 5).toString(), cellFormat1));
                    mysheet.addCell(new Label(3, rowValue, jTable1.getValueAt(i, 6).toString(), cellFormat1));
                    mysheet.addCell(new Label(4, rowValue, jTable1.getValueAt(i, 7).toString(), cellFormat1));
                    mysheet.addCell(new Label(5, rowValue, jTable1.getValueAt(i, 11).toString(), cellFormat1));
                    mysheet.addCell(new Label(6, rowValue, jTable1.getValueAt(i, 13).toString(), cellFormat1));
                    mysheet.addCell(new Label(7, rowValue, jTable1.getValueAt(i, 14).toString(), cellFormat1));
                    mysheet.addCell(new Label(8, rowValue, jTable1.getValueAt(i, 15).toString(), cellFormat1));

//                    String strAmount = "", strProfit = "";
//                    strAmount = currency.format(Float.parseFloat(model.getValueAt(i, 4).toString()));
//                    strProfit = currency.format(Float.parseFloat(model.getValueAt(i, 5).toString()));
//                    mysheet.addCell(new Label(3, rowValue, strAmount, rupeeFormat));
//                    mysheet.addCell(new Label(4, rowValue, strProfit, rupeeFormat));
                }

//                mysheet.addCell(new Label(0, totalPostion, "Total", centerFormat));
//                mysheet.mergeCells(0, totalPostion, 2, totalPostion);
//                mysheet.addCell(new Label(3, totalPostion, currency.format(totalAmt), rightFormat));
//                mysheet.addCell(new Label(4, totalPostion, currency.format(totalProfit), rightFormat));
//
//                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
//                LocalDateTime now = LocalDateTime.now();
//                String d = dtf.format(now);
//                String date1 = "Report generated as on " + d;
//                int datePosition = totalPostion + 2;
//                mysheet.addCell(new Label(0, datePosition, date1, centerFormat));
//                mysheet.mergeCells(0, datePosition, 3, datePosition + 1);
                myexcel.write();
                myexcel.close();

                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(path);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        // no application registered for PDFs
                    }
                }
            }

            com.alee.laf.WebLookAndFeel.install();

        } catch (WriteException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
//    public void goDownDetails(String UQC1, String UQC2, int UQC2Value){
//       UQC2Value= Integer.parseInt(txtUQC2Value.getText());
//       UQC1=comboUQC1.getSelectedItem().toString();
//       UQC2= comboUQC2.getSelectedItem().toString();
//        
//    }

    public void clearWithoutSearchBox() {
        txtHsncode.setText("");
        txtName.setText("");
        txtPriceUQC1.setText("");
        txtPriceUQC2.setText("");
        txtUQC2Value.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);
        comboUQC1.setSelectedIndex(0);
        comboUQC2.setSelectedIndex(0);

//        comboStockCGST.setSelectedIndex(0);
//        comboStockIGST.setSelectedIndex(0);
//        comboStockSGST.setSelectedIndex(0);
//        txtStockIGSTAmount.setText("");
//        txtStockCGSTAmount.setText("");
//        txtStockSGSTAmount.setText("");
//        txtStockUQC1Value.setText("");
//        txtStockUQC2Value.setText("");
//        dateStock.setDate(new Date());
//        txtStockAmount.setText("");
        jTable1.clearSelection();
        btnAdd.setEnabled(companyUserRole.isProductCreate());

        comboUQC1.setEnabled(true);
        txtUQC2Value.setEnabled(true);
        txtBasePriceUQC2.setEnabled(true);
        labelBasePerUQC2.setEnabled(true);
        txtPriceUQC2.setEnabled(true);
        labelPerUQC2.setEnabled(true);
        comboUQC2.setEnabled(true);
//        txtStockUQC1Value.setEnabled(true);
//        txtStockUQC2Value.setEnabled(true);
//        CheckNillRated.setSelected(false);
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
    }

    public void clear() {
        txtHsncode.setText("");
        txtName.setText("");
        txtBasePriceUQC1.setText("");
        txtBasePriceUQC2.setText("");
        txtPriceUQC1.setText("");
        txtPriceUQC2.setText("");
        txtUQC2Value.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
        checkBasePriceTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);
        comboUQC1.setSelectedIndex(0);
        comboUQC2.setSelectedIndex(0);

        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        btnProductDescription.setEnabled(false);
//        btnOpeningStock.setEnabled(false);

//        comboStockCGST.setSelectedIndex(0);
//        comboStockIGST.setSelectedIndex(0);
//        comboStockSGST.setSelectedIndex(0);
//        txtStockIGSTAmount.setText("");
//        txtStockCGSTAmount.setText("");
//        txtStockSGSTAmount.setText("");
//        txtStockUQC1Value.setText("");
//        txtStockUQC2Value.setText("");
//        dateStock.setDate(new Date());
//        txtStockAmount.setText("");
        jTable1.clearSelection();
        btnAdd.setEnabled(companyUserRole.isProductCreate());
//        CheckNillRated.setSelected(false);
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
        comboGroup.setSelectedIndex(0);
        txtCode.setText("");
        searchClear();

        comboUQC1.setEnabled(true);
        txtUQC2Value.setEnabled(true);
        txtBasePriceUQC2.setEnabled(true);
        labelBasePerUQC2.setEnabled(true);
        txtPriceUQC2.setEnabled(true);
        labelPerUQC2.setEnabled(true);
        comboUQC2.setEnabled(true);
//        txtStockUQC1Value.setEnabled(true);
//        txtStockUQC2Value.setEnabled(true);
//        Boolean isInBetweeFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateStock.getDate(), false);
//        if (!isInBetweeFinancialYear) {
//            dateStock.setDate(companyPolicy.getFinancialYearEnd());
//        }

    }

    public void searchClear() {
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(((DefaultTableModel) jTable1.getModel()));
        sorter.setRowFilter(RowFilter.regexFilter("", 1));
        jTable1.setRowSorter(sorter);
        txtSearch.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProductUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckExempted;
    private javax.swing.JCheckBox ChecknonGst;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDownloadBarcode;
    private javax.swing.JButton btnExcelReport;
    private javax.swing.JButton btnExcelReport1;
    private javax.swing.JButton btnExcelReport2;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnProductDescription;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JCheckBox checkBasePriceTaxExclusive;
    private javax.swing.JCheckBox checkBasePriceTaxInclusive;
    private javax.swing.JCheckBox checkTaxExclusive;
    private javax.swing.JCheckBox checkTaxInclusive;
    private javax.swing.JComboBox<String> comboCGST;
    private javax.swing.JComboBox<String> comboGroup;
    private javax.swing.JComboBox<String> comboIGST;
    private javax.swing.JComboBox<String> comboSGST;
    private javax.swing.JComboBox<String> comboUQC1;
    private javax.swing.JComboBox<String> comboUQC2;
    private javax.swing.JDialog dialogBarcode;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelBasePerUQC1;
    private javax.swing.JLabel labelBasePerUQC2;
    private javax.swing.JLabel labelBasePrice;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelPerUQC1;
    private javax.swing.JLabel labelPerUQC2;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTextField txtBarcodeCount;
    private javax.swing.JTextField txtBasePriceUQC1;
    private javax.swing.JTextField txtBasePriceUQC2;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtHsncode;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPriceUQC1;
    private javax.swing.JTextField txtPriceUQC2;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtShortName;
    private javax.swing.JTextField txtTamilName;
    private javax.swing.JTextField txtUQC2Value;
    // End of variables declaration//GEN-END:variables
}
