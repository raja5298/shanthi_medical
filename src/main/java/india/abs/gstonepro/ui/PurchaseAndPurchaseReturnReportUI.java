/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CreditDebitNoteLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PaymentLogic;
import india.abs.gstonepro.business.PurchaseLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import india.abs.gstonepro.unusedui.PaymentGrpUI;

/**
 *
 * @author Raja
 */
public class PurchaseAndPurchaseReturnReportUI extends javax.swing.JFrame {

    /**
     * Creates new form Purchase And Purchase Return
     */
    Company company = SessionDataUtil.getSelectedCompany();
    AppUser user = SessionDataUtil.getSelectedUser();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<Ledger>(ledgers);
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();
    BigDecimal GrandTotal = new BigDecimal(0);

    public PurchaseAndPurchaseReturnReportUI() {
        try {
            initComponents();
            jButton1.setEnabled(false);
            jButton2.setEnabled(false);
            jButton3.setEnabled(false);
            jButton4.setEnabled(false);
            jButton5.setEnabled(false);
            jButton6.setEnabled(false);
            jButton7.setEnabled(false);
            jButton8.setEnabled(false);
            paymentDate.setDate(new Date());
            //            Image im =Imag2eIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            labelCompanyName.setText(company.getCompanyName());
            labelUserName.setText(user.getUserName());
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });
            tableSales.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            tableSalesReturn.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            this.setExtendedState(this.MAXIMIZED_BOTH);

            setParty();
            comboParty.setSelectedItem("All");
            fetch();
            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
            editor1.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboParty.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();
                        scripts.add("All");
                        for (Ledger ledger : sortedLedgers) {
                            if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(ledger.getLedgerName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);

                        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                            comboParty.setPopupVisible(false);
                            comboParty.setSelectedItem(comboParty.getSelectedItem());
                            btnFetch.requestFocus();
                        } else {
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(PurchaseAndPurchaseReturnReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    public void setParty() {
        comboParty.addItem("All");
        for (Ledger ledger : ledgers) {
            comboParty.addItem(ledger.getLedgerName());
        };
        comboParty.setSelectedItem("All");
    }

    public void fetch() {
        paymentDate.setDate(new Date());
        String partyName = (String) comboParty.getSelectedItem();
        DefaultTableModel modelPurchase = (DefaultTableModel) tableSales.getModel();
        DefaultTableModel modelPurchaseReturn = (DefaultTableModel) tableSalesReturn.getModel();

        DefaultTableModel modelPurchase1 = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel modelPurchaseReturn1 = (DefaultTableModel) tableSalesReturn1.getModel();

        if (modelPurchase.getRowCount() > 0) {
            modelPurchase.setRowCount(0);
        }
        if (modelPurchaseReturn.getRowCount() > 0) {
            modelPurchaseReturn.setRowCount(0);
        }
        if (modelPurchase1.getRowCount() > 0) {
            modelPurchase1.setRowCount(0);
        }
        if (modelPurchaseReturn1.getRowCount() > 0) {
            modelPurchaseReturn1.setRowCount(0);
        }
        Ledger ledger = null;
        for (Ledger l : ledgers) {
            if (l.getLedgerName().equals(partyName)) {
                ledger = l;
                break;
            }
        }
//        if (ledger != null) {
            List<PurchaseInvoice> Purchases = new PurchaseLogic().fetchAllPurchaseByParty(ledger);
            BigDecimal totalPurchaseAmount = new BigDecimal(0), totalPurchasePaidAmount = new BigDecimal(0);
            for (PurchaseInvoice ps : Purchases) {
                Object[] row = {ps.getPurchaseInvoiceDate(), ps.getPurchaseInvoiceNo(), ps.getCalculatedPurchaseInvoiceAmount(), ps.getTotalAmountPaid(), ps.getPurchaseInvoiceId()};
                totalPurchaseAmount = totalPurchaseAmount.add(ps.getCalculatedPurchaseInvoiceAmount());

                totalPurchasePaidAmount = totalPurchasePaidAmount.add(ps.getTotalAmountPaid());
                modelPurchase.addRow(row);
            }

//            labelPurchaseTotal.setText("Total Amount : " + totalPurchaseAmount.toString() + "   Total Amount Paid : " + totalPurchasePaidAmount.toString());
            BigDecimal RemaingPurchaseAmount = totalPurchaseAmount.subtract(totalPurchasePaidAmount);
//            String strRemaingPurchaseAmount="";
            if (RemaingPurchaseAmount.compareTo(BigDecimal.ZERO) < 0) {
                //strRemaingPurchaseAmount="Purchase Receivable Amount : ";
//                 strRemaingPurchaseAmount="Cr : ";
                //labelPurchaseBalance.setText("Receivable Amount : " + RemaingPurchaseAmount.negate().toString());
//                labelPurchaseBalance.setText("Balance : " + RemaingPurchaseAmount.negate().toString() + " Cr");
//                labelRemainingPurchaseBalance.setText(RemaingPurchaseAmount.negate().toString() + " Cr");
            } else if (RemaingPurchaseAmount.compareTo(BigDecimal.ZERO) > 0) {
//                strRemaingPurchaseAmount="Purchase Payable Amount : ";
//                strRemaingPurchaseAmount="Dr : ";
//                labelPurchaseBalance.setText("Payable Amount : " + RemaingPurchaseAmount.toString());
//                labelPurchaseBalance.setText("Balance : " + RemaingPurchaseAmount.toString() + " Dr");
//                labelRemainingPurchaseBalance.setText(RemaingPurchaseAmount.toString() + " Dr");
            } else {
                //strRemaingPurchaseAmount="Purchase Receivable Amount : ";
//                strRemaingPurchaseAmount="Cr : ";
//                labelPurchaseBalance.setText("Balance : 0.00 Cr");
//                labelRemainingPurchaseBalance.setText("0.00 Cr");
            }
            tableSales.getColumnModel().getColumn(0).setCellRenderer(new DateTableCellRenderer());
            tableSales.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
            tableSales.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());

            List<CreditDebitNote> PurchaseReturns = new CreditDebitNoteLogic().getAllCreditDebitNoteByType("DEBIT-NOTE", ledger);
            BigDecimal totalPRAmount = new BigDecimal(0), totalPRPaidAmount = new BigDecimal(0);

            for (CreditDebitNote ps : PurchaseReturns) {
                Object[] row = {ps.getNoteDate(), ps.getCdNoteNo(), ps.getNoteValueInclusiveOfTax(), ps.getTotalAmountPaid(), ps.getCdNoteId()};
                totalPRAmount = totalPRAmount.add(ps.getNoteValueInclusiveOfTax());
                totalPRPaidAmount = totalPRPaidAmount.add(ps.getTotalAmountPaid());
                modelPurchaseReturn.addRow(row);
            }
//            labelPRTotal.setText("Total Amount : " + totalPRAmount.toString() + "   Total Amount Paid : " + totalPRPaidAmount.toString());
//            BigDecimal RemaingPRAmount = totalPRPaidAmount.subtract(totalPRAmount);
//            String strRemaingPRAmount="";
//            if (RemaingPRAmount.compareTo(BigDecimal.ZERO) < 0) {
            //strRemaingPRAmount="Purchase Return Receivable Amount : ";
//                 strRemaingPRAmount="Cr : ";
//                labelPRBalance.setText("Receivable Amount : " + RemaingPRAmount.negate().toString());
//                labelPRBalance.setText("Balance : " + RemaingPRAmount.negate().toString() + " Cr");
//                labelRemainingPRBalance.setText(RemaingPRAmount.negate().toString() + " Cr");
//            } else if (RemaingPRAmount.compareTo(BigDecimal.ZERO) > 0) {
//                strRemaingPRAmount="Purchase Return Payable Amount : ";
//                strRemaingPRAmount="Dr : ";
//                labelPRBalance.setText("Payable Amount : " + RemaingPRAmount.toString());
//                labelPRBalance.setText("Balance : " + RemaingPRAmount.toString() + " Dr");
//                labelRemainingPRBalance.setText(RemaingPRAmount.toString() + " Dr");
//            } else {
//                strRemaingPRAmount="Purchase Return Receivable Amount : ";
//                strRemaingPRAmount="Cr : ";
//                labelPRBalance.setText("Balance Amount : 0.00");
//                labelRemainingPRBalance.setText("0.00");
//            }
            tableSalesReturn.getColumnModel().getColumn(0).setCellRenderer(new DateTableCellRenderer());
            tableSalesReturn.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
            tableSalesReturn.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());

//            labelStrPurchase.setText(strRemaingPurchaseAmount);
//            labelStrPR.setText(strRemaingPRAmount);
//            BigDecimal RemainingAmount = RemaingPRAmount.add(RemaingPurchaseAmount);
//            if (RemainingAmount.compareTo(BigDecimal.ZERO) < 0) {
//                //labelFinal.setText("Balance Receivable Amount : ");
//                //labelFinal.setText("Cr : ");
//                labelFinalAmount.setText(RemainingAmount.negate().toString() + " Cr");
//            } else if (RemainingAmount.compareTo(BigDecimal.ZERO) > 0) {
////                labelFinal.setText("Balance Payable Amount : ");
//                //labelFinal.setText("Dr : ");
//                labelFinalAmount.setText(RemainingAmount.toString() + " Dr");
//            } else {
//                //labelFinal.setText("Balance :");
//                labelRemainingPRBalance.setText("0.00 Cr");
//            }
//        } else {
//            JOptionPane.showMessageDialog(null, "Please Select Correct Party", "Alert", JOptionPane.WARNING_MESSAGE);
//        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboParty = new javax.swing.JComboBox<>();
        btnFetch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableSalesReturn = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableSales = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        labelStrPurchase = new javax.swing.JLabel();
        labelStrPR = new javax.swing.JLabel();
        labelRemainingPurchaseBalance = new javax.swing.JLabel();
        labelRemainingPRBalance = new javax.swing.JLabel();
        labelFinal = new javax.swing.JLabel();
        labelFinalAmount = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableSales1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableSalesReturn1 = new javax.swing.JTable();
        jButton5 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelSaleBillTot = new javax.swing.JLabel();
        Cr = new javax.swing.JLabel();
        Cr3 = new javax.swing.JLabel();
        labelSaleRvdAmt = new javax.swing.JLabel();
        lblSaleTotal = new javax.swing.JLabel();
        Cr4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblSRBillAmt = new javax.swing.JLabel();
        Cr2 = new javax.swing.JLabel();
        Cr5 = new javax.swing.JLabel();
        Cr6 = new javax.swing.JLabel();
        lblSRTotal = new javax.swing.JLabel();
        lblSRAmtPd = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblgrandTotal = new javax.swing.JLabel();
        Cr1 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jButton9 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        paymentDate = new com.toedter.calendar.JDateChooser();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Purchase Payment");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        comboParty.setEditable(true);

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setMnemonic('f');
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        tableSalesReturn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill Id"
            }
        ));
        tableSalesReturn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesReturnMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableSalesReturn);
        if (tableSalesReturn.getColumnModel().getColumnCount() > 0) {
            tableSalesReturn.getColumnModel().getColumn(4).setMinWidth(0);
            tableSalesReturn.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSalesReturn.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("PURCHASE");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("PURCHASE RETURN");

        tableSales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill Id"
            }
        ));
        tableSales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tableSales);
        if (tableSales.getColumnModel().getColumnCount() > 0) {
            tableSales.getColumnModel().getColumn(4).setMinWidth(0);
            tableSales.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSales.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        labelStrPurchase.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStrPurchase.setText("Purchase");

        labelStrPR.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStrPR.setText("Purchase Return");

        labelRemainingPurchaseBalance.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelRemainingPurchaseBalance.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelRemainingPurchaseBalance.setText("0.00");

        labelRemainingPRBalance.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelRemainingPRBalance.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelRemainingPRBalance.setText("0.00");

        labelFinal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelFinal.setText("Balance");

        labelFinalAmount.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelFinalAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelFinalAmount.setText("0.00");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelFinal)
                    .addComponent(labelStrPR)
                    .addComponent(labelStrPurchase))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelFinalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                    .addComponent(labelRemainingPRBalance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelRemainingPurchaseBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStrPurchase)
                    .addComponent(labelRemainingPurchaseBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStrPR)
                    .addComponent(labelRemainingPRBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelFinal)
                    .addComponent(labelFinalAmount))
                .addContainerGap())
        );

        tableSales1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill Id"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSales1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSales1MouseClicked(evt);
            }
        });
        tableSales1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableSales1KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tableSales1);
        if (tableSales1.getColumnModel().getColumnCount() > 0) {
            tableSales1.getColumnModel().getColumn(4).setMinWidth(0);
            tableSales1.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSales1.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setText(">>");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tableSalesReturn1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill", "Bill Amount", "Amount Paid", "Bill Id"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSalesReturn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSalesReturn1MouseClicked(evt);
            }
        });
        tableSalesReturn1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableSalesReturn1KeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tableSalesReturn1);
        if (tableSalesReturn1.getColumnModel().getColumnCount() > 0) {
            tableSalesReturn1.getColumnModel().getColumn(4).setMinWidth(0);
            tableSalesReturn1.getColumnModel().getColumn(4).setPreferredWidth(0);
            tableSalesReturn1.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jButton5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton5.setText(">>");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Receivable ");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Received ");

        labelSaleBillTot.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelSaleBillTot.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelSaleBillTot.setText("0.00");

        Cr.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr.setText("Cr");

        Cr3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr3.setText("Cr");

        labelSaleRvdAmt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        labelSaleRvdAmt.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelSaleRvdAmt.setText("0.00");

        lblSaleTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblSaleTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSaleTotal.setText("0.00");

        Cr4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cr4.setText("Cr");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Balance ");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Payable ");

        lblSRBillAmt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblSRBillAmt.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRBillAmt.setText("0.00");

        Cr2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr2.setText("Dr");

        Cr5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr5.setText("Dr");

        Cr6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Cr6.setText("Dr");

        lblSRTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblSRTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRTotal.setText("0.00");

        lblSRAmtPd.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblSRAmtPd.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSRAmtPd.setText("0.00");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Paid");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Balance");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel5.setText("Balance");

        lblgrandTotal.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblgrandTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblgrandTotal.setText("0.00");

        Cr1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Cr1.setText("Dr");

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel10.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-");

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setText("<<");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setText(">>|");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton4.setText("|<<");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton6.setText("<<");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton7.setText(">>|");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton8.setText("|<<");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton9.setMnemonic('h');
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        textArea.setColumns(20);
        textArea.setRows(5);
        jScrollPane5.setViewportView(textArea);

        jLabel11.setText("Remarks");

        jLabel12.setText("Payment Date");

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator10);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCompanyName)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(269, 269, 269))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(28, 28, 28)
                                            .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                                .addComponent(jButton2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                                .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel11))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(paymentDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 4, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(labelUserName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel24)
                        .addGap(22, 22, 22)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelSaleBillTot, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSaleTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelSaleRvdAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Cr4)
                                    .addComponent(Cr)
                                    .addComponent(Cr3)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblgrandTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Cr1))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSRBillAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSRAmtPd, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSRTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Cr2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Cr5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Cr6)))
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelSaleBillTot, labelSaleRvdAmt, lblSRAmtPd, lblSRBillAmt, lblSRTotal, lblSaleTotal, lblgrandTotal});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {Cr, Cr1, Cr2, Cr3, Cr4, Cr5, Cr6});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jScrollPane1, jScrollPane2, jScrollPane3, jScrollPane4});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(labelCompanyName))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(115, 115, 115)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton4))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnFetch))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addGap(11, 11, 11)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUserName)
                            .addComponent(jLabel24)
                            .addComponent(jButton9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelSaleBillTot)
                                    .addComponent(jLabel3)
                                    .addComponent(Cr))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelSaleRvdAmt)
                                    .addComponent(jLabel6)
                                    .addComponent(Cr3))
                                .addGap(7, 7, 7)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(lblSaleTotal)
                                    .addComponent(Cr4))))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(jButton5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton8))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRBillAmt)
                                    .addComponent(jLabel4)
                                    .addComponent(Cr2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRAmtPd)
                                    .addComponent(jLabel8)
                                    .addComponent(Cr5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSRTotal)
                                    .addComponent(jLabel9)
                                    .addComponent(Cr6)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblgrandTotal)
                            .addComponent(Cr1)
                            .addComponent(jLabel5))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel12)
                                    .addGap(6, 6, 6))
                                .addComponent(paymentDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel11)
                                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        fetch();
    }//GEN-LAST:event_btnFetchActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        int i = tableSales.getSelectedRow();
        int count = tableSales1.getRowCount();
        String getBillNo = tableSales.getValueAt(i, 1).toString();
        Boolean isHere = false;
        for (int j = 0; j < count; j++) {
            if (getBillNo == tableSales1.getValueAt(j, 1).toString()) {
                JOptionPane.showMessageDialog(null, "Bill is Already Here", "Alert", JOptionPane.WARNING_MESSAGE);
                isHere = true;
            }
        }

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);
        String amounttoSalesString;
        paidAmountfromSales = new BigDecimal(tableSales.getValueAt(i, 3).toString());
        amountfromSales = new BigDecimal(tableSales.getValueAt(i, 2).toString());
        if (tableSales.getValueAt(i, 3).toString() == "0.00") {
            amounttoSalesString = tableSales.getValueAt(i, 2).toString();
        } else {
            amounttoSales = amountfromSales.subtract(paidAmountfromSales);
            amounttoSalesString = String.valueOf(amounttoSales);
        }

        if (!isHere) {
            Object[] row = {tableSales.getValueAt(i, 0).toString(), tableSales.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSales.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);
//        }
            BigDecimal amountAfterAdd = new BigDecimal(0);
            BigDecimal receivedAfterAdd = new BigDecimal(0);
            BigDecimal balanceAfterAdd = new BigDecimal(0);
            int countAfterInsertRow = tableSales1.getRowCount();
            BigDecimal totSaleAmtLoc = new BigDecimal(0);
            labelSaleBillTot.setText("0.00");
            labelSaleRvdAmt.setText("0.00");
            for (int j = 0; j < countAfterInsertRow; j++) {
//                totSaleAmtLoc = totSaleAmtLoc.add(new BigDecimal(tableSales1.getValueAt(j, 2).toString()));
////            }
//            labelSaleBillTot.setText(totSaleAmtLoc.toString());

//            for (int k = 0; k < count; k++) {
                amountAfterAdd = new BigDecimal(labelSaleBillTot.getText()).add(new BigDecimal(tableSales1.getValueAt(j, 2).toString()));
                labelSaleBillTot.setText(String.valueOf(amountAfterAdd));

                receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSales1.getValueAt(j, 3).toString()));
                labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
            }

            balanceAfterAdd = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText()));
            lblSaleTotal.setText(String.valueOf(balanceAfterAdd));
            labelRemainingPurchaseBalance.setText(String.valueOf(balanceAfterAdd));

            lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));

            if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
                Cr1.setText("Cr");
            } else {
                Cr1.setText("Dr");
                lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
                labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            }
        }
        tableSales.clearSelection();
        jButton1.setEnabled(false);
        jButton3.setEnabled(false);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();

        int i = tableSalesReturn.getSelectedRow();

        int count = tableSalesReturn1.getRowCount();

        String getBillNo = tableSalesReturn.getValueAt(i, 1).toString();

        Boolean isHere = false;
        for (int j = 0; j < count; j++) {
            if (getBillNo == tableSalesReturn1.getValueAt(j, 1).toString()) {
                JOptionPane.showMessageDialog(null, "Bill is Already Here", "Alert", JOptionPane.WARNING_MESSAGE);
                isHere = true;
            }
        }

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);
        String amounttoSalesString;
        paidAmountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 3).toString());
        amountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 2).toString());
        if (tableSalesReturn.getValueAt(i, 3).toString() == "0.00") {
            amounttoSalesString = tableSalesReturn.getValueAt(i, 2).toString();
        } else {
            amounttoSales = amountfromSales.subtract(paidAmountfromSales);
            amounttoSalesString = String.valueOf(amounttoSales);
        }

        if (!isHere) {
            Object[] row = {tableSalesReturn.getValueAt(i, 0).toString(), tableSalesReturn.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSalesReturn.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            BigDecimal amountAfterAdd = new BigDecimal(0);
            BigDecimal receivedAfterAdd = new BigDecimal(0);
            BigDecimal balanceAfterAdd = new BigDecimal(0);
            int countAfterInsertRow = tableSalesReturn1.getRowCount();
            BigDecimal totSaleAmtLoc = new BigDecimal(0);
            lblSRBillAmt.setText("0.00");
            lblSRBillAmt.setText("0.00");
            for (int j = 0; j < countAfterInsertRow; j++) {
//                totSaleAmtLoc = totSaleAmtLoc.add(new BigDecimal(tableSalesReturn1.getValueAt(j, 2).toString()));
                amountAfterAdd = new BigDecimal(lblSRBillAmt.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(j, 2).toString()));
                lblSRBillAmt.setText(String.valueOf(amountAfterAdd));

                receivedAfterAdd = new BigDecimal(lblSRAmtPd.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(j, 3).toString()));
                lblSRAmtPd.setText(String.valueOf(receivedAfterAdd));
            }

            balanceAfterAdd = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText()));
            lblSRTotal.setText(String.valueOf(balanceAfterAdd));
            labelRemainingPRBalance.setText(String.valueOf(balanceAfterAdd));

            lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
            if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
                Cr1.setText("Cr");
            } else {
                Cr1.setText("Dr");
                lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
                labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            }
        }
        tableSalesReturn.clearSelection();
        jButton1.setEnabled(false);
        jButton3.setEnabled(false);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void tableSales1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableSales1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            BigDecimal totSaleAmtPaid = new BigDecimal(0);
            int countAfterInsertRow = tableSales1.getRowCount();
            for (int j = 0; j < countAfterInsertRow; j++) {
                totSaleAmtPaid = totSaleAmtPaid.add(new BigDecimal(tableSales1.getValueAt(j, 3).toString()));
            }
            labelSaleRvdAmt.setText(totSaleAmtPaid.toString());
        }

        lblSaleTotal.setText(new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText())).toString());
        labelRemainingPurchaseBalance.setText(new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText())).toString());

        GrandTotal = new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()));
        lblgrandTotal.setText(GrandTotal.toString());
        labelFinalAmount.setText(GrandTotal.toString());

        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
    }//GEN-LAST:event_tableSales1KeyPressed

    private void tableSalesReturn1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableSalesReturn1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            BigDecimal totSaleAmtPaid = new BigDecimal(0);
            int countAfterInsertRow = tableSalesReturn1.getRowCount();
            for (int j = 0; j < countAfterInsertRow; j++) {
                totSaleAmtPaid = totSaleAmtPaid.add(new BigDecimal(tableSalesReturn1.getValueAt(j, 3).toString()));
            }
            lblSRAmtPd.setText(totSaleAmtPaid.toString());
        }

        lblSRTotal.setText(new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText())).toString());
        labelRemainingPRBalance.setText(new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText())).toString());

        GrandTotal = new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()));
        lblgrandTotal.setText(GrandTotal.toString());
        labelFinalAmount.setText(GrandTotal.toString());

        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
    }//GEN-LAST:event_tableSalesReturn1KeyPressed

    public void newJournal() throws ParseException {
        List<PurchaseInvoice> purchasesale = new ArrayList<>();
        List<CreditDebitNote> cdnList = new ArrayList<>();
        DefaultTableModel model = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel model1 = (DefaultTableModel) tableSalesReturn1.getModel();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date;
        for (int i = 0; i < model.getRowCount(); i++) {
            PurchaseInvoice ps = new PurchaseInvoice();
            ps.setPurchaseInvoiceId(model.getValueAt(i, 4).toString());
            ps.setTotalAmountPaid(new BigDecimal(model.getValueAt(i, 3).toString()));
            purchasesale.add(ps);
        }

        for (int i = 0; i < model1.getRowCount(); i++) {
            CreditDebitNote ps1 = new CreditDebitNote();
//            date = sdf.parse(model1.getValueAt(i, 0).toString());
            ps1.setCdNoteId(model1.getValueAt(i, 4).toString());
            ps1.setTotalAmountPaid(new BigDecimal(model1.getValueAt(i, 3).toString()));
            cdnList.add(ps1);
        }
        Ledger selectedLedger = null;
        String lgName = comboParty.getSelectedItem().toString();
        for (Ledger lg : ledgers) {
            if (lg.getLedgerName().equals(lgName)) {
                selectedLedger = lg;
                break;
            }
        }
        String str = textArea.getText();
        EventStatus isSuccess = new PurchaseSaleLogic().updatePurchasePayment(purchasesale, selectedLedger, cdnList, str,paymentDate.getDate());
        if (isSuccess.isUpdateDone()) {
            JOptionPane.showMessageDialog(null, "Payment is Successfull", "Message", JOptionPane.INFORMATION_MESSAGE);
            resettable();
            fetch();
            paymentDate.setDate(new Date());
        } else {
            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void resettable() {
        DefaultTableModel model = (DefaultTableModel) tableSales1.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        DefaultTableModel model1 = (DefaultTableModel) tableSalesReturn1.getModel();
        int rowCount1 = model1.getRowCount();
        for (int i = rowCount1 - 1; i >= 0; i--) {
            model1.removeRow(i);
        }
    }


    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            newJournal();
        } catch (ParseException ex) {
            Logger.getLogger(SalesAndSalesReturnReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        int i = tableSales1.getSelectedRow();

        BigDecimal amountAfterDelete = new BigDecimal(0);
        BigDecimal receivedAfterDelete = new BigDecimal(0);
        BigDecimal balanceAfterDelete = new BigDecimal(0);

        amountAfterDelete = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(tableSales1.getValueAt(i, 2).toString()));
        labelSaleBillTot.setText(String.valueOf(amountAfterDelete));

        receivedAfterDelete = new BigDecimal(labelSaleRvdAmt.getText()).subtract(new BigDecimal(tableSales1.getValueAt(i, 3).toString()));
        labelSaleRvdAmt.setText(String.valueOf(receivedAfterDelete));

        balanceAfterDelete = new BigDecimal(labelSaleBillTot.getText()).subtract(new BigDecimal(labelSaleRvdAmt.getText()));
        lblSaleTotal.setText(String.valueOf(balanceAfterDelete));
        labelRemainingPurchaseBalance.setText(String.valueOf(balanceAfterDelete));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        salesTable1.removeRow(i);
        tableSales1.clearSelection();
        jButton2.setEnabled(false);
        jButton4.setEnabled(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        int i = tableSalesReturn1.getSelectedRow();

        BigDecimal amountAfterDelete = new BigDecimal(0);
        BigDecimal receivedAfterDelete = new BigDecimal(0);
        BigDecimal balanceAfterDelete = new BigDecimal(0);

        amountAfterDelete = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(tableSalesReturn1.getValueAt(i, 2).toString()));
        lblSRBillAmt.setText(String.valueOf(amountAfterDelete));

        receivedAfterDelete = new BigDecimal(lblSRAmtPd.getText()).subtract(new BigDecimal(tableSalesReturn1.getValueAt(i, 3).toString()));
        lblSRAmtPd.setText(String.valueOf(receivedAfterDelete));

        balanceAfterDelete = new BigDecimal(lblSRBillAmt.getText()).subtract(new BigDecimal(lblSRAmtPd.getText()));
        lblSRTotal.setText(String.valueOf(balanceAfterDelete));
        labelRemainingPRBalance.setText(String.valueOf(balanceAfterDelete));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        salesTable1.removeRow(i);
        tableSalesReturn1.clearSelection();
        jButton2.setEnabled(false);
        jButton4.setEnabled(false);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        DefaultTableModel salesTable = (DefaultTableModel) tableSalesReturn.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        lblSRBillAmt.setText("0.00");
        lblSRAmtPd.setText("0.00");
        lblSRTotal.setText("0.00");
        labelRemainingPRBalance.setText("0.00");

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);

        BigDecimal amountAfterAdd = new BigDecimal(0);
        BigDecimal receivedAfterAdd = new BigDecimal(0);
        BigDecimal balanceAfterAdd = new BigDecimal(0);
        String amounttoSalesString;

        String getBillIdfromSales = "", getBillIdfromSales1;

        for (int i = 0; i < tableSalesReturn.getRowCount(); i++) {
            paidAmountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 3).toString());
            amountfromSales = new BigDecimal(tableSalesReturn.getValueAt(i, 2).toString());
            if (tableSalesReturn.getValueAt(i, 3).toString() == "0.00") {
                amounttoSalesString = tableSalesReturn.getValueAt(i, 2).toString();

            } else {
                amounttoSales = amountfromSales.subtract(paidAmountfromSales);
                amounttoSalesString = String.valueOf(amounttoSales);
            }

            Object[] row = {tableSalesReturn.getValueAt(i, 0).toString(), tableSalesReturn.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSalesReturn.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            amountAfterAdd = amountAfterAdd.add(new BigDecimal(tableSalesReturn1.getValueAt(i, 2).toString()));

            receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSalesReturn1.getValueAt(i, 3).toString()));
            labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
        }

        lblSRBillAmt.setText(String.valueOf(amountAfterAdd));
        balanceAfterAdd = new BigDecimal(lblSRBillAmt.getText()).add(new BigDecimal(lblSRAmtPd.getText()));
        lblSRTotal.setText(String.valueOf(balanceAfterAdd));
        labelRemainingPRBalance.setText(String.valueOf(balanceAfterAdd));

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        tableSalesReturn.clearSelection();
        jButton5.setEnabled(false);
        jButton7.setEnabled(false);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void tableSalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesMouseClicked
        jButton1.setEnabled(true);
        jButton2.setEnabled(false);
        jButton3.setEnabled(true);
        jButton4.setEnabled(false);
    }//GEN-LAST:event_tableSalesMouseClicked

    private void tableSales1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSales1MouseClicked
        jButton1.setEnabled(false);
        jButton3.setEnabled(false);
        jButton2.setEnabled(true);
        jButton4.setEnabled(true);
    }//GEN-LAST:event_tableSales1MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        labelSaleBillTot.setText("0.00");
        labelSaleRvdAmt.setText("0.00");
        lblSaleTotal.setText("0.00");
        labelRemainingPurchaseBalance.setText("0.00");
        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        tableSales1.clearSelection();
        jButton2.setEnabled(false);
        jButton4.setEnabled(false);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSales1.getModel();
        DefaultTableModel salesTable = (DefaultTableModel) tableSales.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblgrandTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        labelSaleBillTot.setText("0.00");
        labelSaleRvdAmt.setText("0.00");
        lblSaleTotal.setText("0.00");
        labelRemainingPurchaseBalance.setText("0.00");

        BigDecimal amountfromSales = new BigDecimal(0);
        BigDecimal paidAmountfromSales = new BigDecimal(0);
        BigDecimal amounttoSales = new BigDecimal(0);

        BigDecimal amountAfterAdd = new BigDecimal(0);
        BigDecimal receivedAfterAdd = new BigDecimal(0);
        BigDecimal balanceAfterAdd = new BigDecimal(0);
        String amounttoSalesString;

        String getBillIdfromSales = "", getBillIdfromSales1;

        for (int i = 0; i < tableSales.getRowCount(); i++) {
            paidAmountfromSales = new BigDecimal(tableSales.getValueAt(i, 3).toString());
            amountfromSales = new BigDecimal(tableSales.getValueAt(i, 2).toString());
            if (tableSales.getValueAt(i, 3).toString() == "0.00") {
                amounttoSalesString = tableSales.getValueAt(i, 2).toString();

            } else {
                amounttoSales = amountfromSales.subtract(paidAmountfromSales);
                amounttoSalesString = String.valueOf(amounttoSales);
            }

            Object[] row = {tableSales.getValueAt(i, 0).toString(), tableSales.getValueAt(i, 1).toString(), amounttoSalesString, "0.00", tableSales.getValueAt(i, 4).toString()};
            salesTable1.addRow(row);

            amountAfterAdd = amountAfterAdd.add(new BigDecimal(tableSales1.getValueAt(i, 2).toString()));

            receivedAfterAdd = new BigDecimal(labelSaleRvdAmt.getText()).add(new BigDecimal(tableSales1.getValueAt(i, 3).toString()));
            labelSaleRvdAmt.setText(String.valueOf(receivedAfterAdd));
        }

        labelSaleBillTot.setText(String.valueOf(amountAfterAdd));
        balanceAfterAdd = new BigDecimal(labelSaleBillTot.getText()).add(new BigDecimal(labelSaleRvdAmt.getText()));
        lblSaleTotal.setText(String.valueOf(balanceAfterAdd));
        labelRemainingPurchaseBalance.setText("0.00");

        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSRTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }
        tableSales.clearSelection();
        jButton1.setEnabled(false);
        jButton3.setEnabled(false);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        DefaultTableModel salesTable1 = (DefaultTableModel) tableSalesReturn1.getModel();
        if (salesTable1.getRowCount() > 0) {
            salesTable1.setRowCount(0);
        }

        lblSRBillAmt.setText("0.00");
        lblSRAmtPd.setText("0.00");
        lblSRTotal.setText("0.00");
        labelRemainingPRBalance.setText("0.00");
        lblgrandTotal.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        labelFinalAmount.setText(String.valueOf(new BigDecimal(lblSaleTotal.getText()).subtract(new BigDecimal(lblSaleTotal.getText()))));
        if (new BigDecimal(lblSaleTotal.getText()).compareTo(new BigDecimal(lblSRTotal.getText())) > 0) {
            Cr1.setText("Cr");
        } else {
            Cr1.setText("Dr");
            lblgrandTotal.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
            labelFinalAmount.setText(new BigDecimal(lblgrandTotal.getText()).multiply(new BigDecimal(1).negate()).toString());
        }

        tableSalesReturn1.clearSelection();
        jButton6.setEnabled(false);
        jButton8.setEnabled(false);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        tableSales.clearSelection();
        tableSales1.clearSelection();
        jButton2.setEnabled(false);
        jButton4.setEnabled(false);
        jButton1.setEnabled(false);
        jButton3.setEnabled(false);

        tableSalesReturn.clearSelection();
        tableSalesReturn1.clearSelection();
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setEnabled(false);
        jButton8.setEnabled(false);
    }//GEN-LAST:event_formMouseClicked

    private void tableSalesReturn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesReturn1MouseClicked
        jButton5.setEnabled(false);
        jButton6.setEnabled(true);
        jButton7.setEnabled(false);
        jButton8.setEnabled(true);
    }//GEN-LAST:event_tableSalesReturn1MouseClicked

    private void tableSalesReturnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableSalesReturnMouseClicked
        jButton5.setEnabled(true);
        jButton6.setEnabled(false);
        jButton7.setEnabled(true);
        jButton8.setEnabled(false);
    }//GEN-LAST:event_tableSalesReturnMouseClicked

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PurchaseAndPurchaseReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PurchaseAndPurchaseReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PurchaseAndPurchaseReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PurchaseAndPurchaseReturnReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PurchaseAndPurchaseReturnReportUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Cr;
    private javax.swing.JLabel Cr1;
    private javax.swing.JLabel Cr2;
    private javax.swing.JLabel Cr3;
    private javax.swing.JLabel Cr4;
    private javax.swing.JLabel Cr5;
    private javax.swing.JLabel Cr6;
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelFinal;
    private javax.swing.JLabel labelFinalAmount;
    private javax.swing.JLabel labelRemainingPRBalance;
    private javax.swing.JLabel labelRemainingPurchaseBalance;
    private javax.swing.JLabel labelSaleBillTot;
    private javax.swing.JLabel labelSaleRvdAmt;
    private javax.swing.JLabel labelStrPR;
    private javax.swing.JLabel labelStrPurchase;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lblSRAmtPd;
    private javax.swing.JLabel lblSRBillAmt;
    private javax.swing.JLabel lblSRTotal;
    private javax.swing.JLabel lblSaleTotal;
    private javax.swing.JLabel lblgrandTotal;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private com.toedter.calendar.JDateChooser paymentDate;
    private javax.swing.JTable tableSales;
    private javax.swing.JTable tableSales1;
    private javax.swing.JTable tableSalesReturn;
    private javax.swing.JTable tableSalesReturn1;
    private javax.swing.JTextArea textArea;
    // End of variables declaration//GEN-END:variables
}
