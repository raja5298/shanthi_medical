/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author Admin
 */
public class GSTpercentageReport extends javax.swing.JFrame {

    /**
     * Creates new form GSTR1UI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();
    HashMap<String, Integer> stateCode = null;

    public GSTpercentageReport() {
        initComponents();
        //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        setMenuRoles();
        jTabbedPane1.setTabPlacement(JTabbedPane.BOTTOM);
//        jTabbedPane1.setEnabledAt(3, true);
//        jTabbedPane1.setEnabledAt(4, true);
//        jTabbedPane1.setEnabledAt(5, false);
//        jTabbedPane1.setEnabledAt(6, false);
//        jTabbedPane1.setEnabledAt(7, false);
//        jTabbedPane1.setEnabledAt(8, false);
//        jTabbedPane1.setEnabledAt(10, false);
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        setExtendedState(GSTpercentageReport.MAXIMIZED_BOTH);
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MMM-yy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
        labelCompanyName1.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
        labelUserName1.setText(SessionDataUtil.getSelectedUser().getUserName());
        Date date = new Date();
        dateFrom.setDate(date);
        dateTo.setDate(date);
        dateFrom.requestFocusInWindow();
        ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                char vChar = evt.getKeyChar();
                boolean dot = false;

                if (dot == false) {
                    if (vChar == '.') {
                        dot = true;
                    } else if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();
                    }
                } else {
                    if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();

                    }
                }
                if (Character.isDigit(vChar)) {
                    String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                    int len = strDate.length();
                    if (len == 1) {
                        ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                        evt.consume();
                    } else if (len == 4) {
                        ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                        evt.consume();
                    }
                }
            }
        });
        ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                        sft.setLenient(false);
                        String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                        if (!(strDate.trim().equals(""))) {
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                dateTo.requestFocusInWindow();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } else {
                            JOptionPane.showMessageDialog(null, "Please Select the Date");
                        }
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                        Logger.getLogger(GSTpercentageReport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                char vChar = evt.getKeyChar();
                boolean dot = false;

                if (dot == false) {
                    if (vChar == '.') {
                        dot = true;
                    } else if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();
                    }
                } else {
                    if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();

                    }
                }
                if (Character.isDigit(vChar)) {
                    String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                    int len = strDate.length();
                    if (len == 1) {
                        ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                        evt.consume();
                    } else if (len == 4) {
                        ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                        evt.consume();
                    }
                }
            }
        });
        ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                        sft.setLenient(false);
                        String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                        if (!(strDate.trim().equals(""))) {
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                btnFetch.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } else {
                            JOptionPane.showMessageDialog(null, "Please Select the Date");
                        }
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                        Logger.getLogger(GSTpercentageReport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
//        } catch (Exception ex) {
//            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);

    }

    public final void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public final void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane14 = new javax.swing.JScrollPane();
        tableCdnr = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelCGST0 = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        tblCGST0 = new javax.swing.JTable();
        totCgst0 = new javax.swing.JTextField();
        totSgst0 = new javax.swing.JTextField();
        totAmount0 = new javax.swing.JTextField();
        totAmountInclTax0 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        panelIGST0 = new javax.swing.JPanel();
        jScrollPane25 = new javax.swing.JScrollPane();
        tblIGST0 = new javax.swing.JTable();
        totIgst0 = new javax.swing.JTextField();
        totAmountIgst0 = new javax.swing.JTextField();
        totAmountInclTaxIgst0 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panelCGST5 = new javax.swing.JPanel();
        jScrollPane17 = new javax.swing.JScrollPane();
        tblCGST5 = new javax.swing.JTable();
        totCgst5 = new javax.swing.JTextField();
        totSgst5 = new javax.swing.JTextField();
        totAmount5 = new javax.swing.JTextField();
        totAmountInclTax5 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        panelIGST5 = new javax.swing.JPanel();
        jScrollPane26 = new javax.swing.JScrollPane();
        tblIGST5 = new javax.swing.JTable();
        totIgst5 = new javax.swing.JTextField();
        totAmountIgst5 = new javax.swing.JTextField();
        totAmountInclTaxIgst5 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        panelCGST12 = new javax.swing.JPanel();
        jScrollPane19 = new javax.swing.JScrollPane();
        tblCGST12 = new javax.swing.JTable();
        totCgst12 = new javax.swing.JTextField();
        totSgst12 = new javax.swing.JTextField();
        totAmount12 = new javax.swing.JTextField();
        totAmountInclTax12 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        panelIGST12 = new javax.swing.JPanel();
        jScrollPane20 = new javax.swing.JScrollPane();
        tblIGST12 = new javax.swing.JTable();
        totIgst12 = new javax.swing.JTextField();
        totAmountIgst12 = new javax.swing.JTextField();
        totAmountInclTaxIgst12 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        panelCGST18 = new javax.swing.JPanel();
        jScrollPane21 = new javax.swing.JScrollPane();
        tblCGST18 = new javax.swing.JTable();
        totCgst18 = new javax.swing.JTextField();
        totSgst18 = new javax.swing.JTextField();
        totAmount18 = new javax.swing.JTextField();
        totAmountInclTax18 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        panelIGST18 = new javax.swing.JPanel();
        jScrollPane22 = new javax.swing.JScrollPane();
        tblIGST18 = new javax.swing.JTable();
        totIgst18 = new javax.swing.JTextField();
        totAmountIgst18 = new javax.swing.JTextField();
        totAmountInclTaxIgst18 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        panelCGST28 = new javax.swing.JPanel();
        jScrollPane23 = new javax.swing.JScrollPane();
        tblCGST28 = new javax.swing.JTable();
        totCgst28 = new javax.swing.JTextField();
        totSgst28 = new javax.swing.JTextField();
        totAmount28 = new javax.swing.JTextField();
        totAmountInclTax28 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        panelIGST28 = new javax.swing.JPanel();
        jScrollPane24 = new javax.swing.JScrollPane();
        tblIGST28 = new javax.swing.JTable();
        totIgst28 = new javax.swing.JTextField();
        totAmountIgst28 = new javax.swing.JTextField();
        totAmountInclTaxIgst28 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        dateTo = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        btnFetch = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        labelUserName1 = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        labelCompanyName1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        tableCdnr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GSTIN/UIN of Recipient ", "Invoice/Advance Receipt Number ", "Invoice/Advance Receipt date ", "Note/Refund Voucher Number ", "Note/Refund Voucher date ", "Document Type ", "Reason For Issuing document ", "Place Of Supply ", "Note/Refund Voucher Value ", "Rate ", "Taxable Value ", "Cess Amount ", "Pre GST ", "cdnNumber"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane14.setViewportView(tableCdnr);
        if (tableCdnr.getColumnModel().getColumnCount() > 0) {
            tableCdnr.getColumnModel().getColumn(13).setMinWidth(0);
            tableCdnr.getColumnModel().getColumn(13).setPreferredWidth(0);
            tableCdnr.getColumnModel().getColumn(13).setMaxWidth(0);
        }

        jLabel7.setText("jLabel7");

        jLabel16.setText("jLabel16");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GSTR1");

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        jTabbedPane1.setFont(new java.awt.Font("Segoe UI", 3, 18)); // NOI18N

        tblCGST0.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblCGST0.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "CGST %", "CGST", "SGST %", "SGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane16.setViewportView(tblCGST0);

        jLabel1.setText("Total CGST");

        jLabel4.setText("Total SGST");

        jLabel5.setText("Total Amount");

        jLabel6.setText("Total Amount(Incl. Tax)");

        javax.swing.GroupLayout panelCGST0Layout = new javax.swing.GroupLayout(panelCGST0);
        panelCGST0.setLayout(panelCGST0Layout);
        panelCGST0Layout.setHorizontalGroup(
            panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCGST0Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST0Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totCgst0))
                        .addGap(70, 70, 70)
                        .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totSgst0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelCGST0Layout.createSequentialGroup()
                                .addComponent(totAmount0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(totAmountInclTax0))
                            .addGroup(panelCGST0Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)))))
                .addContainerGap())
        );

        panelCGST0Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel4, jLabel5, totAmount0, totCgst0, totSgst0});

        panelCGST0Layout.setVerticalGroup(
            panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCGST0Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTax0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmount0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totSgst0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totCgst0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("CGST 0%", panelCGST0);

        tblIGST0.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblIGST0.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "IGST %", "IGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane25.setViewportView(tblIGST0);

        jLabel8.setText("Total IGST");

        jLabel10.setText("Total Amount");

        jLabel11.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelIGST0Layout = new javax.swing.GroupLayout(panelIGST0);
        panelIGST0.setLayout(panelIGST0Layout);
        panelIGST0Layout.setHorizontalGroup(
            panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIGST0Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST0Layout.createSequentialGroup()
                        .addGap(0, 794, Short.MAX_VALUE)
                        .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totIgst0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totAmountIgst0, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGap(20, 20, 20)
                        .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totAmountInclTaxIgst0))))
                .addContainerGap())
        );
        panelIGST0Layout.setVerticalGroup(
            panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST0Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane25, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTaxIgst0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmountIgst0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totIgst0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60))
        );

        jTabbedPane1.addTab("IGST 0%", panelIGST0);

        panelCGST5.setMaximumSize(new java.awt.Dimension(1129, 538));
        panelCGST5.setRequestFocusEnabled(false);

        tblCGST5.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblCGST5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "CGST %", "CGST", "SGST %", "SGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane17.setViewportView(tblCGST5);

        jLabel12.setText("Total CGST");

        jLabel13.setText("Total SGST");

        jLabel14.setText("Total Amount");

        jLabel15.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelCGST5Layout = new javax.swing.GroupLayout(panelCGST5);
        panelCGST5.setLayout(panelCGST5Layout);
        panelCGST5Layout.setHorizontalGroup(
            panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST5Layout.createSequentialGroup()
                .addContainerGap(746, Short.MAX_VALUE)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totCgst5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totSgst5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmount5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTax5, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCGST5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelCGST5Layout.setVerticalGroup(
            panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST5Layout.createSequentialGroup()
                .addContainerGap(367, Short.MAX_VALUE)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTax5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmount5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totSgst5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totCgst5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
            .addGroup(panelCGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCGST5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(111, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("CGST 5%", panelCGST5);

        tblIGST5.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblIGST5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "IGST %", "IGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane26.setViewportView(tblIGST5);

        jLabel41.setText("Total IGST");

        jLabel42.setText("Total Amount");

        jLabel43.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelIGST5Layout = new javax.swing.GroupLayout(panelIGST5);
        panelIGST5.setLayout(panelIGST5Layout);
        panelIGST5Layout.setHorizontalGroup(
            panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST5Layout.createSequentialGroup()
                .addContainerGap(804, Short.MAX_VALUE)
                .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totIgst5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41))
                .addGap(18, 18, 18)
                .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totAmountIgst5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addGap(20, 20, 20)
                .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTaxIgst5))
                .addContainerGap())
            .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane26, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelIGST5Layout.setVerticalGroup(
            panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST5Layout.createSequentialGroup()
                .addContainerGap(365, Short.MAX_VALUE)
                .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(jLabel42)
                    .addComponent(jLabel43))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTaxIgst5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmountIgst5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totIgst5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48))
            .addGroup(panelIGST5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane26, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(105, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("IGST 5%", panelIGST5);

        tblCGST12.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblCGST12.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "CGST %", "CGST", "SGST %", "SGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane19.setViewportView(tblCGST12);

        jLabel19.setText("Total CGST");

        jLabel20.setText("Total SGST");

        jLabel21.setText("Total Amount");

        jLabel22.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelCGST12Layout = new javax.swing.GroupLayout(panelCGST12);
        panelCGST12.setLayout(panelCGST12Layout);
        panelCGST12Layout.setHorizontalGroup(
            panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCGST12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST12Layout.createSequentialGroup()
                        .addGap(0, 728, Short.MAX_VALUE)
                        .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totCgst12, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addGap(18, 18, 18)
                        .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totSgst12, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(totAmount12, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totAmountInclTax12)))
                    .addComponent(jScrollPane19, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelCGST12Layout.setVerticalGroup(
            panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST12Layout.createSequentialGroup()
                .addComponent(jScrollPane19, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTax12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmount12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totSgst12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totCgst12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(74, 74, 74))
        );

        jTabbedPane1.addTab("CGST 12%", panelCGST12);

        tblIGST12.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblIGST12.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "IGST %", "IGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane20.setViewportView(tblIGST12);

        jLabel23.setText("Total IGST");

        jLabel24.setText("Total Amout");

        jLabel26.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelIGST12Layout = new javax.swing.GroupLayout(panelIGST12);
        panelIGST12.setLayout(panelIGST12Layout);
        panelIGST12Layout.setHorizontalGroup(
            panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST12Layout.createSequentialGroup()
                .addContainerGap(804, Short.MAX_VALUE)
                .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totAmountIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(20, 20, 20)
                .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTaxIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST12Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane20, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelIGST12Layout.setVerticalGroup(
            panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST12Layout.createSequentialGroup()
                .addContainerGap(358, Short.MAX_VALUE)
                .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24)
                    .addComponent(jLabel26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTaxIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmountIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totIgst12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55))
            .addGroup(panelIGST12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST12Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(105, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("IGST 12%", panelIGST12);

        tblCGST18.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblCGST18.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "CGST %", "CGST", "SGST %", "SGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane21.setViewportView(tblCGST18);

        jLabel27.setText("Total CGST");

        jLabel28.setText("Total SGST");

        jLabel29.setText("Total Amount");

        jLabel30.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelCGST18Layout = new javax.swing.GroupLayout(panelCGST18);
        panelCGST18.setLayout(panelCGST18Layout);
        panelCGST18Layout.setHorizontalGroup(
            panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST18Layout.createSequentialGroup()
                .addContainerGap(682, Short.MAX_VALUE)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totCgst18, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addGap(74, 74, 74)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totSgst18, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmount18, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTax18))
                .addContainerGap())
            .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST18Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelCGST18Layout.setVerticalGroup(
            panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST18Layout.createSequentialGroup()
                .addContainerGap(358, Short.MAX_VALUE)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(jLabel28)
                    .addComponent(jLabel29)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTax18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmount18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totSgst18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totCgst18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55))
            .addGroup(panelCGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCGST18Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane21, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(105, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("CGST 18%", panelCGST18);

        tblIGST18.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblIGST18.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "IGST %", "IGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane22.setViewportView(tblIGST18);

        jLabel31.setText("Total IGST");

        jLabel32.setText("Total Amount");

        jLabel33.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelIGST18Layout = new javax.swing.GroupLayout(panelIGST18);
        panelIGST18.setLayout(panelIGST18Layout);
        panelIGST18Layout.setHorizontalGroup(
            panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST18Layout.createSequentialGroup()
                .addContainerGap(818, Short.MAX_VALUE)
                .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totIgst18, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(totAmountIgst18, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totAmountInclTaxIgst18, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33))
                .addContainerGap())
            .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST18Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane22, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelIGST18Layout.setVerticalGroup(
            panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST18Layout.createSequentialGroup()
                .addContainerGap(359, Short.MAX_VALUE)
                .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jLabel32)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTaxIgst18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmountIgst18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totIgst18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54))
            .addGroup(panelIGST18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST18Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane22, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(105, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("IGST 18%", panelIGST18);

        tblCGST28.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblCGST28.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "CGST %", "CGST", "SGST %", "SGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane23.setViewportView(tblCGST28);

        jLabel34.setText("Total CGST");

        jLabel35.setText("Total SGST");

        jLabel36.setText("Total Amount");

        jLabel37.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelCGST28Layout = new javax.swing.GroupLayout(panelCGST28);
        panelCGST28.setLayout(panelCGST28Layout);
        panelCGST28Layout.setHorizontalGroup(
            panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST28Layout.createSequentialGroup()
                .addContainerGap(746, Short.MAX_VALUE)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totCgst28, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totSgst28, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmount28, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTax28))
                .addContainerGap())
            .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST28Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane23, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelCGST28Layout.setVerticalGroup(
            panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCGST28Layout.createSequentialGroup()
                .addContainerGap(360, Short.MAX_VALUE)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(jLabel35)
                    .addComponent(jLabel36)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTax28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmount28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totSgst28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totCgst28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53))
            .addGroup(panelCGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelCGST28Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane23, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(105, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("CGST 28%", panelCGST28);

        tblIGST28.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tblIGST28.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Company", "GSTIN", "Product", "Quantity", "Rate", "Rate (Incl Tax)", "IGST %", "IGST", "Amount", "Amount (Incl Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane24.setViewportView(tblIGST28);

        jLabel38.setText("Total IGST");

        jLabel39.setText("Total Amount ");

        jLabel40.setText("Total Amount (Incl. Tax)");

        javax.swing.GroupLayout panelIGST28Layout = new javax.swing.GroupLayout(panelIGST28);
        panelIGST28.setLayout(panelIGST28Layout);
        panelIGST28Layout.setHorizontalGroup(
            panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST28Layout.createSequentialGroup()
                .addContainerGap(804, Short.MAX_VALUE)
                .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addGap(18, 18, 18)
                .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totAmountIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addGap(20, 20, 20)
                .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totAmountInclTaxIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST28Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane24, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        panelIGST28Layout.setVerticalGroup(
            panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIGST28Layout.createSequentialGroup()
                .addContainerGap(375, Short.MAX_VALUE)
                .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(jLabel39)
                    .addComponent(jLabel40))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totAmountInclTaxIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totAmountIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totIgst28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
            .addGroup(panelIGST28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelIGST28Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane24, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(23, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("IGST 28%", panelIGST28);

        jButton1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jButton1.setMnemonic('e');
        jButton1.setText("Download Excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("From");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        dateTo.setDateFormatString("dd-MM-yyyy");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("To");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        jLabel25.setText("User :");

        labelUserName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName1.setText("-");

        btnLogout1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout1.setMnemonic('l');
        btnLogout1.setText("Logout");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });

        jLabel18.setText("Company : ");

        labelCompanyName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName1.setText("----------");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel9.setText("GST1 REPORT");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton2.setMnemonic('h');
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(153, 153, 153)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName1)
                        .addComponent(jLabel25)
                        .addComponent(jButton2))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(labelCompanyName1)
                        .addComponent(jLabel9))
                    .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnFetch)
                        .addComponent(jButton1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("CGST & SGST 0%");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        generateExcel();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        Boolean isInBetweeFinancialYearStart = new CheckFinancialYear().toCheck(companyPolicy, dateFrom.getDate(), false);
        Boolean isInBetweeFinancialYearEnd = new CheckFinancialYear().toCheck(companyPolicy, dateTo.getDate(), false);
        if (isInBetweeFinancialYearStart && isInBetweeFinancialYearEnd) {
            fetch();
        } else {
            dateFrom.setDate(companyPolicy.getFinancialYearStart());
            dateTo.setDate(companyPolicy.getFinancialYearEnd());
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnFetchActionPerformed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed

    public void menuBalanceSheetActionPerformed(java.awt.event.ActionEvent evt) {

    }

    public void menuPurchasePaymentActionPerformed(java.awt.event.ActionEvent evt) {

    }

    public void menuSalesPaymentActionPerformed(java.awt.event.ActionEvent evt) {

    }

    public void menuTradingAccountActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void fetch() {
        List<PurchaseSale> purchaseSales;
        Date fromDate = dateFrom.getDate();
        Date toDate = dateTo.getDate();

        purchaseSales = new PurchaseSaleLogic().getAllPurchaseByDate(fromDate, toDate);
        DefaultTableModel model = (DefaultTableModel) tblCGST0.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        DefaultTableModel modelIGST0 = (DefaultTableModel) tblIGST0.getModel();
        if (modelIGST0.getRowCount() > 0) {
            modelIGST0.setRowCount(0);
        }
        DefaultTableModel modelCGST5 = (DefaultTableModel) tblCGST5.getModel();
        if (modelCGST5.getRowCount() > 0) {
            modelCGST5.setRowCount(0);
        }
        DefaultTableModel modelIGST5 = (DefaultTableModel) tblIGST5.getModel();
        if (modelIGST5.getRowCount() > 0) {
            modelIGST5.setRowCount(0);
        }
        DefaultTableModel modelCGST12 = (DefaultTableModel) tblCGST12.getModel();
        if (modelCGST12.getRowCount() > 0) {
            modelCGST12.setRowCount(0);
        }
        DefaultTableModel modelIGST12 = (DefaultTableModel) tblIGST12.getModel();
        if (modelIGST12.getRowCount() > 0) {
            modelIGST12.setRowCount(0);
        }
        DefaultTableModel modelCGST18 = (DefaultTableModel) tblCGST18.getModel();
        if (modelCGST18.getRowCount() > 0) {
            modelCGST18.setRowCount(0);
        }
        DefaultTableModel modelIGST18 = (DefaultTableModel) tblIGST18.getModel();
        if (modelIGST18.getRowCount() > 0) {
            modelIGST18.setRowCount(0);
        }
        DefaultTableModel modelCGST28 = (DefaultTableModel) tblCGST28.getModel();
        if (modelCGST28.getRowCount() > 0) {
            modelCGST28.setRowCount(0);
        }
        DefaultTableModel modelIGST28 = (DefaultTableModel) tblIGST28.getModel();
        if (modelIGST28.getRowCount() > 0) {
            modelIGST28.setRowCount(0);
        }
        DefaultTableModel selModel;

        JTextField totIgst = null, totCgst = null, totSgst = null, totAmount = null, totAmountIgst = null, totAmountInclTax = null, totAmountInclTaxIgst = null;

        for (PurchaseSale ps : purchaseSales) {
            List<PurchaseSaleLineItem> psLineItem = ps.getPsLineItems();
            String strPartyName = "";
            if (ps.getLedger() != null) {
                strPartyName = ps.getLedger().getLedgerName();
            }

            String strBill = ps.getBillNo();
            for (PurchaseSaleLineItem psItem : psLineItem) {
                BigDecimal totalIgst = new BigDecimal(BigInteger.ZERO);
                BigDecimal totalCgst = new BigDecimal(BigInteger.ZERO);
                BigDecimal totalSgst = new BigDecimal(BigInteger.ZERO);
                BigDecimal totalAmount = new BigDecimal(BigInteger.ZERO);
                BigDecimal totalAmountInclTax = new BigDecimal(BigInteger.ZERO);

                BigDecimal totalIGST = new BigDecimal(0);
                BigDecimal totalCGST = new BigDecimal(0);
                BigDecimal totalSGST = new BigDecimal(0);
                BigDecimal totalAmt = new BigDecimal(0);
                BigDecimal totalAmtInclofTax = new BigDecimal(0);
                
                String strIGST;
                String strTotal;
                String strTotalIncTax;
                String strCGST;
                String strSGST;

                if (ps.isIGST) {
                    int percentage = (int) psItem.getIgstPercentage();
                    switch (percentage) {
                        case 0:
                            selModel = (DefaultTableModel) tblIGST0.getModel();
                            totIgst = totIgst0;
                            totAmountIgst = totAmountIgst0;
                            totAmountInclTaxIgst = totAmountInclTaxIgst0;

                            strIGST = totIgst0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmountIgst0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTaxIgst0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strIGST.equals("")) {
                                totalIGST = new BigDecimal(strIGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 5:
                            selModel = (DefaultTableModel) tblIGST5.getModel();
                            totIgst = totIgst5;
                            totAmountIgst = totAmountIgst5;
                            totAmountInclTaxIgst = totAmountInclTaxIgst5;
                            strIGST = totIgst5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmountIgst5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTaxIgst5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strIGST.equals("")) {
                                totalIGST = new BigDecimal(strIGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 12:
                            selModel = (DefaultTableModel) tblIGST12.getModel();
                            totIgst = totIgst12;
                            totAmountIgst = totAmountIgst12;
                            totAmountInclTaxIgst = totAmountInclTaxIgst12;
                            strIGST = totIgst12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmountIgst12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTaxIgst12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strIGST.equals("")) {
                                totalIGST = new BigDecimal(strIGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 18:
                            selModel = (DefaultTableModel) tblIGST18.getModel();
                            totIgst = totIgst18;
                            totAmountIgst = totAmountIgst18;
                            totAmountInclTaxIgst = totAmountInclTaxIgst18;
                            strIGST = totIgst18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmountIgst18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTaxIgst18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strIGST.equals("")) {
                                totalIGST = new BigDecimal(strIGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 28:
                            selModel = (DefaultTableModel) tblIGST28.getModel();
                            totIgst = totIgst28;
                            totAmountIgst = totAmountIgst28;
                            totAmountInclTaxIgst = totAmountInclTaxIgst28;
                            strIGST = totIgst28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmountIgst28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTaxIgst28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strIGST.equals("")) {
                                totalIGST = new BigDecimal(strIGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        default:
                            selModel = null;
                            totIgst = null;
                            totAmountIgst = null;
                            totAmountInclTaxIgst = null;
                    }
                } else {
                    int percentage = (int) psItem.getIgstPercentage();
                    switch (percentage) {
                        case 0:
                            selModel = (DefaultTableModel) tblCGST0.getModel();
                            totCgst = totCgst0;
                            totSgst = totSgst0;
                            totAmount = totAmount0;
                            totAmountInclTax = totAmountInclTax0;

                            strCGST = totCgst0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strSGST = totSgst0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmount0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTax0.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strCGST.equals("")) {
                                totalCGST = new BigDecimal(strCGST);
                            }
                            if (!strSGST.equals("")) {
                                totalSGST = new BigDecimal(strSGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }

                            break;
                        case 5:
                            selModel = (DefaultTableModel) tblCGST5.getModel();
                            totCgst = totCgst5;
                            totSgst = totSgst5;
                            totAmount = totAmount5;
                            totAmountInclTax = totAmountInclTax5;

                            strCGST = totCgst5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strSGST = totSgst5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmount5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTax5.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strCGST.equals("")) {
                                totalCGST = new BigDecimal(strCGST);
                            }
                            if (!strSGST.equals("")) {
                                totalSGST = new BigDecimal(strSGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 12:
                            selModel = (DefaultTableModel) tblCGST12.getModel();
                            totCgst = totCgst12;
                            totSgst = totSgst12;
                            totAmount = totAmount12;
                            totAmountInclTax = totAmountInclTax12;

                            strCGST = totCgst12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strSGST = totSgst12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmount12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTax12.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strCGST.equals("")) {
                                totalCGST = new BigDecimal(strCGST);
                            }
                            if (!strSGST.equals("")) {
                                totalSGST = new BigDecimal(strSGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 18:
                            selModel = (DefaultTableModel) tblCGST18.getModel();
                            totCgst = totCgst18;
                            totSgst = totSgst18;
                            totAmount = totAmount18;
                            totAmountInclTax = totAmountInclTax18;

                            strCGST = totCgst18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strSGST = totSgst18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmount18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTax18.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strCGST.equals("")) {
                                totalCGST = new BigDecimal(strCGST);
                            }
                            if (!strSGST.equals("")) {
                                totalSGST = new BigDecimal(strSGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        case 28:
                            selModel = (DefaultTableModel) tblCGST28.getModel();
                            totCgst = totCgst28;
                            totSgst = totSgst28;
                            totAmount = totAmount28;
                            totAmountInclTax = totAmountInclTax28;

                            strCGST = totCgst28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strSGST = totSgst28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotal = totAmount28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            strTotalIncTax = totAmountInclTax28.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            if (!strCGST.equals("")) {
                                totalCGST = new BigDecimal(strCGST);
                            }
                            if (!strSGST.equals("")) {
                                totalSGST = new BigDecimal(strSGST);
                            }
                            if (!strTotal.equals("")) {
                                totalAmt = new BigDecimal(strTotal);
                            }
                            if (!strTotalIncTax.equals("")) {
                                totalAmtInclofTax = new BigDecimal(strTotalIncTax);
                            }
                            break;
                        default:
                            selModel = null;
                            totCgst = null;
                            totSgst = null;
                            totAmount = null;
                            totAmountInclTax = null;
                    }
                }

                BigDecimal cessAmt = new BigDecimal(0);
                if (psItem.getCessAmount() != null) {
                    cessAmt = psItem.getCessAmount();
                }
                String strGSTIN = "";
                if ((ps.getLedger() != null) && (ps.getLedger().getGSTIN() != null)) {
                    strGSTIN = ps.getLedger().getGSTIN();
                }
                if (!ps.isIGST) {
                    Object[] row = {
                        ps.getPsDate(),
                        ps.getBillNo(),
                        strPartyName,
                        strGSTIN,
                        psItem.getProduct().getName(),
                        (psItem.getUqcOneQuantity() * psItem.getProduct().getUQC2Value()) + (psItem.getUqcTwoQuantity()),
                        psItem.getUqcTwoRate(),
                        psItem.getUqcTwoRateWithTax(),
                        psItem.getCgstPercentage(),
                        psItem.getCgstValue(),
                        psItem.getSgstPercentage(),
                        psItem.getSgstValue(),
                        psItem.getValue(),
                        psItem.getValue().add(psItem.getCgstValue()).add(psItem.getSgstValue()).add(cessAmt)
                    };
                    if (selModel != null) {
                        selModel.addRow(row);
                    }
                    totalCgst = totalCgst.add(totalCGST).add(psItem.getCgstValue());
                    totCgst.setText(String.valueOf(totalCgst));

                    totalSgst = totalSgst.add(totalSGST).add(psItem.getSgstValue());
                    totSgst.setText(String.valueOf(totalSgst));

                    totalAmount = totalAmount.add(totalAmt).add(psItem.getValue());
                    totAmount.setText(String.valueOf(totalAmount));

                    totalAmountInclTax = totalAmountInclTax.add(totalAmountInclTax).add(psItem.getValue().add(psItem.getCgstValue()).add(psItem.getSgstValue()).add(cessAmt));
                    totAmountInclTax.setText(String.valueOf(totalAmountInclTax));

                } else {
                    Object[] row = {
                        ps.getPsDate(),
                        ps.getBillNo(),
                        strPartyName,
                        strGSTIN,
                        psItem.getProduct().getName(),
                        (psItem.getUqcOneQuantity() * psItem.getProduct().getUQC2Value()) + (psItem.getUqcTwoQuantity()),
                        psItem.getUqcTwoRate(),
                        psItem.getUqcTwoRateWithTax(),
                        psItem.getIgstPercentage(),
                        psItem.getIgstValue(),
                        psItem.getValue(),
                        psItem.getValue().add(psItem.getIgstValue()).add(cessAmt)
                    };
                    if (selModel != null) {
                        selModel.addRow(row);
                    }

                    totalIgst = totalIgst.add(psItem.getIgstValue());
                    totIgst.setText(String.valueOf(totalIgst));

                    totalAmount = totalAmount.add(psItem.getValue());
                    totAmountIgst.setText(String.valueOf(totalAmount));

                    totalAmountInclTax = totalAmountInclTax.add(psItem.getValue().add(psItem.getIgstValue()).add(cessAmt));
                    totAmountInclTaxIgst.setText(String.valueOf(totalAmountInclTax));
                }

            }
        }
    }

    public void generateExcel() {
        try {
            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
            String path = "";
            Boolean isApproved = false;

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            JFrame parentFrame = new JFrame();
            com.alee.laf.WebLookAndFeel.install();
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Report");
            fileChooser.setSelectedFile(new File("Tax-Wise Report " + sft.format(dateFrom.getDate()) + " to " + sft.format(dateTo.getDate())));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                path = fileToSave.getAbsolutePath() + ".xls";
                isApproved = true;
            }
            if (isApproved) {
                File f = new File(path);

                WritableWorkbook myexcel = Workbook.createWorkbook(f);

                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);

                NumberFormat decimalNo = new NumberFormat("#.00");
                WritableCellFormat rightFormat = new WritableCellFormat(decimalNo);
                rightFormat.setFont(wf3);
                rightFormat.setAlignment(Alignment.RIGHT);

                WritableCellFormat headerTotalFormat = new WritableCellFormat(wf2);
                headerTotalFormat.setFont(wf2);

                WritableCellFormat headerFormat = new WritableCellFormat(wf2);
                headerFormat.setFont(wf2);

                WritableCellFormat dateFormat = new jxl.write.WritableCellFormat(new jxl.write.DateFormat("dd/MMM/yy"));
                dateFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat centerFormat = new WritableCellFormat(wf3);
                centerFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat centerFormat1 = new WritableCellFormat(wf2);
                centerFormat1.setAlignment(Alignment.CENTRE);
                centerFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);

                WritableCellFormat rightTotalFormat = new WritableCellFormat(NumberFormats.INTEGER);
                rightTotalFormat.setFont(wf3);
                rightTotalFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat cellFormat = new WritableCellFormat(wf2);
                cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

                WritableSheet cs0p = myexcel.createSheet("Sales CGST 0%", 0);
                setWorkSheetHeader(cs0p, "0", false);
                DefaultTableModel model = (DefaultTableModel) tblCGST0.getModel();
                setWorkSheetData(cs0p, model, false);

                WritableSheet cs5p = myexcel.createSheet("Sales CGST 5%", 1);
                setWorkSheetHeader(cs5p, "5", false);
                model = (DefaultTableModel) tblCGST5.getModel();
                setWorkSheetData(cs5p, model, false);

                WritableSheet cs12p = myexcel.createSheet("Sales CGST 12%", 2);
                setWorkSheetHeader(cs12p, "12", false);
                model = (DefaultTableModel) tblCGST12.getModel();
                setWorkSheetData(cs12p, model, false);

                WritableSheet cs18p = myexcel.createSheet("Sales CGST 18%", 3);
                setWorkSheetHeader(cs18p, "18", false);
                model = (DefaultTableModel) tblCGST18.getModel();
                setWorkSheetData(cs18p, model, false);

                WritableSheet cs28p = myexcel.createSheet("Sales CGST 28%", 4);
                setWorkSheetHeader(cs28p, "28", false);
                model = (DefaultTableModel) tblCGST28.getModel();
                setWorkSheetData(cs28p, model, false);

                WritableSheet g0p = myexcel.createSheet("Sales IGST 0%", 5);
                setWorkSheetHeader(g0p, "0", true);
                model = (DefaultTableModel) tblIGST0.getModel();
                setWorkSheetData(g0p, model, true);

                WritableSheet g5p = myexcel.createSheet("Sales IGST 5%", 6);
                setWorkSheetHeader(g5p, "5", true);
                model = (DefaultTableModel) tblIGST5.getModel();
                setWorkSheetData(g5p, model, true);

                WritableSheet g12p = myexcel.createSheet("Sales IGST 12%", 7);
                setWorkSheetHeader(g12p, "12", true);
                model = (DefaultTableModel) tblIGST12.getModel();
                setWorkSheetData(g12p, model, true);

                WritableSheet g18p = myexcel.createSheet("Sales IGST 18%", 8);
                setWorkSheetHeader(g18p, "18", true);
                model = (DefaultTableModel) tblIGST18.getModel();
                setWorkSheetData(g18p, model, true);

                WritableSheet g28p = myexcel.createSheet("Sales IGST 28%", 9);
                setWorkSheetHeader(g28p, "28", true);
                model = (DefaultTableModel) tblIGST28.getModel();
                setWorkSheetData(g28p, model, true);

                myexcel.write();
                myexcel.close();
                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(path);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        Logger.getLogger(GSTpercentageReport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (WriteException | IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(GSTpercentageReport.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setWorkSheetData(WritableSheet wrksht, DefaultTableModel model, boolean isIGST) {
        try {
            SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
            WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);

            NumberFormat decimalNo = new NumberFormat("#.00");
            WritableCellFormat numFormat = new WritableCellFormat(decimalNo);
            numFormat.setAlignment(Alignment.RIGHT);
            numFormat.setFont(wf3);

            WritableCellFormat rightFormat = new WritableCellFormat();
            rightFormat.setAlignment(Alignment.RIGHT);
            rightFormat.setFont(wf3);

            WritableCellFormat centerFormat = new WritableCellFormat(wf3);
            centerFormat.setAlignment(Alignment.CENTRE);

            WritableCellFormat leftFormat = new WritableCellFormat(wf3);
            leftFormat.setAlignment(Alignment.LEFT);

            WritableCellFormat dateFormat = new jxl.write.WritableCellFormat(new jxl.write.DateFormat("dd/MM/yy"));
            dateFormat.setAlignment(Alignment.CENTRE);

            int nRowCount = model.getRowCount();
            if (nRowCount > 0) {
                for (int i = 0; i < nRowCount; i++) {
                    String strBillDate;
                    String strBillNo;
                    String strParty = null;
                    String strProduct;
                    String strGSTIN;
                    float fQuantity;
                    float fRate;
                    float fRateWithTax;
                    float amt;
                    float amtWithTax;
                    float CGSTper = 0;
                    float CGST = 0;
                    float SGSTper = 0;
                    float SGST = 0;
                    float IGSTper = 0;
                    float IGST = 0;

                    strBillDate = model.getValueAt(i, 0).toString();
                    Date selDate;
                    selDate = sft.parse(strBillDate);
                    strBillNo = model.getValueAt(i, 1).toString();
                    if (model.getValueAt(i, 2) != null) {
                        strParty = model.getValueAt(i, 2).toString();
                    }
                    strGSTIN = model.getValueAt(i, 3).toString();
                    strProduct = model.getValueAt(i, 4).toString();
                    fQuantity = Float.parseFloat(model.getValueAt(i, 5).toString());
                    fRate = Float.parseFloat(model.getValueAt(i, 6).toString());
                    fRateWithTax = Float.parseFloat(model.getValueAt(i, 7).toString());

                    if (isIGST) {
                        IGSTper = Float.parseFloat(model.getValueAt(i, 8).toString());
                        IGST = Float.parseFloat(model.getValueAt(i, 9).toString());
                        amt = Float.parseFloat(model.getValueAt(i, 10).toString());
                        amtWithTax = Float.parseFloat(model.getValueAt(i, 11).toString());
                    } else {
                        CGSTper = Float.parseFloat(model.getValueAt(i, 8).toString());
                        CGST = Float.parseFloat(model.getValueAt(i, 9).toString());
                        SGSTper = Float.parseFloat(model.getValueAt(i, 10).toString());
                        SGST = Float.parseFloat(model.getValueAt(i, 11).toString());
                        amt = Float.parseFloat(model.getValueAt(i, 12).toString());
                        amtWithTax = Float.parseFloat(model.getValueAt(i, 13).toString());
                    }

                    wrksht.addCell(new jxl.write.DateTime(0, (i + 9), selDate, dateFormat));
                    wrksht.addCell(new Label(1, (i + 9), strBillNo, rightFormat));
                    wrksht.addCell(new Label(2, (i + 9), strParty, leftFormat));
                    wrksht.addCell(new Label(3, (i + 9), strGSTIN, leftFormat));
                    wrksht.addCell(new Label(4, (i + 9), strProduct, leftFormat));
                    wrksht.addCell(new jxl.write.Number(5, (i + 9), fQuantity, numFormat));
                    wrksht.addCell(new jxl.write.Number(6, (i + 9), fRate, numFormat));
                    wrksht.addCell(new jxl.write.Number(7, (i + 9), fRateWithTax, numFormat));
                    if (isIGST) {
                        wrksht.addCell(new jxl.write.Number(8, (i + 9), IGSTper, numFormat));
                        wrksht.addCell(new jxl.write.Number(9, (i + 9), IGST, numFormat));
                        wrksht.addCell(new jxl.write.Number(10, (i + 9), amt, numFormat));
                        wrksht.addCell(new jxl.write.Number(11, (i + 9), amtWithTax, numFormat));
                    } else {
                        wrksht.addCell(new jxl.write.Number(8, (i + 9), CGSTper, numFormat));
                        wrksht.addCell(new jxl.write.Number(9, (i + 9), CGST, numFormat));
                        wrksht.addCell(new jxl.write.Number(10, (i + 9), SGSTper, numFormat));
                        wrksht.addCell(new jxl.write.Number(11, (i + 9), SGST, numFormat));
                        wrksht.addCell(new jxl.write.Number(12, (i + 9), amt, numFormat));
                        wrksht.addCell(new jxl.write.Number(13, (i + 9), amtWithTax, numFormat));
                    }

                }
            }
        } catch (NumberFormatException | ParseException | WriteException ex) {
            Logger.getLogger(GSTpercentageReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setWorkSheetHeader(WritableSheet wrksht, String strPercentage, boolean isIGST) throws jxl.write.WriteException {

        SimpleDateFormat sft = new SimpleDateFormat("dd/MM/yyyy");
        WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);

        NumberFormat decimalNo = new NumberFormat("#.00");
        WritableCellFormat rightFormat = new WritableCellFormat(decimalNo);
        rightFormat.setFont(wf3);
        rightFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat headerFormat = new WritableCellFormat(wf2);
        headerFormat.setFont(wf2);

        WritableCellFormat centerFormat = new WritableCellFormat(wf3);
        centerFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat centerFormat1 = new WritableCellFormat(wf2);
        centerFormat1.setAlignment(Alignment.CENTRE);
        centerFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat cellFormat = new WritableCellFormat(wf2);
        cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        String strGstIn = company.getGSTIN();
        String strName = company.getCompanyName();
        String strAddress = company.getAddress().replaceAll("/~/", "");
        String strCity = company.getCity();
        String strState = company.getState();
        String strMobile = company.getMobile();
        String strPhone = company.getPhone();
        String strEmail = company.getEmail();
        wrksht.setColumnView(0, 11);
        wrksht.setColumnView(1, 16);
        wrksht.setColumnView(2, 30);
        wrksht.setColumnView(3, 22);
        wrksht.setColumnView(4, 40);
        wrksht.setColumnView(5, 17);
        wrksht.setColumnView(6, 17);
        wrksht.setColumnView(7, 17);
        wrksht.setColumnView(8, 10);
        wrksht.setColumnView(9, 17);
        wrksht.setColumnView(10, 10);
        wrksht.setColumnView(11, 17);
        wrksht.setColumnView(12, 17);
        wrksht.setColumnView(13, 20);

        if (isIGST) {
            wrksht.setName("Sales IGST " + strPercentage + "%");
        } else {
            wrksht.setName("Sales CGST & SGST " + strPercentage + "%");
        }
        wrksht.addCell(new Label(0, 0, "GSTIN/UIN: " + strGstIn, cellFormat));
        wrksht.mergeCells(0, 0, 7, 0);

        if (isIGST) {
            wrksht.addCell(new Label(3, 0, strPercentage + "% IGST Reports" + sft.format(dateFrom.getDate()) + " to " + sft.format(dateTo.getDate()) + "", rightFormat));
        } else {
            wrksht.addCell(new Label(3, 0, strPercentage + "% CGST,SGST Reports" + sft.format(dateFrom.getDate()) + " to " + sft.format(dateTo.getDate()) + "", rightFormat));
        }
        wrksht.mergeCells(8, 0, 13, 0);

        if (!strName.equals("")) {
            wrksht.addCell(new Label(0, 1, strName, centerFormat1));
        }
        wrksht.mergeCells(0, 1, 13, 1);

        if (!strAddress.equals("")) {
            wrksht.addCell(new Label(0, 2, strAddress, centerFormat1));
        }
        wrksht.mergeCells(0, 2, 13, 2);

        if (!strCity.equals("")) {
            wrksht.addCell(new Label(0, 3, "City : " + strCity, centerFormat1));
        }
        wrksht.mergeCells(0, 3, 13, 3);

        if (!strState.equals("")) {
            wrksht.addCell(new Label(0, 4, "State : " + strState + "  Code : " + StateCode.getStateCode(strState), centerFormat1));
        }
        wrksht.mergeCells(0, 4, 13, 4);

        if (!strPhone.equals("")) {
            wrksht.addCell(new Label(0, 5, "Phone : " + strPhone, centerFormat1));
        }
        wrksht.mergeCells(0, 5, 13, 5);

        if (!strMobile.equals("")) {
            wrksht.addCell(new Label(0, 6, "Mobile : " + strMobile, centerFormat1));
        }
        wrksht.mergeCells(0, 6, 13, 6);

        if (!strEmail.equals("")) {
            wrksht.addCell(new Label(0, 7, "Email : " + strEmail, centerFormat1));
        }
        wrksht.mergeCells(0, 7, 13, 7);

        wrksht.addCell(new Label(0, 8, "Date", headerFormat));
        wrksht.addCell(new Label(1, 8, "Bill", headerFormat));
        wrksht.addCell(new Label(2, 8, "Company", headerFormat));
        wrksht.addCell(new Label(3, 8, "GSTIN", headerFormat));
        wrksht.addCell(new Label(4, 8, "Product", headerFormat));
        wrksht.addCell(new Label(5, 8, "Quantity", headerFormat));
        wrksht.addCell(new Label(6, 8, "Rate", headerFormat));
        wrksht.addCell(new Label(7, 8, "Rate (Incl.Tax)", headerFormat));
        if (!isIGST) {
            wrksht.addCell(new Label(8, 8, "CGST %", headerFormat));
            wrksht.addCell(new Label(9, 8, "CGST", headerFormat));
            wrksht.addCell(new Label(10, 8, "SGST %", headerFormat));
            wrksht.addCell(new Label(11, 8, "SGST", headerFormat));
            wrksht.addCell(new Label(12, 8, "Amount", headerFormat));
            wrksht.addCell(new Label(13, 8, "Amount (Incl.Tax)", headerFormat));
        } else {
            wrksht.addCell(new Label(8, 8, "IGST %", headerFormat));
            wrksht.addCell(new Label(9, 8, "IGST", headerFormat));
            wrksht.addCell(new Label(10, 8, "Amount", headerFormat));
            wrksht.addCell(new Label(11, 8, "Amount (Incl.Tax)", headerFormat));
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GSTpercentageReport.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GSTpercentageReport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout1;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane20;
    private javax.swing.JScrollPane jScrollPane21;
    private javax.swing.JScrollPane jScrollPane22;
    private javax.swing.JScrollPane jScrollPane23;
    private javax.swing.JScrollPane jScrollPane24;
    private javax.swing.JScrollPane jScrollPane25;
    private javax.swing.JScrollPane jScrollPane26;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelCompanyName1;
    private javax.swing.JLabel labelUserName1;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JPanel panelCGST0;
    private javax.swing.JPanel panelCGST12;
    private javax.swing.JPanel panelCGST18;
    private javax.swing.JPanel panelCGST28;
    private javax.swing.JPanel panelCGST5;
    private javax.swing.JPanel panelIGST0;
    private javax.swing.JPanel panelIGST12;
    private javax.swing.JPanel panelIGST18;
    private javax.swing.JPanel panelIGST28;
    private javax.swing.JPanel panelIGST5;
    private javax.swing.JTable tableCdnr;
    private javax.swing.JTable tblCGST0;
    private javax.swing.JTable tblCGST12;
    private javax.swing.JTable tblCGST18;
    private javax.swing.JTable tblCGST28;
    private javax.swing.JTable tblCGST5;
    private javax.swing.JTable tblIGST0;
    private javax.swing.JTable tblIGST12;
    private javax.swing.JTable tblIGST18;
    private javax.swing.JTable tblIGST28;
    private javax.swing.JTable tblIGST5;
    private javax.swing.JTextField totAmount0;
    private javax.swing.JTextField totAmount12;
    private javax.swing.JTextField totAmount18;
    private javax.swing.JTextField totAmount28;
    private javax.swing.JTextField totAmount5;
    private javax.swing.JTextField totAmountIgst0;
    private javax.swing.JTextField totAmountIgst12;
    private javax.swing.JTextField totAmountIgst18;
    private javax.swing.JTextField totAmountIgst28;
    private javax.swing.JTextField totAmountIgst5;
    private javax.swing.JTextField totAmountInclTax0;
    private javax.swing.JTextField totAmountInclTax12;
    private javax.swing.JTextField totAmountInclTax18;
    private javax.swing.JTextField totAmountInclTax28;
    private javax.swing.JTextField totAmountInclTax5;
    private javax.swing.JTextField totAmountInclTaxIgst0;
    private javax.swing.JTextField totAmountInclTaxIgst12;
    private javax.swing.JTextField totAmountInclTaxIgst18;
    private javax.swing.JTextField totAmountInclTaxIgst28;
    private javax.swing.JTextField totAmountInclTaxIgst5;
    private javax.swing.JTextField totCgst0;
    private javax.swing.JTextField totCgst12;
    private javax.swing.JTextField totCgst18;
    private javax.swing.JTextField totCgst28;
    private javax.swing.JTextField totCgst5;
    private javax.swing.JTextField totIgst0;
    private javax.swing.JTextField totIgst12;
    private javax.swing.JTextField totIgst18;
    private javax.swing.JTextField totIgst28;
    private javax.swing.JTextField totIgst5;
    private javax.swing.JTextField totSgst0;
    private javax.swing.JTextField totSgst12;
    private javax.swing.JTextField totSgst18;
    private javax.swing.JTextField totSgst28;
    private javax.swing.JTextField totSgst5;
    // End of variables declaration//GEN-END:variables
}
