/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.dao.GoDownStockCRUD;
import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PricingPolicyLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BillChargeLogic;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.pdf.TaxInvoiceNew;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.FormValidation;
import india.abs.gstonepro.ui.utils.GetPDFHeaders;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 *
 * @author ABS
 */
public class SalesB2CUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String HSNCode;
    boolean isIGST = false, isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    int lastBillId;
    Long LedgerId;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0, BaseProductRate = 0;
    float amountWithoutDiscount = 0, amount = 0, discount = 0, discountPer = 0, oldDiscount = 0;
    float IGSTper = 0, CGSTper = 0, SGSTper = 0, fTotalCess;
    long productStock = 0;
    int nCount = 0;
    Boolean isOldBill = false;
    boolean bError = false;
    boolean bUQCSame = false;
    boolean isUnderStockPossible = false;
    boolean bCheckStock = false;
    boolean bErrorAlert = false;
    boolean isnewPartyAdded = false;
    boolean descAdded = false;
    GoDownStockDetail stock = null;
    boolean isBtnProduct = false;
    boolean isQty = true;

    String selValue = "";

    String desc1 = "";
    String desc2 = "";
    String desc3 = "";
    String desc4 = "";
    String desc5 = "";
    String desc6 = "";
    String desc7 = "";
    String desc8 = "";
    String desc9 = "";
    String desc10 = "";
    int descCount = 0;

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Long companyId = company.getCompanyId();
    String CompanyState = SessionDataUtil.getSelectedCompany().getState();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(companyId);
    Ledger ledgerData = new Ledger();
    Product productDetail = new Product();
    PSLineItemsDescriptionUI productDescriptionUI = null;
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(companyId);
    List<BillCharge> charges = new BillChargeLogic().fetchAllBillCharges();

    List<PricingPolicy> pricingPolicy = new PricingPolicyLogic().fetchAllPP(companyId);
    Set<SingleProductPolicy> recentSingleProductPolicies = new HashSet<>();
    PurchaseSale oldSale = new PurchaseSale();
    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();

    ///////////////total values are set global////////////////
    float totalAmount = 0,
            totalIGST = 0,
            totalCGST = 0,
            totalSGST = 0,
            totalProfit = 0,
            totalCess = 0, NetAmount = 0, roundOff = 0;
    Boolean needPrint = false, isServiceProduct = false;
///////////////total values are set global////////////////

    /**
     * Creates new form Bill
     */
    public SalesB2CUI() {
        initComponents();
        //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        setMenuRoles();
        setExtendedState(SalesB2CUI.MAXIMIZED_BOTH);
        comboState.setSelectedItem("Tamil Nadu");
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        saveData();
        printData();
        setGodown();
//        loadComboProduct();

        Collections.sort(sortedLedgers, new Comparator<Ledger>() {
            public int compare(Ledger o1, Ledger o2) {
                return o1.getLedgerName().compareTo(o2.getLedgerName());
            }
        });
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        btnProductDescription.setEnabled(false);
        setParty();
        dateBill.setDate(new Date());
        setProduct();
        setPricingPolicy();
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);

        JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
        editor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {

                if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
                    setNewProduct();
                }
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboProduct.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();
                        for (Product product : products) {
                            if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(product.getName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem(val);
                        comboProduct.setPopupVisible(true);
                    }
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_N) {
                    new NewProductUI().setVisible(true);
                }
                else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Product product : products) {
                        scripts.add(product.getName());
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboProduct.setModel(model);
                    comboProduct.getEditor().setItem("");
                    comboProduct.setPopupVisible(true);
                }
                else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                    DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                    String selectedObj = (String) model.getSelectedItem();
                    comboProduct.setSelectedItem("");
                    comboProduct.getEditor().setItem(selectedObj);
                    comboProduct.setPopupVisible(false);
                    getProductDetails(selectedObj);

                }
            }
        });

        JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
        editor1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboParty.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();

                        if (("No Party".toLowerCase().contains(val.toLowerCase()))) {
                            scripts.add("No Party");
                        }
                        if (("New Party".toLowerCase().contains(val.toLowerCase()))) {
                            scripts.add("New Party");
                        }

                        for (Ledger ledger : sortedLedgers) {
                            if ((ledger.getLedgerName().toLowerCase().contains(val.toLowerCase()))) {
                                scripts.add(ledger.getLedgerName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);
                        comboParty.getEditor().setItem(val);
                        comboParty.setPopupVisible(true);
                    }
                }

            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                    ArrayList<String> scripts = new ArrayList<>();
                    scripts.add("No Party");
                    scripts.add("New Party");
                    for (Ledger ledger : sortedLedgers) {
                        scripts.add(ledger.getLedgerName());
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboParty.setModel(model);
                    comboParty.getEditor().setItem("");
                    comboParty.setPopupVisible(true);
                }
                else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                    DefaultComboBoxModel model = (DefaultComboBoxModel) comboParty.getModel();
                    String selectedObj = (String) model.getSelectedItem();
                    comboParty.setPopupVisible(false);
                    comboParty.getEditor().setItem(selectedObj);
                    getPartyDetails();
                    dateBill.requestFocusInWindow();
                }
            }
        });
        ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                char vChar = evt.getKeyChar();
                boolean dot = false;

                if (dot == false) {
                    if (vChar == '.') {
                        dot = true;
                    }
                    else if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();
                    }
                }
                else {
                    if (!(Character.isDigit(vChar)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                            || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                        evt.consume();

                    }
                }
                if (Character.isDigit(vChar)) {
                    String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                    int len = strDate.length();
                    if (len == 1) {
                        ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                        evt.consume();
                    }
                    else if (len == 4) {
                        ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                        evt.consume();
                    }
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                        sft.setLenient(false);
                        String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                        if (!(strDate.trim().equals(""))) {
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                String strName = comboParty.getSelectedItem().toString();
                                if (strName.equals("")) {
                                    comboParty.requestFocus();
                                }
                                else if (strName.equalsIgnoreCase("no party") || strName.equalsIgnoreCase("new Party")) {
                                    txtPartyName.requestFocus();
                                }
                                else {
                                    comboProduct.requestFocus();
                                }
                            }
                            else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "Please Select the Date");
                        }
                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                        Logger.getLogger(SalesB2CUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        JTextComponent editor2 = (JTextComponent) comboGodown.getEditor().getEditorComponent();
        editor2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {

                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboGodown.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();
                    for (GoDown godown : godowns) {
                        if (godown.getGoDownName().toLowerCase().contains(val.toLowerCase())) {
                            scripts.add(godown.getGoDownName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboGodown.setModel(model);
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboGodown.setPopupVisible(false);
                        comboGodown.getEditor().setItem(val);
                        val = val.replaceAll("\\s+", "").replaceAll("\\s+$", "");
                        if (!val.equals("")) {
                            setStock(comboProduct.getEditor().getItem().toString());
                            txtCessAmt.requestFocus();
                        }
                    }
                    else {
                        comboGodown.getEditor().setItem(val);
                        comboGodown.setPopupVisible(true);
                    }
                }
            }
        });
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    private void setPricingPolicy() {

        for (PricingPolicy policy : pricingPolicy) {
            comboPricingPolicy.addItem(policy.getPolicyName());
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        }
        else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        }
        else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        }
        else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        }
        else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        }
        else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        }
        else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        }
        else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        paymentDialog = new javax.swing.JDialog();
        btnOk = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        txtPayment = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        labelPaymentBillAmount = new javax.swing.JLabel();
        labelBalance = new javax.swing.JLabel();
        RemarksDialog = new javax.swing.JDialog();
        btnRemarks = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtRemarks = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtPartyName = new javax.swing.JTextField();
        txtAddrLine = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        comboState = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        txtGSTIN = new javax.swing.JTextField();
        panelUQC = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        labelStock = new javax.swing.JLabel();
        comboProduct = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblPrice = new javax.swing.JLabel();
        txtUQC1Rate = new javax.swing.JTextField();
        labelUQC1forPrice = new javax.swing.JLabel();
        labelUQC2forPrice = new javax.swing.JLabel();
        txtUQC2Rate = new javax.swing.JTextField();
        lblPriceWithTax = new javax.swing.JLabel();
        txtUQC1RateWithTax = new javax.swing.JTextField();
        labelUQC1PriceWithTax = new javax.swing.JLabel();
        labelUQC2PriceWithTax = new javax.swing.JLabel();
        txtUQC2RateWithTax = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblProductsTotal = new javax.swing.JLabel();
        txtCessAmt = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        comboPricingPolicy = new javax.swing.JComboBox<>();
        btnClear = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        txtBillDiscount = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnProductDescription = new javax.swing.JButton();
        txtTotalBillAmount = new javax.swing.JLabel();
        btnRemark = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblGodown = new javax.swing.JLabel();
        comboGodown = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtIGSTper = new javax.swing.JLabel();
        txtIGSTamt = new javax.swing.JLabel();
        txtCGSTper = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtCGSTamt = new javax.swing.JLabel();
        txtSGSTper = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtSGSTamt = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        btnOk.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel23.setText("PAYMENT");

        jLabel24.setText("Balance");

        txtPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPaymentActionPerformed(evt);
            }
        });
        txtPayment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaymentKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaymentKeyTyped(evt);
            }
        });

        jLabel32.setText("Amount Tendered");

        jLabel38.setText("Bill Amount");

        labelPaymentBillAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelPaymentBillAmount.setText("0.00");

        labelBalance.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelBalance.setText("0.00");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38)
                    .addComponent(jLabel32)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPaymentBillAmount)
                    .addComponent(labelBalance))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(labelPaymentBillAmount))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(labelBalance))
                .addContainerGap())
        );

        javax.swing.GroupLayout paymentDialogLayout = new javax.swing.GroupLayout(paymentDialog.getContentPane());
        paymentDialog.getContentPane().setLayout(paymentDialogLayout);
        paymentDialogLayout.setHorizontalGroup(
            paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paymentDialogLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paymentDialogLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        paymentDialogLayout.setVerticalGroup(
            paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paymentDialogLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOk)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        btnRemarks.setMnemonic('k');
        btnRemarks.setText("OK");
        btnRemarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemarksActionPerformed(evt);
            }
        });

        txtRemarks.setColumns(20);
        txtRemarks.setRows(5);
        jScrollPane2.setViewportView(txtRemarks);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Remarks");

        javax.swing.GroupLayout RemarksDialogLayout = new javax.swing.GroupLayout(RemarksDialog.getContentPane());
        RemarksDialog.getContentPane().setLayout(RemarksDialogLayout);
        RemarksDialogLayout.setHorizontalGroup(
            RemarksDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RemarksDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(RemarksDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(RemarksDialogLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(RemarksDialogLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, RemarksDialogLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRemarks, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        RemarksDialogLayout.setVerticalGroup(
            RemarksDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, RemarksDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(btnRemarks))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tax Invoice B2C");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product / Service", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Discount %", "Discount", "Amount", "IGST %", "IGST", "CGST %", "CGST", "SGST %", "SGST", "UQC1BaseRate", "UQC2BaseRate", "Profit", "Cess Amount", "Total Amount", "isService", "godown", "desc1", "desc2", "desc3", "desc4", "desc5", "desc6", "desc7", "desc8", "desc9", "desc10", "desccount", "Total Amount(Incl.Tax)", "Uqc1RateWithTax", "Uqc2RateWithTax"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(50);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(1).setMinWidth(350);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(350);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(350);
            jTable1.getColumnModel().getColumn(2).setMinWidth(0);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(0);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(15).setMinWidth(0);
            jTable1.getColumnModel().getColumn(15).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(15).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(22).setMinWidth(0);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(23).setMinWidth(0);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(27).setMinWidth(0);
            jTable1.getColumnModel().getColumn(27).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(27).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(28).setMinWidth(0);
            jTable1.getColumnModel().getColumn(28).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(28).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(29).setMinWidth(0);
            jTable1.getColumnModel().getColumn(29).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(29).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(30).setMinWidth(0);
            jTable1.getColumnModel().getColumn(30).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(30).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(31).setMinWidth(0);
            jTable1.getColumnModel().getColumn(31).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(31).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(32).setMinWidth(0);
            jTable1.getColumnModel().getColumn(32).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(32).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(33).setMinWidth(0);
            jTable1.getColumnModel().getColumn(33).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(33).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(34).setMinWidth(0);
            jTable1.getColumnModel().getColumn(34).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(34).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(35).setMinWidth(0);
            jTable1.getColumnModel().getColumn(35).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(35).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(36).setMinWidth(0);
            jTable1.getColumnModel().getColumn(36).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(36).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(37).setMinWidth(0);
            jTable1.getColumnModel().getColumn(37).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(37).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(38).setMinWidth(0);
            jTable1.getColumnModel().getColumn(38).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(38).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(39).setMinWidth(0);
            jTable1.getColumnModel().getColumn(39).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(39).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(41).setMinWidth(0);
            jTable1.getColumnModel().getColumn(41).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(41).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(42).setMinWidth(0);
            jTable1.getColumnModel().getColumn(42).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(42).setMaxWidth(0);
        }

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel2.setText("Date ");

        dateBill.setDateFormatString("dd-MM-yyyy");
        dateBill.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateBillKeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        jLabel16.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtPartyName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPartyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartyNameActionPerformed(evt);
            }
        });
        txtPartyName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartyNameKeyPressed(evt);
            }
        });

        txtAddrLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddrLineActionPerformed(evt);
            }
        });
        txtAddrLine.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddrLineKeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel20.setText("City");

        jLabel25.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel25.setText("District");

        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });
        txtDistrict.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDistrictKeyPressed(evt);
            }
        });

        txtCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });
        txtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCityKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        jLabel26.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel26.setText("GSTIN/UIN");

        comboState.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboState.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboStateItemStateChanged(evt);
            }
        });
        comboState.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboStateFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                comboStateFocusLost(evt);
            }
        });
        comboState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboStateActionPerformed(evt);
            }
        });
        comboState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboStateKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel6.setText("Party");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboPartyKeyReleased(evt);
            }
        });

        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });
        txtGSTIN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtGSTINKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboParty, 0, 184, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtAddrLine, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboState, 0, 190, Short.MAX_VALUE)
                    .addComponent(txtGSTIN))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel17, jLabel26});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(comboState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(2, 2, 2)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtAddrLine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel15, jLabel16, jLabel17, jLabel2, jLabel20, jLabel25, jLabel26, jLabel6});

        jLabel35.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel35.setText("Quantity ");

        txtUQC1Qty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUQC1QtyFocusGained(evt);
            }
        });
        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setText("UQC1");
        labelUQC1.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setText("UQC2");
        labelUQC2.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel1.setText("Available Stock ");

        labelStock.setFont(new java.awt.Font("Segoe UI", 1, 13)); // NOI18N
        labelStock.setText("N/A");

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboProductFocusGained(evt);
            }
        });
        comboProduct.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboProductPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboProduct.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                comboProductComponentHidden(evt);
            }
        });
        comboProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductActionPerformed(evt);
            }
        });
        comboProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboProductKeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel18.setText("Product / Service");

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lblPrice.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        lblPrice.setText("Price Excl of Tax");

        txtUQC1Rate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC1Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateActionPerformed(evt);
            }
        });
        txtUQC1Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyTyped(evt);
            }
        });

        labelUQC1forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1forPrice.setText("per UQC1");

        labelUQC2forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2forPrice.setText("per UQC2");

        txtUQC2Rate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC2Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateActionPerformed(evt);
            }
        });
        txtUQC2Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyTyped(evt);
            }
        });

        lblPriceWithTax.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        lblPriceWithTax.setText("Price Inc of Tax");

        txtUQC1RateWithTax.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC1RateWithTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateWithTaxActionPerformed(evt);
            }
        });
        txtUQC1RateWithTax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyTyped(evt);
            }
        });

        labelUQC1PriceWithTax.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1PriceWithTax.setText("per UQC1");

        labelUQC2PriceWithTax.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2PriceWithTax.setText("per UQC2");

        txtUQC2RateWithTax.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC2RateWithTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateWithTaxActionPerformed(evt);
            }
        });
        txtUQC2RateWithTax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyTyped(evt);
            }
        });

        jPanel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(180, 180, 180), 1, true));
        jPanel5.setPreferredSize(new java.awt.Dimension(1, 102));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 153, Short.MAX_VALUE)
        );

        jLabel13.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel13.setText("Total Amount Excl Tax");

        txtAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtAmount.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                txtAmountComponentAdded(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel4.setText("Total Amount Inc Tax");

        lblProductsTotal.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblProductsTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblProductsTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblPriceWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtUQC2RateWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelUQC2PriceWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtUQC1RateWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelUQC1PriceWithTax, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                                .addGap(8, 8, 8)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrice)
                    .addComponent(lblPriceWithTax))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUQC1RateWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC1PriceWithTax))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUQC2RateWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC2PriceWithTax))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblProductsTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelUQC1forPrice)
                            .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelUQC2forPrice)
                            .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(3, 3, 3))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel4, lblProductsTotal});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel13, lblPrice});

        txtCessAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCessAmtActionPerformed(evt);
            }
        });
        txtCessAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCessAmtKeyPressed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel19.setText("Cess Amount");
        jLabel19.setToolTipText("");

        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel27.setText("Disount");
        jLabel27.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel5.setText("Discount %");

        txtDiscountPer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDiscountPerFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDiscountPerFocusLost(evt);
            }
        });
        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel29.setText("Pricing Policy");

        comboPricingPolicy.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        comboPricingPolicy.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Default" }));
        comboPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPricingPolicyActionPerformed(evt);
            }
        });
        comboPricingPolicy.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPricingPolicyKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelUQCLayout = new javax.swing.GroupLayout(panelUQC);
        panelUQC.setLayout(panelUQCLayout);
        panelUQCLayout.setHorizontalGroup(
            panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUQCLayout.createSequentialGroup()
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(labelStock, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiscountPer, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiscount, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29))
                        .addGap(18, 18, 18)
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comboProduct, 0, 193, Short.MAX_VALUE)
                            .addComponent(comboPricingPolicy, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCessAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtUQC1Qty, txtUQC2Qty});

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtDiscount, txtDiscountPer});

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel18, jLabel29});

        panelUQCLayout.setVerticalGroup(
            panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUQCLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel35)
                            .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(comboPricingPolicy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel29)))
                        .addGap(18, 18, 18)
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelUQC2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUQC2Qty)
                            .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelStock)))
                        .addGap(18, 18, 18)
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtDiscount, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                            .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtCessAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        panelUQCLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCessAmt, txtDiscount, txtDiscountPer});

        comboProduct.getAccessibleContext().setAccessibleName("");

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear ");
        btnClear.setMaximumSize(new java.awt.Dimension(100, 100));
        btnClear.setMinimumSize(new java.awt.Dimension(70, 70));
        btnClear.setName(""); // NOI18N
        btnClear.setPreferredSize(new java.awt.Dimension(70, 15));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.setMaximumSize(new java.awt.Dimension(100, 100));
        btnDelete.setMinimumSize(new java.awt.Dimension(70, 70));
        btnDelete.setName(""); // NOI18N
        btnDelete.setPreferredSize(new java.awt.Dimension(70, 15));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.setMaximumSize(new java.awt.Dimension(100, 100));
        btnUpdate.setMinimumSize(new java.awt.Dimension(70, 70));
        btnUpdate.setName(""); // NOI18N
        btnUpdate.setPreferredSize(new java.awt.Dimension(70, 15));
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.setMaximumSize(new java.awt.Dimension(100, 100));
        btnAdd.setMinimumSize(new java.awt.Dimension(70, 70));
        btnAdd.setName(""); // NOI18N
        btnAdd.setPreferredSize(new java.awt.Dimension(70, 15));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        txtBillDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillDiscountActionPerformed(evt);
            }
        });
        txtBillDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyTyped(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel21.setText("Bill Discount");

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnPrint.setMnemonic('p');
        btnPrint.setText("Save & Print");
        btnPrint.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnProductDescription.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnProductDescription.setMnemonic('u');
        btnProductDescription.setText("Product Description");
        btnProductDescription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductDescriptionActionPerformed(evt);
            }
        });

        txtTotalBillAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtTotalBillAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        btnRemark.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnRemark.setText("Remarks");
        btnRemark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemarkActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(180, 180, 180)));
        jPanel3.setPreferredSize(new java.awt.Dimension(1, 1));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        lblGodown.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        lblGodown.setText("Godown");
        lblGodown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        comboGodown.setEditable(true);
        comboGodown.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboGodown.setName(""); // NOI18N
        comboGodown.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboGodownFocusGained(evt);
            }
        });
        comboGodown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGodownActionPerformed(evt);
            }
        });
        comboGodown.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGodownKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboGodownKeyReleased(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("IGST ");

        txtIGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtIGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtIGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtIGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtCGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtCGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("CGST ");

        txtCGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtCGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtSGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtSGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("SGST");

        txtSGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtSGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel14.setText(" %");
        jLabel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel22.setText("Amount");
        jLabel22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(txtIGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel14, jLabel22, txtCGSTamt, txtCGSTper, txtIGSTamt, txtIGSTper, txtSGSTamt, txtSGSTper});

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel7});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(jLabel22))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)))
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtCGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)))
                .addGap(0, 0, 0))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCGSTamt, txtCGSTper, txtIGSTamt, txtSGSTamt, txtSGSTper});

        jLabel8.setText("Total Amount (Incl of Tax)");

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 1272, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTotalBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnProductDescription)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRemark, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(panelUQC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnPrint, btnRemark, btnSave});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBillDiscount))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtTotalBillAmount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelUQC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnRemark, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btnPrint, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnProductDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnPrint, btnRemark, btnSave});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txtPartyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartyNameActionPerformed
        txtAddrLine.requestFocus();
    }//GEN-LAST:event_txtPartyNameActionPerformed

    private void txtAddrLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddrLineActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddrLineActionPerformed

    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed
    public void setTable() {
        boolean isProfitEnabled = SessionDataUtil.getCompanyPolicyData().isProfitEnabled();
        if (isIGST) {
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(100);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(100);
        }
        else {
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(100);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(100);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);
        }
        if (isProfitEnabled) {
            jTable1.getColumnModel().getColumn(24).setMinWidth(60);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(60);
        }
        else {
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
        }
    }
    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        comboState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void comboProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboProductFocusGained
        //    comboProduct.showPopup();
    }//GEN-LAST:event_comboProductFocusGained

    private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_labelProductKeyReleased

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String party = comboParty.getSelectedItem().toString();
        if (party.equalsIgnoreCase("no party") || validateFields()) {
            addProductList();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String party = comboParty.getSelectedItem().toString();
            if (party.equalsIgnoreCase("no party") || validateFields()) {
                addProductList();
            }
            evt.consume();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    if (!(Boolean) model.getValueAt(y, 27)) {
                        model.setValueAt(y + 1, y, 0); //setValueAt(data,row,column)
                    }
                }
                clear(false);
                TotalCalculate();
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed
    public void clear(boolean bBtnClear) {
        txtUQC1Qty.setText("");
        txtUQC1Qty.setEnabled(true);
        txtUQC2Qty.setText("");
        txtUQC1RateWithTax.setText("");
        txtUQC2RateWithTax.setText("");
        txtIGSTper.setText("");
        txtCGSTper.setText("");
        txtSGSTper.setText("");
        txtIGSTamt.setText("");
        txtCGSTamt.setText("");
        txtSGSTamt.setText("");
        txtAmount.setText("");
        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("UQC1");
        labelUQC2.setText("UQC2");
        labelUQC1forPrice.setText("/UQC1");
        labelUQC2forPrice.setText("/UQC2");
        comboProduct.setSelectedItem("");
        comboProduct.requestFocus();
        labelStock.setText("N/A");
        lblProductsTotal.setText("");
        txtCessAmt.setText("");
        txtUQC1Rate.setText("");
        txtUQC2Rate.setText("");
        labelUQC1PriceWithTax.setText("");
        labelUQC2PriceWithTax.setText("");
        labelUQC1.setVisible(true);
        txtUQC2Qty.setVisible(true);
        btnProductDescription.setEnabled(false);
        comboProduct.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        desc1 = "";
        desc2 = "";
        desc3 = "";
        desc4 = "";
        desc5 = "";
        desc6 = "";
        desc7 = "";
        desc8 = "";
        desc9 = "";
        desc10 = "";
        productDescriptionUI.prodDesc1 = "";
        productDescriptionUI.prodDesc2 = "";
        productDescriptionUI.prodDesc3 = "";
        productDescriptionUI.prodDesc4 = "";
        productDescriptionUI.prodDesc5 = "";
        productDescriptionUI.prodDesc6 = "";
        productDescriptionUI.prodDesc7 = "";
        productDescriptionUI.prodDesc8 = "";
        productDescriptionUI.prodDesc9 = "";
        productDescriptionUI.prodDesc10 = "";

        descCount = 0;
        productDescriptionUI.descCount = 0;
        if (bBtnClear) {
            comboPricingPolicy.setSelectedItem("Default");
            lblProductsTotal.setText("");
            jTable1.clearSelection();
//            dateBill.setDate(null);
//            txtPartyName.setText("");
//            txtAddrLine.setText("");
//            txtCity.setText("");
//            txtDistrict.setText("");
//            comboState.setSelectedItem("");
//            txtGSTIN.setText("");
        }
        btnAdd.setEnabled(true);
    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        SaveAndPrint(false);
    }//GEN-LAST:event_btnSaveActionPerformed
    public void SaveAndPrint(Boolean isPrint) {
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateBill.getDate());
        }
        if (isNotExpired) {
            needPrint = isPrint;
            Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);
            if (!isInBetweenFinancialYear) {
                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                        + " to <b>" + strFinancialYearEnd + "</b></html>";
                JLabel label = new JLabel(msg);
                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            }
            else {
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                int rowCount = model.getRowCount();
                totalAmount = 0;
                totalIGST = 0;
                totalCGST = 0;
                totalSGST = 0;
                totalProfit = 0;
                totalCess = 0;
                NetAmount = 0;
                roundOff = 0;

                for (int i = 0; i < model.getRowCount(); i++) {
                    if (model.getValueAt(i, 25) != null) {
                        totalCess = Float.parseFloat(model.getValueAt(i, 25).toString()) + totalCess;
                    }
                    totalAmount = (float) model.getValueAt(i, 15) + totalAmount;
                    totalIGST = (float) model.getValueAt(i, 17) + totalIGST;
                    totalCGST = (float) model.getValueAt(i, 19) + totalCGST;
                    totalSGST = (float) model.getValueAt(i, 21) + totalSGST;
                    totalProfit = (float) model.getValueAt(i, 24) + totalProfit;
                    //totalAmountInclTax = (float) model.getValueAt(i, 40) + totalAmountInclTax;
                }
                NetAmount = totalAmount + totalIGST + totalCGST + totalSGST + totalCess;
                NetAmount = NetAmount - Float.parseFloat("0" + txtBillDiscount.getText());
                String value = "0.00";
                try {
                    value = JOptionPane.showInputDialog(null, "Round Off " + txtTotalBillAmount.getText());
                    if (value.equals("")) {
                        value = "0.00";
                    }
                    roundOff = Float.parseFloat(value);

                    NetAmount = NetAmount + roundOff;
                    labelPaymentBillAmount.setText(currency.format(NetAmount));
                    if (company.getCompanyPolicy().isAmountTendered()) {
                        paymentDialog.setVisible(true);
                        paymentDialog.setSize(500, 300);
                        paymentDialog.setLocationRelativeTo(null);
                        txtPayment.requestFocusInWindow();
                    }
                    else {
                        billFinished();
                    }

                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please Enter Correct Value", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }

    public void OldPrintAndSaveAction() {
        GoDown SelGodown = null;
        setPartyEditable(true);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();

        ledgerData = findLedgerData();
//            PurchaseSale newPurchaseSale = new PurchaseSale();
//            oldSale.setCompany(SessionDataUtil.getSelectedCompany());
//            oldSale.setPsId(oldSale.getPsId());
//            oldSale.setPsType("SALES");
//            oldSale.setBillNo(oldSale.getBillNo());
//            oldSale.setPsDate(dateBill.getDate());
        oldSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
        oldSale.setIsIGST(isIGST);
        oldSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
        oldSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
        oldSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
        oldSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
        oldSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
        oldSale.setLedger(ledgerData);
        oldSale.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
        oldSale.setPaymentType("Cash");//Payment Type
        oldSale.setVariationAmount(convertDecimal.Currency("0"));
//            oldSale.setSmsMessage("");
//            String addr = oldSale.getLedger().getAddress();
//            String[] words = addr.split("/~/");
//            oldSale.setShippingAddressCity(oldSale.getLedger().getCity());
//            oldSale.setShippingAddressDistrict(oldSale.getLedger().getDistrict());
//            oldSale.setShippingAddressMobile(oldSale.getLedger().getMobile());
//            oldSale.setShippingAddressName(oldSale.getLedger().getLedgerName());
//            oldSale.setShippingAddressOne(words[0]);
//            oldSale.setShippingAddressPhone(oldSale.getLedger().getPhone());
//            oldSale.setShippingAddressState(oldSale.getLedger().getState());
//            if (words.length == 2) {
//                oldSale.setShippingAddressTwo(words[1]);
//                oldSale.setShippingAddressThree("");
//            } else if (words.length > 2) {
//                oldSale.setShippingAddressTwo(words[1]);
//                oldSale.setShippingAddressThree(words[2]);
//            } else {
//                oldSale.setShippingAddressTwo("");
//                oldSale.setShippingAddressThree("");
//            }
//            String strAmountPaid = txtAmountPaid.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strAmountPaid = "";
        if (strAmountPaid.equals("")) {
            strAmountPaid = "0";
        }
        oldSale.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));
        oldSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
        oldSale.setTotalAmountPaid(new BigDecimal(0));
        oldSale.setRemarks(txtRemarks.getText());

        List<PurchaseSaleLineItem> psLineItems = new ArrayList<PurchaseSaleLineItem>();
        List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<PurchaseSaleTaxSummary>();
        Long count = 1L;

        for (int i = 0; i < rowCount; i++) {
            String strGodown = model.getValueAt(i, 28).toString();
            if (!strGodown.equals("") && isInventoryEnabled) {
                for (GoDown godown : godowns) {
                    if (godown.getGoDownName().equalsIgnoreCase(strGodown)) {
                        SelGodown = godown;
                    }
                }
            }
            PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
            psLineItem.setLineNumber((int) model.getValueAt(i, 0));
            psLineItem.setProductName((String) model.getValueAt(i, 1));
            psLineItem.setHsnSac((String) model.getValueAt(i, 2));
            Product lineItemProduct = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

            psLineItem.setProduct(lineItemProduct);

            psLineItem.setUqcOne((String) model.getValueAt(i, 3));
            psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
            psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
            psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));

            psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
            psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
            psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
            psLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

            psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
            psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
            psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
            psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
            psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
            psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
            psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
            psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
            psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
            psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
            psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
            psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
            psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));
            psLineItem.setGodown(SelGodown);
            psLineItem.setDescriptionOne(model.getValueAt(i, 29).toString());
            psLineItem.setDescriptionTwo(model.getValueAt(i, 30).toString());
            psLineItem.setDescriptionThree(model.getValueAt(i, 31).toString());
            psLineItem.setDescriptionFour(model.getValueAt(i, 32).toString());
            psLineItem.setDescriptionFive(model.getValueAt(i, 33).toString());
            psLineItem.setDescriptionSix(model.getValueAt(i, 34).toString());
            psLineItem.setDescriptionSeven(model.getValueAt(i, 35).toString());
            psLineItem.setDescriptionEight(model.getValueAt(i, 36).toString());
            psLineItem.setDescriptionNine(model.getValueAt(i, 37).toString());
            psLineItem.setDescriptionTen(model.getValueAt(i, 38).toString());
            psLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 39).toString()));

            psLineItem.setUqcOneRateWithTax(convertDecimal.Currency(currency.format(model.getValueAt(i, 41))));
            psLineItem.setUqcTwoRateWithTax(convertDecimal.Currency(currency.format(model.getValueAt(i, 42))));

            psLineItems.add(psLineItem);

            Boolean isNewSalesTax = true;
            for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                    float amount = tax.getTaxableValue().floatValue();
                    amount = (float) model.getValueAt(i, 15) + amount;
                    float Igst = tax.getIgstValue().floatValue();
                    Igst = (float) model.getValueAt(i, 17) + Igst;
                    float Cgst = tax.getCgstValue().floatValue();
                    Cgst = (float) model.getValueAt(i, 19) + Cgst;
                    float Sgst = tax.getSgstValue().floatValue();
                    Sgst = (float) model.getValueAt(i, 21) + Sgst;
                    float taxAmount = Igst + Cgst + Sgst;

                    tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));

                    isNewSalesTax = false;
                }
            }
            if (isNewSalesTax) {
                PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                float amount = (float) model.getValueAt(i, 15);
                float Igst = (float) model.getValueAt(i, 17);
                float Cgst = (float) model.getValueAt(i, 19);
                float Sgst = (float) model.getValueAt(i, 21);
                float taxAmount = Igst + Cgst + Sgst;

                newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 16));
                newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 18));
                newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 20));
                newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                purchaseSalesTax.add(newSaleTaxSummary);

                count++;
            }
        }
        oldSale.setPsLineItems(psLineItems);
        oldSale.setPsTaxSummaries(purchaseSalesTax);
        if (needPrint) {
            try {
                new TaxInvoiceNew().printPdf(needPrint, false, oldSale, psLineItems, purchaseSalesTax, new GetPDFHeaders().getPdfHeaders());
            } catch (IOException ex) {
                Logger.getLogger(SalesB2CUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Boolean isSuccess = new PurchaseSaleLogic().updateAPurchaseOrSale("SALES", LedgerId, oldSale, psLineItems, purchaseSalesTax);

        if (isSuccess) {
            JOptionPane.showMessageDialog(null, "Bill is Successfully updated", "Message", JOptionPane.INFORMATION_MESSAGE);
            new SalesDateWiseReportUI().setVisible(true);
            dispose();
            clear(false);
        }
        else {
            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }

    public void NewPrintAndSaveAction() {
        GoDown SelGodown = null;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();

        String partyName = (String) comboParty.getSelectedItem();
        partyName = partyName.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isApproved = false;
        Boolean isNoParty = false;
        if (partyName.equalsIgnoreCase("No Party")) {
            isApproved = true;
            ledgerData = null;
            LedgerId = null;
            isNoParty = true;
        }
        else if (partyName.equals("New Party")) {
            Boolean isSuccess = addNewPartyDetails();
            if (isSuccess) {
                ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
                sortedLedgers = new ArrayList<>(ledgers);
                Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                    @Override
                    public int compare(Ledger o1, Ledger o2) {
                        return o1.getLedgerName().compareTo(o2.getLedgerName());
                    }
                });
                comboParty.removeAllItems();
                setParty();
                isnewPartyAdded = true;
                ledgerData = findLedgerData();
                isApproved = true;
            }
            else if (!bError) {
                JOptionPane.showMessageDialog(null, "New Party is not added", "Alert", JOptionPane.WARNING_MESSAGE);
                isApproved = false;
                bError = true;
            }
        }
        else {
            ledgerData = findLedgerData();
            isApproved = true;
        }
        if (isApproved) {
            PurchaseSale newPurchaseSale = new PurchaseSale();

            if (isNoParty) {
                newPurchaseSale.setNoParty(true);
                String strNoPartyName = txtPartyName.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyLineOne = txtAddrLine.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyCity = txtCity.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyDist = txtDistrict.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strTxtGSTIN = txtGSTIN.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                newPurchaseSale.setNoPartyName(strNoPartyName);
                newPurchaseSale.setNoPartyLineOne(strNoPartyLineOne);
                newPurchaseSale.setNoPartyCity(strNoPartyCity);
                newPurchaseSale.setNoPartyDistrict(strNoPartyDist);
                if (comboState.getSelectedIndex() < 0) {
                    comboState.setSelectedIndex(0);
                }
                newPurchaseSale.setNoPartyState((String) comboState.getSelectedItem());
                newPurchaseSale.setNoPartyGSTIN(strTxtGSTIN);
                String addr = strNoPartyLineOne;
                String[] words = addr.split("/,/");
                newPurchaseSale.setShippingAddressCity(strNoPartyCity);
                newPurchaseSale.setShippingAddressDistrict(strNoPartyDist);
                newPurchaseSale.setShippingAddressMobile("");
                newPurchaseSale.setShippingAddressName(strNoPartyName);
                newPurchaseSale.setShippingAddressOne(words[0]);
                newPurchaseSale.setShippingAddressPhone("");
                newPurchaseSale.setShippingAddressState((String) comboState.getSelectedItem());
                if (words.length == 2) {
                    newPurchaseSale.setShippingAddressTwo(words[1]);
                    newPurchaseSale.setShippingAddressThree("");
                }
                else if (words.length > 2) {
                    newPurchaseSale.setShippingAddressTwo(words[1]);
                    newPurchaseSale.setShippingAddressThree(words[2]);
                }
                else {
                    newPurchaseSale.setShippingAddressTwo("");
                    newPurchaseSale.setShippingAddressThree("");
                }
            }
            else {
                newPurchaseSale.setNoParty(false);
                newPurchaseSale.setNoPartyName("");
                newPurchaseSale.setNoPartyLineOne("");
                newPurchaseSale.setNoPartyCity("");
                newPurchaseSale.setNoPartyDistrict("");
                newPurchaseSale.setNoPartyState("");
                newPurchaseSale.setNoPartyGSTIN("");
                String addr = ledgerData.getAddress();
                String[] words = addr.split("/~/");
                newPurchaseSale.setShippingAddressCity(ledgerData.getCity());
                newPurchaseSale.setShippingAddressDistrict(ledgerData.getDistrict());
                newPurchaseSale.setShippingAddressMobile(ledgerData.getMobile());
                newPurchaseSale.setShippingAddressName(ledgerData.getLedgerName());
                newPurchaseSale.setShippingAddressOne(words[0]);
                newPurchaseSale.setShippingAddressPhone(ledgerData.getPhone());
                newPurchaseSale.setShippingAddressState(ledgerData.getState());
                if (words.length == 2) {
                    newPurchaseSale.setShippingAddressTwo(words[1]);
                    newPurchaseSale.setShippingAddressThree("");
                }
                else if (words.length > 2) {
                    newPurchaseSale.setShippingAddressTwo(words[1]);
                    newPurchaseSale.setShippingAddressThree(words[2]);
                }
                else {
                    newPurchaseSale.setShippingAddressTwo("");
                    newPurchaseSale.setShippingAddressThree("");
                }
            }

            newPurchaseSale.setCompany(SessionDataUtil.getSelectedCompany());
            newPurchaseSale.setPsType("SALES");
//              newPurchaseSale.setPsId(value);
//              newPurchaseSale.setBillNo(value);
            newPurchaseSale.setPsDate(dateBill.getDate());
            newPurchaseSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
            newPurchaseSale.setIsIGST(isIGST);
            newPurchaseSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            newPurchaseSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            newPurchaseSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            newPurchaseSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
            newPurchaseSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
            newPurchaseSale.setCancelled(false);
            newPurchaseSale.setThrough("");
            newPurchaseSale.setLedger(ledgerData);
            newPurchaseSale.setPaymentType("Cash");//Payment Type
            newPurchaseSale.setModeOfDelivery("");
            newPurchaseSale.setTotalCessAmount(new BigDecimal(Float.toString(fTotalCess)));
//              String strAmountPaid = txtAmountPaid.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strAmountPaid = "";
            if (strAmountPaid.equals("")) {
                strAmountPaid = "0";
            }
            newPurchaseSale.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));
            newPurchaseSale.setVariationAmount(convertDecimal.Currency("0"));
            newPurchaseSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
            newPurchaseSale.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
            newPurchaseSale.setTotalAmountPaid(new BigDecimal(0));
            newPurchaseSale.setRemarks(txtRemarks.getText());

            List<PurchaseSaleLineItem> psLineItems = new ArrayList<PurchaseSaleLineItem>();
            List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<PurchaseSaleTaxSummary>();
            Long count = 1L;
            for (int i = 0; i < rowCount; i++) {
                String strGodown = model.getValueAt(i, 28).toString();
                if (!strGodown.equals("")) {
                    for (GoDown godown : godowns) {
                        if (godown.getGoDownName().equalsIgnoreCase(strGodown)) {
                            SelGodown = godown;
                        }
                    }
                }
                PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                Product lineItemProduct = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(lineItemProduct);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue(Float.parseFloat(model.getValueAt(i, 10).toString()));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
                psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));
                psLineItem.setCessAmount(convertDecimal.Currency(model.getValueAt(i, 25).toString()));
                psLineItem.setGodown(SelGodown);
                psLineItem.setDescriptionOne("" + model.getValueAt(i, 29).toString());
                psLineItem.setDescriptionTwo("" + model.getValueAt(i, 30).toString());
                psLineItem.setDescriptionThree("" + model.getValueAt(i, 31).toString());
                psLineItem.setDescriptionFour("" + model.getValueAt(i, 32).toString());
                psLineItem.setDescriptionFive("" + model.getValueAt(i, 33).toString());
                psLineItem.setDescriptionSix("" + model.getValueAt(i, 34).toString());
                psLineItem.setDescriptionSeven("" + model.getValueAt(i, 35).toString());
                psLineItem.setDescriptionEight("" + model.getValueAt(i, 36).toString());
                psLineItem.setDescriptionNine("" + model.getValueAt(i, 37).toString());
                psLineItem.setDescriptionTen("" + model.getValueAt(i, 38).toString());
                psLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 39).toString()));
                psLineItem.setUqcOneRateWithTax(convertDecimal.Currency(model.getValueAt(i, 41).toString()));
                psLineItem.setUqcTwoRateWithTax(convertDecimal.Currency(model.getValueAt(i, 42).toString()));

                psLineItems.add(psLineItem);
                Boolean isNewSalesTax = true;
                for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                    if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                        float amount = tax.getTaxableValue().floatValue();
                        amount = (float) model.getValueAt(i, 15) + amount;
                        float Igst = tax.getIgstValue().floatValue();
                        Igst = (float) model.getValueAt(i, 17) + Igst;
                        float Cgst = tax.getCgstValue().floatValue();
                        Cgst = (float) model.getValueAt(i, 19) + Cgst;
                        float Sgst = tax.getSgstValue().floatValue();
                        Sgst = (float) model.getValueAt(i, 21) + Sgst;
                        float taxAmount = Igst + Cgst + Sgst;

                        tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                        tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                        tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                        tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                        tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));

                        isNewSalesTax = false;
                    }
                }
                if (isNewSalesTax) {
                    PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                    float amount = (float) model.getValueAt(i, 15);
                    float Igst = (float) model.getValueAt(i, 17);
                    float Cgst = (float) model.getValueAt(i, 19);
                    float Sgst = (float) model.getValueAt(i, 21);
                    float taxAmount = Igst + Cgst + Sgst;

//                        newSaleTaxSummary.setPsTaxId(count);
//                        newSaleTaxSummary.setPurchaseSale(newPurchaseSale);
                    newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                    newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 16));
                    newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 18));
                    newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 20));
                    newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                    purchaseSalesTax.add(newSaleTaxSummary);

                    count++;
                }
            }

            EventStatus result = new PurchaseSaleLogic().createAPS("SALES", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);

            if (result.isCreateDone()) {
                if (needPrint) {
                    new TaxInvoiceNew().pdfByBillId(true, false, result.getBillID(), new GetPDFHeaders().getPdfHeaders(), false);
//                        if (checkSMS.isSelected()) {
//                            sendSms(result.getBillID());
//                        }
                }
                JOptionPane.showMessageDialog(null, "New Bill is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                clearAll();
            }
            else {
                JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                bError = true;
            }
        }
        else if (!bError) {
            JOptionPane.showMessageDialog(null, "Bill is Canceled", "Alert", JOptionPane.WARNING_MESSAGE);
            bError = true;
        }

    }

    public void sendSms(String Id) {

        PurchaseSale newPS = new PurchaseSaleLogic().getPSDetail(Id);
        String message = newPS.getSmsMessage().replaceAll("####", newPS.getBillNo());
        String phone = newPS.getLedger().getGSTIN();
        if (message == null) {
            JOptionPane.showMessageDialog(null, "Message not found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        else if (phone.equals("") || phone == null) {
            JOptionPane.showMessageDialog(null, "Mobile Number not found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        else {
            try {
                message = message.replaceAll(" ", "%20");

                //               String limitMessage = message.substring(0, 160);
                String url = "http://api.cutesms.in/sms.aspx?a=submit&email=srinivas@marvellabs.in&pw=gQa96&sid=SAKTHI&msg=" + message + "&to=" + phone;

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            } catch (MalformedURLException ex) {
                Logger.getLogger(SalesB2CUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SalesB2CUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SalesB2CUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void clearAll() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
//        ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
//        sortedLedgers = new ArrayList<Ledger>(ledgers);
//        Collections.sort(sortedLedgers, new Comparator<Ledger>() {
//            public int compare(Ledger o1, Ledger o2) {
//                return o1.getLedgerName().compareTo(o2.getLedgerName());
//            }
//        });
//        setParty();
        comboParty.setSelectedItem("No Party");
        txtPartyName.setText("");
        txtAddrLine.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        comboState.setSelectedItem("Tamil Nadu");
        txtGSTIN.setText("");
        dateBill.setDate(new Date());
        txtTotalBillAmount.setText("");
        txtBillDiscount.setText("");

        labelPaymentBillAmount.setText("0.00");
        txtPayment.setText("");
        labelBalance.setText("0.00");
        txtRemarks.setText("");

        clear(false);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        comboParty.requestFocus();
    }

    public void addProductList() {
        boolean isError = true;
        boolean productUpdate = false;
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDisount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        String strProduct = "";
        strProduct = comboProduct.getSelectedItem().toString();
        boolean checkQty = false;

        if (!productDetail.isService()) {
            if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
                checkQty = true;
            }
        }

        if (comboProduct.getSelectedItem().equals("")) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Select the Product to Add", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        }
        else if (checkQty) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        }
        else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1RateWithTax.requestFocus();
        }
        else {

            int count = 1;
            int wholeQuantity = 0;

            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0, fCessAmt = 0, UQC1Value = 0;
            float UQC1RateWithTax = 0, UQC2RateWithTax = 0;
            float lineItemUQC1rate = 0, lineItemUQC2rate = 0, lineItemfCessAmt = 0;
            float lineItemAmt = 0, lineItemDiscount = 0, lineItemIGST = 0, lineItemCGST = 0, lineItemSGST = 0;
            float lineItemProfit = 0, lineItemTotalAmount = 0, lineItemRate1Tax = 0, lineItemRate2Tax = 0;
            int lineItemUQC1qty = 0, lineItemUQC2qty = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if (!strCessAmount.equals("")) {
                fCessAmt = Float.parseFloat(strCessAmount);
            }
            else {
                txtCessAmt.setText("0");
            }
            if (!strUQC1rateWithTax.equals("")) {
                UQC1RateWithTax = Float.parseFloat(strUQC1rateWithTax);
            }
            if (!strUQC2rateWithTax.equals("")) {
                UQC2RateWithTax = Float.parseFloat(strUQC2rateWithTax);
            }
            boolean bMsg = true;
            if (UQC1rate == 0 && UQC2rate == 0) {
                bMsg = false;
                isError = false;
                JOptionPane.showMessageDialog(null, "Please Enter The Price");
            }
            int rowId = isProductHereInTable(strProduct, -1);
            if (rowId >= 0) {
                productUpdate = true;
//                JOptionPane.showConfirmDialog(null,strProduct+" is Already in the list Do you want to add More", "CONFORM", JOptionPane.YES_NO_OPTION);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                if ((model.getValueAt(rowId, 4) != null) && !(model.getValueAt(rowId, 4).equals(""))) {
                    lineItemUQC1qty = Integer.parseInt(model.getValueAt(rowId, 4).toString());
                }
                if ((model.getValueAt(rowId, 5) != null) && !(model.getValueAt(rowId, 5).equals(""))) {
                    lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 5).toString());
                }
                if ((model.getValueAt(rowId, 8) != null) && !(model.getValueAt(rowId, 8).equals(""))) {
                    lineItemUQC2qty = Integer.parseInt(model.getValueAt(rowId, 8).toString());
                }
                if ((model.getValueAt(rowId, 9) != null) && !(model.getValueAt(rowId, 9).equals(""))) {
                    lineItemUQC2rate = Float.parseFloat(model.getValueAt(rowId, 9).toString());
                }
                if ((model.getValueAt(rowId, 14) != null) && !(model.getValueAt(rowId, 14).equals(""))) {
                    lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 14).toString());
                }
                if ((model.getValueAt(rowId, 15) != null) && !(model.getValueAt(rowId, 15).equals(""))) {
                    lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 15).toString());
                }
                if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                    lineItemIGST = Float.parseFloat(model.getValueAt(rowId, 17).toString());
                }
                if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                    lineItemCGST = Float.parseFloat(model.getValueAt(rowId, 19).toString());
                }
                if ((model.getValueAt(rowId, 21) != null) && !(model.getValueAt(rowId, 21).equals(""))) {
                    lineItemSGST = Float.parseFloat(model.getValueAt(rowId, 21).toString());
                }
                if ((model.getValueAt(rowId, 24) != null) && !(model.getValueAt(rowId, 24).equals(""))) {
                    lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 24).toString());
                }
                if ((model.getValueAt(rowId, 40) != null) && !(model.getValueAt(rowId, 40).equals(""))) {
                    lineItemTotalAmount = Float.parseFloat(model.getValueAt(rowId, 40).toString());
                }
                if ((model.getValueAt(rowId, 25) != null) && !(model.getValueAt(rowId, 25).equals(""))) {
                    lineItemfCessAmt = Float.parseFloat(model.getValueAt(rowId, 25).toString());
                }
                if ((model.getValueAt(rowId, 41) != null) && !(model.getValueAt(rowId, 41).equals(""))) {
                    lineItemRate1Tax = Float.parseFloat(model.getValueAt(rowId, 41).toString());
                }
                if ((model.getValueAt(rowId, 42) != null) && !(model.getValueAt(rowId, 42).equals(""))) {
                    lineItemRate2Tax = Float.parseFloat(model.getValueAt(rowId, 42).toString());
                }
                descAdded = true;
                UQC1qty = UQC1qty + lineItemUQC1qty;
                UQC1rate = (UQC1rate + lineItemUQC1rate) / 2;
                UQC2qty = UQC2qty + lineItemUQC2qty;
                UQC2rate = (UQC2rate + lineItemUQC2rate) / 2;
                UQC1RateWithTax = (UQC1RateWithTax + lineItemRate1Tax) / 2;
                UQC2RateWithTax = (UQC2RateWithTax + lineItemRate2Tax) / 2;
            }

            String strQuantity = "";

            if (UQC1qty == 0) {
                strQuantity = UQC2qty + " " + labelUQC2.getText();
            }
            else if (UQC2qty == 0) {
                strQuantity = UQC1qty + " " + labelUQC1.getText();
            }
            else {
                strQuantity = UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText();
            }
            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;

            float IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                    CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                    SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                    cessAmt = Float.parseFloat(txtCessAmt.getText()),
                    Amount = Float.parseFloat(txtAmount.getText());
            String Discountper = "0.00", Discount = "0.00";
            if (!strDiscountPer.equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            }
            if (!strDisount.equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            }
            int nUQC2Value = 0;
            Product descProduct = null;
            for (Product selProduct : products) {
                if (selProduct.getName().equals(comboProduct.getSelectedItem().toString())) {
                    if (selProduct.getBaseProductRate() != null) {
                        UQC2Baserate = selProduct.getBaseProductRate().floatValue();
                    }
                    nUQC2Value = selProduct.getUQC2Value();
                    descProduct = selProduct;
                }
            }

            if (descAdded) {
                desc1 = productDescriptionUI.prodDesc1;
                desc2 = productDescriptionUI.prodDesc2;
                desc3 = productDescriptionUI.prodDesc3;
                desc4 = productDescriptionUI.prodDesc4;
                desc5 = productDescriptionUI.prodDesc5;
                desc6 = productDescriptionUI.prodDesc6;
                desc7 = productDescriptionUI.prodDesc7;
                desc8 = productDescriptionUI.prodDesc8;
                desc9 = productDescriptionUI.prodDesc9;
                desc10 = productDescriptionUI.prodDesc10;
                descCount = productDescriptionUI.descCount;
            }
            else {
                desc1 = descProduct.getDescriptionOne();
                desc2 = descProduct.getDescriptionTwo();
                desc3 = descProduct.getDescriptionThree();
                desc4 = descProduct.getDescriptionFour();
                desc5 = descProduct.getDescriptionFive();
                desc6 = descProduct.getDescriptionSix();
                desc7 = descProduct.getDescriptionSeven();
                desc8 = descProduct.getDescriptionEight();
                desc9 = descProduct.getDescriptionNine();
                desc10 = descProduct.getDescriptionTen();
                descCount = descProduct.getDescriptioncount();
            }
            float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - (((UQC1qty * nUQC2Value) + UQC2qty) * UQC2Baserate);
            Profit = Profit - Float.parseFloat(Discount);
            String strTotalAmountInclTax = currency.format(Amount + IGST_amt + CGST_amt + SGST_amt + cessAmt);
            Float TotalAmoutInclTax = Float.parseFloat(strTotalAmountInclTax);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            bCheckStock = false;
            setStock(comboProduct.getEditor().getItem().toString());
            String strGodown = "";
            boolean bService = descProduct.isService();
            if (!bService && isInventoryEnabled) {
                strGodown = comboGodown.getSelectedItem().toString();
            }
            isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
            if (isInventoryEnabled && !isUnderStockPossible) {
                if (wholeQuantity <= productStock) {
                    bCheckStock = true;
                }
            }
            else {
                bCheckStock = true;
            }
            float discount = Float.parseFloat(Discount) + lineItemDiscount;
            Amount = Amount + lineItemAmt;
            if (discount != 0) {
                Discountper = currency.format((discount / Amount) * 100);
                Discount = currency.format(discount);
            }
            IGST_amt = IGST_amt + lineItemIGST;
            SGST_amt = SGST_amt + lineItemSGST;
            CGST_amt = CGST_amt + lineItemCGST;
            Profit = Profit + lineItemProfit;
            fCessAmt = fCessAmt + lineItemfCessAmt;
            TotalAmoutInclTax = TotalAmoutInclTax + lineItemTotalAmount;
            int pos = 0;
            if (!bService && (bCheckStock && isError)) {
                for (int ii = 0; ii < model.getRowCount(); ii++) {
                    if ((Boolean) model.getValueAt(ii, 27)) {
                        pos = ii;
                        break;
                    }
                    else {
                        pos++;
                        count++;
                    }
                }

                Object[] row = {count, strProduct, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                    labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                    wholeQuantity, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGSTper, IGST_amt, CGSTper, CGST_amt, SGSTper, SGST_amt,
                    UQC1Baserate, UQC2Baserate, Profit, fCessAmt, Amount, false, strGodown, desc1, desc2, desc3, desc4, desc5, desc6,
                    desc7, desc8, desc9, desc10, descCount, TotalAmoutInclTax, UQC1RateWithTax, UQC2RateWithTax};
                if (productUpdate) {
                    model.setValueAt(descProduct.getName(), rowId, 1);
                    model.setValueAt(HSNCode, rowId, 2);
                    model.setValueAt(labelUQC1.getText(), rowId, 3);
                    model.setValueAt(UQC1qty, rowId, 4);
                    model.setValueAt(UQC1rate, rowId, 5);
                    model.setValueAt(UQC1qty * UQC1rate, rowId, 6);
                    model.setValueAt(labelUQC2.getText(), rowId, 7);
                    model.setValueAt(UQC2qty, rowId, 8);
                    model.setValueAt(UQC2rate, rowId, 9);
                    model.setValueAt(UQC2qty * UQC2rate, rowId, 10);
                    model.setValueAt(strQuantity, rowId, 11);
                    model.setValueAt(wholeQuantity, rowId, 12);
                    model.setValueAt(Float.parseFloat(Discountper), rowId, 13);
                    model.setValueAt(Float.parseFloat(Discount), rowId, 14);
                    model.setValueAt(Amount, rowId, 15);
                    model.setValueAt(IGSTper, rowId, 16);
                    model.setValueAt(IGST_amt, rowId, 17);
                    model.setValueAt(CGSTper, rowId, 18);
                    model.setValueAt(CGST_amt, rowId, 19);
                    model.setValueAt(SGSTper, rowId, 20);
                    model.setValueAt(SGST_amt, rowId, 21);
                    model.setValueAt(UQC1Baserate, rowId, 22);
                    model.setValueAt(UQC2Baserate, rowId, 23);
                    model.setValueAt(Profit, rowId, 24);
                    model.setValueAt(fCessAmt, rowId, 25);
                    model.setValueAt(Amount, rowId, 26);
                    model.setValueAt(strGodown, rowId, 28);
                    model.setValueAt(desc1, rowId, 29);
                    model.setValueAt(desc2, rowId, 30);
                    model.setValueAt(desc3, rowId, 31);
                    model.setValueAt(desc4, rowId, 32);
                    model.setValueAt(desc5, rowId, 33);
                    model.setValueAt(desc6, rowId, 34);
                    model.setValueAt(desc7, rowId, 35);
                    model.setValueAt(desc8, rowId, 36);
                    model.setValueAt(desc9, rowId, 37);
                    model.setValueAt(desc10, rowId, 38);
                    model.setValueAt(descCount, rowId, 39);
                    model.setValueAt(TotalAmoutInclTax, rowId, 40);
                    model.setValueAt(UQC1RateWithTax, rowId, 41);
                    model.setValueAt(UQC2RateWithTax, rowId, 42);

                }
                else {
                    model.insertRow(pos, row);
                }

                clear(false);
                jTableRender();
                comboProduct.requestFocus();
                btnProductDescription.setEnabled(false);
                TotalCalculate();
            }
            else if (bService) {
                for (int ii = 0; ii < model.getRowCount(); ii++) {
                    if ((Boolean) model.getValueAt(ii, 27)) {
                        pos = ii;
                        break;
                    }
                    else {
                        pos++;
                        count++;
                    }
                }
                Object[] row = {count, strProduct, HSNCode, "", UQC1qty, UQC1rate, UQC1rate,
                    "", 0, 0, 0, "",
                    0, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGSTper, IGST_amt, CGSTper, CGST_amt, SGSTper, SGST_amt,
                    UQC1Baserate, UQC2Baserate, Profit, fCessAmt, Amount, true, "", desc1, desc2, desc3, desc4, desc5, desc6,
                    desc7, desc8, desc9, desc10, descCount, TotalAmoutInclTax, UQC1RateWithTax, 0};
                model.insertRow(pos, row);
                clear(false);
                jTableRender();
                comboProduct.requestFocus();
                btnProductDescription.setEnabled(false);
                TotalCalculate();
            }
            else if (bMsg) {
                JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
            else {
                txtUQC1RateWithTax.requestFocus();
            }
        }
    }

    public void updateProductsItems() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        int i = jTable1.getSelectedRow();
        if (i >= 0) {

            if (comboProduct.getSelectedItem().equals("")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            }
            else if (strUQC1qty.equals("") && strUQC2qty.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
            else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1RateWithTax.requestFocus();
            }
            else if (isProductHereInTable(comboProduct.getSelectedItem().toString(), i) > -1) {
                JOptionPane.showMessageDialog(null, "Product already in List", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            }
            else {
                int count = jTable1.getRowCount() + 1;
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0, fCessAmt = 0;
                float UQC1RateWithTax = 0, UQC2RateWithTax = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strUQC2rate.equals("")) {
                    UQC2rate = Float.parseFloat(strUQC2rate);
                }
                if (!strUQC1rateWithTax.equals("")) {
                    UQC1RateWithTax = Float.parseFloat(strUQC1rateWithTax);
                }
                if (!strUQC2rateWithTax.equals("")) {
                    UQC2RateWithTax = Float.parseFloat(strUQC1rateWithTax);
                }
                if (!strCessAmount.equals("")) {
                    fCessAmt = Float.parseFloat(strCessAmount);
                }
                else {
                    txtCessAmt.setText("0");
                }
                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
                float Rate = Float.parseFloat(txtUQC1RateWithTax.getText()),
                        IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                        CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                        SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                        Amount = Float.parseFloat(txtAmount.getText()),
                        cessAmt = Float.parseFloat(txtCessAmt.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String selProduct = (String) comboProduct.getSelectedItem();
                float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
                Profit = Profit - Float.parseFloat(Discount);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                Float TotalAmountInclTax = Amount + IGST_amt + CGST_amt + SGST_amt + cessAmt;
                setStock(comboProduct.getEditor().getItem().toString());
                Product selecProduct = null;
                for (Product selectedProduct : products) {
                    if (selectedProduct.getName().equalsIgnoreCase(selProduct)) {
                        selecProduct = selectedProduct;
                        break;
                    }
                }
                String strGodown = "";
                boolean bService = selecProduct.isService();
                if (!bService && isInventoryEnabled) {
                    strGodown = comboGodown.getSelectedItem().toString();
                }
                isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
                bCheckStock = false;
                if (isInventoryEnabled && !isUnderStockPossible) {
                    if (wholeQuantity <= productStock) {
                        bCheckStock = true;
                    }
                }
                else {
                    bCheckStock = true;
                }
                if (descAdded) {
                    desc1 = productDescriptionUI.prodDesc1;
                    desc2 = productDescriptionUI.prodDesc2;
                    desc3 = productDescriptionUI.prodDesc3;
                    desc4 = productDescriptionUI.prodDesc4;
                    desc5 = productDescriptionUI.prodDesc5;
                    desc6 = productDescriptionUI.prodDesc6;
                    desc7 = productDescriptionUI.prodDesc7;
                    desc8 = productDescriptionUI.prodDesc8;
                    desc9 = productDescriptionUI.prodDesc9;
                    desc10 = productDescriptionUI.prodDesc10;
                    descCount = productDescriptionUI.descCount;
                }
                if (bService) {
                    model.setValueAt(selProduct, i, 1);
                    model.setValueAt(HSNCode, i, 2);
                    model.setValueAt("", i, 3);
                    model.setValueAt(0, i, 4);
                    model.setValueAt(UQC1rate, i, 5);
                    model.setValueAt(UQC1rate, i, 6);
                    model.setValueAt("", i, 7);
                    model.setValueAt(0, i, 8);
                    model.setValueAt(0, i, 9);
                    model.setValueAt(0, i, 10);
                    model.setValueAt("", i, 11);
                    model.setValueAt(0, i, 12);
                    model.setValueAt(Float.parseFloat(Discountper), i, 13);
                    model.setValueAt(Float.parseFloat(Discount), i, 14);
                    model.setValueAt(Amount, i, 15);
                    model.setValueAt(IGSTper, i, 16);
                    model.setValueAt(IGST_amt, i, 17);
                    model.setValueAt(CGSTper, i, 18);
                    model.setValueAt(CGST_amt, i, 19);
                    model.setValueAt(SGSTper, i, 20);
                    model.setValueAt(SGST_amt, i, 21);
                    model.setValueAt(UQC1Baserate, i, 22);
                    model.setValueAt(UQC2Baserate, i, 23);
                    model.setValueAt(Profit, i, 24);
                    model.setValueAt(fCessAmt, i, 25);
                    model.setValueAt(Amount, i, 26);
                    model.setValueAt(strGodown, i, 28);
                    model.setValueAt(desc1, i, 29);
                    model.setValueAt(desc2, i, 30);
                    model.setValueAt(desc3, i, 31);
                    model.setValueAt(desc4, i, 32);
                    model.setValueAt(desc5, i, 33);
                    model.setValueAt(desc6, i, 34);
                    model.setValueAt(desc7, i, 35);
                    model.setValueAt(desc8, i, 36);
                    model.setValueAt(desc9, i, 37);
                    model.setValueAt(desc10, i, 38);
                    model.setValueAt(descCount, i, 39);
                    model.setValueAt(TotalAmountInclTax, i, 40);
                    model.setValueAt(UQC1RateWithTax, i, 41);
                    model.setValueAt(UQC2RateWithTax, i, 42);
                    clear(false);
                    TotalCalculate();
                    jTableRender();
                    comboProduct.requestFocus();
                    btnProductDescription.setEnabled(false);
                }
                else if (bCheckStock) {
                    model.setValueAt(selProduct, i, 1);
                    model.setValueAt(HSNCode, i, 2);
                    model.setValueAt(labelUQC1.getText(), i, 3);
                    model.setValueAt(UQC1qty, i, 4);
                    model.setValueAt(UQC1rate, i, 5);
                    model.setValueAt(UQC1qty * UQC1rate, i, 6);
                    model.setValueAt(labelUQC2.getText(), i, 7);
                    model.setValueAt(UQC2qty, i, 8);
                    model.setValueAt(UQC2rate, i, 9);
                    model.setValueAt(UQC2qty * UQC2rate, i, 10);
                    model.setValueAt(UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(), i, 11);
                    model.setValueAt(wholeQuantity, i, 12);
                    model.setValueAt(Float.parseFloat(Discountper), i, 13);
                    model.setValueAt(Float.parseFloat(Discount), i, 14);
                    model.setValueAt(Amount, i, 15);
                    model.setValueAt(IGSTper, i, 16);
                    model.setValueAt(IGST_amt, i, 17);
                    model.setValueAt(CGSTper, i, 18);
                    model.setValueAt(CGST_amt, i, 19);
                    model.setValueAt(SGSTper, i, 20);
                    model.setValueAt(SGST_amt, i, 21);
                    model.setValueAt(UQC1Baserate, i, 22);
                    model.setValueAt(UQC2Baserate, i, 23);
                    model.setValueAt(Profit, i, 24);
                    model.setValueAt(fCessAmt, i, 25);
                    model.setValueAt(Amount, i, 26);
                    model.setValueAt(strGodown, i, 28);
                    model.setValueAt(desc1, i, 29);
                    model.setValueAt(desc2, i, 30);
                    model.setValueAt(desc3, i, 31);
                    model.setValueAt(desc4, i, 32);
                    model.setValueAt(desc5, i, 33);
                    model.setValueAt(desc6, i, 34);
                    model.setValueAt(desc7, i, 35);
                    model.setValueAt(desc8, i, 36);
                    model.setValueAt(desc9, i, 37);
                    model.setValueAt(desc10, i, 38);
                    model.setValueAt(descCount, i, 39);
                    model.setValueAt(TotalAmountInclTax, i, 40);
                    model.setValueAt(UQC1RateWithTax, i, 41);
                    model.setValueAt(UQC2RateWithTax, i, 42);
                    clear(false);
                    TotalCalculate();
                    jTableRender();
                    comboProduct.requestFocus();
                    btnProductDescription.setEnabled(false);
                }
                else {
                    JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
            TotalCalculate();
        }
        else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProductsItems();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        SaveAndPrint(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear(true);
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
        txtDiscount.requestFocus();
    }//GEN-LAST:event_txtDiscountPerActionPerformed

    private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDiscountPerKeyPressed

    private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtDiscountPerKeyReleased

    private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped

    }//GEN-LAST:event_txtDiscountPerKeyTyped

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        txtCessAmt.requestFocus();
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
//        String quantity = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String quantity2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String rate = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String rate2 = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String discountAmt = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String strDiscountPer = "";
//        Boolean isError = false;
//        if (productDetail.isService()) {
//            if (rate.equals("")) {
//                isError = true;
//            }
//        } else {
//            if ((rate.equals("") || rate2.equals("")) && ((quantity.equals("")) || (quantity2.equals("")))) {
//                isError = true;
//            }
//        }
//        if (!isError) {
//            int Qty = 0, Qty2 = 0;
//            float Rate = 0, Rate2 = 0;
//            if (productDetail.isService()) {
//                Qty = 1;
//            }
//
//            if (!quantity.equals("")) {
//                Qty = Integer.parseInt(quantity);
//            }
//
//            if (!quantity2.equals("")) {
//                Qty2 = Integer.parseInt(quantity2);
//            }
//
//            if (!rate.equals("")) {
//                Rate = Float.parseFloat(rate);
//            }
//
//            if (!rate2.equals("")) {
//                Rate2 = Float.parseFloat(rate2);
//            }
//            float TotalAmount = (Rate * Qty) + (Rate2 * Qty2);
//            Float DiscountAmount = 0.00f;
//            if (!discountAmt.equals("") && TotalAmount > 0) {
//                DiscountAmount = Float.parseFloat(discountAmt);
//                strDiscountPer = currency.format((DiscountAmount / TotalAmount) * 100);
//            } else {
//                strDiscountPer = "0.00";
//            }
//            txtDiscountPer.setText(strDiscountPer);
//            TotalAmount = TotalAmount - DiscountAmount;
//            float TaxValue = TotalAmount * (IGSTper / 100);
//            txtAmount.setText(currency.format(TotalAmount));
//            lblProductsTotal.setText(currency.format(TotalAmount + TaxValue));
//            if (isIGST) {
//                txtIGSTamt.setText(currency.format(TaxValue));
//                txtCGSTamt.setText("0.00");
//                txtSGSTamt.setText("0.00");
//            } else {
//                txtIGSTamt.setText("0.00");
//                txtCGSTamt.setText(currency.format(TaxValue / 2));
//                txtSGSTamt.setText(currency.format(TaxValue / 2));
//            }
//        } else {
//            JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value", "Alert", JOptionPane.WARNING_MESSAGE);
//        }
        if (isServiceProduct) {
            TaxvalueCalculationForService(true);
        }
        else {
            TaxvalueCalculationForProduct(true);
        }
    }//GEN-LAST:event_txtDiscountKeyReleased

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2RateWithTax.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        comboProduct.setSelectedItem(model.getValueAt(i, 1).toString());
        getProductDetails(model.getValueAt(i, 1).toString());
        comboProduct.setEnabled(false);
        labelUQC2.setText((String) model.getValueAt(i, 7));
        labelUQC2forPrice.setText((String) model.getValueAt(i, 7));
        txtUQC2Qty.setText(model.getValueAt(i, 8).toString());
        txtUQC2Rate.setText(currency.format(model.getValueAt(i, 9)));

        HSNCode = (String) model.getValueAt(i, 2);
        labelUQC1.setText((String) model.getValueAt(i, 3));
        labelUQC1forPrice.setText((String) model.getValueAt(i, 3));
        txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
        txtUQC1Rate.setText(currency.format(model.getValueAt(i, 5)));

        txtDiscountPer.setText(model.getValueAt(i, 13).toString());
        txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
        txtAmount.setText(currency.format(model.getValueAt(i, 15)));
        txtIGSTper.setText(model.getValueAt(i, 16).toString());
        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
        txtCGSTper.setText(model.getValueAt(i, 18).toString());
        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
        txtSGSTper.setText(model.getValueAt(i, 20).toString());
        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
        lblProductsTotal.setText(currency.format(model.getValueAt(i, 40)));
        txtUQC1RateWithTax.setText(currency.format(model.getValueAt(i, 41)));
        txtUQC2RateWithTax.setText(currency.format(model.getValueAt(i, 42)));

        float cessAmt = 0.00f;
        if (model.getValueAt(i, 25) != null) {
            cessAmt = Float.parseFloat(model.getValueAt(i, 25).toString());
        }
        txtCessAmt.setText(cessAmt + "");
        comboGodown.setSelectedItem(model.getValueAt(i, 28));
        btnAdd.setEnabled(false);
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);
        txtUQC1Qty.requestFocus();
        productDescriptionUI.prodDesc1 = model.getValueAt(i, 29).toString();
        productDescriptionUI.prodDesc2 = model.getValueAt(i, 30).toString();
        productDescriptionUI.prodDesc3 = model.getValueAt(i, 31).toString();
        productDescriptionUI.prodDesc4 = model.getValueAt(i, 32).toString();
        productDescriptionUI.prodDesc5 = model.getValueAt(i, 33).toString();
        productDescriptionUI.prodDesc6 = model.getValueAt(i, 34).toString();
        productDescriptionUI.prodDesc7 = model.getValueAt(i, 35).toString();
        productDescriptionUI.prodDesc8 = model.getValueAt(i, 36).toString();
        productDescriptionUI.prodDesc9 = model.getValueAt(i, 37).toString();
        productDescriptionUI.prodDesc10 = model.getValueAt(i, 38).toString();
        productDescriptionUI.descCount = Integer.parseInt(model.getValueAt(i, 39).toString());
        desc1 = productDescriptionUI.prodDesc1;
        desc2 = productDescriptionUI.prodDesc2;
        desc3 = productDescriptionUI.prodDesc3;
        desc4 = productDescriptionUI.prodDesc4;
        desc5 = productDescriptionUI.prodDesc5;
        desc6 = productDescriptionUI.prodDesc6;
        desc7 = productDescriptionUI.prodDesc7;
        desc8 = productDescriptionUI.prodDesc8;
        desc9 = productDescriptionUI.prodDesc9;
        desc10 = productDescriptionUI.prodDesc10;
        descCount = productDescriptionUI.descCount;
        setStock(comboProduct.getEditor().getItem().toString());

        if (isServiceProduct) {
            if (isProductTaxInclusive) {
                txtUQC1RateWithTax.requestFocus();
            }
            else {
                txtUQC1Rate.requestFocus();
            }
        }
        else {
            txtUQC1Qty.requestFocus();
        }

        //getProductDetails();
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtUQC2QtyKeyReleased

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
        bErrorAlert = true;
    }//GEN-LAST:event_txtUQC1QtyKeyReleased

    private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC1QtyKeyTyped

    private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC2QtyKeyTyped


    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        if (txtUQC2Qty.isVisible()) {
            if (bUQCSame) {
                if (isProductTaxInclusive) {
                    txtUQC1RateWithTax.requestFocus();
                }
                else {
                    txtUQC1Rate.requestFocus();
                }
            }
            else {
                txtUQC2Qty.requestFocus();
            }
        }
        else {

        }

    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
        if (isProductTaxInclusive) {
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            txtUQC1Rate.requestFocus();
        }
    }//GEN-LAST:event_txtUQC2QtyActionPerformed

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
//        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//            updateProductsItems();
//            comboProduct.requestFocus();
//            evt.consume();
//        }
    }//GEN-LAST:event_btnUpdateKeyPressed


    private void comboProductKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboProductKeyPressed


    }//GEN-LAST:event_comboProductKeyPressed

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained

    }//GEN-LAST:event_comboPartyFocusGained

    private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_comboPartyKeyPressed

    private void comboPartyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyReleased

    }//GEN-LAST:event_comboPartyKeyReleased

    private void comboStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboStateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtGSTIN.requestFocus();
        }
    }//GEN-LAST:event_comboStateKeyPressed

    private void txtGSTINKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGSTINKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboPricingPolicy.requestFocus();
        }
    }//GEN-LAST:event_txtGSTINKeyPressed

    private void txtUQC1QtyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUQC1QtyFocusGained
        if (comboProduct.getSelectedItem().equals("") || comboProduct.getSelectedItem() == null) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyFocusGained

    private void comboStateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboStateFocusGained
        comboState.showPopup();
    }//GEN-LAST:event_comboStateFocusGained

    private void txtCessAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCessAmtActionPerformed
        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        }
        else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtCessAmtActionPerformed

    private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
        bErrorAlert = true;
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2QtyKeyPressed

    private void txtDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
        else {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (comboGodown.isVisible()) {
                    comboGodown.requestFocus();
                }
                else {
                    txtCessAmt.requestFocus();
                }
            }
        }
    }//GEN-LAST:event_txtDiscountKeyPressed

    private void txtCessAmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessAmtKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strCess = txtCessAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strCess.equals("")) {
                txtCessAmt.setText("0");
            }
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCessAmtKeyPressed

    private void comboGodownFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboGodownFocusGained
        comboGodown.showPopup();
    }//GEN-LAST:event_comboGodownFocusGained

    private void comboGodownKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGodownKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCessAmt.requestFocus();
            evt.consume();
        }
    }//GEN-LAST:event_comboGodownKeyPressed

    private void comboProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductActionPerformed

    }//GEN-LAST:event_comboProductActionPerformed

    private void txtPartyNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartyNameKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtPartyNameKeyPressed

    private void txtAddrLineKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddrLineKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtAddrLineKeyPressed

    private void txtCityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtCityKeyPressed

    private void txtDistrictKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDistrictKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
            evt.consume();
        }
    }//GEN-LAST:event_txtDistrictKeyPressed

    private void comboGodownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGodownActionPerformed

    }//GEN-LAST:event_comboGodownActionPerformed

    private void comboGodownKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGodownKeyReleased

    }//GEN-LAST:event_comboGodownKeyReleased

    private void txtBillDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillDiscountActionPerformed

    }//GEN-LAST:event_txtBillDiscountActionPerformed

    private void txtBillDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtBillDiscountKeyPressed

    private void txtBillDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyReleased

        if (evt.getKeyCode() != KeyEvent.VK_ENTER && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE && Character.isDigit(evt.getKeyChar())) {
            discount = Float.parseFloat(txtBillDiscount.getText());
            amount = amountWithoutDiscount - discount;
            txtTotalBillAmount.setText(currency.format(amount));
            discountPer = (discount / amountWithoutDiscount) * 100;
        }
        else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && (amount < amountWithoutDiscount)) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscount.equals("")) {
                Float fDiscount = Float.parseFloat("0" + txtBillDiscount.getText());
                if ((!isOldBill) || (!(fDiscount == oldDiscount))) {
                    amount = amount + (discount - fDiscount);
                    discount = discount - (discount - fDiscount);
                    txtTotalBillAmount.setText(currency.format(amount));
                    discountPer = (discount / amountWithoutDiscount) * 100;
                }
            }
            else {
                txtTotalBillAmount.setText(currency.format(amountWithoutDiscount));
                discountPer = 0;
            }
        }
        else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strDiscount.equals("")) {
                txtBillDiscount.setText("0");
            }
            btnSave.requestFocus();
        }
        evt.consume();
    }//GEN-LAST:event_txtBillDiscountKeyReleased

    private void txtBillDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBillDiscount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBillDiscountKeyTyped

    private void comboStateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboStateFocusLost
        String state = comboState.getSelectedItem().toString();
        isIGST = !(state.equals(CompanyState));
        setTable();
        txtGSTIN.requestFocus();
        setTable();
    }//GEN-LAST:event_comboStateFocusLost

    private void comboStateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboStateItemStateChanged

    }//GEN-LAST:event_comboStateItemStateChanged

    private void dateBillKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateBillKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_dateBillKeyPressed

    private void btnProductDescriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductDescriptionActionPerformed
        descAdded = true;
        Product selProduct = null;
        String productName = (String) comboProduct.getEditor().getItem();
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                selProduct = product;
                break;
            }
        }
        int p = isProductHereInTable(productName, -1);
        if (jTable1.getSelectedRow() > -1 || p >= 0) {
            productDescriptionUI = new PSLineItemsDescriptionUI(desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10);
            productDescriptionUI.setVisible(true);
        }
        else if (selProduct != null) {
            try {
                productDescriptionUI = new PSLineItemsDescriptionUI(selProduct);

            } catch (IOException ex) {
                Logger.getLogger(SalesB2CUI.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            productDescriptionUI.setVisible(true);
        }
        else {
            productDescriptionUI = new PSLineItemsDescriptionUI();
            productDescriptionUI.setVisible(true);
        }
    }//GEN-LAST:event_btnProductDescriptionActionPerformed

    private void comboStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboStateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboStateActionPerformed

    private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGSTINActionPerformed

    private void comboProductComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_comboProductComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_comboProductComponentHidden

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        billFinished();
    }//GEN-LAST:event_btnOkActionPerformed
    private void billFinished() {
        if (isOldBill) {
            OldPrintAndSaveAction();
        }
        else {
            NewPrintAndSaveAction();
        }

        clearAll();
        paymentDialog.setVisible(false);
    }
    private void txtPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPaymentActionPerformed
        btnOk.requestFocusInWindow();
    }//GEN-LAST:event_txtPaymentActionPerformed

    private void txtPaymentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaymentKeyReleased
        float Amount = Float.parseFloat(labelPaymentBillAmount.getText());
        float payment = 0;
        if (!txtPayment.getText().equals("")) {
            payment = Float.parseFloat(txtPayment.getText());
        }
        float Balance = payment - Amount;
        labelBalance.setText(currency.format(Balance));
    }//GEN-LAST:event_txtPaymentKeyReleased

    private void txtPaymentKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaymentKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPayment.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPaymentKeyTyped

    private void txtUQC2RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateActionPerformed
        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateActionPerformed

    private void txtUQC2RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC1RateWithTax.setText(currency.format(0));
                txtUQC2RateWithTax.setText(currency.format(0));
                txtUQC1Rate.setText(currency.format(0));
            }
            else {
                float fRate2 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate1 = fRate2 * Uqc2Value;
                float fRateWithTax1 = 0;
                float fRateWithTax2 = 0;
                if (productDetail.isInclusiveOfGST()) {
                    fRateWithTax1 = fRate1 + ((fRate1 * IGSTper) / 100);
                }
                else {
                    fRateWithTax1 = fRate1;
                }
                fRateWithTax2 = fRateWithTax1 / Uqc2Value;
                txtUQC1RateWithTax.setText(currency.format(fRateWithTax1));
                txtUQC2RateWithTax.setText(currency.format(fRateWithTax2));
                txtUQC1Rate.setText(currency.format(fRate1));
            }
        }
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtUQC2RateKeyReleased

    private void txtUQC2RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyTyped

    private void txtUQC1RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateActionPerformed
        if (txtUQC2Rate.isEnabled()) {
            txtUQC2Rate.requestFocus();
        }
        else {
            txtDiscountPer.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1RateActionPerformed

    private void txtUQC1RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyPressed

    private void txtUQC1RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC1RateWithTax.setText(currency.format(0));
                txtUQC2RateWithTax.setText(currency.format(0));
                txtUQC2Rate.setText(currency.format(0));
            }
            else {
                float fRate1 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate2 = fRate1 / Uqc2Value;
                float fRateWithTax1 = 0;
                float fRateWithTax2 = 0;
                if (productDetail.isInclusiveOfGST()) {
                    fRateWithTax1 = fRate1 + ((fRate1 * IGSTper) / 100);
                }
                else {
                    fRateWithTax1 = fRate1;
                }
                DecimalFormat dcm = new DecimalFormat("#.0000");
                fRateWithTax2 = fRateWithTax1 / Uqc2Value;
                txtUQC1RateWithTax.setText(currency.format(fRateWithTax1));
                txtUQC2RateWithTax.setText(dcm.format(fRateWithTax2));
                txtUQC2Rate.setText(dcm.format(fRate2));
            }
        }
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtUQC1RateKeyReleased

    private void txtUQC1RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyTyped

    private void txtUQC1RateWithTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxActionPerformed
        if (txtUQC2RateWithTax.isEnabled()) {
            txtUQC2RateWithTax.requestFocus();
        }
        else {
            txtDiscountPer.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1RateWithTaxActionPerformed

    private void txtUQC1RateWithTaxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateWithTaxKeyPressed

    private void txtUQC1RateWithTaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC2RateWithTax.setText(currency.format(0));
                txtUQC1Rate.setText(currency.format(0));
                txtUQC2Rate.setText(currency.format(0));
            }
            else {
                float fRate1 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate2 = fRate1 / Uqc2Value;
                float fRateWithoutTax1 = 0;
                float fRateWithoutTax2 = 0;
                if (productDetail.isInclusiveOfGST()) {
                    fRateWithoutTax1 = (fRate1 / (100 + IGSTper)) * 100;
                }
                else {
                    fRateWithoutTax1 = fRate1;
                }
                fRateWithoutTax2 = fRateWithoutTax1 / Uqc2Value;
                txtUQC1Rate.setText(currency.format(fRateWithoutTax1));
                txtUQC2Rate.setText(currency.format(fRateWithoutTax2));
                txtUQC2RateWithTax.setText(currency.format(fRate2));
            }
        }
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtUQC1RateWithTaxKeyReleased

    private void txtUQC1RateWithTaxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1RateWithTax.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateWithTaxKeyTyped

    private void txtUQC2RateWithTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxActionPerformed
        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateWithTaxActionPerformed

    private void txtUQC2RateWithTaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC1RateWithTax.setText(currency.format(0));
                txtUQC2RateWithTax.setText(currency.format(0));
                txtUQC1Rate.setText(currency.format(0));
            }
            else {
                float fRate2 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate1 = fRate2 * Uqc2Value;
                float fRateWithoutTax1 = 0;
                float fRateWithoutTax2 = 0;
                if (productDetail.isInclusiveOfGST()) {
                    fRateWithoutTax1 = (fRate1 / (100 + IGSTper)) * 100;
                }
                else {
                    fRateWithoutTax1 = fRate1;
                }
                fRateWithoutTax2 = fRateWithoutTax1 / Uqc2Value;
                txtUQC1Rate.setText(currency.format(fRateWithoutTax1));
                txtUQC2Rate.setText(currency.format(fRateWithoutTax2));
                txtUQC1RateWithTax.setText(currency.format(fRate1));
            }
        }
        if (isServiceProduct) {
            TaxvalueCalculationForService(false);
        }
        else {
            TaxvalueCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtUQC2RateWithTaxKeyReleased

    private void txtUQC2RateWithTaxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1RateWithTax.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateWithTaxKeyTyped

    private void btnRemarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemarkActionPerformed
        RemarksDialog.setVisible(true);
        RemarksDialog.setSize(300, 250);
        RemarksDialog.setLocationRelativeTo(null);
        txtRemarks.requestFocusInWindow();
    }//GEN-LAST:event_btnRemarkActionPerformed

    private void btnRemarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemarksActionPerformed
        RemarksDialog.setVisible(false);
        btnSave.requestFocus();
    }//GEN-LAST:event_btnRemarksActionPerformed

    private void txtUQC2RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyPressed

    private void txtUQC2RateWithTaxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2RateWithTaxKeyPressed

    private void txtAmountComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_txtAmountComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAmountComponentAdded

    private void comboProductPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboProductPopupMenuWillBecomeInvisible
//        String name = "";
//        if (isBtnProduct) {
//            name = selValue;
//            isBtnProduct = false;
//        }
//        else {
//            DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
//            name = model.getSelectedItem().toString();
//        }
//
//        getProductDetails(name);
    }//GEN-LAST:event_comboProductPopupMenuWillBecomeInvisible

    private void txtDiscountPerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiscountPerFocusGained
        float qtyVal1 = 0;
        float qtyVal2 = 0;
        String val1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String val2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (isQty && !isServiceProduct) {
            if (!val1.equalsIgnoreCase("")) {
                qtyVal1 = Float.parseFloat(val1);
            }
            if (!val2.equalsIgnoreCase("")) {
                qtyVal2 = Float.parseFloat(val2);
            }
            if (qtyVal1 <= 0 && qtyVal2 <= 0 && isQty) {
                JOptionPane.showMessageDialog(null, "Please Enter the quantity");
                txtUQC1Qty.requestFocus();
                isQty = false;
            }
            else {
                isQty = false;
            }
        }
    }//GEN-LAST:event_txtDiscountPerFocusGained

    private void txtDiscountPerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiscountPerFocusLost
        isQty = true;
    }//GEN-LAST:event_txtDiscountPerFocusLost

    private void comboPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPricingPolicyActionPerformed

        String name = (String) comboPricingPolicy.getSelectedItem();
        if (name.equals("Default")) {
            recentSingleProductPolicies = new HashSet<SingleProductPolicy>();
        }
        else {
            for (PricingPolicy policy : pricingPolicy) {
                if (policy.getPolicyName().equals(name)) {
                    Set<SingleProductPolicy> AllSPP = new PricingPolicyLogic().fetchAllSPP(policy);
                    recentSingleProductPolicies = AllSPP;
                }
            }
        }
        comboProduct.requestFocus();
    }//GEN-LAST:event_comboPricingPolicyActionPerformed

    private void comboPricingPolicyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPricingPolicyKeyPressed


    }//GEN-LAST:event_comboPricingPolicyKeyPressed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void addSingleProductList(Product product) {
        boolean isError = true;
        boolean productUpdate = false;
        String strProduct = product.getName();
        Boolean isDescAdd = false;

        int count = 1;
        int wholeQuantity = 0;

        int UQC1qty = 1, UQC2qty = 0;
        float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0, fCessAmt = 0, UQC1Value = 0;
        float UQC1RateWithTax = 0, UQC2RateWithTax = 0;
        float lineItemUQC1rate = 0, lineItemUQC2rate = 0, lineItemfCessAmt = 0;
        float lineItemAmt = 0, lineItemDiscount = 0, lineItemIGST = 0, lineItemCGST = 0, lineItemSGST = 0;
        float lineItemProfit = 0, lineItemTotalAmount = 0, lineItemRate1Tax = 0, lineItemRate2Tax = 0;
        int lineItemUQC1qty = 0, lineItemUQC2qty = 0;
        float igstPer = product.getIgstPercentage();
        float productRateForUQC1 = product.getProductRate().floatValue() * product.getUQC2Value();
        System.out.println(productRateForUQC1 + " rate " + productRateForUQC1);

        if (product.isInclusiveOfGST()) {
            UQC1RateWithTax = productRateForUQC1;
            UQC1rate = (UQC1RateWithTax / (100 + igstPer)) * 100;
        }
        else {
            UQC1rate = productRateForUQC1;
            UQC1RateWithTax = UQC1rate + ((igstPer / 100) * UQC1rate);
        }
        System.out.println(UQC1rate + " rate " + UQC1RateWithTax);
        boolean bMsg = true;
        if (UQC1rate == 0 && UQC2rate == 0) {
            bMsg = false;
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Enter The Price");
        }
        int rowId = isProductHereInTable(strProduct, -1);
        if (rowId >= 0) {
            productUpdate = true;
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            if ((model.getValueAt(rowId, 4) != null) && !(model.getValueAt(rowId, 4).equals(""))) {
                lineItemUQC1qty = Integer.parseInt(model.getValueAt(rowId, 4).toString());
            }
            if ((model.getValueAt(rowId, 5) != null) && !(model.getValueAt(rowId, 5).equals(""))) {
                lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 5).toString());
            }
            if ((model.getValueAt(rowId, 8) != null) && !(model.getValueAt(rowId, 8).equals(""))) {
                lineItemUQC2qty = Integer.parseInt(model.getValueAt(rowId, 8).toString());
            }
            if ((model.getValueAt(rowId, 9) != null) && !(model.getValueAt(rowId, 9).equals(""))) {
                lineItemUQC2rate = Float.parseFloat(model.getValueAt(rowId, 9).toString());
            }
            if ((model.getValueAt(rowId, 14) != null) && !(model.getValueAt(rowId, 14).equals(""))) {
                lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 14).toString());
            }
            if ((model.getValueAt(rowId, 15) != null) && !(model.getValueAt(rowId, 15).equals(""))) {
                lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 15).toString());
            }
            if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                lineItemIGST = Float.parseFloat(model.getValueAt(rowId, 17).toString());
            }
            if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                lineItemCGST = Float.parseFloat(model.getValueAt(rowId, 19).toString());
            }
            if ((model.getValueAt(rowId, 21) != null) && !(model.getValueAt(rowId, 21).equals(""))) {
                lineItemSGST = Float.parseFloat(model.getValueAt(rowId, 21).toString());
            }
            if ((model.getValueAt(rowId, 24) != null) && !(model.getValueAt(rowId, 24).equals(""))) {
                lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 24).toString());
            }
            if ((model.getValueAt(rowId, 40) != null) && !(model.getValueAt(rowId, 40).equals(""))) {
                lineItemTotalAmount = Float.parseFloat(model.getValueAt(rowId, 40).toString());
            }
            if ((model.getValueAt(rowId, 25) != null) && !(model.getValueAt(rowId, 25).equals(""))) {
                lineItemfCessAmt = Float.parseFloat(model.getValueAt(rowId, 25).toString());
            }
            if ((model.getValueAt(rowId, 41) != null) && !(model.getValueAt(rowId, 41).equals(""))) {
                lineItemRate1Tax = Float.parseFloat(model.getValueAt(rowId, 41).toString());
            }
            if ((model.getValueAt(rowId, 42) != null) && !(model.getValueAt(rowId, 42).equals(""))) {
                lineItemRate2Tax = Float.parseFloat(model.getValueAt(rowId, 42).toString());
            }
            isDescAdd = true;
            UQC1qty = UQC1qty + lineItemUQC1qty;
            UQC1rate = (UQC1rate + lineItemUQC1rate) / 2;
            UQC2qty = UQC2qty + lineItemUQC2qty;
            UQC2rate = (UQC2rate + lineItemUQC2rate) / 2;
            UQC1RateWithTax = (UQC1RateWithTax + lineItemRate1Tax) / 2;
            UQC2RateWithTax = (UQC2RateWithTax + lineItemRate2Tax) / 2;
        }

        String strQuantity = "";

        if (UQC1qty == 0) {
            strQuantity = UQC2qty + " " + product.getUQC2();
        }
        else if (UQC2qty == 0) {
            strQuantity = UQC1qty + " " + product.getUQC1();
        }
        else {
            strQuantity = UQC1qty + " " + product.getUQC1() + "," + UQC2qty + " " + product.getUQC2();
        }
        wholeQuantity = (UQC1qty * product.getUQC2Value()) + UQC2qty;

        float TaxValue = UQC1rate * (product.getIgstPercentage() / 100);
        float IGST_amt = 0,
                CGST_amt = 0,
                SGST_amt = 0,
                Amount = UQC1rate;
        if (isIGST) {
            IGST_amt = TaxValue;
        }
        else {
            CGST_amt = TaxValue / 2;
            SGST_amt = TaxValue / 2;
        }

        int nUQC2Value = 0;
        if (product.getBaseProductRate() != null) {
            UQC2Baserate = product.getBaseProductRate().floatValue();
        }
        nUQC2Value = product.getUQC2Value();

        if (isDescAdd) {
            desc1 = productDescriptionUI.prodDesc1;
            desc2 = productDescriptionUI.prodDesc2;
            desc3 = productDescriptionUI.prodDesc3;
            desc4 = productDescriptionUI.prodDesc4;
            desc5 = productDescriptionUI.prodDesc5;
            desc6 = productDescriptionUI.prodDesc6;
            desc7 = productDescriptionUI.prodDesc7;
            desc8 = productDescriptionUI.prodDesc8;
            desc9 = productDescriptionUI.prodDesc9;
            desc10 = productDescriptionUI.prodDesc10;
            descCount = productDescriptionUI.descCount;
        }
        else {
            desc1 = product.getDescriptionOne();
            desc2 = product.getDescriptionTwo();
            desc3 = product.getDescriptionThree();
            desc4 = product.getDescriptionFour();
            desc5 = product.getDescriptionFive();
            desc6 = product.getDescriptionSix();
            desc7 = product.getDescriptionSeven();
            desc8 = product.getDescriptionEight();
            desc9 = product.getDescriptionNine();
            desc10 = product.getDescriptionTen();
            descCount = product.getDescriptioncount();
        }
        float Profit = ((UQC1qty * productRateForUQC1) + (UQC2qty * UQC2rate)) - (((UQC1qty * nUQC2Value) + UQC2qty) * UQC2Baserate);
        String strTotalAmountInclTax = currency.format(Amount + IGST_amt + CGST_amt + SGST_amt);
        Float TotalAmoutInclTax = Float.parseFloat(strTotalAmountInclTax);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        bCheckStock = false;
        setStock(product.getName());
        String strGodown = "";
        boolean bService = product.isService();
        if (!bService && isInventoryEnabled) {
            strGodown = comboGodown.getSelectedItem().toString();
        }
        isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
        if (isInventoryEnabled && !isUnderStockPossible) {
            if (wholeQuantity <= productStock) {
                bCheckStock = true;
            }
        }
        else {
            bCheckStock = true;
        }
        Amount = Amount + lineItemAmt;

        IGST_amt = IGST_amt + lineItemIGST;
        SGST_amt = SGST_amt + lineItemSGST;
        CGST_amt = CGST_amt + lineItemCGST;
        Profit = Profit + lineItemProfit;
        fCessAmt = fCessAmt + lineItemfCessAmt;
        TotalAmoutInclTax = TotalAmoutInclTax + lineItemTotalAmount;
        int pos = 0;
        if (!bService && (bCheckStock && isError)) {
            for (int ii = 0; ii < model.getRowCount(); ii++) {
                if ((Boolean) model.getValueAt(ii, 27)) {
                    pos = ii;
                    break;
                }
                else {
                    pos++;
                    count++;
                }
            }

            Object[] row = {count, strProduct, product.getHsnCode(), product.getUQC1(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                product.getUQC2(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                wholeQuantity, Float.parseFloat("0"), Float.parseFloat("0"), Amount, product.getIgstPercentage(), IGST_amt, product.getCgstPercentage(),
                CGST_amt, product.getSgstPercentage(), SGST_amt,
                UQC1Baserate, UQC2Baserate, Profit, fCessAmt, Amount, false, strGodown, desc1, desc2, desc3, desc4, desc5, desc6,
                desc7, desc8, desc9, desc10, descCount, TotalAmoutInclTax, UQC1RateWithTax, UQC2RateWithTax};
            if (productUpdate) {
                model.setValueAt(product.getName(), rowId, 1);
                model.setValueAt(product.getHsnCode(), rowId, 2);
                model.setValueAt(product.getUQC1(), rowId, 3);
                model.setValueAt(UQC1qty, rowId, 4);
                model.setValueAt(UQC1rate, rowId, 5);
                model.setValueAt(UQC1qty * UQC1rate, rowId, 6);
                model.setValueAt(product.getUQC2(), rowId, 7);
                model.setValueAt(UQC2qty, rowId, 8);
                model.setValueAt(UQC2rate, rowId, 9);
                model.setValueAt(UQC2qty * UQC2rate, rowId, 10);
                model.setValueAt(strQuantity, rowId, 11);
                model.setValueAt(wholeQuantity, rowId, 12);
                model.setValueAt(Float.parseFloat("0"), rowId, 13);
                model.setValueAt(Float.parseFloat("0"), rowId, 14);
                model.setValueAt(Amount, rowId, 15);
                model.setValueAt(product.getIgstPercentage(), rowId, 16);
                model.setValueAt(IGST_amt, rowId, 17);
                model.setValueAt(product.getCgstPercentage(), rowId, 18);
                model.setValueAt(CGST_amt, rowId, 19);
                model.setValueAt(product.getSgstPercentage(), rowId, 20);
                model.setValueAt(SGST_amt, rowId, 21);
                model.setValueAt(UQC1Baserate, rowId, 22);
                model.setValueAt(UQC2Baserate, rowId, 23);
                model.setValueAt(Profit, rowId, 24);
                model.setValueAt(fCessAmt, rowId, 25);
                model.setValueAt(Amount, rowId, 26);
                model.setValueAt(strGodown, rowId, 28);
                model.setValueAt(desc1, rowId, 29);
                model.setValueAt(desc2, rowId, 30);
                model.setValueAt(desc3, rowId, 31);
                model.setValueAt(desc4, rowId, 32);
                model.setValueAt(desc5, rowId, 33);
                model.setValueAt(desc6, rowId, 34);
                model.setValueAt(desc7, rowId, 35);
                model.setValueAt(desc8, rowId, 36);
                model.setValueAt(desc9, rowId, 37);
                model.setValueAt(desc10, rowId, 38);
                model.setValueAt(descCount, rowId, 39);
                model.setValueAt(TotalAmoutInclTax, rowId, 40);
                model.setValueAt(UQC1RateWithTax, rowId, 41);
                model.setValueAt(UQC2RateWithTax, rowId, 42);

            }
            else {
                model.insertRow(pos, row);
            }

            jTableRender();
            btnProductDescription.setEnabled(false);
            TotalCalculate();
            labelStock.setText("N/A");
        }
        else if (bService) {
            for (int ii = 0; ii < model.getRowCount(); ii++) {
                if ((Boolean) model.getValueAt(ii, 27)) {
                    pos = ii;
                    break;
                }
                else {
                    pos++;
                    count++;
                }
            }
            Object[] row = {count, strProduct, product.getHsnCode(), "", UQC1qty, UQC1rate, UQC1rate,
                "", 0, 0, 0, "",
                0, Float.parseFloat("0"), Float.parseFloat("0"), Amount, product.getIgstPercentage(), IGST_amt, product.getCgstPercentage(),
                CGST_amt, product.getSgstPercentage(), SGST_amt,
                UQC1Baserate, UQC2Baserate, Profit, fCessAmt, Amount, true, "", desc1, desc2, desc3, desc4, desc5, desc6,
                desc7, desc8, desc9, desc10, descCount, TotalAmoutInclTax, UQC1RateWithTax, 0};
            model.insertRow(pos, row);
            jTableRender();

            btnProductDescription.setEnabled(false);
            TotalCalculate();
            labelStock.setText("N/A");
            comboProduct.requestFocus();
        }
        else if (bMsg) {
            JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
            labelStock.setText("N/A");
            comboProduct.requestFocus();
        }

    }

    public void TotalCalculate() {
        float fTotal = 0.00f;
        DefaultTableModel tModel = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < tModel.getRowCount(); i++) {
            String strBillCharge = (String) tModel.getValueAt(i, 1);
            if (strBillCharge.equalsIgnoreCase("Deliver Charge") || strBillCharge.equalsIgnoreCase("Cutting Charge")) {
                fTotal = fTotal + ((float) tModel.getValueAt(i, 15));
            }
            else {
                fTotal = fTotal + ((float) tModel.getValueAt(i, 40));
            }
            if (tModel.getValueAt(i, 25) != null) {
                if (isOldBill) {
                    fTotalCess = fTotalCess + (new BigDecimal("0" + tModel.getValueAt(i, 25))).floatValue();
                }
                else {
                    fTotalCess = fTotalCess + ((float) tModel.getValueAt(i, 25));
                }
            }
            amountWithoutDiscount = fTotal;
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strDiscount.equals("")) {
                strDiscount = "0";
            }
            float DiscountAmt = Float.parseFloat(strDiscount);
            amount = amountWithoutDiscount - DiscountAmt;
            txtTotalBillAmount.setText(currency.format(amount));
        }
        for (int sno = 1; sno <= tModel.getRowCount(); sno++) {
            tModel.setValueAt(sno, sno - 1, 0);
        }
    }

    public int isProductHereInTable(String productName, int donotCheckRow) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int bProductAlreadyAdded = -1;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (donotCheckRow != i) {
                if (model.getValueAt(i, 1).toString().equals(productName)) {
                    bProductAlreadyAdded = i;
                }
            }
        }
        return bProductAlreadyAdded;
    }

    public boolean validateFields() {
        boolean bValidate = false;
        if (dateBill.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please Select Date");
            dateBill.requestFocusInWindow();
            return bValidate;
        }
        else if (txtPartyName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Company Name");
            txtPartyName.requestFocus();
            return bValidate;
        }
        else if (comboState.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the State");
            comboState.requestFocus();
            return bValidate;
        }
        return true;
    }

    public void getPartyDetails() {
        String strAddr = "";
        String name = (String) comboParty.getSelectedItem();
        if (name.equals("")) {
            txtPartyName.setText("");
            txtAddrLine.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            comboState.setSelectedItem("");
            txtGSTIN.setText("");
        }
        else {
            if (name.equalsIgnoreCase("No Party")) {
                dateBill.requestFocusInWindow();
                txtPartyName.setText("");
                txtAddrLine.setText("");
                txtCity.setText("");
                txtDistrict.setText("");
                comboState.setSelectedItem("Tamil Nadu");
                txtGSTIN.setText("");
            }
            else if (name.equalsIgnoreCase("New Party")) {
                dateBill.requestFocusInWindow();
                txtPartyName.setText("");
                txtAddrLine.setText("");
                txtCity.setText("");
                txtDistrict.setText("");
                comboState.setSelectedItem("Tamil Nadu");
                txtGSTIN.setText("");
            }
            else {
                for (Ledger ledger : sortedLedgers) {
                    if (ledger.getLedgerName().equals(name)) {
                        ledgerData = ledger;
                        LedgerId = ledger.getLedgerId();
                        txtPartyName.setText(ledger.getLedgerName());
                        String address = ledger.getAddress();
                        String[] words = address.split("/~/");
                        for (int ii = 0; ii < words.length; ii++) {
                            if (ii == 0) {
                                strAddr = words[ii];
                            }
                            else if (!words[ii].equals(",") && !words[ii].trim().equals("")) {
                                strAddr = strAddr + ", " + words[ii];
                            }
                        }
                        txtAddrLine.setText(strAddr);
                        txtCity.setText(ledger.getCity());
                        txtDistrict.setText(ledger.getDistrict());
                        comboState.setSelectedItem(ledger.getState());
                        txtGSTIN.setText(ledger.getGSTIN());
                        dateBill.requestFocusInWindow();
                        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                        String selectedState = ledger.getState();
                        isIGST = !(selectedState.equals(CompanyState));
                        setTable();
                        if (model.getRowCount() > 0) {
                            model.setRowCount(0);
                        }
                        setTable();
                    }
                }
            }
        }
    }

    public void setParty() {
        comboParty.addItem("No Party");
        comboParty.addItem("New Party");
        for (Ledger ledger : sortedLedgers) {
            comboParty.addItem(ledger.getLedgerName());
        }
    }

    private void printData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveAndPrint(true);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnPrint.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnPrint.getActionMap();
        actionMap.put("Action", actionListener);
        btnPrint.setActionMap(actionMap);
    }

    private void saveData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveAndPrint(false);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnSave.getActionMap();
        actionMap.put("Action", actionListener);
        btnSave.setActionMap(actionMap);
    }

    public Boolean checkPartyName(String name) {
        Boolean isFound = false;
        for (Ledger ledger : ledgers) {
            if (ledger.getLedgerName().equals(name)) {
                isFound = true;
            }
        }
        return isFound;
    }

    public Boolean addNewPartyDetails() {
        String Address = "";
        Boolean isSuccess = false;
        FormValidation formValidate = new FormValidation();
        String strPartyName = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isAlreadyHere = checkPartyName(strPartyName);
        String strGSTIN = txtGSTIN.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        int errorReport = formValidate.checkGSTIN(txtGSTIN.getText());
//        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate());
        if (strPartyName.equals("")) {
            JOptionPane.showMessageDialog(null, "please enter the Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        }
        else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strPartyName + " is already here.", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        }
        else if (errorReport != 0) {
            switch (errorReport) {
                case 1:
                    JOptionPane.showMessageDialog(null, "First two digits must be a State Code", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null, "One Digit Prior to the last must be Z", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                case 3:
                    JOptionPane.showMessageDialog(null, "Invalid GSTIN Number", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                default:
                    break;
            }
        }
        else {
            String addr1 = txtAddrLine.getText();
            if (addr1.equals("")) {
                addr1 = " ";
            }
            else if (addr1.contains(",")) {
                String[] words = addr1.split(",");
                for (int ii = 0; ii < words.length; ii++) {
                    if (ii == 0) {
                        Address = words[ii];
                    }
                    else if (!words[ii].equals(" ")) {
                        Address = Address + "/~/" + words[ii];
                    }
                }
            }
//             else {
//                String[] words = addr1.split(" ");
//                for (int ii = 0; ii < words.length; ii++) {
//                    if (ii == 0) {
//                        Address = words[ii];
//                    } else if (!words[ii].equals(" ")) {
//                        Address = Address + "/~/" + words[ii];
//                    }
//                }
//            }
            Ledger newLedger = new Ledger();

            newLedger.setLedgerName(strPartyName);
            newLedger.setAddress(Address);
            newLedger.setCity(txtCity.getText());
            newLedger.setDistrict(txtDistrict.getText());
            newLedger.setState((String) comboState.getSelectedItem());
            newLedger.setEmail("");
            newLedger.setPhone("");
            newLedger.setMobile("");
            newLedger.setGSTIN(txtGSTIN.getText());
            newLedger.setOpeningBalanceDate(new Date());
            newLedger.setOpeningBalance(new BigDecimal(0));

            isSuccess = new LedgerLogic().createPartyLedger(companyId, newLedger, false);

        }
        return isSuccess;
    }

    public Ledger findLedgerData() {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : sortedLedgers) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                LedgerId = ledger.getLedgerId();
                break;
            }
        }
        return ledgerData;
    }

    public void setGodown() {
        int nLen = godowns.size();
        for (GoDown godown : godowns) {
            comboGodown.addItem(godown.getGoDownName());
        }
        if (nLen > 1) {
            lblGodown.setText("Godown");
            comboGodown.setVisible(true);
        }
        else if (nLen == 1) {
            lblGodown.setText("");
            comboGodown.setSelectedItem(godowns.get(0).getGoDownName());
            comboGodown.setVisible(false);
        }
    }

    public void setStock(String productName) {
        Product selProduct = null;
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                selProduct = product;
                break;
            }
        }

        if (!selProduct.isService() && isInventoryEnabled) {
            GoDown selGodown = godowns.get(0);
            for (GoDown godown : godowns) {
                if (comboGodown.getSelectedItem().toString().equals(godown.getGoDownName())) {
                    selGodown = godown;
                    break;
                }
            }
            stock = new GoDownStockCRUD().getGoDownStockDetail(selProduct.getProductId(), selGodown.getGoDownId());
            productStock = stock.getCurrentStock();
            if (isOldBill) {
                List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
                for (PurchaseSaleLineItem item : psLIs) {
                    if (item.getProductName().equals(productName)) {
                        productStock = productStock + item.getStockQuantity();
                    }
                }
            }
            labelStock.setText(setStockConverstion(productStock, selProduct.getUQC1(), selProduct.getUQC2(), selProduct.getUQC2Value()));
        }
        else {
            labelStock.setText("N/A");
        }
    }

    public void setPartyEditable(boolean isEditable) {
        txtPartyName.setEnabled(isEditable);
        dateBill.setEnabled(isEditable);
        comboParty.setEnabled(isEditable);
        txtAddrLine.setEnabled(isEditable);
        txtCity.setEnabled(isEditable);
        txtDistrict.setEnabled(isEditable);
        comboState.setEnabled(isEditable);
        txtGSTIN.setEnabled(isEditable);
        comboPricingPolicy.setEnabled(isEditable);
    }

    public void setNewProduct() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboProduct.setModel(model);
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    public void EditOldBill(String id) {
        String strAddr = "";
        isOldBill = true;
        setPartyEditable(false);
        oldSale = new PurchaseSaleLogic().getPSDetail(id);
        dateBill.setDate(oldSale.getPsDate());
        List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
        List<PurchaseSaleTaxSummary> psTSs = oldSale.getPsTaxSummaries();
        comboPricingPolicy.setSelectedItem(oldSale.getPricingPolicyName());
//        oldSale.printCollections();
        if (oldSale.isNoParty() != null && oldSale.isNoParty()) {
            LedgerId = null;
            txtPartyName.setText(oldSale.getNoPartyName());
            txtAddrLine.setText(oldSale.getNoPartyLineOne());
            txtCity.setText(oldSale.getNoPartyCity());
            txtDistrict.setText(oldSale.getNoPartyDistrict());
            comboState.setSelectedItem(oldSale.getNoPartyState());
            txtGSTIN.setText(oldSale.getNoPartyState());
        }
        else {
            LedgerId = oldSale.getLedger().getLedgerId();
            comboParty.setSelectedItem(oldSale.getLedger().getLedgerName());
            txtPartyName.setText(oldSale.getLedger().getLedgerName());
            String address = oldSale.getLedger().getAddress();
            String[] words = address.split("/~/");
            for (int ii = 0; ii < words.length; ii++) {
                if (ii == 0) {
                    strAddr = words[ii];
                }
                else if (!words[ii].equals(" ")) {
                    strAddr = strAddr + ", " + words[ii];
                }
            }
            txtAddrLine.setText(strAddr);
            txtCity.setText(oldSale.getLedger().getCity());
            txtDistrict.setText(oldSale.getLedger().getDistrict());
            comboState.setSelectedItem(oldSale.getLedger().getState());
            txtGSTIN.setText(oldSale.getLedger().getGSTIN());

        }
        txtRemarks.setText(oldSale.getRemarks());
        isIGST = oldSale.isIsIGST();

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        int nCount = 0;
        for (PurchaseSaleLineItem item : psLIs) {
            if (!item.getProduct().isService() && isInventoryEnabled) {
                BigDecimal cessAmt = item.getCessAmount();
                if (cessAmt == null) {
                    cessAmt = new BigDecimal(0);
                }
                Object[] row = {item.getLineNumber(),
                    item.getProductName(),
                    item.getHsnSac(),
                    item.getUqcOne(),
                    item.getUqcOneQuantity(),
                    item.getUqcOneRate(),
                    (new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate())).floatValue(),
                    item.getUqcTwo(),
                    item.getUqcTwoQuantity(),
                    item.getUqcTwoRate(),
                    (new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate())).floatValue(),
                    item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                    item.getStockQuantity(),
                    item.getDiscountPercent(),
                    item.getDiscountValue().floatValue(),
                    item.getValue().floatValue(),
                    item.getIgstPercentage(), item.getIgstValue().floatValue(),
                    item.getCgstPercentage(), item.getCgstValue().floatValue(),
                    item.getSgstPercentage(), item.getSgstValue().floatValue(),
                    item.getUqc1BaseRate().floatValue(), item.getUqc2BaseRate().floatValue(), item.getProfit().floatValue(),
                    cessAmt.floatValue(),
                    item.getValue().floatValue(),
                    false,
                    item.getGodownStockEntry().getGodown().getGoDownName(),
                    item.getDescriptionOne(), item.getDescriptionTwo(), item.getDescriptionThree(), item.getDescriptionFour(),
                    item.getDescriptionFive(), item.getDescriptionSix(), item.getDescriptionSeven(), item.getDescriptionEight(),
                    item.getDescriptionNine(), item.getDescriptionTen(), item.getDescriptionCount(),
                    item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue()))).floatValue(),
                    item.getUqcOneRateWithTax(), item.getUqcTwoRateWithTax()
                };
                model.addRow(row);
                nCount++;
            }
            else {
                Object[] row = {item.getLineNumber(),
                    item.getProductName(),
                    item.getHsnSac(),
                    item.getUqcOne(),
                    item.getUqcOneQuantity(),
                    item.getUqcOneRate(),
                    (new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate())).floatValue(),
                    item.getUqcTwo(),
                    item.getUqcTwoQuantity(),
                    item.getUqcTwoRate(),
                    (new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate())).floatValue(),
                    item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                    item.getStockQuantity(),
                    item.getDiscountPercent(),
                    item.getDiscountValue().floatValue(),
                    item.getValue().floatValue(),
                    item.getIgstPercentage(), item.getIgstValue().floatValue(),
                    item.getCgstPercentage(), item.getCgstValue().floatValue(),
                    item.getSgstPercentage(), item.getSgstValue().floatValue(),
                    item.getUqc1BaseRate().floatValue(), item.getUqc2BaseRate().floatValue(), item.getProfit().floatValue(),
                    item.getCessAmount(),
                    item.getValue().floatValue(),
                    false,
                    "",
                    item.getDescriptionOne(), item.getDescriptionTwo(), item.getDescriptionThree(), item.getDescriptionFour(),
                    item.getDescriptionFive(), item.getDescriptionSix(), item.getDescriptionSeven(), item.getDescriptionEight(),
                    item.getDescriptionNine(), item.getDescriptionTen(), item.getDescriptionCount(),
                    item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue()))).floatValue(),
                    item.getUqcOneRateWithTax(), item.getUqcTwoRateWithTax()
                };
                model.addRow(row);
                nCount++;
            }

            TotalCalculate();
            jTableRender();
            comboProduct.requestFocus();
            clear(false);
            setTable();
        }
    }

    public String setStockConverstion(long qty, String UQC1, String UQC2, int UQC2Value) {
        long UQC1Count = qty / UQC2Value;
        long UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void getProductDetails(String productName) {
        bUQCSame = false;
        descAdded = false;

        if ((productName != null) && (!(productName.equals("")))) {
            for (Product selProduct : products) {
                if (selProduct.getName().equals(productName)) {
                    btnProductDescription.setEnabled(true);
                    productDetail = selProduct;
                    isServiceProduct = selProduct.isService();

                    if (isServiceProduct) {
                        bUQCSame = true;
                        labelUQC1.setText("");
                        labelUQC2.setText("");
                        txtUQC1Qty.setText("");
                        txtUQC1Qty.setEnabled(false);
                        txtUQC2Qty.setText("");
                        txtUQC2Qty.setEnabled(false);
                        labelUQC1forPrice.setText("");
                        labelUQC2forPrice.setText("");
                        btnProductDescription.setEnabled(true);
                        labelUQC1forPrice.setText("");
                        labelUQC2forPrice.setText("");
                        txtUQC1RateWithTax.setText("");
                        txtUQC2RateWithTax.setText("");
                        txtUQC2RateWithTax.setEnabled(false);

                        txtUQC2Rate.setText("");
                        txtUQC1Rate.setText("");
                        txtUQC2Rate.setEnabled(false);

                        setServiceProductDetails(productName);
                    }
                    else {
                        if (productDetail.getUQC1().equalsIgnoreCase(productDetail.getUQC2())) {
                            bUQCSame = true;
                            labelUQC2.setEnabled(false);
                            labelUQC2forPrice.setEnabled(false);
                            txtUQC1Qty.setEnabled(true);
                            txtUQC2Qty.setEnabled(false);
                            txtUQC2RateWithTax.setEnabled(false);
                            labelUQC2PriceWithTax.setEnabled(false);
                            txtUQC2Rate.setEnabled(false);
                        }
                        else {
                            bUQCSame = false;
                            labelUQC2.setEnabled(true);
                            labelUQC2forPrice.setEnabled(true);
                            txtUQC1Qty.setEnabled(true);
                            txtUQC2Qty.setEnabled(true);
                            txtUQC2RateWithTax.setEnabled(true);
                            labelUQC2PriceWithTax.setEnabled(true);
                            txtUQC2Rate.setEnabled(true);
                        }
                        setProductDetails(productName);
                    }
                    break;
                }
            }

        }
        else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtUQC1RateWithTax.setText("");
            txtUQC2RateWithTax.setText("");
            txtAmount.setText("");
            txtIGSTper.setText("");
            txtCGSTper.setText("");
            txtSGSTper.setText("");
            txtIGSTamt.setText("");
            txtCGSTamt.setText("");
            txtSGSTamt.setText("");

            labelUQC1forPrice.setText("");
            labelUQC2forPrice.setText("");
            txtUQC2Rate.setText("");
            txtUQC1Rate.setText("");

            labelStock.setText("N/A");
            comboProduct.setSelectedItem("");
            comboProduct.requestFocus();
        }
    }

    private void setProductDetails(String productName) {
        labelUQC1.setText(productDetail.getUQC1());
        labelUQC1forPrice.setText(productDetail.getUQC1());
        labelUQC1PriceWithTax.setText(productDetail.getUQC1());
        labelUQC2.setText(productDetail.getUQC2());
        labelUQC2forPrice.setText(productDetail.getUQC2());
        labelUQC2PriceWithTax.setText(productDetail.getUQC2());
        HSNCode = productDetail.getHsnCode();
        UQC2Value = productDetail.getUQC2Value();
        if (productDetail.getBaseProductRate() != null) {
            BaseProductRate = productDetail.getBaseProductRate().floatValue();
        }
        else {
            BaseProductRate = 0;
        }

        IGSTper = productDetail.getIgstPercentage();
        CGSTper = productDetail.getCgstPercentage();
        SGSTper = productDetail.getSgstPercentage();
        txtIGSTper.setText(String.valueOf(productDetail.getIgstPercentage()));
        txtCGSTper.setText(String.valueOf(productDetail.getCgstPercentage()));
        txtSGSTper.setText(String.valueOf(productDetail.getSgstPercentage()));
        isBasePriceProductTaxInclusive = productDetail.isBasePriceInclusiveOfGST();

        float basePrice = 0;
        if ((isBasePriceProductTaxInclusive) && (BaseProductRate > 0)) {
            basePrice = (BaseProductRate / (100 + IGSTper)) * 100;
        }
        else {
            basePrice = BaseProductRate;
        }
        //                    txtUQC1BaseRate.setText(currency.format(basePrice * UQC2Value));
        //                    txtUQC2BaseRate.setText(currency.format(basePrice));

        ProductRate = productDetail.getProductRate().floatValue();
        isProductTaxInclusive = productDetail.isInclusiveOfGST();

        String nameOfPolicy = (String) comboPricingPolicy.getSelectedItem();
        if (nameOfPolicy.equals("Default")) {
            ProductRate = productDetail.getProductRate().floatValue();
            isProductTaxInclusive = productDetail.isInclusiveOfGST();
            float price = 0, priceWithTax = 0;
            if (isProductTaxInclusive) {
                price = (ProductRate / (100 + IGSTper)) * 100;
                priceWithTax = ProductRate;
            }
            else {
                price = ProductRate;
                priceWithTax = price + ((IGSTper / 100) * price);
            }
            txtUQC1Rate.setText(currency.format(price * UQC2Value));
            txtUQC2Rate.setText(currency.format(price));
            txtUQC1RateWithTax.setText(currency.format(priceWithTax * UQC2Value));
            txtUQC2RateWithTax.setText(currency.format(priceWithTax));
        }
        else {
            Boolean isNotHere = true;
            for (SingleProductPolicy productPolicy : recentSingleProductPolicies) {
                if (productPolicy.getProduct().getName().equals(productName)) {
                    float uqc1Rate = productPolicy.getUqc1Rate().floatValue();
                    isProductTaxInclusive = productPolicy.isInclusiveOfGST();
                    float Uqc1price = 0, Uqc1PriceWithTax = 0;
                    if (isProductTaxInclusive) {
                        Uqc1price = (uqc1Rate / (100 + IGSTper)) * 100;
                        Uqc1PriceWithTax = uqc1Rate;
                    }
                    else {
                        Uqc1price = uqc1Rate;
                        Uqc1PriceWithTax = uqc1Rate + ((uqc1Rate * IGSTper) / 100);
                    }
                    txtUQC1Rate.setText(currency.format(Uqc1price));
                    txtUQC2Rate.setText(currency.format(Uqc1price / UQC2Value));
                    txtUQC1RateWithTax.setText(currency.format(Uqc1PriceWithTax));
                    txtUQC2RateWithTax.setText(currency.format(Uqc1PriceWithTax / UQC2Value));
                    isNotHere = false;
                }
            }
            if (isNotHere) {
                JOptionPane.showMessageDialog(null, productName + " Product is not available in " + nameOfPolicy, "Alert", JOptionPane.WARNING_MESSAGE);

                ProductRate = productDetail.getProductRate().floatValue();
                isProductTaxInclusive = productDetail.isInclusiveOfGST();
                float price = 0, priceWithTax = 0;
                if (isProductTaxInclusive) {
                    price = (ProductRate / (100 + IGSTper)) * 100;
                    priceWithTax = ProductRate;
                }
                else {
                    price = ProductRate;
                    priceWithTax = price + ((IGSTper / 100) * price);
                }
                txtUQC1Rate.setText(currency.format(price * UQC2Value));
                txtUQC2Rate.setText(currency.format(price));
                txtUQC1RateWithTax.setText(currency.format(priceWithTax * UQC2Value));
                txtUQC2RateWithTax.setText(currency.format(priceWithTax));

            }
        }
        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);
        setStock(comboProduct.getEditor().getItem().toString());
        txtUQC1Qty.requestFocus();
    }

    private void setServiceProductDetails(String productName) {

        HSNCode = productDetail.getHsnCode();
        UQC2Value = productDetail.getUQC2Value();
        if (productDetail.getBaseProductRate() != null) {
            BaseProductRate = productDetail.getBaseProductRate().floatValue();
        }
        else {
            BaseProductRate = 0;
        }

        IGSTper = productDetail.getIgstPercentage();
        CGSTper = productDetail.getCgstPercentage();
        SGSTper = productDetail.getSgstPercentage();
        txtIGSTper.setText(String.valueOf(productDetail.getIgstPercentage()));
        txtCGSTper.setText(String.valueOf(productDetail.getCgstPercentage()));
        txtSGSTper.setText(String.valueOf(productDetail.getSgstPercentage()));
        isBasePriceProductTaxInclusive = productDetail.isBasePriceInclusiveOfGST();

        float basePrice = 0;
        if ((isBasePriceProductTaxInclusive) && (BaseProductRate > 0)) {
            basePrice = (BaseProductRate / (100 + IGSTper)) * 100;
        }
        else {
            basePrice = BaseProductRate;
        }

        ///////////Price Policy set//////////////////////////////////
        String nameOfPolicy = (String) comboPricingPolicy.getSelectedItem();
        if (nameOfPolicy.equals("Default")) {
            ProductRate = productDetail.getProductRate().floatValue();
            isProductTaxInclusive = productDetail.isInclusiveOfGST();
            float price = 0, priceWithTax = 0;
            if (isProductTaxInclusive) {
                price = (ProductRate / (100 + IGSTper)) * 100;
                priceWithTax = ProductRate;
            }
            else {
                price = ProductRate;
                priceWithTax = price + ((IGSTper / 100) * price);
            }
            txtUQC1Rate.setText(currency.format(price));
            txtUQC1RateWithTax.setText(currency.format(priceWithTax));
        }
        else {
            Boolean isNotHere = true;
            for (SingleProductPolicy productPolicy : recentSingleProductPolicies) {
                if (productPolicy.getProduct().getName().equals(productName)) {
                    float uqc1Rate = productPolicy.getUqc1Rate().floatValue();
                    isProductTaxInclusive = productPolicy.isInclusiveOfGST();
                    float Uqc1price = 0, Uqc1PriceWithTax = 0;
                    if (isProductTaxInclusive) {
                        Uqc1price = (uqc1Rate / (100 + IGSTper)) * 100;
                        Uqc1PriceWithTax = uqc1Rate;
                    }
                    else {
                        Uqc1price = uqc1Rate;
                        Uqc1PriceWithTax = uqc1Rate + ((uqc1Rate * IGSTper) / 100);
                    }
                    txtUQC1Rate.setText(currency.format(Uqc1price));
                    txtUQC1RateWithTax.setText(currency.format(Uqc1PriceWithTax));
                    isNotHere = false;
                }
            }
            if (isNotHere) {
                JOptionPane.showMessageDialog(null, productName + " Service is not available in " + nameOfPolicy, "Alert", JOptionPane.WARNING_MESSAGE);

                ProductRate = productDetail.getProductRate().floatValue();
                isProductTaxInclusive = productDetail.isInclusiveOfGST();
                float price = 0, priceWithTax = 0;
                if (isProductTaxInclusive) {
                    price = (ProductRate / (100 + IGSTper)) * 100;
                    priceWithTax = ProductRate;
                }
                else {
                    price = ProductRate;
                    priceWithTax = price + ((IGSTper / 100) * price);
                }
                txtUQC1Rate.setText(currency.format(price));
                txtUQC1RateWithTax.setText(currency.format(priceWithTax));

            }
        }

        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);

        labelStock.setText("");
        if (isProductTaxInclusive) {
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            txtUQC1Rate.requestFocus();
        }
    }

    private void setProductTaxIncluviceHideAndShow(Boolean isTaxInclusive) {
        txtUQC1RateWithTax.setEnabled(isTaxInclusive);
        txtUQC2RateWithTax.setEnabled(isTaxInclusive && !bUQCSame);
        txtUQC1Rate.setEnabled(!isTaxInclusive);
        txtUQC2Rate.setEnabled(!isTaxInclusive && !bUQCSame);
    }

    public void TaxvalueCalculationForProduct(boolean isDisAmt) {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("") || !strUQC2rate.equals(""))) && ((!strUQC1qty.equals("") || !strUQC2qty.equals("")))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt("0" + strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt("0" + strUQC2qty);
            }
            if (!strUQC1qty.equals("")) {
                UQC1rate = Float.parseFloat("0" + strUQC1rate);
            }
            if (!strUQC2qty.equals("")) {
                UQC2rate = Float.parseFloat("0" + strUQC2rate);
            }

            if ((UQC2qty / UQC2Value) > 0) {
                UQC1qty += UQC2qty / UQC2Value;
                UQC2qty = UQC2qty % UQC2Value;
                txtUQC1Qty.setText(UQC1qty + "");
                txtUQC2Qty.setText(UQC2qty + "");
            }
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!isDisAmt) {
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    DiscountPer = Float.parseFloat(strDiscountPer);
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                }
                else {
                    txtDiscount.setText("");
                }
            }
            else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (TotalAmount > 0) {
                    if (!strDiscount.equals("")) {
                        Discount = Float.parseFloat(strDiscount);
                    }
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                }
                else {
                    txtDiscountPer.setText("0");
                }
            }
            String productName = comboProduct.getSelectedItem().toString();
            if ((productName != null) && (!(productName.equals("")))) {
                for (Product product : products) {
                    if (product.getName().equals(productName)) {
                        IGSTper = product.getIgstPercentage();
                        float TaxValue = TotalAmount * (IGSTper / 100);
                        if (isIGST) {
                            txtIGSTamt.setText(currency.format(TaxValue));
                            txtCGSTamt.setText("0.00");
                            txtSGSTamt.setText("0.00");
                        }
                        else {
                            txtIGSTamt.setText("0.00");
                            txtCGSTamt.setText(currency.format(TaxValue / 2));
                            txtSGSTamt.setText(currency.format(TaxValue / 2));
                        }
                        lblProductsTotal.setText(currency.format(TotalAmount + TaxValue));
                        break;
                    }
                    else {
                        txtAmount.setText("0.00");
                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");

                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");
                    }
                }
            }
            txtAmount.setText(currency.format(TotalAmount));
        }
    }

    public void TaxvalueCalculationForService(boolean isDisAmt) {

        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("")))) {
            float UQC1rate = Float.parseFloat(strUQC1rate);
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = UQC1rate;
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!isDisAmt) {
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    DiscountPer = Float.parseFloat(strDiscountPer);
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                }
                else {
                    txtDiscount.setText("");
                }
            }
            else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (TotalAmount > 0) {
                    if (!strDiscount.equals("")) {
                        Discount = Float.parseFloat(strDiscount);
                    }
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                }
                else {
                    txtDiscountPer.setText("0");
                }
            }
            String productName = comboProduct.getSelectedItem().toString();
            if ((productName != null) && (!(productName.equals("")))) {
                for (Product product : products) {
                    if (product.getName().equals(productName)) {
                        IGSTper = product.getIgstPercentage();
                        float TaxValue = TotalAmount * (IGSTper / 100);
                        if (isIGST) {
                            txtIGSTamt.setText(currency.format(TaxValue));
                            txtCGSTamt.setText("0.00");
                            txtSGSTamt.setText("0.00");
                        }
                        else {
                            txtIGSTamt.setText("0.00");
                            txtCGSTamt.setText(currency.format(TaxValue / 2));
                            txtSGSTamt.setText(currency.format(TaxValue / 2));
                        }
                        lblProductsTotal.setText(currency.format(TotalAmount + TaxValue));
                        break;
                    }
                    else {
                        txtAmount.setText("0.00");
                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");

                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");
                    }
                }
            }
            txtAmount.setText(currency.format(TotalAmount));
        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(14).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(17).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(19).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(21).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(40).setCellRenderer(new FloatValueTableCellRenderer());

    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SalesB2CUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SalesB2CUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SalesB2CUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SalesB2CUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //WebLookAndFeel.install ();
        com.alee.laf.WebLookAndFeel.install();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SalesB2CUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog RemarksDialog;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProductDescription;
    private javax.swing.JButton btnRemark;
    private javax.swing.JButton btnRemarks;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboGodown;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboPricingPolicy;
    private javax.swing.JComboBox<String> comboProduct;
    private javax.swing.JComboBox<String> comboState;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelBalance;
    private javax.swing.JLabel labelPaymentBillAmount;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1PriceWithTax;
    private javax.swing.JLabel labelUQC1forPrice;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2PriceWithTax;
    private javax.swing.JLabel labelUQC2forPrice;
    private javax.swing.JLabel lblGodown;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblPriceWithTax;
    private javax.swing.JLabel lblProductsTotal;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JPanel panelUQC;
    private javax.swing.JDialog paymentDialog;
    private javax.swing.JTextField txtAddrLine;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JTextField txtBillDiscount;
    private javax.swing.JLabel txtCGSTamt;
    private javax.swing.JLabel txtCGSTper;
    private javax.swing.JTextField txtCessAmt;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JLabel txtIGSTamt;
    private javax.swing.JLabel txtIGSTper;
    private javax.swing.JTextField txtPartyName;
    private javax.swing.JTextField txtPayment;
    private javax.swing.JTextArea txtRemarks;
    private javax.swing.JLabel txtSGSTamt;
    private javax.swing.JLabel txtSGSTper;
    private javax.swing.JLabel txtTotalBillAmount;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC1Rate;
    private javax.swing.JTextField txtUQC1RateWithTax;
    private javax.swing.JTextField txtUQC2Qty;
    private javax.swing.JTextField txtUQC2Rate;
    private javax.swing.JTextField txtUQC2RateWithTax;
    // End of variables declaration//GEN-END:variables
}
