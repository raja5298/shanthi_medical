/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CompanyLogic;
import india.abs.gstonepro.business.CompanyUserRoleLogic;
import india.abs.gstonepro.business.UserLogic;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Acer
 */
public class UserRoleUI extends javax.swing.JFrame {

    List<Company> companies = new CompanyLogic().fetchAllCompanies();
    AppUser selectedAppUser;
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isBOS = SystemPolicyUtil.getSystemPolicy().isBillOfSupply(),
            isPlatinum = SystemPolicyUtil.getSystemPolicy().isPlatinum();
    boolean isBalanceSheet = SystemPolicyUtil.getSystemPolicy().isBalanceSheet();

    public UserRoleUI(AppUser appuser) {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            if (!SessionDataUtil.getSelectedUser().isAdmin()) {
                menuBar.remove(menuSettings);
            }
            listCompany.requestFocus();
            selectedAppUser = appuser;
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            this.setExtendedState(this.MAXIMIZED_BOTH);
//            int size = companies.size();
//            if (size == 1) {
//                labelSelectedCompany.setText(companies.get(0).toString());
//                listCompany.setVisible(false);
//                setAction();
//            } else {
            setJListValue();
//            }
            txtUsername.setText(selectedAppUser.getUserName());
            txtUsername.setEditable(false);
            setDisabled();
        } catch (Exception ex) {
            Logger.getLogger(UserRoleUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setDisabled() {
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);
        jPanel4.setVisible(false);
        jPanel5.setVisible(false);
        jPanel6.setVisible(false);
        jPanel8.setVisible(false);
        jPanel9.setVisible(false);
        jPanel10.setVisible(false);
        jPanel11.setVisible(false);
        jPanel12.setVisible(false);
        panelBOS.setVisible(false);
        jPanel14.setVisible(false);
        SelectAll.setEnabled(false);
    }

    public void setEnabled() {
        if (!isAccountEnabled) {
            jPanel2.setVisible(true);
            jPanel3.setVisible(true);
            jPanel4.setVisible(true);
            jPanel5.setVisible(true);
            jPanel8.setVisible(true);
            jPanel9.setVisible(true);
            jPanel10.setVisible(true);
            jPanel11.setVisible(true);
            panelBOS.setVisible(true);
            jPanel14.setVisible(true);
            SelectAll.setEnabled(true);
            jPanel1.remove(jPanel12);
            jPanel1.remove(jPanel6);
        }
        if (!isInventoryEnabled) {
            jPanel2.setVisible(true);
            jPanel3.setVisible(true);
            jPanel4.setVisible(true);
            jPanel5.setVisible(true);
            jPanel6.setVisible(true);
            jPanel8.setVisible(true);
            jPanel9.setVisible(true);
            jPanel11.setVisible(true);
            jPanel12.setVisible(true);
            panelBOS.setVisible(true);
            jPanel14.setVisible(true);
            SelectAll.setEnabled(true);
            jPanel1.remove(jPanel10);
        }
        if (!isAccountEnabled && !isInventoryEnabled) {
            jPanel2.setVisible(true);
            jPanel3.setVisible(true);
            jPanel4.setVisible(true);
            jPanel5.setVisible(true);
            jPanel8.setVisible(true);
            jPanel9.setVisible(true);
            jPanel11.setVisible(true);
            jPanel14.setVisible(true);
            panelBOS.setVisible(true);
            SelectAll.setEnabled(true);
            jPanel1.remove(jPanel12);
            jPanel1.remove(jPanel6);
            jPanel1.remove(jPanel10);
        }
        if (isAccountEnabled && isInventoryEnabled) {
            jPanel2.setVisible(true);
            jPanel3.setVisible(true);
            jPanel4.setVisible(true);
            jPanel5.setVisible(true);
            jPanel6.setVisible(true);
            jPanel8.setVisible(true);
            jPanel9.setVisible(true);
            jPanel10.setVisible(true);
            jPanel11.setVisible(true);
            jPanel12.setVisible(true);
            panelBOS.setVisible(true);
            jPanel14.setVisible(true);
            SelectAll.setEnabled(true);
        }
//        jPanel2.setVisible(true);
//        jPanel3.setVisible(true);
//        jPanel4.setVisible(true);
//        jPanel5.setVisible(true);
//        jPanel6.setVisible(true);
//        jPanel8.setVisible(true);
//        jPanel9.setVisible(true);
//        jPanel10.setVisible(true);
//        jPanel11.setVisible(true);
//        jPanel12.setVisible(true);
//        SelectAll.setEnabled(true);
          AccCheckBalSheet.setVisible(isBalanceSheet);
    }

    private UserRoleUI() {

    }

    public void setJListValue() {

        DefaultListModel listModel = new DefaultListModel();

        Collections.sort(companies, new Comparator<Company>() {

            public int compare(Company o1, Company o2) {
                return o1.getCompanyName().compareTo(o2.getCompanyName());
            }
        });

        for (Company company : companies) {

            listModel.addElement(company.getCompanyName());
            // listModel.addElement("company");
        }
        listCompany.setModel(listModel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel22 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ProdCheckSelect = new javax.swing.JCheckBox();
        ProdCheckCreate = new javax.swing.JCheckBox();
        ProdCheckView = new javax.swing.JCheckBox();
        ProdCheckUpdate = new javax.swing.JCheckBox();
        ProdCheckDel = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        PPCheckSelect = new javax.swing.JCheckBox();
        PPCheckCreate = new javax.swing.JCheckBox();
        PPCheckView = new javax.swing.JCheckBox();
        PPCheckUpdate = new javax.swing.JCheckBox();
        PPCheckDel = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        CDCheckSelect = new javax.swing.JCheckBox();
        CDCheckCreate = new javax.swing.JCheckBox();
        CDCheckView = new javax.swing.JCheckBox();
        CDCheckEdit = new javax.swing.JCheckBox();
        CDCheckDel = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        PartyCheckSelect = new javax.swing.JCheckBox();
        PartyCheckCreate = new javax.swing.JCheckBox();
        PartyCheckView = new javax.swing.JCheckBox();
        PartyCheckUpdate = new javax.swing.JCheckBox();
        PartyCheckDel = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        AccCheckSelect = new javax.swing.JCheckBox();
        AccCheckBank = new javax.swing.JCheckBox();
        AccCheckLedgerGrpBal = new javax.swing.JCheckBox();
        AccCheckCash = new javax.swing.JCheckBox();
        AccCheckJournal = new javax.swing.JCheckBox();
        AccCheckTBal = new javax.swing.JCheckBox();
        AccCheckPaymnt = new javax.swing.JCheckBox();
        AccCheckDaybook = new javax.swing.JCheckBox();
        AccCheckBalSheet = new javax.swing.JCheckBox();
        jPanel8 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        PICheckSelect = new javax.swing.JCheckBox();
        PICheckCreate = new javax.swing.JCheckBox();
        PICheckView = new javax.swing.JCheckBox();
        PICheckEdit = new javax.swing.JCheckBox();
        PICheckDel = new javax.swing.JCheckBox();
        jPanel9 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        SICheckSelect = new javax.swing.JCheckBox();
        SICheckCreate = new javax.swing.JCheckBox();
        SICheckView = new javax.swing.JCheckBox();
        SICheckEdit = new javax.swing.JCheckBox();
        SICheckDel = new javax.swing.JCheckBox();
        jPanel10 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        InvCheckSelect = new javax.swing.JCheckBox();
        InvCheckAdd = new javax.swing.JCheckBox();
        InvCheckView = new javax.swing.JCheckBox();
        InvCheckDedu = new javax.swing.JCheckBox();
        InvCheckOpeningBal = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        listCompany = new javax.swing.JList<>();
        label = new javax.swing.JLabel();
        labelSelectedCompany = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        SelectAll = new javax.swing.JCheckBox();
        jPanel11 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        gstReports = new javax.swing.JCheckBox();
        jPanel12 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        LedgerCheckView = new javax.swing.JCheckBox();
        LedgerCheckCreate = new javax.swing.JCheckBox();
        LedgerCheckEdit = new javax.swing.JCheckBox();
        LedgerCheckDel = new javax.swing.JCheckBox();
        LedgerCheckSel = new javax.swing.JCheckBox();
        LedgerCheckOpeningBalance = new javax.swing.JCheckBox();
        panelBOS = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        BosCheckSelect = new javax.swing.JCheckBox();
        BosCheckCreate = new javax.swing.JCheckBox();
        BosCheckView = new javax.swing.JCheckBox();
        BosCheckEdit = new javax.swing.JCheckBox();
        BosCheckDel = new javax.swing.JCheckBox();
        jPanel14 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        EstimateCheckSelect = new javax.swing.JCheckBox();
        EstimateCheckCreate = new javax.swing.JCheckBox();
        EstimateCheckView = new javax.swing.JCheckBox();
        EstimateCheckEdit = new javax.swing.JCheckBox();
        EstimateCheckDel = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        Home = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuBackUP = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("User Role");

        jLabel22.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel1.setText("User");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setDoubleBuffered(false);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel2.setText("Product / Service / Product Group");

        ProdCheckSelect.setText(" All");
        ProdCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProdCheckSelectActionPerformed(evt);
            }
        });

        ProdCheckCreate.setText("Create");

        ProdCheckView.setText("View");

        ProdCheckUpdate.setText("Update");

        ProdCheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProdCheckUpdate)
                    .addComponent(ProdCheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProdCheckDel)
                    .addComponent(ProdCheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(ProdCheckSelect)
                .addGap(16, 16, 16))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ProdCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ProdCheckView)
                    .addComponent(ProdCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProdCheckDel)
                    .addComponent(ProdCheckUpdate))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel3.setText("Pricing Policy");

        PPCheckSelect.setText("All");
        PPCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PPCheckSelectActionPerformed(evt);
            }
        });

        PPCheckCreate.setText("Create");

        PPCheckView.setText("View");

        PPCheckUpdate.setText("Update");

        PPCheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PPCheckUpdate)
                    .addComponent(PPCheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PPCheckDel)
                    .addComponent(PPCheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PPCheckSelect)
                .addGap(15, 15, 15))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PPCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PPCheckView)
                    .addComponent(PPCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PPCheckDel)
                    .addComponent(PPCheckUpdate))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Credit/Debit Note");

        CDCheckSelect.setText("All");
        CDCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CDCheckSelectActionPerformed(evt);
            }
        });

        CDCheckCreate.setText("Create");

        CDCheckView.setText("Reports");

        CDCheckEdit.setText("Edit");

        CDCheckDel.setText("Delete");
        CDCheckDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CDCheckDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CDCheckSelect)
                .addGap(17, 17, 17))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CDCheckEdit)
                    .addComponent(CDCheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CDCheckDel)
                    .addComponent(CDCheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CDCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CDCheckView)
                    .addComponent(CDCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CDCheckDel)
                    .addComponent(CDCheckEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setText("Party");

        PartyCheckSelect.setText(" All");
        PartyCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PartyCheckSelectActionPerformed(evt);
            }
        });

        PartyCheckCreate.setText("Create");

        PartyCheckView.setText("View");

        PartyCheckUpdate.setText("Update");

        PartyCheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PartyCheckUpdate)
                    .addComponent(PartyCheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PartyCheckDel)
                    .addComponent(PartyCheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PartyCheckSelect)
                .addGap(15, 15, 15))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PartyCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PartyCheckView)
                    .addComponent(PartyCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PartyCheckDel)
                    .addComponent(PartyCheckUpdate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Accounts");

        AccCheckSelect.setText(" All");
        AccCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccCheckSelectActionPerformed(evt);
            }
        });

        AccCheckBank.setText("Bank");

        AccCheckLedgerGrpBal.setText("Ledger Group Balance");

        AccCheckCash.setText("Cash");

        AccCheckJournal.setText("Journal");

        AccCheckTBal.setText("Trial Balance");

        AccCheckPaymnt.setText("Payment");

        AccCheckDaybook.setText("Day Book");

        AccCheckBalSheet.setText("Balance sheet");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AccCheckBalSheet)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(AccCheckSelect))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(AccCheckJournal)
                                .addComponent(AccCheckCash)
                                .addComponent(AccCheckBank))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(AccCheckDaybook)
                                .addComponent(AccCheckPaymnt)
                                .addComponent(AccCheckTBal))
                            .addGap(18, 18, 18)))
                    .addComponent(AccCheckLedgerGrpBal, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AccCheckSelect))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(AccCheckDaybook)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AccCheckPaymnt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AccCheckTBal))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(AccCheckJournal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AccCheckCash)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AccCheckBank)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AccCheckLedgerGrpBal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(AccCheckBalSheet)
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Purchase Invoice");
        jLabel8.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel8.setMinimumSize(new java.awt.Dimension(51, 16));

        PICheckSelect.setText("All");
        PICheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PICheckSelectActionPerformed(evt);
            }
        });

        PICheckCreate.setText("Create");

        PICheckView.setText("Reports");

        PICheckEdit.setText("Edit");

        PICheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PICheckSelect)
                .addGap(17, 17, 17))
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PICheckEdit)
                    .addComponent(PICheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PICheckDel)
                    .addComponent(PICheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PICheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PICheckView)
                    .addComponent(PICheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PICheckDel)
                    .addComponent(PICheckEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Sale Invoice (B2B, B2C)");
        jLabel9.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel9.setMinimumSize(new java.awt.Dimension(51, 16));

        SICheckSelect.setText("All");
        SICheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SICheckSelectActionPerformed(evt);
            }
        });

        SICheckCreate.setText("Create");

        SICheckView.setText("Reports");

        SICheckEdit.setText("Edit");

        SICheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SICheckEdit)
                    .addComponent(SICheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SICheckDel)
                    .addComponent(SICheckCreate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SICheckSelect)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SICheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SICheckView)
                    .addComponent(SICheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SICheckDel)
                    .addComponent(SICheckEdit))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Inventory");
        jLabel10.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel10.setMinimumSize(new java.awt.Dimension(51, 16));

        InvCheckSelect.setText("All");
        InvCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InvCheckSelectActionPerformed(evt);
            }
        });

        InvCheckAdd.setText("Add Stock");

        InvCheckView.setText("Reports");

        InvCheckDedu.setText("Deduct Stock");

        InvCheckOpeningBal.setText("Opening Balance");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(InvCheckSelect)
                        .addGap(17, 17, 17))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(InvCheckDedu)
                            .addComponent(InvCheckView))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(InvCheckAdd)
                            .addComponent(InvCheckOpeningBal))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(InvCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(InvCheckView)
                    .addComponent(InvCheckAdd))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(InvCheckDedu)
                    .addComponent(InvCheckOpeningBal))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        listCompany.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        listCompany.setMaximumSize(new java.awt.Dimension(1140, 800));
        listCompany.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listCompanyMouseClicked(evt);
            }
        });
        listCompany.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listCompanyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                listCompanyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                listCompanyKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(listCompany);

        label.setText(" Company :");

        labelSelectedCompany.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelSelectedCompany.setText("company");

        jButton1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jButton1.setMnemonic('s');
        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        SelectAll.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        SelectAll.setText("Select All");
        SelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SelectAllActionPerformed(evt);
            }
        });
        SelectAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SelectAllKeyPressed(evt);
            }
        });

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("GST Reports");
        jLabel11.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel11.setMinimumSize(new java.awt.Dimension(51, 16));

        gstReports.setText("Reports");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(gstReports)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(gstReports)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Ledger");
        jLabel12.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel12.setMinimumSize(new java.awt.Dimension(51, 16));

        LedgerCheckView.setText("Reports");

        LedgerCheckCreate.setText("Create");

        LedgerCheckEdit.setText("Edit");

        LedgerCheckDel.setText("Delete");

        LedgerCheckSel.setText("All");
        LedgerCheckSel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LedgerCheckSelActionPerformed(evt);
            }
        });

        LedgerCheckOpeningBalance.setText("Opening Balance");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(LedgerCheckSel))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LedgerCheckEdit)
                                    .addComponent(LedgerCheckView))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LedgerCheckDel)
                                    .addComponent(LedgerCheckCreate)))
                            .addComponent(LedgerCheckOpeningBalance))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LedgerCheckSel))
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LedgerCheckCreate)
                    .addComponent(LedgerCheckView))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LedgerCheckDel)
                    .addComponent(LedgerCheckEdit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LedgerCheckOpeningBalance)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelBOS.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Bill Of Supply");
        jLabel13.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel13.setMinimumSize(new java.awt.Dimension(51, 16));

        BosCheckSelect.setText("All");
        BosCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BosCheckSelectActionPerformed(evt);
            }
        });

        BosCheckCreate.setText("Create");

        BosCheckView.setText("Reports");

        BosCheckEdit.setText("Edit");
        BosCheckEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BosCheckEditActionPerformed(evt);
            }
        });

        BosCheckDel.setText("Delete");

        javax.swing.GroupLayout panelBOSLayout = new javax.swing.GroupLayout(panelBOS);
        panelBOS.setLayout(panelBOSLayout);
        panelBOSLayout.setHorizontalGroup(
            panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBOSLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BosCheckEdit)
                    .addComponent(BosCheckView))
                .addGap(18, 18, 18)
                .addGroup(panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BosCheckDel)
                    .addComponent(BosCheckCreate))
                .addContainerGap(59, Short.MAX_VALUE))
            .addGroup(panelBOSLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BosCheckSelect)
                .addContainerGap())
        );
        panelBOSLayout.setVerticalGroup(
            panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBOSLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BosCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BosCheckView)
                    .addComponent(BosCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BosCheckDel)
                    .addComponent(BosCheckEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Estimate");
        jLabel14.setMaximumSize(new java.awt.Dimension(51, 16));
        jLabel14.setMinimumSize(new java.awt.Dimension(51, 16));

        EstimateCheckSelect.setText("All");
        EstimateCheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EstimateCheckSelectActionPerformed(evt);
            }
        });

        EstimateCheckCreate.setText("Create");

        EstimateCheckView.setText("Reports");

        EstimateCheckEdit.setText("Edit");

        EstimateCheckDel.setText("Delete");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EstimateCheckEdit)
                    .addComponent(EstimateCheckView))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EstimateCheckDel)
                    .addComponent(EstimateCheckCreate))
                .addContainerGap(59, Short.MAX_VALUE))
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(EstimateCheckSelect)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EstimateCheckSelect))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EstimateCheckView)
                    .addComponent(EstimateCheckCreate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EstimateCheckDel)
                    .addComponent(EstimateCheckEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelSelectedCompany, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(SelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(panelBOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(20, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SelectAll)
                    .addComponent(label)
                    .addComponent(labelSelectedCompany))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(panelBOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jScrollPane1.setViewportView(jPanel1);

        jButton2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jButton2.setMnemonic('b');
        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton3.setMnemonic('h');
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        Home.setMnemonic('f');
        Home.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        Home.add(menuDashboard);

        menuBar.add(Home);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuBackUP.setText("Maintenance");
        menuBackUP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBackUPActionPerformed(evt);
            }
        });
        menuSettings.add(menuBackUP);

        menuBar.add(menuSettings);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1318, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelUserName)
                            .addComponent(btnLogout)
                            .addComponent(jLabel22)
                            .addComponent(jButton3))
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void ProdCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProdCheckSelectActionPerformed

        ProductSelectValue();
    }//GEN-LAST:event_ProdCheckSelectActionPerformed
    public void ProductSelectValue() {
        boolean Prodchecked = ProdCheckSelect.isSelected();
        if (Prodchecked == true) {
            ProdCheckCreate.setSelected(true);
            ProdCheckView.setSelected(true);
            ProdCheckUpdate.setSelected(true);
            ProdCheckDel.setSelected(true);
        } else {
            ProdCheckCreate.setSelected(false);
            ProdCheckView.setSelected(false);
            ProdCheckUpdate.setSelected(false);
            ProdCheckDel.setSelected(false);
        }
    }

    private void PartyCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PartyCheckSelectActionPerformed
        PartySelectValue();
    }//GEN-LAST:event_PartyCheckSelectActionPerformed

    private void SICheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SICheckSelectActionPerformed
        SISelectValue();
    }//GEN-LAST:event_SICheckSelectActionPerformed

    private void PPCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PPCheckSelectActionPerformed
        PPSelectValue();
    }//GEN-LAST:event_PPCheckSelectActionPerformed

    private void PICheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PICheckSelectActionPerformed
        PISelectValue();
    }//GEN-LAST:event_PICheckSelectActionPerformed

    private void CDCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CDCheckSelectActionPerformed
        CDSelectValue();
    }//GEN-LAST:event_CDCheckSelectActionPerformed

    private void InvCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InvCheckSelectActionPerformed
        InvSelectValue();

    }//GEN-LAST:event_InvCheckSelectActionPerformed

    private void AccCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccCheckSelectActionPerformed
        AccSelectValue();
    }//GEN-LAST:event_AccCheckSelectActionPerformed

    private void SelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SelectAllActionPerformed
        boolean checked = SelectAll.isSelected();
        if (checked == true) {
            ProdCheckSelect.setSelected(true);
            PPCheckSelect.setSelected(true);
            SICheckSelect.setSelected(true);
            InvCheckSelect.setSelected(true);
            AccCheckSelect.setSelected(true);
            PICheckSelect.setSelected(true);
            PartyCheckSelect.setSelected(true);
            CDCheckSelect.setSelected(true);
            ProdCheckCreate.setSelected(true);
            ProdCheckView.setSelected(true);
            ProdCheckUpdate.setSelected(true);
            ProdCheckDel.setSelected(true);
            PPCheckCreate.setSelected(true);
            PPCheckView.setSelected(true);
            PPCheckUpdate.setSelected(true);
            PPCheckDel.setSelected(true);
            PartyCheckCreate.setSelected(true);
            PartyCheckView.setSelected(true);
            PartyCheckUpdate.setSelected(true);
            PartyCheckDel.setSelected(true);
            SICheckCreate.setSelected(true);
            SICheckView.setSelected(true);
            SICheckEdit.setSelected(true);
            SICheckDel.setSelected(true);
            BosCheckCreate.setSelected(true);
            BosCheckView.setSelected(true);
            BosCheckEdit.setSelected(true);
            BosCheckDel.setSelected(true);
            BosCheckSelect.setSelected(true);
            EstimateCheckSelect.setSelected(true);
            EstimateCheckCreate.setSelected(true);
            EstimateCheckView.setSelected(true);
            EstimateCheckEdit.setSelected(true);
            EstimateCheckDel.setSelected(true);
            PICheckCreate.setSelected(true);
            PICheckView.setSelected(true);
            PICheckEdit.setSelected(true);
            PICheckDel.setSelected(true);
            CDCheckCreate.setSelected(true);
            CDCheckView.setSelected(true);
            CDCheckEdit.setSelected(true);
            CDCheckDel.setSelected(true);
            InvCheckView.setSelected(true);
            InvCheckAdd.setSelected(true);
            InvCheckDedu.setSelected(true);
            InvCheckOpeningBal.setSelected(true);
            AccCheckBank.setSelected(true);
            AccCheckLedgerGrpBal.setSelected(true);
            AccCheckJournal.setSelected(true);
            AccCheckCash.setSelected(true);
            AccCheckDaybook.setSelected(true);
            AccCheckPaymnt.setSelected(true);
            AccCheckTBal.setSelected(true);
            AccCheckBalSheet.setSelected(true && isPlatinum);
            LedgerCheckView.setSelected(true);
            LedgerCheckCreate.setSelected(true);
            LedgerCheckEdit.setSelected(true);
            LedgerCheckDel.setSelected(true);
            LedgerCheckSel.setSelected(true);
            LedgerCheckOpeningBalance.setSelected(true);
            gstReports.setSelected(true);
        } else {
            ProdCheckSelect.setSelected(false);
            PPCheckSelect.setSelected(false);
            SICheckSelect.setSelected(false);
            InvCheckSelect.setSelected(false);
            AccCheckSelect.setSelected(false);
            PICheckSelect.setSelected(false);
            PartyCheckSelect.setSelected(false);
            CDCheckSelect.setSelected(false);
            ProdCheckCreate.setSelected(false);
            ProdCheckView.setSelected(false);
            ProdCheckUpdate.setSelected(false);
            ProdCheckDel.setSelected(false);
            PPCheckCreate.setSelected(false);
            PPCheckView.setSelected(false);
            PPCheckUpdate.setSelected(false);
            PPCheckDel.setSelected(false);
            PartyCheckCreate.setSelected(false);
            PartyCheckView.setSelected(false);
            PartyCheckUpdate.setSelected(false);
            PartyCheckDel.setSelected(false);
            SICheckCreate.setSelected(false);
            SICheckView.setSelected(false);
            SICheckEdit.setSelected(false);
            SICheckDel.setSelected(false);
            BosCheckCreate.setSelected(false);
            BosCheckView.setSelected(false);
            BosCheckEdit.setSelected(false);
            BosCheckDel.setSelected(false);
            BosCheckSelect.setSelected(false);
            EstimateCheckSelect.setSelected(false);
            EstimateCheckCreate.setSelected(false);
            EstimateCheckView.setSelected(false);
            EstimateCheckEdit.setSelected(false);
            EstimateCheckDel.setSelected(false);
            PICheckCreate.setSelected(false);
            PICheckView.setSelected(false);
            PICheckEdit.setSelected(false);
            PICheckDel.setSelected(false);
            CDCheckCreate.setSelected(false);
            CDCheckView.setSelected(false);
            CDCheckEdit.setSelected(false);
            CDCheckDel.setSelected(false);
            InvCheckView.setSelected(false);
            InvCheckAdd.setSelected(false);
            InvCheckDedu.setSelected(false);
            InvCheckOpeningBal.setSelected(false);
            AccCheckBank.setSelected(false);
            AccCheckLedgerGrpBal.setSelected(false);
            AccCheckJournal.setSelected(false);
            AccCheckCash.setSelected(false);
            AccCheckDaybook.setSelected(false);
            AccCheckPaymnt.setSelected(false);
            AccCheckTBal.setSelected(false);
            AccCheckBalSheet.setSelected(false);
            LedgerCheckView.setSelected(false);
            LedgerCheckCreate.setSelected(false);
            LedgerCheckEdit.setSelected(false);
            LedgerCheckDel.setSelected(false);
            LedgerCheckSel.setSelected(false);
            LedgerCheckOpeningBalance.setSelected(false);
            gstReports.setSelected(false);
        }
    }//GEN-LAST:event_SelectAllActionPerformed

    public void setAction() {
        String companyName = labelSelectedCompany.getText();
        setEnabled();
        if (!companyName.equals("")) {
            for (Company company : companies) {
                if (companyName.equals(company.getCompanyName())) {
                    CompanyUserRole roles = new CompanyUserRoleLogic().getCompanyUserRole(company, selectedAppUser);

                    setSelectedRoleValues(roles);
                    break;
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select the Compnay", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        panelBOS.setVisible(isBOS);
        AccCheckBalSheet.setVisible(isPlatinum);
    }

    public void setSelectedRoleValues(CompanyUserRole roles) {

        ProdCheckCreate.setSelected(roles.isProductCreate());
        ProdCheckView.setSelected(roles.isProductView());
        ProdCheckUpdate.setSelected(roles.isProductUpdate());
        ProdCheckDel.setSelected(roles.isProductDelete());
        ProdCheckSelect.setSelected((roles.isProductCreate() && roles.isProductView() && roles.isProductUpdate() && roles.isProductDelete()));

        PPCheckCreate.setSelected(roles.isPricingPolicyCreate());
        PPCheckView.setSelected(roles.isPricingPolicyView());
        PPCheckUpdate.setSelected(roles.isPricingPolicyUpdate());
        PPCheckDel.setSelected(roles.isPricingPolicyDelete());
        PPCheckSelect.setSelected((roles.isPricingPolicyCreate() && roles.isPricingPolicyView() && roles.isPricingPolicyUpdate() && roles.isPricingPolicyDelete()));

        PartyCheckCreate.setSelected(roles.isPartyCreate());
        PartyCheckView.setSelected(roles.isPartyView());
        PartyCheckUpdate.setSelected(roles.isPartyUpdate());
        PartyCheckDel.setSelected(roles.isPartyDelete());
        PartyCheckSelect.setSelected((roles.isPartyCreate() && roles.isPartyView() && roles.isPartyUpdate() && roles.isPartyDelete()));

        SICheckCreate.setSelected(roles.isSalesCreate());
        SICheckView.setSelected(roles.isSalesView());
        SICheckEdit.setSelected(roles.isSalesUpdate());
        SICheckDel.setSelected(roles.isSalesDelete());
        SICheckSelect.setSelected((roles.isSalesCreate() && roles.isSalesView() && roles.isSalesUpdate() && roles.isSalesDelete()));

        BosCheckCreate.setSelected(roles.isBosCreate());
        BosCheckView.setSelected(roles.isBosView());
        BosCheckEdit.setSelected(roles.isBosUpdate());
        BosCheckDel.setSelected(roles.isBosDelete());
        BosCheckSelect.setSelected((roles.isBosCreate() && roles.isBosView() && roles.isBosUpdate() && roles.isBosDelete()));

        EstimateCheckCreate.setSelected(roles.isEstimateCreate());
        EstimateCheckView.setSelected(roles.isEstimateView());
        EstimateCheckEdit.setSelected(roles.isEstimateUpdate());
        EstimateCheckDel.setSelected(roles.isEstimateDelete());
        EstimateCheckSelect.setSelected((roles.isEstimateCreate() && roles.isEstimateView() && roles.isEstimateUpdate() && roles.isEstimateDelete()));

        PICheckCreate.setSelected(roles.isPurchaseCreate());
        PICheckView.setSelected(roles.isPurchaseView());
        PICheckEdit.setSelected(roles.isPurchaseUpdate());
        PICheckDel.setSelected(roles.isPurchaseDelete());
        PICheckSelect.setSelected((roles.isPurchaseCreate() && roles.isPurchaseView() && roles.isPurchaseUpdate() && roles.isPurchaseDelete()));

        CDCheckCreate.setSelected(roles.isCrdrNoteCreate());
        CDCheckView.setSelected(roles.isCrdrNoteView());
        CDCheckEdit.setSelected(roles.isCrdrNoteUpdate());
        CDCheckDel.setSelected(roles.isCrdrNoteDelete());
        CDCheckSelect.setSelected((roles.isCrdrNoteCreate() && roles.isCrdrNoteView() && roles.isCrdrNoteUpdate() && roles.isCrdrNoteDelete()));

        InvCheckView.setSelected(roles.isInventoryView());
        InvCheckAdd.setSelected(roles.isInventoryAdd());
        InvCheckDedu.setSelected(roles.isInventoryDeduct());
        InvCheckOpeningBal.setSelected(roles.isInventoryOpeningBalance());
        InvCheckSelect.setSelected((roles.isInventoryView() && roles.isInventoryAdd() && roles.isInventoryDeduct() && roles.isInventoryOpeningBalance()));

        gstReports.setSelected(roles.isGstReports());

        LedgerCheckView.setSelected(roles.isLedgerView());
        LedgerCheckCreate.setSelected(roles.isLedgerCreate());
        LedgerCheckEdit.setSelected(roles.isLedgerUpdate());
        LedgerCheckDel.setSelected(roles.isLedgerDelete());
        LedgerCheckOpeningBalance.setSelected(roles.isLedgerOpeningBalance());
        LedgerCheckSel.setSelected((roles.isLedgerView() && roles.isLedgerCreate() && roles.isLedgerUpdate() && roles.isLedgerDelete() && roles.isLedgerOpeningBalance()));

        AccCheckBank.setSelected(roles.isAccountsBank());
        AccCheckLedgerGrpBal.setSelected(roles.isAccountsLedgerGroupBalance());
        AccCheckJournal.setSelected(roles.isAccountsJournal());
        AccCheckCash.setSelected(roles.isAccountsCash());
        AccCheckDaybook.setSelected(roles.isAccountsDayBook());
        AccCheckPaymnt.setSelected(roles.isAccountsPayment());
        AccCheckTBal.setSelected(roles.isAccountsTrailBalance());
        AccCheckBalSheet.setSelected(roles.isAccountsBalanceSheet() && isPlatinum);
        Boolean checkBalanceSheet = false;
        if (isPlatinum) {
            checkBalanceSheet = roles.isAccountsBalanceSheet();
        } else {
            checkBalanceSheet = true;
        }
        AccCheckSelect.setSelected((roles.isAccountsBank() && roles.isAccountsLedgerGroupBalance() && roles.isAccountsJournal() && roles.isAccountsCash()
                && roles.isAccountsDayBook() && roles.isAccountsPayment() && roles.isAccountsTrailBalance() && checkBalanceSheet));
    }

    public void selectedListValue() {

        String company = listCompany.getSelectedValue();

        labelSelectedCompany.setText(company);
    }

    private void listCompanyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listCompanyMouseClicked
        selectedListValue();
        setAction();
    }//GEN-LAST:event_listCompanyMouseClicked

    private void listCompanyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listCompanyKeyPressed
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            setAction();
        }
    }//GEN-LAST:event_listCompanyKeyPressed

    private void listCompanyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listCompanyKeyReleased

    }//GEN-LAST:event_listCompanyKeyReleased

    private void listCompanyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listCompanyKeyTyped
        //        selectedListValue();
    }//GEN-LAST:event_listCompanyKeyTyped

    private void SelectAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SelectAllKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            SelectAll.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            SelectAll.setSelected(false);
        }
//        else if (vChar == com.sun.glass.events.KeyEvent.VK_ENTER) {
//            comboIGST.requestFocus();
//        }
    }//GEN-LAST:event_SelectAllKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Company getCompany = new Company();
//        company.setCompanyName(labelSelectedCompany.getText());

        String companyName = labelSelectedCompany.getText();
        if (!companyName.equals("")) {
            for (Company company : companies) {
                if (companyName.equals(company.getCompanyName())) {
                    getCompany = company;
                    break;
                }
            }
        }
//        AppUser appuser = new AppUser();
//        appuser.setUserName(txtUsername.getText());

        CompanyUserRole companyuserrole = new CompanyUserRoleLogic().getCompanyUserRole(getCompany, selectedAppUser);;

        companyuserrole.setAccountsBank(AccCheckBank.isSelected());
        companyuserrole.setAccountsCash(AccCheckCash.isSelected());
        companyuserrole.setAccountsDayBook(AccCheckDaybook.isSelected());
        companyuserrole.setAccountsJournal(AccCheckJournal.isSelected());
        companyuserrole.setAccountsLedgerGroupBalance(AccCheckLedgerGrpBal.isSelected());
        companyuserrole.setAccountsPayment(AccCheckPaymnt.isSelected());
        companyuserrole.setAccountsTrailBalance(AccCheckTBal.isSelected());
        companyuserrole.setAccountsBalanceSheet(AccCheckBalSheet.isSelected() && isPlatinum);

        companyuserrole.setCrdrNoteCreate(CDCheckCreate.isSelected());
        companyuserrole.setCrdrNoteDelete(CDCheckDel.isSelected());
        companyuserrole.setCrdrNoteUpdate(CDCheckEdit.isSelected());
        companyuserrole.setCrdrNoteView(CDCheckView.isSelected());

        companyuserrole.setInventoryAdd(InvCheckAdd.isSelected());
        companyuserrole.setInventoryDeduct(InvCheckDedu.isSelected());
        companyuserrole.setInventoryView(InvCheckView.isSelected());
        companyuserrole.setInventoryOpeningBalance(InvCheckOpeningBal.isSelected());

        companyuserrole.setPartyCreate(PartyCheckCreate.isSelected());
        companyuserrole.setPartyDelete(PartyCheckDel.isSelected());
        companyuserrole.setPartyUpdate(PartyCheckUpdate.isSelected());
        companyuserrole.setPartyView(PartyCheckView.isSelected());

        companyuserrole.setPricingPolicyCreate(PPCheckCreate.isSelected());
        companyuserrole.setPricingPolicyDelete(PPCheckDel.isSelected());
        companyuserrole.setPricingPolicyUpdate(PPCheckUpdate.isSelected());
        companyuserrole.setPricingPolicyView(PPCheckView.isSelected());

        companyuserrole.setProductCreate(ProdCheckCreate.isSelected());
        companyuserrole.setProductDelete(ProdCheckDel.isSelected());
        companyuserrole.setProductUpdate(ProdCheckUpdate.isSelected());
        companyuserrole.setProductView(ProdCheckView.isSelected());

        companyuserrole.setPurchaseCreate(PICheckCreate.isSelected());
        companyuserrole.setPurchaseDelete(PICheckDel.isSelected());
        companyuserrole.setPurchaseUpdate(PICheckEdit.isSelected());
        companyuserrole.setPurchaseView(PICheckView.isSelected());

        companyuserrole.setSalesCreate(SICheckCreate.isSelected());
        companyuserrole.setSalesDelete(SICheckDel.isSelected());
        companyuserrole.setSalesUpdate(SICheckEdit.isSelected());
        companyuserrole.setSalesView(SICheckView.isSelected());

        companyuserrole.setBosCreate((BosCheckCreate.isSelected() && isBOS));
        companyuserrole.setBosDelete((BosCheckDel.isSelected() && isBOS));
        companyuserrole.setBosUpdate((BosCheckEdit.isSelected() && isBOS));
        companyuserrole.setBosView((BosCheckView.isSelected() && isBOS));

        companyuserrole.setEstimateCreate(EstimateCheckCreate.isSelected());
        companyuserrole.setEstimateDelete(EstimateCheckDel.isSelected());
        companyuserrole.setEstimateUpdate(EstimateCheckEdit.isSelected());
        companyuserrole.setEstimateView(EstimateCheckView.isSelected());

        companyuserrole.setLedgerCreate(LedgerCheckCreate.isSelected());
        companyuserrole.setLedgerDelete(LedgerCheckDel.isSelected());
        companyuserrole.setLedgerUpdate(LedgerCheckEdit.isSelected());
        companyuserrole.setLedgerView(LedgerCheckView.isSelected());
        companyuserrole.setLedgerOpeningBalance(LedgerCheckOpeningBalance.isSelected());

        companyuserrole.setGstReports(gstReports.isSelected());
//        companyuserrole.setCompany(getCompany);
//        
//        companyuserrole.setUser(selectedAppUser);
        Boolean isSuccess = new CompanyUserRoleLogic().updateUserRole(companyuserrole);
        if (isSuccess) {
            JOptionPane.showMessageDialog(null, "Please LOGOUT and LOGIN To Update User Roles for " + companyName, "Message", JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
        }
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuBackUPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBackUPActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBackUPActionPerformed

    private void LedgerCheckSelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LedgerCheckSelActionPerformed
        LedgerSelectValue();
    }//GEN-LAST:event_LedgerCheckSelActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void BosCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BosCheckSelectActionPerformed
        BosSelectValue();
    }//GEN-LAST:event_BosCheckSelectActionPerformed

    private void EstimateCheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EstimateCheckSelectActionPerformed
        EstimateSelectValue();
    }//GEN-LAST:event_EstimateCheckSelectActionPerformed

    private void CDCheckDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CDCheckDelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CDCheckDelActionPerformed

    private void BosCheckEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BosCheckEditActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BosCheckEditActionPerformed

    public void LedgerSelectValue() {
        boolean checked = LedgerCheckSel.isSelected();
        if (checked == true) {
            LedgerCheckView.setSelected(true);
            LedgerCheckCreate.setSelected(true);
            LedgerCheckEdit.setSelected(true);
            LedgerCheckDel.setSelected(true);
            LedgerCheckOpeningBalance.setSelected(true);
        } else {
            LedgerCheckView.setSelected(false);
            LedgerCheckCreate.setSelected(false);
            LedgerCheckEdit.setSelected(false);
            LedgerCheckDel.setSelected(false);
            LedgerCheckOpeningBalance.setSelected(false);
        }
    }

    public void SISelectValue() {
        boolean checked = SICheckSelect.isSelected();
        if (checked == true) {
            SICheckCreate.setSelected(true);
            SICheckView.setSelected(true);
            SICheckEdit.setSelected(true);
            SICheckDel.setSelected(true);
        } else {
            SICheckCreate.setSelected(false);
            SICheckView.setSelected(false);
            SICheckEdit.setSelected(false);
            SICheckDel.setSelected(false);
        }
    }

    public void BosSelectValue() {
        boolean checked = BosCheckSelect.isSelected();
        if (checked == true) {
            BosCheckCreate.setSelected(true);
            BosCheckView.setSelected(true);
            BosCheckEdit.setSelected(true);
            BosCheckDel.setSelected(true);
        } else {
            BosCheckCreate.setSelected(false);
            BosCheckView.setSelected(false);
            BosCheckEdit.setSelected(false);
            BosCheckDel.setSelected(false);
        }
    }

    public void EstimateSelectValue() {
        boolean checked = EstimateCheckSelect.isSelected();
        if (checked == true) {
            EstimateCheckCreate.setSelected(true);
            EstimateCheckView.setSelected(true);
            EstimateCheckEdit.setSelected(true);
            EstimateCheckDel.setSelected(true);
        } else {
            EstimateCheckCreate.setSelected(false);
            EstimateCheckView.setSelected(false);
            EstimateCheckEdit.setSelected(false);
            EstimateCheckDel.setSelected(false);
        }
    }

    public void PPSelectValue() {
        boolean checked = PPCheckSelect.isSelected();
        if (checked == true) {
            PPCheckCreate.setSelected(true);
            PPCheckView.setSelected(true);
            PPCheckUpdate.setSelected(true);
            PPCheckDel.setSelected(true);
        } else {
            PPCheckCreate.setSelected(false);
            PPCheckView.setSelected(false);
            PPCheckUpdate.setSelected(false);
            PPCheckDel.setSelected(false);
        }
    }

    public void PISelectValue() {
        boolean checked = PICheckSelect.isSelected();
        if (checked == true) {
            PICheckCreate.setSelected(true);
            PICheckView.setSelected(true);
            PICheckEdit.setSelected(true);
            PICheckDel.setSelected(true);
        } else {
            PICheckCreate.setSelected(false);
            PICheckView.setSelected(false);
            PICheckEdit.setSelected(false);
            PICheckDel.setSelected(false);
        }
    }

    public void CDSelectValue() {
        boolean checked = CDCheckSelect.isSelected();
        if (checked == true) {
            CDCheckCreate.setSelected(true);
            CDCheckView.setSelected(true);
            CDCheckEdit.setSelected(true);
            CDCheckDel.setSelected(true);
        } else {
            CDCheckCreate.setSelected(false);
            CDCheckView.setSelected(false);
            CDCheckEdit.setSelected(false);
            CDCheckDel.setSelected(false);
        }
    }

    public void InvSelectValue() {
        boolean checked = InvCheckSelect.isSelected();
        if (checked == true) {
            InvCheckView.setSelected(true);
            InvCheckAdd.setSelected(true);
            InvCheckDedu.setSelected(true);
            InvCheckOpeningBal.setSelected(true);
        } else {
            SICheckCreate.setSelected(false);
            InvCheckAdd.setSelected(false);
            InvCheckDedu.setSelected(false);
            InvCheckOpeningBal.setSelected(false);
        }
    }

    public void AccSelectValue() {
        boolean checked = AccCheckSelect.isSelected();
        if (checked == true) {
            AccCheckBank.setSelected(true);
            AccCheckLedgerGrpBal.setSelected(true);
            AccCheckJournal.setSelected(true);
            AccCheckCash.setSelected(true);
            AccCheckDaybook.setSelected(true);
            AccCheckPaymnt.setSelected(true);
            AccCheckTBal.setSelected(true);
            AccCheckBalSheet.setSelected(true && isPlatinum);
        } else {
            AccCheckBank.setSelected(false);
            AccCheckLedgerGrpBal.setSelected(false);
            AccCheckJournal.setSelected(false);
            AccCheckCash.setSelected(false);
            AccCheckDaybook.setSelected(false);
            AccCheckPaymnt.setSelected(false);
            AccCheckTBal.setSelected(false);
            AccCheckBalSheet.setSelected(false);
        }
    }

    public void PartySelectValue() {
        boolean checked = PartyCheckSelect.isSelected();
        if (checked == true) {
            PartyCheckCreate.setSelected(true);
            PartyCheckView.setSelected(true);
            PartyCheckUpdate.setSelected(true);
            PartyCheckDel.setSelected(true);
        } else {
            PartyCheckCreate.setSelected(false);
            PartyCheckView.setSelected(false);
            PartyCheckUpdate.setSelected(false);
            PartyCheckDel.setSelected(false);
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserRoleUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserRoleUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserRoleUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserRoleUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UserRoleUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox AccCheckBalSheet;
    private javax.swing.JCheckBox AccCheckBank;
    private javax.swing.JCheckBox AccCheckCash;
    private javax.swing.JCheckBox AccCheckDaybook;
    private javax.swing.JCheckBox AccCheckJournal;
    private javax.swing.JCheckBox AccCheckLedgerGrpBal;
    private javax.swing.JCheckBox AccCheckPaymnt;
    private javax.swing.JCheckBox AccCheckSelect;
    private javax.swing.JCheckBox AccCheckTBal;
    private javax.swing.JCheckBox BosCheckCreate;
    private javax.swing.JCheckBox BosCheckDel;
    private javax.swing.JCheckBox BosCheckEdit;
    private javax.swing.JCheckBox BosCheckSelect;
    private javax.swing.JCheckBox BosCheckView;
    private javax.swing.JCheckBox CDCheckCreate;
    private javax.swing.JCheckBox CDCheckDel;
    private javax.swing.JCheckBox CDCheckEdit;
    private javax.swing.JCheckBox CDCheckSelect;
    private javax.swing.JCheckBox CDCheckView;
    private javax.swing.JCheckBox EstimateCheckCreate;
    private javax.swing.JCheckBox EstimateCheckDel;
    private javax.swing.JCheckBox EstimateCheckEdit;
    private javax.swing.JCheckBox EstimateCheckSelect;
    private javax.swing.JCheckBox EstimateCheckView;
    private javax.swing.JMenu Home;
    private javax.swing.JCheckBox InvCheckAdd;
    private javax.swing.JCheckBox InvCheckDedu;
    private javax.swing.JCheckBox InvCheckOpeningBal;
    private javax.swing.JCheckBox InvCheckSelect;
    private javax.swing.JCheckBox InvCheckView;
    private javax.swing.JCheckBox LedgerCheckCreate;
    private javax.swing.JCheckBox LedgerCheckDel;
    private javax.swing.JCheckBox LedgerCheckEdit;
    private javax.swing.JCheckBox LedgerCheckOpeningBalance;
    private javax.swing.JCheckBox LedgerCheckSel;
    private javax.swing.JCheckBox LedgerCheckView;
    private javax.swing.JCheckBox PICheckCreate;
    private javax.swing.JCheckBox PICheckDel;
    private javax.swing.JCheckBox PICheckEdit;
    private javax.swing.JCheckBox PICheckSelect;
    private javax.swing.JCheckBox PICheckView;
    private javax.swing.JCheckBox PPCheckCreate;
    private javax.swing.JCheckBox PPCheckDel;
    private javax.swing.JCheckBox PPCheckSelect;
    private javax.swing.JCheckBox PPCheckUpdate;
    private javax.swing.JCheckBox PPCheckView;
    private javax.swing.JCheckBox PartyCheckCreate;
    private javax.swing.JCheckBox PartyCheckDel;
    private javax.swing.JCheckBox PartyCheckSelect;
    private javax.swing.JCheckBox PartyCheckUpdate;
    private javax.swing.JCheckBox PartyCheckView;
    private javax.swing.JCheckBox ProdCheckCreate;
    private javax.swing.JCheckBox ProdCheckDel;
    private javax.swing.JCheckBox ProdCheckSelect;
    private javax.swing.JCheckBox ProdCheckUpdate;
    private javax.swing.JCheckBox ProdCheckView;
    private javax.swing.JCheckBox SICheckCreate;
    private javax.swing.JCheckBox SICheckDel;
    private javax.swing.JCheckBox SICheckEdit;
    private javax.swing.JCheckBox SICheckSelect;
    private javax.swing.JCheckBox SICheckView;
    private javax.swing.JCheckBox SelectAll;
    private javax.swing.JButton btnLogout;
    private javax.swing.JCheckBox gstReports;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label;
    private javax.swing.JLabel labelSelectedCompany;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JList<String> listCompany;
    private javax.swing.JMenuItem menuBackUP;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JPanel panelBOS;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
