/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.sun.glass.events.KeyEvent;
import india.abs.gstonepro.api.dao.GoDownCRUD;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CompanyUserRoleLogic;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.business.ProductGroupLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.TaxLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.event.KeyAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author ABS
 */
public class NewProductUI extends javax.swing.JFrame {

    /**
     * Creates new form ProductUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    AppUser user = SessionDataUtil.getSelectedUser();
    Long companyId = company.getCompanyId();
    Long goDownId;
    CompanyUserRole companyUserRole = new CompanyUserRoleLogic().getCompanyUserRole(company, user);
//    GodownStockEntryUI godownStockEntryUI = new GodownStockEntryUI();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    ProductDescriptionUI productDescriptionUI = new ProductDescriptionUI();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isProfitEnable = company.getCompanyPolicy().isProfitEnabled();
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isService = false;
    String strSecondValue;
    GoDownCRUD g4 = new GoDownCRUD();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<GoDown> goDowns = new GoDownLogic().fetchAllGodowns(companyId);
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public NewProductUI() {
//        try {
        initComponents();
        comboGroup.requestFocus();
        //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
        btnAdd.setEnabled(true);
        txtUQC2Value.setText("1");
        txtUQC2Value.setEnabled(false);
        labelBasePerUQC1.setVisible(isProfitEnable);
        labelBasePerUQC2.setVisible(isProfitEnable);
        txtBasePriceUQC1.setVisible(isProfitEnable);
        txtBasePriceUQC2.setVisible(isProfitEnable);
        labelBasePrice.setVisible(isProfitEnable);
        checkBasePriceTaxInclusive.setVisible(isProfitEnable);
        if (!isInventoryEnabled) {
            jPanel5.setVisible(false);
        }
        setGodown();
        setGroup();
        setUQCvalue();
        setTaxValue();

//setsystemPolicy();
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
        jTable1.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                
//                char key = e.getKeyChar();
//                int selectedColumn = jTable1.getSelectedColumn();
//                for (int i = 0; i < model.getRowCount(); i++) {
//                    String value = (String) model.getValueAt(i, selectedColumn);
//                    model.setValueAt(value + key, i, selectedColumn);
//                }
            }
        });

    }

    public void setGroup() {
        List<ProductGroup> productGroups = new ProductGroupLogic().fetchAllPG();
        for (ProductGroup productGroup : productGroups) {
            comboGroup.addItem(productGroup.getProductGroupName());
        }
    }

    public void setUQCvalue() {
        List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
        for (UQC uqc : UQCs) {
            comboUQC1.addItem(uqc.getQuantityName());
            comboUQC2.addItem(uqc.getQuantityName());
        }
    }

    public String setStockConverstion(int qty, String UQC1, String UQC2, int UQC2Value) {
        int UQC1Count = qty / UQC2Value;
        int UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void setTaxValue() {
        List<Tax> taxes = new ArrayList<Tax>();
        taxes = new TaxLogic().fetchAllTaxes();
        Collections.sort(taxes, new Comparator<Tax>() {
            @Override
            public int compare(Tax o1, Tax o2) {
                return o1.getTaxRate() < o2.getTaxRate() ? -1 : (o1.getTaxRate() > o2.getTaxRate()) ? 1 : 0;
            }
        });
        for (Tax tax : taxes) {
            String s = Float.toString(tax.getTaxRate());
            comboCGST.addItem(s);
            comboIGST.addItem(s);
            comboSGST.addItem(s);

        }
    }

    public void setGodown() {

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (GoDown goDown : goDowns) {
            goDownId = goDown.getGoDownId();
            Object[] row = {
                goDownId,
                goDown.getGoDownName(),
                "",
                "",
                "",
                "",
                "", "", txtUQC2Value.getText()
            };
            model.addRow(row);
        }
//        TableColumn date = jTable1.getColumnModel().getColumn(3);
//        date.setCellEditor(new DatePickerCellEditor());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem3 = new javax.swing.JMenuItem();
        jLabel4 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtTamilName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtHsncode = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        txtCode = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        comboGroup = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        CheckExempted = new javax.swing.JCheckBox();
        ChecknonGst = new javax.swing.JCheckBox();
        comboIGST = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        comboCGST = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        comboSGST = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        comboUQC1 = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtUQC2Value = new javax.swing.JTextField();
        comboUQC2 = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        txtBasePriceUQC2 = new javax.swing.JTextField();
        labelBasePerUQC2 = new javax.swing.JLabel();
        txtBasePriceUQC1 = new javax.swing.JTextField();
        labelBasePerUQC1 = new javax.swing.JLabel();
        labelBasePrice = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPriceUQC1 = new javax.swing.JTextField();
        txtPriceUQC2 = new javax.swing.JTextField();
        labelPerUQC1 = new javax.swing.JLabel();
        labelPerUQC2 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        checkBasePriceTaxExclusive = new javax.swing.JCheckBox();
        checkTaxExclusive = new javax.swing.JCheckBox();
        checkBasePriceTaxInclusive = new javax.swing.JCheckBox();
        checkTaxInclusive = new javax.swing.JCheckBox();
        txtShortName = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jMenuItem3.setText("jMenuItem3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("New Product");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel4.setText("Product Name");

        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        txtTamilName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTamilNameActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Short Name");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("HSN Code");

        txtHsncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHsncodeActionPerformed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel26.setText("Product Code");

        txtCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodeActionPerformed(evt);
            }
        });

        jLabel27.setText("Product Group");

        comboGroup.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGroupKeyPressed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Product Name in Regional Language (Optional)");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        CheckExempted.setText("Exempted");
        CheckExempted.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckExemptedActionPerformed(evt);
            }
        });

        ChecknonGst.setText("nonGST");
        ChecknonGst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChecknonGstActionPerformed(evt);
            }
        });

        comboIGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboIGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboIGSTFocusGained(evt);
            }
        });
        comboIGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboIGSTActionPerformed(evt);
            }
        });
        comboIGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboIGSTKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel11.setText("IGST %");

        comboCGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboCGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboCGSTFocusGained(evt);
            }
        });
        comboCGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCGSTActionPerformed(evt);
            }
        });
        comboCGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboCGSTKeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("CGST %");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("SGST %");

        comboSGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboSGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboSGSTFocusGained(evt);
            }
        });
        comboSGST.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSGSTActionPerformed(evt);
            }
        });
        comboSGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboSGSTKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChecknonGst)
                            .addComponent(CheckExempted))))
                .addContainerGap(119, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CheckExempted)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChecknonGst)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("1");

        comboUQC1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboUQC1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUQC1FocusGained(evt);
            }
        });
        comboUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUQC1ActionPerformed(evt);
            }
        });
        comboUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboUQC1KeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel9.setText("UQC Conversion");

        jLabel8.setText("=");

        txtUQC2Value.setText("1");
        txtUQC2Value.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2ValueActionPerformed(evt);
            }
        });
        txtUQC2Value.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2ValueKeyTyped(evt);
            }
        });

        comboUQC2.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboUQC2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboUQC2FocusGained(evt);
            }
        });
        comboUQC2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                comboUQC2MouseClicked(evt);
            }
        });
        comboUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUQC2ActionPerformed(evt);
            }
        });
        comboUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboUQC2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(comboUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUQC2Value, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtBasePriceUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasePriceUQC2ActionPerformed(evt);
            }
        });
        txtBasePriceUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC2KeyTyped(evt);
            }
        });

        labelBasePerUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePerUQC2.setText("per UQC2");

        txtBasePriceUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasePriceUQC1ActionPerformed(evt);
            }
        });
        txtBasePriceUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBasePriceUQC1KeyTyped(evt);
            }
        });

        labelBasePerUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePerUQC1.setText("per UQC1");

        labelBasePrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelBasePrice.setText("Base Price");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("Price");

        txtPriceUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUQC1ActionPerformed(evt);
            }
        });
        txtPriceUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyTyped(evt);
            }
        });

        txtPriceUQC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUQC2ActionPerformed(evt);
            }
        });
        txtPriceUQC2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPriceUQC2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceUQC2KeyTyped(evt);
            }
        });

        labelPerUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelPerUQC1.setText("per UQC1");

        labelPerUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelPerUQC2.setText("per UQC2");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        checkBasePriceTaxExclusive.setSelected(true);
        checkBasePriceTaxExclusive.setText("Tax Exclusive");
        checkBasePriceTaxExclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkBasePriceTaxExclusiveItemStateChanged(evt);
            }
        });
        checkBasePriceTaxExclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBasePriceTaxExclusiveActionPerformed(evt);
            }
        });
        checkBasePriceTaxExclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxExclusiveKeyPressed(evt);
            }
        });

        checkTaxExclusive.setSelected(true);
        checkTaxExclusive.setText("Tax Exclusive");
        checkTaxExclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxExclusiveItemStateChanged(evt);
            }
        });
        checkTaxExclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxExclusiveKeyPressed(evt);
            }
        });

        checkBasePriceTaxInclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkBasePriceTaxInclusive.setText("Tax Inclusive");
        checkBasePriceTaxInclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkBasePriceTaxInclusiveItemStateChanged(evt);
            }
        });
        checkBasePriceTaxInclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBasePriceTaxInclusiveActionPerformed(evt);
            }
        });
        checkBasePriceTaxInclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxInclusiveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                checkBasePriceTaxInclusiveKeyReleased(evt);
            }
        });

        checkTaxInclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkTaxInclusive.setText("Tax Inclusive");
        checkTaxInclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxInclusiveItemStateChanged(evt);
            }
        });
        checkTaxInclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTaxInclusiveActionPerformed(evt);
            }
        });
        checkTaxInclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxInclusiveKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtBasePriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBasePerUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelBasePrice)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtBasePriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBasePerUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(checkBasePriceTaxExclusive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(checkBasePriceTaxInclusive)))
                .addGap(18, 18, 18)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtPriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPerUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtPriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPerUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(checkTaxExclusive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(checkTaxInclusive)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(labelBasePrice)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelBasePerUQC1)
                                    .addComponent(txtBasePriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelBasePerUQC2)
                                    .addComponent(txtBasePriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtPriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelPerUQC1))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(labelPerUQC2)
                                    .addComponent(txtPriceUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(checkBasePriceTaxExclusive)
                                    .addComponent(checkBasePriceTaxInclusive))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(checkTaxInclusive, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(checkTaxExclusive, javax.swing.GroupLayout.Alignment.TRAILING)))))
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        txtShortName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtShortNameActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("PRODUCT");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Godown Id", "Godown", "Opening Stock ", "Opening Date", "Stock Value", "Godown Stock Detail ID", "UQC1", "UQC2", "UQC2value"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTable1KeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(0);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 704, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(82, 82, 82)
                                        .addComponent(jLabel26))
                                    .addComponent(jLabel27)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel4)
                                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTamilName, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtCode, txtHsncode, txtShortName});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTamilName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        txtTamilName.requestFocus();
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtTamilNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTamilNameActionPerformed
        txtHsncode.requestFocus();
    }//GEN-LAST:event_txtTamilNameActionPerformed

    private void txtShortNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtShortNameActionPerformed

        comboUQC1.requestFocus();

    }//GEN-LAST:event_txtShortNameActionPerformed

    private void txtHsncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHsncodeActionPerformed
        txtCode.requestFocus();
    }//GEN-LAST:event_txtHsncodeActionPerformed

    private void comboUQC1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUQC1FocusGained
        comboUQC1.showPopup();
    }//GEN-LAST:event_comboUQC1FocusGained

    private void comboUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboUQC1KeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {

            comboUQC2.requestFocus();
        }
    }//GEN-LAST:event_comboUQC1KeyPressed

    private void txtUQC2ValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2ValueActionPerformed
        txtBasePriceUQC1.requestFocus();
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            jTable1.setValueAt(txtUQC2Value.getText(), i, 8);
        }
    }//GEN-LAST:event_txtUQC2ValueActionPerformed

    private void comboUQC2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboUQC2FocusGained
        comboUQC2.showPopup();
    }//GEN-LAST:event_comboUQC2FocusGained

    private void comboUQC2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboUQC2KeyPressed
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            if (comboUQC1.getSelectedItem() == comboUQC2.getSelectedItem()) {
                txtUQC2Value.setEnabled(false);
                txtBasePriceUQC2.setEnabled(false);
                labelBasePerUQC2.setEnabled(false);
                txtPriceUQC2.setEnabled(false);
                labelPerUQC2.setEnabled(false);
                txtUQC2Value.setText("1");
                txtBasePriceUQC1.requestFocus();
            } else {
                txtUQC2Value.setEnabled(true);
                txtBasePriceUQC2.setEnabled(true);
                labelBasePerUQC2.setEnabled(true);
                txtPriceUQC2.setEnabled(true);
                labelPerUQC2.setEnabled(true);
                txtUQC2Value.setText("");
                txtUQC2Value.requestFocus();
            }
        }
    }//GEN-LAST:event_comboUQC2KeyPressed

    private void comboIGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboIGSTActionPerformed

    }//GEN-LAST:event_comboIGSTActionPerformed

    private void comboCGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCGSTActionPerformed

    }//GEN-LAST:event_comboCGSTActionPerformed

    private void comboSGSTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSGSTActionPerformed

    }//GEN-LAST:event_comboSGSTActionPerformed

    private void comboIGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboIGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            float value = Float.parseFloat((String) comboIGST.getSelectedItem());
            String divValue = String.valueOf(value / 2);

            comboCGST.setSelectedItem(divValue);
            comboSGST.setSelectedItem(divValue);

//            comboStockIGST.setSelectedItem(comboIGST.getSelectedItem());
//            comboStockCGST.setSelectedItem(comboCGST.getSelectedItem());
//            comboStockSGST.setSelectedItem(comboSGST.getSelectedItem());
            comboCGST.requestFocus();
        }
    }//GEN-LAST:event_comboIGSTKeyPressed

    private void comboCGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboCGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            comboSGST.requestFocus();
        }
    }//GEN-LAST:event_comboCGSTKeyPressed

    private void comboSGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboSGSTKeyPressed
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            if(isInventoryEnabled){
            jTable1.requestFocus();
//            for(int )
            jTable1.editCellAt(0, 3);
            }else{
                btnAdd.requestFocus();
            }
        }
//int i = jTable1.getSelectedRow();
//        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
////            if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
//            if (btnAdd.isEnabled()) {
//                btnAdd.requestFocus();
//            } else {
//                btnUpdate.requestFocus();
//            }
//        }
////            else {
//                txtStockUQC1Value.requestFocus();
//            }
//        }

    }//GEN-LAST:event_comboSGSTKeyPressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        addProduct();
        this.dispose();
    }//GEN-LAST:event_btnAddActionPerformed

    public Boolean checkSameProductName(String name, int pos) {
        Boolean isFound = false;
        for (Product product : products) {
            if (product.getName().equals(name)) {
                isFound = true;
            }
        }
        return isFound;
    }

    public long getProductGroupIDbyName() {

        String strProductGroup = comboGroup.getSelectedItem().toString();
        Long productGroupId = null;
//        Company company = null;

        List<ProductGroup> productGroups = new ProductGroupLogic().fetchAllPG();
        for (ProductGroup productGroup : productGroups) {

            if (productGroup.getProductGroupName().equals(strProductGroup)) {
//                if (productGroup.getProductGroupName() == null) {
//                    productGroupId = null;
//                } else {
                productGroupId = productGroup.getProductGroupId();
//                }

//                company = productGroup.getCompany();
            }

        }
        return productGroupId;
    }

    public void addProduct() {
        String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strProductGroup;
        if (comboGroup.getSelectedItem().toString().equals("No Product Group")) {
            strProductGroup = null;
        } else {
            strProductGroup = comboGroup.getSelectedItem().toString();
        }
        String strHsnName = txtHsncode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strSecondValue = txtUQC2Value.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strAmount = txtPriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strBaseAmount = txtBasePriceUQC2.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateStock.getDate(), false);
        Boolean isAlreadyHere = checkSameProductName(strProductName, -1);
        if (strProductName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
//        } else if (strHsnName.equals("")) {
//            JOptionPane.showMessageDialog(null, "Please enter the HSN Code", "Alert", JOptionPane.WARNING_MESSAGE);
//            txtHsncode.requestFocus();
        } else if (strSecondValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the UQC Value", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC2Value.requestFocus();
//        } else if (strAmount.equals("")) {
//            JOptionPane.showMessageDialog(null, "Please enter the Amount", "Alert", JOptionPane.WARNING_MESSAGE);
//            txtPriceUQC1.requestFocus();
        } else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strProductName + " is already here. Please Enter another Product Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
//        } else if (!isInBetweenFinancialYear) {
//            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
//                    + " to <b>" + strFinancialYearEnd + "</b></html>";
//            JLabel label = new JLabel(msg);
//            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        } else {

            int getWholeStock = getStock(Integer.parseInt(strSecondValue));
            if (strBaseAmount.equals("")) {
                strBaseAmount = "0.00";
            }
            Long productGroupId = getProductGroupIDbyName();
            ProductGroup newProductGroup = new ProductGroup();

            newProductGroup.setCompany(company);
            newProductGroup.setProductGroupId(productGroupId);
            newProductGroup.setProductGroupName(strProductGroup);

            Product newProduct = new Product();
            newProduct.setProductGroup(newProductGroup);
            newProduct.setName(strProductName);
            newProduct.setTamilName(txtTamilName.getText());
            newProduct.setShortName(txtShortName.getText());
            newProduct.setProductCode(txtCode.getText());
            newProduct.setHsnCode(strHsnName);
            newProduct.setIgstPercentage(Float.parseFloat((String) comboIGST.getSelectedItem()));
            newProduct.setCgstPercentage(Float.parseFloat((String) comboCGST.getSelectedItem()));
            newProduct.setSgstPercentage(Float.parseFloat((String) comboSGST.getSelectedItem()));
            newProduct.setDescriptionOne("");
            newProduct.setDescriptionTwo("");
            newProduct.setDescriptionThree("");
            newProduct.setDescriptionFour("");
            newProduct.setDescriptionFive("");
            newProduct.setDescriptionSix("");
            newProduct.setDescriptionSeven("");
            newProduct.setDescriptionEight("");
            newProduct.setDescriptionNine("");
            newProduct.setDescriptionTen("");
            if (strAmount.equals("")) {
                strAmount = "0.00";
                newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strAmount))));
            } else {
                newProduct.setProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strAmount))));
            }

            if (isProfitEnable) {
                newProduct.setBaseProductRate(convertDecimal.Currency(currency.format(Float.parseFloat(strBaseAmount))));
                newProduct.setBasePriceInclusiveOfGST(checkBasePriceTaxInclusive.isSelected());
            }
            newProduct.setInclusiveOfGST(checkTaxInclusive.isSelected());
            newProduct.setBasePriceInclusiveOfGST(checkBasePriceTaxInclusive.isSelected());
            newProduct.setUQC1(comboUQC1.getSelectedItem().toString());
            newProduct.setUQC2(comboUQC2.getSelectedItem().toString());
            newProduct.setUQC2Value(Integer.parseInt(strSecondValue));
            newProduct.setCurrentStock(getWholeStock);
//            newProduct.setNillRated(CheckNillRated.isSelected());
            newProduct.setNonGST(ChecknonGst.isSelected());
            newProduct.setExempted(CheckExempted.isSelected());

            newProduct.setService(isService);

            // String strStock = newProduct.getStockValue(getWholeStock);
            float stockAmount = 0;
//            String strStockAmount = txtStockAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockAmount.equals("")) {
//                stockAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockAmount)));
//            }
//
//            float stockIGSTAmount = 0;
//            String strStockIGSTAmount = txtStockIGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockIGSTAmount.equals("")) {
//                stockIGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockIGSTAmount)));
//            }
//            float stockCGSTAmount = 0;
//            String strStockCGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockCGSTAmount.equals("")) {
//                stockCGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockCGSTAmount)));
//            }
//            float stockSGSTAmount = 0;
//            String strStockSGSTAmount = txtStockSGSTAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strStockSGSTAmount.equals("")) {
//                stockSGSTAmount = Float.parseFloat(currency.format(Float.parseFloat(strStockSGSTAmount)));
//            }
            List<GoDownStockDetail> gsds = new ArrayList<GoDownStockDetail>();
            if (isInventoryEnabled) {

//            for (GoDown god : g4.fetchAllGodowns(company.getCompanyId())) {
                for (int i = 0; i < jTable1.getRowCount(); i++) {

                    try {
                        GoDownStockDetail gsd = new GoDownStockDetail();
                        
                        gsd.setGodown(getGodownById((Long) jTable1.getValueAt(i, 0)));

                        gsd.setOpeningStock(Long.parseLong(jTable1.getValueAt(i, 2).toString()));
//                gsd.setOpeningStockDate(new Date());

                        gsd.setOpeningStockDate(sdf.parse(jTable1.getValueAt(i, 3).toString()));
                        gsd.setOpeningStockValue(new BigDecimal(0));
                        gsds.add(gsd);
                    } catch (ParseException ex) {
                        Logger.getLogger(NewProductUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            ProductStockEntry newProductStock = new ProductStockEntry();

            newProductStock.setQuantity(getWholeStock);
            newProductStock.setProduct(newProduct);
            newProductStock.setStockValue(convertDecimal.Currency(currency.format(stockAmount)));
//            GodownStockEntryUI godownStockEntry = new GodownStockEntryUI();

//            newProductStock.setStockUpdateDate(dateStock.getDate());
//            newProductStock.setIgstPercentage(Float.parseFloat((String) comboStockIGST.getSelectedItem()));
//            newProductStock.setCgstPercentage(Float.parseFloat((String) comboStockCGST.getSelectedItem()));
//            newProductStock.setSgstPercentage(Float.parseFloat((String) comboStockSGST.getSelectedItem()));
//            newProductStock.setIgstAmount(convertDecimal.Currency(currency.format(stockIGSTAmount)));
//            newProductStock.setCgstAmount(convertDecimal.Currency(currency.format(stockCGSTAmount)));
//            newProductStock.setSgstAmount(convertDecimal.Currency(currency.format(stockSGSTAmount)));
            newProductStock.setStockUpdateType("OPENING");
            Boolean isSuccess;
            if (!SystemPolicyUtil.getSystemPolicy().isInventoryEnabled()) {
                isSuccess = new ProductLogic().createProduct(companyId, newProduct, null);
            } else {
                isSuccess = new ProductLogic().createProduct(companyId, newProduct, gsds);
            }

            if (isSuccess) { //Check this
                
                JOptionPane.showMessageDialog(null, strProductName + " is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                clear();
            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                clear();
            }
        }
    }

    private GoDown getGodownById(Long id) {
        GoDown godown = null;
        for (GoDown go : goDowns) {
            if (go.getGoDownId().toString().equals(go.getGoDownId().toString())) {
                godown = go;
                break;
            }
        }

        return godown;
    }

    public int getStock(int num) {
        int wholePic = 0, Pic = 0, convertValue = 0;

//        if (!txtStockUQC1Value.getText().equals("")) {
//            wholePic = Integer.parseInt(txtStockUQC1Value.getText());
//        }
//        if (!txtStockUQC2Value.getText().equals("")) {
//            Pic = Integer.parseInt(txtStockUQC2Value.getText());
//        }
        convertValue = (num * wholePic) + Pic;
        return convertValue;
    }

    public String getUQC2ValuefromProduct() {
        System.err.println("$$$$$$$$$ " + strSecondValue);
        return strSecondValue;
    }

    private void comboCGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboCGSTFocusGained
        comboCGST.showPopup();
    }//GEN-LAST:event_comboCGSTFocusGained

    private void comboSGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboSGSTFocusGained
        comboSGST.showPopup();
    }//GEN-LAST:event_comboSGSTFocusGained

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == java.awt.event.KeyEvent.VK_ENTER) {
            addProduct();
            evt.consume();
            comboGroup.requestFocus();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void comboUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUQC1ActionPerformed
        txtUQC2Value.setEnabled(false);
        txtBasePriceUQC2.setEnabled(false);
        labelBasePerUQC2.setEnabled(false);
        txtPriceUQC2.setEnabled(false);
        labelPerUQC2.setEnabled(false);
//        labelUQC1.setText((String) comboUQC1.getSelectedItem());
        labelPerUQC1.setText((String) comboUQC1.getSelectedItem());
        labelBasePerUQC1.setText((String) comboUQC1.getSelectedItem());
        JTableHeader th = jTable1.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();
        TableColumn tc = tcm.getColumn(6);
        tc.setHeaderValue((String) comboUQC1.getSelectedItem());
        th.repaint();


    }//GEN-LAST:event_comboUQC1ActionPerformed

    private void comboUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUQC2ActionPerformed
        if (comboUQC1.getSelectedItem() == comboUQC2.getSelectedItem()) {
            txtUQC2Value.setEnabled(false);
            txtBasePriceUQC2.setEnabled(false);
            labelBasePerUQC2.setEnabled(false);
            txtPriceUQC2.setEnabled(false);
            labelPerUQC2.setEnabled(false);
            txtUQC2Value.setText("1");
//            jTable1.getColumnModel().getColumn(7).setHeaderValue("");
//            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
        } else {
            txtUQC2Value.setEnabled(true);
            txtBasePriceUQC2.setEnabled(true);
            labelBasePerUQC2.setEnabled(true);
            txtPriceUQC2.setEnabled(true);
            labelPerUQC2.setEnabled(true);
            txtUQC2Value.setText("");
            JTableHeader th = jTable1.getTableHeader();
            TableColumnModel tcm = th.getColumnModel();
            TableColumn tc1 = tcm.getColumn(7);
            tc1.setHeaderValue((String) comboUQC2.getSelectedItem());
            tc1.setMaxWidth(50);
            tc1.setMinWidth(50);
            th.repaint();
        }
//        labelUQC2.setText((String) comboUQC2.getSelectedItem());
        labelPerUQC2.setText((String) comboUQC2.getSelectedItem());
        labelBasePerUQC2.setText((String) comboUQC2.getSelectedItem());


    }//GEN-LAST:event_comboUQC2ActionPerformed

    private void txtUQC2ValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2ValueKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtUQC2ValueKeyTyped

    private void comboIGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboIGSTFocusGained
        comboIGST.showPopup();
    }//GEN-LAST:event_comboIGSTFocusGained

    private void CheckExemptedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckExemptedActionPerformed
        if (CheckExempted.isSelected()) {
//            CheckNillRated.setSelected(false);
            ChecknonGst.setSelected(false);
        }
    }//GEN-LAST:event_CheckExemptedActionPerformed

    private void ChecknonGstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChecknonGstActionPerformed
        if (ChecknonGst.isSelected()) {
//            CheckNillRated.setSelected(false);
            CheckExempted.setSelected(false);
        }
    }//GEN-LAST:event_ChecknonGstActionPerformed


    private void comboGroupKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGroupKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtName.requestFocus();
        }
    }//GEN-LAST:event_comboGroupKeyPressed

    private void txtCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodeActionPerformed
        txtShortName.requestFocus();
    }//GEN-LAST:event_txtCodeActionPerformed


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    }//GEN-LAST:event_jButton1ActionPerformed

    private void checkBasePriceTaxInclusiveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveKeyReleased

    }//GEN-LAST:event_checkBasePriceTaxInclusiveKeyReleased

    private void checkBasePriceTaxInclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkBasePriceTaxInclusive.setSelected(true);
            checkBasePriceTaxExclusive.setSelected(false);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkBasePriceTaxInclusive.setSelected(false);
            checkBasePriceTaxExclusive.setSelected(true);
        } else if (vChar == KeyEvent.VK_ENTER) {
            txtPriceUQC1.requestFocus();
        }
    }//GEN-LAST:event_checkBasePriceTaxInclusiveKeyPressed

    private void checkBasePriceTaxInclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveActionPerformed
        txtBasePriceUQC1.requestFocus();
    }//GEN-LAST:event_checkBasePriceTaxInclusiveActionPerformed

    private void txtBasePriceUQC2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBasePriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBasePriceUQC2KeyTyped

    private void txtBasePriceUQC2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC2Price = Float.parseFloat(txtBasePriceUQC2.getText());
        float UQC1Price = UQC2Price * Value;
        txtBasePriceUQC1.setText(currency.format(UQC1Price));
    }//GEN-LAST:event_txtBasePriceUQC2KeyReleased

    private void txtBasePriceUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasePriceUQC2ActionPerformed
        checkBasePriceTaxInclusive.requestFocus();
    }//GEN-LAST:event_txtBasePriceUQC2ActionPerformed

    private void txtBasePriceUQC1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBasePriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBasePriceUQC1KeyTyped

    private void txtBasePriceUQC1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC1Price = Float.parseFloat(txtBasePriceUQC1.getText());
        float UQC2Price = UQC1Price / Value;
        txtBasePriceUQC2.setText(currency.format(UQC2Price));
    }//GEN-LAST:event_txtBasePriceUQC1KeyReleased

    private void txtBasePriceUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1ActionPerformed
//        txtBasePriceUQC2.requestFocus();
    }//GEN-LAST:event_txtBasePriceUQC1ActionPerformed

    private void txtPriceUQC2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC2KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPriceUQC2KeyTyped

    private void txtPriceUQC2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC2KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC2Price = Float.parseFloat(txtPriceUQC2.getText());
        float UQC1Price = UQC2Price * Value;
        txtPriceUQC1.setText(currency.format(UQC1Price));
    }//GEN-LAST:event_txtPriceUQC2KeyReleased

    private void txtPriceUQC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUQC2ActionPerformed
        checkTaxInclusive.requestFocus();
    }//GEN-LAST:event_txtPriceUQC2ActionPerformed

    private void txtPriceUQC1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPriceUQC1KeyTyped

    private void txtPriceUQC1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyReleased
        int Value = Integer.parseInt(txtUQC2Value.getText());
        float UQC1Price = Float.parseFloat(txtPriceUQC1.getText());
        float UQC2Price = UQC1Price / Value;
        txtPriceUQC2.setText(currency.format(UQC2Price));
    }//GEN-LAST:event_txtPriceUQC1KeyReleased

    private void txtPriceUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtBasePriceUQC2.isEnabled()) {
                checkTaxInclusive.requestFocus();
            } else {
                txtPriceUQC2.requestFocus();
            }
        }
    }//GEN-LAST:event_txtPriceUQC1KeyPressed

    private void txtPriceUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUQC1ActionPerformed
//        txtPriceUQC2.requestFocus();
    }//GEN-LAST:event_txtPriceUQC1ActionPerformed

    private void checkTaxInclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxInclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkTaxInclusive.setSelected(true);
            checkTaxExclusive.setSelected(false);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkTaxInclusive.setSelected(false);
            checkTaxExclusive.setSelected(true);
        } else if (vChar == KeyEvent.VK_ENTER) {
            comboIGST.requestFocus();
        }
    }//GEN-LAST:event_checkTaxInclusiveKeyPressed

    private void checkTaxInclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTaxInclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxInclusiveActionPerformed

    private void txtBasePriceUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBasePriceUQC1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtBasePriceUQC2.isEnabled()) {
                checkBasePriceTaxInclusive.requestFocus();
            } else {
                txtBasePriceUQC2.requestFocus();
            }
        }
    }//GEN-LAST:event_txtBasePriceUQC1KeyPressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        int uqc1quantity = 0;
        int uqc2quantity = 0;
        int uqc2value = 0;
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                uqc1quantity = Integer.parseInt(jTable1.getValueAt(i, 6).toString());
                if (labelBasePerUQC1 == labelBasePerUQC2) {
                    uqc2quantity = 0;
                } else {
                    uqc2quantity = Integer.parseInt("0" + jTable1.getValueAt(i, 7).toString());
                }
                uqc2value = Integer.parseInt(jTable1.getValueAt(i, 8).toString());
                jTable1.setValueAt(((uqc1quantity * uqc2value) + uqc2quantity), i, 2);
            }
        }
        
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyTyped
        int row = jTable1.getSelectedRow();
        int column = jTable1.getSelectedColumn();
        
    }//GEN-LAST:event_jTable1KeyTyped

    private void comboUQC2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboUQC2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_comboUQC2MouseClicked

    private void checkBasePriceTaxExclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkBasePriceTaxExclusiveActionPerformed

    private void checkBasePriceTaxInclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkBasePriceTaxInclusiveItemStateChanged
        checkBasePriceTaxExclusive.setSelected(!checkBasePriceTaxInclusive.isSelected());
    }//GEN-LAST:event_checkBasePriceTaxInclusiveItemStateChanged

    private void checkTaxExclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxExclusiveItemStateChanged
          checkTaxInclusive.setSelected(!checkTaxExclusive.isSelected());
    }//GEN-LAST:event_checkTaxExclusiveItemStateChanged

    private void checkBasePriceTaxExclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveItemStateChanged
        checkBasePriceTaxInclusive.setSelected(!checkBasePriceTaxExclusive.isSelected());
    }//GEN-LAST:event_checkBasePriceTaxExclusiveItemStateChanged

    private void checkBasePriceTaxExclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkBasePriceTaxExclusiveKeyPressed
         char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkBasePriceTaxInclusive.setSelected(false);
            checkBasePriceTaxExclusive.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkBasePriceTaxInclusive.setSelected(true);
            checkBasePriceTaxExclusive.setSelected(false);
        } else if (vChar == KeyEvent.VK_ENTER) {
            checkBasePriceTaxInclusive.requestFocus();
        }
    }//GEN-LAST:event_checkBasePriceTaxExclusiveKeyPressed

    private void checkTaxExclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxExclusiveKeyPressed
          char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkTaxInclusive.setSelected(false);
            checkTaxExclusive.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkTaxInclusive.setSelected(true);
            checkTaxExclusive.setSelected(false);
        } else if (vChar == KeyEvent.VK_ENTER) {
            checkTaxInclusive.requestFocus();
        }
    }//GEN-LAST:event_checkTaxExclusiveKeyPressed

    private void checkTaxInclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxInclusiveItemStateChanged
        checkTaxExclusive.setSelected(!checkTaxInclusive.isSelected());
    }//GEN-LAST:event_checkTaxInclusiveItemStateChanged

    public void clearWithoutSearchBox() {
        txtHsncode.setText("");
        txtName.setText("");
        txtPriceUQC1.setText("");
        txtPriceUQC2.setText("");
        txtUQC2Value.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);
        comboUQC1.setSelectedIndex(0);
        comboUQC2.setSelectedIndex(0);
        btnAdd.setEnabled(companyUserRole.isProductCreate());

        comboUQC1.setEnabled(true);
        txtUQC2Value.setEnabled(true);
        txtBasePriceUQC2.setEnabled(true);
        labelBasePerUQC2.setEnabled(true);
        txtPriceUQC2.setEnabled(true);
        labelPerUQC2.setEnabled(true);
        comboUQC2.setEnabled(true);
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
    }

    public void clear() {
        txtHsncode.setText("");
        txtName.setText("");
        txtBasePriceUQC1.setText("");
        txtBasePriceUQC2.setText("");
        txtPriceUQC1.setText("");
        txtPriceUQC2.setText("");
        txtUQC2Value.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
        checkBasePriceTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);
        comboUQC1.setSelectedIndex(0);
        comboUQC2.setSelectedIndex(0);

        btnAdd.setEnabled(true);
        btnAdd.setEnabled(companyUserRole.isProductCreate());
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
        comboGroup.setSelectedIndex(0);
        txtCode.setText("");

        comboUQC1.setEnabled(true);
        txtUQC2Value.setEnabled(true);
        txtBasePriceUQC2.setEnabled(true);
        labelBasePerUQC2.setEnabled(true);
        txtPriceUQC2.setEnabled(true);
        labelPerUQC2.setEnabled(true);
        comboUQC2.setEnabled(true);
//        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
//        if (model.getRowCount() > 0) {
//            model.setRowCount(0);
//        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewProductUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewProductUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckExempted;
    private javax.swing.JCheckBox ChecknonGst;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JCheckBox checkBasePriceTaxExclusive;
    private javax.swing.JCheckBox checkBasePriceTaxInclusive;
    private javax.swing.JCheckBox checkTaxExclusive;
    private javax.swing.JCheckBox checkTaxInclusive;
    private javax.swing.JComboBox<String> comboCGST;
    private javax.swing.JComboBox<String> comboGroup;
    private javax.swing.JComboBox<String> comboIGST;
    private javax.swing.JComboBox<String> comboSGST;
    private javax.swing.JComboBox<String> comboUQC1;
    private javax.swing.JComboBox<String> comboUQC2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelBasePerUQC1;
    private javax.swing.JLabel labelBasePerUQC2;
    private javax.swing.JLabel labelBasePrice;
    private javax.swing.JLabel labelPerUQC1;
    private javax.swing.JLabel labelPerUQC2;
    private javax.swing.JTextField txtBasePriceUQC1;
    private javax.swing.JTextField txtBasePriceUQC2;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtHsncode;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPriceUQC1;
    private javax.swing.JTextField txtPriceUQC2;
    private javax.swing.JTextField txtShortName;
    private javax.swing.JTextField txtTamilName;
    private javax.swing.JTextField txtUQC2Value;
    // End of variables declaration//GEN-END:variables
}
