/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.business.PurchaseLogic;
import india.abs.gstonepro.pdf.PurchaseBill;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.JTextComponent;

/**
 *
 * @author ABS
 */
public class PurchaseUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();

    DecimalFormat currency = new DecimalFormat("0.000");
    String CompanyState = SessionDataUtil.getSelectedCompany().getState();

    String HSNCode;
    boolean isIGST = false, isProductTaxInclusive = false, isUnregistered = false;
    int lastBillId;
    Long LedgerId;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0;
    float amountWithoutDiscount = 0, amount = 0, discount = 0, discountPer = 0;
    float IGST = 0, CGST = 0, SGST = 0, price = 0;

    Boolean isEditBill = false;
    Boolean isOldBill = false;

    JButton btnFocus = null;
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Ledger ledgerData = new Ledger();
    Product product = new Product();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPurchaseLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(company.getCompanyId());

    List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(company.getCompanyId());
    List<Product> products = new ProductLogic().fetchAllProducts(company.getCompanyId());
    PurchaseInvoice oldSale = new PurchaseInvoice();
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();
    Boolean isActive = false;

    /**
     * Creates new form Bill
     */
    public PurchaseUI() {
        try {
            initComponents();
            setMenuRoles();
            comboProduct.setEnabled(false);
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();

            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            totalTable.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                @Override
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });
            Collections.sort(products, new Comparator<Product>() {
                @Override
                public int compare(Product o1, Product o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            this.setExtendedState(this.MAXIMIZED_BOTH);
            labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            dateBill.setDate(null);
            setParty();
            setProduct();;
            setIGST();
            setGodown();
            saveData();
            getbtnAddFocus();
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);

            labelPartyName.setText("");
            labelAddress.setText("");
            labelGSTIN.setText("");

            txtSGSTamt.setForeground(Color.RED);
            txtIGSTamt.setForeground(Color.RED);
            txtCGSTamt.setForeground(Color.RED);
            comboParty.requestFocus();
            KeyStroke ctrlTab = KeyStroke.getKeyStroke("ctrl TAB");
            KeyStroke ctrlShiftTab = KeyStroke.getKeyStroke("ctrl shift TAB");

            KeyStroke right = KeyStroke.getKeyStroke("alt RIGHT");
            KeyStroke left = KeyStroke.getKeyStroke("alt LEFT");

//         Remove ctrl-tab from normal focus traversal
            Set<AWTKeyStroke> forwardKeys = new HashSet<AWTKeyStroke>(jTabbedPane1.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
            forwardKeys.remove(ctrlTab);
            jTabbedPane1.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, forwardKeys);

// Remove ctrl-shift-tab from normal focus traversal
            Set<AWTKeyStroke> backwardKeys = new HashSet<>(jTabbedPane1.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
            backwardKeys.remove(ctrlShiftTab);
            jTabbedPane1.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, backwardKeys);

//         Add keys to the tab's input map
            InputMap inputMap = jTabbedPane1.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            inputMap.put(right, "navigateNext");
            inputMap.put(left, "navigatePrevious");

//      Manually Enter Date
            ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    jTabbedPane1.setSelectedIndex(1);
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                isActive = false;
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");

                            }

                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(PurchaseUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
            editor.addKeyListener(new KeyAdapter() {
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboProduct.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            for (Product product : products) {
                                if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(product.getName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboProduct.setModel(model);
                            comboProduct.getEditor().setItem(val);
                            comboProduct.setPopupVisible(true);
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();

                        for (Product product : products) {
                            scripts.add(product.getName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem("");
                        comboProduct.setPopupVisible(true);
                    } else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboProduct.getEditor().setItem(selectedObj);
                        comboProduct.setPopupVisible(false);
                        getProductDetails();
                        if (!product.isService()) {
                            txtUQC1Qty.requestFocus();
                        } else {
                            txtTotalAmt.requestFocus();
                        }
                    }
                }
            });
//            JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
//            editor.addKeyListener(new KeyAdapter() {
//                @Override
//                public void keyReleased(java.awt.event.KeyEvent evt) {
//                    if (isActive) {
//                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
//                            String val = (String) comboProduct.getEditor().getItem();
//                            ArrayList<String> scripts = new ArrayList<>();
//
//                            for (Product product : products) {
//                                if (product.getName().toLowerCase().contains(val.toLowerCase())) {
//                                    scripts.add(product.getName());
//                                }
//                            }
//                            String[] myArray = new String[scripts.size()];
//                            scripts.toArray(myArray);
//                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
//                            comboProduct.setModel(model);
//                            if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
//                                comboProduct.setPopupVisible(false);
//                                comboProduct.setSelectedItem(comboProduct.getSelectedItem());
//                                getProductDetails();
//                                txtUQC1Qty.requestFocus();
//                            } else {
//                                comboProduct.getEditor().setItem(val);
//                                comboProduct.setPopupVisible(true);
//                            }
//                        }
//                    } else {
//                        isActive = true;
//                        ArrayList<String> scripts = new ArrayList<>();
//                        for (Product product : products) {
//                            scripts.add(product.getName());
//                        }
//                        String[] myArray = new String[scripts.size()];
//                        scripts.toArray(myArray);
//                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
//                        comboProduct.setModel(model);
//                        comboProduct.getEditor().setItem("");
//                        comboProduct.setPopupVisible(true);
//                    }
//                }
//            });

            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();

            editor1.addKeyListener(
                    new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt
                ) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboParty.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();

                        for (Ledger ledger : sortedLedgers) {
                            if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(ledger.getLedgerName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);
                        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                            comboParty.setPopupVisible(false);
                            comboParty.setSelectedItem(comboParty.getSelectedItem());
                            getPartyDetails();
                            comboProduct.setEnabled(true);
                            txtBillNo.requestFocus();
                        } else {
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(PurchaseUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(((DefaultTableModel) tableProductPurchaseDetails.getModel()));
        tableProductPurchaseDetails.setRowSorter(sorter);
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

//    private void setBanks() {
//        for (Ledger bank : Banks) {
//            comboBank.addItem(bank.getLedgerName());
//        };
//    }
//
//    private void checkRadioButton() {
//        comboBank.setEnabled(radioBank.isSelected());
//    }
    public void EditOldBill(String id) {
        setPartyEditable(false);
        comboProduct.setEnabled(true);
        isOldBill = true;
        oldSale = new PurchaseLogic().fetchAPurchaseInvoice(id);
        dateBill.setDate(oldSale.getPurchaseInvoiceDate());
        List<PurchaseInvoiceLineItem> psLIs = oldSale.getPiLineItems();
//        if (!oldSale.getTotalAmountPaid().equals(null)) {
//            txtAmountPaid.setText(oldSale.getTotalAmountPaid().toString());
//        } else {
//            txtAmountPaid.setText("0");
//        }

        labelPartyName.setText(oldSale.getLedger().getLedgerName());
        labelGSTIN.setText(oldSale.getLedger().getGSTIN());
        LedgerId = oldSale.getLedger().getLedgerId();
        comboParty.setSelectedItem(oldSale.getLedger().getLedgerName());
        txtCompanyName.setText(oldSale.getLedger().getLedgerName());
        String address = oldSale.getLedger().getAddress();
        String[] words = address.split("/~/");
        txtBillNo.setText(oldSale.getSupplierInvoiceNo());
        if (words.length == 1) {
            txtAddr1.setText(words[0]);
            txtAddr2.setText("");
            txtAddr3.setText("");
            labelAddress.setText(words[0]);
        } else if (words.length == 2) {
            txtAddr1.setText(words[0]);
            txtAddr2.setText(words[1]);
            txtAddr3.setText("");
            labelAddress.setText(words[0] + "  " + words[1]);
        } else {
            txtAddr1.setText(words[0]);
            txtAddr2.setText(words[1]);
            txtAddr3.setText(words[2]);
            labelAddress.setText(words[0] + "  " + words[1] + "  " + words[2]);
        }
        txtCity.setText(oldSale.getLedger().getCity());
        txtDistrict.setText(oldSale.getLedger().getDistrict());
        txtState.setText(oldSale.getLedger().getState());
        txtEmail.setText(oldSale.getLedger().getEmail());
        txtMobile.setText(oldSale.getLedger().getMobile());
        txtPhone.setText(oldSale.getLedger().getPhone());
        txtGSTIN.setText(oldSale.getLedger().getGSTIN());
        isIGST = oldSale.isIsIGST();
        txtEwayBillNo.setText(oldSale.geteWayBillNo());
        txtEwayRemark.setText(oldSale.geteWayBillRemarks());
        txtBillNo.setText(oldSale.getSupplierInvoiceNo());
        dateBill.setDate(oldSale.getPurchaseInvoiceDate());
        txtRemarks.setText(oldSale.getRemarks());
        discountPer = oldSale.getDiscountPercent();

        if (isIGST) {
            chkboxIGST.setSelected(true);
        }

        DefaultTableModel modelTotal = (DefaultTableModel) totalTable.getModel();
        if (modelTotal.getRowCount() > 0) {
            modelTotal.setRowCount(0);
        }

        String strBillDiscount = "";
        if (oldSale.getDiscountAmount() == null) {
            strBillDiscount = "0.0";
        } else {
            strBillDiscount = oldSale.getDiscountAmount().toString();
        }
        txtBillDiscount.setText(strBillDiscount);
        float fIgstVal = oldSale.getIgstValue().floatValue();
        float fCgstVal = oldSale.getCgstValue().floatValue();
        float fSgstVal = oldSale.getSgstValue().floatValue();
        float fCessAmt = oldSale.getCessValue().floatValue();
        float fTotal = oldSale.getCalculatedPurchaseInvoiceAmount().floatValue();
        float DiscountAmt = Float.parseFloat(strBillDiscount);
        Object[] rowtotal = {oldSale.getPuchaseInvoiceAmount().floatValue(),
            fIgstVal,
            fCgstVal,
            fSgstVal,
            fCessAmt,
            fTotal
        };

        amountWithoutDiscount = fTotal + DiscountAmt;
        amount = fTotal;
        labelBillAmount.setText(fTotal + "");

        modelTotal.addRow(rowtotal);

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        String strGodown = "";
        for (PurchaseInvoiceLineItem item : psLIs) {
            if (isInventoryEnabled) {
                strGodown = item.getGodownStockEntry().getGodown().getGoDownName();
            }
            Object[] row = {
                item.getLineNumber(),
                item.getProductName(),
                item.getHsnSac(),
                item.getUqcOne(),
                item.getUqcOneQuantity(),
                item.getUqcOneRate(),
                (new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate())).floatValue(),
                item.getUqcTwo(),
                item.getUqcTwoQuantity(),
                item.getUqcTwoRate(),
                (new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate())).floatValue(),
                item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                item.getStockQuantity(),
                item.getDiscountPercent(),
                item.getDiscountValue(),
                item.getValue().floatValue(),
                item.getIgstPercentage(),
                item.getIgstValue().floatValue(),
                item.getCgstPercentage(),
                item.getCgstValue().floatValue(),
                item.getSgstPercentage(),
                item.getSgstValue().floatValue(),
                item.getCessAmount(),
                item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue()).add(item.getCessAmount()))).floatValue(),
                strGodown,
                item.getPiLiId(),
                item.getITCEligible(),
                item.getItcValueforIGST(),
                item.getItcValueforCGST(),
                item.getItcValueforSGST(),
                item.getItcValueforCess(),
                item.getTotalAmountWithTax().floatValue()
            };
            model.addRow(row);
        }
        clear();
        jTableRender();
        comboProduct.requestFocus();
    }

    public void getProductDetails() {
        String productName = (String) comboProduct.getEditor().getItem();
        DefaultTableModel model3 = (DefaultTableModel) tableProductPurchaseDetails.getModel();
        if (model3.getRowCount() > 0) {
            model3.removeRow(0);
        }

        if ((productName != null) && (!(productName.equals("")))) {
            for (Product selProduct : products) {
                if (selProduct.getName().equals(productName)) {
                    product = selProduct;
                    fetchProductPurchaseDetails();
                    if (product.getUQC1().equals(product.getUQC2())) {
                        labelUQC1.setText(product.getUQC1());
                        labelUQC2.setText("");
                        txtUQC2Qty.setEnabled(false);
                    } else {
                        labelUQC1.setText(product.getUQC1());
                        labelUQC2.setText(product.getUQC2());
                        txtUQC2Qty.setEnabled(true);
                    }

                    HSNCode = product.getHsnCode();
                    UQC2Value = product.getUQC2Value();

                    ProductRate = product.getBaseProductRate().floatValue();

                    IGST = product.getIgstPercentage();
                    CGST = product.getCgstPercentage();
                    SGST = product.getSgstPercentage();
                    txtIGSTper.setText(String.valueOf(product.getIgstPercentage()));
                    txtCGSTper.setText(String.valueOf(product.getCgstPercentage()));
                    txtSGSTper.setText(String.valueOf(product.getSgstPercentage()));
                    isProductTaxInclusive = product.isBasePriceInclusiveOfGST();
                    price = 0;
                    if (isProductTaxInclusive) {
                        price = (ProductRate / (100 + IGST)) * 100;
                    } else {
                        price = ProductRate;
                    }
//                    txtUQC1Rate.setText(currency.format(price * UQC2Value));
                    txtUQC1Qty.requestFocus();
                }
            }
        } else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtTotalAmt.setText("");
            txtAmount.setText("");
            txtIGSTper.setText("");
            txtCGSTper.setText("");
            txtSGSTper.setText("");
            txtIGSTamt.setText("");
            txtCGSTamt.setText("");
            txtSGSTamt.setText("");
            comboProduct.setSelectedItem("");
            comboProduct.setPopupVisible(false);
            comboProduct.requestFocus();

        }
    }

    public void fetchProductPurchaseDetails() {
        List<PurchaseInvoiceLineItem> pLiItems = new PurchaseLogic().getAllPurchaseByProduct(companyPolicy.getFinancialYearStart(), companyPolicy.getFinancialYearEnd(), product);
        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
        DefaultTableModel model = (DefaultTableModel) tableProductPurchaseDetails.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (PurchaseInvoiceLineItem pLItem : pLiItems) {
            int quantity = pLItem.getStockQuantity();
            int UQC2Value = product.getUQC2Value();
            float OneQtyRate = pLItem.getValue().floatValue() / quantity;
            String UQC1Rate = currency.format(pLItem.getUqcOneQuantity() * UQC2Value * OneQtyRate);
            String UQC2Rate = currency.format(pLItem.getUqcTwoQuantity() * OneQtyRate);

            String strDate = sft.format(pLItem.getPurchaseInvoice().getPurchaseInvoiceDate());
            Object[] row = {
                pLItem.getPiLiId(),
                strDate,
                pLItem.getPurchaseInvoice().getPurchaseInvoiceNo(),
                pLItem.getPurchaseInvoice().getLedger().getLedgerName(),
                UQC1Rate,
                UQC2Rate,};
            model.addRow(row);
        }

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(txtAmount.RIGHT);
        TableColumnModel columnModel = tableProductPurchaseDetails.getColumnModel();
        columnModel.getColumn(4).setHeaderValue("Rate per " + product.getUQC1());
        columnModel.getColumn(5).setHeaderValue("Rate per " + product.getUQC2());
        columnModel.getColumn(4).setCellRenderer(rightRenderer);
        columnModel.getColumn(5).setCellRenderer(rightRenderer);
    }

    private void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    private void setParty() {
        for (Ledger ledger : sortedLedgers) {
            comboParty.addItem(ledger.getLedgerName());
        };
        comboParty.setSelectedItem("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtAddr2 = new javax.swing.JTextField();
        txtAddr3 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jScrollPane3 = new javax.swing.JScrollPane();
        totalTable = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        jLabel1 = new javax.swing.JLabel();
        txtBillNo = new javax.swing.JTextField();
        chkboxIGST = new javax.swing.JCheckBox();
        chkComboUser = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtEwayBillNo = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtEwayRemark = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtCompanyName = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtAddr1 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        txtState = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        txtMobile = new javax.swing.JTextField();
        txtPhone = new javax.swing.JTextField();
        txtGSTIN = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtRemarks = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        comboProduct = new javax.swing.JComboBox<>();
        labelProduct = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        txtTotalAmt = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        lblTotalWithTax = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtIGSTper = new javax.swing.JLabel();
        lblIGST1 = new javax.swing.JLabel();
        txtIGSTcalcamt = new javax.swing.JLabel();
        txtCGSTcalcamt = new javax.swing.JLabel();
        txtSGSTcalcamt = new javax.swing.JLabel();
        lblSGST1 = new javax.swing.JLabel();
        lblCGST1 = new javax.swing.JLabel();
        txtCGSTper = new javax.swing.JLabel();
        txtSGSTper = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnPrint = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        lblGodown = new javax.swing.JLabel();
        comboGodown = new javax.swing.JComboBox<>();
        jLabel33 = new javax.swing.JLabel();
        txtBillDiscount = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        labelBillAmount = new javax.swing.JLabel();
        txtCGSTamt = new javax.swing.JTextField();
        lblCGST = new javax.swing.JLabel();
        lblIGST = new javax.swing.JLabel();
        txtIGSTamt = new javax.swing.JTextField();
        lblSGST = new javax.swing.JLabel();
        txtSGSTamt = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtCess = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        txtITCIGST1 = new javax.swing.JTextField();
        lblITCIGST1 = new javax.swing.JLabel();
        lblITCCGST1 = new javax.swing.JLabel();
        txtITCCGST1 = new javax.swing.JTextField();
        lblITCSGST1 = new javax.swing.JLabel();
        txtITCSGST1 = new javax.swing.JTextField();
        lblITCCess1 = new javax.swing.JLabel();
        txtITCCess1 = new javax.swing.JTextField();
        comboITC = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtTotalWithTax = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableProductPurchaseDetails = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        labelPartyName = new javax.swing.JLabel();
        labelAddress = new javax.swing.JLabel();
        labelGSTIN = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Purchase");

        jTabbedPane1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        txtAddr2.setEditable(false);
        txtAddr2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr2ActionPerformed(evt);
            }
        });

        txtAddr3.setEditable(false);
        txtAddr3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr3ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Date :");

        dateBill.setDateFormatString("dd-MM-yyyy");
        dateBill.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateBillKeyPressed(evt);
            }
        });

        totalTable.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        totalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Total Amount", "IGST", "CGST", "SGST ", "Cess Amount", "Total Amount (Incl.Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        totalTable.getTableHeader().setResizingAllowed(false);
        totalTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(totalTable);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("Bill Number");

        txtBillNo.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtBillNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillNoActionPerformed(evt);
            }
        });
        txtBillNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillNoKeyPressed(evt);
            }
        });

        chkboxIGST.setText("is IGST ");
        chkboxIGST.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                chkboxIGSTStateChanged(evt);
            }
        });

        chkComboUser.setText("Composition Scheme Supplier");

        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(180, 180, 180)));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jLabel10.setText("E-Way Bill");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("Bill No:");

        jLabel32.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel32.setText("Remarks");

        txtEwayBillNo.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        txtEwayRemark.setColumns(20);
        txtEwayRemark.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        txtEwayRemark.setRows(5);
        jScrollPane4.setViewportView(txtEwayRemark);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEwayBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtEwayBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Party ");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPartyActionPerformed(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        txtCompanyName.setEditable(false);
        txtCompanyName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCompanyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCompanyNameActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtAddr1.setEditable(false);
        txtAddr1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr1ActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel20.setText("City");

        txtCity.setEditable(false);
        txtCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel25.setText("District");

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        txtDistrict.setEditable(false);
        txtDistrict.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });

        txtState.setEditable(false);
        txtState.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        txtEmail.setEditable(false);
        txtEmail.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        txtMobile.setEditable(false);
        txtMobile.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileActionPerformed(evt);
            }
        });

        txtPhone.setEditable(false);
        txtPhone.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhoneActionPerformed(evt);
            }
        });

        txtGSTIN.setEditable(false);
        txtGSTIN.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel21.setText("GSTIN/UIN");

        jLabel24.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel24.setText("Phone Number");

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("Mobile Number");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("Email");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel8.setText("Remarks");

        txtRemarks.setColumns(20);
        txtRemarks.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtRemarks.setRows(5);
        jScrollPane2.setViewportView(txtRemarks);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(17, 17, 17)
                            .addComponent(jScrollPane2))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtMobile, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                                    .addComponent(txtPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtGSTIN, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtState, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtDistrict)))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtCompanyName))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(40, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(comboParty, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabel20, jLabel21, jLabel24, jLabel25, jLabel26, jLabel6, jLabel8});

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jScrollPane2, txtAddr1, txtCity, txtCompanyName, txtDistrict, txtEmail, txtGSTIN, txtMobile, txtPhone, txtState});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(14, 14, 14)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {comboParty, txtAddr1, txtCity, txtCompanyName, txtDistrict, txtEmail, txtGSTIN, txtMobile, txtPhone, txtState});

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabel20, jLabel21, jLabel24, jLabel25, jLabel26, jLabel6, jLabel8});

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(chkComboUser)
                                    .addGap(50, 50, 50)
                                    .addComponent(chkboxIGST, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addGap(97, 97, 97)
                            .addComponent(jLabel1)
                            .addGap(5, 5, 5)
                            .addComponent(txtBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(25, 25, 25))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(179, 179, 179)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtAddr2, txtAddr3});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel1))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chkComboUser)
                            .addComponent(chkboxIGST))
                        .addGap(15, 15, 15)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(112, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 970, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(115, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Party Address", jPanel1);

        jPanel2.setPreferredSize(new java.awt.Dimension(1400, 549));

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboProductItemStateChanged(evt);
            }
        });
        comboProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboProductFocusGained(evt);
            }
        });
        comboProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductActionPerformed(evt);
            }
        });

        labelProduct.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelProduct.setText("Product ");
        labelProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                labelProductKeyReleased(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                btnAddAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear ");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel35.setText("Quantity");

        txtTotalAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalAmtActionPerformed(evt);
            }
        });
        txtTotalAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTotalAmtKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalAmtKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTotalAmtKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("Total Price");

        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Discount %");

        txtDiscountPer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDiscountPerFocusGained(evt);
            }
        });
        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel27.setText("Discount");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Discount %", "Discount", "Amount", "IGST %", "IGST", "CGST %", "CGST", "SGST %", "SGST", "Cess Amount", "Amount (Incl.Tax)", "godown", "piLiId", "ITC", "ITC IGST", "ITC CGST", "ITC SGCT", "ITC Cess", "TotalAmountWithTax"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.String.class, java.lang.Long.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(40);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(40);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(40);
            jTable1.getColumnModel().getColumn(1).setMinWidth(350);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(350);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(350);
            jTable1.getColumnModel().getColumn(2).setMinWidth(0);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(0);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(50);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(18).setMinWidth(50);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(20).setMinWidth(50);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(22).setMinWidth(80);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(80);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(80);
            jTable1.getColumnModel().getColumn(23).setMinWidth(100);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(25).setMinWidth(0);
            jTable1.getColumnModel().getColumn(25).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(25).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(26).setMinWidth(0);
            jTable1.getColumnModel().getColumn(26).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(26).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(27).setMinWidth(0);
            jTable1.getColumnModel().getColumn(27).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(27).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(28).setMinWidth(0);
            jTable1.getColumnModel().getColumn(28).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(28).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(29).setMinWidth(0);
            jTable1.getColumnModel().getColumn(29).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(29).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(30).setMinWidth(0);
            jTable1.getColumnModel().getColumn(30).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(30).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(31).setMinWidth(0);
            jTable1.getColumnModel().getColumn(31).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(31).setMaxWidth(0);
        }

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setText("UQC2");

        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setText("UQC1");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("Total Price");

        txtAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel23.setText("Total Price (Incl of Tax)");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("IGST %");

        txtIGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        lblIGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblIGST1.setText("IGST");

        lblSGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblSGST1.setText("SGST");
        lblSGST1.setAlignmentX(1.0F);
        lblSGST1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        lblSGST1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblCGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblCGST1.setText("CGST ");

        txtCGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        txtSGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel11.setText("SGST %");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel9.setText("CGST %");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(txtIGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(lblIGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txtIGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addComponent(lblSGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addComponent(lblCGST1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(txtIGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSGST1, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                            .addComponent(txtCGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                            .addComponent(txtSGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblIGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel11)
                        .addComponent(txtSGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblCGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCGSTcalcamt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel11, jLabel7, jLabel9, lblSGST1, txtCGSTper, txtIGSTper, txtSGSTper});

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblTotalWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotalWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnPrint.setMnemonic('p');
        btnPrint.setText("Save & Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        lblGodown.setText("Godown");

        comboGodown.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGodownKeyPressed(evt);
            }
        });

        jLabel33.setText("Bill Discount");

        txtBillDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillDiscountActionPerformed(evt);
            }
        });
        txtBillDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyTyped(evt);
            }
        });

        jLabel18.setText("Bill Amount");

        txtCGSTamt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCGSTamtActionPerformed(evt);
            }
        });
        txtCGSTamt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCGSTamtKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCGSTamtKeyReleased(evt);
            }
        });

        lblCGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblCGST.setText("CGST ");
        lblCGST.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        lblIGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblIGST.setText("IGST");
        lblIGST.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        txtIGSTamt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIGSTamtActionPerformed(evt);
            }
        });
        txtIGSTamt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIGSTamtKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtIGSTamtKeyReleased(evt);
            }
        });

        lblSGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblSGST.setText("SGST");
        lblSGST.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        lblSGST.setAlignmentX(1.0F);
        lblSGST.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        lblSGST.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        txtSGSTamt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSGSTamtKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSGSTamtKeyReleased(evt);
            }
        });

        jLabel29.setText("Cess");
        jLabel29.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        txtCess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCessActionPerformed(evt);
            }
        });
        txtCess.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCessKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCessKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCessKeyTyped(evt);
            }
        });

        txtITCIGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtITCIGST1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtITCIGST1ActionPerformed(evt);
            }
        });

        lblITCIGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblITCIGST1.setText("ITC IGST");

        lblITCCGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblITCCGST1.setText("ITC CGST");

        txtITCCGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtITCCGST1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtITCCGST1ActionPerformed(evt);
            }
        });

        lblITCSGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblITCSGST1.setText("ITC SGCT");

        txtITCSGST1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtITCSGST1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtITCSGST1ActionPerformed(evt);
            }
        });

        lblITCCess1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblITCCess1.setText("ITC Cess");

        txtITCCess1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtITCCess1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtITCCess1ActionPerformed(evt);
            }
        });
        txtITCCess1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtITCCess1KeyPressed(evt);
            }
        });

        comboITC.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ineligible", "Capital goods", "Inputs", "Input services" }));
        comboITC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboITCActionPerformed(evt);
            }
        });
        comboITC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboITCKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboITC, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblITCCGST1)
                                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblITCCess1)
                                        .addComponent(lblITCSGST1)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtITCCess1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtITCSGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtITCCGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(lblITCIGST1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtITCIGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtITCCGST1, txtITCCess1, txtITCIGST1, txtITCSGST1});

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblITCCGST1, lblITCCess1, lblITCIGST1, lblITCSGST1});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(comboITC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblITCIGST1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtITCIGST1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblITCCGST1)
                    .addComponent(txtITCCGST1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblITCSGST1)
                    .addComponent(txtITCSGST1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblITCCess1)
                    .addComponent(txtITCCess1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel4.setText("Total Price  (Incl of Tax)");

        txtTotalWithTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalWithTaxActionPerformed(evt);
            }
        });
        txtTotalWithTax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTotalWithTaxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalWithTaxKeyReleased(evt);
            }
        });

        jPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(180, 180, 180)));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        tableProductPurchaseDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Purchase Id", "Date", "Bill", "Party Name", "Rate", "Rate"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tableProductPurchaseDetails);
        if (tableProductPurchaseDetails.getColumnModel().getColumnCount() > 0) {
            tableProductPurchaseDetails.getColumnModel().getColumn(0).setMinWidth(0);
            tableProductPurchaseDetails.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableProductPurchaseDetails.getColumnModel().getColumn(0).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(774, 774, 774)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelProduct, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(11, 11, 11)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
                                            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addComponent(txtTotalAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtCess, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(115, 115, 115)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblCGST, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                                            .addComponent(lblIGST, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblSGST, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtIGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotalWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(lblGodown)
                                        .addGap(18, 18, 18)
                                        .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(19, 19, 19)
                                .addComponent(jScrollPane5)
                                .addGap(18, 18, 18)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(64, 64, 64))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(16, 16, 16))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelUQC1, labelUQC2});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtTotalAmt, txtUQC1Qty, txtUQC2Qty});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel3, jLabel33, jLabel35, labelProduct});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblCGST, lblIGST, lblSGST});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtCess, txtDiscount, txtDiscountPer});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5)
                                .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblIGST, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtIGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel35)
                                .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblCGST, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtSGSTamt)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lblSGST, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(8, 8, 8)))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtTotalWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(labelUQC2)
                                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtCess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtTotalAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblGodown))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelUQC1, labelUQC2});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {comboProduct, txtTotalAmt, txtUQC1Qty, txtUQC2Qty});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCess, txtDiscount, txtDiscountPer});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel3, jLabel33, jLabel35, labelProduct});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel27, jLabel29, jLabel5});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCGSTamt, txtIGSTamt, txtSGSTamt});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblCGST, lblIGST, lblSGST});

        jTabbedPane1.addTab("Product Details", jPanel2);

        jLabel19.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel19.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel22.setText("User :");

        labelPartyName.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelPartyName.setText("Party");

        labelAddress.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelAddress.setText("Address");

        labelGSTIN.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelGSTIN.setText("GSTIN");

        jLabel28.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel28.setText("PURCHASE");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 107, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(labelPartyName)
                        .addComponent(labelAddress)
                        .addComponent(labelGSTIN)
                        .addComponent(jLabel28))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 102, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelPartyName)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelAddress)
                    .addGap(2, 2, 2)
                    .addComponent(labelGSTIN)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1202, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel19)
                                .addComponent(labelCompanyName))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelUserName)
                                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel22)
                                .addComponent(jButton1)))
                        .addGap(66, 66, 66)
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCompanyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCompanyNameActionPerformed
        txtAddr1.requestFocus();
    }//GEN-LAST:event_txtCompanyNameActionPerformed

    private void txtAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr1ActionPerformed
        txtAddr2.requestFocus();
    }//GEN-LAST:event_txtAddr1ActionPerformed

    private void txtAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr2ActionPerformed
        txtAddr3.requestFocus();
    }//GEN-LAST:event_txtAddr2ActionPerformed

    private void txtAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr3ActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddr3ActionPerformed
    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(14).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(17).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(19).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(21).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(22).setCellRenderer(new NumberTableCellRenderer());

        totalTable.getColumnModel().getColumn(0).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(1).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(4).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(5).setCellRenderer(new NumberTableCellRenderer());
    }

    public void TotalCalculate() {
        float totalIGST = 0, totalCGST = 0, totalSGST = 0, totalAmount = 0, totalAmountWithTax = 0, totalCess = 0;

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }

        for (int i = 0; i < model.getRowCount(); i++) {
            BigDecimal cess = new BigDecimal(model.getValueAt(i, 22).toString());
            totalAmount = (float) model.getValueAt(i, 15) + totalAmount;
            totalIGST = (float) model.getValueAt(i, 17) + totalIGST;
            totalCGST = (float) model.getValueAt(i, 19) + totalCGST;
            totalSGST = (float) model.getValueAt(i, 21) + totalSGST;
            totalCess = cess.floatValue() + totalCess;
            totalAmountWithTax = (float) model.getValueAt(i, 23) + totalAmountWithTax;
        }
        amountWithoutDiscount = totalAmountWithTax;
        String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (strDiscount.equals("")) {
            strDiscount = "0";
        }
        float DiscountAmt = Float.parseFloat(strDiscount);
        amount = amountWithoutDiscount - DiscountAmt;
        labelBillAmount.setText(amount + "");
        Object[] row = {totalAmount, totalIGST, totalCGST, totalSGST, totalCess, amount};
        model2.addRow(row);
    }
    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void txtPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhoneActionPerformed
        txtGSTIN.requestFocus();
    }//GEN-LAST:event_txtPhoneActionPerformed

    private void txtMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileActionPerformed
        txtPhone.requestFocus();
    }//GEN-LAST:event_txtMobileActionPerformed

    private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed

    }//GEN-LAST:event_txtGSTINActionPerformed

    public void setTable() {
        jTable1.getColumnModel().getColumn(16).setMinWidth(100);
        jTable1.getColumnModel().getColumn(16).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(17).setMinWidth(100);
        jTable1.getColumnModel().getColumn(17).setMaxWidth(100);

        jTable1.getColumnModel().getColumn(18).setMinWidth(100);
        jTable1.getColumnModel().getColumn(18).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(19).setMinWidth(100);
        jTable1.getColumnModel().getColumn(19).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(20).setMinWidth(100);
        jTable1.getColumnModel().getColumn(20).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(21).setMinWidth(100);
        jTable1.getColumnModel().getColumn(21).setMaxWidth(100);

//        totalTable.getColumnModel().getColumn(0).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(0).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(1).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(1).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(2).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(2).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(3).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(3).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(4).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
        if (isIGST) {

            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);

//            totalTable.getColumnModel().getColumn(0).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(1).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(1).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(4).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(4).setMaxWidth(300);
        } else {
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);

//            totalTable.getColumnModel().getColumn(0).setMinWidth(100);
//            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(2).setMinWidth(100);
//            totalTable.getColumnModel().getColumn(2).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(3).setMinWidth(100);
//            totalTable.getColumnModel().getColumn(3).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(4).setMinWidth(100);
//            totalTable.getColumnModel().getColumn(4).setMaxWidth(300);
        }
    }

    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        txtState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void comboProductItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboProductItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_comboProductItemStateChanged

    private void comboProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboProductFocusGained

    }//GEN-LAST:event_comboProductFocusGained

    private void comboProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductActionPerformed
//        getProductDetails();
    }//GEN-LAST:event_comboProductActionPerformed

    private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_labelProductKeyReleased

    private void btnAddAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_btnAddAncestorMoved

    }//GEN-LAST:event_btnAddAncestorMoved

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if (txtCompanyName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Company Name", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            addProductList();
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_btnAddActionPerformed
    public void addProductList() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmt = txtCess.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String Discount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String Discountper = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strTotalWithTax = txtTotalWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strITC = comboITC.getSelectedItem().toString();
        GoDown selGodown = null;
        String strGodown = "";
        if (isInventoryEnabled) {
            for (GoDown godown : godowns) {
                if (comboGodown.getSelectedItem().toString().equalsIgnoreCase(godown.getGoDownName())) {
                    selGodown = godown;
                    strGodown = selGodown.getGoDownName();
                    break;
                }
            }
        }
        String product = (String) comboProduct.getSelectedItem();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Boolean isProductAlreadyHere = false;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (model.getValueAt(i, 1).toString().equals(product)) {
                isProductAlreadyHere = true;
            }
        }

        if (comboProduct.getSelectedItem().equals("Select the Product")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        } else if (strUQC1rate.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter Total Amount", "Alert", JOptionPane.WARNING_MESSAGE);
            txtTotalAmt.requestFocus();
        } else if (isProductAlreadyHere) {
            JOptionPane.showMessageDialog(null, product + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else if (strTotalWithTax.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter Total Amount With Tax", "Alert", JOptionPane.WARNING_MESSAGE);
            txtTotalWithTax.requestFocus();
        } else {
            int count = jTable1.getRowCount() + 1;
            int wholeQuantity = 0;

            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, cessAmt = 0, totalWithTax = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (strCessAmt.equals("")) {
                strCessAmt = "0.0";
            }
            if (!strTotalWithTax.equals("")) {
                totalWithTax = Float.parseFloat(strTotalWithTax);
            }

            UQC2rate = price;
            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
            float Rate = Float.parseFloat("0" + txtTotalAmt.getText()),
                    IGST_amt = Float.parseFloat("0" + txtIGSTamt.getText()),
                    CGST_amt = Float.parseFloat("0" + txtCGSTamt.getText()),
                    SGST_amt = Float.parseFloat("0" + txtSGSTamt.getText()),
                    Amount = Float.parseFloat("0" + txtAmount.getText());
            cessAmt = Float.parseFloat(strCessAmt);

            float amountWithTax = IGST_amt + CGST_amt + SGST_amt + Amount + cessAmt;

            if (!Discountper.equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            } else {
                Discountper = "0.0";
            }
            if (!Discount.equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            } else {
                Discount = "0";
            }
            float ITCIGST = 0;
            float ITCSGST = 0;
            float ITCCGST = 0;
            float ITCCess = 0;

            if (!strITC.equalsIgnoreCase("Ineligible")) {
                if ((txtITCIGST1.getText() != null) && !(txtITCIGST1.getText().equals(""))) {
                    ITCIGST = Float.parseFloat(txtITCIGST1.getText());
                }
                if ((txtITCCGST1.getText() != null) && !(txtITCCGST1.getText().equals(""))) {
                    ITCCGST = Float.parseFloat(txtITCCGST1.getText());
                }
                if ((txtITCSGST1.getText() != null) && !(txtITCSGST1.getText().equals(""))) {
                    ITCSGST = Float.parseFloat(txtITCSGST1.getText());
                }
                if ((txtITCCess1.getText() != null) && !(txtITCCess1.getText().equals(""))) {
                    ITCCess = Float.parseFloat(txtITCCess1.getText());
                }
            }

            Object[] row = {count, product, HSNCode, labelUQC1.getText(), UQC1qty, Rate, Rate,
                labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(),
                wholeQuantity, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGST, IGST_amt, CGST, CGST_amt, SGST, SGST_amt, cessAmt,
                amountWithTax, strGodown, 0, strITC, ITCIGST, ITCCGST, ITCSGST, ITCCess, totalWithTax
            };
            model.addRow(row);
            clear();
            TotalCalculate();
            jTableRender();
            comboProduct.requestFocus();
        }
    }

    public void TaxvalueCalculation() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCess = txtCess.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strAmount = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = "";
        if (txtUQC2Qty.isEnabled()) {
            strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        boolean bAmount = false;
        float Amount = 0;
        if ((!strAmount.equals("")) && (!strAmount.equals("0"))) {
            Amount = Float.parseFloat(strAmount);
            bAmount = true;
        }
        if (bAmount && (!strUQC1rate.equals("") && (((!strUQC1qty.equals("") && (!strUQC1qty.equals("0"))) || (!strUQC2qty.equals("") && (!strUQC2qty.equals("0"))))))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, cessAmt = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1qty.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strCess.equals("")) {
                cessAmt = Float.parseFloat(strCess);
            }

            float DiscountPer = 0, Discount = 0;
            float IGSTper = 0;
//            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);
            String productName = comboProduct.getSelectedItem().toString();
            if ((productName != null) && (!(productName.equals("")))) {
                for (Product product : products) {
                    if (product.getName().equals(productName)) {
                        IGSTper = product.getIgstPercentage();
                        float TaxValue = Amount * (IGSTper / 100);
                        if (isIGST) {
                            txtIGSTcalcamt.setText(currency.format(TaxValue));
                            txtCGSTcalcamt.setText("0.00");
                            txtSGSTcalcamt.setText("0.00");
                            IGSTValidation();
                        } else {
                            txtIGSTcalcamt.setText("0.00");
                            txtCGSTcalcamt.setText(currency.format(TaxValue / 2));
                            txtSGSTcalcamt.setText(currency.format(TaxValue / 2));
                            SGSTValidation();
                            CGSTValidation();
                        }
                        String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        if (!strDiscountPer.equals("") && Amount > 0) {
                            DiscountPer = Float.parseFloat(strDiscountPer);
                            Discount = (Amount * DiscountPer) / 100;
                            Amount = Amount - Discount;
                            txtDiscount.setText(currency.format(Discount));
                        } else {
                            txtDiscount.setText("");
                        }
                        txtAmount.setText(currency.format(Amount));
                        Amount = Amount + cessAmt;
                        lblTotalWithTax.setText(currency.format(Amount + TaxValue));
                        break;
                    } else {
                        txtAmount.setText("");
                        lblTotalWithTax.setText("");

                        txtIGSTamt.setText("");
                        txtCGSTamt.setText("");
                        txtSGSTamt.setText("");

                        txtIGSTamt.setText("");
                        txtCGSTamt.setText("");
                        txtSGSTamt.setText("");

                        txtIGSTcalcamt.setText("");
                        txtCGSTcalcamt.setText("");
                        txtSGSTcalcamt.setText("");
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Enter the Quantity and Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        }
    }

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtCompanyName.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Please Add Company Name", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                addProductList();
                comboProduct.requestFocus();
                evt.consume();
            }
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    model.setValueAt(y + 1, y, 0); //setValueAt(data,row,column)
                }
                clear();
                TotalCalculate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed
    public void clear() {
        txtUQC1Qty.setText("");
        txtUQC2Qty.setText("");
        txtTotalAmt.setText("");
        txtIGSTper.setText("");
        txtCGSTper.setText("");
        txtSGSTper.setText("");
        txtIGSTamt.setText("");
        txtCGSTamt.setText("");
        txtSGSTamt.setText("");
        txtAmount.setText("");
        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("UQC1");
        labelUQC2.setText("UQC2");
        comboITC.setSelectedIndex(0);
        txtITCIGST1.setText("");
        txtITCCGST1.setText("");
        txtITCSGST1.setText("");
        txtITCCess1.setText("");
        comboProduct.setSelectedItem("");
        jTable1.clearSelection();
        txtCess.setText("");
        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        isActive = false;
        txtIGSTcalcamt.setText("");
        txtCGSTcalcamt.setText("");
        txtSGSTcalcamt.setText("");
        lblTotalWithTax.setText("");
        txtTotalWithTax.setText("");

        DefaultTableModel model3 = (DefaultTableModel) tableProductPurchaseDetails.getModel();
        if (model3.getRowCount() > 0) {
            model3.setRowCount(0);
        }

    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        SaveAndPrint(false);
    }//GEN-LAST:event_btnSaveActionPerformed
    public void SaveAndPrint(Boolean isPrint) {

        String BillNo = txtBillNo.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (dateBill.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please add Purchase Date", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            Boolean isNotExpired = true;
            if (isTrialPackage) {
                isNotExpired = new CheckTrialPackage().checkByDate(dateBill.getDate());
            }
            if (isNotExpired) {
                Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);
                if (BillNo.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please Enter the Bill Number", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtBillNo.requestFocus();
                } else if (!isInBetweenFinancialYear) {
                    String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                            + " to <b>" + strFinancialYearEnd + "</b></html>";
                    JLabel label = new JLabel(msg);
                    JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
                } else {
                    if (isOldBill) {
                        OldPrintAndSaveAction(isPrint);
                    } else {
                        NewPrintAndSaveAction(isPrint);
                    }
                }
            } else {
                new TrialPackageExpiryUI().setVisible(true);
            }
        }

    }

    public void OldPrintAndSaveAction(Boolean isPrint) {
        setPartyEditable(true);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        float totalCess = 0;
        if (rowCount > 0) {
            float totalAmount = (float) totalTable.getModel().getValueAt(0, 0),
                    totalIGST = (float) totalTable.getModel().getValueAt(0, 1),
                    totalCGST = (float) totalTable.getModel().getValueAt(0, 2),
                    totalSGST = (float) totalTable.getModel().getValueAt(0, 3);
            totalCess = (float) totalTable.getModel().getValueAt(0, 4);
            float NetAmount = (float) totalTable.getModel().getValueAt(0, 5);

            ledgerData = findLedgerData();
            PurchaseInvoice newPurchaseInvoice = new PurchaseInvoice();

            newPurchaseInvoice.setCompany(SessionDataUtil.getSelectedCompany());
            newPurchaseInvoice.setPurchaseInvoiceId(oldSale.getPurchaseInvoiceId());
            newPurchaseInvoice.setIsIGST(chkboxIGST.isSelected());
            newPurchaseInvoice.setPuchaseInvoiceAmount(convertDecimal.Currency(currency.format(totalAmount)));

            newPurchaseInvoice.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            newPurchaseInvoice.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            newPurchaseInvoice.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            newPurchaseInvoice.setCessValue(convertDecimal.Currency(currency.format(totalCess)));
            newPurchaseInvoice.setCalculatedPurchaseInvoiceAmount(convertDecimal.Currency(currency.format(NetAmount)));
            newPurchaseInvoice.setCancelled(oldSale.isCancelled());
            newPurchaseInvoice.setLedger(ledgerData);
            newPurchaseInvoice.setDiscountAmount(convertDecimal.Currency(txtBillDiscount.getText()));
            newPurchaseInvoice.setDiscountPercent((discountPer));
            newPurchaseInvoice.setRemarks(txtRemarks.getText());
            newPurchaseInvoice.seteWayBillNo(txtEwayBillNo.getText());
            newPurchaseInvoice.seteWayBillRemarks(txtEwayRemark.getText());
            newPurchaseInvoice.setSupplierInvoiceNo(txtBillNo.getText());
            newPurchaseInvoice.setPurchaseInvoiceDate(dateBill.getDate());
            newPurchaseInvoice.setPurchaseInvoiceNo(oldSale.purchaseInvoiceNo);
            List<PurchaseInvoiceLineItem> psLineItems = new ArrayList<PurchaseInvoiceLineItem>();
            Long count = 1L;
            for (int i = 0; i < rowCount; i++) {

                PurchaseInvoiceLineItem psLineItem = new PurchaseInvoiceLineItem();
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(product);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setCessAmount(convertDecimal.Currency(model.getValueAt(i, 22).toString()));

                psLineItem.setUqc1BaseRate(convertDecimal.Currency("0.000"));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency("0.000"));
                psLineItem.setITCEligible(model.getValueAt(i, 26).toString());
                psLineItem.setItcValueforIGST(convertDecimal.Currency(model.getValueAt(i, 27).toString()));
                psLineItem.setItcValueforCGST(convertDecimal.Currency(model.getValueAt(i, 28).toString()));
                psLineItem.setItcValueforSGST(convertDecimal.Currency(model.getValueAt(i, 29).toString()));
                psLineItem.setItcValueforCess(convertDecimal.Currency(model.getValueAt(i, 30).toString()));
                psLineItem.setTotalAmountWithTax(convertDecimal.Currency(model.getValueAt(i, 31).toString()));
                psLineItems.add(psLineItem);

                String strGodown = model.getValueAt(i, 24).toString();
                GoDown SelGodown = null;
                for (GoDown godown : godowns) {
                    if (strGodown.equalsIgnoreCase(godown.getGoDownName())) {
                        SelGodown = godown;
                        break;
                    }
                }
                psLineItem.setGodown(SelGodown);
            }
            Boolean isSuccess = false;
            EventStatus status = new PurchaseLogic().updDatePurchase(newPurchaseInvoice, psLineItems);
            isSuccess = status.isUpdateDone();

            if (isPrint) {
                try {
                    new PurchaseBill().printPdf(isPrint, false, newPurchaseInvoice, psLineItems);
                } catch (Exception ex) {
                    Logger.getLogger(PurchaseUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (isSuccess) {
                JOptionPane.showMessageDialog(null, "Purchase is successful", "Message", JOptionPane.INFORMATION_MESSAGE);
                new PurchaseReportUI().setVisible(true);
                dispose();
                clear();
            } else {
                JOptionPane.showMessageDialog(null, status.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void NewPrintAndSaveAction(Boolean isPrint) {
        GoDown SelGodown = null;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {

            String strEwayBillNo = txtEwayBillNo.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strEwayRemarks = txtEwayRemark.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            float totalAmount = (float) totalTable.getModel().getValueAt(0, 0),
                    totalIGST = (float) totalTable.getModel().getValueAt(0, 1),
                    totalCGST = (float) totalTable.getModel().getValueAt(0, 2),
                    totalSGST = (float) totalTable.getModel().getValueAt(0, 3),
                    totalCess = (float) totalTable.getModel().getValueAt(0, 4);
            float NetAmount = (float) totalTable.getModel().getValueAt(0, 5);
            PurchaseInvoice newPurchaseInvoice = new PurchaseInvoice();

            newPurchaseInvoice.setCompany(SessionDataUtil.getSelectedCompany());
            newPurchaseInvoice.seteWayBillNo(strEwayBillNo);
            newPurchaseInvoice.seteWayBillRemarks(strEwayRemarks);
            newPurchaseInvoice.setPurchaseInvoiceDate(dateBill.getDate());
            newPurchaseInvoice.setPuchaseInvoiceAmount(convertDecimal.Currency(currency.format(totalAmount)));
            newPurchaseInvoice.setIsIGST(isIGST);
            newPurchaseInvoice.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            newPurchaseInvoice.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            newPurchaseInvoice.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            newPurchaseInvoice.setCessValue(convertDecimal.Currency(currency.format(totalCess)));
            newPurchaseInvoice.setCalculatedPurchaseInvoiceAmount(convertDecimal.Currency(currency.format(NetAmount)));
            newPurchaseInvoice.setLedger(ledgerData);
            newPurchaseInvoice.setRemarks(txtRemarks.getText());
            newPurchaseInvoice.setDiscountAmount(convertDecimal.Currency("0" + txtBillDiscount.getText()));
            newPurchaseInvoice.setDiscountPercent(discountPer);
            newPurchaseInvoice.setTotalAmountPaid(new BigDecimal(0));

            newPurchaseInvoice.setUnregistered(isUnregistered);
            if (isUnregistered) {
                newPurchaseInvoice.setUnregisteredCity(txtCity.getText());
                newPurchaseInvoice.setUnregisteredDistrict(txtDistrict.getText());
                newPurchaseInvoice.setUnregisteredId("");
                newPurchaseInvoice.setUnregisteredLineOne(txtAddr1.getText());
                newPurchaseInvoice.setUnregisteredLineTwo(txtAddr2.getText());
                newPurchaseInvoice.setUnregisteredLineThree(txtAddr3.getText());
                newPurchaseInvoice.setUnregisteredMobile(txtMobile.getText());
                newPurchaseInvoice.setUnregisteredState(txtState.getText());
                newPurchaseInvoice.setUnregisteredName(txtCompanyName.getText());
                newPurchaseInvoice.setUnregisteredEmail(txtEmail.getText());
                newPurchaseInvoice.setUnregisteredId(txtGSTIN.getText());

            }
            newPurchaseInvoice.setSupplierInvoiceNo(txtBillNo.getText());
            newPurchaseInvoice.setPurchaseInvoiceType("Regular");
            newPurchaseInvoice.setReverseChargeApplicable(false);

            List<PurchaseInvoiceLineItem> psLineItems = new ArrayList<PurchaseInvoiceLineItem>();
            Long count = 1L;
            for (int i = 0; i < rowCount; i++) {
                String strGodown = model.getValueAt(i, 24).toString();
                if (!strGodown.equals("") && isInventoryEnabled) {
                    for (GoDown godown : godowns) {
                        if (godown.getGoDownName().equalsIgnoreCase(strGodown)) {
                            SelGodown = godown;
                        }
                    }
                }
                PurchaseInvoiceLineItem psLineItem = new PurchaseInvoiceLineItem();
                psLineItem.setGodown(SelGodown);
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(product);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setCessAmount(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
                psLineItem.setUqc1BaseRate(convertDecimal.Currency("0.000"));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency("0.000"));
                psLineItem.setITCEligible(model.getValueAt(i, 26).toString());
                psLineItem.setItcValueforIGST(convertDecimal.Currency(model.getValueAt(i, 27).toString()));
                psLineItem.setItcValueforCGST(convertDecimal.Currency(model.getValueAt(i, 28).toString()));
                psLineItem.setItcValueforSGST(convertDecimal.Currency(model.getValueAt(i, 29).toString()));
                psLineItem.setItcValueforCess(convertDecimal.Currency(model.getValueAt(i, 30).toString()));
                psLineItem.setTotalAmountWithTax(convertDecimal.Currency(model.getValueAt(i, 31).toString()));
                psLineItems.add(psLineItem);

            }
            EventStatus result = new PurchaseLogic().createPurchase(newPurchaseInvoice, psLineItems);
            if (isPrint) {
                new PurchaseBill().pdfByBillId(true, false, result.getBillID());
            }
            if (result.isCreateDone()) {
                JOptionPane.showMessageDialog(null, "Purchase is Successful", "Message", JOptionPane.INFORMATION_MESSAGE);
                clearAll();
                comboParty.requestFocus();
            } else {
                JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void clearAll() {
        comboParty.setSelectedItem("");
        dateBill.setDate(null);
        jTabbedPane1.setSelectedIndex(0);

        txtCompanyName.setText("");
        txtAddr1.setText("");
        txtAddr2.setText("");
        txtAddr3.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        txtState.setText("");
        txtEmail.setText("");
        txtMobile.setText("");
        txtPhone.setText("");
        txtGSTIN.setText("");
        txtBillNo.setText("");
        txtBillDiscount.setText("");
        labelBillAmount.setText("");
        clear();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }

    }

    public void updateProductItems() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmt = txtCess.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strTotalWithTax = txtTotalWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strITC = comboITC.getSelectedItem().toString();

        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            if (comboProduct.getSelectedItem().equals("Select the Product")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            } else if (strUQC1rate.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Enter Total Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtTotalAmt.requestFocus();
            } else if (strTotalWithTax.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Enter Total Price With Tax", "Alert", JOptionPane.WARNING_MESSAGE);
                txtTotalWithTax.requestFocus();
            } else {
                int count = jTable1.getRowCount() + 1;
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, cessAmt = 0, totalAmountWithtTax = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strCessAmt.equals("")) {
                    cessAmt = Float.parseFloat(strCessAmt);
                }
                if (!strTotalWithTax.equals("")) {
                    totalAmountWithtTax = Float.parseFloat(strTotalWithTax);
                }
                UQC2rate = price;
                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
                float Rate = Float.parseFloat(txtTotalAmt.getText()),
                        IGST_amt = Float.parseFloat("0" + txtIGSTamt.getText()),
                        CGST_amt = Float.parseFloat("0" + txtCGSTamt.getText()),
                        SGST_amt = Float.parseFloat("0" + txtSGSTamt.getText()),
                        Amount = Float.parseFloat("0" + txtAmount.getText());
                float amountWithTax = IGST_amt + CGST_amt + SGST_amt + Amount + cessAmt;
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String product = (String) comboProduct.getSelectedItem();

                float ITCIGST = 0;
                float ITCSGST = 0;
                float ITCCGST = 0;
                float ITCCess = 0;

                if (!strITC.equalsIgnoreCase("Ineligible")) {
                    if ((txtITCIGST1.getText() != null) && !(txtITCIGST1.getText().equals(""))) {
                        ITCIGST = Float.parseFloat(txtITCIGST1.getText());
                    }
                    if ((txtITCCGST1.getText() != null) && !(txtITCCGST1.getText().equals(""))) {
                        ITCCGST = Float.parseFloat(txtITCCGST1.getText());
                    }
                    if ((txtITCSGST1.getText() != null) && !(txtITCSGST1.getText().equals(""))) {
                        ITCSGST = Float.parseFloat(txtITCSGST1.getText());
                    }
                    if ((txtITCCess1.getText() != null) && !(txtITCCess1.getText().equals(""))) {
                        ITCCess = Float.parseFloat(txtITCCess1.getText());
                    }
                }
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                model.setValueAt(product, i, 1);
                model.setValueAt(HSNCode, i, 2);
                model.setValueAt(labelUQC1.getText(), i, 3);
                model.setValueAt(UQC1qty, i, 4);
                model.setValueAt(UQC1rate, i, 5);
                model.setValueAt(UQC1qty * UQC1rate, i, 6);
                model.setValueAt(labelUQC2.getText(), i, 7);
                model.setValueAt(UQC2qty, i, 8);
                model.setValueAt(UQC2rate, i, 9);
                model.setValueAt(UQC2qty * UQC2rate, i, 10);
                model.setValueAt(UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(), i, 11);
                model.setValueAt(wholeQuantity, i, 12);
                model.setValueAt(Float.parseFloat(Discountper), i, 13);
                model.setValueAt(Float.parseFloat(Discount), i, 14);
                model.setValueAt(Amount, i, 15);
                model.setValueAt(IGST, i, 16);
                model.setValueAt(IGST_amt, i, 17);
                model.setValueAt(CGST, i, 18);
                model.setValueAt(CGST_amt, i, 19);
                model.setValueAt(SGST, i, 20);
                model.setValueAt(SGST_amt, i, 21);
                model.setValueAt(cessAmt, i, 22);
                model.setValueAt(amountWithTax, i, 23);
                model.setValueAt(strITC, i, 26);
                model.setValueAt(ITCIGST, i, 27);
                model.setValueAt(ITCCGST, i, 28);
                model.setValueAt(ITCSGST, i, 29);
                model.setValueAt(ITCCess, i, 30);
                model.setValueAt(totalAmountWithtTax, i, 31);
                clear();
                TotalCalculate();
                jTableRender();
                labelProduct.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProductItems();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        SaveAndPrint(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtTotalAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalAmtActionPerformed

    }//GEN-LAST:event_txtTotalAmtActionPerformed

    private void txtTotalAmtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalAmtKeyReleased

    }//GEN-LAST:event_txtTotalAmtKeyReleased

    private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
        txtDiscount.requestFocus();
    }//GEN-LAST:event_txtDiscountPerActionPerformed

    private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtDiscount.requestFocus();
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDiscountPerKeyPressed

    private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
        TaxvalueCalculation();
        evt.consume();
    }//GEN-LAST:event_txtDiscountPerKeyReleased

    private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped

    }//GEN-LAST:event_txtDiscountPerKeyTyped

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        int i = jTable1.getSelectedRow();
        if (i == -1) {
            btnAdd.requestFocus();
        } else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
        String quantity = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String quantity2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String rate = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCess = txtCess.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strAmount = txtTotalAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if ((!rate.equals("")) && ((!quantity.equals("")) || (!quantity2.equals("")))) {
            if (quantity.equals("")) {
                quantity = "0";
            }
            if (quantity2.equals("")) {
                quantity2 = "0";
            }
            int Qty = Integer.parseInt(quantity);
            int Qty2 = Integer.parseInt(quantity2);
            float Rate2 = price;
            float Rate = Float.parseFloat(rate);
            float DiscountPer = 0, Discount = 0, TotalAmount = 0;
            boolean bTotalAmount = false;
            if ((!strAmount.equals("")) && !(strAmount.equals("0"))) {
                TotalAmount = Float.parseFloat(strAmount);
                bTotalAmount = true;
            }
            float cessAmt = 0;
            if (bTotalAmount) {
                if (!strDiscount.equals("")) {
                    Float DiscountAmount = Float.parseFloat(strDiscount);
                    String strDiscountPer = currency.format((DiscountAmount / TotalAmount) * 100);
                    txtDiscountPer.setText(strDiscountPer);
                    TotalAmount = TotalAmount - DiscountAmount;
                }
                float TaxValue = TotalAmount * (IGST / 100);
                txtAmount.setText(currency.format(TotalAmount));
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtDiscountKeyReleased

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtTotalAmt.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        comboProduct.setSelectedItem(model.getValueAt(i, 1));
        HSNCode = (String) model.getValueAt(i, 2);
        labelUQC1.setText((String) model.getValueAt(i, 3));
        txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
        txtTotalAmt.setText(currency.format(model.getValueAt(i, 5)));
        labelUQC2.setText((String) model.getValueAt(i, 7));
        txtUQC2Qty.setText(model.getValueAt(i, 8).toString());
        txtDiscountPer.setText(model.getValueAt(i, 13).toString());
        txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
        txtAmount.setText(currency.format(model.getValueAt(i, 15)));
        txtCess.setText(currency.format(model.getValueAt(i, 22)));
        comboGodown.setSelectedItem(model.getValueAt(i, 24).toString());
        comboITC.setSelectedItem(model.getValueAt(i, 26).toString());
        txtITCIGST1.setText(model.getValueAt(i, 27).toString());
        txtITCCGST1.setText(model.getValueAt(i, 28).toString());
        txtITCSGST1.setText(model.getValueAt(i, 29).toString());
        txtITCCess1.setText(model.getValueAt(i, 30).toString());

        btnAdd.setEnabled(false);
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);

        btnFocus = btnUpdate;
        getbtnAddFocus();

        String productName = (String) comboProduct.getSelectedItem();
        if ((productName != null) && (!(productName.equals("")))) {
            for (Product pro : products) {
                if (pro.getName().equals(productName)) {
                    product = pro;
                    fetchProductPurchaseDetails();
                    labelUQC1.setText(pro.getUQC1());
                    labelUQC2.setText(pro.getUQC2());
                    HSNCode = pro.getHsnCode();
                    UQC2Value = pro.getUQC2Value();

                    ProductRate = Float.parseFloat(txtTotalAmt.getText().replaceAll("^\\s+", ""));
                    IGST = pro.getIgstPercentage();
                    CGST = pro.getCgstPercentage();
                    SGST = pro.getSgstPercentage();
                    txtIGSTper.setText(String.valueOf(pro.getIgstPercentage()));
                    txtCGSTper.setText(String.valueOf(pro.getCgstPercentage()));
                    txtSGSTper.setText(String.valueOf(pro.getSgstPercentage()));
                    isProductTaxInclusive = pro.isBasePriceInclusiveOfGST();
                    price = 0;
                    if (isProductTaxInclusive) {
                        price = ((ProductRate / (100 + IGST)) * 100) / UQC2Value;
                    } else {
                        price = ProductRate / UQC2Value;
                    }
//                    txtUQC1Rate.setText(currency.format(price * UQC2Value));
                    comboProduct.requestFocus();
                }
            }
        }
        TaxvalueCalculation();
        txtIGSTper.setText(model.getValueAt(i, 16).toString());
        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
        IGSTValidation();
        txtCGSTper.setText(model.getValueAt(i, 18).toString());
        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
        txtSGSTper.setText(model.getValueAt(i, 20).toString());
        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
        CGSTValidation();
        SGSTValidation();
        txtTotalWithTax.setText(currency.format(model.getValueAt(i, 31)));
        totalValidation();
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
//        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC2QtyKeyReleased

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
//        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC1QtyKeyReleased

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void txtTotalAmtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalAmtKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtTotalAmt.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtTotalAmtKeyTyped

    private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC1QtyKeyTyped

    private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC2QtyKeyTyped

    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        if (txtUQC2Qty.isEnabled()) {
            txtUQC2Qty.requestFocus();
        } else {
            txtTotalAmt.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
        txtTotalAmt.requestFocus();
    }//GEN-LAST:event_txtUQC2QtyActionPerformed

    private void comboPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPartyActionPerformed
//        getPartyDetails();
    }//GEN-LAST:event_comboPartyActionPerformed

    public void getPartyDetails() {
        String name = (String) comboParty.getSelectedItem();

        if ("Unregistered".equalsIgnoreCase(name)) {
            isUnregistered = true;
            txtCompanyName.setText("");
            txtAddr1.setText("");
            txtAddr2.setText("");
            txtAddr3.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            txtState.setText("");
            txtEmail.setText("");
            txtMobile.setText("");
            txtPhone.setText("");
            txtGSTIN.setText("");

            labelPartyName.setText("");
            labelAddress.setText("");
            labelGSTIN.setText("");

        } else {
            for (Ledger ledger : sortedLedgers) {
                if (ledger.getLedgerName().equals(name)) {

                    ledgerData = ledger;
                    LedgerId = ledger.getLedgerId();
                    txtCompanyName.setText(ledger.getLedgerName());
                    String address = ledger.getAddress();
                    String[] words = address.split("/~/");
                    if (words.length == 1) {
                        txtAddr1.setText(words[0]);
                        txtAddr2.setText("");
                        txtAddr3.setText("");
                        labelAddress.setText(words[0]);
                    } else if (words.length == 2) {
                        txtAddr1.setText(words[0]);
                        txtAddr2.setText(words[1]);
                        txtAddr3.setText("");
                        labelAddress.setText(words[0] + "  " + words[1]);
                    } else {
                        txtAddr1.setText(words[0]);
                        txtAddr2.setText(words[1]);
                        txtAddr3.setText(words[2]);
                        labelAddress.setText(words[0] + "  " + words[1] + "  " + words[2]);
                    }
                    txtCity.setText(ledger.getCity());
                    txtDistrict.setText(ledger.getDistrict());
                    txtState.setText(ledger.getState());
                    txtEmail.setText(ledger.getEmail());
                    txtMobile.setText(ledger.getMobile());
                    txtPhone.setText(ledger.getPhone());
                    txtGSTIN.setText(ledger.getGSTIN());

                    labelPartyName.setText(ledger.getLedgerName());
                    labelGSTIN.setText(ledger.getGSTIN());

                    String selectedState = txtState.getText();
                    isIGST = !(selectedState.equals(CompanyState));

                    if (isIGST) {
                        chkboxIGST.setSelected(true);
                    }
                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    if (model.getRowCount() > 0) {
                        model.setRowCount(0);
                    }
                    DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
                    if (model2.getRowCount() > 0) {
                        model2.setRowCount(0);
                    }
                    setTable();
                }
            };

        }
    }

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained
//        comboParty.setPopupVisible(true);
    }//GEN-LAST:event_comboPartyFocusGained

    private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtBillNo.requestFocus();
        }
    }//GEN-LAST:event_comboPartyKeyPressed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (jTabbedPane1.getSelectedIndex() == 0) {
            comboParty.requestFocus();
        } else {
            comboProduct.setSelectedItem("");
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateProductItems();
            btnFocus = btnAdd;
            getbtnAddFocus();
            evt.consume();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void txtBillNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillNoActionPerformed

    }//GEN-LAST:event_txtBillNoActionPerformed

    private void txtCGSTamtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCGSTamtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCGSTamtActionPerformed

    private void chkboxIGSTStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_chkboxIGSTStateChanged
        isIGST = chkboxIGST.isSelected();
        if (isIGST) {
            lblIGST.setText("IGST");
            lblCGST.setText("");
            lblSGST.setText("");
            txtIGSTamt.setVisible(true);
            txtSGSTamt.setVisible(false);
            txtCGSTamt.setVisible(false);
        } else {
            lblIGST.setText("");
            lblCGST.setText("CGST");
            lblSGST.setText("SGST");
            txtIGSTamt.setVisible(false);
            txtSGSTamt.setVisible(true);
            txtCGSTamt.setVisible(true);
        }
    }//GEN-LAST:event_chkboxIGSTStateChanged

    private void txtCessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCessActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCessActionPerformed

    private void txtTotalAmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalAmtKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtDiscountPer.requestFocus();
            TaxvalueCalculation();
            evt.consume();
        }

        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            TaxvalueCalculation();
            evt.consume();
        }


    }//GEN-LAST:event_txtTotalAmtKeyPressed

    private void txtCessKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessKeyReleased
    }//GEN-LAST:event_txtCessKeyReleased

    private void txtCessKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessKeyPressed
        TaxvalueCalculation();
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isIGST) {
                txtIGSTamt.requestFocus();
            } else {
                txtCGSTamt.requestFocus();
            }
            evt.consume();
        } else if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_txtCessKeyPressed

    private void txtCGSTamtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCGSTamtKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtSGSTamt.isVisible()) {
                txtSGSTamt.requestFocus();
            } else if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCGSTamtKeyPressed

    private void txtIGSTamtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIGSTamtKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtTotalWithTax.requestFocus();
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtIGSTamtKeyPressed

    private void txtSGSTamtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSGSTamtKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtTotalWithTax.requestFocus();
            evt.consume();
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }

    }//GEN-LAST:event_txtSGSTamtKeyPressed

    private void txtDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCess.requestFocus();
            evt.consume();
        }
    }//GEN-LAST:event_txtDiscountKeyPressed

    private void comboGodownKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGodownKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_comboGodownKeyPressed

    private void txtBillNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillNoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strBillNo = txtBillNo.getText().replaceAll("^\\s+", "").replaceAll("^\\s+$", "");
            if (!strBillNo.equals("")) {
                dateBill.requestFocusInWindow();
            } else {
                JOptionPane.showMessageDialog(null, "Please Enter The Bill Number", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_txtBillNoKeyPressed

    private void txtCessKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessKeyTyped
        char vChar = evt.getKeyChar();
//        if (vChar == KeyEvent.VK_ENTER) {
//            if (isIGST) {
//                txtIGSTamt.requestFocus();
//            } else {
//                txtCGSTamt.requestFocus();
//            }
//        }
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtCessKeyTyped

    private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
        
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_txtUQC2QtyKeyPressed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void txtBillDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillDiscountActionPerformed

    }//GEN-LAST:event_txtBillDiscountActionPerformed

    private void txtBillDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnSave.requestFocus();
        }
    }//GEN-LAST:event_txtBillDiscountKeyPressed

    private void txtBillDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyReleased

        if (evt.getKeyCode() != KeyEvent.VK_ENTER && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE && Character.isDigit(evt.getKeyChar())) {
            discount = Float.parseFloat(txtBillDiscount.getText());
            amount = amountWithoutDiscount - discount;
            labelBillAmount.setText(String.valueOf(amount));
            discountPer = (discount / amountWithoutDiscount) * 100;
        } else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && (amount < amountWithoutDiscount)) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscount.equals("")) {
                Float fDiscount = Float.parseFloat(txtBillDiscount.getText());
                amount = amount + (discount - fDiscount);
                discount = discount - (discount - fDiscount);
                discountPer = (discount / amountWithoutDiscount) * 100;
                labelBillAmount.setText(currency.format(amount));
            } else {
                labelBillAmount.setText(currency.format(amountWithoutDiscount));
                discountPer = 0;
            }
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strDiscount.equals("")) {
                txtBillDiscount.setText("0");
            }
            TotalCalculate();
            btnSave.requestFocus();
        }
        evt.consume();
    }//GEN-LAST:event_txtBillDiscountKeyReleased

    private void txtBillDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBillDiscount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBillDiscountKeyTyped

    private void txtIGSTamtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIGSTamtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIGSTamtActionPerformed

    private void dateBillKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateBillKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTabbedPane1.setSelectedIndex(1);
        }
    }//GEN-LAST:event_dateBillKeyPressed

    private void txtITCIGST1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtITCIGST1ActionPerformed
        txtITCCGST1.requestFocus();
    }//GEN-LAST:event_txtITCIGST1ActionPerformed

    private void txtITCCGST1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtITCCGST1ActionPerformed
        txtITCSGST1.requestFocus();
    }//GEN-LAST:event_txtITCCGST1ActionPerformed

    private void comboITCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboITCActionPerformed

    }//GEN-LAST:event_comboITCActionPerformed

    private void txtITCSGST1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtITCSGST1ActionPerformed
        txtITCCess1.requestFocus();
    }//GEN-LAST:event_txtITCSGST1ActionPerformed

    private void txtITCCess1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtITCCess1KeyPressed

    }//GEN-LAST:event_txtITCCess1KeyPressed

    private void txtITCCess1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtITCCess1ActionPerformed
        if (comboGodown.isVisible()) {
            comboGodown.requestFocus();
        } else {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtITCCess1ActionPerformed

    private void comboITCKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboITCKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!comboITC.getSelectedItem().toString().equalsIgnoreCase("Ineligible")) {
                txtITCIGST1.requestFocus();
            } else if (comboGodown.isVisible()) {
                comboGodown.requestFocus();
            } else {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                } else {
                    btnUpdate.requestFocus();
                }
            }
        }
    }//GEN-LAST:event_comboITCKeyPressed

    private void txtIGSTamtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIGSTamtKeyReleased
        IGSTValidation();
    }//GEN-LAST:event_txtIGSTamtKeyReleased

    private void txtCGSTamtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCGSTamtKeyReleased
        CGSTValidation();
    }//GEN-LAST:event_txtCGSTamtKeyReleased

    private void txtSGSTamtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSGSTamtKeyReleased
        SGSTValidation();
    }//GEN-LAST:event_txtSGSTamtKeyReleased

    private void txtDiscountPerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiscountPerFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiscountPerFocusGained

    private void txtTotalWithTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalWithTaxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalWithTaxActionPerformed

    private void txtTotalWithTaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalWithTaxKeyReleased
        totalValidation();
    }//GEN-LAST:event_txtTotalWithTaxKeyReleased

    private void txtTotalWithTaxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalWithTaxKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboITC.requestFocus();
            evt.consume();
        }
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_txtTotalWithTaxKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    private void setIGST() {
        if (isIGST) {
            lblIGST.setText("IGST");
            lblCGST.setText("");
            lblSGST.setText("");
            txtIGSTamt.setVisible(true);
            txtSGSTamt.setVisible(false);
            txtCGSTamt.setVisible(false);
        } else {
            lblIGST.setText("");
            lblCGST.setText("CGST");
            lblSGST.setText("SGST");
            txtIGSTamt.setVisible(false);
            txtSGSTamt.setVisible(true);
            txtCGSTamt.setVisible(true);
        }
    }

    private void setGodown() {
        int nLen = godowns.size();

        for (GoDown godown : godowns) {
            comboGodown.addItem(godown.getGoDownName());
        }
        if (nLen > 1) {
            lblGodown.setText("Godown");
            comboGodown.setVisible(true);
        } else if (nLen == 1) {
            lblGodown.setText("");
            comboGodown.setSelectedItem(godowns.get(0).getGoDownName());
            comboGodown.setVisible(false);
        }
    }

    public Ledger findLedgerData() {
        String name = txtCompanyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : sortedLedgers) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                LedgerId = ledger.getLedgerId();
                break;
            }
        }
        return ledgerData;
    }

    private void saveData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean isPrint = false;
                if (isOldBill) {
                    OldPrintAndSaveAction(isPrint);
                } else {
                    NewPrintAndSaveAction(isPrint);
                }
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnSave.getActionMap();
        actionMap.put("Action", actionListener);
        btnSave.setActionMap(actionMap);
    }

    private void getbtnAddFocus() {
        if (btnAdd.isEnabled()) {
            btnFocus = btnAdd;
        } else {
            btnFocus = btnUpdate;
        }
        Action actionListener;
        actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnFocus.requestFocus();
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnFocus.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnFocus.getActionMap();
        actionMap.put("Action", actionListener);
        btnFocus.setActionMap(actionMap);
    }

    public void setPartyEditable(boolean isEditable) {
        dateBill.setEnabled(isEditable);
        comboParty.setEnabled(isEditable);
        txtBillNo.setEnabled(isEditable);
        txtCompanyName.setEnabled(isEditable);
        txtAddr1.setEnabled(isEditable);
        txtAddr2.setEnabled(isEditable);
        txtAddr3.setEnabled(isEditable);
        txtCity.setEnabled(isEditable);
        txtDistrict.setEnabled(isEditable);
        txtState.setEnabled(isEditable);
        txtGSTIN.setEnabled(isEditable);
        txtEmail.setEnabled(isEditable);
        txtMobile.setEnabled(isEditable);
        txtPhone.setEnabled(isEditable);
        txtRemarks.setEnabled(isEditable);
        chkComboUser.setEnabled(isEditable);
        chkboxIGST.setEnabled(isEditable);
        txtEwayBillNo.setEnabled(isEditable);
        txtEwayRemark.setEnabled(isEditable);
    }

    public void IGSTValidation() {
        String IGSTAmt = txtIGSTamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String calIGSTAmt = txtIGSTcalcamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!IGSTAmt.equals("") && (!IGSTAmt.equals("0"))) {
            if (calIGSTAmt.equals(IGSTAmt)) {
                txtIGSTamt.setForeground(Color.green);
            } else {
                txtIGSTamt.setForeground(Color.RED);
            }
        } else {
            txtSGSTamt.setForeground(Color.RED);
        }
    }

    public void CGSTValidation() {
        String CGSTAmt = txtCGSTamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String calCGSTAmt = txtCGSTcalcamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!CGSTAmt.equals("") && (!CGSTAmt.equals("0"))) {
            if (calCGSTAmt.equals(CGSTAmt)) {
                txtCGSTamt.setForeground(Color.green);
            } else {
                txtCGSTamt.setForeground(Color.RED);
            }
        } else {
            txtSGSTamt.setForeground(Color.RED);
        }
    }

    public void SGSTValidation() {
        String SGSTAmt = txtSGSTamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String calSGSTAmt = txtSGSTcalcamt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!SGSTAmt.equals("") && (!SGSTAmt.equals("0"))) {
            if (calSGSTAmt.equals(SGSTAmt)) {
                txtSGSTamt.setForeground(Color.green);
            } else {
                txtSGSTamt.setForeground(Color.RED);
            }
        } else {
            txtSGSTamt.setForeground(Color.RED);
        }
    }

    public void totalValidation() {
        String totalWithTax = txtTotalWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String calTotalWithTax = lblTotalWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!totalWithTax.equals("") && (!totalWithTax.equals("0"))) {
            if (calTotalWithTax.equals(totalWithTax)) {
                txtTotalWithTax.setForeground(Color.green);
            } else {
                txtTotalWithTax.setForeground(Color.RED);
            }
        } else {
            txtTotalWithTax.setForeground(Color.RED);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PurchaseUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PurchaseUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PurchaseUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PurchaseUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PurchaseUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox chkComboUser;
    private javax.swing.JCheckBox chkboxIGST;
    private javax.swing.JComboBox<String> comboGodown;
    private javax.swing.JComboBox<String> comboITC;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboProduct;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelAddress;
    private javax.swing.JLabel labelBillAmount;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelGSTIN;
    private javax.swing.JLabel labelPartyName;
    private javax.swing.JLabel labelProduct;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lblCGST;
    private javax.swing.JLabel lblCGST1;
    private javax.swing.JLabel lblGodown;
    private javax.swing.JLabel lblIGST;
    private javax.swing.JLabel lblIGST1;
    private javax.swing.JLabel lblITCCGST1;
    private javax.swing.JLabel lblITCCess1;
    private javax.swing.JLabel lblITCIGST1;
    private javax.swing.JLabel lblITCSGST1;
    private javax.swing.JLabel lblSGST;
    private javax.swing.JLabel lblSGST1;
    private javax.swing.JLabel lblTotalWithTax;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTable tableProductPurchaseDetails;
    private javax.swing.JTable totalTable;
    private javax.swing.JTextField txtAddr1;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtAddr3;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JTextField txtBillDiscount;
    private javax.swing.JTextField txtBillNo;
    private javax.swing.JTextField txtCGSTamt;
    private javax.swing.JLabel txtCGSTcalcamt;
    private javax.swing.JLabel txtCGSTper;
    private javax.swing.JTextField txtCess;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtCompanyName;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEwayBillNo;
    private javax.swing.JTextArea txtEwayRemark;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JTextField txtIGSTamt;
    private javax.swing.JLabel txtIGSTcalcamt;
    private javax.swing.JLabel txtIGSTper;
    private javax.swing.JTextField txtITCCGST1;
    private javax.swing.JTextField txtITCCess1;
    private javax.swing.JTextField txtITCIGST1;
    private javax.swing.JTextField txtITCSGST1;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextArea txtRemarks;
    private javax.swing.JTextField txtSGSTamt;
    private javax.swing.JLabel txtSGSTcalcamt;
    private javax.swing.JLabel txtSGSTper;
    private javax.swing.JTextField txtState;
    private javax.swing.JTextField txtTotalAmt;
    private javax.swing.JTextField txtTotalWithTax;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC2Qty;
    // End of variables declaration//GEN-END:variables
}
