/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.sun.glass.events.KeyEvent;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.Tax;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CompanyUserRoleLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.TaxLogic;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ABS
 */
public class ServiceUI extends javax.swing.JFrame {

    /**
     * Creates new form ProductUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    AppUser user = SessionDataUtil.getSelectedUser();
    Long companyId = company.getCompanyId();
    CompanyUserRole companyUserRole = new CompanyUserRoleLogic().getCompanyUserRole(company, user);

    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isProfitEnable = company.getCompanyPolicy().isProfitEnabled();
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isService = true;

    public ServiceUI() {
        try {
            initComponents();
            btnAdd.setEnabled(true);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            checkTaxExclusive.setSelected(true);
            txtSearch.requestFocus();
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setExtendedState(this.MAXIMIZED_BOTH);
            labelCompanyName.setText(company.getCompanyName());
            labelUserName.setText(user.getUserName());
//        txtBasePriceUQC1.setVisible(isProfitEnable);
//        labelBasePrice.setVisible(isProfitEnable);
//        checkBasePriceTaxInclusive.setVisible(isProfitEnable);
            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            setTaxValue();
            fetch();

//setsystemPolicy();
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
        } catch (Exception ex) {
            Logger.getLogger(ServiceUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    public void fetch() {
        List<Product> products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }

        for (Product product : products) {
            if (product.isService()) {
                Object[] row = {
                    product.getProductId(),
                    product.getName(),
                    product.getTamilName(),
                    product.getShortName(),
                    product.getHsnCode(),
                    product.getProductCode(),
                    product.getBaseProductRate(),
                    product.isBasePriceInclusiveOfGST(),
                    product.getProductRate(),
                    product.isInclusiveOfGST(),
                    product.getIgstPercentage(),
                    product.getCgstPercentage(),
                    product.getSgstPercentage(),
                    product.isNillRated(),
                    product.isExempted(),
                    product.isNonGST(),
                    product.isService()
                };
                model.addRow(row);
            }
        }

        jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(8).setCellRenderer(new NumberTableCellRenderer());
//        if (isProfitEnable) {
//            jTable1.getColumnModel().getColumn(9).setCellRenderer(new NumberTableCellRenderer());
//        } else {
//            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
//        }
        searchClear();
    }

    public void setTaxValue() {
        List<Tax> taxes = new ArrayList<Tax>();
        taxes = new TaxLogic().fetchAllTaxes();
        Collections.sort(taxes, new Comparator<Tax>() {
            @Override
            public int compare(Tax o1, Tax o2) {
                return o1.getTaxRate() < o2.getTaxRate() ? -1 : (o1.getTaxRate() > o2.getTaxRate()) ? 1 : 0;
            }
        });
        for (Tax tax : taxes) {
            String s = Float.toString(tax.getTaxRate());
            comboCGST.addItem(s);
            comboIGST.addItem(s);
            comboSGST.addItem(s);

//            comboStockIGST.addItem(s);
//            comboStockCGST.addItem(s);
//            comboStockSGST.addItem(s);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem3 = new javax.swing.JMenuItem();
        jCheckBox1 = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        comboIGST = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        comboCGST = new javax.swing.JComboBox<>();
        comboSGST = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        ChecknonGst = new javax.swing.JCheckBox();
        CheckExempted = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtTamilName = new javax.swing.JTextField();
        txtName = new javax.swing.JTextField();
        txtHsncode = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtCode = new javax.swing.JTextField();
        txtShortName = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtPriceUQC1 = new javax.swing.JTextField();
        checkTaxInclusive = new javax.swing.JCheckBox();
        checkTaxExclusive = new javax.swing.JCheckBox();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        jMenuItem3.setText("jMenuItem3");

        jCheckBox1.setText("jCheckBox1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Service");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Service Name", "Tamil Name", "Short Name", "SAC Code", "Service code", "Base Price", "Base Price Tax Inclusive", "Price", "Tax Inclusive", "IGST%", "CGST%", "SGST%", "NillRated", "Exempted", "nonGST", "isService"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(0);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(100);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(11).setMinWidth(0);
            jTable1.getColumnModel().getColumn(11).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(11).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(0);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(14).setMinWidth(0);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(14).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(15).setMinWidth(0);
            jTable1.getColumnModel().getColumn(15).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(15).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
        }

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N

        txtSearch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel11.setText("IGST %");

        comboIGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboIGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboIGSTFocusGained(evt);
            }
        });
        comboIGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboIGSTKeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("CGST %");

        comboCGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboCGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboCGSTFocusGained(evt);
            }
        });
        comboCGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboCGSTKeyPressed(evt);
            }
        });

        comboSGST.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboSGST.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboSGSTFocusGained(evt);
            }
        });
        comboSGST.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboSGSTKeyPressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("SGST %");

        ChecknonGst.setText("nonGST");
        ChecknonGst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChecknonGstActionPerformed(evt);
            }
        });

        CheckExempted.setText("Exempted");
        CheckExempted.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckExemptedActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(CheckExempted)
                    .addComponent(ChecknonGst))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(comboIGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(comboCGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(comboSGST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CheckExempted)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ChecknonGst)
                .addContainerGap())
        );

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel8.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("SERVICE");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel24)
                        .addComponent(jButton1))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(labelCompanyName)
                        .addComponent(jLabel1))
                    .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtTamilName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtTamilName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTamilNameActionPerformed(evt);
            }
        });

        txtName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        txtHsncode.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtHsncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHsncodeActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("SAC Code");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Service Name in Regional Language (Optional)");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Short Name");

        txtCode.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodeActionPerformed(evt);
            }
        });

        txtShortName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtShortName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtShortNameActionPerformed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("Service Code");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel4.setText("Service Name");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(txtTamilName, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTamilName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtHsncode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtShortName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("Price");

        txtPriceUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPriceUQC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUQC1ActionPerformed(evt);
            }
        });
        txtPriceUQC1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceUQC1KeyTyped(evt);
            }
        });

        checkTaxInclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkTaxInclusive.setText("Tax Inclusive");
        checkTaxInclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxInclusiveItemStateChanged(evt);
            }
        });
        checkTaxInclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTaxInclusiveActionPerformed(evt);
            }
        });
        checkTaxInclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxInclusiveKeyPressed(evt);
            }
        });

        checkTaxExclusive.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        checkTaxExclusive.setText("Tax Exclusive");
        checkTaxExclusive.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                checkTaxExclusiveItemStateChanged(evt);
            }
        });
        checkTaxExclusive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTaxExclusiveActionPerformed(evt);
            }
        });
        checkTaxExclusive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                checkTaxExclusiveKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPriceUQC1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(checkTaxInclusive)
                            .addComponent(jLabel10)
                            .addComponent(checkTaxExclusive))
                        .addGap(0, 49, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPriceUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkTaxExclusive)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkTaxInclusive)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem4.setText("Payment");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem4);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 19, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 869, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 19, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtSearch)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPriceUQC1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPriceUQC1.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPriceUQC1KeyTyped

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        txtTamilName.requestFocus();
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtTamilNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTamilNameActionPerformed
        txtShortName.requestFocus();
    }//GEN-LAST:event_txtTamilNameActionPerformed

    private void txtShortNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtShortNameActionPerformed
        txtCode.requestFocus();
    }//GEN-LAST:event_txtShortNameActionPerformed

    private void txtHsncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHsncodeActionPerformed
        txtPriceUQC1.requestFocus();
    }//GEN-LAST:event_txtHsncodeActionPerformed

    private void txtPriceUQC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUQC1ActionPerformed
        checkTaxInclusive.requestFocus();
    }//GEN-LAST:event_txtPriceUQC1ActionPerformed

    private void comboIGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboIGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            float value = Float.parseFloat((String) comboIGST.getSelectedItem());
            String divValue = String.valueOf(value / 2);

            comboCGST.setSelectedItem(divValue);
            comboSGST.setSelectedItem(divValue);
            comboCGST.requestFocus();
        }
    }//GEN-LAST:event_comboIGSTKeyPressed

    private void comboCGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboCGSTKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            comboSGST.requestFocus();
        }
    }//GEN-LAST:event_comboCGSTKeyPressed

    private void comboSGSTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboSGSTKeyPressed
        int i = jTable1.getSelectedRow();
        if (btnAdd.isEnabled() == true) {
            if (i == -1) {
                btnAdd.requestFocus();
            }
        } else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_comboSGSTKeyPressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        addProduct();
    }//GEN-LAST:event_btnAddActionPerformed

    public Boolean checkSameProductName(String name, int pos) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Boolean isFound = false;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (i != pos) {
                if (model.getValueAt(i, 1).toString().equals(name)) {
                    isFound = true;
                }
            }
        }
        return isFound;
    }

    public void updateProduct() {
        int i = jTable1.getSelectedRow();

        if (i >= 0) {
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }

            String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String serviceNamefromTable = jTable1.getModel().getValueAt(i, 1).toString();
            String strHsnName = txtHsncode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

            Boolean isAlreadyHere = checkSameProductName(strProductName, i);
            if (strProductName.equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter the Service Name", "Alert", JOptionPane.WARNING_MESSAGE);
                txtName.requestFocus();
            } else if (isAlreadyHere) {
                JOptionPane.showMessageDialog(null, strProductName + " is already here. Please Enter another Service Name", "Alert", JOptionPane.WARNING_MESSAGE);
                txtName.requestFocus();
            } else {
                Long id = (Long) jTable1.getModel().getValueAt(i, 0);

                Product newProduct = new Product();

                newProduct.setProductId(id);
                newProduct.setName(strProductName);
                newProduct.setTamilName(txtTamilName.getText());
                newProduct.setShortName(txtShortName.getText());
                newProduct.setProductCode(txtCode.getText());
                newProduct.setHsnCode(strHsnName);
                newProduct.setIgstPercentage(Float.parseFloat((String) comboIGST.getSelectedItem()));
                newProduct.setCgstPercentage(Float.parseFloat((String) comboCGST.getSelectedItem()));
                newProduct.setSgstPercentage(Float.parseFloat((String) comboSGST.getSelectedItem()));

                newProduct.setInclusiveOfGST(checkTaxInclusive.isSelected());
                newProduct.setBasePriceInclusiveOfGST(true);

//                newProduct.setUQC1("BOXES");
//                newProduct.setUQC2("BOXES");
                newProduct.setUQC1("");
                newProduct.setUQC2("");

                newProduct.setUQC2Value(1);
                newProduct.setIsOpeningStockEditable(true);

                newProduct.setService(isService);

//                newProduct.setNillRated(CheckNillRated.isSelected());
                newProduct.setNonGST(ChecknonGst.isSelected());
                newProduct.setExempted(CheckExempted.isSelected());

                newProduct.setBaseProductRate(new BigDecimal("0.00"));
                newProduct.setProductRate(new BigDecimal(txtPriceUQC1.getText()));

                Boolean isSuccess;
                isSuccess = new ProductLogic().updateProduct(newProduct, null, companyId);

                if (isSuccess) {
                    JOptionPane.showMessageDialog(null, serviceNamefromTable + " is Successfully Updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clear();
                    fetch();
                } else {
                    JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void addProduct() {
        String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strHsnName = txtHsncode.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isAlreadyHere = checkSameProductName(strProductName, -1);
        if (strProductName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the Service Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
        } else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strProductName + " is already here. Please Enter another Service Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtName.requestFocus();
        } else {
            Product newProduct = new Product();

            newProduct.setName(strProductName);
            newProduct.setTamilName(txtTamilName.getText());
            newProduct.setShortName(txtShortName.getText());
            newProduct.setProductCode(txtCode.getText());
            newProduct.setHsnCode(strHsnName);

            newProduct.setIgstPercentage(Float.parseFloat((String) comboIGST.getSelectedItem()));
            newProduct.setCgstPercentage(Float.parseFloat((String) comboCGST.getSelectedItem()));
            newProduct.setSgstPercentage(Float.parseFloat((String) comboSGST.getSelectedItem()));

            newProduct.setBaseProductRate(new BigDecimal("0.00"));
            if (txtPriceUQC1.getText().isEmpty()) {
                newProduct.setProductRate(new BigDecimal("0.00"));
            } else {
                newProduct.setProductRate(new BigDecimal(txtPriceUQC1.getText()));
            }

            newProduct.setUQC1("");
            newProduct.setUQC2("");
            newProduct.setUQC2Value(1);

            newProduct.setInclusiveOfGST(checkTaxInclusive.isSelected());
            newProduct.setBasePriceInclusiveOfGST(true);
//            newProduct.setNillRated(CheckNillRated.isSelected());
            newProduct.setNonGST(ChecknonGst.isSelected());
            newProduct.setExempted(CheckExempted.isSelected());

            newProduct.setService(isService);

            // String strStock = newProduct.getStockValue(getWholeStock);
            Boolean isSuccess;
            isSuccess = new ProductLogic().createProduct(companyId, newProduct, null);

            if (isSuccess) { //Check this
                JOptionPane.showMessageDialog(null, "New Service " + strProductName + " is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                clear();
                fetch();
            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }
    }


    private void comboCGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboCGSTFocusGained
        comboCGST.showPopup();
    }//GEN-LAST:event_comboCGSTFocusGained

    private void comboSGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboSGSTFocusGained
        comboSGST.showPopup();
    }//GEN-LAST:event_comboSGSTFocusGained

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
        txtSearch.requestFocus();
    }//GEN-LAST:event_btnClearActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        btnAdd.setEnabled(false);
        btnUpdate.setEnabled(companyUserRole.isProductUpdate());
        btnDelete.setEnabled(companyUserRole.isProductDelete());
        int i = jTable1.getSelectedRow();
        if (jTable1.getRowSorter() != null) {
            i = jTable1.getRowSorter().convertRowIndexToModel(i);
        }
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        txtName.setText(model.getValueAt(i, 1).toString());
        txtTamilName.setText(model.getValueAt(i, 2).toString());
        txtShortName.setText(model.getValueAt(i, 3).toString());
        txtHsncode.setText(model.getValueAt(i, 4).toString());
        txtCode.setText(model.getValueAt(i, 5).toString());

//            txtBasePriceUQC1.setText(model.getValueAt(i, 6).toString());
//            checkBasePriceTaxInclusive.setSelected((Boolean) model.getValueAt(i, 7));
        txtPriceUQC1.setText(model.getValueAt(i, 8).toString());
        Boolean isTaxInclusive = (Boolean) model.getValueAt(i, 9);
        checkTaxInclusive.setSelected(isTaxInclusive);
//        checkTaxExclusive.setSelected(!isTaxInclusive);
        comboIGST.setSelectedItem(model.getValueAt(i, 10).toString());
        comboCGST.setSelectedItem(model.getValueAt(i, 11).toString());
        comboSGST.setSelectedItem(model.getValueAt(i, 12).toString());

        Boolean isNillRated = (Boolean) model.getValueAt(i, 13);
//            CheckNillRated.setSelected(isNillRated);
        Boolean isExempted = (Boolean) model.getValueAt(i, 14);
        CheckExempted.setSelected(isExempted);
        Boolean isNonGST = (Boolean) model.getValueAt(i, 15);
        ChecknonGst.setSelected(isNonGST);
        txtName.requestFocus();
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == java.awt.event.KeyEvent.VK_ENTER) {
            addProduct();
            evt.consume();
            txtSearch.requestFocus();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProduct();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();

        if (i >= 0) {
            if (jTable1.getRowSorter() != null) {
                i = jTable1.getRowSorter().convertRowIndexToModel(i);
            }
            Long id = (Long) jTable1.getModel().getValueAt(i, 0);

            Object[] options = {"Yes", "No"};
            int p = JOptionPane.showOptionDialog(null, "SURE YOU WANT TO DELETE ?", "CONFIRMATION", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
            if (p == JOptionPane.YES_OPTION) {
                String strProductName = txtName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                EventStatus result = new ProductLogic().deleteProduct(id);
                if (result.isDeleteDone()) {
                    JOptionPane.showMessageDialog(null, strProductName + " is Sucessfully Deleted", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clear();
                    fetch();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Delete has been cancelled", "Message", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select any Service", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(((DefaultTableModel) jTable1.getModel()));
        sorter.setRowFilter(RowFilter.regexFilter("(?i)" + txtSearch.getText(), 1));
        jTable1.setRowSorter(sorter);
        clearWithoutSearchBox();
    }//GEN-LAST:event_txtSearchKeyReleased

    private void txtPriceUQC1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyPressed

    }//GEN-LAST:event_txtPriceUQC1KeyPressed

    private void txtPriceUQC1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceUQC1KeyReleased


    }//GEN-LAST:event_txtPriceUQC1KeyReleased

    private void checkTaxInclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTaxInclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxInclusiveActionPerformed

    private void checkTaxInclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxInclusiveKeyPressed
        char vChar = evt.getKeyChar();
        if ((vChar == 'y') || (vChar == 'Y')) {
            checkTaxInclusive.setSelected(true);
        } else if ((vChar == 'n') || (vChar == 'N')) {
            checkTaxInclusive.setSelected(false);
        } else if (vChar == KeyEvent.VK_ENTER) {
            comboIGST.requestFocus();
        }
    }//GEN-LAST:event_checkTaxInclusiveKeyPressed

    private void comboIGSTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboIGSTFocusGained
        comboIGST.showPopup();
    }//GEN-LAST:event_comboIGSTFocusGained

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        updateProduct();
        evt.consume();
        txtSearch.requestFocus();
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void CheckExemptedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckExemptedActionPerformed
        if (CheckExempted.isSelected()) {
//            CheckNillRated.setSelected(false);
            ChecknonGst.setSelected(false);
        }
    }//GEN-LAST:event_CheckExemptedActionPerformed

    private void ChecknonGstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChecknonGstActionPerformed
        if (ChecknonGst.isSelected()) {
//            CheckNillRated.setSelected(false);
            CheckExempted.setSelected(false);
        }
    }//GEN-LAST:event_ChecknonGstActionPerformed


    private void txtCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodeActionPerformed
        txtHsncode.requestFocus();
    }//GEN-LAST:event_txtCodeActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtName.requestFocus();
        }
    }//GEN-LAST:event_txtSearchKeyPressed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void checkTaxExclusiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTaxExclusiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxExclusiveActionPerformed

    private void checkTaxExclusiveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_checkTaxExclusiveKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkTaxExclusiveKeyPressed

    private void checkTaxExclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxExclusiveItemStateChanged
        checkTaxInclusive.setSelected(!checkTaxExclusive.isSelected());
    }//GEN-LAST:event_checkTaxExclusiveItemStateChanged

    private void checkTaxInclusiveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_checkTaxInclusiveItemStateChanged
        checkTaxExclusive.setSelected(!checkTaxInclusive.isSelected());
    }//GEN-LAST:event_checkTaxInclusiveItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
                        new DashBoardUI().setVisible(true);
                        dispose();
                    } else if (isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardSilverUI().setVisible(true);
                        dispose();
                    } else if (!isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardCopperUI().setVisible(true);
                        dispose();
                    }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void clearWithoutSearchBox() {
        txtHsncode.setText("");
        txtName.setText("");
        txtPriceUQC1.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);

        jTable1.clearSelection();
        btnAdd.setEnabled(companyUserRole.isProductCreate());

//        CheckNillRated.setSelected(false);
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
    }

    public void clear() {
        txtHsncode.setText("");
        txtName.setText("");
//        txtBasePriceUQC1.setText("");
        txtPriceUQC1.setText("");
        txtShortName.setText("");
        txtTamilName.setText("");
        checkTaxInclusive.setSelected(false);
//        checkBasePriceTaxInclusive.setSelected(false);
        comboCGST.setSelectedIndex(0);
        comboIGST.setSelectedIndex(0);
        comboSGST.setSelectedIndex(0);
        jTable1.clearSelection();
        btnAdd.setEnabled(companyUserRole.isProductCreate());
//        CheckNillRated.setSelected(false);
        ChecknonGst.setSelected(false);
        CheckExempted.setSelected(false);
        txtCode.setText("");
        searchClear();
        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
    }

    public void searchClear() {
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(((DefaultTableModel) jTable1.getModel()));
        sorter.setRowFilter(RowFilter.regexFilter("", 1));
        jTable1.setRowSorter(sorter);
        txtSearch.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ServiceUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ServiceUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ServiceUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServiceUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ServiceUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckExempted;
    private javax.swing.JCheckBox ChecknonGst;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JCheckBox checkTaxExclusive;
    private javax.swing.JCheckBox checkTaxInclusive;
    private javax.swing.JComboBox<String> comboCGST;
    private javax.swing.JComboBox<String> comboIGST;
    private javax.swing.JComboBox<String> comboSGST;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtHsncode;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPriceUQC1;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtShortName;
    private javax.swing.JTextField txtTamilName;
    // End of variables declaration//GEN-END:variables
}
