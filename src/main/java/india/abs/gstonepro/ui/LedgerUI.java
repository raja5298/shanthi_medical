package india.abs.gstonepro.ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Admin
 */
import com.sun.glass.events.KeyEvent;
import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.Transaction;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.LedgerGroupLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PaymentLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;

public class LedgerUI extends javax.swing.JFrame {

    /**
     * Creates new form NewTree
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    List<LedgerGroup> ledgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
    DecimalFormat currency = new DecimalFormat("0.000");
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();

    public LedgerUI() {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setExtendedState(this.MAXIMIZED_BOTH);
            fetch();
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
            radioLedger.setSelected(true);
            labelLedgerORGroup.requestFocus();
            labelSelectedValue.setText("");
            labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            checkLedger(true, false);

            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
            Boolean isInBetweeFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, new Date(), false);
            if (!isInBetweeFinancialYear) {
                dateOpeningBalance.setDate(companyPolicy.getFinancialYearEnd());
                dateAddOpeningBalance.setDate(companyPolicy.getFinancialYearEnd());
            } else {
                dateOpeningBalance.setDate(companyPolicy.getFinancialYearStart());
                dateAddOpeningBalance.setDate(companyPolicy.getFinancialYearStart());
            }

            KeyStroke ctrlTab = KeyStroke.getKeyStroke("ctrl TAB");
            KeyStroke ctrlShiftTab = KeyStroke.getKeyStroke("ctrl shift TAB");

//         Remove ctrl-tab from normal focus traversal
            Set<AWTKeyStroke> forwardKeys = new HashSet<AWTKeyStroke>(tabbedPane.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
            forwardKeys.remove(ctrlTab);
            tabbedPane.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, forwardKeys);
//        comboProduct.requestFocus();

// Remove ctrl-shift-tab from normal focus traversal
            Set<AWTKeyStroke> backwardKeys = new HashSet<AWTKeyStroke>(tabbedPane.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
            backwardKeys.remove(ctrlShiftTab);
            tabbedPane.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, backwardKeys);

            KeyStroke right = KeyStroke.getKeyStroke("alt RIGHT");
            KeyStroke left = KeyStroke.getKeyStroke("alt LEFT");

            InputMap inputMap = tabbedPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            inputMap.put(right, "navigateNext");
            inputMap.put(left, "navigatePrevious");

            treeValue.setSelectionRow(0);

            ((JTextField) dateAddOpeningBalance.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateAddOpeningBalance.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateAddOpeningBalance.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateAddOpeningBalance.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            String strDate = ((JTextField) dateAddOpeningBalance.getDateEditor().getUiComponent()).getText();
                            sft.setLenient(false);
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                btnAdd.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            ((JTextField) dateOpeningBalance.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateOpeningBalance.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateOpeningBalance.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateOpeningBalance.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            String strDate = ((JTextField) dateOpeningBalance.getDateEditor().getUiComponent()).getText();
                            sft.setLenient(false);
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                btnUpdateOpeningBalance.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

        } catch (Exception ex) {
            Logger.getLogger(LedgerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    public static Date getFirstDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public void fetch() {
        DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) model.getRoot();
        rootNode.removeAllChildren();
        model.setRoot(rootNode);
        ledgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
        for (LedgerGroup lg : ledgerGroups) {
            if (lg.getParentLedgerGroup() == null) {
                TreeData ledger = new TreeData(lg.getLedgerGroupId(), lg.getLedgerGroupName(), false, true, new Ledger(), lg);
                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(ledger);
                model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
//                loadAllLedgerGroupsLedger(lg,newNode);
            }
        }
        if (rootNode.getChildCount() != 0) {
            treeValue.expandPath(new TreePath(rootNode.getPath()));
        }
        treeValue.setCellRenderer(new CountryTreeCellRenderer());
    }
//    public void loadAllLedgerGroupsLedger(LedgerGroup parentLedgerGroup,DefaultMutableTreeNode rootNode){
//        DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
//        rootNode.removeAllChildren();
//        model.setRoot(rootNode);
//        ledgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
//        for (LedgerGroup lg : ledgerGroups) {
//            if (lg.getParentLedgerGroup().toString().equals(parentLedgerGroup.toString())) {
//                TreeData ledger = new TreeData(lg.getLedgerGroupId(), lg.getLedgerGroupName(), false, true, new Ledger(), lg);
//                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(ledger);
//                model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
//                loadAllLedgerGroupsLedger(lg,newNode);
//            }
//        }
//        if (rootNode.getChildCount() != 0) {
//            treeValue.expandPath(new TreePath(rootNode.getPath()));
//        }
//        treeValue.setCellRenderer(new CountryTreeCellRenderer());    
//    }

    public LedgerGroup getSelectedGroup(Long id) {
        LedgerGroup getLG = new LedgerGroup();
        for (LedgerGroup lg : ledgerGroups) {
            if (lg.getLedgerGroupId() == id) {
                getLG = lg;
            }
        }
        return getLG;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radioGroupLedger = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeValue = new javax.swing.JTree();
        tabbedPane = new javax.swing.JTabbedPane();
        addPanel = new javax.swing.JPanel();
        labelAddPhone = new javax.swing.JLabel();
        comboAddTransactionType = new javax.swing.JComboBox<>();
        txtAddPhone = new javax.swing.JTextField();
        txtAddAddr1 = new javax.swing.JTextField();
        txtAddAddr2 = new javax.swing.JTextField();
        labelAddAddress = new javax.swing.JLabel();
        labelAddState = new javax.swing.JLabel();
        txtAddAddr3 = new javax.swing.JTextField();
        labelAddCity = new javax.swing.JLabel();
        txtAddCity = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        radioLedger = new javax.swing.JRadioButton();
        radioLedgerGroup = new javax.swing.JRadioButton();
        txtAddDistrict = new javax.swing.JTextField();
        labelAddDistrict = new javax.swing.JLabel();
        comboAddState = new javax.swing.JComboBox<>();
        labelAddMobile = new javax.swing.JLabel();
        txtAddMobile = new javax.swing.JTextField();
        labelAddLedgerORGroup = new javax.swing.JLabel();
        labelAddEmail = new javax.swing.JLabel();
        txtAddGroupName = new javax.swing.JTextField();
        txtAddEmail = new javax.swing.JTextField();
        labelAddOpeningBalance = new javax.swing.JLabel();
        txtAddOpeningBalance = new javax.swing.JTextField();
        labelAddDateAsOn = new javax.swing.JLabel();
        btnAddClear = new javax.swing.JButton();
        dateAddOpeningBalance = new com.toedter.calendar.JDateChooser();
        labelAddType = new javax.swing.JLabel();
        labelSelectedValue = new javax.swing.JLabel();
        managePanel = new javax.swing.JPanel();
        labelLedgerORGroup = new javax.swing.JLabel();
        txtGroupName = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        labelPhone = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        txtAddr1 = new javax.swing.JTextField();
        txtAddr2 = new javax.swing.JTextField();
        labelAddress = new javax.swing.JLabel();
        labelState = new javax.swing.JLabel();
        txtAddr3 = new javax.swing.JTextField();
        labelCity = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        txtDistrict = new javax.swing.JTextField();
        labelDistrict = new javax.swing.JLabel();
        comboState = new javax.swing.JComboBox<>();
        labelMobile = new javax.swing.JLabel();
        txtMobile = new javax.swing.JTextField();
        labelEmail = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        labelOpeningBalance = new javax.swing.JLabel();
        txtOpeningBalance = new javax.swing.JTextField();
        labelDateAsOn = new javax.swing.JLabel();
        dateOpeningBalance = new com.toedter.calendar.JDateChooser();
        labelType = new javax.swing.JLabel();
        comboTransactionType = new javax.swing.JComboBox<>();
        btnUpdateOpeningBalance = new javax.swing.JButton();
        btnClearOpeningBalance = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ledger");

        treeValue.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("BOOKS");
        treeValue.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeValue.setDragEnabled(true);
        treeValue.setDropMode(javax.swing.DropMode.ON);
        treeValue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeValueMouseClicked(evt);
            }
        });
        treeValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                treeValueKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                treeValueKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(treeValue);

        tabbedPane.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tabbedPane.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        tabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabbedPaneStateChanged(evt);
            }
        });
        tabbedPane.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabbedPaneKeyPressed(evt);
            }
        });

        labelAddPhone.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddPhone.setText("Phone Number ");

        comboAddTransactionType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Credit", "Debit" }));
        comboAddTransactionType.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboAddTransactionTypeFocusGained(evt);
            }
        });
        comboAddTransactionType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAddTransactionTypeActionPerformed(evt);
            }
        });
        comboAddTransactionType.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboAddTransactionTypeKeyPressed(evt);
            }
        });

        txtAddPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddPhoneActionPerformed(evt);
            }
        });
        txtAddPhone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAddPhoneKeyTyped(evt);
            }
        });

        txtAddAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddAddr1ActionPerformed(evt);
            }
        });

        txtAddAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddAddr2ActionPerformed(evt);
            }
        });

        labelAddAddress.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddAddress.setText("Address            ");

        labelAddState.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddState.setText("State");

        txtAddAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddAddr3ActionPerformed(evt);
            }
        });

        labelAddCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddCity.setText("City");

        txtAddCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddCityActionPerformed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        radioGroupLedger.add(radioLedger);
        radioLedger.setText("Ledger");
        radioLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioLedgerActionPerformed(evt);
            }
        });

        radioGroupLedger.add(radioLedgerGroup);
        radioLedgerGroup.setText("Ledger Group");
        radioLedgerGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioLedgerGroupActionPerformed(evt);
            }
        });

        txtAddDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddDistrictActionPerformed(evt);
            }
        });

        labelAddDistrict.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddDistrict.setText("District");

        comboAddState.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboAddState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboAddState.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboAddStateFocusGained(evt);
            }
        });
        comboAddState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAddStateActionPerformed(evt);
            }
        });
        comboAddState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboAddStateKeyPressed(evt);
            }
        });

        labelAddMobile.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddMobile.setText("Mobile Number ");

        txtAddMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddMobileActionPerformed(evt);
            }
        });
        txtAddMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAddMobileKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAddMobileKeyTyped(evt);
            }
        });

        labelAddLedgerORGroup.setText("Name");

        labelAddEmail.setText("Email");

        txtAddGroupName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddGroupNameActionPerformed(evt);
            }
        });
        txtAddGroupName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddGroupNameKeyPressed(evt);
            }
        });

        txtAddEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddEmailActionPerformed(evt);
            }
        });

        labelAddOpeningBalance.setText("Opening Balance");

        txtAddOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddOpeningBalanceActionPerformed(evt);
            }
        });
        txtAddOpeningBalance.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAddOpeningBalanceKeyTyped(evt);
            }
        });

        labelAddDateAsOn.setText("as on");

        btnAddClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAddClear.setMnemonic('c');
        btnAddClear.setText("Clear");
        btnAddClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddClearActionPerformed(evt);
            }
        });

        dateAddOpeningBalance.setDateFormatString("dd-MM-yyyy");

        labelAddType.setText("Type");

        labelSelectedValue.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelSelectedValue.setText("Selected");

        javax.swing.GroupLayout addPanelLayout = new javax.swing.GroupLayout(addPanel);
        addPanel.setLayout(addPanelLayout);
        addPanelLayout.setHorizontalGroup(
            addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelSelectedValue)
                    .addComponent(labelAddLedgerORGroup)
                    .addComponent(txtAddGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelAddAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(addPanelLayout.createSequentialGroup()
                        .addComponent(radioLedger)
                        .addGap(18, 18, 18)
                        .addComponent(radioLedgerGroup))
                    .addGroup(addPanelLayout.createSequentialGroup()
                        .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, addPanelLayout.createSequentialGroup()
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddClear, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtAddEmail, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAddAddr1)
                            .addComponent(labelAddCity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelAddEmail, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelAddType, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboAddTransactionType, javax.swing.GroupLayout.Alignment.LEADING, 0, 198, Short.MAX_VALUE)
                            .addComponent(txtAddCity, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(18, 18, 18)
                        .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(addPanelLayout.createSequentialGroup()
                                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtAddDistrict)
                                        .addComponent(txtAddAddr2)
                                        .addComponent(labelAddDistrict)
                                        .addComponent(txtAddMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(labelAddMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelAddPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(labelAddState, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(comboAddState, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtAddPhone)
                                        .addComponent(txtAddAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(labelAddOpeningBalance)
                            .addGroup(addPanelLayout.createSequentialGroup()
                                .addComponent(txtAddOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(3, 3, 3)
                                .addComponent(labelAddDateAsOn)
                                .addGap(3, 3, 3)
                                .addComponent(dateAddOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(82, Short.MAX_VALUE))
        );

        addPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnAddClear});

        addPanelLayout.setVerticalGroup(
            addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addPanelLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(labelSelectedValue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioLedger)
                    .addComponent(radioLedgerGroup))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelAddLedgerORGroup)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelAddAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAddCity)
                    .addComponent(labelAddDistrict)
                    .addComponent(labelAddState))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboAddState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAddEmail)
                    .addComponent(labelAddMobile)
                    .addComponent(labelAddPhone))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAddType)
                    .addComponent(labelAddOpeningBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboAddTransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtAddOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelAddDateAsOn, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dateAddOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(addPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAddClear)
                    .addComponent(btnAdd))
                .addContainerGap(69, Short.MAX_VALUE))
        );

        tabbedPane.addTab("New", addPanel);

        labelLedgerORGroup.setText("Name");

        txtGroupName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGroupNameActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.setPreferredSize(new java.awt.Dimension(80, 23));
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.setPreferredSize(new java.awt.Dimension(80, 23));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setText("Clear");
        btnClear.setPreferredSize(new java.awt.Dimension(80, 23));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        labelPhone.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelPhone.setText("Phone Number ");

        txtPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhoneActionPerformed(evt);
            }
        });
        txtPhone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPhoneKeyTyped(evt);
            }
        });

        txtAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr1ActionPerformed(evt);
            }
        });

        txtAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr2ActionPerformed(evt);
            }
        });

        labelAddress.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelAddress.setText("Address            ");

        labelState.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelState.setText("State");

        txtAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr3ActionPerformed(evt);
            }
        });

        labelCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelCity.setText("City");

        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });

        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });

        labelDistrict.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelDistrict.setText("District");

        comboState.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboState.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboStateFocusGained(evt);
            }
        });
        comboState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboStateActionPerformed(evt);
            }
        });
        comboState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboStateKeyPressed(evt);
            }
        });

        labelMobile.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelMobile.setText("Mobile Number ");

        txtMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileActionPerformed(evt);
            }
        });
        txtMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMobileKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMobileKeyTyped(evt);
            }
        });

        labelEmail.setText("Email");

        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });

        labelOpeningBalance.setText("Opening Balance");

        txtOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOpeningBalanceActionPerformed(evt);
            }
        });
        txtOpeningBalance.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtOpeningBalanceKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOpeningBalanceKeyTyped(evt);
            }
        });

        labelDateAsOn.setText("as on");

        dateOpeningBalance.setDateFormatString("dd-MM-yyyy");

        labelType.setText("Type");

        comboTransactionType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Credit", "Debit" }));
        comboTransactionType.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboTransactionTypeFocusGained(evt);
            }
        });
        comboTransactionType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTransactionTypeActionPerformed(evt);
            }
        });
        comboTransactionType.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboTransactionTypeKeyPressed(evt);
            }
        });

        btnUpdateOpeningBalance.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdateOpeningBalance.setText("Update Opening Balance");
        btnUpdateOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateOpeningBalanceActionPerformed(evt);
            }
        });

        btnClearOpeningBalance.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClearOpeningBalance.setText("Clear");
        btnClearOpeningBalance.setPreferredSize(new java.awt.Dimension(80, 23));
        btnClearOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearOpeningBalanceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout managePanelLayout = new javax.swing.GroupLayout(managePanel);
        managePanel.setLayout(managePanelLayout);
        managePanelLayout.setHorizontalGroup(
            managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(managePanelLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(managePanelLayout.createSequentialGroup()
                        .addComponent(btnUpdateOpeningBalance)
                        .addGap(18, 18, 18)
                        .addComponent(btnClearOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(managePanelLayout.createSequentialGroup()
                        .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelType)
                            .addComponent(comboTransactionType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelOpeningBalance)
                            .addGroup(managePanelLayout.createSequentialGroup()
                                .addComponent(txtOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(3, 3, 3)
                                .addComponent(labelDateAsOn)
                                .addGap(3, 3, 3)
                                .addComponent(dateOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(managePanelLayout.createSequentialGroup()
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelLedgerORGroup)
                    .addComponent(labelAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(managePanelLayout.createSequentialGroup()
                        .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(labelCity, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelEmail)
                            .addComponent(txtAddr1)
                            .addComponent(txtCity))
                        .addGap(18, 18, 18)
                        .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtDistrict)
                                .addComponent(labelDistrict)
                                .addComponent(txtMobile, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                                .addComponent(txtAddr2))
                            .addComponent(labelMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(labelState, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboState, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtPhone)
                                .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(txtGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        managePanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnClear, btnClearOpeningBalance, btnDelete, btnUpdate});

        managePanelLayout.setVerticalGroup(
            managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(managePanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(labelLedgerORGroup)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelCity)
                    .addComponent(labelDistrict)
                    .addComponent(labelState))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEmail)
                    .addComponent(labelMobile)
                    .addComponent(labelPhone))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelType)
                    .addComponent(labelOpeningBalance))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboTransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelDateAsOn, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dateOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(managePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdateOpeningBalance)
                    .addComponent(btnClearOpeningBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        tabbedPane.addTab("Manage", managePanel);

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel17.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setText("LEDGER");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel24))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(labelCompanyName)
                        .addComponent(jLabel3))
                    .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(tabbedPane))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tabbedPane)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private Boolean checkLedgerGroup(String name) {
        Boolean isHere = false;
        List<LedgerGroup> allLedgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
        for (LedgerGroup lg : allLedgerGroups) {
            if (lg.getLedgerGroupName().equals(name)) {
                isHere = true;
            }
        }
        return isHere;
    }

    public void addNewLedgerOrLedgerGroup() {
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateAddOpeningBalance.getDate());
        }
        if (isNotExpired) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
            if (treeValue.isSelectionEmpty() || node.isRoot()) {
                JOptionPane.showMessageDialog(null, "Please Select the Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
            } else if (txtAddGroupName.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Please add Name", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateAddOpeningBalance.getDate(), false);
                if (!isInBetweenFinancialYear) {
                    String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                            + " to <b>" + strFinancialYearEnd + "</b></html>";
                    JLabel label = new JLabel(msg);
                    JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
                } else {
                    Boolean isPhoneCorrect = checkPhoneValid(txtAddPhone.getText());
                    Boolean isEmailValid = checkEmailID(txtAddEmail.getText());
                    Boolean isMobileValid = checkMobileValid(txtAddMobile.getText());
                    if (isEmailValid) {
                        JOptionPane.showMessageDialog(null, "Invalid Email ID", "Alert", JOptionPane.WARNING_MESSAGE);
                        txtAddEmail.requestFocus();
                    } else if (isMobileValid) {
                        JOptionPane.showMessageDialog(null, "Please enter valid Mobile No", "Alert", JOptionPane.WARNING_MESSAGE);
                        txtAddMobile.requestFocus();
                    } else if (isPhoneCorrect) {
                        JOptionPane.showMessageDialog(null, "Please enter valid Phone No \nPhone Number must be in the form XXX-XXXXXXX", "Alert", JOptionPane.WARNING_MESSAGE);
                        txtAddPhone.requestFocus();
                    } else {
                        if (radioLedgerGroup.isSelected()) {
                            String ledgerGroupName = txtAddGroupName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            Boolean isAvailable = checkLedgerGroup(ledgerGroupName);
                            if (ledgerGroupName.equals("")) {
                                JOptionPane.showMessageDialog(null, "Please enter the Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
                                txtAddGroupName.requestFocus();
                            } else if (isAvailable) {
                                JOptionPane.showMessageDialog(null, ledgerGroupName + " is already here", "Alert", JOptionPane.WARNING_MESSAGE);
                                txtAddGroupName.requestFocus();
                            } else {
                                DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();

                                Audit newAudit = new Audit();
                                newAudit.setCreatedBy(SessionDataUtil.getSelectedUser());
                                LedgerGroup newLedgerGroup = new LedgerGroup();

                                newLedgerGroup.setAuditData(newAudit);
                                newLedgerGroup.setCompany(SessionDataUtil.getSelectedCompany());
                                newLedgerGroup.setEligibleForChildren(true);
                                newLedgerGroup.setDeletable(true);
                                newLedgerGroup.setEditable(true);
                                newLedgerGroup.setLedgerGroupName(ledgerGroupName);
                                if (!treeValue.isSelectionEmpty()) {
                                    DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
                                    Object o = parentNode.getUserObject();
                                    if (parentNode != treeValue.getModel().getRoot()) {
                                        if (o instanceof TreeData) {
                                            TreeData data = (TreeData) o;
                                            newLedgerGroup.setParentLedgerGroup(getSelectedGroup(data.getId()));
                                        }
                                    }
                                }
                                LedgerGroup resultGroup = new LedgerGroupLogic().createLedgerGroup(newLedgerGroup);

                                if (resultGroup != null) {
                                    JOptionPane.showMessageDialog(null, ledgerGroupName + "is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                                    if (treeValue.isSelectionEmpty()) {
                                        TreeData data = new TreeData(resultGroup.getLedgerGroupId(), resultGroup.getLedgerGroupName(), false, true, new Ledger(), resultGroup);
                                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(data);
                                        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) treeValue.getModel().getRoot();
                                        model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
                                    } else {
                                        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
                                        TreeData data = new TreeData(resultGroup.getLedgerGroupId(), resultGroup.getLedgerGroupName(), false, true, new Ledger(), resultGroup);
                                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(data);
                                        model.insertNodeInto(newNode, parentNode, parentNode.getChildCount());
                                    }

                                    clearAdd();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                                }
                            }
                        } else if (radioLedger.isSelected()) {
                            DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                            String name = txtAddGroupName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                            String getGroupName = "";

                            if (!treeValue.isSelectionEmpty()) {

                                DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();

                                Object o = parentNode.getUserObject();
                                TreeData parentData = (TreeData) o;
                                if (o instanceof TreeData) {
                                    parentData = (TreeData) o;
                                    getGroupName = parentData.getName();
                                }

                                String addr1 = txtAddAddr1.getText(), addr2 = txtAddAddr2.getText(), addr3 = txtAddAddr3.getText();
                                if (addr1.equals("")) {
                                    addr1 = " ";
                                }
                                if (addr2.equals("")) {
                                    addr2 = " ";
                                }
                                if (addr3.equals("")) {
                                    addr3 = " ";
                                }
                                float openingAmount = 0;
                                String stropeningAmount = txtAddOpeningBalance.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                                if (!stropeningAmount.equals("")) {
                                    openingAmount = Float.parseFloat(currency.format(Float.parseFloat(stropeningAmount)));
                                    if (comboAddTransactionType.getSelectedItem().equals("Debit")) {
                                        openingAmount = (-1) * openingAmount;
                                    }
                                }
                                String Address = addr1 + "/~/" + addr2 + "/~/" + addr3;

                                Ledger newLedger = new Ledger();
                                newLedger.setLedgerName(name);
                                newLedger.setAddress(Address);
                                newLedger.setCity(txtAddCity.getText());
                                newLedger.setDistrict(txtAddDistrict.getText());
                                newLedger.setState((String) comboAddState.getSelectedItem());
                                newLedger.setEmail(txtAddEmail.getText());
                                newLedger.setPhone(txtAddPhone.getText());
                                newLedger.setMobile(txtAddMobile.getText());
                                newLedger.setOpeningBalanceDate(dateAddOpeningBalance.getDate());
                                newLedger.setOpeningBalance(convertDecimal.Currency(currency.format(openingAmount)));
//                newLedger.setLedgerGroupName(getGroupName);
                                newLedger.setLedgerGroup(getSelectedGroup(parentData.getId()));
                                newLedger.setEligibleForChildren(true);
                                newLedger.setEditable(true);
                                newLedger.setDeletable(true);

                                Ledger resultLedger = new LedgerLogic().createLedger(SessionDataUtil.getSelectedCompany().getCompanyId(), newLedger);
                                if (resultLedger != null) { //Check this
                                    JOptionPane.showMessageDialog(null, "New Ledger " + name + " is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                                    TreeData data = new TreeData(resultLedger.getLedgerId(), resultLedger.getLedgerName(), true, false, resultLedger, new LedgerGroup());
                                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(data);
                                    model.insertNodeInto(newNode, parentNode, parentNode.getChildCount());
                                    clearAdd();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                                }

                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Please select the Ledger or Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            }
        } else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }

    public Boolean checkEmailID(String email) {
        Boolean isWrong = true;
        Pattern patternEmail = Pattern.compile(
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcherEmail = patternEmail.matcher(email);
        if (matcherEmail.matches() || email.equals("")) {
            isWrong = false;
        }

        return isWrong;
    }

    public Boolean checkPhoneValid(String Phone) {
        Boolean isWrong = false;
        if (!Phone.equals("")) {
            if (!Phone.contains("-")) {
                isWrong = true;
            }
        }
//        if ((!txtPhone.getText().contains("-")) || !txtPhone.getText().equals("")) {
//            isWrong = true;
//        }
        return isWrong;
    }

    public Boolean checkMobileValid(String Mobile) {
        Boolean isWrong = false;
        if (!Mobile.equals("")) {
            if (Mobile.length() != 10) {
                isWrong = true;
            }
        }
        return isWrong;
    }

    public void updateLedgerOrLedgerGroups() {

        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate(), false);
        if (!isInBetweenFinancialYear) {
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
            if (!treeValue.isSelectionEmpty()) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
                Object o = node.getUserObject();
                if (o instanceof TreeData) {
                    TreeData getData = (TreeData) o;
                    if (getData.isLedgerGroup) {
                        String ledgerGroupName = txtGroupName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        Boolean isAvailable = checkLedgerGroup(ledgerGroupName);
                        if (ledgerGroupName.equals("")) {
                            JOptionPane.showMessageDialog(null, "Please enter the Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
                            txtGroupName.requestFocus();
                        } else if (isAvailable) {
                            JOptionPane.showMessageDialog(null, "Ledger Group is already here", "Alert", JOptionPane.WARNING_MESSAGE);
                            txtGroupName.requestFocus();
                        } else {
                            for (LedgerGroup lg : ledgerGroups) {

                                if (lg.getLedgerGroupId() == getData.getId()) {
                                    Boolean isSuccess = new LedgerGroupLogic().updateLedgerGroup(getData.getId(), ledgerGroupName, lg.getLedgers());
                                    if (isSuccess) { //Check this
                                        JOptionPane.showMessageDialog(null, "Updated Successfully", "Message", JOptionPane.WARNING_MESSAGE);
                                        clear();
                                        lg.setLedgerGroupName(ledgerGroupName);
                                        TreeData updateData = new TreeData(lg.getLedgerGroupId(), lg.getLedgerGroupName(), false, true, new Ledger(), lg);
                                        node.setUserObject(updateData);
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                                    }
                                    treeValue.removeSelectionPath(treeValue.getSelectionPath());
                                    clear();
                                }
                            }
                        }
                    } else if (getData.isLedger) {
                        Boolean isPhoneCorrect = checkPhoneValid(txtPhone.getText());
                        Boolean isEmailValid = checkEmailID(txtEmail.getText());
                        Boolean isMobileValid = checkMobileValid(txtMobile.getText());
                        if (isEmailValid) {
                            JOptionPane.showMessageDialog(null, "Invalid Email ID", "Alert", JOptionPane.WARNING_MESSAGE);
                            txtEmail.requestFocus();
                        } else if (isMobileValid) {
                            JOptionPane.showMessageDialog(null, "Please enter valid Mobile No", "Alert", JOptionPane.WARNING_MESSAGE);
                            txtMobile.requestFocus();
                        } else if (isPhoneCorrect) {
                            JOptionPane.showMessageDialog(null, "Please enter valid Phone No \nPhone Number must be in the form XXX-XXXXXXX", "Alert", JOptionPane.WARNING_MESSAGE);
                            txtPhone.requestFocus();
                        } else {

                            Ledger updatLedger = getData.getLedger();
                            String addr1 = txtAddr1.getText(), addr2 = txtAddr2.getText(), addr3 = txtAddr3.getText();

                            if (addr1.equals("")) {
                                addr1 = " ";
                            }
                            if (addr2.equals("")) {
                                addr2 = " ";
                            }
                            if (addr3.equals("")) {
                                addr3 = " ";
                            }
                            float openingAmount = 0;
                            String stropeningAmount = txtOpeningBalance.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            if (!stropeningAmount.equals("")) {
                                openingAmount = Float.parseFloat(currency.format(Float.parseFloat(stropeningAmount)));
                                if (comboTransactionType.getSelectedItem().equals("Debit")) {
                                    openingAmount = (-1) * openingAmount;
                                }
                            }
                            String Address = addr1 + "/~/" + addr2 + "/~/" + addr3;

                            updatLedger.setLedgerName(txtGroupName.getText());
                            updatLedger.setAddress(Address);
                            updatLedger.setCity(txtCity.getText());
                            updatLedger.setDistrict(txtDistrict.getText());
                            updatLedger.setState((String) comboState.getSelectedItem());
                            updatLedger.setEmail(txtEmail.getText());
                            updatLedger.setPhone(txtPhone.getText());
                            updatLedger.setMobile(txtMobile.getText());
//                            updatLedger.setOpeningBalanceDate(dateOpeningBalance.getDate());
//                            updatLedger.setOpeningBalance(convertDecimal.Currency(currency.format(openingAmount)));

                            Boolean isSuccess = new LedgerLogic().updateLedger(updatLedger);
                            if (isSuccess) { //Check this
                                JOptionPane.showMessageDialog(null, "Successfully Updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                                TreeData updateData = new TreeData(updatLedger.getLedgerId(), updatLedger.getLedgerName(), true, false, updatLedger, new LedgerGroup());
                                node.setUserObject(updateData);
                            } else {
                                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                            }
                            treeValue.removeSelectionPath(treeValue.getSelectionPath());
                            clear();
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please select the Ledger or Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void clear() {
        txtGroupName.setText("");
        txtAddr1.setText("");
        txtAddr2.setText("");
        txtAddr3.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        comboState.setSelectedItem("Tamil Nadu");
        txtEmail.setText("");
        txtMobile.setText("");
        txtPhone.setText("");
        treeValue.setCellRenderer(new CountryTreeCellRenderer());
        treeValue.removeSelectionPath(treeValue.getSelectionPath());
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        comboTransactionType.setSelectedItem("Credit");
        txtOpeningBalance.setText("");
        dateOpeningBalance.setDate(companyPolicy.getFinancialYearStart());
        Boolean isInBetweeFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate(), false);
        if (!isInBetweeFinancialYear) {
            dateOpeningBalance.setDate(companyPolicy.getFinancialYearEnd());
        }
        checkLedger(true, false);
    }

    public void clearAdd() {
        txtAddGroupName.setText("");
        txtAddAddr1.setText("");
        txtAddAddr2.setText("");
        txtAddAddr3.setText("");
        txtAddCity.setText("");
        txtAddDistrict.setText("");
        comboAddState.setSelectedItem("Tamil Nadu");
        txtAddEmail.setText("");
        txtAddMobile.setText("");
        txtAddPhone.setText("");
        dateAddOpeningBalance.setDate(companyPolicy.getFinancialYearStart());
        txtAddOpeningBalance.setText("");
        treeValue.setCellRenderer(new CountryTreeCellRenderer());
        Boolean isInBetweeFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateAddOpeningBalance.getDate(), false);
        if (!isInBetweeFinancialYear) {
            dateAddOpeningBalance.setDate(companyPolicy.getFinancialYearEnd());
        }
    }

    private void elibleForAddLedgerGroup(Boolean isLedgerGroup) {
        if (isLedgerGroup) {
            radioLedgerGroup.setEnabled(true);
            radioLedgerGroup.setSelected(true);
            btnAdd.setEnabled(true);
        } else {
            radioLedger.setSelected(true);
            radioLedgerGroup.setEnabled(false);
            btnAdd.setEnabled(false);
        }
        checkRadioLedger();
    }
    private void treeValueMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeValueMouseClicked
        if (!treeValue.isSelectionEmpty()) {
//            getNodeDetails();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();

            Object o = node.getUserObject();
            if (o instanceof TreeData) {
                TreeData getData = (TreeData) o;
                if (getData.isLedgerGroup) {
                    labelSelectedValue.setText("Selected Ledger Group : " + getData.getName());
                    buttonEnable(getData.getLedgerGroup().isEligibleForChildren(), getData.getLedgerGroup().isEditable());
                    labelLedgerORGroup.setText("Ledger Group");
                    checkLedger(false, false);
                    DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                    txtGroupName.setText(getData.getName());
                    if (node.getChildCount() == 0) {
                        for (LedgerGroup lg : ledgerGroups) {
                            if (lg.getParentLedgerGroup() != null) {
                                if (getData.getId() == lg.getParentLedgerGroup().getLedgerGroupId()) {
                                    TreeData ledger = new TreeData(lg.getLedgerGroupId(), lg.getLedgerGroupName(), false, true, new Ledger(), lg);
                                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(ledger);
                                    model.insertNodeInto(newNode, node, node.getChildCount());
                                }
                            }

                        }
                        for (Ledger l : getData.getLedgerGroup().getLedgers()) {
                            TreeData treeData = new TreeData(l.getLedgerId(), l.getLedgerName(), true, false, l, new LedgerGroup());
                            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(treeData);
                            model.insertNodeInto(newNode, node, node.getChildCount());
                        }
                    }
                    if (node.getChildCount() != 0) {
                        treeValue.expandPath(new TreePath(node.getPath()));
                    }
                    treeValue.setCellRenderer(new CountryTreeCellRenderer());
                } else if (getData.isLedger) {
                    buttonEnable(false, getData.getLedger().isEditable());
//                    buttonEnable(getData.getLedger().isEligibleForChildren(), getData.getLedger().isEditable());
//                    elibleForAddLedgerGroup(false);
                    labelLedgerORGroup.setText("Ledger");
                    labelSelectedValue.setText("Selected Ledger : " + getData.getName());

                    checkLedger(true, getData.getLedger().isEditable());
                    txtGroupName.setText(getData.getLedger().getLedgerName());
                    String address = " /~/ /~/ ";
                    if (getData.getLedger().getAddress() != null) {
                        address = getData.getLedger().getAddress();
                    }
                    String[] words = address.split("/~/");
                    txtAddr1.setText(words[0].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtAddr2.setText(words[1].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtAddr3.setText(words[2].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtCity.setText(getData.getLedger().getCity());
                    txtDistrict.setText(getData.getLedger().getDistrict());
                    comboState.setSelectedItem(getData.getLedger().getState());
                    txtEmail.setText(getData.getLedger().getEmail());
                    txtMobile.setText(getData.getLedger().getMobile());
                    txtPhone.setText(getData.getLedger().getPhone());
                    dateOpeningBalance.setDate(getData.getLedger().getOpeningBalanceDate());

                    float Amount = 0;
                    if (getData.getLedger().getOpeningBalance() != null) {
                        Amount = Float.parseFloat(currency.format(getData.getLedger().getOpeningBalance()));
                    }
                    if (Amount < 0) {
                        Amount = -1 * Amount;
                        comboTransactionType.setSelectedItem("Debit");
                    } else {
                        comboTransactionType.setSelectedItem("Credit");
                    }
                    txtOpeningBalance.setText(currency.format(Amount));
                    DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                    if (node.getChildCount() == 0) {
                        for (Ledger l : getData.getLedger().getChildrenLedgers()) {
                            TreeData treeData = new TreeData(l.getLedgerId(), l.getLedgerName(), true, false, l, new LedgerGroup());
                            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(treeData);
                            model.insertNodeInto(newNode, node, node.getChildCount());
                        }
                    }
                    if (node.getChildCount() != 0) {
                        treeValue.expandPath(new TreePath(node.getPath()));
                    }
                }
            } else {
                txtGroupName.setText("");
            }
        } else {
            labelSelectedValue.setText("");
        }
        treeValue.requestFocus();
    }//GEN-LAST:event_treeValueMouseClicked
    public void buttonEnable(Boolean eligibleForChildren, Boolean isEditable) {
        btnAdd.setEnabled(eligibleForChildren);
        btnUpdate.setEnabled(isEditable);
        btnDelete.setEnabled(isEditable);
        btnUpdateOpeningBalance.setEnabled(isEditable);
        btnClearOpeningBalance.setEnabled(isEditable);
    }

//    public void getNodeDetails() {
//        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
//        Object o = node.getUserObject();
//        if (o instanceof TreeData) {
//
//        }
//    }
    public void childrenLedgerGroupLoad() {

    }
    private void treeValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_treeValueKeyReleased

        if (!treeValue.isSelectionEmpty()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
            Object o = node.getUserObject();
            if (o instanceof TreeData) {
                TreeData getData = (TreeData) o;
                if (getData.isLedgerGroup) {

                    labelSelectedValue.setText("Selected Ledger Group : " + getData.getName());
                    txtGroupName.setText(getData.getName());
                    labelLedgerORGroup.setText("Ledger Group");
                    checkLedger(false, false);
                    buttonEnable(getData.getLedgerGroup().isEligibleForChildren(), getData.getLedgerGroup().isEditable());
                    if (evt.getKeyCode() == 39) {
                        DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                        if (node.getChildCount() == 0) {
                            for (LedgerGroup lg : ledgerGroups) {
                                if (lg.getParentLedgerGroup() != null) {
                                    if (getData.getId() == lg.getParentLedgerGroup().getLedgerGroupId()) {
                                        TreeData ledger = new TreeData(lg.getLedgerGroupId(), lg.getLedgerGroupName(), false, true, new Ledger(), lg);
                                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(ledger);
                                        model.insertNodeInto(newNode, node, node.getChildCount());
                                    }
                                }
                            }

                            for (Ledger l : getData.getLedgerGroup().getLedgers()) {
                                TreeData treeData = new TreeData(l.getLedgerId(), l.getLedgerName(), true, false, l, new LedgerGroup());
                                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(treeData);
                                model.insertNodeInto(newNode, node, node.getChildCount());
                            }

                            if (node.getChildCount() != 0) {
                                treeValue.expandPath(new TreePath(node.getPath()));
                            }
                        }
                    }
                } else if (getData.getIsLedger()) {
                    buttonEnable(false, getData.getLedger().isEditable());
//                    buttonEnable(getData.getLedger().isEligibleForChildren(), getData.getLedger().isEditable());
//                    elibleForAddLedgerGroup(false);
                    labelLedgerORGroup.setText("Ledger");
                    labelSelectedValue.setText("Selected Ledger : " + getData.getName());
                    checkLedger(true, getData.getLedger().isEditable());

                    txtGroupName.setText(getData.getLedger().getLedgerName());
                    String address = " /~/ /~/ ";
                    if (getData.getLedger().getAddress() != null) {
                        address = getData.getLedger().getAddress();
                    }
                    String[] words = address.split("/~/");
                    txtAddr1.setText(words[0].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtAddr2.setText(words[1].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtAddr3.setText(words[2].replaceAll("^\\s+", "").replaceAll("\\s+$", ""));
                    txtCity.setText(getData.getLedger().getCity());
                    txtDistrict.setText(getData.getLedger().getDistrict());
                    comboState.setSelectedItem(getData.getLedger().getState());
                    txtEmail.setText(getData.getLedger().getEmail());
                    txtMobile.setText(getData.getLedger().getMobile());
                    txtPhone.setText(getData.getLedger().getPhone());
                    dateOpeningBalance.setDate(getData.getLedger().getOpeningBalanceDate());
                    float Amount = 0;
                    if (getData.getLedger().getOpeningBalance() != null) {
                        Amount = Float.parseFloat(currency.format(getData.getLedger().getOpeningBalance()));
                    }

                    if (Amount < 0) {
                        Amount = -1 * Amount;
                        comboTransactionType.setSelectedItem("Debit");
                    } else {
                        comboTransactionType.setSelectedItem("Credit");
                    }
                    txtOpeningBalance.setText(currency.format(Amount));
                    if (evt.getKeyCode() == 39) {

                        DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                        if (node.getChildCount() == 0) {
                            for (Ledger l : getData.getLedger().getChildrenLedgers()) {
                                TreeData treeData = new TreeData(l.getLedgerId(), l.getLedgerName(), true, false, l, new LedgerGroup());
                                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(treeData);
                                model.insertNodeInto(newNode, node, node.getChildCount());
                            }
                            if (node.getChildCount() != 0) {
                                treeValue.expandPath(new TreePath(node.getPath()));
                            }
                        }
                    }

                }
                treeValue.setCellRenderer(new CountryTreeCellRenderer());
            } else {
                txtGroupName.setText("");
            }
        } else {
            labelSelectedValue.setText("");
        }
        treeValue.requestFocus();
    }//GEN-LAST:event_treeValueKeyReleased


    private void treeValueKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_treeValueKeyPressed

        if (evt.isControlDown() && evt.getKeyCode() == 39) {
            tabbedPane.requestFocus();
            tabbedPane.setSelectedIndex(0);

        }
    }//GEN-LAST:event_treeValueKeyPressed

    private void tabbedPaneKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabbedPaneKeyPressed

    }//GEN-LAST:event_tabbedPaneKeyPressed

    private void tabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabbedPaneStateChanged
        if (tabbedPane.getSelectedIndex() == 0) {
            txtAddGroupName.requestFocus();
        } else if (tabbedPane.getSelectedIndex() == 1) {
            txtGroupName.requestFocus();
        }
    }//GEN-LAST:event_tabbedPaneStateChanged

    private void btnAddClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddClearActionPerformed
        clearAdd();
    }//GEN-LAST:event_btnAddClearActionPerformed

    private void txtAddOpeningBalanceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddOpeningBalanceKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtAddOpeningBalance.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtAddOpeningBalanceKeyTyped

    private void txtAddOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddOpeningBalanceActionPerformed
        btnAdd.requestFocus();
    }//GEN-LAST:event_txtAddOpeningBalanceActionPerformed

    private void txtAddEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddEmailActionPerformed
        txtAddMobile.requestFocus();
    }//GEN-LAST:event_txtAddEmailActionPerformed

    private void txtAddGroupNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddGroupNameKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAddGroupNameKeyPressed

    private void txtAddGroupNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddGroupNameActionPerformed
        if (radioLedgerGroup.isSelected() == true) {
            btnAdd.requestFocus();
        } else {
            txtAddAddr1.requestFocus();
        }
    }//GEN-LAST:event_txtAddGroupNameActionPerformed

    private void txtAddMobileKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddMobileKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtAddMobileKeyTyped

    private void txtAddMobileKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddMobileKeyReleased
        String mobile = txtAddMobile.getText();
        if (mobile.length() <= 10) {

        } else {
            JOptionPane.showMessageDialog(null, "Enter 10 Digit Number only", "Alert", JOptionPane.WARNING_MESSAGE);
            mobile = mobile.substring(0, 10);
            txtAddMobile.setText(mobile);
        }
    }//GEN-LAST:event_txtAddMobileKeyReleased

    private void txtAddMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddMobileActionPerformed
        txtAddPhone.requestFocus();
    }//GEN-LAST:event_txtAddMobileActionPerformed

    private void comboAddStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboAddStateKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            txtAddEmail.requestFocus();
        }
    }//GEN-LAST:event_comboAddStateKeyPressed

    private void comboAddStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAddStateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboAddStateActionPerformed

    private void comboAddStateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboAddStateFocusGained
        comboAddState.showPopup();
    }//GEN-LAST:event_comboAddStateFocusGained

    private void txtAddDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddDistrictActionPerformed
        comboAddState.requestFocus();
    }//GEN-LAST:event_txtAddDistrictActionPerformed

    private void radioLedgerGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioLedgerGroupActionPerformed
        checkRadioLedger();
    }//GEN-LAST:event_radioLedgerGroupActionPerformed

    private void radioLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioLedgerActionPerformed
        checkRadioLedger();
    }//GEN-LAST:event_radioLedgerActionPerformed

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            addNewLedgerOrLedgerGroup();
            evt.consume();
            txtAddGroupName.requestFocus();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        addNewLedgerOrLedgerGroup();
    }//GEN-LAST:event_btnAddActionPerformed

    private void txtAddCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddCityActionPerformed
        txtAddDistrict.requestFocus();
    }//GEN-LAST:event_txtAddCityActionPerformed

    private void txtAddAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddAddr3ActionPerformed
        txtAddCity.requestFocus();
    }//GEN-LAST:event_txtAddAddr3ActionPerformed

    private void txtAddAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddAddr2ActionPerformed
        txtAddAddr3.requestFocus();
    }//GEN-LAST:event_txtAddAddr2ActionPerformed

    private void txtAddAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddAddr1ActionPerformed
        txtAddAddr2.requestFocus();
    }//GEN-LAST:event_txtAddAddr1ActionPerformed

    private void txtAddPhoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddPhoneKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE) || (vChar == KeyEvent.VK_MINUS))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtAddPhoneKeyTyped

    private void txtAddPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddPhoneActionPerformed
        comboAddTransactionType.requestFocus();
    }//GEN-LAST:event_txtAddPhoneActionPerformed

    private void comboAddTransactionTypeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboAddTransactionTypeKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            txtAddOpeningBalance.requestFocus();
        }
    }//GEN-LAST:event_comboAddTransactionTypeKeyPressed

    private void comboAddTransactionTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAddTransactionTypeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboAddTransactionTypeActionPerformed

    private void comboAddTransactionTypeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboAddTransactionTypeFocusGained
        comboAddTransactionType.showPopup();
    }//GEN-LAST:event_comboAddTransactionTypeFocusGained

    private void btnClearOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearOpeningBalanceActionPerformed
        clear();
    }//GEN-LAST:event_btnClearOpeningBalanceActionPerformed

    private void btnUpdateOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateOpeningBalanceActionPerformed
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateOpeningBalance.getDate());
        }
        if (isNotExpired) {
            Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate(), false);
            if (!isInBetweenFinancialYear) {
                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                        + " to <b>" + strFinancialYearEnd + "</b></html>";
                JLabel label = new JLabel(msg);
                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
                if (!treeValue.isSelectionEmpty()) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
                    Object o = node.getUserObject();
                    if (o instanceof TreeData) {
                        TreeData getData = (TreeData) o;
                        if (getData.isLedger) {
                            Ledger updatLedger = getData.getLedger();
                            String addr1 = txtAddr1.getText(), addr2 = txtAddr2.getText(), addr3 = txtAddr3.getText();

                            if (addr1.equals("")) {
                                addr1 = " ";
                            }
                            if (addr2.equals("")) {
                                addr2 = " ";
                            }
                            if (addr3.equals("")) {
                                addr3 = " ";
                            }
                            float openingAmount = 0;
                            String stropeningAmount = txtOpeningBalance.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                            if (!stropeningAmount.equals("")) {
                                openingAmount = Float.parseFloat(currency.format(Float.parseFloat(stropeningAmount)));
                                if (comboTransactionType.getSelectedItem().equals("Debit")) {
                                    openingAmount = (-1) * openingAmount;
                                }
                            }
                            String Address = addr1 + "/~/" + addr2 + "/~/" + addr3;
                            updatLedger.setOpeningBalanceDate(dateOpeningBalance.getDate());
                            updatLedger.setOpeningBalance(convertDecimal.Currency(currency.format(openingAmount)));

                            EventStatus result = new LedgerLogic().updateOpeningBalance(updatLedger);
                            if (result.isUpdateDone()) { //Check this
                                JOptionPane.showMessageDialog(null, " Opening Balance was Updated Successfully", "Message", JOptionPane.INFORMATION_MESSAGE);
                                TreeData updateData = new TreeData(updatLedger.getLedgerId(), updatLedger.getLedgerName(), true, false, updatLedger, new LedgerGroup());
                                node.setUserObject(updateData);
                            } else {
                                JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Please select the Ledger", "Alert", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select the Ledger or Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
        } else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }//GEN-LAST:event_btnUpdateOpeningBalanceActionPerformed

    private void comboTransactionTypeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboTransactionTypeKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            txtOpeningBalance.requestFocus();
        }
    }//GEN-LAST:event_comboTransactionTypeKeyPressed

    private void comboTransactionTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTransactionTypeActionPerformed

    }//GEN-LAST:event_comboTransactionTypeActionPerformed

    private void comboTransactionTypeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboTransactionTypeFocusGained
        comboTransactionType.showPopup();
    }//GEN-LAST:event_comboTransactionTypeFocusGained

    private void txtOpeningBalanceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOpeningBalanceKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtOpeningBalance.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACKSPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtOpeningBalanceKeyTyped

    private void txtOpeningBalanceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOpeningBalanceKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOpeningBalanceKeyPressed

    private void txtOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOpeningBalanceActionPerformed
        btnUpdate.requestFocus();
    }//GEN-LAST:event_txtOpeningBalanceActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        txtMobile.requestFocus();
    }//GEN-LAST:event_txtEmailActionPerformed

    private void txtMobileKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMobileKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMobileKeyTyped

    private void txtMobileKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMobileKeyReleased
        String mobile = txtMobile.getText();
        if (mobile.length() <= 10) {

        } else {
            JOptionPane.showMessageDialog(null, "Enter 10 Digit Number only", "Alert", JOptionPane.WARNING_MESSAGE);
            mobile = mobile.substring(0, 10);
            txtMobile.setText(mobile);
        }
    }//GEN-LAST:event_txtMobileKeyReleased

    private void txtMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileActionPerformed
        txtPhone.requestFocus();
    }//GEN-LAST:event_txtMobileActionPerformed

    private void comboStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboStateKeyPressed
        char vChar = evt.getKeyChar();
        if (vChar == KeyEvent.VK_ENTER) {
            txtEmail.requestFocus();
        }
    }//GEN-LAST:event_comboStateKeyPressed

    private void comboStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboStateActionPerformed

    }//GEN-LAST:event_comboStateActionPerformed

    private void comboStateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboStateFocusGained
        comboState.showPopup();
    }//GEN-LAST:event_comboStateFocusGained

    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        comboState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void txtAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr3ActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddr3ActionPerformed

    private void txtAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr2ActionPerformed
        txtAddr3.requestFocus();
    }//GEN-LAST:event_txtAddr2ActionPerformed

    private void txtAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr1ActionPerformed
        txtAddr2.requestFocus();
    }//GEN-LAST:event_txtAddr1ActionPerformed

    private void txtPhoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACKSPACE)
                || (vChar == KeyEvent.VK_DELETE) || (vChar == KeyEvent.VK_MINUS))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPhoneKeyTyped

    private void txtPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhoneActionPerformed
        comboTransactionType.requestFocus();
    }//GEN-LAST:event_txtPhoneActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        DefaultTreeModel model = (DefaultTreeModel) treeValue.getModel();
        String txtledgername = txtGroupName.getText();
        if (!treeValue.isSelectionEmpty()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeValue.getSelectionPath().getLastPathComponent();
            Object o = node.getUserObject();
            if (o instanceof TreeData) {
                TreeData getData = (TreeData) o;
                if (getData.isLedgerGroup) {
                    Boolean isSuccess = new LedgerGroupLogic().deleteLedgerGroup(getData.getId());
                    if (isSuccess) { //Check this
                        JOptionPane.showMessageDialog(null, "Ledger Group " + txtledgername + " is successfully deleted", "Message", JOptionPane.WARNING_MESSAGE);
                        TreePath[] paths = treeValue.getSelectionPaths();
                        for (int i = 0; i < paths.length; i++) {
                            node = (DefaultMutableTreeNode) (paths[i].getLastPathComponent());
                            model.removeNodeFromParent(node);
                        }
                        clear();
                    } else {
                        JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
                    }

                } else if (getData.isLedger) {

                    Ledger updatLedger = getData.getLedger();

                    EventStatus result = new LedgerLogic().deleteLedger(getData.getId());
                    if (result.isDeleteDone()) { //Check this
                        JOptionPane.showMessageDialog(null, "Ledger " + txtledgername + " is Sucessfully  Deleted ", "Message", JOptionPane.INFORMATION_MESSAGE);
                        TreePath[] paths = treeValue.getSelectionPaths();
                        for (int i = 0; i < paths.length; i++) {
                            node = (DefaultMutableTreeNode) (paths[i].getLastPathComponent());
                            model.removeNodeFromParent(node);
                        }
                        clear();
                    } else {
                        JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                    }

                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select the Ledger or Ledger Group", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateLedgerOrLedgerGroups();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateLedgerOrLedgerGroups();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void txtGroupNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGroupNameActionPerformed
        txtAddr1.requestFocus();
    }//GEN-LAST:event_txtGroupNameActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void checkRadioLedger() {
        Boolean isLedger = radioLedger.isSelected();
        if (isLedger) {
            labelAddLedgerORGroup.setText("Ledger");
        } else {
            labelAddLedgerORGroup.setText("Ledger Group");
        }
        labelAddAddress.setVisible(isLedger);
        txtAddAddr1.setVisible(isLedger);
        txtAddAddr2.setVisible(isLedger);
        txtAddAddr3.setVisible(isLedger);
        labelAddCity.setVisible(isLedger);
        txtAddCity.setVisible(isLedger);
        labelAddDistrict.setVisible(isLedger);
        txtAddDistrict.setVisible(isLedger);
        labelAddState.setVisible(isLedger);
        comboAddState.setVisible(isLedger);
        labelAddEmail.setVisible(isLedger);
        txtAddEmail.setVisible(isLedger);
        labelAddMobile.setVisible(isLedger);
        txtAddMobile.setVisible(isLedger);
        labelAddPhone.setVisible(isLedger);
        txtAddPhone.setVisible(isLedger);
        labelAddType.setVisible(isLedger);
        comboAddTransactionType.setVisible(isLedger);
        labelAddOpeningBalance.setVisible(isLedger);
        txtAddOpeningBalance.setVisible(isLedger);
        labelAddDateAsOn.setVisible(isLedger);
        dateAddOpeningBalance.setVisible(isLedger);
    }

    public void checkLedger(Boolean isLedger, Boolean isEditable) {
        if (isLedger) {
            labelLedgerORGroup.setText("Ledger");
        } else {
            labelLedgerORGroup.setText("Ledger Group");
        }
        labelAddress.setVisible(isLedger);
        txtAddr1.setVisible(isLedger);
        txtAddr2.setVisible(isLedger);
        txtAddr3.setVisible(isLedger);
        labelCity.setVisible(isLedger);
        txtCity.setVisible(isLedger);
        labelDistrict.setVisible(isLedger);
        txtDistrict.setVisible(isLedger);
        labelState.setVisible(isLedger);
        comboState.setVisible(isLedger);
        labelEmail.setVisible(isLedger);
        txtEmail.setVisible(isLedger);
        labelMobile.setVisible(isLedger);
        txtMobile.setVisible(isLedger);
        labelPhone.setVisible(isLedger);
        txtPhone.setVisible(isLedger);
        labelType.setVisible(isLedger);
        comboTransactionType.setVisible(isLedger);
        labelOpeningBalance.setVisible(isLedger);
        txtOpeningBalance.setVisible(isLedger);
        labelDateAsOn.setVisible(isLedger);
        dateOpeningBalance.setVisible(isLedger);

        btnUpdateOpeningBalance.setVisible(true);
        btnClearOpeningBalance.setVisible(true);

        if (isLedger) {
            labelType.setVisible(isEditable);
            comboTransactionType.setVisible(isEditable);
            labelOpeningBalance.setVisible(isEditable);
            txtOpeningBalance.setVisible(isEditable);
            labelDateAsOn.setVisible(isEditable);
            dateOpeningBalance.setVisible(isEditable);
            btnUpdateOpeningBalance.setVisible(isEditable);
            btnClearOpeningBalance.setVisible(isEditable);
        } else {
            btnUpdateOpeningBalance.setVisible(isLedger);
            btnClearOpeningBalance.setVisible(isLedger);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LedgerUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LedgerUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LedgerUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LedgerUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LedgerUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel addPanel;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAddClear;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClearOpeningBalance;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdateOpeningBalance;
    private javax.swing.JComboBox<String> comboAddState;
    private javax.swing.JComboBox<String> comboAddTransactionType;
    private javax.swing.JComboBox<String> comboState;
    private javax.swing.JComboBox<String> comboTransactionType;
    private com.toedter.calendar.JDateChooser dateAddOpeningBalance;
    private com.toedter.calendar.JDateChooser dateOpeningBalance;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JLabel labelAddAddress;
    private javax.swing.JLabel labelAddCity;
    private javax.swing.JLabel labelAddDateAsOn;
    private javax.swing.JLabel labelAddDistrict;
    private javax.swing.JLabel labelAddEmail;
    private javax.swing.JLabel labelAddLedgerORGroup;
    private javax.swing.JLabel labelAddMobile;
    private javax.swing.JLabel labelAddOpeningBalance;
    private javax.swing.JLabel labelAddPhone;
    private javax.swing.JLabel labelAddState;
    private javax.swing.JLabel labelAddType;
    private javax.swing.JLabel labelAddress;
    private javax.swing.JLabel labelCity;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelDateAsOn;
    private javax.swing.JLabel labelDistrict;
    private javax.swing.JLabel labelEmail;
    private javax.swing.JLabel labelLedgerORGroup;
    private javax.swing.JLabel labelMobile;
    private javax.swing.JLabel labelOpeningBalance;
    private javax.swing.JLabel labelPhone;
    private javax.swing.JLabel labelSelectedValue;
    private javax.swing.JLabel labelState;
    private javax.swing.JLabel labelType;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JPanel managePanel;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.ButtonGroup radioGroupLedger;
    private javax.swing.JRadioButton radioLedger;
    private javax.swing.JRadioButton radioLedgerGroup;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTree treeValue;
    private javax.swing.JTextField txtAddAddr1;
    private javax.swing.JTextField txtAddAddr2;
    private javax.swing.JTextField txtAddAddr3;
    private javax.swing.JTextField txtAddCity;
    private javax.swing.JTextField txtAddDistrict;
    private javax.swing.JTextField txtAddEmail;
    private javax.swing.JTextField txtAddGroupName;
    private javax.swing.JTextField txtAddMobile;
    private javax.swing.JTextField txtAddOpeningBalance;
    private javax.swing.JTextField txtAddPhone;
    private javax.swing.JTextField txtAddr1;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtAddr3;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtGroupName;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtOpeningBalance;
    private javax.swing.JTextField txtPhone;
    // End of variables declaration//GEN-END:variables

    class CountryTreeCellRenderer implements TreeCellRenderer {

        private JLabel label;

        CountryTreeCellRenderer() {
            label = new JLabel();
        }

        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                boolean leaf, int row, boolean hasFocus) {
            Object o = ((DefaultMutableTreeNode) value).getUserObject();
            URL ledgerUrl = getClass().getResource("/images/account.png");
            URL ledgerGroupUrl = getClass().getResource("/images/group.png");

            if (o instanceof TreeData) {
                TreeData data = (TreeData) o;
                if (data.isLedger) {
                    label.setIcon(new ImageIcon(ledgerUrl));
                } else if (data.isLedgerGroup) {
                    label.setIcon(new ImageIcon(ledgerGroupUrl));
                }
                label.setText(data.getName());
            } else {
                label.setIcon(null);
                label.setText("" + value);
            }

            return label;
        }
    }

    class TreeData {

        private Long id;
        private String name;
        private Boolean isLedger;
        private Boolean isLedgerGroup;
        private LedgerGroup ledgerGroup;
        private Ledger ledger;

        TreeData(Long id, String name, Boolean isledger, Boolean isLedgerGroup, Ledger ledger, LedgerGroup ledgerGroup) {
            this.id = id;
            this.name = name;
            this.isLedger = isledger;
            this.isLedgerGroup = isLedgerGroup;
            this.ledger = ledger;
            this.ledgerGroup = ledgerGroup;
        }
//        TreeData(Long id, String name, Boolean isledger, Boolean isLedgerGroup,String amount) {
//            this.id = id;
//            this.name = name;
//            this.isLedger = isledger;
//            this.isLedgerGroup = isLedgerGroup;
//            this.Amount = amount;
//        }

        public LedgerGroup getLedgerGroup() {
            return ledgerGroup;
        }

        public void setLedgerGroup(LedgerGroup ledgerGroup) {
            this.ledgerGroup = ledgerGroup;
        }

        public Ledger getLedger() {
            return ledger;
        }

        public void setLedger(Ledger ledger) {
            this.ledger = ledger;
        }

        public String getName() {
            return name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Boolean getIsLedger() {
            return isLedger;
        }

        public void setIsLedger(Boolean isLedger) {
            this.isLedger = isLedger;
        }

        public Boolean getIsLedgerGroup() {
            return isLedgerGroup;
        }

        public void setIsLedgerGroup(Boolean isLedgerGroup) {
            this.isLedgerGroup = isLedgerGroup;
        }

    }
}
