/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.PurchaseLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author Mani
 */
public class GSTR3BUI extends javax.swing.JFrame {

    /**
     * Creates new form GSTR2UI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();
    HashMap<String, Integer> stateCode = null;

    public GSTR3BUI() {
        try {
            initComponents();
            //        Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();

            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setExtendedState(GSTR3BUI.MAXIMIZED_BOTH);
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MMM-yy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
            labelCompanyName1.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
            labelUserName1.setText(SessionDataUtil.getSelectedUser().getUserName());

            Date date = new Date();
            dateFrom.setDate(date);
            dateTo.setDate(date);
            dateFrom.requestFocusInWindow();
            ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    dateTo.requestFocusInWindow();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    btnFetch.requestFocus();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    private void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    public void fetch() throws ParseException {
        Boolean isInBetweeFinancialYearStart = new CheckFinancialYear().toCheck(companyPolicy, dateFrom.getDate(), false);
        Boolean isInBetweeFinancialYearEnd = new CheckFinancialYear().toCheck(companyPolicy, dateTo.getDate(), false);

        if (isInBetweeFinancialYearStart && isInBetweeFinancialYearEnd) {
            List<PurchaseInvoice> PurchaseList = new PurchaseLogic().fetchAllPurchaseByDate(dateFrom.getDate(), dateTo.getDate());
            List<PurchaseSale> psItems = new PurchaseSaleLogic().getAllPurchaseByDate(dateFrom.getDate(), dateTo.getDate());

            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            if (model.getRowCount() > 0) {
                model.setRowCount(0);
            }
            long totalTaxableValue = 0;
            long igst = 0;
            long cgst = 0;
            long sgst = 0;
            long cess = 0;
            long totalTaxableValueZero = 0;
            long igstZero = 0;
            long cessZero = 0;
            long otherTotalTaxableValue = 0;
            String strNature1 = "(a) Outward Taxable  supplies  (other than zero rated, nil rated and exempted)";
            String strNature2 = "(b) Outward Taxable  supplies  (zero rated )";
            String strNature3 = "(c) Other Outward Taxable  supplies (Nil rated, exempted)";
            for (PurchaseSale purchase : psItems) {
                List<PurchaseSaleLineItem> psLiItems = purchase.getPsLineItems();
                if (purchase.isReverseChargeApplicable() && !purchase.isCancelled()) {
                    for (PurchaseSaleLineItem psItem : psLiItems) {
                        if (psItem.getIgstPercentage() > 0) {
                            if (psItem.getIgstValue() != null) {
                                igst += psItem.getIgstValue().longValue();
                            } else if (psItem.getCgstValue() != null) {
                                cgst += psItem.getCgstValue().longValue();
                                sgst += psItem.getSgstValue().longValue();
                            }
                            if (psItem.getCessAmount() != null) {
                                cess += psItem.getCessAmount().longValue();
                            }
                        } else if (psItem.getIgstPercentage() == 0) {
                            if (psItem.getValue() != null) {
                                totalTaxableValueZero += psItem.getValue().longValue();
                            }
                            if (psItem.getIgstValue() != null) {
                                igstZero += psItem.getIgstValue().longValue();
                            }
                            if (psItem.getCessAmount() != null) {
                                cessZero += psItem.getCessAmount().longValue();
                            }
                            if (psItem.getValue() != null) {
                                totalTaxableValue += psItem.getValue().longValue();
                            }
                        } else if (psItem.getValue() != null) {
                            otherTotalTaxableValue += psItem.getValue().longValue();
                        }
                    }
                }
            }
            Object[] row = {strNature1, totalTaxableValue, igst, cgst, sgst, cess};
            model.addRow(row);
            Object[] row1 = {strNature2, totalTaxableValueZero, igstZero, "", "", cessZero};
            model.addRow(row1);
            Object[] row3 = {strNature3, otherTotalTaxableValue, "", "", "", ""};
            model.addRow(row3);

            long totalTaxableValueIn = 0;
            long igstIn = 0;
            long cgstIn = 0;
            long sgstIn = 0;
            long cessIn = 0;
            String strNature4 = "(d) Inward supplies (liable to reverse charge) ";
            String strNature5 = "(e) Non-GST Outward supplies";

            for (PurchaseInvoice ps : PurchaseList) {
                if ((!ps.isCancelled()) && (ps.isReverseChargeApplicable())) {
                    List<PurchaseInvoiceLineItem> psLiItems = ps.getPiLineItems();
                    for (PurchaseInvoiceLineItem psliItem : psLiItems) {
                        if (psliItem.getValue() != null) {
                            totalTaxableValueIn += psliItem.getValue().longValue();
                        }
                        if (psliItem.getIgstValue() != null) {
                            igstIn += psliItem.getIgstValue().longValue();
                        }
                        if (psliItem.getCgstValue() != null) {
                            cgstIn += psliItem.getCgstValue().longValue();
                            sgstIn += psliItem.getSgstValue().longValue();
                        }
                        if (psliItem.getCessAmount() != null) {
                            cessIn += psliItem.getCessAmount().longValue();
                        }

                    }
                }
            }
            Object[] rowIn1 = {strNature4, totalTaxableValueIn, igstIn, cgstIn, sgstIn, cessIn};
            model.addRow(rowIn1);

            Object[] rowIn2 = {strNature5, "", "", "", "", ""};
            model.addRow(rowIn2);

            long totalValue = totalTaxableValue + totalTaxableValueZero + otherTotalTaxableValue + totalTaxableValueIn;
            long igstTotal = igstIn + igst + igstZero;
            long cgstTotal = cgstIn + cgst;
            long sgstTotal = sgstIn + sgst;
            long cessTotal = cess + cessZero + cessIn;

            Object[] rowTotal = {"Total", totalValue, igstTotal, cgstTotal, sgstTotal, cessTotal};
            model.addRow(rowTotal);
        } else {
            dateFrom.setDate(companyPolicy.getFinancialYearEnd());
            dateTo.setDate(companyPolicy.getFinancialYearEnd());
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel13 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        labelUserName1 = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        labelCompanyName1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        dateTo = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        btnFetch = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GSTR2");

        jLabel25.setText("User :");

        labelUserName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName1.setText("-");

        btnLogout1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout1.setMnemonic('l');
        btnLogout1.setText("Logout");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });

        jLabel18.setText("Company : ");

        labelCompanyName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName1.setText("----------");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel9.setText("GST3B REPORT");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton2.setMnemonic('h');
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel1.setText("Details of Outward Supplies and inward Supplies liable to revese charge");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nature of Supplies", "Total Taxable Value", "Integrated Tax", "Central Tax", "State/UT Tax", "Cess"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Long.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(500);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(500);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(500);
        }

        jButton1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jButton1.setMnemonic('e');
        jButton1.setText("Download Excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("From");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        dateTo.setDateFormatString("dd-MM-yyyy");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("To");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Details", "Integrated Tax", "Central Tax", "State Tax", "Cess"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable2);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 653, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 257, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 257, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1030, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName1)
                        .addComponent(jLabel25)
                        .addComponent(jButton2))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(labelCompanyName1)
                        .addComponent(jLabel9))
                    .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnFetch, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(231, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel13);

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1363, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            excel();
        } catch (WriteException | IOException | SQLException | ClassNotFoundException | IllegalAccessException ex) {
            Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        Boolean isInBetweeFinancialYearStart = new CheckFinancialYear().toCheck(companyPolicy, dateFrom.getDate(), false);
        Boolean isInBetweeFinancialYearEnd = new CheckFinancialYear().toCheck(companyPolicy, dateTo.getDate(), false);
        if (isInBetweeFinancialYearStart && isInBetweeFinancialYearEnd) {
            try {
                fetch();
            } catch (ParseException ex) {
                Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            dateFrom.setDate(companyPolicy.getFinancialYearEnd());
            dateTo.setDate(companyPolicy.getFinancialYearEnd());
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnFetchActionPerformed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed


    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void excel() throws WriteException, IOException, SQLException, ClassNotFoundException, IllegalAccessException {
//        DefaultTableModel model = (DefaultTableModel) tableb2cs.getModel();
        int count, countFromTot;
        String path = "";
        Boolean isApproved = false;

        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Windows".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                } catch (InstantiationException ex) {
                    Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }

//            if (isPrint) {
//                path = AppConstants.getTempDocPath() + "Tax Invoice Date Wise Report " + sdf.format(from) + " to " + sdf.format(to) + ".xls";
//                isApproved = true;
//            } else {
        JFrame parentFrame = new JFrame();
        com.alee.laf.WebLookAndFeel.install();
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Windows".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                } catch (InstantiationException ex) {
                    Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(GSTR3BUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Download Report");
        fileChooser.setSelectedFile(new File("GSTR2 Report "));
        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            path = fileToSave.getAbsolutePath() + ".xls";
            isApproved = true;
        }
//            }

        if (isApproved) {
            File f = new File(path);

            WritableWorkbook myexcel = Workbook.createWorkbook(f);

            WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            wf2.setColour(Colour.WHITE);

            WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);

            NumberFormat decimalNo = new NumberFormat("0.00");
            WritableCellFormat rightFormat = new WritableCellFormat(decimalNo);
            rightFormat.setFont(wf3);
            rightFormat.setAlignment(Alignment.RIGHT);

            WritableCellFormat headerTotalFormat = new WritableCellFormat(wf2);
            headerTotalFormat.setBackground(Colour.BLUE_GREY);
            headerTotalFormat.setFont(wf2);

            WritableCellFormat headerTotalFormat1 = new WritableCellFormat(wf2);
            headerTotalFormat1.setBackground(Colour.BLUE_GREY);
            headerTotalFormat1.setWrap(true);
            headerTotalFormat1.setFont(wf2);

            WritableCellFormat headerFormat = new WritableCellFormat(wf2);
            headerFormat.setBackground(Colour.LIGHT_ORANGE);
            headerFormat.setFont(wf2);

            WritableCellFormat dateFormat = new jxl.write.WritableCellFormat(new jxl.write.DateFormat("dd/MMM/yy"));
            dateFormat.setAlignment(Alignment.CENTRE);

            WritableCellFormat centerFormat = new WritableCellFormat(wf3);
            centerFormat.setAlignment(Alignment.CENTRE);

            WritableCellFormat rightTotalFormat = new WritableCellFormat(NumberFormats.INTEGER);
            rightTotalFormat.setFont(wf3);
            rightTotalFormat.setAlignment(Alignment.RIGHT);

            WritableCellFormat textFormat = new WritableCellFormat(wf3);

            WritableSheet b2b = myexcel.createSheet("b2b", 0);
            b2b.setColumnView(0, 26);
            b2b.setColumnView(1, 16);
            b2b.setColumnView(2, 13);
            b2b.setColumnView(3, 21);
            b2b.setColumnView(4, 18);
            b2b.setColumnView(5, 17);
            b2b.setColumnView(6, 20);
            b2b.setColumnView(7, 11);
            b2b.setColumnView(8, 20);
            b2b.setColumnView(9, 26);
            b2b.setColumnView(10, 24);
            b2b.setColumnView(11, 25);
            b2b.setColumnView(12, 13);
            b2b.setColumnView(13, 19);
            b2b.setColumnView(14, 33);
            b2b.setColumnView(15, 31);
            b2b.setColumnView(16, 32);
            b2b.setColumnView(17, 24);

            b2b.setName("b2b");
            b2b.addCell(new Label(0, 0, "Summary Of Supplies From Registered Suppliers  B2B(3)", headerTotalFormat1));

            b2b.addCell(new Label(2, 1, "", headerTotalFormat));
            b2b.addCell(new Label(4, 1, "", headerTotalFormat));
            b2b.addCell(new Label(5, 1, "", headerTotalFormat));
            b2b.addCell(new Label(6, 1, "", headerTotalFormat));
            b2b.addCell(new Label(7, 1, "", headerTotalFormat));
            b2b.addCell(new Label(13, 1, "", headerTotalFormat));

            b2b.addCell(new Label(0, 1, "No.of Suppliers", headerTotalFormat));
            b2b.addCell(new Label(1, 1, "No. of Invoices", headerTotalFormat));
            b2b.addCell(new Label(3, 1, "Total Invoice Value", headerTotalFormat));
            b2b.addCell(new Label(8, 1, "Total Taxable Value", headerTotalFormat));
            b2b.addCell(new Label(9, 1, "Total Integrated Tax Paid", headerTotalFormat));
            b2b.addCell(new Label(10, 1, "Total Central Tax Paid", headerTotalFormat));
            b2b.addCell(new Label(11, 1, "Total TState/UT Tax Paid", headerTotalFormat));
            b2b.addCell(new Label(12, 1, "Total Cess", headerTotalFormat));
            b2b.addCell(new Label(14, 1, "Total Availed ITC Integrated Tax  ", headerTotalFormat));
            b2b.addCell(new Label(15, 1, "Total Availed ITC Central Tax", headerTotalFormat));
            b2b.addCell(new Label(16, 1, "Total Availed ITC State/UT Tax ", headerTotalFormat));
            b2b.addCell(new Label(17, 1, "Total Availed ITC Cess", headerTotalFormat));

            b2b.addCell(new Label(0, 3, "GSTIN of Supplier", headerFormat));
            b2b.addCell(new Label(1, 3, "Invoice Number", headerFormat));
            b2b.addCell(new Label(2, 3, "Invoice Date", headerFormat));
            b2b.addCell(new Label(3, 3, "Invoice Value", headerFormat));
            b2b.addCell(new Label(4, 3, "Place Of Supply", headerFormat));
            b2b.addCell(new Label(5, 3, "Reverse Charge", headerFormat));
            b2b.addCell(new Label(6, 3, "invoice Type", headerFormat));
            b2b.addCell(new Label(7, 3, "Rate", headerFormat));
            b2b.addCell(new Label(8, 3, "Taxable Value", headerFormat));
            b2b.addCell(new Label(9, 3, "Integrated Tax Paid", headerFormat));
            b2b.addCell(new Label(10, 3, "Central Tax Paid", headerFormat));
            b2b.addCell(new Label(11, 3, "State/UT Tax Paid", headerFormat));
            b2b.addCell(new Label(12, 3, "Cess Paid", headerFormat));
            b2b.addCell(new Label(13, 3, "Eligibility For ITC", headerFormat));
            b2b.addCell(new Label(14, 3, "Availed ITC Integrated Tax", headerFormat));
            b2b.addCell(new Label(15, 3, "Availed ITC Central Tax", headerFormat));
            b2b.addCell(new Label(16, 3, "Availed ITC State/UT Tax", headerFormat));
            b2b.addCell(new Label(17, 3, "Availed ITC Cess", headerFormat));

            WritableSheet cdnr = myexcel.createSheet("cdnr", 1);
            cdnr.setColumnView(0, 24);
            cdnr.setColumnView(1, 30);
            cdnr.setColumnView(2, 27);
            cdnr.setColumnView(3, 42);
            cdnr.setColumnView(4, 40);
            cdnr.setColumnView(5, 10);
            cdnr.setColumnView(6, 16);
            cdnr.setColumnView(7, 29);
            cdnr.setColumnView(8, 14);
            cdnr.setColumnView(9, 28);
            cdnr.setColumnView(10, 10);
            cdnr.setColumnView(11, 20);
            cdnr.setColumnView(12, 26);
            cdnr.setColumnView(13, 24);
            cdnr.setColumnView(14, 26);
            cdnr.setColumnView(15, 14);
            cdnr.setColumnView(16, 19);
            cdnr.setColumnView(17, 35);
            cdnr.setColumnView(18, 26);
            cdnr.setColumnView(19, 25);
            cdnr.setColumnView(20, 18);

            cdnr.setName("cdnr");
            cdnr.addCell(new Label(0, 0, "Summary For CDNR(6C)", headerTotalFormat1));

            cdnr.addCell(new Label(0, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(2, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(4, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(5, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(6, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(7, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(8, 1, "", headerTotalFormat));
            cdnr.addCell(new Label(10, 1, "", headerTotalFormat));

            cdnr.addCell(new Label(0, 1, "No. of Supplier", headerTotalFormat1));
            cdnr.addCell(new Label(1, 1, "No. of Notes/Vouchers", headerTotalFormat1));
            cdnr.addCell(new Label(3, 1, "No. of Invoices", headerTotalFormat1));
            cdnr.addCell(new Label(9, 1, "Total Note/Voucher Value", headerTotalFormat));
            cdnr.addCell(new Label(11, 1, "Total Taxable Value", headerTotalFormat));
            cdnr.addCell(new Label(12, 1, "Total Integrated Tax Paid", headerTotalFormat));
            cdnr.addCell(new Label(13, 1, "Total Central Tax Paid", headerTotalFormat));
            cdnr.addCell(new Label(14, 1, "Total TState/UT Tax Paid", headerTotalFormat));
            cdnr.addCell(new Label(15, 1, "Total Cess", headerTotalFormat));
            cdnr.addCell(new Label(16, 1, "Eligibility For ITC", headerTotalFormat));
            cdnr.addCell(new Label(17, 1, "Total Availed ITC Integrated Tax  ", headerTotalFormat));
            cdnr.addCell(new Label(18, 1, "Total ITC Central Tax", headerTotalFormat));
            cdnr.addCell(new Label(19, 1, "Total ITC State/UT Tax", headerTotalFormat));
            cdnr.addCell(new Label(20, 1, "Total ITC Cess", headerTotalFormat));

            cdnr.addCell(new Label(0, 3, "GSTIN of Supplier", headerFormat));
            cdnr.addCell(new Label(1, 3, "Note/Refund Voucher Number", headerFormat));
            cdnr.addCell(new Label(2, 3, "Note/Refund Voucher date", headerFormat));
            cdnr.addCell(new Label(3, 3, "Invoice/Advance Payment Voucher Number", headerFormat));
            cdnr.addCell(new Label(4, 3, "Invoice/Advance Payment Voucher date", headerFormat));
            cdnr.addCell(new Label(5, 3, "Pre GST", headerFormat));
            cdnr.addCell(new Label(6, 3, "Document Type", headerFormat));
            cdnr.addCell(new Label(7, 3, "Reason For Issuing document", headerFormat));
            cdnr.addCell(new Label(8, 3, "Supply Type", headerFormat));
            cdnr.addCell(new Label(9, 3, "Note/Refund Voucher Value", headerFormat));
            cdnr.addCell(new Label(10, 3, "Rate", headerFormat));
            cdnr.addCell(new Label(11, 3, "Taxable Value", headerFormat));
            cdnr.addCell(new Label(12, 3, "Integrated Tax Paid", headerFormat));
            cdnr.addCell(new Label(13, 3, "Central Tax Paid", headerFormat));
            cdnr.addCell(new Label(14, 3, "State/UT Tax Paid", headerFormat));
            cdnr.addCell(new Label(15, 3, "Cess Paid", headerFormat));
            cdnr.addCell(new Label(16, 3, "Eligibility For ITC", headerFormat));
            cdnr.addCell(new Label(17, 3, "Availed ITC Integrated Tax", headerFormat));
            cdnr.addCell(new Label(18, 3, "Availed ITC Central Tax", headerFormat));
            cdnr.addCell(new Label(19, 3, "Availed ITC State/UT Tax", headerFormat));
            cdnr.addCell(new Label(20, 3, "Availed ITC Cess", headerFormat));

            WritableSheet hsn = myexcel.createSheet("hsnsum", 2);
            hsn.setColumnView(0, 23);
            hsn.setColumnView(1, 30);
            hsn.setColumnView(2, 16);
            hsn.setColumnView(3, 23);
            hsn.setColumnView(4, 25);
            hsn.setColumnView(5, 25);
            hsn.setColumnView(6, 25);
            hsn.setColumnView(7, 25);
            hsn.setColumnView(8, 25);
            hsn.setColumnView(9, 25);

            hsn.setName("hsn");
            hsn.addCell(new Label(0, 0, "Summary For HSN(13)", headerTotalFormat1));

            hsn.addCell(new Label(0, 1, "", headerTotalFormat));
            hsn.addCell(new Label(1, 1, "", headerTotalFormat));
            hsn.addCell(new Label(2, 1, "", headerTotalFormat));
            hsn.addCell(new Label(3, 1, "", headerTotalFormat));

            hsn.addCell(new Label(0, 1, "No. of HSN", headerTotalFormat));
            hsn.addCell(new Label(4, 1, "Total Value", headerTotalFormat));
            hsn.addCell(new Label(5, 1, "Total Taxable Value", headerTotalFormat));
            hsn.addCell(new Label(6, 1, "Total Integrated Tax", headerTotalFormat));
            hsn.addCell(new Label(7, 1, "Total Central Tax", headerTotalFormat));
            hsn.addCell(new Label(8, 1, "Total State/UT Tax", headerTotalFormat));
            hsn.addCell(new Label(9, 1, "Total Cess", headerTotalFormat));
            hsn.addCell(new Label(0, 3, "HSN", headerFormat));
            hsn.addCell(new Label(1, 3, "Description", headerFormat));
            hsn.addCell(new Label(2, 3, "UQC", headerFormat));
            hsn.addCell(new Label(3, 3, "Total Quantity", headerFormat));
            hsn.addCell(new Label(4, 3, "Total Value", headerFormat));
            hsn.addCell(new Label(5, 3, "Taxable Value", headerFormat));
            hsn.addCell(new Label(6, 3, "Integrated Tax Amount", headerFormat));
            hsn.addCell(new Label(7, 3, "Central Tax Amount", headerFormat));
            hsn.addCell(new Label(8, 3, "State/UT Tax Amount", headerFormat));
            hsn.addCell(new Label(9, 3, "Cess Amount", headerFormat));

            myexcel.write();
            myexcel.close();
            JOptionPane.showMessageDialog(null, "Excel File Downloaded At " + path);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GSTR3BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GSTR3BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GSTR3BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GSTR3BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GSTR3BUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout1;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JLabel labelCompanyName1;
    private javax.swing.JLabel labelUserName1;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    // End of variables declaration//GEN-END:variables
}
