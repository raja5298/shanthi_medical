/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.CreditDebitNoteLogic;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author Mani
 */
public class DebitNoteUI extends javax.swing.JFrame {

    /**
     * Creates new form CreditNoteUI
     */
    boolean isIGST = false, bUQCSame = false;
    //boolean isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    //float ProductRate = 0, ProductRateWithoutTax = 0;
    float IGSTper = 0, CGSTper = 0, SGSTper = 0;
    int nUQC2Value = 0;
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    DecimalFormat currency = new DecimalFormat("0.000");
    List<Product> products = new ProductLogic().fetchAllProducts(company.getCompanyId());
    List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(company.getCompanyId());
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPurchaseLedgers();
    List<Ledger> sortedLedgers = new ArrayList<Ledger>(ledgers);
    List<Ledger> companyLedgers = new LedgerLogic().fetchAllCompanyLedgers(company.getCompanyId());
    SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
    CreditDebitNote oldNote = null;
    Boolean isOldNote = false, isGodownOne = false;
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();
    Product selectProduct = null;

    public DebitNoteUI() {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            setRoles();
            saveData();

            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());

//        jMenuItem3.setEnabled(false);
//        jMenuItem9.setEnabled(false);
//        jMenuItem11.setEnabled(false);
//        jMenuItem12.setEnabled(false);
            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            setExtendedState(this.MAXIMIZED_BOTH);
            comboPOS.setSelectedItem("Tamil Nadu");
            comboState.setSelectedItem("Tamil Nadu");
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });
            Collections.sort(products, new Comparator<Product>() {
                public int compare(Product o1, Product o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            setGodown();
            setProduct();
            setParty();
            setExtendedState(this.MAXIMIZED_BOTH);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
            dateNote.setDate(new Date());
            dateInv.setDate(null);
            checkIGST();

            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
            editor1.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboParty.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            String noParty = "No Party";
                            if (noParty.toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(noParty);
                            }
                            for (Ledger ledger : sortedLedgers) {
                                if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(ledger.getLedgerName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboParty.setModel(model);
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        txtInvNum.requestFocus();
                    } else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        scripts.add("No Party");
                        for (Ledger ledger : sortedLedgers) {
                            scripts.add(ledger.getLedgerName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);
                        comboParty.getEditor().setItem("");
                        comboParty.setPopupVisible(true);
                    } else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboParty.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboParty.setPopupVisible(false);

                        comboParty.getEditor().setItem(selectedObj);
                        getPartyDetails();
                        dateNote.requestFocusInWindow();
                    }
                }
            });

            ((JTextField) dateNote.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateNote.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateNote.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateNote.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        txtInvNum.requestFocus();
                    } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            String strDate = ((JTextField) dateNote.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                sft.setLenient(false);
                                Date date = sft.parse(strDate);

                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    String strName = comboParty.getSelectedItem().toString();
                                    if (strName.equals("")) {
                                        comboParty.requestFocus();
                                    } else if (strName.equalsIgnoreCase("no party")) {
                                        txtPartyName.requestFocus();
                                    } else {
                                        txtInvNum.requestFocus();
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                    dateNote.setDate(null);
                                    dateNote.requestFocusInWindow();
                                }
                                evt.consume();
                            } else {

                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }

                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(CreditNoteUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            ((JTextField) dateInv.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateInv.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateInv.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateInv.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        setRouteToButton();

                    } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            String strDate = ((JTextField) dateInv.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                sft.setLenient(false);
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    comboPOS.requestFocus();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");

                            }

                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(DebitNoteUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
            editor.addKeyListener(new KeyAdapter() {
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboProduct.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            for (Product product : products) {
                                if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(product.getName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboProduct.setModel(model);
                            comboProduct.getEditor().setItem(val);
                            comboProduct.setPopupVisible(true);
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        setRouteToButton();
                    } else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        for (Product product : products) {
                            scripts.add(product.getName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem("");
                        comboProduct.setPopupVisible(true);
                    } else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboProduct.setPopupVisible(false);

                        comboProduct.getEditor().setItem(selectedObj);
                        getProductDetails();
                    }

                }
            });

            JTextComponent editor3 = (JTextComponent) comboGodown.getEditor().getEditorComponent();
            editor3.addKeyListener(new KeyAdapter() {
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                        String val = (String) comboGodown.getEditor().getItem();
                        ArrayList<String> scripts = new ArrayList<>();
                        for (GoDown godown : godowns) {
                            if (godown.getGoDownName().toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(godown.getGoDownName());
                            }
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboGodown.setModel(model);
                        comboGodown.getEditor().setItem(val);
                        comboGodown.setPopupVisible(true);
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        setRouteToButton();
                    } else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        for (GoDown godown : godowns) {
                            scripts.add(godown.getGoDownName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboGodown.setModel(model);
                        comboGodown.getEditor().setItem("");
                        comboGodown.setPopupVisible(true);
                    } else {
                        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                            DefaultComboBoxModel model = (DefaultComboBoxModel) comboGodown.getModel();
                            String selectedObj = (String) model.getSelectedItem();
                            comboGodown.setPopupVisible(false);

                            comboGodown.getEditor().setItem(selectedObj);
                            setProduct();
                            comboProduct.requestFocus();
                        }
                    }
                }
            });

        } catch (Exception ex) {
            Logger.getLogger(DebitNoteUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    private void saveData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveNote(false);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnSave.getActionMap();
        actionMap.put("Action", actionListener);
        btnSave.setActionMap(actionMap);
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    private void setRouteToButton() {

        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        } else {
            btnUpdate.requestFocus();
        }
    }

    public void EditOldNote(String id) {
        oldNote = new CreditDebitNoteLogic().fetchCDNote(id);
        Ledger partyLedger = oldNote.getLedger();
        if (partyLedger == null) {
            comboParty.setSelectedItem("No Party");
        } else {
            comboParty.setSelectedItem(partyLedger.getLedgerName());
            txtPartyName.setText(partyLedger.getLedgerName());
            String address = partyLedger.getAddress();
            String[] words = address.split("/~/");
            String strAddr = "";
            for (int ii = 0; ii < words.length; ii++) {
                if (ii == 0) {
                    strAddr = words[ii];
                } else if (!words[ii].equals(" ")) {
                    strAddr = strAddr + ", " + words[ii];
                }
            }
            txtAddress.setText(strAddr);
            txtCity.setText(partyLedger.getCity());
            txtDistrict.setText(partyLedger.getDistrict());
            comboState.setSelectedItem(partyLedger.getState());
            txtGSTIN.setText(partyLedger.getGSTIN());
            txtInvNum.requestFocus();
        }
        dateNote.setDate(oldNote.getNoteDate());
        comboParty.setEnabled(false);
        txtPartyName.setEnabled(false);
        txtAddress.setEnabled(false);
        txtCity.setEnabled(false);
        txtDistrict.setEnabled(false);
        comboState.setEnabled(false);
        txtGSTIN.setEnabled(false);
        dateNote.setEnabled(false);

        List<CreditDebitNoteLineItem> cdnLis = oldNote.getCdLis();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        isOldNote = true;

        for (CreditDebitNoteLineItem cdn : cdnLis) {
            Object[] row = {
                cdn.getNoteLineNumber(),
                cdn.getInvoiceNumber(),
                sft.format(cdn.getInvoiceDate()),
                cdn.getLineNumber(),
                cdn.getGstPOS(),
                cdn.getGodownStockEntry().getGodown().getGoDownName(),
                cdn.getProduct().getName(),
                cdn.getGstReason(),
                cdn.getUqc1Qty() + " " + cdn.getProduct().getUQC1() + ", " + cdn.getUqc2Qty() + " " + cdn.getProduct().getUQC2(),
                cdn.getValueInclusiveOfTax(),
                cdn.getGstPercentage(),
                cdn.getUqc1Qty(),
                cdn.getUqc2Qty(),
                cdn.getValue().floatValue(),
                cdn.getCessAmount().floatValue(),
                cdn.isIGST(),
                cdn.getIgstPercentage(),
                cdn.getIgstValue().floatValue(),
                cdn.getCgstPercentage(),
                cdn.getCgstValue().floatValue(),
                cdn.getSgstPercentage(),
                cdn.getSgstValue().floatValue()};
            model.addRow(row);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lblInvoiceNum = new javax.swing.JLabel();
        lblInvoiceDate = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblReason = new javax.swing.JLabel();
        lblLineNum = new javax.swing.JLabel();
        txtLineNumber = new javax.swing.JTextField();
        comboReason = new javax.swing.JComboBox<>();
        comboPOS = new javax.swing.JComboBox<>();
        dateInv = new com.toedter.calendar.JDateChooser();
        txtInvNum = new javax.swing.JTextField();
        productPane = new javax.swing.JPanel();
        lblProduct = new javax.swing.JLabel();
        lblQty = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        lblAmount = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        txtAmountIncl = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        labelSGST = new javax.swing.JLabel();
        txtSGSTAmt = new javax.swing.JTextField();
        labelCGST = new javax.swing.JLabel();
        txtCGSTAmt = new javax.swing.JTextField();
        labelIGST = new javax.swing.JLabel();
        txtIGSTAmt = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        comboGodown = new javax.swing.JComboBox<>();
        comboProduct = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtCessAmount = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        labelIGSTper = new javax.swing.JLabel();
        labelCGSTper = new javax.swing.JLabel();
        labelSGSTper = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        dateNote = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtPartyName = new javax.swing.JTextField();
        txtAddress = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        comboState = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        txtGSTIN = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Debit Note");

        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });

        lblInvoiceNum.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblInvoiceNum.setText("Invoice Number :");

        lblInvoiceDate.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblInvoiceDate.setText("Invoice Date :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("POS :");

        lblReason.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblReason.setText("Reason : ");

        lblLineNum.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblLineNum.setText("Line Number : ");

        txtLineNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLineNumberActionPerformed(evt);
            }
        });
        txtLineNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtLineNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtLineNumberKeyReleased(evt);
            }
        });

        comboReason.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboReason.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sales Return", "Post Sale Discount", "Deficiency in services", "Correction in Invoice", "Change in POS", "Finalization of Provisional assessment", "Others" }));
        comboReason.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboReasonFocusGained(evt);
            }
        });
        comboReason.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboReasonActionPerformed(evt);
            }
        });
        comboReason.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboReasonKeyPressed(evt);
            }
        });

        comboPOS.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboPOS.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboPOS.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPOSFocusGained(evt);
            }
        });
        comboPOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPOSActionPerformed(evt);
            }
        });
        comboPOS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPOSKeyPressed(evt);
            }
        });

        dateInv.setDateFormatString("dd-MM-yyyy");
        dateInv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateInvKeyPressed(evt);
            }
        });

        txtInvNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtInvNumActionPerformed(evt);
            }
        });
        txtInvNum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtInvNumKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblInvoiceNum)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInvNum))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblInvoiceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblReason, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comboPOS, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboReason, 0, 0, Short.MAX_VALUE)
                            .addComponent(dateInv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblLineNum)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtLineNumber)))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel3, lblInvoiceDate, lblInvoiceNum, lblLineNum, lblReason});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblInvoiceNum)
                    .addComponent(txtInvNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(dateInv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboPOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboReason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(lblLineNum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLineNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblInvoiceDate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblReason, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {dateInv, jLabel3, lblInvoiceDate, lblInvoiceNum, lblLineNum, lblReason, txtInvNum});

        productPane.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                productPaneFocusGained(evt);
            }
        });

        lblProduct.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblProduct.setText("Product :");

        lblQty.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblQty.setText("Quantity : ");

        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setText("/UQC1");

        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
        });

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setText("/UQC2");

        lblAmount.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblAmount.setText("Amount :");

        txtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmountActionPerformed(evt);
            }
        });
        txtAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAmountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAmountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAmountKeyTyped(evt);
            }
        });

        txtAmountIncl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmountInclActionPerformed(evt);
            }
        });
        txtAmountIncl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAmountInclKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAmountInclKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("(Incl.Tax)");

        labelSGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelSGST.setText("SGST");

        txtSGSTAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSGSTAmtActionPerformed(evt);
            }
        });
        txtSGSTAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSGSTAmtKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSGSTAmtKeyTyped(evt);
            }
        });

        labelCGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelCGST.setText("CGST");

        txtCGSTAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCGSTAmtActionPerformed(evt);
            }
        });
        txtCGSTAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCGSTAmtKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCGSTAmtKeyTyped(evt);
            }
        });

        labelIGST.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelIGST.setText("IGST");

        txtIGSTAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIGSTAmtKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIGSTAmtKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel8.setText("Godown :");

        comboGodown.setEditable(true);
        comboGodown.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboGodown.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboGodownFocusGained(evt);
            }
        });

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel9.setText("Cess Amount");

        txtCessAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCessAmountKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCessAmountKeyTyped(evt);
            }
        });

        labelIGSTper.setText("()");

        labelCGSTper.setText("()");

        labelSGSTper.setText("()");

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout productPaneLayout = new javax.swing.GroupLayout(productPane);
        productPane.setLayout(productPaneLayout);
        productPaneLayout.setHorizontalGroup(
            productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productPaneLayout.createSequentialGroup()
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblProduct)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(productPaneLayout.createSequentialGroup()
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addComponent(labelCGST, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtCGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(txtCessAmount))
                    .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(productPaneLayout.createSequentialGroup()
                            .addComponent(lblQty)
                            .addGap(18, 18, 18)
                            .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(productPaneLayout.createSequentialGroup()
                            .addComponent(lblAmount)
                            .addGap(18, 18, 18)
                            .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelCGSTper)
                    .addComponent(labelUQC1)
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(labelIGST, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addComponent(txtAmountIncl, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(183, 183, 183))
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(productPaneLayout.createSequentialGroup()
                                .addComponent(txtIGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelIGSTper))
                            .addGroup(productPaneLayout.createSequentialGroup()
                                .addComponent(txtUQC2Qty, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addComponent(labelSGST)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelSGSTper)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productPaneLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(183, 183, 183))
        );

        productPaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtIGSTAmt, txtSGSTAmt, txtUQC2Qty});

        productPaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancel, btnSave});

        productPaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel8, jLabel9, lblAmount, lblProduct, lblQty});

        productPaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelIGST, labelSGST});

        productPaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtAmount, txtCGSTAmt, txtCessAmount, txtUQC1Qty});

        productPaneLayout.setVerticalGroup(
            productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQty, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUQC1Qty)
                    .addComponent(txtUQC2Qty)
                    .addComponent(labelUQC2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelUQC1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(productPaneLayout.createSequentialGroup()
                        .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtAmount)
                                .addComponent(lblAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtAmountIncl, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGap(12, 12, 12)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelIGST)
                    .addComponent(jLabel9)
                    .addComponent(txtCessAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelIGSTper))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCGST)
                    .addComponent(labelSGST)
                    .addComponent(txtSGSTAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCGSTper)
                    .addComponent(labelSGSTper))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(productPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        productPaneLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelCGST, labelSGST, labelUQC1, labelUQC2, lblAmount, lblProduct, lblQty, txtAmount, txtAmountIncl, txtSGSTAmt, txtUQC1Qty, txtUQC2Qty});

        productPaneLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelIGST, txtCGSTAmt, txtIGSTAmt});

        productPaneLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnCancel, btnSave});

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        btnDelete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDeleteKeyPressed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        btnClear.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnClearKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(productPane, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(productPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Invoice Number", "Invoice Date", "Line Number", "POS", "Godown", "Product", "Reason", "Quantity", "Amount", "GST%", "UQC1", "UQC2", "Amount", "CessAmount", "isIGST", "IGST %", "IGST", "CGST%", "CGST", "SGST%", "SGST"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Boolean.class, java.lang.Float.class, java.lang.Double.class, java.lang.Float.class, java.lang.Double.class, java.lang.Float.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(11).setMinWidth(0);
            jTable1.getColumnModel().getColumn(11).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(11).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(0);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(14).setMinWidth(0);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(14).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(15).setMinWidth(0);
            jTable1.getColumnModel().getColumn(15).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(15).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);
        }

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("Date ");

        dateNote.setDateFormatString("dd-MM-yyyy");

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtPartyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartyNameActionPerformed(evt);
            }
        });
        txtPartyName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartyNameKeyPressed(evt);
            }
        });

        txtAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddressActionPerformed(evt);
            }
        });
        txtAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddressKeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel20.setText("City");

        jLabel25.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel25.setText("District");

        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });
        txtDistrict.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDistrictKeyPressed(evt);
            }
        });

        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });
        txtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCityKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("GSTIN/UIN");

        comboState.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboStateActionPerformed(evt);
            }
        });
        comboState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboStateKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel11.setText("Party");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });

        txtGSTIN.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGSTINFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtGSTINFocusLost(evt);
            }
        });
        txtGSTIN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtGSTINMouseClicked(evt);
            }
        });
        txtGSTIN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtGSTINKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboParty, 0, 184, Short.MAX_VALUE))
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtAddress)
                    .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDistrict)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboState, 0, 190, Short.MAX_VALUE)
                    .addComponent(txtGSTIN))
                .addGap(24, 24, 24))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel20)
                                .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel25)
                                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel17)
                                .addComponent(comboState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel26)
                                    .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(dateNote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel13)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void comboReasonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboReasonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboReasonActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (isOldNote) {
            new CreditNoteDateWiseReportUI().setVisible(true);
            dispose();
        } else {
            clearAll();
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtPartyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartyNameActionPerformed
        txtAddress.requestFocus();
    }//GEN-LAST:event_txtPartyNameActionPerformed

    private void txtAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddressActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddressActionPerformed

    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        comboState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void comboStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboStateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtGSTIN.requestFocus();
        }

    }//GEN-LAST:event_comboStateKeyPressed

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained
        comboParty.setPopupVisible(false);
    }//GEN-LAST:event_comboPartyFocusGained

    private void txtGSTINKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGSTINKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        }

    }//GEN-LAST:event_txtGSTINKeyPressed

    private void txtSGSTAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSGSTAmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSGSTAmtActionPerformed

    private void txtInvNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtInvNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInvNumActionPerformed

    private void txtInvNumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInvNumKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {

            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            dateInv.requestFocusInWindow();
        }
    }//GEN-LAST:event_txtInvNumKeyPressed

    private void dateInvKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateInvKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboPOS.requestFocus();
        }
    }//GEN-LAST:event_dateInvKeyPressed

    private void comboPOSKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPOSKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            checkIGST();
            comboReason.requestFocus();
        }
    }//GEN-LAST:event_comboPOSKeyPressed

    private void comboReasonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboReasonKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtLineNumber.requestFocus();
        }
    }//GEN-LAST:event_comboReasonKeyPressed

    private void txtLineNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLineNumberKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isGodownOne) {
                comboProduct.requestFocus();
            } else {
                comboGodown.requestFocus();
            }

            evt.consume();
        }
    }//GEN-LAST:event_txtLineNumberKeyPressed

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtUQC2Qty.isEnabled()) {
                txtUQC2Qty.requestFocus();
            } else {
                txtAmount.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//            txtAmount.setText(currency.format(getTotal(false)));
//            txtAmountIncl.setText(currency.format(getTotal(true)));
            txtAmount.requestFocus();
        }
    }//GEN-LAST:event_txtUQC2QtyKeyPressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int nRow = jTable1.getSelectedRow();
        if (nRow > -1) {
            try {

                btnAdd.setEnabled(false);
                btnUpdate.setEnabled(true);
                btnDelete.setEnabled(true);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                txtInvNum.setText(model.getValueAt(nRow, 1).toString());
                Date invDate = sft.parse(model.getValueAt(nRow, 2).toString());
                dateInv.setDate(invDate);
                txtLineNumber.setText(model.getValueAt(nRow, 3).toString());
                comboPOS.setSelectedItem(model.getValueAt(nRow, 4));
                checkIGST();
                comboGodown.setSelectedItem(model.getValueAt(nRow, 5));
                comboProduct.setSelectedItem(model.getValueAt(nRow, 6));
                getProductDetails();
                comboReason.setSelectedItem(model.getValueAt(nRow, 7));
                txtAmountIncl.setText(model.getValueAt(nRow, 9).toString());
                txtUQC1Qty.setText(model.getValueAt(nRow, 11).toString());
                txtUQC2Qty.setText((String) model.getValueAt(nRow, 12).toString());
                txtAmount.setText(model.getValueAt(nRow, 13).toString());
                txtCessAmount.setText(currency.format(model.getValueAt(nRow, 14)));
                isIGST = (boolean) model.getValueAt(nRow, 15);
                labelIGSTper.setText("(" + String.valueOf(model.getValueAt(nRow, 16)) + "%)");
                txtIGSTAmt.setText(model.getValueAt(nRow, 17).toString());
                labelCGSTper.setText("(" + String.valueOf(model.getValueAt(nRow, 18)) + "%)");
                txtCGSTAmt.setText(model.getValueAt(nRow, 19).toString());
                labelSGSTper.setText("(" + String.valueOf(model.getValueAt(nRow, 20)) + "%)");
                txtSGSTAmt.setText(model.getValueAt(nRow, 21).toString());
                if (bUQCSame) {
                    txtUQC2Qty.setText("");
                }
                txtInvNum.requestFocusInWindow();

            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(DebitNoteUI.class
                        .getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            addProductDetails();
            evt.consume();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        saveNote(false);
    }//GEN-LAST:event_btnSaveActionPerformed
    public void saveNote(Boolean isPrint) {
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateNote.getDate());
        }
        if (isNotExpired) {
            if (isOldNote) {
                oldCreditNote(isPrint);
            } else {
                newCreditNote(isPrint);
            }
        } else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }
    private void btnClearKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnClearKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            clearData(true);
        }
    }//GEN-LAST:event_btnClearKeyPressed

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateProduct();
            evt.consume();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void btnDeleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDeleteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            deleteData();
            clearData(false);
        }
    }//GEN-LAST:event_btnDeleteKeyPressed

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel1MouseClicked

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clearData(false);
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        addProductDetails();
    }//GEN-LAST:event_btnAddActionPerformed

    private void comboGodownFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboGodownFocusGained
        comboGodown.showPopup();
    }//GEN-LAST:event_comboGodownFocusGained

    private void comboPOSFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPOSFocusGained
        comboPOS.showPopup();
    }//GEN-LAST:event_comboPOSFocusGained

    private void comboReasonFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboReasonFocusGained
        comboReason.showPopup();
    }//GEN-LAST:event_comboReasonFocusGained

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProduct();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteData();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void productPaneFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_productPaneFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_productPaneFocusGained

    private void txtLineNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLineNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLineNumberActionPerformed

    private void txtLineNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLineNumberKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            evt.consume();
        }
    }//GEN-LAST:event_txtLineNumberKeyReleased
    private void checkIGST() {
        String getState = (String) comboPOS.getSelectedItem();
        if (getState.equals(company.getState())) {
            isIGST = false;
            txtIGSTAmt.setText("");
        } else {
            isIGST = true;
            txtCGSTAmt.setText("");
            txtSGSTAmt.setText("");
        }
        labelIGST.setVisible(isIGST);
        txtIGSTAmt.setVisible(isIGST);
        labelIGSTper.setVisible(isIGST);

        labelCGST.setVisible(!isIGST);
        txtCGSTAmt.setVisible(!isIGST);
        labelCGSTper.setVisible(!isIGST);

        labelSGST.setVisible(!isIGST);
        txtSGSTAmt.setVisible(!isIGST);
        labelSGSTper.setVisible(!isIGST);
    }

    private void txtCessAmountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessAmountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isIGST) {
                txtIGSTAmt.requestFocus();
            } else {
                txtCGSTAmt.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCessAmountKeyPressed

    private void comboPOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPOSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboPOSActionPerformed

    private void txtPartyNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartyNameKeyPressed

        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        }
    }//GEN-LAST:event_txtPartyNameKeyPressed

    private void txtAddressKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddressKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        }
    }//GEN-LAST:event_txtAddressKeyPressed

    private void txtCityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        }
    }//GEN-LAST:event_txtCityKeyPressed

    private void txtDistrictKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDistrictKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInvNum.requestFocus();
        }
    }//GEN-LAST:event_txtDistrictKeyPressed

    private void txtCGSTAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCGSTAmtActionPerformed
        txtSGSTAmt.requestFocus();
    }//GEN-LAST:event_txtCGSTAmtActionPerformed

    private void txtAmountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        }
    }//GEN-LAST:event_txtAmountKeyPressed

    private void txtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmountActionPerformed
        txtAmountIncl.requestFocus();
    }//GEN-LAST:event_txtAmountActionPerformed

    private void txtAmountInclActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmountInclActionPerformed
        txtCessAmount.requestFocus();
    }//GEN-LAST:event_txtAmountInclActionPerformed

    private void txtAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtAmount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtAmountKeyTyped

    private void txtAmountInclKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountInclKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtAmountIncl.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtAmountInclKeyTyped

    private void txtCessAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessAmountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtCessAmount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtCessAmountKeyTyped

    private void txtIGSTAmtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIGSTAmtKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtIGSTAmt.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtIGSTAmtKeyTyped

    private void txtCGSTAmtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCGSTAmtKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtCGSTAmt.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtCGSTAmtKeyTyped

    private void txtSGSTAmtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSGSTAmtKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtSGSTAmt.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtSGSTAmtKeyTyped

    private void txtAmountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountKeyReleased
        txtAmountIncl.setText(txtAmount.getText());
    }//GEN-LAST:event_txtAmountKeyReleased

    private void txtIGSTAmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIGSTAmtKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtIGSTAmtKeyPressed

    private void txtSGSTAmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSGSTAmtKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtSGSTAmtKeyPressed

    private void txtAmountInclKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountInclKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        }
    }//GEN-LAST:event_txtAmountInclKeyPressed

    private void txtCGSTAmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCGSTAmtKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setRouteToButton();
        }
    }//GEN-LAST:event_txtCGSTAmtKeyPressed

    private void comboStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboStateActionPerformed
        StateCode obj = new StateCode();
        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_comboStateActionPerformed

    private void txtGSTINFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGSTINFocusLost
        String str = txtGSTIN.getText();
        if (str.length() == 2) {
            txtGSTIN.setText("");
        }
    }//GEN-LAST:event_txtGSTINFocusLost

    private void txtGSTINFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGSTINFocusGained
//        StateCode obj = new StateCode();
//        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_txtGSTINFocusGained

    private void txtGSTINMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtGSTINMouseClicked
//        StateCode obj = new StateCode();
//        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_txtGSTINMouseClicked

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void addProductDetails() {
        String strProduct = "", strPOS = "", strReason = "", strInvDate = "", strQty = "", strGodown = "",
                strQty1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", ""),
                strQty2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", ""),
                strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        int nInvNum = 0, qty1 = 0, qty2 = 0, lineNumber = 0;
        float fAmountTax = 0, fAmount = 0, fCessAmount = 0;
        float fIGST = 0, fCGST = 0, fSGST = 0;
        try {
            if (validateFields()) {
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                strProduct = comboProduct.getSelectedItem().toString();
                selectProduct = getProduct(strProduct);
                strGodown = comboGodown.getSelectedItem().toString();
                strPOS = comboPOS.getSelectedItem().toString();
                strReason = comboReason.getSelectedItem().toString();
                nInvNum = Integer.parseInt(txtInvNum.getText());
                strInvDate = sft.format(dateInv.getDate());
                lineNumber = Integer.parseInt(txtLineNumber.getText());
                String strAmount = txtAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                String strAmountTax = txtAmountIncl.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                String strIGST = txtIGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                String strCGST = txtCGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                String strSGST = txtSGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                if (!strAmount.equals("")) {
                    fAmount = Float.parseFloat(strAmount);
                }
                if (!strAmountTax.equals("")) {
                    fAmountTax = Float.parseFloat(strAmountTax);
                }
                if (!strCessAmount.equals("")) {
                    fCessAmount = Float.parseFloat(strCessAmount);
                }

                if (!strQty1.equals("")) {
                    qty1 = Integer.parseInt(strQty1);
                }
                if (!strQty2.equals("")) {
                    qty2 = Integer.parseInt(strQty2);
                }
                if (!strIGST.equals("")) {
                    fIGST = Float.parseFloat(strIGST);
                }
                if (!strCGST.equals("")) {
                    fCGST = Float.parseFloat(strCGST);
                }
                if (!strSGST.equals("")) {
                    fSGST = Float.parseFloat(strSGST);
                }

                if (bUQCSame) {
                    strQty = qty1 + labelUQC1.getText();
                } else {
                    strQty = qty1 + labelUQC1.getText() + ", " + qty2 + labelUQC2.getText();
                }

                checkIGST();

//                Object[] row1={model.getRowCount()+1, nInvNum, strInvDate, lineNumber, strPOS, strGodown, strProduct, strReason,strQty, fAmountTax,
//                    };
//                model.addRow(row1);
                Object[] row = {model.getRowCount() + 1, nInvNum, strInvDate, lineNumber, strPOS, strGodown, strProduct, strReason,
                    strQty, fAmountTax, selectProduct.getIgstPercentage(), qty1, qty2, fAmount, fCessAmount,
                    isIGST, selectProduct.getIgstPercentage(), fIGST, selectProduct.getCgstPercentage(), fCGST, selectProduct.getSgstPercentage(), fSGST};
                model.addRow(row);
                clearData(false);

            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }

    private Product getProduct(String getProduct) {
        Product selectProduct = null;
        for (Product p : products) {
            if (p.getName().equals(getProduct)) {
                selectProduct = p;
            }

        }
        return selectProduct;
    }

    private GoDown findGodownByName(String strGodown) {
        GoDown godown = null;
        for (GoDown g : godowns) {
            if (g.getGoDownName().equals(strGodown)) {
                godown = g;
            }
        }
        return godown;
    }
//    public float getTaxAmount() {
//        int nQty = 0;
//        int nQty2 = 0;
//        int nTotalQty = 0;
//        float fTotalAmount = 0.00f;
//        float fTaxPer = 0.00f;
//
//        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String strTaxPer = txtIGSTPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        if (!strUQC1qty.equals("")) {
//            nQty = Integer.parseInt(txtUQC1Qty.getText());
//        }
//        if (!strUQC2qty.equals("")) {
//            nQty2 = Integer.parseInt(txtUQC2Qty.getText());
//        }
//        if (!strTaxPer.equals("")) {
//            fTaxPer = Float.parseFloat(txtIGSTPer.getText());
//        }
//        nTotalQty = (nQty * nUQC2Value) + nQty2;
//        fTotalAmount = (nTotalQty * (fTaxPer) / 100);
//        return fTotalAmount;
//    }

//    public float getTotal(boolean isTaxinclude) {
//        int nQty = 0;
//        int nQty2 = 0;
//        int nTotalQty = 0;
//        float fTotal = 0.00f;
//        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//        if (!strUQC1qty.equals("")) {
//            nQty = Integer.parseInt(txtUQC1Qty.getText());
//        }
//        if (!strUQC2qty.equals("")) {
//            nQty2 = Integer.parseInt(txtUQC2Qty.getText());
//        }
//        nTotalQty = (nQty * nUQC2Value) + nQty2;
//        if (isTaxinclude) {
//            fTotal = nTotalQty * ProductRate;
//        } else {
//            fTotal = nTotalQty * ProductRateWithoutTax;
//        }
//        return fTotal;
//    }
    public void getPartyDetails() {
        String strAddr = "";
        String name = (String) comboParty.getSelectedItem();
        if (name == null) {
            name = "";
        }
        if (name.equals("")) {
            txtPartyName.setText("");
            txtAddress.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            comboState.setSelectedItem("");
            txtGSTIN.setText("");
        } else {
            for (Ledger ledger : sortedLedgers) {
                if (ledger.getLedgerName().equals(name)) {
                    txtPartyName.setText(ledger.getLedgerName());
                    String address = ledger.getAddress();
                    String[] words = address.split("/~/");
                    for (int ii = 0; ii < words.length; ii++) {
                        if (ii == 0) {
                            strAddr = words[ii];
                        } else if (!words[ii].equals(" ")) {
                            strAddr = strAddr + ", " + words[ii];
                        }
                    }
                    txtAddress.setText(strAddr);
                    txtCity.setText(ledger.getCity());
                    txtDistrict.setText(ledger.getDistrict());
                    comboState.setSelectedItem(ledger.getState());
                    txtGSTIN.setText(ledger.getGSTIN());
//                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
//                    if (model.getRowCount() > 0) {
//                        model.setRowCount(0);
//                    }
                    break;
                } else if (name.equalsIgnoreCase("no party")) {
                    txtPartyName.setText("");
                    txtAddress.setText("");
                    txtCity.setText("");
                    txtDistrict.setText("");
                    comboState.setSelectedItem("");
                    txtGSTIN.setText("");
                }
            }
        }
    }

    public void setParty() {
        comboParty.addItem("No Party");
        for (Ledger ledger : sortedLedgers) {
            if ((!(ledger.getGSTIN().equals(""))) && (ledger.getGSTIN() != null)) {
                comboParty.addItem(ledger.getLedgerName());
            }
        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    public void setGodown() {
        for (GoDown godown : godowns) {
            comboGodown.addItem(godown.getGoDownName());
        }
        if (godowns.size() <= 1) {
            isGodownOne = true;
            comboGodown.setSelectedItem(godowns.get(0).getGoDownName());
            jLabel8.setVisible(false);
            comboGodown.setVisible(false);
        } else {
            comboGodown.setSelectedItem("");
            jLabel8.setVisible(true);
            comboGodown.setVisible(true);
        }
    }

    public void getProductDetails() {
        String productName = comboProduct.getSelectedItem().toString();
        if ((productName != null) && (!(productName.equals("")))) {
            for (Product product : products) {
                if (product.getName().equals(productName)) {
                    selectProduct = product;
                    labelUQC1.setText(product.getUQC1());
                    labelUQC2.setText(product.getUQC2());
                    nUQC2Value = product.getUQC2Value();
                    IGSTper = product.getIgstPercentage();
                    CGSTper = product.getCgstPercentage();
                    SGSTper = product.getSgstPercentage();
                    labelIGSTper.setText("(" + String.valueOf(product.getIgstPercentage()) + "%)");
                    labelCGSTper.setText("(" + String.valueOf(product.getCgstPercentage()) + "%)");
                    labelSGSTper.setText("(" + String.valueOf(product.getSgstPercentage()) + "%)");
                    if (product.getUQC1().equalsIgnoreCase(product.getUQC2())) {
                        bUQCSame = true;
                        txtUQC2Qty.setEnabled(false);
                        labelUQC2.setText("");
                    } else {
                        bUQCSame = false;
                        txtUQC2Qty.setEnabled(true);
                    }
//                    ProductRate = product.getProductRate().floatValue();
//                    isProductTaxInclusive = product.isInclusiveOfGST();
//                    if (isProductTaxInclusive) {
//                        ProductRateWithoutTax = (ProductRate / (100 + IGSTper)) * 100;
//                    }
                    txtUQC1Qty.requestFocus();
                }
            }
        } else {
            txtAmount.setText("");
            txtIGSTAmt.setText("");
            txtSGSTAmt.setText("");
            txtCGSTAmt.setText("");
            comboProduct.setSelectedIndex(0);
            comboProduct.setPopupVisible(true);
            comboProduct.requestFocus();
            labelIGSTper.setText("()");
            labelCGSTper.setText("()");
            labelSGSTper.setText("()");

        }
    }

    public void clearData(boolean clearAll) {
        jTable1.getColumnModel().getColumn(9).setCellRenderer(new FloatValueTableCellRenderer());
        txtAmount.setText("");
        txtIGSTAmt.setText("");
        txtSGSTAmt.setText("");
        txtCGSTAmt.setText("");
        txtInvNum.setText("");
        txtLineNumber.setText("");
        txtUQC1Qty.setText("");
        txtUQC2Qty.setText("");
        txtAmountIncl.setText("");
        dateInv.setDate(new Date());
        comboProduct.setSelectedItem("");
        if (isGodownOne) {
            comboGodown.setSelectedItem(godowns.get(0).getGoDownName());
        } else {
            comboGodown.setSelectedItem("");
        }
        comboPOS.setSelectedItem("Tamil Nadu");
        comboReason.setSelectedIndex(0);
        jTable1.clearSelection();
        txtInvNum.requestFocus();
        txtCessAmount.setText("");
        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        labelUQC1.setText("/UQC1");
        labelUQC2.setText("/UQC2");
        txtUQC2Qty.setEnabled(true);
        checkIGST();
        if (clearAll) {
            dateNote.setDate(new Date());
            comboState.setSelectedIndex(0);
            comboParty.setSelectedIndex(0);
            txtPartyName.setText("");
            txtAddress.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            txtGSTIN.setText("");
        }

    }

    public void updateProduct() {
        int nRow = jTable1.getSelectedRow();
        try {
            if (nRow > -1) {
                String strProduct = "", strPOS = "", strReason = "", strInvDate = "", strQty = "", strGST = "", strGodown = "",
                        strQty1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", ""),
                        strQty2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", ""),
                        strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                int nInvNum = 0, qty1 = 0, qty2 = 0, lineNumber = 0;
                float fAmountTax = 0, fAmount = 0, fCessAmount = 0;
                float fIGST = 0, fCGST = 0, fSGST = 0;
                try {
                    if (validateFields()) {
                        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                        strProduct = comboProduct.getSelectedItem().toString();
                        selectProduct = getProduct(strProduct);
                        strGodown = comboGodown.getSelectedItem().toString();
                        strPOS = comboPOS.getSelectedItem().toString();
                        strReason = comboReason.getSelectedItem().toString();
                        nInvNum = Integer.parseInt(txtInvNum.getText());
                        strInvDate = sft.format(dateInv.getDate());
                        lineNumber = Integer.parseInt(txtLineNumber.getText());
                        String strAmount = txtAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        String strAmountTax = txtAmountIncl.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        String strIGST = txtIGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        String strCGST = txtCGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                        String strSGST = txtSGSTAmt.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

                        if (!strAmount.equals("")) {
                            fAmount = Float.parseFloat(strAmount);
                        }
                        if (!strAmountTax.equals("")) {
                            fAmountTax = Float.parseFloat(strAmountTax);
                        }
                        if (!strCessAmount.equals("")) {
                            fCessAmount = Float.parseFloat(strCessAmount);
                        }

                        if (!strQty1.equals("")) {
                            qty1 = Integer.parseInt(strQty1);
                        }
                        if (!strQty2.equals("")) {
                            qty2 = Integer.parseInt(strQty2);
                        }
                        if (!strIGST.equals("")) {
                            fIGST = Float.parseFloat(strIGST);
                        }
                        if (!strCGST.equals("")) {
                            fCGST = Float.parseFloat(strCGST);
                        }
                        if (!strSGST.equals("")) {
                            fSGST = Float.parseFloat(strSGST);
                        }

                        if (bUQCSame) {
                            strQty = qty1 + labelUQC1.getText();
                        } else {
                            strQty = qty1 + labelUQC1.getText() + ", " + qty2 + labelUQC2.getText();
                        }
                        checkIGST();
                        int SerialNo = (int) model.getValueAt(nRow, 0);

                        Object[] row = {SerialNo, nInvNum, strInvDate, lineNumber, strPOS, strGodown, strProduct, strReason,
                            strQty, fAmountTax, selectProduct.getIgstPercentage(), qty1, qty2, fAmount, fCessAmount,
                            isIGST, selectProduct.getIgstPercentage(), fIGST, selectProduct.getCgstPercentage(), fCGST, selectProduct.getSgstPercentage(), fSGST};
                        model.removeRow(nRow);
                        model.insertRow(nRow, row);
                        clearData(false);

                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(DebitNoteUI.class
                            .getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Select atleast one row", "Alert", JOptionPane.WARNING_MESSAGE);

            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        btnAdd.setEnabled(true);
    }

    public void deleteData() {
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    model.setValueAt(y + 1, y, 0);
                }
                clearData(false);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public boolean validateFields() {
        String strInvNum = "";
        String strLineNum = "";
        String strPOS = "";
        String strReason = "";
        String strProduct = "";
        String strUQC1 = "";
        String strUQC2 = "";
        String strLedger = "";
        String strParty = "";
        String strState = "", strGodown = "";

        Date creditDate = dateNote.getDate();
        Date invDate = dateInv.getDate();
        boolean result = true;
        strInvNum = txtInvNum.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strLineNum = txtLineNumber.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strUQC1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strUQC2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        strParty = comboParty.getSelectedItem().toString();
        strState = comboState.getSelectedItem().toString();
        strPOS = comboPOS.getSelectedItem().toString();
        strReason = comboReason.getSelectedItem().toString();
        strProduct = comboProduct.getSelectedItem().toString();
        strGodown = (String) comboGodown.getSelectedItem();
        if (strParty.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select Party.", "Alert", JOptionPane.WARNING_MESSAGE);
            comboParty.requestFocus();
            return false;
        } else if (!strParty.equalsIgnoreCase("no party")) {
            if (strState.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Select Prty State .", "Alert", JOptionPane.WARNING_MESSAGE);
                comboState.requestFocus();
                return false;
            } else if (creditDate == null) {
                JOptionPane.showMessageDialog(null, "Please Select the Date.", "Alert", JOptionPane.WARNING_MESSAGE);
                dateNote.requestFocusInWindow();
                return false;
            }
        }
        if (strInvNum.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter the Invoice Number");
            txtInvNum.requestFocus();
            return false;
        } else if (invDate == null) {
            JOptionPane.showMessageDialog(null, "Please Select Invoice Date.", "Alert", JOptionPane.WARNING_MESSAGE);
            dateInv.requestFocusInWindow();
            return false;
        } else if (strPOS.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select Place Of Service (POS).", "Alert", JOptionPane.WARNING_MESSAGE);
            comboPOS.requestFocus();
            return false;
        } else if (strReason.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Reason .", "Alert", JOptionPane.WARNING_MESSAGE);
            comboReason.requestFocus();
            return false;
        } else if (strLineNum.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter Line Number.", "Alert", JOptionPane.WARNING_MESSAGE);
            txtLineNumber.requestFocus();
            return false;
        } else if (strGodown.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Godown .", "Alert", JOptionPane.WARNING_MESSAGE);
            comboGodown.requestFocus();
            return false;
        } else if (strProduct.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product .", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
            return false;
        }
        return true;
    }

    private void newCreditNote(boolean isPrint) {
        try {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            int nRowCount = model.getRowCount();
            Product productSelected = null;
            if (nRowCount > 0) {
                CreditDebitNote cd = new CreditDebitNote();
                List<CreditDebitNoteLineItem> cdList = new ArrayList<CreditDebitNoteLineItem>();
                double fTotalAmountTax = 0, fTotalAmount = 0, fTotalCessAmount = 0, fTotalIGST = 0, fTotalCGST = 0, fTotalSGST = 0;
                for (int ii = 0; ii < nRowCount; ii++) {
                    //String date = sft.format(model.getValueAt(ii, 2));
                    for (Product product : products) {
                        if (product.getName().equals(model.getValueAt(ii, 6).toString())) {
                            productSelected = product;
                        }
                    }
                    int nLineNum = Integer.parseInt(model.getValueAt(ii, 3).toString());
                    int nQty1 = Integer.parseInt(model.getValueAt(ii, 11).toString());
                    int nQty2 = Integer.parseInt(model.getValueAt(ii, 12).toString());

                    int nQuantity = (nQty1 * productSelected.getUQC2Value()) + nQty2;
                    double fAmountTax = Double.parseDouble(model.getValueAt(ii, 9).toString());
                    double fAmount = Double.parseDouble(model.getValueAt(ii, 13).toString());
                    double fcessAmount = Double.parseDouble(model.getValueAt(ii, 14).toString());
                    double fTaxAmount = fAmountTax - fAmount;
                    double fIGSTAmount = Double.parseDouble(model.getValueAt(ii, 17).toString());
                    double fCGSTAmount = Double.parseDouble(model.getValueAt(ii, 19).toString());
                    double fSGSTAmount = Double.parseDouble(model.getValueAt(ii, 21).toString());
                    fTotalAmountTax = fTotalAmountTax + fAmountTax;
                    fTotalAmount = fTotalAmount + fAmount;
                    fTotalCessAmount = fTotalCessAmount + fcessAmount;
                    fTotalIGST = fTotalIGST + fIGSTAmount;
                    fTotalCGST = fTotalCGST + fCGSTAmount;
                    fTotalSGST = fTotalSGST + fSGSTAmount;

                    CreditDebitNoteLineItem cdNote = new CreditDebitNoteLineItem();

                    cdNote.setNoteLineNumber((int) model.getValueAt(ii, 0));
                    cdNote.setInvoiceNumber(model.getValueAt(ii, 1).toString());
                    cdNote.setInvoiceDate(sft.parse(model.getValueAt(ii, 2).toString()));
                    cdNote.setLineNumber(nLineNum);
                    cdNote.setGstPOS(model.getValueAt(ii, 4).toString());
                    GoDown godown = findGodownByName(model.getValueAt(ii, 5).toString());
                    cdNote.setGodown(godown);
                    cdNote.setProduct(productSelected);
                    cdNote.setProductName(model.getValueAt(ii, 6).toString());
                    cdNote.setGstReason(model.getValueAt(ii, 7).toString());
                    cdNote.setUqc1Qty(nQty1);
                    cdNote.setUqc2Qty(nQty2);
                    cdNote.setStockQuantity(-nQuantity);
                    cdNote.setValueInclusiveOfTax(BigDecimal.valueOf(fAmountTax));
                    cdNote.setValue(BigDecimal.valueOf(fAmount));
                    cdNote.setIgstValue(BigDecimal.valueOf(fIGSTAmount));
                    cdNote.setCgstValue(BigDecimal.valueOf(fCGSTAmount));
                    cdNote.setSgstValue(BigDecimal.valueOf(fSGSTAmount));
                    cdNote.setCessAmount(BigDecimal.valueOf(fcessAmount));
                    cdNote.setIsIGST((boolean) model.getValueAt(ii, 15));
                    float igstPer = Float.parseFloat(model.getValueAt(ii, 16).toString()),
                            cgstPer = Float.parseFloat(model.getValueAt(ii, 18).toString()),
                            sgstPer = Float.parseFloat(model.getValueAt(ii, 20).toString());
                    cdNote.setGstPercentage(igstPer);
                    cdNote.setIgstPercentage(igstPer);
                    cdNote.setCgstPercentage(cgstPer);
                    cdNote.setSgstPercentage(sgstPer);
                    cdNote.setAccountsLedger(null);
                    cdNote.setParticulars("");
                    cdList.add(cdNote);
                }
                boolean isRegistered = false;
                Ledger selectLedger = null;
                String party = (String) comboParty.getSelectedItem();
                if (!party.equals("No Party")) {
                    for (Ledger l : sortedLedgers) {
                        if (l.getLedgerName().equals(party)) {
                            selectLedger = l;
                            break;
                        }
                    }
                }
                if ((selectLedger != null) && (selectLedger.getGSTIN() != null) && (!selectLedger.getGSTIN().equals(""))) {
                    isRegistered = true;
                }

                cd.setIsRegistered(isRegistered);
                cd.setCompany(company);
                cd.setLedger(selectLedger);
                cd.setNoteDate(dateNote.getDate());
                cd.setNoteType("DEBIT-NOTE");
                cd.setNoteValue(BigDecimal.valueOf(fTotalAmount));
                cd.setNoteValueInclusiveOfTax(BigDecimal.valueOf(fTotalAmountTax));
                cd.setCessValue(BigDecimal.valueOf(fTotalCessAmount));
                cd.setIgstValue(BigDecimal.valueOf(fTotalCGST));
                cd.setCgstValue(BigDecimal.valueOf(fTotalCGST));
                cd.setSgstValue(BigDecimal.valueOf(fTotalCGST));
                cd.setTotalAmountPaid(new BigDecimal(0));

                EventStatus result = new CreditDebitNoteLogic().createCDN(cd, cdList);

                if (result.isCreateDone()) {
//                    if (isPrint) {
//
//                    }
                    JOptionPane.showMessageDialog(null, "New Debit Note is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clearAll();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please add Atleast one Debit Note", "Alert", JOptionPane.WARNING_MESSAGE);

            }

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    private void oldCreditNote(boolean isPrint) {
        try {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            int nRowCount = model.getRowCount();

            if (nRowCount > 0) {
                List<CreditDebitNoteLineItem> cdList = new ArrayList<CreditDebitNoteLineItem>();
                double fTotalAmountTax = 0, fTotalAmount = 0, fTotalCessAmount = 0, fTotalIGST = 0, fTotalCGST = 0, fTotalSGST = 0;
                for (int ii = 0; ii < nRowCount; ii++) {
                    //String date = sft.format(model.getValueAt(ii, 2));
                    Product productSelected = null;
                    for (Product product : products) {
                        if (product.getName().equals(model.getValueAt(ii, 6).toString())) {
                            productSelected = product;
                        }
                    }
                    int nLineNum = Integer.parseInt(model.getValueAt(ii, 3).toString());
                    int nQty1 = Integer.parseInt(model.getValueAt(ii, 11).toString());
                    int nQty2 = Integer.parseInt(model.getValueAt(ii, 12).toString());

                    int nQuantity = (nQty1 * productSelected.getUQC2Value()) + nQty2;
                    double fAmountTax = Double.parseDouble(model.getValueAt(ii, 9).toString());
                    double fAmount = Double.parseDouble(model.getValueAt(ii, 13).toString());
                    double fcessAmount = Double.parseDouble(model.getValueAt(ii, 14).toString());
                    double fTaxAmount = fAmountTax - fAmount;
                    double fIGSTAmount = Double.parseDouble(model.getValueAt(ii, 17).toString());
                    double fCGSTAmount = Double.parseDouble(model.getValueAt(ii, 19).toString());
                    double fSGSTAmount = Double.parseDouble(model.getValueAt(ii, 21).toString());
                    fTotalAmountTax = fTotalAmountTax + fAmountTax;
                    fTotalAmount = fTotalAmount + fAmount;
                    fTotalCessAmount = fTotalCessAmount + fcessAmount;
                    fTotalIGST = fTotalIGST + fIGSTAmount;
                    fTotalCGST = fTotalCGST + fCGSTAmount;
                    fTotalSGST = fTotalSGST + fSGSTAmount;

                    CreditDebitNoteLineItem cdNote = new CreditDebitNoteLineItem();

                    cdNote.setNoteLineNumber((int) model.getValueAt(ii, 0));
                    cdNote.setInvoiceNumber(model.getValueAt(ii, 1).toString());
                    cdNote.setInvoiceDate(sft.parse(model.getValueAt(ii, 2).toString()));
                    cdNote.setLineNumber(nLineNum);
                    cdNote.setGstPOS(model.getValueAt(ii, 4).toString());
                    GoDown godown = findGodownByName(model.getValueAt(ii, 5).toString());
                    cdNote.setGodown(godown);
                    cdNote.setProduct(productSelected);
                    cdNote.setProductName(model.getValueAt(ii, 6).toString());
                    cdNote.setGstReason(model.getValueAt(ii, 7).toString());
                    cdNote.setUqc1Qty(nQty1);
                    cdNote.setUqc2Qty(nQty2);
                    cdNote.setStockQuantity(-nQuantity);
                    cdNote.setValueInclusiveOfTax(BigDecimal.valueOf(fAmountTax));
                    cdNote.setValue(BigDecimal.valueOf(fAmount));
                    cdNote.setIgstValue(BigDecimal.valueOf(fIGSTAmount));
                    cdNote.setCgstValue(BigDecimal.valueOf(fCGSTAmount));
                    cdNote.setSgstValue(BigDecimal.valueOf(fSGSTAmount));
                    cdNote.setCessAmount(BigDecimal.valueOf(fcessAmount));
                    cdNote.setIsIGST((boolean) model.getValueAt(ii, 15));
                    float igstPer = Float.parseFloat(model.getValueAt(ii, 16).toString()),
                            cgstPer = Float.parseFloat(model.getValueAt(ii, 18).toString()),
                            sgstPer = Float.parseFloat(model.getValueAt(ii, 20).toString());
                    cdNote.setGstPercentage(igstPer);
                    cdNote.setIgstPercentage(igstPer);
                    cdNote.setCgstPercentage(cgstPer);
                    cdNote.setSgstPercentage(sgstPer);
                    cdNote.setAccountsLedger(null);
                    cdNote.setParticulars("");
                    cdList.add(cdNote);
                }
                boolean isRegistered = false;
                Ledger selectLedger = null;
                String party = (String) comboParty.getSelectedItem();
                if (!party.equals("No Party")) {
                    for (Ledger l : sortedLedgers) {
                        if (l.getLedgerName().equals(party)) {
                            selectLedger = l;
                            break;
                        }
                    }
                }
                if ((selectLedger != null) && (selectLedger.getGSTIN() != null) && (!selectLedger.getGSTIN().equals(""))) {
                    isRegistered = true;
                }

                oldNote.setIsRegistered(isRegistered);
//                oldNote.setCompany(company);
//                oldNote.setLedger(selectLedger);
//                oldNote.setNoteDate(dateNote.getDate());
//                oldNote.setNoteType("DEBIT-NOTE");
                oldNote.setNoteValue(BigDecimal.valueOf(fTotalAmount));
                oldNote.setNoteValueInclusiveOfTax(BigDecimal.valueOf(fTotalAmountTax));
                oldNote.setCessValue(BigDecimal.valueOf(fTotalCessAmount));
                oldNote.setIgstValue(BigDecimal.valueOf(fTotalCGST));
                oldNote.setCgstValue(BigDecimal.valueOf(fTotalCGST));
                oldNote.setSgstValue(BigDecimal.valueOf(fTotalCGST));
                oldNote.setTotalAmountPaid(new BigDecimal(0));

                EventStatus result = new CreditDebitNoteLogic().updateCDN(oldNote, cdList);

                if (result.isUpdateDone()) {
//                    if (isPrint) {
//
//                    }
                    JOptionPane.showMessageDialog(null, "Debit Note is Successfully Updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                    new DebitNoteDateWiseReportUI().setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please add Atleast one Debit Note", "Alert", JOptionPane.WARNING_MESSAGE);

            }

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public Ledger findLedgerData() {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : sortedLedgers) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                Long LedgerId = ledger.getLedgerId();
                break;
            }
        }
        return ledgerData;
    }

    public void clearAll() {
        comboParty.requestFocus();
        comboParty.setSelectedItem("No Party");
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        dateNote.setDate(new Date());
        txtPartyName.setText("");
        txtAddress.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        comboState.setSelectedItem("Tamil Nadu");
        txtGSTIN.setText("");
        clearData(true);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DebitNoteUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DebitNoteUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> comboGodown;
    private javax.swing.JComboBox<String> comboPOS;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboProduct;
    private javax.swing.JComboBox<String> comboReason;
    private javax.swing.JComboBox<String> comboState;
    private com.toedter.calendar.JDateChooser dateInv;
    private com.toedter.calendar.JDateChooser dateNote;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCGST;
    private javax.swing.JLabel labelCGSTper;
    private javax.swing.JLabel labelIGST;
    private javax.swing.JLabel labelIGSTper;
    private javax.swing.JLabel labelSGST;
    private javax.swing.JLabel labelSGSTper;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel lblAmount;
    private javax.swing.JLabel lblInvoiceDate;
    private javax.swing.JLabel lblInvoiceNum;
    private javax.swing.JLabel lblLineNum;
    private javax.swing.JLabel lblProduct;
    private javax.swing.JLabel lblQty;
    private javax.swing.JLabel lblReason;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JPanel productPane;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtAmountIncl;
    private javax.swing.JTextField txtCGSTAmt;
    private javax.swing.JTextField txtCessAmount;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JTextField txtIGSTAmt;
    private javax.swing.JTextField txtInvNum;
    private javax.swing.JTextField txtLineNumber;
    private javax.swing.JTextField txtPartyName;
    private javax.swing.JTextField txtSGSTAmt;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC2Qty;
    // End of variables declaration//GEN-END:variables
}
