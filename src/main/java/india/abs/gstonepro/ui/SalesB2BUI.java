/*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.dao.GoDownStockCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PricingPolicyLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.pdf.TaxInvoiceNew;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.GetPDFHeaders;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author ABS
 */
public class SalesB2BUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String HSNCode;
    boolean isIGST = false, isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    int lastBillId;
    Long LedgerId;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0, BaseProductRate = 0;
    float IGSTper = 0, CGSTper = 0, SGSTper = 0;
    long productStock = 0;
    float billAmount = 0, billprofit, amountWithoutDiscount, amount, discount;
    Boolean isOldBill = false;
    boolean bChanged = false;
    boolean isQty = true;

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Long companyId = company.getCompanyId();
    String CompanyState = SessionDataUtil.getSelectedCompany().getState();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    Ledger ledgerData = new Ledger();
    //   Product product = null;
    Product productDetail = new Product();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    List<Ledger> b2bPartyLedger = new ArrayList<>();
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(companyId);

    List<PricingPolicy> pricingPolicy = new PricingPolicyLogic().fetchAllPP(companyId);
    Set<SingleProductPolicy> recentSingleProductPolicies = new HashSet<>();
    ShippingAddressUI shippingAddressUI = new ShippingAddressUI();
    PSLineItemsDescriptionUI productDescriptionUI = null;
    List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(companyId);

    PurchaseSale oldSale = new PurchaseSale();
    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");

    boolean bUQCSame = false;
    boolean descAdded = false;
    boolean isProfitEnabled = false;
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();
    boolean isUnderStockPossible = false;
    boolean bCheckStock = false;
    boolean bErrorAlert = false;
    GoDownStockDetail stock = null;
    String desc1 = "";
    String desc2 = "";
    String desc3 = "";
    String desc4 = "";
    String desc5 = "";
    String desc6 = "";
    String desc7 = "";
    String desc8 = "";
    String desc9 = "";
    String desc10 = "";
    int descCount = 0;

    ///////////////total values are set global////////////////
    float totalAmount = 0,
            totalIGST = 0,
            totalCGST = 0,
            totalSGST = 0,
            totalProfit = 0,
            totalCess = 0, NetAmount = 0, roundOff = 0;
    Boolean needPrint = false;
    boolean isServiceProduct = false;
///////////////total values are set global////////////////

    /**
     * Creates new form Bill
     */
    public SalesB2BUI() {
        try {
            initComponents();
            setExtendedState(MAXIMIZED_BOTH);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setGoDown();
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                @Override
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });

            Collections.sort(products, new Comparator<Product>() {
                @Override
                public int compare(Product o1, Product o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            saveData();

            labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            dateBill.setDate(new Date());

            setProduct();
            setPricingPolicy();
            setBanks();
            //        radioCash.setSelected(true);
            checkRadioButton();
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());

            btnProductDescription.setEnabled(false);
            setParty();
            labelPartyName.setText("");
            labelAddress.setText("");
            labelGSTIN.setText("");
            txtDiscount.setColumns(06);

            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            totalTable.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));

            checkSMS.setSelected(true);
            txtMessage.setEnabled(checkSMS.isSelected());
            comboParty.requestFocus();
            comboParty.setSelectedItem("");

            KeyStroke right = KeyStroke.getKeyStroke("alt RIGHT");
            KeyStroke left = KeyStroke.getKeyStroke("alt LEFT");
            InputMap inputMap = jTabbedPane1.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            inputMap.put(right, "navigateNext");
            inputMap.put(left, "navigatePrevious");

            JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
            editor.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
                        setNewProduct();
                    }
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboProduct.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            for (Product product : products) {
                                if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(product.getName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboProduct.setModel(model);
                            comboProduct.getEditor().setItem(val);
                            comboProduct.setPopupVisible(true);
                        }
                    }
                }

                @Override
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_N) {
                        new NewProductUI().setVisible(true);
                    }
                    else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();

                        for (Product product : products) {
                            scripts.add(product.getName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem("");
                        comboProduct.setPopupVisible(true);
                    }
                    else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboProduct.setSelectedItem("");
                        comboProduct.getEditor().setItem(selectedObj);
                        comboProduct.setPopupVisible(false);
                        getProductDetails();
                    }
                }
            });

            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
            editor1.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    String val1 = (String) comboParty.getEditor().getItem();
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboParty.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();

                            for (Ledger ledger : b2bPartyLedger) {
                                if ((ledger.getLedgerName().toLowerCase().contains(val.toLowerCase()))) {
                                    scripts.add(ledger.getLedgerName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboParty.setModel(model);
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }

                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        for (Ledger ledger : b2bPartyLedger) {
                            scripts.add(ledger.getLedgerName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);
                        comboParty.getEditor().setItem("");
                        comboParty.setPopupVisible(true);
                    }
                    else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboParty.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboParty.setPopupVisible(false);
                        comboParty.getEditor().setItem(selectedObj);
                        getPartyDetails();
                        dateBill.requestFocusInWindow();
                    }
                }
            });

            ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        }
                        else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    }
                    else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        }
                        else if (len == 4) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                            sft.setLenient(false);
                            Date date = sft.parse(strDate);
                            if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                jTabbedPane1.setSelectedIndex(1);
                            }
                            else {
                                JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                            }
                            evt.consume();
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

        } catch (Exception ex) {
            Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setNewProduct() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboProduct.setModel(model);
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    private void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    private void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        }
        else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());

        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        }
        else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        }
        else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        }
        else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        }
        else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        }
        else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        }
        else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }

    }

    private void setBanks() {
        for (Ledger bank : Banks) {
            //            comboBank.addItem(bank.getLedgerName());
        };
    }

    private void checkRadioButton() {
        //        comboBank.setEnabled(radioBank.isSelected());
    }

    private void setPricingPolicy() {

        for (PricingPolicy policy : pricingPolicy) {
            comboPricingPolicy.addItem(policy.getPolicyName());
        }
    }

    public void EditOldBill(String id) {
        try {
            setPartyEditable(false);
            isOldBill = true;
            oldSale = new PurchaseSaleLogic().getPSDetail(id);
            dateBill.setDate(oldSale.getPsDate());
            List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
            List<PurchaseSaleTaxSummary> psTSs = oldSale.getPsTaxSummaries();
            oldSale.printCollections();
            LedgerId = oldSale.getLedger().getLedgerId();
            comboParty.setSelectedItem(oldSale.getLedger().getLedgerName());

            txtPartyName.setText(oldSale.getLedger().getLedgerName());
            String address = oldSale.getLedger().getAddress();
            String[] words = address.split("/~/");
            txtAddr1.setText(words[0]);
            txtAddr2.setText(words[1]);
            txtAddr3.setText(words[2]);
            txtCity.setText(oldSale.getLedger().getCity());
            txtDistrict.setText(oldSale.getLedger().getDistrict());
            txtState.setText(oldSale.getLedger().getState());
            txtEmail.setText(oldSale.getLedger().getEmail());
            txtMobile.setText(oldSale.getLedger().getMobile());
            txtPhone.setText(oldSale.getLedger().getPhone());
            txtGSTIN.setText(oldSale.getLedger().getGSTIN());
            isIGST = oldSale.isIsIGST();
            comboPricingPolicy.setSelectedItem(oldSale.getPricingPolicyName());
            checkSMS.setSelected((!oldSale.getSmsMessage().equals("")));
            txtMessage.setEnabled(checkSMS.isSelected());
            txtMessage.setText(oldSale.getSmsMessage());
            labelPartyName.setText(oldSale.getLedger().getLedgerName());
            labelAddress.setText(words[0] + "  " + words[1] + "  " + words[2]);
            labelGSTIN.setText(oldSale.getLedger().getGSTIN());
            messageCount();

            shippingAddressUI.name = oldSale.getShippingAddressName();
            shippingAddressUI.addr1 = oldSale.getShippingAddressOne();
            shippingAddressUI.addr2 = oldSale.getShippingAddressTwo();
            shippingAddressUI.addr3 = oldSale.getShippingAddressThree();
            shippingAddressUI.city = oldSale.getShippingAddressCity();
            shippingAddressUI.district = oldSale.getShippingAddressDistrict();
            shippingAddressUI.state = oldSale.getShippingAddressState();
            shippingAddressUI.mobile = oldSale.getShippingAddressMobile();
            shippingAddressUI.phone = oldSale.getShippingAddressPhone();
            shippingAddressUI.SetTextBox();
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            if (model.getRowCount() > 0) {
                model.setRowCount(0);
            }
            for (PurchaseSaleLineItem item : psLIs) {
                BigDecimal cessAmt = item.getCessAmount();
                if (cessAmt == null) {
                    cessAmt = new BigDecimal(0);
                }
                if (!item.getProduct().isService()) {
                    String strGodown = "";
                    if (isInventoryEnabled) {
                        strGodown = item.getGodownStockEntry().getGodown().getGoDownName();
                    }
                    String strQuantity = "";
                    if (item.getUqcOneQuantity() == 0) {
                        strQuantity = item.getUqcTwoQuantity() + " " + item.getUqcTwo();
                    }
                    else if (item.getUqcTwoQuantity() == 0) {
                        strQuantity = item.getUqcOneQuantity() + " " + item.getUqcOne();
                    }
                    else {
                        strQuantity = item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo();
                    }
                    Object[] row = {
                        item.getLineNumber(),
                        item.getProductName(),
                        item.getHsnSac(),
                        item.getUqcOne(),
                        item.getUqcOneQuantity(),
                        item.getUqcOneRate(),
                        (new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate())).floatValue(),
                        item.getUqcTwo(),
                        item.getUqcTwoQuantity(),
                        item.getUqcTwoRate(),
                        (new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate())).floatValue(),
                        strQuantity,
                        item.getStockQuantity(),
                        item.getDiscountPercent(),
                        item.getDiscountValue().floatValue(),
                        item.getValue().floatValue(),
                        item.getIgstPercentage(),
                        item.getIgstValue().floatValue(),
                        item.getCgstPercentage(),
                        item.getCgstValue().floatValue(),
                        item.getSgstPercentage(),
                        item.getSgstValue().floatValue(),
                        item.getUqc1BaseRate().floatValue(),
                        item.getUqc2BaseRate().floatValue(),
                        item.getProfit().floatValue(),
                        item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue()))).floatValue(),
                        cessAmt.floatValue(),
                        item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue().add(cessAmt)))).floatValue(),
                        strGodown,
                        item.getDescriptionOne(), item.getDescriptionTwo(), item.getDescriptionThree(), item.getDescriptionFour(),
                        item.getDescriptionFive(), item.getDescriptionSix(), item.getDescriptionSeven(), item.getDescriptionEight(),
                        item.getDescriptionNine(), item.getDescriptionTen(), item.getDescriptionCount(),
                        item.isService(),
                        item.getUqcOneRateWithTax().floatValue(),
                        item.getUqcTwoRateWithTax().floatValue(),};
                    model.addRow(row);
                }
                else {
                    Object[] row = {
                        item.getLineNumber(),
                        item.getProductName(),
                        item.getHsnSac(),
                        "",
                        0,
                        item.getUqcOneRate(),
                        0,
                        "",
                        0,
                        0,
                        0,
                        "",
                        0,
                        item.getDiscountPercent(),
                        item.getDiscountValue().floatValue(),
                        item.getValue().floatValue(),
                        item.getIgstPercentage(),
                        item.getIgstValue().floatValue(),
                        item.getCgstPercentage(),
                        item.getCgstValue().floatValue(),
                        item.getSgstPercentage(),
                        item.getSgstValue().floatValue(),
                        item.getUqc1BaseRate().floatValue(),
                        item.getUqc2BaseRate().floatValue(),
                        item.getProfit().floatValue(),
                        item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue()))).floatValue(),
                        cessAmt.floatValue(),
                        item.getValue().add(item.getIgstValue().add(item.getCgstValue().add(item.getSgstValue().add(cessAmt)))).floatValue(),
                        "", item.getDescriptionOne(), item.getDescriptionTwo(), item.getDescriptionThree(), item.getDescriptionFour(),
                        item.getDescriptionFive(), item.getDescriptionSix(), item.getDescriptionSeven(), item.getDescriptionEight(),
                        item.getDescriptionNine(), item.getDescriptionTen(), item.getDescriptionCount(),
                        item.isService(),
                        item.getUqcOneRateWithTax().floatValue(),
                        item.getUqcTwoRateWithTax().floatValue(),};
                    model.addRow(row);
                }
            }
            clear();
            TotalCalculate();
            jTableRender();
            comboProduct.requestFocus();
            setTable();
        } catch (Exception ex) {
            Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String setStockConverstion(long qty, String UQC1, String UQC2, int UQC2Value) {
        long UQC1Count = qty / UQC2Value;
        long UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void getProductDetails() {
        bUQCSame = false;
        descAdded = false;
        String productName = (String) comboProduct.getEditor().getItem();

        if ((productName != null) && (!(productName.equals("")))) {
            for (Product selProduct : products) {
                if (selProduct.getName().equals(productName)) {
                    btnProductDescription.setEnabled(true);
                    productDetail = selProduct;
                    isServiceProduct = selProduct.isService();

                    if (isServiceProduct) {
                        bUQCSame = true;
                        labelUQC1.setText("");
                        labelUQC2.setText("");
                        txtUQC1Qty.setText("");
                        txtUQC1Qty.setEnabled(false);
                        txtUQC2Qty.setText("");
                        txtUQC2Qty.setEnabled(false);
                        labelUQC1forPrice.setText("");
                        labelUQC2forPrice.setText("");
//                        btnProductDescription.setEnabled(false);
                        labelUQC1forPriceWithTax.setText("");
                        labelUQC2forPriceWithTax.setText("");

                        txtUQC1RateWithTax.setText("");
                        txtUQC2RateWithTax.setText("");
                        txtUQC2RateWithTax.setEnabled(false);

                        txtUQC2Rate.setText("");
                        txtUQC1Rate.setText("");
                        txtUQC1Rate.setEnabled(true);
                        txtUQC2Rate.setEnabled(false);

                        setServiceDetails(productName);
                    }
                    else {
                        if (productDetail.getUQC1().equalsIgnoreCase(productDetail.getUQC2())) {
                            bUQCSame = true;
                            labelUQC2.setEnabled(false);
                            labelUQC2forPriceWithTax.setEnabled(false);
                            txtUQC2Qty.setEnabled(false);
                            txtUQC2RateWithTax.setEnabled(false);
                            labelUQC2forPrice.setEnabled(false);
                            txtUQC2Rate.setEnabled(false);
                        }
                        else {
                            bUQCSame = false;
                            labelUQC2.setEnabled(true);
                            labelUQC2forPriceWithTax.setEnabled(true);
                            txtUQC2Qty.setEnabled(true);
                            txtUQC2RateWithTax.setEnabled(true);
                            labelUQC2forPrice.setEnabled(true);
                            txtUQC2Rate.setEnabled(true);
                        }
                        setProductDetails(productName);
                    }
                    break;
                }
            }

        }
        else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtUQC1RateWithTax.setText("");
            txtUQC2RateWithTax.setText("");
            labelProductTotal.setText("");
            txtIGSTper.setText("");
            txtCGSTper.setText("");
            txtSGSTper.setText("");
            txtIGSTamt.setText("");
            txtCGSTamt.setText("");
            txtSGSTamt.setText("");

            labelUQC1forPrice.setText("");
            labelUQC2forPrice.setText("");
            txtUQC2Rate.setText("");
            txtUQC1Rate.setText("");

            labelStock.setText("N/A");
            comboProduct.setSelectedItem("");
            comboProduct.requestFocus();
        }
    }

    private void setServiceDetails(String productName) {

        HSNCode = productDetail.getHsnCode();
        UQC2Value = productDetail.getUQC2Value();
        if (productDetail.getBaseProductRate() != null) {
            BaseProductRate = productDetail.getBaseProductRate().floatValue();
        }
        else {
            BaseProductRate = 0;
        }

        IGSTper = productDetail.getIgstPercentage();
        CGSTper = productDetail.getCgstPercentage();
        SGSTper = productDetail.getSgstPercentage();
        txtIGSTper.setText(String.valueOf(productDetail.getIgstPercentage()));
        txtCGSTper.setText(String.valueOf(productDetail.getCgstPercentage()));
        txtSGSTper.setText(String.valueOf(productDetail.getSgstPercentage()));
        isBasePriceProductTaxInclusive = productDetail.isBasePriceInclusiveOfGST();

        float basePrice = 0;
        if ((isBasePriceProductTaxInclusive) && (BaseProductRate > 0)) {
            basePrice = (BaseProductRate / (100 + IGSTper)) * 100;
        }
        else {
            basePrice = BaseProductRate;
        }

        ///////////Price Policy set//////////////////////////////////
        String nameOfPolicy = (String) comboPricingPolicy.getSelectedItem();
        if (nameOfPolicy.equals("Default")) {
            ProductRate = productDetail.getProductRate().floatValue();
            isProductTaxInclusive = productDetail.isInclusiveOfGST();
            float price = 0, priceWithTax = 0;
            if (isProductTaxInclusive) {
                price = (ProductRate / (100 + IGSTper)) * 100;
                priceWithTax = ProductRate;
            }
            else {
                price = ProductRate;
                priceWithTax = price + ((IGSTper / 100) * price);
            }
            txtUQC1Rate.setText(currency.format(price));
            txtUQC1RateWithTax.setText(currency.format(priceWithTax));
        }
        else {
            Boolean isNotHere = true;
            for (SingleProductPolicy productPolicy : recentSingleProductPolicies) {
                if (productPolicy.getProduct().getName().equals(productName)) {
                    float uqc1Rate = productPolicy.getUqc1Rate().floatValue();
                    isProductTaxInclusive = productPolicy.isInclusiveOfGST();
                    float Uqc1price = 0, Uqc1PriceWithTax = 0;
                    if (isProductTaxInclusive) {
                        Uqc1price = (uqc1Rate / (100 + IGSTper)) * 100;
                        Uqc1PriceWithTax = uqc1Rate;
                    }
                    else {
                        Uqc1price = uqc1Rate;
                        Uqc1PriceWithTax = uqc1Rate + ((uqc1Rate * IGSTper) / 100);
                    }
                    txtUQC1Rate.setText(currency.format(Uqc1price));
                    txtUQC1RateWithTax.setText(currency.format(Uqc1PriceWithTax));
                    isNotHere = false;
                }
            }
            if (isNotHere) {
                JOptionPane.showMessageDialog(null, productName + " Service is not available in " + nameOfPolicy, "Alert", JOptionPane.WARNING_MESSAGE);

                ProductRate = productDetail.getProductRate().floatValue();
                isProductTaxInclusive = productDetail.isInclusiveOfGST();
                float price = 0, priceWithTax = 0;
                if (isProductTaxInclusive) {
                    price = (ProductRate / (100 + IGSTper)) * 100;
                    priceWithTax = ProductRate;
                }
                else {
                    price = ProductRate;
                    priceWithTax = price + ((IGSTper / 100) * price);
                }
                txtUQC1Rate.setText(currency.format(price));
                txtUQC1RateWithTax.setText(currency.format(priceWithTax));

            }
        }
        if (isProductTaxInclusive) {
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            txtUQC1Rate.requestFocus();
        }
        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);

        labelStock.setText("");
    }

    private void setProductTaxIncluviceHideAndShow(Boolean isTaxInclusive) {
        txtUQC1RateWithTax.setEnabled(isTaxInclusive);
        txtUQC2RateWithTax.setEnabled(isTaxInclusive && !bUQCSame);
        txtUQC1Rate.setEnabled(!isTaxInclusive);
        txtUQC2Rate.setEnabled(!isTaxInclusive && !bUQCSame);
    }

    private void setProductDetails(String productName) {
        labelUQC1.setText(productDetail.getUQC1());
        labelUQC1forPrice.setText(productDetail.getUQC1());
        labelUQC1forPriceWithTax.setText(productDetail.getUQC1());
        labelUQC2.setText(productDetail.getUQC2());
        labelUQC2forPrice.setText(productDetail.getUQC2());
        labelUQC2forPriceWithTax.setText(productDetail.getUQC2());
        HSNCode = productDetail.getHsnCode();
        UQC2Value = productDetail.getUQC2Value();
        if (productDetail.getBaseProductRate() != null) {
            BaseProductRate = productDetail.getBaseProductRate().floatValue();
        }
        else {
            BaseProductRate = 0;
        }

        IGSTper = productDetail.getIgstPercentage();
        CGSTper = productDetail.getCgstPercentage();
        SGSTper = productDetail.getSgstPercentage();
        txtIGSTper.setText(String.valueOf(productDetail.getIgstPercentage()));
        txtCGSTper.setText(String.valueOf(productDetail.getCgstPercentage()));
        txtSGSTper.setText(String.valueOf(productDetail.getSgstPercentage()));
        isBasePriceProductTaxInclusive = productDetail.isBasePriceInclusiveOfGST();

        float basePrice = 0;
        if ((isBasePriceProductTaxInclusive) && (BaseProductRate > 0)) {
            basePrice = (BaseProductRate / (100 + IGSTper)) * 100;
        }
        else {
            basePrice = BaseProductRate;
        }
        //                    txtUQC1BaseRate.setText(currency.format(basePrice * UQC2Value));
        //                    txtUQC2BaseRate.setText(currency.format(basePrice));

        ///////////Price Policy set//////////////////////////////////
        String nameOfPolicy = (String) comboPricingPolicy.getSelectedItem();
        if (nameOfPolicy.equals("Default")) {
            ProductRate = productDetail.getProductRate().floatValue();
            isProductTaxInclusive = productDetail.isInclusiveOfGST();
            float price = 0, priceWithTax = 0;
            if (isProductTaxInclusive) {
                price = (ProductRate / (100 + IGSTper)) * 100;
                priceWithTax = ProductRate;
            }
            else {
                price = ProductRate;
                priceWithTax = price + ((IGSTper / 100) * price);
            }
            txtUQC1Rate.setText(currency.format(price * UQC2Value));
            txtUQC2Rate.setText(currency.format(price));
            txtUQC1RateWithTax.setText(currency.format(priceWithTax * UQC2Value));
            txtUQC2RateWithTax.setText(currency.format(priceWithTax));
            txtUQC1Qty.requestFocus();
        }
        else {
            Boolean isNotHere = true;
            for (SingleProductPolicy productPolicy : recentSingleProductPolicies) {
                if (productPolicy.getProduct().getName().equals(productName)) {
                    float uqc1Rate = productPolicy.getUqc1Rate().floatValue(),
                            uqc2Rate = productPolicy.getUqc2Rate().floatValue();
                    isProductTaxInclusive = productPolicy.isInclusiveOfGST();
                    float Uqc1price = 0, Uqc2price = 0, Uqc1PriceWithTax = 0, Uqc2PriceWithTax = 0;
                    if (isProductTaxInclusive) {
                        Uqc1price = (uqc1Rate / (100 + IGSTper)) * 100;
                        Uqc2price = (uqc2Rate / (100 + IGSTper)) * 100;
                        Uqc1PriceWithTax = uqc1Rate;
                        Uqc2PriceWithTax = uqc2Rate;
                    }
                    else {
                        Uqc1price = uqc1Rate;
                        Uqc2price = uqc2Rate;
                        Uqc1PriceWithTax = uqc1Rate + ((uqc1Rate * IGSTper) / 100);
                        Uqc2PriceWithTax = uqc2Rate + ((uqc2Rate * IGSTper) / 100);
                    }
                    txtUQC1Rate.setText(currency.format(Uqc1price));
                    txtUQC2Rate.setText(currency.format(Uqc2price));
                    txtUQC1RateWithTax.setText(currency.format(Uqc1PriceWithTax));
                    txtUQC2RateWithTax.setText(currency.format(Uqc2PriceWithTax));
                    txtUQC1Qty.requestFocus();
                    isNotHere = false;
                }
            }
            if (isNotHere) {
                JOptionPane.showMessageDialog(null, productName + " product is not available in " + nameOfPolicy, "Alert", JOptionPane.WARNING_MESSAGE);

                ProductRate = productDetail.getProductRate().floatValue();
                isProductTaxInclusive = productDetail.isInclusiveOfGST();
                float price = 0, priceWithTax = 0;
                if (isProductTaxInclusive) {
                    price = (ProductRate / (100 + IGSTper)) * 100;
                    priceWithTax = ProductRate;
                }
                else {
                    price = ProductRate;
                    priceWithTax = price + ((IGSTper / 100) * price);
                }
                txtUQC1Rate.setText(currency.format(price * UQC2Value));
                txtUQC2Rate.setText(currency.format(price));
                txtUQC1RateWithTax.setText(currency.format(priceWithTax * UQC2Value));
                txtUQC2RateWithTax.setText(currency.format(priceWithTax));
                txtUQC1Qty.requestFocus();

            }
        }
        if (isProductTaxInclusive) {
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            txtUQC1Rate.requestFocus();
        }
        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);
        setStock();
        txtUQC1Qty.requestFocus();
    }

    private void setProduct() {
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboProduct.setModel(model);
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    private void setParty() {
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboParty.setModel(model);
        b2bPartyLedger = new ArrayList<Ledger>();
        for (Ledger ledger : sortedLedgers) {
            if ((!(ledger.getGSTIN().equals(""))) && (ledger.getGSTIN() != null)) {
                comboParty.addItem(ledger.getLedgerName());
                b2bPartyLedger.add(ledger);
            }
        };
        comboParty.setSelectedItem("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel4 = new javax.swing.JLabel();
        paymentDialog = new javax.swing.JDialog();
        btnOk = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        txtPayment = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        labelPaymentBillAmount = new javax.swing.JLabel();
        labelBalance = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtPartyName = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtAddr1 = new javax.swing.JTextField();
        txtAddr2 = new javax.swing.JTextField();
        txtAddr3 = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtMobile = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtGSTIN = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        txtState = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtMessage = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        totalTable = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        jLabel28 = new javax.swing.JLabel();
        labelLeftMessageChar = new javax.swing.JLabel();
        comboPricingPolicy = new javax.swing.JComboBox<>();
        jLabel29 = new javax.swing.JLabel();
        checkSMS = new javax.swing.JCheckBox();
        jLabel33 = new javax.swing.JLabel();
        btnShippingAddress = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        chkReverseCharge = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnProductDescription = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        txtBillDiscount = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        labelBillAmount = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        labelUQC2 = new javax.swing.JLabel();
        lblGodown = new javax.swing.JLabel();
        comboGodown = new javax.swing.JComboBox<>();
        labelProduct = new javax.swing.JLabel();
        comboProduct = new javax.swing.JComboBox<>();
        labelStock = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        lblPrice = new javax.swing.JLabel();
        txtUQC1Rate = new javax.swing.JTextField();
        labelUQC1forPrice = new javax.swing.JLabel();
        labelUQC2forPrice = new javax.swing.JLabel();
        txtUQC2Rate = new javax.swing.JTextField();
        lblPriceWithTax = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        labelProductTotal = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        labelProductTotalWithTax = new javax.swing.JLabel();
        labelUQC1forPriceWithTax = new javax.swing.JLabel();
        labelUQC2forPriceWithTax = new javax.swing.JLabel();
        txtUQC2RateWithTax = new javax.swing.JTextField();
        txtUQC1RateWithTax = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtCessAmount = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtIGSTper = new javax.swing.JLabel();
        txtIGSTamt = new javax.swing.JLabel();
        txtCGSTper = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtCGSTamt = new javax.swing.JLabel();
        txtSGSTper = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtSGSTamt = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        labelPartyName = new javax.swing.JLabel();
        labelAddress = new javax.swing.JLabel();
        labelGSTIN = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        jLabel4.setText("jLabel4");

        paymentDialog.setResizable(false);

        btnOk.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel19.setText("PAYMENT");

        jLabel22.setText("Balance");

        txtPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPaymentActionPerformed(evt);
            }
        });
        txtPayment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaymentKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaymentKeyTyped(evt);
            }
        });

        jLabel32.setText("Amount Tendered");

        jLabel38.setText("Bill Amount");

        labelPaymentBillAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelPaymentBillAmount.setText("0.00");

        labelBalance.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelBalance.setText("0.00");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38)
                    .addComponent(jLabel32)
                    .addComponent(jLabel22))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPaymentBillAmount)
                    .addComponent(labelBalance))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(labelPaymentBillAmount))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(labelBalance))
                .addContainerGap())
        );

        javax.swing.GroupLayout paymentDialogLayout = new javax.swing.GroupLayout(paymentDialog.getContentPane());
        paymentDialog.getContentPane().setLayout(paymentDialogLayout);
        paymentDialogLayout.setHorizontalGroup(
            paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paymentDialogLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paymentDialogLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        paymentDialogLayout.setVerticalGroup(
            paymentDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paymentDialogLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOk)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tax Invoice B2B");

        jTabbedPane1.setToolTipText("");
        jTabbedPane1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Party ");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPartyActionPerformed(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboPartyKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        txtPartyName.setEditable(false);
        txtPartyName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPartyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartyNameActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtAddr1.setEditable(false);
        txtAddr1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr1ActionPerformed(evt);
            }
        });

        txtAddr2.setEditable(false);
        txtAddr2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr2ActionPerformed(evt);
            }
        });

        txtAddr3.setEditable(false);
        txtAddr3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr3ActionPerformed(evt);
            }
        });

        txtCity.setEditable(false);
        txtCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel20.setText("City");

        jLabel25.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel25.setText("District");

        txtDistrict.setEditable(false);
        txtDistrict.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("Mobile Number");

        txtMobile.setEditable(false);
        txtMobile.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileActionPerformed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel24.setText("Phone Number");

        txtPhone.setEditable(false);
        txtPhone.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhoneActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel21.setText("GSTIN/UIN");

        txtGSTIN.setEditable(false);
        txtGSTIN.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Date :");

        dateBill.setDateFormatString("dd-MM-yyyy");
        dateBill.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateBillKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("Email");

        txtEmail.setEditable(false);
        txtEmail.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        txtState.setEditable(false);
        txtState.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStateActionPerformed(evt);
            }
        });

        txtMessage.setColumns(20);
        txtMessage.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtMessage.setRows(5);
        txtMessage.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMessageKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(txtMessage);

        totalTable.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        totalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Total Amount", "IGST", "CGST", "SGST ", "Profit", "Product Total Amount", "Cess Amount", "Total Amount(Incl. Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(totalTable);
        if (totalTable.getColumnModel().getColumnCount() > 0) {
            totalTable.getColumnModel().getColumn(4).setMinWidth(0);
            totalTable.getColumnModel().getColumn(4).setPreferredWidth(0);
            totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
            totalTable.getColumnModel().getColumn(5).setMinWidth(0);
            totalTable.getColumnModel().getColumn(5).setPreferredWidth(0);
            totalTable.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        jLabel28.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel28.setText("Character Left :");

        labelLeftMessageChar.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelLeftMessageChar.setText("0");

        comboPricingPolicy.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        comboPricingPolicy.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Default" }));
        comboPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPricingPolicyActionPerformed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel29.setText("Pricing Policy");

        checkSMS.setText("SMS");
        checkSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkSMSActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel33.setText("Shipping Address");

        btnShippingAddress.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        btnShippingAddress.setText("Shipping Address");
        btnShippingAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShippingAddressActionPerformed(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel34.setText("Reverse Charge");

        chkReverseCharge.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        chkReverseCharge.setText("Yes");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCity, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                                    .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(164, 164, 164)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(checkSMS)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel28)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelLeftMessageChar)))
                                .addGap(153, 153, 153))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addGap(9, 9, 9)
                                .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel6))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtGSTIN)
                                    .addComponent(txtState)
                                    .addComponent(txtEmail))))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 586, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addGap(29, 29, 29)
                        .addComponent(chkReverseCharge))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel29)
                        .addGap(18, 18, 18)
                        .addComponent(comboPricingPolicy, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel33)
                        .addGap(18, 18, 18)
                        .addComponent(btnShippingAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                        .addGap(30, 30, 30)
                        .addComponent(txtAddr1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabel20, jLabel21, jLabel24, jLabel25, jLabel26, jLabel34, jLabel6});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtAddr1, txtCity, txtDistrict, txtEmail, txtGSTIN, txtMobile, txtPhone, txtState});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(jLabel2)
                            .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel28)
                            .addComponent(labelLeftMessageChar)
                            .addComponent(checkSMS)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkReverseCharge)
                    .addComponent(jLabel34))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboPricingPolicy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel33)
                    .addComponent(btnShippingAddress))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabel20, jLabel21, jLabel24, jLabel25, jLabel26, jLabel34, jLabel6});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtAddr1, txtCity, txtDistrict, txtEmail, txtGSTIN, txtMobile, txtPhone, txtState});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(139, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Bill Address", jPanel1);

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product / Service", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Discount %", "Discount", "Amount", "IGST %", "IGST", "CGST %", "CGST", "SGST %", "SGST", "UQC1BaseRate", "UQC2BaseRate", "Profit", "Product Amount", "Cess Amount", "Total Amount (Incl.tax)", "godown", "Desc1", "Desc2", "Desc3", "Desc4", "Desc5", "Desc6", "Desc7", "Desc8", "Desc9", "Desc10", "DescCount", "isService", "UQC1RateWithTax", "UQC2RateWithTax"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(50);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(1).setMinWidth(350);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(350);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(350);
            jTable1.getColumnModel().getColumn(2).setMinWidth(0);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(14).setMinWidth(70);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(70);
            jTable1.getColumnModel().getColumn(14).setMaxWidth(70);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(50);
            jTable1.getColumnModel().getColumn(17).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(50);
            jTable1.getColumnModel().getColumn(19).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(50);
            jTable1.getColumnModel().getColumn(21).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(50);
            jTable1.getColumnModel().getColumn(22).setMinWidth(0);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(23).setMinWidth(0);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(25).setMinWidth(0);
            jTable1.getColumnModel().getColumn(25).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(25).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(26).setMinWidth(0);
            jTable1.getColumnModel().getColumn(26).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(26).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(28).setMinWidth(0);
            jTable1.getColumnModel().getColumn(28).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(28).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(29).setMinWidth(0);
            jTable1.getColumnModel().getColumn(29).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(29).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(30).setMinWidth(0);
            jTable1.getColumnModel().getColumn(30).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(30).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(31).setMinWidth(0);
            jTable1.getColumnModel().getColumn(31).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(31).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(32).setMinWidth(0);
            jTable1.getColumnModel().getColumn(32).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(32).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(33).setMinWidth(0);
            jTable1.getColumnModel().getColumn(33).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(33).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(34).setMinWidth(0);
            jTable1.getColumnModel().getColumn(34).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(34).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(35).setMinWidth(0);
            jTable1.getColumnModel().getColumn(35).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(35).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(36).setMinWidth(0);
            jTable1.getColumnModel().getColumn(36).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(36).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(37).setMinWidth(0);
            jTable1.getColumnModel().getColumn(37).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(37).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(38).setMinWidth(0);
            jTable1.getColumnModel().getColumn(38).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(38).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(39).setMinWidth(0);
            jTable1.getColumnModel().getColumn(39).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(39).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(40).setMinWidth(0);
            jTable1.getColumnModel().getColumn(40).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(40).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(41).setMinWidth(0);
            jTable1.getColumnModel().getColumn(41).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(41).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(42).setMinWidth(0);
            jTable1.getColumnModel().getColumn(42).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(42).setMaxWidth(0);
        }

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear ");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnProductDescription.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnProductDescription.setMnemonic('u');
        btnProductDescription.setText("Product Description");
        btnProductDescription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductDescriptionActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnPrint.setMnemonic('p');
        btnPrint.setText("Save & Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        jLabel18.setText("Bill Discount");

        txtBillDiscount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtBillDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillDiscountActionPerformed(evt);
            }
        });
        txtBillDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyTyped(evt);
            }
        });

        jLabel37.setText("Total Amount (Incl of Tax)");

        jPanel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(180, 180, 180)));
        jPanel12.setPreferredSize(new java.awt.Dimension(1, 1));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel35.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel35.setText("Quantity");

        txtUQC1Qty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUQC1QtyFocusGained(evt);
            }
        });
        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setPreferredSize(new java.awt.Dimension(97, 14));

        lblGodown.setText("Godown");

        comboGodown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGodownActionPerformed(evt);
            }
        });
        comboGodown.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGodownKeyPressed(evt);
            }
        });

        labelProduct.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelProduct.setText("Product / Service");
        labelProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                labelProductKeyReleased(evt);
            }
        });

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboProductPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        labelStock.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStock.setText("N/A");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("Available Stock ");

        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lblPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblPrice.setText("Price");

        txtUQC1Rate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC1Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateActionPerformed(evt);
            }
        });
        txtUQC1Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyTyped(evt);
            }
        });

        labelUQC1forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        labelUQC2forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        txtUQC2Rate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC2Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateActionPerformed(evt);
            }
        });
        txtUQC2Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyTyped(evt);
            }
        });

        lblPriceWithTax.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        lblPriceWithTax.setText("Price Inc of Tax");

        jPanel11.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(180, 180, 180), 1, true));
        jPanel11.setPreferredSize(new java.awt.Dimension(1, 102));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 106, Short.MAX_VALUE)
        );

        jLabel42.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel42.setText("Total Amount ");

        labelProductTotal.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelProductTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelProductTotal.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                labelProductTotalComponentAdded(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel43.setText("Total Amount Inc Tax");

        labelProductTotalWithTax.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        labelProductTotalWithTax.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        labelUQC1forPriceWithTax.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1forPriceWithTax.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPriceWithTax.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPriceWithTax.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC2forPriceWithTax.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2forPriceWithTax.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPriceWithTax.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPriceWithTax.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2RateWithTax.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC2RateWithTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateWithTaxActionPerformed(evt);
            }
        });
        txtUQC2RateWithTax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateWithTaxKeyTyped(evt);
            }
        });

        txtUQC1RateWithTax.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUQC1RateWithTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateWithTaxActionPerformed(evt);
            }
        });
        txtUQC1RateWithTax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateWithTaxKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelProductTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPriceWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelProductTotalWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtUQC2RateWithTax)
                            .addComponent(txtUQC1RateWithTax, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelUQC1forPriceWithTax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelUQC2forPriceWithTax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPrice)
                            .addComponent(lblPriceWithTax))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelUQC1forPrice)
                                .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtUQC1RateWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(labelUQC1forPriceWithTax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelUQC2forPrice)
                            .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUQC2RateWithTax)
                            .addComponent(labelUQC2forPriceWithTax, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelProductTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel43, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(labelProductTotalWithTax, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtUQC1Rate, txtUQC1RateWithTax, txtUQC2Rate, txtUQC2RateWithTax});

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelUQC1forPrice, labelUQC1forPriceWithTax, labelUQC2forPrice, labelUQC2forPriceWithTax});

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Discount %");

        txtDiscountPer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDiscountPerFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDiscountPerFocusLost(evt);
            }
        });
        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel27.setText("Disount");

        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        jLabel36.setText("Cess Amount");

        txtCessAmount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCessAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCessAmountActionPerformed(evt);
            }
        });
        txtCessAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCessAmountKeyPressed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("IGST ");

        txtIGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtIGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtIGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtIGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtCGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtCGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel10.setText("CGST ");

        txtCGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtCGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        txtSGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtSGSTper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel12.setText("SGST");

        txtSGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        txtSGSTamt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel47.setText(" %");
        jLabel47.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        jLabel48.setText("Amount");
        jLabel48.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(185, 185, 185)));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addComponent(txtIGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtIGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                    .addComponent(txtSGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47)
                            .addComponent(jLabel48))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCGSTamt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel12))
                .addGap(0, 0, 0))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel10, jLabel12, jLabel7, txtSGSTper});

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboProduct, 0, 252, Short.MAX_VALUE)
                            .addComponent(labelStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel36)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                    .addComponent(txtUQC1Qty, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtUQC2Qty)
                    .addComponent(txtCessAmount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblGodown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboGodown, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel36, lblGodown});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(labelStock)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(labelProduct)
                                                    .addComponent(comboProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(12, 12, 12)
                                                .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addComponent(labelUQC1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel36)
                                    .addComponent(txtCessAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lblGodown)
                                .addGap(0, 0, 0)
                                .addComponent(comboGodown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(3, 3, 3))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel27, jLabel35, jLabel36, jLabel5, lblGodown});

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 1267, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnProductDescription)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProductDescription)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Product Details", jPanel2);

        labelPartyName.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelPartyName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPartyName.setText("name");

        labelAddress.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelAddress.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAddress.setText("address");

        labelGSTIN.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelGSTIN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGSTIN.setText("gstin");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(labelPartyName)
                    .addComponent(labelAddress)
                    .addComponent(labelGSTIN))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelAddress, labelGSTIN, labelPartyName});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(labelPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(labelAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(labelGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel30.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel23.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        jLabel31.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel31.setText("SALES BUSINESS TO BUSINESS");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton1.setMnemonic('h');
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(6, 6, 6)
                .addComponent(jLabel30)
                .addGap(2, 2, 2)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel30)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel23)
                        .addComponent(labelCompanyName)
                        .addComponent(jLabel31)))
                .addGap(0, 0, 0))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 591, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        private void comboPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPartyActionPerformed

        }//GEN-LAST:event_comboPartyActionPerformed

    public void getPartyDetails() {
        String name = (String) comboParty.getSelectedItem();
        if (name == null) {
            name = "";
        }
        if (name.equals("")) {
            txtPartyName.setText("");
            txtAddr1.setText("");
            txtAddr2.setText("");
            txtAddr3.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            txtState.setText("");
            txtEmail.setText("");
            txtMobile.setText("");
            txtPhone.setText("");
            txtGSTIN.setText("");

            labelPartyName.setText("");
            labelAddress.setText("");
            labelGSTIN.setText("");
            shippingAddressUI.SetTextBox();

        }
        else {
            for (Ledger ledger : b2bPartyLedger) {
                if (ledger.getLedgerName().equals(name)) {
                    ledgerData = ledger;
                    LedgerId = ledger.getLedgerId();
                    txtPartyName.setText(ledger.getLedgerName());
                    shippingAddressUI.name = ledger.getLedgerName();
                    String address = ledger.getAddress();
                    String[] words = address.split("/~/");

                    if (words.length == 1) {
                        txtAddr1.setText(words[0]);
                        shippingAddressUI.addr1 = words[0];
                        txtAddr2.setText("");
                        shippingAddressUI.addr2 = "";
                        txtAddr3.setText("");
                        shippingAddressUI.addr3 = "";
                        labelAddress.setText(words[0]);
                    }
                    else if (words.length == 2) {
                        txtAddr1.setText(words[0]);
                        shippingAddressUI.addr1 = words[0];
                        txtAddr2.setText(words[1]);
                        shippingAddressUI.addr2 = words[1];
                        txtAddr3.setText("");
                        shippingAddressUI.addr3 = "";
                        labelAddress.setText(words[0] + "  " + words[1]);
                    }
                    else {
                        txtAddr1.setText(words[0]);
                        shippingAddressUI.addr1 = words[0];
                        txtAddr2.setText(words[1]);
                        shippingAddressUI.addr2 = words[1];
                        txtAddr3.setText(words[2]);
                        shippingAddressUI.addr3 = words[2];
                        labelAddress.setText(words[0] + "  " + words[1] + "  " + words[2]);
                    }
                    txtCity.setText(ledger.getCity());
                    shippingAddressUI.city = ledger.getCity();
                    txtDistrict.setText(ledger.getDistrict());
                    shippingAddressUI.district = ledger.getDistrict();
                    txtState.setText(ledger.getState());
                    shippingAddressUI.state = ledger.getState();
                    txtEmail.setText(ledger.getEmail());
                    txtMobile.setText(ledger.getMobile());
                    shippingAddressUI.mobile = ledger.getMobile();
                    txtPhone.setText(ledger.getPhone());
                    shippingAddressUI.phone = ledger.getPhone();
                    txtGSTIN.setText(ledger.getGSTIN());

                    shippingAddressUI.SetTextBox();
                    labelPartyName.setText(ledger.getLedgerName());
                    labelGSTIN.setText(ledger.getGSTIN());

                    String selectedState = txtState.getText();
                    isIGST = !(selectedState.equals(CompanyState));

                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    if (model.getRowCount() > 0) {
                        model.setRowCount(0);
                    }
                    DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
                    if (model2.getRowCount() > 0) {
                        model2.removeRow(0);
                    }
                    setTable();
                    checkMobile();
                    break;

                }
            };
        }
    }

    public void checkMobile() {
        Boolean isCorrect = true;
        if (txtMobile.getText().equals("") || txtMobile.getText().length() != 10) {
            isCorrect = false;
            txtMessage.setText("");
        }
        checkSMS.setSelected(isCorrect);
        checkSMS.setEnabled(isCorrect);
        txtMessage.setEnabled(isCorrect);
    }

        private void txtPartyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartyNameActionPerformed
            txtAddr1.requestFocus();
        }//GEN-LAST:event_txtPartyNameActionPerformed

        private void txtAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr1ActionPerformed
            txtAddr2.requestFocus();
        }//GEN-LAST:event_txtAddr1ActionPerformed

        private void txtAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr2ActionPerformed
            txtAddr3.requestFocus();
        }//GEN-LAST:event_txtAddr2ActionPerformed

        private void txtAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr3ActionPerformed
            txtCity.requestFocus();
        }//GEN-LAST:event_txtAddr3ActionPerformed
    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(14).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(17).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(19).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(21).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(25).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(27).setCellRenderer(new FloatValueTableCellRenderer());

        totalTable.getColumnModel().getColumn(0).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(1).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(4).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(5).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(6).setCellRenderer(new FloatValueTableCellRenderer());
        totalTable.getColumnModel().getColumn(7).setCellRenderer(new FloatValueTableCellRenderer());

    }

    public void TotalCalculate() {
        float totalIGST = 0, totalCGST = 0, totalSGST = 0, totalAmount = 0, totalProfit = 0, totalAmountInclTax = 0, totalCess = 0, totalProductAmount = 0;

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }

        for (int i = 0; i < model.getRowCount(); i++) {
            totalAmount = (float) model.getValueAt(i, 15) + totalAmount;
            totalIGST = (float) model.getValueAt(i, 17) + totalIGST;
            totalCGST = (float) model.getValueAt(i, 19) + totalCGST;
            totalSGST = (float) model.getValueAt(i, 21) + totalSGST;
            totalProfit = (float) model.getValueAt(i, 24) + totalProfit;
            totalProductAmount = (float) model.getValueAt(i, 25) + totalProductAmount;
            totalCess = (float) model.getValueAt(i, 26) + totalCess;
            totalAmountInclTax = (float) model.getValueAt(i, 27) + totalAmountInclTax;

        }
        amountWithoutDiscount = totalAmountInclTax;
        String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (strDiscount.equals("")) {
            strDiscount = "0";
        }
        float DiscountAmt = Float.parseFloat(strDiscount);
        amount = amountWithoutDiscount - DiscountAmt;
        labelBillAmount.setText(currency.format(amount));
        Object[] row = {totalAmount, totalIGST, totalCGST, totalSGST, totalProfit, totalProductAmount, totalCess, totalAmountInclTax};
        model2.addRow(row);
    }
        private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
            txtDistrict.requestFocus();
        }//GEN-LAST:event_txtCityActionPerformed

        private void txtPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhoneActionPerformed
            txtGSTIN.requestFocus();
        }//GEN-LAST:event_txtPhoneActionPerformed

        private void txtMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileActionPerformed
            txtPhone.requestFocus();
        }//GEN-LAST:event_txtMobileActionPerformed

        private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed

        }//GEN-LAST:event_txtGSTINActionPerformed
    public void setTable() {

        isProfitEnabled = SessionDataUtil.getCompanyPolicyData().isProfitEnabled();
        if (!isProfitEnabled) {
            totalTable.getColumnModel().getColumn(4).setHeaderValue("");
            totalTable.getColumnModel().getColumn(4).setResizable(false);
            totalTable.getColumnModel().getColumn(4).setPreferredWidth(0);
            totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
            totalTable.getColumnModel().getColumn(4).setMinWidth(0);
        }
        else {
            totalTable.getColumnModel().getColumn(4).setHeaderValue("Profit");
            totalTable.getColumnModel().getColumn(4).setResizable(true);
            totalTable.getColumnModel().getColumn(4).setPreferredWidth(100);
            totalTable.getColumnModel().getColumn(4).setMaxWidth(100);
            totalTable.getColumnModel().getColumn(4).setMinWidth(100);
        }
        jTable1.getColumnModel().getColumn(16).setMinWidth(100);
        jTable1.getColumnModel().getColumn(16).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(17).setMinWidth(100);
        jTable1.getColumnModel().getColumn(17).setMaxWidth(100);

        jTable1.getColumnModel().getColumn(18).setMinWidth(100);
        jTable1.getColumnModel().getColumn(18).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(19).setMinWidth(100);
        jTable1.getColumnModel().getColumn(19).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(20).setMinWidth(100);
        jTable1.getColumnModel().getColumn(20).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(21).setMinWidth(100);
        jTable1.getColumnModel().getColumn(21).setMaxWidth(100);

        totalTable.getColumnModel().getColumn(0).setMinWidth(0);
        totalTable.getColumnModel().getColumn(0).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(1).setMinWidth(0);
        totalTable.getColumnModel().getColumn(1).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(2).setMinWidth(0);
        totalTable.getColumnModel().getColumn(2).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(3).setMinWidth(0);
        totalTable.getColumnModel().getColumn(3).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(4).setMinWidth(0);
        totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(5).setMinWidth(0);
        totalTable.getColumnModel().getColumn(5).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(6).setMinWidth(0);
        totalTable.getColumnModel().getColumn(6).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(7).setMinWidth(0);
        totalTable.getColumnModel().getColumn(7).setMaxWidth(0);

        if (isIGST) {
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);

            totalTable.getColumnModel().getColumn(0).setMinWidth(100);
            totalTable.getColumnModel().getColumn(0).setMaxWidth(100);
            totalTable.getColumnModel().getColumn(1).setMinWidth(100);
            totalTable.getColumnModel().getColumn(1).setMaxWidth(100);
            //            totalTable.getColumnModel().getColumn(5).setMinWidth(200);
            //            totalTable.getColumnModel().getColumn(5).setMaxWidth(200);
            totalTable.getColumnModel().getColumn(6).setMinWidth(100);
            totalTable.getColumnModel().getColumn(6).setMaxWidth(100);
            totalTable.getColumnModel().getColumn(7).setMinWidth(200);
            totalTable.getColumnModel().getColumn(7).setMaxWidth(200);

        }
        else {
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);

            totalTable.getColumnModel().getColumn(0).setMinWidth(100);
            totalTable.getColumnModel().getColumn(0).setMaxWidth(100);
            totalTable.getColumnModel().getColumn(2).setMinWidth(100);
            totalTable.getColumnModel().getColumn(2).setMaxWidth(100);
            totalTable.getColumnModel().getColumn(3).setMinWidth(100);
            totalTable.getColumnModel().getColumn(3).setMaxWidth(100);
            //            totalTable.getColumnModel().getColumn(5).setMinWidth(200);
            //            totalTable.getColumnModel().getColumn(5).setMaxWidth(200);
            totalTable.getColumnModel().getColumn(6).setMinWidth(100);
            totalTable.getColumnModel().getColumn(6).setMaxWidth(100);

            totalTable.getColumnModel().getColumn(7).setMinWidth(200);
            totalTable.getColumnModel().getColumn(7).setMaxWidth(200);

        }
    }
        private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
            txtState.requestFocus();
        }//GEN-LAST:event_txtDistrictActionPerformed

        private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
            if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }//GEN-LAST:event_labelProductKeyReleased

        private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
            if (txtPartyName.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Please Add Company Name", "Alert", JOptionPane.WARNING_MESSAGE);
                jTabbedPane1.setSelectedIndex(0);
                comboParty.requestFocus();
            }
            else {
                if (isServiceProduct) {
                    addServiceInList();
                }
                else {
                    addProductInList();
                }

            }
        }//GEN-LAST:event_btnAddActionPerformed

    public void TaxValueCalculationForProduct(Boolean isDiscountPercentageBased) {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1RateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2RateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("") || !strUQC2rate.equals(""))) && ((!strUQC1qty.equals("") || !strUQC2qty.equals("")))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if ((UQC2qty / UQC2Value) > 0) {
                UQC1qty += UQC2qty / UQC2Value;
                UQC2qty = UQC2qty % UQC2Value;
                txtUQC1Qty.setText(UQC1qty + "");
                txtUQC2Qty.setText(UQC2qty + "");
            }

            float DiscountPer = 0, Discount = 0;
            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);
            if (isDiscountPercentageBased) {
                String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    if (!strDiscountPer.equals("")) {
                        DiscountPer = Float.parseFloat(strDiscountPer);
                    }
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                }
                else {
                    txtDiscount.setText("0");
                }
            }
            else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (TotalAmount > 0) {
                    if (!strDiscount.equals("")) {
                        Discount = Float.parseFloat(strDiscount);
                    }
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                }
                else {
                    txtDiscountPer.setText("0");
                }
            }
            String productName = comboProduct.getSelectedItem().toString();
            if ((productName != null) && (!(productName.equals("")))) {
                for (Product product : products) {
                    if (product.getName().equals(productName)) {
                        IGSTper = product.getIgstPercentage();
                        float TaxValue = TotalAmount * (IGSTper / 100);

                        if (isIGST) {
                            txtIGSTamt.setText(currency.format(TaxValue));
                            txtCGSTamt.setText("0.00");
                            txtSGSTamt.setText("0.00");
                        }
                        else {
                            txtIGSTamt.setText("0.00");
                            txtCGSTamt.setText(currency.format(TaxValue / 2));
                            txtSGSTamt.setText(currency.format(TaxValue / 2));
                        }
                        labelProductTotal.setText(currency.format(TotalAmount));
                        labelProductTotalWithTax.setText(currency.format(TotalAmount + TaxValue));
                        break;
                    }
                    else {
                        labelProductTotal.setText("0.00");
                        labelProductTotalWithTax.setText("0.00");
                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");

                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");
                    }
                }
            }
        }
    }

    public void TaxValueCalculationForService(Boolean isDiscountPercentageBased) {
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!strUQC1rate.equals("")) {
            float UQC1rate = Float.parseFloat(strUQC1rate);
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = UQC1rate;
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (isDiscountPercentageBased) {
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    DiscountPer = Float.parseFloat(strDiscountPer);
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                }
                else {
                    txtDiscount.setText("0.00");
                }
            }
            else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (TotalAmount > 0) {
                    if (!strDiscount.equals("")) {
                        Discount = Float.parseFloat(strDiscount);
                    }
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                }
                else {
                    txtDiscountPer.setText("0");
                }
            }
            String productName = comboProduct.getSelectedItem().toString();
            if ((productName != null) && (!(productName.equals("")))) {
                for (Product product : products) {
                    if (product.getName().equals(productName)) {
                        IGSTper = product.getIgstPercentage();
                        float TaxValue = TotalAmount * (IGSTper / 100);

                        if (isIGST) {
                            txtIGSTamt.setText(currency.format(TaxValue));
                            txtCGSTamt.setText("0.00");
                            txtSGSTamt.setText("0.00");
                        }
                        else {
                            txtIGSTamt.setText("0.00");
                            txtCGSTamt.setText(currency.format(TaxValue / 2));
                            txtSGSTamt.setText(currency.format(TaxValue / 2));
                        }
                        labelProductTotal.setText(currency.format(TotalAmount));
                        labelProductTotalWithTax.setText(currency.format(TotalAmount + TaxValue));
                        break;
                    }
                    else {
                        labelProductTotal.setText("0.00");
                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");
                        txtIGSTamt.setText("0.00");
                        txtCGSTamt.setText("0.00");
                        txtSGSTamt.setText("0.00");
                        labelProductTotalWithTax.setText("0.00");
                    }
                }
            }
        }
    }


        private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
//            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//                if (txtPartyName.getText().equals("")) {
//                    JOptionPane.showMessageDialog(null, "Please Add Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
//                } else {
//                    addProductList();
//                }
//                evt.consume();
//            }
        }//GEN-LAST:event_btnAddKeyPressed

        private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            int i = jTable1.getSelectedRow();
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            if (i >= 0) {
                int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
                if (p == JOptionPane.YES_OPTION) {
                    model.removeRow(i);
                    for (int y = i; y < model.getRowCount(); y++) {
                        model.setValueAt(y + 1, y, 0); //setValueAt(data,row,column)
                    }
                    clear();
                    TotalCalculate();
                }
            }
            else {
                JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }//GEN-LAST:event_btnDeleteActionPerformed
    public void clear() {
        productDetail = null;
        txtUQC1Qty.setText("");
        txtUQC1Qty.setEnabled(true);
        txtUQC2Qty.setText("");
        txtUQC2Qty.setEnabled(true);
        txtUQC1Rate.setText("");
        txtUQC1Rate.setEnabled(true);
        txtUQC2Rate.setText("");
        txtUQC2Rate.setEnabled(true);
        txtUQC1RateWithTax.setText("");
        txtUQC1RateWithTax.setEnabled(true);
        txtUQC2RateWithTax.setText("");
        txtUQC1RateWithTax.setEnabled(true);
        //        txtUQC1BaseRate.setText("");
        //        txtUQC2BaseRate.setText("");
        txtIGSTper.setText("");
        txtCGSTper.setText("");
        txtSGSTper.setText("");
        txtIGSTamt.setText("");
        txtCGSTamt.setText("");
        txtSGSTamt.setText("");

        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("per UQC1");
        labelUQC2.setText("per UQC2");
        labelUQC1forPrice.setText("per UQC1");
        labelUQC1forPriceWithTax.setText("per UQC1");
        labelUQC2forPrice.setText("per UQC2");
        labelUQC2forPrice.setEnabled(true);
        labelUQC2forPriceWithTax.setText("per UQC2");
        labelUQC2forPriceWithTax.setEnabled(true);
        labelProductTotal.setText("");
        labelProductTotalWithTax.setText("");
        txtCessAmount.setText("");
        comboProduct.setSelectedItem("");
        comboGodown.setSelectedIndex(0);
        jTable1.clearSelection();
        comboProduct.setEnabled(true);

        labelStock.setText("N/A");
        jTable1.clearSelection();
        btnAdd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        comboProduct.requestFocus();
        btnProductDescription.setEnabled(false);
        desc1 = "";
        desc2 = "";
        desc3 = "";
        desc4 = "";
        desc5 = "";
        desc6 = "";
        desc7 = "";
        desc8 = "";
        desc9 = "";
        desc10 = "";
        productDescriptionUI.prodDesc1 = "";
        productDescriptionUI.prodDesc2 = "";
        productDescriptionUI.prodDesc3 = "";
        productDescriptionUI.prodDesc4 = "";
        productDescriptionUI.prodDesc5 = "";
        productDescriptionUI.prodDesc6 = "";
        productDescriptionUI.prodDesc7 = "";
        productDescriptionUI.prodDesc8 = "";
        productDescriptionUI.prodDesc9 = "";
        productDescriptionUI.prodDesc10 = "";

        descCount = 0;
        productDescriptionUI.descCount = 0;
        setProduct();
    }

        private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            SaveAndPrint(false);
        }//GEN-LAST:event_btnSaveActionPerformed

    private void SaveAndPrint(Boolean isPrint) {
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateBill.getDate());
        }
        if (isNotExpired) {
            needPrint = isPrint;
            Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);
            if (!isInBetweenFinancialYear) {
                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                        + " to <b>" + strFinancialYearEnd + "</b></html>";
                JLabel label = new JLabel(msg);
                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            }
            else {
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                int rowCount = model.getRowCount();
                if (rowCount > 0) {
                    totalAmount = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 0)));
                    totalIGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 1)));
                    totalCGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 2)));
                    totalSGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 3)));
                    totalProfit = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 4)));
                    totalCess = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 6)));
                    NetAmount = totalAmount + totalIGST + totalCGST + totalSGST + totalCess;
                    float Discout = 0;
                    String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                    if (!strDiscount.equals("")) {
                        Discout = Float.parseFloat(txtBillDiscount.getText());
                    }
                    NetAmount = NetAmount - Discout;
                    roundOff = 0;
                    String value = "0.00";
                    try {
                        value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(amount), "Round Off", JOptionPane.INFORMATION_MESSAGE);
                        if (value.equals("")) {
                            value = "0.00";
                        }
                        roundOff = Float.parseFloat(value);
                        NetAmount = NetAmount + roundOff;

                        if (company.getCompanyPolicy().isAmountTendered()) {
                            paymentDialog.setVisible(true);
                            paymentDialog.setSize(500, 300);
                            paymentDialog.setLocationRelativeTo(null);
                            txtPayment.requestFocusInWindow();
                        }
                        else {
                            billFinished();
                        }

                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Please Enter Correct Value", "Alert", JOptionPane.WARNING_MESSAGE);
                    }

                }
                else {
                    JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }

    public Ledger findLedgerData() {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : b2bPartyLedger) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                break;
            }
        }
        return ledgerData;
    }

    public void OldPrintAndSaveAction() {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();

        Ledger ledgerData = findLedgerData();
        if (ledgerData != null) {

            oldSale.setCompany(SessionDataUtil.getSelectedCompany());
            oldSale.setPsId(oldSale.getPsId());
            oldSale.setPsType("SALES");
            oldSale.setBillNo(oldSale.getBillNo());
            oldSale.setPsDate(dateBill.getDate());
            oldSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));

            oldSale.setIsIGST(isIGST);
            oldSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            oldSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            oldSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            oldSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
            oldSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
            oldSale.setCancelled(false);
            oldSale.setLedger(ledgerData);
            oldSale.setPaymentType("Cash");//Payment Type
            oldSale.setRemarks("");
            oldSale.setModeOfDelivery("");
            //                oldSale.setDiscountPercent(0);
            //                oldSale.setDiscountAmount(convertDecimal.Currency("0"));
            oldSale.setVariationAmount(convertDecimal.Currency("0"));
            //                oldSale.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));
            oldSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
            oldSale.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
            oldSale.setSmsMessage(txtMessage.getText());
            oldSale.setPurchaseSaleJournal(oldSale.getPurchaseSaleJournal());

            oldSale.setShippingAddressName(shippingAddressUI.name);
            oldSale.setShippingAddressOne(shippingAddressUI.addr1);
            oldSale.setShippingAddressTwo(shippingAddressUI.addr2);
            oldSale.setShippingAddressThree(shippingAddressUI.addr3);
            oldSale.setShippingAddressCity(shippingAddressUI.city);
            oldSale.setShippingAddressDistrict(shippingAddressUI.district);
            oldSale.setShippingAddressState(shippingAddressUI.state);
            oldSale.setShippingAddressMobile(shippingAddressUI.mobile);
            oldSale.setShippingAddressPhone(shippingAddressUI.phone);
            oldSale.setTotalAmountPaid(new BigDecimal(0));

            List<PurchaseSaleLineItem> psLineItems = new ArrayList<>();
            List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<>();
            Long count = 1L;

            for (int i = 0; i < rowCount; i++) {
                PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                Product product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(product);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue(Float.parseFloat(model.getValueAt(i, 6).toString()));

                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue(Float.parseFloat(model.getValueAt(i, 10).toString()));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
                psLineItem.setCessAmount(convertDecimal.Currency(model.getValueAt(i, 26).toString()));

                psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));
                psLineItem.setDescriptionOne(model.getValueAt(i, 29).toString());
                psLineItem.setDescriptionTwo(model.getValueAt(i, 30).toString());
                psLineItem.setDescriptionThree(model.getValueAt(i, 31).toString());
                psLineItem.setDescriptionFour(model.getValueAt(i, 32).toString());
                psLineItem.setDescriptionFive(model.getValueAt(i, 33).toString());
                psLineItem.setDescriptionSix(model.getValueAt(i, 34).toString());
                psLineItem.setDescriptionSeven(model.getValueAt(i, 35).toString());
                psLineItem.setDescriptionEight(model.getValueAt(i, 36).toString());
                psLineItem.setDescriptionNine(model.getValueAt(i, 37).toString());
                psLineItem.setDescriptionTen(model.getValueAt(i, 38).toString());
                psLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 39).toString()));
                Boolean isItemService = Boolean.parseBoolean(model.getValueAt(i, 40).toString());
                psLineItem.setService(isItemService);
                psLineItem.setUqcOneRateWithTax(convertDecimal.Currency(model.getValueAt(i, 41).toString()));
                psLineItem.setUqcTwoRateWithTax(convertDecimal.Currency(model.getValueAt(i, 42).toString()));

                GoDown selGodown = null;
                if (isInventoryEnabled && (!isItemService)) {
                    for (GoDown godown : godowns) {
                        if (godown.getGoDownName().equals(model.getValueAt(i, 28).toString())) {
                            selGodown = godown;
                            break;
                        }
                    }
                }
                psLineItem.setGodown(selGodown);
                psLineItems.add(psLineItem);

                Boolean isNewSalesTax = true;
                for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                    if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                        float amount = tax.getTaxableValue().floatValue();
                        amount = (float) model.getValueAt(i, 15) + amount;
                        float Igst = tax.getIgstValue().floatValue();
                        Igst = (float) model.getValueAt(i, 17) + Igst;
                        float Cgst = tax.getCgstValue().floatValue();
                        Cgst = (float) model.getValueAt(i, 19) + Cgst;
                        float Sgst = tax.getSgstValue().floatValue();
                        Sgst = (float) model.getValueAt(i, 21) + Sgst;
                        float cessAmt = tax.getCessValue().floatValue();
                        cessAmt = (float) model.getValueAt(i, 26) + cessAmt;
                        float taxAmount = Igst + Cgst + Sgst + cessAmt;

                        tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                        tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                        tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                        tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                        tax.setCessValue(convertDecimal.Currency(currency.format(cessAmt)));
                        tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));

                        isNewSalesTax = false;
                    }
                }
                if (isNewSalesTax) {
                    PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                    float amount = (float) model.getValueAt(i, 15);
                    float Igst = (float) model.getValueAt(i, 17);
                    float Cgst = (float) model.getValueAt(i, 19);
                    float Sgst = (float) model.getValueAt(i, 21);
                    float cessAmt = (float) model.getValueAt(i, 26);
                    float taxAmount = Igst + Cgst + Sgst + cessAmt;

                    //                    newSaleTaxSummary.setPsTaxId(count);
                    //                    newSaleTaxSummary.setPurchaseSale(newPurchaseSale);
                    newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                    newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 16));
                    newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 18));
                    newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 20));
                    newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                    newSaleTaxSummary.setCessValue(convertDecimal.Currency(currency.format(cessAmt)));
                    purchaseSalesTax.add(newSaleTaxSummary);

                    count++;
                }
            }
            oldSale.setPsLineItems(psLineItems);
            oldSale.setPsTaxSummaries(purchaseSalesTax);
            if (needPrint) {
                try {
                    new TaxInvoiceNew().printPdf(needPrint, false, oldSale, psLineItems, purchaseSalesTax, new GetPDFHeaders().getPdfHeaders());
                } catch (IOException ex) {
                    Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Boolean isSuccess = new PurchaseSaleLogic().updateAPurchaseOrSale("SALES", LedgerId, oldSale, psLineItems, purchaseSalesTax);

            if (isSuccess) {
                JOptionPane.showMessageDialog(null, "Bill is Successfully updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                new SalesDateWiseReportUI().setVisible(true);
                dispose();
                clear();
                paymentDialog.dispose();
                clearAll();
            }
            else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Please Choose Party in the Party List ", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }

    public void NewPrintAndSaveAction() {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        PurchaseSale newPurchaseSale = new PurchaseSale();

        newPurchaseSale.setCompany(SessionDataUtil.getSelectedCompany());
        newPurchaseSale.setPsType("SALES");
        newPurchaseSale.setPsDate(dateBill.getDate());
        newPurchaseSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
        newPurchaseSale.setIsIGST(isIGST);
        newPurchaseSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
        newPurchaseSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
        newPurchaseSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
        newPurchaseSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
        newPurchaseSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
        newPurchaseSale.setCancelled(false);
        newPurchaseSale.setLedger(ledgerData);
        newPurchaseSale.setPaymentType("Cash");//Payment Type
        newPurchaseSale.setRemarks("");
        newPurchaseSale.setModeOfDelivery("");
        float Discount = 0;
        String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (!strDiscount.equals("")) {
            Discount = Float.parseFloat(strDiscount);
        }
        //            newPurchaseSale.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));
        newPurchaseSale.setVariationAmount(convertDecimal.Currency("0"));
        newPurchaseSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
        newPurchaseSale.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
        newPurchaseSale.setSmsMessage(txtMessage.getText());
        newPurchaseSale.setB2bInvoice(true);

        newPurchaseSale.setShippingAddressName(shippingAddressUI.name);
        newPurchaseSale.setShippingAddressOne(shippingAddressUI.addr1);
        newPurchaseSale.setShippingAddressTwo(shippingAddressUI.addr2);
        newPurchaseSale.setShippingAddressThree(shippingAddressUI.addr3);
        newPurchaseSale.setShippingAddressCity(shippingAddressUI.city);
        newPurchaseSale.setShippingAddressDistrict(shippingAddressUI.district);
        newPurchaseSale.setShippingAddressState(shippingAddressUI.state);
        newPurchaseSale.setShippingAddressMobile(shippingAddressUI.mobile);
        newPurchaseSale.setShippingAddressPhone(shippingAddressUI.phone);

        newPurchaseSale.setNoParty(false);
        newPurchaseSale.setNoPartyName("");
        newPurchaseSale.setNoPartyLineOne("");
        newPurchaseSale.setNoPartyCity("");
        newPurchaseSale.setNoPartyDistrict("");
        newPurchaseSale.setNoPartyState("");
        newPurchaseSale.setNoPartyGSTIN("");
        newPurchaseSale.setTotalAmountPaid(new BigDecimal(0));

        //          newPurchaseSale.setPsLineItems(psLineItems);
        //          newPurchaseSale.setPsTaxSummaries(psTaxSummaries);
        List<PurchaseSaleLineItem> psLineItems = new ArrayList<PurchaseSaleLineItem>();
        List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<PurchaseSaleTaxSummary>();
        Long count = 1L;
        for (int i = 0; i < rowCount; i++) {
            PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
            psLineItem.setLineNumber((int) model.getValueAt(i, 0));
            psLineItem.setProductName((String) model.getValueAt(i, 1));
            psLineItem.setHsnSac((String) model.getValueAt(i, 2));
            Product product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

            psLineItem.setProduct(product);

            psLineItem.setUqcOne((String) model.getValueAt(i, 3));
            psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
            psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
            psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
            psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
            psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
            psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
            psLineItem.setUqcTwoValue(Float.parseFloat(model.getValueAt(i, 10).toString()));

            psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
            psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
            psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
            psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
            psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
            psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
            psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
            psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
            psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
            psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
            psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
            psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
            psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));
            psLineItem.setCessAmount(convertDecimal.Currency(model.getValueAt(i, 26).toString()));
            psLineItem.setDescriptionOne("" + model.getValueAt(i, 29).toString());
            psLineItem.setDescriptionTwo("" + model.getValueAt(i, 30).toString());
            psLineItem.setDescriptionThree("" + model.getValueAt(i, 31).toString());
            psLineItem.setDescriptionFour("" + model.getValueAt(i, 32).toString());
            psLineItem.setDescriptionFive("" + model.getValueAt(i, 33).toString());
            psLineItem.setDescriptionSix("" + model.getValueAt(i, 34).toString());
            psLineItem.setDescriptionSeven("" + model.getValueAt(i, 35).toString());
            psLineItem.setDescriptionEight("" + model.getValueAt(i, 36).toString());
            psLineItem.setDescriptionNine("" + model.getValueAt(i, 37).toString());
            psLineItem.setDescriptionTen("" + model.getValueAt(i, 38).toString());
            psLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 39).toString()));
            Boolean isItemService = Boolean.parseBoolean(model.getValueAt(i, 40).toString());
            psLineItem.setService(isItemService);
            psLineItem.setUqcOneRateWithTax(convertDecimal.Currency(model.getValueAt(i, 41).toString()));
            psLineItem.setUqcTwoRateWithTax(convertDecimal.Currency(model.getValueAt(i, 42).toString()));

            GoDown selGodown = null;
            if (isInventoryEnabled && (!isItemService)) {
                for (GoDown godown : godowns) {
                    if (godown.getGoDownName().equals(model.getValueAt(i, 28).toString())) {
                        selGodown = godown;
                        break;
                    }
                }
            }
            psLineItem.setGodown(selGodown);
            psLineItems.add(psLineItem);

            Boolean isNewSalesTax = true;
            for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                    float amount = tax.getTaxableValue().floatValue();
                    amount = (float) model.getValueAt(i, 15) + amount;
                    float Igst = tax.getIgstValue().floatValue();
                    Igst = (float) model.getValueAt(i, 17) + Igst;
                    float Cgst = tax.getCgstValue().floatValue();
                    Cgst = (float) model.getValueAt(i, 19) + Cgst;
                    float Sgst = tax.getSgstValue().floatValue();
                    Sgst = (float) model.getValueAt(i, 21) + Sgst;
                    float cessAmt = tax.getCessValue().floatValue();
                    float taxAmount = Igst + Cgst + Sgst + cessAmt;

                    tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                    tax.setCessValue(convertDecimal.Currency(currency.format(cessAmt)));

                    isNewSalesTax = false;
                }
            }
            if (isNewSalesTax) {
                PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                float amount = (float) model.getValueAt(i, 15);
                float Igst = (float) model.getValueAt(i, 17);
                float Cgst = (float) model.getValueAt(i, 19);
                float Sgst = (float) model.getValueAt(i, 21);
                float cessAmt = (float) model.getValueAt(i, 26);
                float taxAmount = Igst + Cgst + Sgst + cessAmt;

                //                    newSaleTaxSummary.setPsTaxId(count);
                //                    newSaleTaxSummary.setPurchaseSale(newPurchaseSale);
                newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 16));
                newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 18));
                newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 20));
                newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                newSaleTaxSummary.setCessValue(convertDecimal.Currency(currency.format(cessAmt)));
                purchaseSalesTax.add(newSaleTaxSummary);

                count++;
            }
        }
//            PaymentAmountUI newPaymentAmountUI = new PaymentAmountUI();
//            newPaymentAmountUI.setPaymentAmout(currency.format(NetAmount));
//            final JDialog dialog = new JDialog(newPaymentAmountUI,
//                    "Click a button",
//                    true);
//            
//            dialog.setDefaultCloseOperation(
//                    JDialog.DO_NOTHING_ON_CLOSE);

        EventStatus result = new PurchaseSaleLogic().createAPS("SALES", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);
        if (result.isCreateDone()) {
            if (needPrint) {
                new TaxInvoiceNew().pdfByBillId(true, false, result.getBillID(), new GetPDFHeaders().getPdfHeaders(), false);
                if (checkSMS.isSelected()) {
                    sendSms(result.getBillID());
                }
            }
            paymentDialog.dispose();
            JOptionPane.showMessageDialog(null, "New Bill is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
            clearAll();
        }
        else {
            JOptionPane.showMessageDialog(null, "Error Occured " + result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void sendSms(String Id) {

        PurchaseSale newPS = new PurchaseSaleLogic().getPSDetail(Id);
        String message = newPS.getSmsMessage().replaceAll("####", newPS.getBillNo());
        String phone = newPS.getLedger().getMobile();
        if (message == null) {
            JOptionPane.showMessageDialog(null, "Message not found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        else if (phone.equals("") || phone == null) {
            JOptionPane.showMessageDialog(null, "Mobile Number not found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        else {
            try {
                message = message.replaceAll(" ", "%20");

                //               String limitMessage = message.substring(0, 160);
                String url = "http://api.cutesms.in/sms.aspx?a=submit&email=srinivas@marvellabs.in&pw=gQa96&sid=SAKTHI&msg=" + message + "&to=" + phone;

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            } catch (MalformedURLException ex) {
                Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SalesB2BUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void clearAll() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        setParty();
        txtPartyName.setText("");
        txtAddr1.setText("");
        txtAddr2.setText("");
        txtAddr3.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        txtState.setText("");
        txtEmail.setText("");
        txtMobile.setText("");
        txtPhone.setText("");
        txtGSTIN.setText("");
        txtMessage.setText("");
        messageCount();
        checkSMS.setSelected(true);
        comboPricingPolicy.setSelectedItem("Default");
        txtBillDiscount.setText("");
        labelPartyName.setText("");
        labelAddress.setText("");
        labelGSTIN.setText("");
        dateBill.setDate(new Date());
        clear();
        shippingAddressUI.Reset();
        labelBillAmount.setText("");

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }
        jTabbedPane1.setSelectedIndex(0);

        labelPaymentBillAmount.setText("0.00");
        txtPayment.setText("");
        labelBalance.setText("0.00");
    }

    private int findInsertRowofProduct() {
        int rowPosition = 0;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if ((Boolean) model.getValueAt(i, 40)) {
                rowPosition = i;
                break;
            }
        }
        return rowPosition;
    }

    private void addServiceInList() {
        boolean productUpdate = false;
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String UQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String productName = comboProduct.getSelectedItem().toString().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscountPercent = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

        if (productName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
        else if (strUQC1rate.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            int count = jTable1.getRowCount() + 1;
            float UQC1rate = 0, cessAmount = 0;
            float lineItemUQC1rate = 0, lineItemfCessAmt = 0;
            float lineItemAmt = 0, lineItemDiscount = 0, lineItemIGST = 0, lineItemCGST = 0, lineItemSGST = 0, lineItemProfit = 0, lineItemTotalAmount = 0;

            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strCessAmount.equals("")) {
                cessAmount = Float.parseFloat(strCessAmount);
            }

            boolean bMsg = true;
            if ((UQC1rate == 0)) {
                JOptionPane.showMessageDialog(null, "Please Enter The Price");
                bMsg = false;
            }

            int rowId = isProductHereInTable(productName, -1);
            if (rowId >= 0) {
                productUpdate = true;
//                JOptionPane.showConfirmDialog(null,strProduct+" is Already in the list Do you want to add More", "CONFORM", JOptionPane.YES_NO_OPTION);

                if ((model.getValueAt(rowId, 5) != null) && !(model.getValueAt(rowId, 5).equals(""))) {
                    lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 5).toString());
                }

                if ((model.getValueAt(rowId, 14) != null) && !(model.getValueAt(rowId, 14).equals(""))) {
                    lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 14).toString());
                }
                if ((model.getValueAt(rowId, 15) != null) && !(model.getValueAt(rowId, 15).equals(""))) {
                    lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 15).toString());
                }
                if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                    lineItemIGST = Float.parseFloat(model.getValueAt(rowId, 17).toString());
                }
                if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                    lineItemCGST = Float.parseFloat(model.getValueAt(rowId, 19).toString());
                }
                if ((model.getValueAt(rowId, 21) != null) && !(model.getValueAt(rowId, 21).equals(""))) {
                    lineItemSGST = Float.parseFloat(model.getValueAt(rowId, 21).toString());
                }
                if ((model.getValueAt(rowId, 24) != null) && !(model.getValueAt(rowId, 24).equals(""))) {
                    lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 24).toString());
                }
                if ((model.getValueAt(rowId, 27) != null) && !(model.getValueAt(rowId, 27).equals(""))) {
                    lineItemTotalAmount = Float.parseFloat(model.getValueAt(rowId, 27).toString());
                }
                if ((model.getValueAt(rowId, 26) != null) && !(model.getValueAt(rowId, 26).equals(""))) {
                    lineItemfCessAmt = Float.parseFloat(model.getValueAt(rowId, 26).toString());
                }
                descAdded = true;
                UQC1rate = UQC1rate + lineItemUQC1rate;
            }

            Product selProduct = null;
            for (Product selectedProduct : products) {
                if (selectedProduct.getName().equalsIgnoreCase(productName)) {
                    selProduct = selectedProduct;
                    break;
                }
            }
            float Rate = Float.parseFloat(txtUQC1RateWithTax.getText()),
                    IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                    CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                    SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                    Amount = Float.parseFloat(labelProductTotal.getText());
            if (!strCessAmount.equals("")) {
                cessAmount = Float.parseFloat(txtCessAmount.getText());
            }
            float Discountper = 0, Discount = 0;
            if (!txtDiscountPer.getText().equals("")) {
                Discountper = Float.parseFloat(strDiscountPercent);
            }
            if (!txtDiscount.getText().equals("")) {
                Discount = Float.parseFloat(strDiscount);
            }

            float Profit = Rate;
            Profit = Profit - Discount;
            float discount = Discount + lineItemDiscount;
            Amount = Amount + lineItemAmt;
            if (discount != 0) {
                Discountper = (discount / (Amount + discount)) * 100;
                Discount = discount;
            }
            IGST_amt = IGST_amt + lineItemIGST;
            SGST_amt = SGST_amt + lineItemSGST;
            CGST_amt = CGST_amt + lineItemCGST;
            Profit = Profit + lineItemProfit;
            cessAmount = cessAmount + lineItemfCessAmt;
            float ProductTotalAmount = Amount + IGST_amt + CGST_amt + SGST_amt;
            float TotalAmoutInclTax = ProductTotalAmount + cessAmount;
            if (descAdded) {
                desc1 = productDescriptionUI.prodDesc1;
                desc2 = productDescriptionUI.prodDesc2;
                desc3 = productDescriptionUI.prodDesc3;
                desc4 = productDescriptionUI.prodDesc4;
                desc5 = productDescriptionUI.prodDesc5;
                desc6 = productDescriptionUI.prodDesc6;
                desc7 = productDescriptionUI.prodDesc7;
                desc8 = productDescriptionUI.prodDesc8;
                desc9 = productDescriptionUI.prodDesc9;
                desc10 = productDescriptionUI.prodDesc10;
                descCount = productDescriptionUI.descCount;
                productDescriptionUI.descAdded = false;
            }
            else {
                desc1 = selProduct.getDescriptionOne();
                desc2 = selProduct.getDescriptionTwo();
                desc3 = selProduct.getDescriptionThree();
                desc4 = selProduct.getDescriptionFour();
                desc5 = selProduct.getDescriptionFive();
                desc6 = selProduct.getDescriptionSix();
                desc7 = selProduct.getDescriptionSeven();
                desc8 = selProduct.getDescriptionEight();
                desc9 = selProduct.getDescriptionNine();
                desc10 = selProduct.getDescriptionTen();
                descCount = selProduct.getDescriptioncount();
            }

            if (productUpdate) {
                int serialNumber = (int) model.getValueAt(rowId, 0);
                Object[] row = {serialNumber, productName, HSNCode, "", 0, UQC1rate, UQC1rate,
                    "", 0, 0, 0, "", 0, Float.parseFloat(currency.format(Discountper)), Float.parseFloat(currency.format(Discount)), Amount, IGSTper, IGST_amt, CGSTper,
                    CGST_amt, SGSTper, SGST_amt, 0, 0, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax,
                    "", desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, true, UQC1rateWithTax, 0};
                model.removeRow(rowId);
                model.insertRow(rowId, row);
            }
            else {
                Object[] row = {count, productName, HSNCode, "", 0, UQC1rate, UQC1rate,
                    "", 0, 0, 0, "", 0, Float.parseFloat(currency.format(Discountper)), Float.parseFloat(currency.format(Discount)), Amount, IGSTper, IGST_amt, CGSTper,
                    CGST_amt, SGSTper, SGST_amt, 0, 0, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax,
                    "", desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, true, UQC1rateWithTax, 0};

                model.addRow(row);
            }
            clear();
            TotalCalculate();
            jTableRender();

        }
    }

    public void addProductInList() {
        boolean isCorrect = true;
        boolean productUpdate = false;
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscountPercent = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String productName = comboProduct.getSelectedItem().toString().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

        if (productName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            isCorrect = false;
        }
        else if (strUQC1qty.equals("") && strUQC2qty.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
            isCorrect = false;
        }
        else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1RateWithTax.requestFocus();
            isCorrect = false;
//        } else if (isProductAlreadyHere) {
//            JOptionPane.showMessageDialog(null, product + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
//            comboProduct.requestFocus();
//            isCorrect = false;
        }
        else {
            int count = jTable1.getRowCount() + 1;
            int rowSerialNumber = 0;
            int wholeQuantity = 0;
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, UQC1rateWithTax = 0, UQC2rateWithTax = 0, UQC1Baserate = 0, UQC2Baserate = 0, cessAmount = 0;
            float lineItemUQC1rate = 0, lineItemUQC2rate = 0, lineItemfCessAmt = 0;
            float lineItemAmt = 0, lineItemDiscount = 0, lineItemIGST = 0, lineItemCGST = 0, lineItemSGST = 0, lineItemProfit = 0, lineItemTotalAmount = 0;
            int lineItemUQC1qty = 0, lineItemUQC2qty = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if (!strUQC1rateWithTax.equals("")) {
                UQC1rateWithTax = Float.parseFloat(strUQC1rateWithTax);
            }
            if (!strUQC2rateWithTax.equals("")) {
                UQC2rateWithTax = Float.parseFloat(strUQC2rateWithTax);
            }
            boolean bMsg = true;
            if ((UQC1rate == 0) && (UQC2rate == 0)) {
                JOptionPane.showMessageDialog(null, "Please Enter The Price");
                isCorrect = false;
                bMsg = false;

            }

            //            if (!strUQC1Baserate.equals("")) {
            //                UQC1Baserate = Float.parseFloat(strUQC1Baserate);
            //            }
            //            if (!strUQC2Baserate.equals("")) {
            //                UQC2Baserate = Float.parseFloat(strUQC2Baserate);
            //            }
            if (!strCessAmount.equals("")) {
                cessAmount = Float.parseFloat(strCessAmount);
            }
            else {
                txtCessAmount.setText("0");
            }
            int rowId = isProductHereInTable(productName, -1);
            if (rowId >= 0) {
                productUpdate = true;
                rowSerialNumber = (int) model.getValueAt(rowId, 0);
//                JOptionPane.showConfirmDialog(null,strProduct+" is Already in the list Do you want to add More", "CONFORM", JOptionPane.YES_NO_OPTION);
                if ((model.getValueAt(rowId, 4) != null) && !(model.getValueAt(rowId, 4).equals(""))) {
                    lineItemUQC1qty = Integer.parseInt(model.getValueAt(rowId, 4).toString());
                }
                if ((model.getValueAt(rowId, 5) != null) && !(model.getValueAt(rowId, 5).equals(""))) {
                    lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 5).toString());
                }
                if ((model.getValueAt(rowId, 8) != null) && !(model.getValueAt(rowId, 8).equals(""))) {
                    lineItemUQC2qty = Integer.parseInt(model.getValueAt(rowId, 8).toString());
                }
                if ((model.getValueAt(rowId, 9) != null) && !(model.getValueAt(rowId, 9).equals(""))) {
                    lineItemUQC2rate = Float.parseFloat(model.getValueAt(rowId, 9).toString());
                }
                if ((model.getValueAt(rowId, 14) != null) && !(model.getValueAt(rowId, 14).equals(""))) {
                    lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 14).toString());
                }
                if ((model.getValueAt(rowId, 15) != null) && !(model.getValueAt(rowId, 15).equals(""))) {
                    lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 15).toString());
                }
                if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                    lineItemIGST = Float.parseFloat(model.getValueAt(rowId, 17).toString());
                }
                if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                    lineItemCGST = Float.parseFloat(model.getValueAt(rowId, 19).toString());
                }
                if ((model.getValueAt(rowId, 21) != null) && !(model.getValueAt(rowId, 21).equals(""))) {
                    lineItemSGST = Float.parseFloat(model.getValueAt(rowId, 21).toString());
                }
                if ((model.getValueAt(rowId, 24) != null) && !(model.getValueAt(rowId, 24).equals(""))) {
                    lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 24).toString());
                }
                if ((model.getValueAt(rowId, 27) != null) && !(model.getValueAt(rowId, 27).equals(""))) {
                    lineItemTotalAmount = Float.parseFloat(model.getValueAt(rowId, 27).toString());
                }

                if ((model.getValueAt(rowId, 26) != null) && !(model.getValueAt(rowId, 26).equals(""))) {
                    lineItemfCessAmt = Float.parseFloat(model.getValueAt(rowId, 26).toString());
                }
                descAdded = true;
                UQC1qty = UQC1qty + lineItemUQC1qty;
                //UQC1rate = UQC1rate + lineItemUQC1rate;
                UQC2qty = UQC2qty + lineItemUQC2qty;
                //UQC2rate = UQC2rate + lineItemUQC2rate;
            }

            String strQuantity = "";
            if (UQC1qty == 0) {
                strQuantity = UQC2qty + " " + labelUQC2.getText();
            }
            else if (UQC2qty == 0) {
                strQuantity = UQC1qty + " " + labelUQC1.getText();
            }
            else {
                strQuantity = UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText();
            }
            Product selProduct = null;
            for (Product selectedProduct : products) {
                if (selectedProduct.getName().equalsIgnoreCase(productName)) {
                    selProduct = selectedProduct;
                    break;
                }
            }
            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
            float IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                    CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                    SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                    Amount = Float.parseFloat(labelProductTotal.getText());
            cessAmount = Float.parseFloat(txtCessAmount.getText());
            float Discountper = 0, Discount = 0;
            if (!strDiscountPercent.equals("")) {
                Discountper = Float.parseFloat(txtDiscountPer.getText());
            }
            if (!strDiscount.equals("")) {
                Discount = Float.parseFloat(txtDiscount.getText());
            }
            String strGodown = "";
            if (isInventoryEnabled) {
                strGodown = comboGodown.getSelectedItem().toString();
            }
            float Profit = 0;
            if (isProfitEnabled) {
                float BaseRateOfProduct = 0;
                if (selProduct.getBaseProductRate() != null) {
                    BaseRateOfProduct = selProduct.getBaseProductRate().floatValue();
                }
                float basePrice = 0;
                if (selProduct.isBasePriceInclusiveOfGST() && (BaseRateOfProduct > 0)) {
                    basePrice = (BaseRateOfProduct / (100 + IGSTper)) * 100;
                }
                else {
                    basePrice = BaseRateOfProduct;
                }
                UQC1Baserate = basePrice * selProduct.getUQC2Value();
                UQC2Baserate = basePrice;
            }
            Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
            float discount = Discount + lineItemDiscount;
            Profit = Profit - discount;
            Amount = Amount + lineItemAmt;
            if (discount != 0) {
                Discountper = (discount / (Amount + discount)) * 100;
                Discount = discount;
            }

            IGST_amt = IGST_amt + lineItemIGST;
            SGST_amt = SGST_amt + lineItemSGST;
            CGST_amt = CGST_amt + lineItemCGST;
            Profit = Profit + lineItemProfit;
            cessAmount = cessAmount + lineItemfCessAmt;

            float ProductTotalAmount = Amount + IGST_amt + CGST_amt + SGST_amt;
            float TotalAmoutInclTax = ProductTotalAmount + cessAmount;
            if (descAdded) {
                desc1 = productDescriptionUI.prodDesc1;
                desc2 = productDescriptionUI.prodDesc2;
                desc3 = productDescriptionUI.prodDesc3;
                desc4 = productDescriptionUI.prodDesc4;
                desc5 = productDescriptionUI.prodDesc5;
                desc6 = productDescriptionUI.prodDesc6;
                desc7 = productDescriptionUI.prodDesc7;
                desc8 = productDescriptionUI.prodDesc8;
                desc9 = productDescriptionUI.prodDesc9;
                desc10 = productDescriptionUI.prodDesc10;
                descCount = productDescriptionUI.descCount;
                productDescriptionUI.descAdded = false;
            }
            else {
                desc1 = selProduct.getDescriptionOne();
                desc2 = selProduct.getDescriptionTwo();
                desc3 = selProduct.getDescriptionThree();
                desc4 = selProduct.getDescriptionFour();
                desc5 = selProduct.getDescriptionFive();
                desc6 = selProduct.getDescriptionSix();
                desc7 = selProduct.getDescriptionSeven();
                desc8 = selProduct.getDescriptionEight();
                desc9 = selProduct.getDescriptionNine();
                desc10 = selProduct.getDescriptionTen();
                descCount = selProduct.getDescriptioncount();
            }

            bCheckStock = false;
            isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
            if (isInventoryEnabled && !isUnderStockPossible) {
                if (wholeQuantity <= productStock) {
                    bCheckStock = true;
                }
            }
            else {
                bCheckStock = true;
            }

            if (bCheckStock && isCorrect) {
                if (productUpdate) {
                    Object[] row = {rowSerialNumber, productName, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                        labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                        wholeQuantity, Float.parseFloat(currency.format(Discountper)), Float.parseFloat(currency.format(Discount)), Amount, IGSTper, IGST_amt, CGSTper, CGST_amt, SGSTper, SGST_amt,
                        UQC1Baserate, UQC2Baserate, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax, strGodown, desc1, desc2, desc3, desc4, desc5, desc6,
                        desc7, desc8, desc9, desc10, descCount, false, UQC1rateWithTax, UQC2rateWithTax};
                    model.removeRow(rowId);
                    model.insertRow(rowId, row);
                }
                else {
                    int position = findInsertRowofProduct();
                    Object[] row = {count, productName, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                        labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                        wholeQuantity, Float.parseFloat(currency.format(Discountper)), Float.parseFloat(currency.format(Discount)), Amount, IGSTper, IGST_amt, CGSTper, CGST_amt, SGSTper, SGST_amt,
                        UQC1Baserate, UQC2Baserate, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax, strGodown, desc1, desc2, desc3, desc4, desc5, desc6,
                        desc7, desc8, desc9, desc10, descCount, false, UQC1rateWithTax, UQC2rateWithTax};
                    if (position == 0) {
                        model.addRow(row);
                    }
                    else {
                        model.insertRow(position, row);
                    }
                }
                for (int sno = 1; sno <= model.getRowCount(); sno++) {
                    model.setValueAt(sno, sno - 1, 0);
                }
                clear();
                TotalCalculate();
                jTableRender();
                comboProduct.requestFocus();
            }
            else if (bMsg) {
                JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
            else {
                txtUQC1RateWithTax.requestFocus();
            }
        }
    }

    public void updateProductsItems() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        //        String strUQC1Baserate = txtUQC1BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        //        String strUQC2Baserate = txtUQC2BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        int rowId = jTable1.getSelectedRow();
        if (rowId >= 0) {
            if (comboProduct.getSelectedItem().equals("Select the Product")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            }
            else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
            else if ((strUQC1rate.equals("") || strUQC2rate.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                if (isProductTaxInclusive) {
                    txtUQC1RateWithTax.requestFocus();
                }
                else {
                    txtUQC1Rate.requestFocus();
                }
            }
            else if (isProductHereInTable(comboProduct.getSelectedItem().toString(), rowId) > -1) {
                JOptionPane.showMessageDialog(null, "Product Already in list", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            }
            else {
                int count = jTable1.getRowCount() + 1;
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, UQC1rateWithRate = 0, UQC2rateWithRate = 0, UQC1Baserate = 0, UQC2Baserate = 0, cessAmount = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strUQC2rate.equals("")) {
                    UQC2rate = Float.parseFloat(strUQC2rate);
                }
                if (!strUQC1rateWithTax.equals("")) {
                    UQC1rateWithRate = Float.parseFloat(strUQC1rateWithTax);
                }
                if (!strUQC2rateWithTax.equals("")) {
                    UQC2rateWithRate = Float.parseFloat(strUQC2rateWithTax);
                }
                //                if (!strUQC1Baserate.equals("")) {
                //                    UQC1Baserate = Float.parseFloat(strUQC1Baserate);
                //                }
                //                if (!strUQC2Baserate.equals("")) {
                //                    UQC2Baserate = Float.parseFloat(strUQC2Baserate);
                //                }
                if (!strCessAmount.equals("")) {
                    cessAmount = Float.parseFloat(strCessAmount);
                }

                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
                float Rate = Float.parseFloat(txtUQC1RateWithTax.getText()),
                        IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                        CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                        SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                        Amount = Float.parseFloat(labelProductTotal.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String productName = (String) comboProduct.getSelectedItem();
                float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
                Profit = Profit - Float.parseFloat(Discount);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                Float ProductTotalAmount = Amount + IGST_amt + CGST_amt + SGST_amt;
                Float TotalAmoutInclTax = ProductTotalAmount + cessAmount;
                isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
                bCheckStock = false;
                Product selProduct = null;
                for (Product selectedProduct : products) {
                    if (selectedProduct.getName().equalsIgnoreCase(productName)) {
                        selProduct = selectedProduct;
                        break;
                    }
                }
                String strGodown = "";
                if (!isServiceProduct && isInventoryEnabled) {
                    strGodown = comboGodown.getSelectedItem().toString();
                }
                if (isInventoryEnabled && !isUnderStockPossible) {
                    if (wholeQuantity <= productStock) {
                        bCheckStock = true;
                    }
                }
                else {
                    bCheckStock = true;
                }
                if (descAdded) {
                    desc1 = productDescriptionUI.prodDesc1;
                    desc2 = productDescriptionUI.prodDesc2;
                    desc3 = productDescriptionUI.prodDesc3;
                    desc4 = productDescriptionUI.prodDesc4;
                    desc5 = productDescriptionUI.prodDesc5;
                    desc6 = productDescriptionUI.prodDesc6;
                    desc7 = productDescriptionUI.prodDesc7;
                    desc8 = productDescriptionUI.prodDesc8;
                    desc9 = productDescriptionUI.prodDesc9;
                    desc10 = productDescriptionUI.prodDesc10;
                    descCount = productDescriptionUI.descCount;
                }
                if (bCheckStock) {

                    int rowSerialNumber = (int) model.getValueAt(rowId, 0);
                    String strQuantity = "";
                    if (UQC1qty == 0) {
                        strQuantity = UQC2qty + " " + labelUQC2.getText();
                    }
                    else if (UQC2qty == 0) {
                        strQuantity = UQC1qty + " " + labelUQC1.getText();
                    }
                    else {
                        strQuantity = UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText();
                    }

                    Object[] row = {rowSerialNumber, productName, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                        labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                        wholeQuantity, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGSTper, IGST_amt, CGSTper, CGST_amt, SGSTper, SGST_amt,
                        UQC1Baserate, UQC2Baserate, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax, strGodown, desc1, desc2, desc3, desc4, desc5, desc6,
                        desc7, desc8, desc9, desc10, descCount, false, UQC1rateWithRate, UQC2rateWithRate};
                    model.removeRow(rowId);
                    model.insertRow(rowId, row);

                    clear();
                    TotalCalculate();
                    jTableRender();
                    comboProduct.requestFocus();
                    btnProductDescription.setEnabled(false);
                }
                else {
                    JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtUQC1Qty.requestFocus();
                }
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void updateServiceItems() {
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strCessAmount = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        int rowId = jTable1.getSelectedRow();
        if (rowId >= 0) {
            if (comboProduct.getSelectedItem().equals("Select the Product")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            }
            else if (strUQC1rate.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1RateWithTax.requestFocus();
            }
            else if (isProductHereInTable(comboProduct.getSelectedItem().toString(), rowId) > -1) {
                JOptionPane.showMessageDialog(null, "Product Already in list", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            }
            else {
                float UQC1rate = 0, UQC1rateWithTax = 0, cessAmount = 0;

                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }

                if (!strUQC1rateWithTax.equals("")) {
                    UQC1rateWithTax = Float.parseFloat(strUQC1rateWithTax);
                }

                if (!strCessAmount.equals("")) {
                    cessAmount = Float.parseFloat(strCessAmount);
                }

                float Rate = Float.parseFloat(txtUQC1RateWithTax.getText()),
                        IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                        CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                        SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                        Amount = Float.parseFloat(labelProductTotal.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String productName = (String) comboProduct.getSelectedItem();
                float Profit = UQC1rate;
                Profit = Profit - Float.parseFloat(Discount);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                float ProductTotalAmount = Amount + IGST_amt + CGST_amt + SGST_amt;
                float TotalAmoutInclTax = ProductTotalAmount + cessAmount;
                bCheckStock = false;
                Product selProduct = null;
                for (Product selectedProduct : products) {
                    if (selectedProduct.getName().equalsIgnoreCase(productName)) {
                        selProduct = selectedProduct;
                        break;
                    }
                }
                String strGodown = "";

                desc1 = productDescriptionUI.prodDesc1;
                desc2 = productDescriptionUI.prodDesc2;
                desc3 = productDescriptionUI.prodDesc3;
                desc4 = productDescriptionUI.prodDesc4;
                desc5 = productDescriptionUI.prodDesc5;
                desc6 = productDescriptionUI.prodDesc6;
                desc7 = productDescriptionUI.prodDesc7;
                desc8 = productDescriptionUI.prodDesc8;
                desc9 = productDescriptionUI.prodDesc9;
                desc10 = productDescriptionUI.prodDesc10;
                descCount = productDescriptionUI.descCount;

                int serialNumber = (int) model.getValueAt(rowId, 0);
                Object[] row = {serialNumber, productName, HSNCode, "", 0, UQC1rate, UQC1rate,
                    "", 0, 0, 0, "", 0, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGSTper, IGST_amt, CGSTper,
                    CGST_amt, SGSTper, SGST_amt, 0, 0, Profit, ProductTotalAmount, cessAmount, TotalAmoutInclTax,
                    "", desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, true, UQC1rateWithTax, 0};
                model.removeRow(rowId);
                model.insertRow(rowId, row);
                clear();
                TotalCalculate();
                jTableRender();
                comboProduct.requestFocus();
                btnProductDescription.setEnabled(false);

            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }
        private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
            if (isServiceProduct) {
                updateServiceItems();
            }
            else {
                updateProductsItems();
            }

        }//GEN-LAST:event_btnUpdateActionPerformed

        private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            SaveAndPrint(true);
        }//GEN-LAST:event_btnPrintActionPerformed

        private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
            clear();
        }//GEN-LAST:event_btnClearActionPerformed

        private void txtUQC1RateWithTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxActionPerformed
            if (txtUQC2RateWithTax.isEnabled()) {
                txtUQC2RateWithTax.requestFocus();
            }
            else {
                txtDiscountPer.requestFocus();
            }
            if (!isServiceProduct) {
                String strUQC1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("^\\s+$", "");
                String strUQC2 = txtUQC2Qty.getText().replaceAll("\\s+", "").replaceAll("^\\s+$", "");
                if (((strUQC1.equals("") || strUQC1.equals("0")) && (strUQC2.equals("0") || strUQC2.equals("")))) {
                    JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtUQC1Qty.requestFocus();
                }
            }
        }//GEN-LAST:event_txtUQC1RateWithTaxActionPerformed

        private void txtUQC1RateWithTaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyReleased
            if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
                String strUQC1RateWithTax = txtUQC1RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (strUQC1RateWithTax.equals("")) {
                    txtUQC1Rate.setText(currency.format(0));
                    txtUQC2RateWithTax.setText(currency.format(0));
                    txtUQC2Rate.setText(currency.format(0));
                }
                else {
                    float fUQC1RateWithTax = Float.parseFloat(strUQC1RateWithTax);
                    int Uqc2Value = productDetail.getUQC2Value();
                    float fUQC2RateWithTax = fUQC1RateWithTax / Uqc2Value;
                    float fUQC1Rate = 0;
                    float fUQC2Rate = 0;
                    fUQC1Rate = (fUQC1RateWithTax / (100 + IGSTper)) * 100;
                    fUQC2Rate = fUQC1Rate / Uqc2Value;

                    if (!isServiceProduct) {
                        txtUQC2RateWithTax.setText(currency.format(fUQC2RateWithTax));
                        txtUQC1Rate.setText(currency.format(fUQC1Rate));
                        txtUQC2Rate.setText(currency.format(fUQC2Rate));
                    }
                    else {
                        txtUQC2RateWithTax.setText("");
                        txtUQC1Rate.setText(currency.format(fUQC1Rate));
                        txtUQC2Rate.setText("");
                    }
                }

                if (isServiceProduct) {
                    TaxValueCalculationForService(true);
                }
                else {
                    TaxValueCalculationForProduct(true);
                }
            }
        }//GEN-LAST:event_txtUQC1RateWithTaxKeyReleased

        private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
            txtDiscount.requestFocus();
        }//GEN-LAST:event_txtDiscountPerActionPerformed

        private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtDiscountPerKeyPressed

        private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
            if (isServiceProduct) {
                TaxValueCalculationForService(true);
            }
            else {
                TaxValueCalculationForProduct(true);
            }
        }//GEN-LAST:event_txtDiscountPerKeyReleased

        private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC2RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtDiscountPerKeyTyped

        private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
            if (comboGodown.isVisible()) {
                comboGodown.requestFocus();
            }
            else {
                txtCessAmount.requestFocus();
            }
        }//GEN-LAST:event_txtDiscountActionPerformed

        private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
            if (isServiceProduct) {
                TaxValueCalculationForService(false);
            }
            else {
                TaxValueCalculationForProduct(false);
            }

        }//GEN-LAST:event_txtDiscountKeyReleased

        private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC2RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtDiscountKeyTyped

        private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
            int i = jTable1.getSelectedRow();
            if (i != -1) {
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                isServiceProduct = (Boolean) model.getValueAt(i, 40);
                productDetail = new GetProduct().getProductByName(model.getValueAt(i, 1).toString());

                if (isServiceProduct) {
                    setServiceDetailForUpdate();
                }
                else {
                    setProductDetailForUpdate();
                }
            }
        }//GEN-LAST:event_jTable1MouseClicked
    private void setProductDetailForUpdate() {
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

        comboProduct.setSelectedItem(model.getValueAt(i, 1));
        productDetail = new GetProduct().getProductByName(model.getValueAt(i, 1).toString());
        isProductTaxInclusive = productDetail.isInclusiveOfGST();

        comboProduct.setEnabled(false);
        HSNCode = (String) model.getValueAt(i, 2);
        labelUQC1.setText((String) model.getValueAt(i, 3));
        labelUQC1forPrice.setText((String) model.getValueAt(i, 3));
        labelUQC1forPriceWithTax.setText((String) model.getValueAt(i, 3));
        txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
        txtUQC1Rate.setText(currency.format(model.getValueAt(i, 5)));
        txtUQC1RateWithTax.setText(currency.format(model.getValueAt(i, 41)));
        labelUQC2forPriceWithTax.setText((String) model.getValueAt(i, 7));
        labelUQC2forPrice.setText((String) model.getValueAt(i, 7));
        labelUQC2.setText((String) model.getValueAt(i, 7));
        txtUQC2Qty.setText(model.getValueAt(i, 8).toString());
        txtUQC2Rate.setText(currency.format(model.getValueAt(i, 9)));
        txtUQC2RateWithTax.setText(currency.format(model.getValueAt(i, 42)));

        txtDiscountPer.setText(String.valueOf((float) model.getValueAt(i, 13)));
        txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
        labelProductTotal.setText(currency.format(model.getValueAt(i, 15)));
        labelProductTotalWithTax.setText(currency.format(model.getValueAt(i, 25)));
        txtIGSTper.setText(model.getValueAt(i, 16).toString());
        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
        txtCGSTper.setText(model.getValueAt(i, 18).toString());
        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
        txtSGSTper.setText(model.getValueAt(i, 20).toString());
        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
        txtCessAmount.setText(currency.format(model.getValueAt(i, 26)));

        btnProductDescription.setEnabled(true);

        productDescriptionUI.prodDesc1 = model.getValueAt(i, 29).toString();
        productDescriptionUI.prodDesc2 = model.getValueAt(i, 30).toString();
        productDescriptionUI.prodDesc3 = model.getValueAt(i, 31).toString();
        productDescriptionUI.prodDesc4 = model.getValueAt(i, 32).toString();
        productDescriptionUI.prodDesc5 = model.getValueAt(i, 33).toString();
        productDescriptionUI.prodDesc6 = model.getValueAt(i, 34).toString();
        productDescriptionUI.prodDesc7 = model.getValueAt(i, 35).toString();
        productDescriptionUI.prodDesc8 = model.getValueAt(i, 36).toString();
        productDescriptionUI.prodDesc9 = model.getValueAt(i, 37).toString();
        productDescriptionUI.prodDesc10 = model.getValueAt(i, 38).toString();
        productDescriptionUI.descCount = Integer.parseInt(model.getValueAt(i, 39).toString());
        desc1 = (String) model.getValueAt(i, 29);
        desc2 = (String) model.getValueAt(i, 30);
        desc3 = (String) model.getValueAt(i, 31);
        desc4 = (String) model.getValueAt(i, 32);
        desc5 = (String) model.getValueAt(i, 33);
        desc6 = (String) model.getValueAt(i, 34);
        desc7 = (String) model.getValueAt(i, 35);
        desc8 = (String) model.getValueAt(i, 36);
        desc9 = (String) model.getValueAt(i, 37);
        desc10 = (String) model.getValueAt(i, 38);
        descCount = (Integer) model.getValueAt(i, 39);

        btnAdd.setEnabled(false);
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);
        txtUQC1Qty.requestFocus();

        if (!isServiceProduct && isInventoryEnabled) {
            comboGodown.setSelectedItem(model.getValueAt(i, 28).toString());
            comboGodown.setEnabled(true);
        }
        else {
            comboGodown.setSelectedIndex(0);
            comboGodown.setEnabled(false);
        }

        if (productDetail.getUQC1().equalsIgnoreCase(productDetail.getUQC2())) {
            bUQCSame = true;
            labelUQC2.setEnabled(false);
            labelUQC2forPriceWithTax.setEnabled(false);
            txtUQC2Qty.setEnabled(false);
            txtUQC2RateWithTax.setEnabled(false);
        }
        else {
            bUQCSame = false;
            labelUQC2.setEnabled(true);
            labelUQC2forPriceWithTax.setEnabled(true);
            txtUQC2Qty.setEnabled(true);
            txtUQC2RateWithTax.setEnabled(true);
            labelUQC2forPriceWithTax.setEnabled(true);
        }
        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);

        setStock();
    }

    private void setServiceDetailForUpdate() {
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

        comboProduct.setSelectedItem(model.getValueAt(i, 1));
        productDetail = new GetProduct().getProductByName(model.getValueAt(i, 1).toString());
        isProductTaxInclusive = productDetail.isInclusiveOfGST();

        comboProduct.setEnabled(false);
        HSNCode = (String) model.getValueAt(i, 2);
        labelUQC1.setText("");
        labelUQC1forPriceWithTax.setText("");
        labelUQC1forPrice.setText("");
        txtUQC1Qty.setText("");
        txtUQC1Qty.setEnabled(false);
        txtUQC1Rate.setText(currency.format(model.getValueAt(i, 5)));
        txtUQC1RateWithTax.setText(currency.format(Float.parseFloat(model.getValueAt(i, 41).toString())));
        labelUQC2forPriceWithTax.setText("");
        labelUQC2forPrice.setText("");
        labelUQC2.setText("");
        txtUQC2Qty.setText("");
        txtUQC2Qty.setEnabled(false);
        txtUQC2Rate.setText("");
        txtUQC2Rate.setEnabled(false);
        txtUQC2RateWithTax.setText("");
        txtUQC2RateWithTax.setEnabled(false);

        txtDiscountPer.setText(String.valueOf((float) model.getValueAt(i, 13)));
        txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
        labelProductTotal.setText(currency.format(model.getValueAt(i, 15)));
        labelProductTotalWithTax.setText(currency.format(model.getValueAt(i, 25)));
        txtIGSTper.setText(model.getValueAt(i, 16).toString());
        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
        txtCGSTper.setText(model.getValueAt(i, 18).toString());
        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
        txtSGSTper.setText(model.getValueAt(i, 20).toString());
        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
        txtCessAmount.setText(currency.format(model.getValueAt(i, 26)));

        btnProductDescription.setEnabled(true);
        desc1 = (String) model.getValueAt(i, 29);
        desc2 = (String) model.getValueAt(i, 30);
        desc3 = (String) model.getValueAt(i, 31);
        desc4 = (String) model.getValueAt(i, 32);
        desc5 = (String) model.getValueAt(i, 33);
        desc6 = (String) model.getValueAt(i, 34);
        desc7 = (String) model.getValueAt(i, 35);
        desc8 = (String) model.getValueAt(i, 36);
        desc9 = (String) model.getValueAt(i, 37);
        desc10 = (String) model.getValueAt(i, 38);
        descCount = (Integer) model.getValueAt(i, 39);
        btnAdd.setEnabled(false);
//        btnProductDescription.setEnabled(false);
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);
        setProductTaxIncluviceHideAndShow(isProductTaxInclusive);
        if (isProductTaxInclusive) {
            txtUQC1RateWithTax.requestFocus();
        }
        else {
            txtUQC1Rate.requestFocus();
        }

        comboGodown.setSelectedItem("");
        comboGodown.setEnabled(false);

        bUQCSame = true;
        labelStock.setText("N/A");
    }

        private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
            TaxValueCalculationForProduct(true);
        }//GEN-LAST:event_txtUQC2QtyKeyReleased

        private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
            bErrorAlert = true;
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtUQC1QtyKeyPressed

        private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
            TaxValueCalculationForProduct(true);
        }//GEN-LAST:event_txtUQC1QtyKeyReleased

        private void txtUQC2RateWithTaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyReleased
            if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
                String strUQC2RateWithTax = txtUQC2RateWithTax.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (strUQC2RateWithTax.equals("")) {
                    txtUQC1RateWithTax.setText(currency.format(0));
                    txtUQC1Rate.setText(currency.format(0));
                    txtUQC2Rate.setText(currency.format(0));
                }
                else {
                    float fUQC2RateWithTax = Float.parseFloat(strUQC2RateWithTax);
                    int Uqc2Value = productDetail.getUQC2Value();
                    float fUQC1RateWithTax = fUQC2RateWithTax * Uqc2Value;
                    float fUQC1Rate = 0;
                    float fUQC2Rate = 0;
                    fUQC1Rate = (fUQC1RateWithTax / (100 + IGSTper)) * 100;
                    fUQC2Rate = fUQC1Rate / Uqc2Value;
                    if (isServiceProduct) {
                        txtUQC1RateWithTax.setText("");
                        txtUQC1Rate.setText("");
                        txtUQC2Rate.setText("");
                    }
                    else {
                        txtUQC1RateWithTax.setText(currency.format(fUQC1RateWithTax));
                        txtUQC1Rate.setText(currency.format(fUQC1Rate));
                        txtUQC2Rate.setText(currency.format(fUQC2Rate));
                    }
                }
                TaxValueCalculationForProduct(true);
            }
        }//GEN-LAST:event_txtUQC2RateWithTaxKeyReleased

        private void txtUQC1RateWithTaxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC1RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtUQC1RateWithTaxKeyTyped

        private void txtUQC2RateWithTaxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC2RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtUQC2RateWithTaxKeyTyped

        private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
            char vChar = evt.getKeyChar();
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }//GEN-LAST:event_txtUQC1QtyKeyTyped

        private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
            char vChar = evt.getKeyChar();
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }//GEN-LAST:event_txtUQC2QtyKeyTyped


        private void comboPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPricingPolicyActionPerformed

            String name = (String) comboPricingPolicy.getSelectedItem();
            if (name.equals("Default")) {
                recentSingleProductPolicies = new HashSet<SingleProductPolicy>();
            }
            else {
                for (PricingPolicy policy : pricingPolicy) {
                    if (policy.getPolicyName().equals(name)) {
                        Set<SingleProductPolicy> AllSPP = new PricingPolicyLogic().fetchAllSPP(policy);
                        recentSingleProductPolicies = AllSPP;
                    }
                }
            }
            btnShippingAddress.requestFocus();
        }//GEN-LAST:event_comboPricingPolicyActionPerformed

        private void txtMessageKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMessageKeyReleased
            messageCount();
        }//GEN-LAST:event_txtMessageKeyReleased

        private void checkSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkSMSActionPerformed
            Boolean isCheck = checkSMS.isSelected();
            txtMessage.setEnabled(isCheck);
            if (isCheck) {
                txtMessage.setText("Inv.No:#### ");
                messageCount();
            }
            else {
                txtMessage.setText("");
                messageCount();
            }
        }//GEN-LAST:event_checkSMSActionPerformed

        private void txtUQC2RateWithTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxActionPerformed
            txtDiscountPer.requestFocus();
        }//GEN-LAST:event_txtUQC2RateWithTaxActionPerformed

        private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
            if (bUQCSame) {
                if (isProductTaxInclusive) {
                    txtUQC1RateWithTax.requestFocus();
                }
                else {
                    txtUQC1Rate.requestFocus();
                }
            }
            else {
                txtUQC2Qty.requestFocus();
            }
            bErrorAlert = true;
        }//GEN-LAST:event_txtUQC1QtyActionPerformed

        private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
            if (isProductTaxInclusive) {
                txtUQC1RateWithTax.requestFocus();
            }
            else {
                txtUQC1Rate.requestFocus();
            }
            bErrorAlert = true;
        }//GEN-LAST:event_txtUQC2QtyActionPerformed

        private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained
            comboParty.setPopupVisible(true);
        }//GEN-LAST:event_comboPartyFocusGained

        private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed
            if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                chkReverseCharge.requestFocus();
            }
        }//GEN-LAST:event_comboPartyKeyPressed

        private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (isServiceProduct) {
                    updateServiceItems();
                }
                else {
                    updateProductsItems();
                }
                evt.consume();
            }
        }//GEN-LAST:event_btnUpdateKeyPressed

        private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
            if (jTabbedPane1.getSelectedIndex() == 0) {
                comboParty.requestFocus();
            }
            else if (jTabbedPane1.getSelectedIndex() == 1) {
                comboProduct.setSelectedItem("");
                comboProduct.requestFocus();
                btnAdd.setEnabled(true);
            }
        }//GEN-LAST:event_jTabbedPane1StateChanged


        private void comboPartyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyReleased
            // TODO add your handling code here:
        }//GEN-LAST:event_comboPartyKeyReleased


        private void txtUQC1BaseRateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1BaseRateKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC1RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtUQC1BaseRateKeyTyped

        private void txtUQC2BaseRateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2BaseRateKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtUQC1RateWithTax.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                    evt.consume();

                }
            }
        }//GEN-LAST:event_txtUQC2BaseRateKeyTyped

        private void txtUQC1QtyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUQC1QtyFocusGained
            if (comboProduct.getSelectedItem().equals("") || comboProduct.getSelectedItem() == null) {
                comboProduct.requestFocus();
            }
        }//GEN-LAST:event_txtUQC1QtyFocusGained

        private void btnShippingAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShippingAddressActionPerformed
            shippingAddressUI.setVisible(true);
        }//GEN-LAST:event_btnShippingAddressActionPerformed


        private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
            bErrorAlert = true;
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtUQC2QtyKeyPressed

        private void txtUQC1RateWithTaxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateWithTaxKeyPressed
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtUQC1RateWithTaxKeyPressed

        private void txtUQC2RateWithTaxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateWithTaxKeyPressed
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtUQC2RateWithTaxKeyPressed

        private void txtDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyPressed
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtDiscountKeyPressed

        private void comboGodownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGodownActionPerformed

            //      txtCessAmount.requestFocus();
        }//GEN-LAST:event_comboGodownActionPerformed

        private void txtStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStateActionPerformed
            // TODO add your handling code here:
        }//GEN-LAST:event_txtStateActionPerformed


        private void comboGodownItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboGodownItemStateChanged

        }//GEN-LAST:event_comboGodownItemStateChanged


        private void comboGodownKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGodownKeyPressed
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                setStock();
                txtCessAmount.requestFocus();
            }
        }//GEN-LAST:event_comboGodownKeyPressed

        private void txtBillDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillDiscountActionPerformed

        }//GEN-LAST:event_txtBillDiscountActionPerformed

        private void txtBillDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyPressed
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (btnAdd.isEnabled()) {
                    btnAdd.requestFocus();
                }
                else {
                    btnUpdate.requestFocus();
                }
            }
        }//GEN-LAST:event_txtBillDiscountKeyPressed

        private void txtBillDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyReleased

            if (evt.getKeyCode() != KeyEvent.VK_ENTER && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE && Character.isDigit(evt.getKeyChar())) {
                discount = Float.parseFloat(txtBillDiscount.getText());
                amount = amountWithoutDiscount - discount;
                labelBillAmount.setText(currency.format(amount));
            }
            else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && (amount < amountWithoutDiscount)) {
                String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscount.equals("")) {
                    Float fDiscount = Float.parseFloat(txtBillDiscount.getText());
                    amount = amount + (discount - fDiscount);
                    discount = discount - (discount - fDiscount);
                    labelBillAmount.setText(currency.format(amount));
                }
                else {
                    labelBillAmount.setText(currency.format(amountWithoutDiscount));
                }
            }
            else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (strDiscount.equals("")) {
                    txtBillDiscount.setText("0");
                }
                btnSave.requestFocus();
            }
        }//GEN-LAST:event_txtBillDiscountKeyReleased

        private void txtBillDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyTyped
            char vChar = evt.getKeyChar();
            boolean dot = false;

            if (txtBillDiscount.getText().equals("")) {
                dot = false;
            }
            if (dot == false) {
                if (vChar == '.') {
                    dot = true;
                }
                else if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                    evt.consume();

                }
            }
            else {
                if (!(Character.isDigit(vChar)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                        || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                    evt.consume();

                }
            }

        }//GEN-LAST:event_txtBillDiscountKeyTyped

        private void dateBillKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateBillKeyPressed

        }//GEN-LAST:event_dateBillKeyPressed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnProductDescriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductDescriptionActionPerformed
        descAdded = true;
        Product selProduct = null;
        String productName = (String) comboProduct.getEditor().getItem();
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                selProduct = product;
                break;
            }
        }
        int p = isProductHereInTable(productName, -1);
        if ((jTable1.getSelectedRow() > -1) || (p >= 0)) {
            productDescriptionUI = new PSLineItemsDescriptionUI(desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10);
            productDescriptionUI.setVisible(true);
        }
        else if (selProduct != null) {
            try {
                productDescriptionUI = new PSLineItemsDescriptionUI(selProduct);

            } catch (IOException ex) {
                Logger.getLogger(SalesB2BUI.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            productDescriptionUI.setVisible(true);
        }
        else {
            productDescriptionUI = new PSLineItemsDescriptionUI();
            productDescriptionUI.setVisible(true);
        }
    }//GEN-LAST:event_btnProductDescriptionActionPerformed

    private void billFinished() {
        if (isOldBill) {
            OldPrintAndSaveAction();
        }
        else {
            NewPrintAndSaveAction();
        }
    }

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        billFinished();
    }//GEN-LAST:event_btnOkActionPerformed

    private void txtPaymentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaymentKeyReleased
        float Amount = Float.parseFloat(labelPaymentBillAmount.getText());
        float payment = 0;
        if (!txtPayment.getText().equals("")) {
            payment = Float.parseFloat(txtPayment.getText());
        }
        float Balance = payment - Amount;
        labelBalance.setText(currency.format(Balance));
    }//GEN-LAST:event_txtPaymentKeyReleased

    private void txtPaymentKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaymentKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtPayment.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtPaymentKeyTyped

    private void txtPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPaymentActionPerformed
        btnOk.requestFocusInWindow();
    }//GEN-LAST:event_txtPaymentActionPerformed

    private void labelProductTotalComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_labelProductTotalComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_labelProductTotalComponentAdded

    private void txtCessAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCessAmountActionPerformed
        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        }
        else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtCessAmountActionPerformed

    private void txtCessAmountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCessAmountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strCess = txtCessAmount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strCess.equals("")) {
                txtCessAmount.setText("0");
            }
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCessAmountKeyPressed

    private void txtUQC2RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyTyped

    private void txtUQC2RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strUQC2Rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strUQC2Rate.equals("")) {
                txtUQC1RateWithTax.setText("");
                txtUQC2RateWithTax.setText("");
                txtUQC1Rate.setText("");
            }
            else {
                float fUQC2Rate = Float.parseFloat(strUQC2Rate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fUQC1Rate = fUQC2Rate * Uqc2Value;
                float fUQC1RateWithTax = 0;
                float fUQC2RateWithTax = 0;
                fUQC1RateWithTax = fUQC1Rate + ((fUQC1Rate * IGSTper) / 100);
                fUQC2RateWithTax = fUQC1RateWithTax / Uqc2Value;

                if (isServiceProduct) {
                    txtUQC1RateWithTax.setText(currency.format(fUQC1RateWithTax));
                    txtUQC2RateWithTax.setText("");
                    txtUQC2Rate.setText("");
                }
                else {
                    txtUQC1RateWithTax.setText(currency.format(fUQC1RateWithTax));
                    txtUQC2RateWithTax.setText(currency.format(fUQC2RateWithTax));
                    txtUQC1Rate.setText(currency.format(fUQC1Rate));
                }
            }
        }
        TaxValueCalculationForProduct(true);
    }//GEN-LAST:event_txtUQC2RateKeyReleased

    private void txtUQC2RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyPressed

    private void txtUQC2RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateActionPerformed
        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateActionPerformed

    private void txtUQC1RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            }
            else if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
        else {
            if (!(Character.isDigit(vChar)
                    || (vChar == KeyEvent.VK_BACK_SPACE)
                    || (vChar == KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyTyped

    private void txtUQC1RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC1RateWithTax.setText("");
                txtUQC2RateWithTax.setText("");
                txtUQC2Rate.setText("");
            }
            else {
                float fRate1 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate2 = fRate1 / Uqc2Value;
                float fRateWithTax1 = 0;
                float fRateWithTax2 = 0;
                fRateWithTax1 = fRate1 + ((fRate1 * IGSTper) / 100);
                fRateWithTax2 = fRateWithTax1 / Uqc2Value;
                if (isServiceProduct) {
                    txtUQC1RateWithTax.setText(currency.format(fRateWithTax1));
                    txtUQC2RateWithTax.setText("");
                    txtUQC2Rate.setText("");
                }
                else {
                    DecimalFormat dcm = new DecimalFormat("#.0000");
                    txtUQC1RateWithTax.setText(currency.format(fRateWithTax1));
                    txtUQC2RateWithTax.setText(dcm.format(fRateWithTax2));
                    txtUQC2Rate.setText(dcm.format(fRate2));

                }
            }

        }
        if (isServiceProduct) {
            TaxValueCalculationForService(true);
        }
        else {
            TaxValueCalculationForProduct(true);
        }
    }//GEN-LAST:event_txtUQC1RateKeyReleased

    private void txtUQC1RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            }
            else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyPressed

    private void txtUQC1RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateActionPerformed
        if (txtUQC2Rate.isEnabled()) {
            txtUQC2Rate.requestFocus();
        }
        else {
            txtDiscountPer.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1RateActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        }
        else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void comboProductPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboProductPopupMenuWillBecomeInvisible
        getProductDetails();
    }//GEN-LAST:event_comboProductPopupMenuWillBecomeInvisible

    private void txtDiscountPerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiscountPerFocusGained
        float qtyVal1 = 0;
        float qtyVal2 = 0;
        String val1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String val2 = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (isQty && !isServiceProduct) {
            if (!val1.equalsIgnoreCase("")) {
                qtyVal1 = Float.parseFloat(val1);
            }
            if (!val2.equalsIgnoreCase("")) {
                qtyVal2 = Float.parseFloat(val2);
            }
            if (qtyVal1 <= 0 && qtyVal2 <= 0 && isQty) {
                JOptionPane.showMessageDialog(null, "Please Enter the quantity");
                txtUQC1Qty.requestFocus();
                isQty = false;
            }
            else {
                isQty = false;
            }
        }
    }//GEN-LAST:event_txtDiscountPerFocusGained

    private void txtDiscountPerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiscountPerFocusLost
        isQty = true;

    }//GEN-LAST:event_txtDiscountPerFocusLost

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void messageCount() {
        String Message = txtMessage.getText();
        int count = 160 - Message.length();
        labelLeftMessageChar.setText(String.valueOf(count));
        if (Message.length() <= 160) {

        }
        else {
            JOptionPane.showMessageDialog(null, "Please Enter 160 Character only", "Alert", JOptionPane.WARNING_MESSAGE);
            Message = Message.substring(0, 160);
            txtMessage.setText(Message);
        }
    }

    private void setGoDown() {
        int lSize = godowns.size();
        for (int i = 0; i < lSize; i++) {
            comboGodown.addItem(godowns.get(i).getGoDownName());
        }
        if (lSize == 1) {
            lblGodown.setText("");
            comboGodown.setSelectedIndex(0);
            comboGodown.setVisible(false);
        }
        else {
            lblGodown.setText("Godown");
            comboGodown.setVisible(true);
        }
    }

    private void saveData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveAndPrint(false);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnSave.getActionMap();
        actionMap.put("Action", actionListener);
        btnSave.setActionMap(actionMap);
    }

    public void setPartyEditable(boolean isEditable) {
        dateBill.setEnabled(isEditable);
        comboParty.setEnabled(isEditable);
        chkReverseCharge.setEnabled(isEditable);
        checkSMS.setEnabled(isEditable);
        txtAddr1.setEnabled(isEditable);
        txtAddr2.setEnabled(isEditable);
        txtAddr3.setEnabled(isEditable);
        txtCity.setEnabled(isEditable);
        txtDistrict.setEnabled(isEditable);
        txtState.setEnabled(isEditable);
        txtGSTIN.setEnabled(isEditable);
        txtEmail.setEnabled(isEditable);
        txtMobile.setEnabled(isEditable);
        txtPhone.setEnabled(isEditable);
        txtMessage.setEnabled(isEditable);
        comboPricingPolicy.setEnabled(isEditable);
        btnShippingAddress.setEnabled(true);
        txtPartyName.setEnabled(isEditable);
    }

    public void setStock() {
        Product selProduct = null;
        String productName = (String) comboProduct.getEditor().getItem();
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                selProduct = product;
                break;
            }
        }
        if (!selProduct.isService() && isInventoryEnabled) {
            GoDown selGodown = godowns.get(0);
            for (GoDown godown : godowns) {
                if (comboGodown.getSelectedItem().toString().equals(godown.getGoDownName())) {
                    selGodown = godown;
                    break;
                }
            }
            stock = new GoDownStockCRUD().getGoDownStockDetail(selProduct.getProductId(), selGodown.getGoDownId());
            productStock = stock.getCurrentStock();
            if (isOldBill) {
                List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
                for (PurchaseSaleLineItem item : psLIs) {
                    if (item.getProductName().equals(productName)) {
                        productStock = productStock + item.getStockQuantity();
                    }
                }
            }
            labelStock.setText(setStockConverstion(productStock, selProduct.getUQC1(), selProduct.getUQC2(), selProduct.getUQC2Value()));
        }
        else {
            labelStock.setText("N/A");
        }
    }

    public int isProductHereInTable(String productName, int donotCheckRow) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int bProductAlreadyAdded = -1;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (donotCheckRow != i) {
                if (model.getValueAt(i, 1).toString().equals(productName)) {
                    bProductAlreadyAdded = i;
                    break;
                }
            }
        }
        return bProductAlreadyAdded;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
             * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SalesB2BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SalesB2BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SalesB2BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SalesB2BUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //WebLookAndFeel.install ();
        //com.alee.laf.WebLookAndFeel.install();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SalesB2BUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProductDescription;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnShippingAddress;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox checkSMS;
    private javax.swing.JCheckBox chkReverseCharge;
    private javax.swing.JComboBox<String> comboGodown;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboPricingPolicy;
    private javax.swing.JComboBox<String> comboProduct;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelAddress;
    private javax.swing.JLabel labelBalance;
    private javax.swing.JLabel labelBillAmount;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelGSTIN;
    private javax.swing.JLabel labelLeftMessageChar;
    private javax.swing.JLabel labelPartyName;
    private javax.swing.JLabel labelPaymentBillAmount;
    private javax.swing.JLabel labelProduct;
    private javax.swing.JLabel labelProductTotal;
    private javax.swing.JLabel labelProductTotalWithTax;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1forPrice;
    private javax.swing.JLabel labelUQC1forPriceWithTax;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2forPrice;
    private javax.swing.JLabel labelUQC2forPriceWithTax;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JLabel lblGodown;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblPriceWithTax;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JDialog paymentDialog;
    private javax.swing.JTable totalTable;
    private javax.swing.JTextField txtAddr1;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtAddr3;
    private javax.swing.JTextField txtBillDiscount;
    private javax.swing.JLabel txtCGSTamt;
    private javax.swing.JLabel txtCGSTper;
    private javax.swing.JTextField txtCessAmount;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JLabel txtIGSTamt;
    private javax.swing.JLabel txtIGSTper;
    private javax.swing.JTextArea txtMessage;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtPartyName;
    private javax.swing.JTextField txtPayment;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JLabel txtSGSTamt;
    private javax.swing.JLabel txtSGSTper;
    private javax.swing.JTextField txtState;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC1Rate;
    private javax.swing.JTextField txtUQC1RateWithTax;
    private javax.swing.JTextField txtUQC2Qty;
    private javax.swing.JTextField txtUQC2Rate;
    private javax.swing.JTextField txtUQC2RateWithTax;
    // End of variables declaration//GEN-END:variables
}
