/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import com.itextpdf.text.DocumentException;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.ProfitLossAccount;
import india.abs.gstonepro.api.models.TradingAccount;
import india.abs.gstonepro.api.models.BalanceSheet;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BalanceSheetLogic;
import india.abs.gstonepro.business.LedgerGroupLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.pdf.BalanceSheetPdf;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
import java.awt.Font;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author hp
 */
public class BalanceSheetUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    List<Ledger> AllLedger = new LedgerLogic().fetchAllCompanyLedgers(company.getCompanyId());
    List<TradingAccount> tradingAccountLedgers = new BalanceSheetLogic().fetchLedgersPresentInTradingAccount();
    List<ProfitLossAccount> profitLossLedgers = new BalanceSheetLogic().fetchLedgersPresentInProfitLossAccount();
    List<BalanceSheet> bsLedgers = new BalanceSheetLogic().fetchBSLedgers();
    Set<Ledger> tapfLedgers = new BalanceSheetLogic().fetchTradingAccountProfitLossLedgers();
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();
    List<LedgerGroup> ledgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
    Set<String> addedIncLedgers = new HashSet<>();
    Set<String> addedExpLedgers = new HashSet<>();
    Set<String> addedAssetLedgers = new HashSet<>();
    Set<String> addedLiabLedgers = new HashSet<>();
    List<Ledger> tableinclist = new ArrayList<>();
    List<Ledger> profitLossList = new ArrayList<>();

    public BalanceSheetUI() {
        initComponents();

        setMenuRoles();
        setIncLedgerGroup();
        setExpLedgerGroup();
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        comboIncomeLedger.requestFocus();
        btnIncremove.setEnabled(false);
        btnExpremove.setEnabled(false);
        this.setExtendedState(MAXIMIZED_BOTH);
        tableIncome.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        tableExpense.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        setTableData();
        fetch();
        fetchledGroups();
        labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
        labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());

        JTextComponent editor = (JTextComponent) comboIncomeLedger.getEditor().getEditorComponent();
        editor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboIncomeLedger.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Ledger ledger : AllLedger) {
                        if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                            scripts.add(ledger.getLedgerName());
                        }
                    }
                    for (Ledger lg : tapfLedgers) {
                        scripts.remove(lg.getLedgerName());
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboIncomeLedger.setModel(model);

                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboIncomeLedger.setPopupVisible(false);
                        btnIncomeAdd.requestFocus();
                    } else {
                        comboIncomeLedger.getEditor().setItem(val);
                        comboIncomeLedger.setPopupVisible(true);
                    }
                }
            }
        });

        JTextComponent editor1 = (JTextComponent) comboExpenseLedger.getEditor().getEditorComponent();
        editor1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboExpenseLedger.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Ledger ledger : AllLedger) {
                        if ((ledger.getLedgerName().toLowerCase().contains(val.toLowerCase()))) {
                            scripts.add(ledger.getLedgerName());
                        }
                    }
                    for (Ledger lg : tapfLedgers) {
                        scripts.remove(lg.getLedgerName());
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboExpenseLedger.setModel(model);

                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboExpenseLedger.setPopupVisible(false);
                        btnExpenseAdd.requestFocus();
                    } else {
                        comboExpenseLedger.getEditor().setItem(val);
                        comboExpenseLedger.setPopupVisible(true);
                    }
                }
            }
        });

        JTextComponent editor2 = (JTextComponent) comboLedgerGroup.getEditor().getEditorComponent();
        editor2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboLedgerGroup.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (LedgerGroup ledgerGroup : ledgerGroups) {
                        boolean bLedgerAlreadyHere = false;
                        Set<Ledger> selLedgers = ledgerGroup.getLedgers();
                        for (Ledger ledger : selLedgers) {
                            long id = ledger.getLedgerId();
                            if (bLedgerAlreadyHere == false) {
                                for (Ledger lg : tapfLedgers) {
                                    if (lg.getLedgerId() == id) {
                                        bLedgerAlreadyHere = true;
                                        break;
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        if ((ledgerGroup.getLedgerGroupName().toLowerCase().contains(val.toLowerCase())) && (!bLedgerAlreadyHere)) {
                            scripts.add(ledgerGroup.getLedgerGroupName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboLedgerGroup.setModel(model);

                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboLedgerGroup.setPopupVisible(false);
                        btnIncomeLedgerGrpAdd.requestFocus();
                    } else {
                        comboLedgerGroup.getEditor().setItem(val);
                        comboLedgerGroup.setPopupVisible(true);
                    }
                }
            }
        });
//
        JTextComponent editor3 = (JTextComponent) comboLedgerGroupExp.getEditor().getEditorComponent();
        editor3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboLedgerGroupExp.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (LedgerGroup ledgerGroup : ledgerGroups) {
                        boolean bLedgerAlreadyHere = false;
                        Set<Ledger> selLedgers = ledgerGroup.getLedgers();
                        for (Ledger ledger : selLedgers) {
                            long id = ledger.getLedgerId();
                            if (bLedgerAlreadyHere == false) {
                                for (Ledger lg : tapfLedgers) {
                                    if (lg.getLedgerId() == id) {
                                        bLedgerAlreadyHere = true;
                                        break;
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        if ((ledgerGroup.getLedgerGroupName().toLowerCase().contains(val.toLowerCase())) && (!bLedgerAlreadyHere)) {
                            scripts.add(ledgerGroup.getLedgerGroupName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboLedgerGroupExp.setModel(model);

                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboLedgerGroupExp.setPopupVisible(false);
                        btnExpenseGrpAdd.requestFocus();
                    } else {
                        comboLedgerGroupExp.getEditor().setItem(val);
                        comboLedgerGroupExp.setPopupVisible(true);
                    }
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableExpense = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableIncome = new javax.swing.JTable();
        btnIncomeAdd = new javax.swing.JButton();
        btnExpenseAdd = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        comboIncomeLedger = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        comboExpenseLedger = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        comboLedgerGroup = new javax.swing.JComboBox<>();
        comboLedgerGroupExp = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        btnIncomeLedgerGrpAdd = new javax.swing.JButton();
        btnExpenseGrpAdd = new javax.swing.JButton();
        btnIncremove = new javax.swing.JButton();
        btnExpremove = new javax.swing.JButton();
        btnClearExp = new javax.swing.JButton();
        btnClearInc = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        btnTAupdate = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        goHomebtn = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Balance Sheet");

        tableExpense.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Expense", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableExpense.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableExpenseMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableExpense);
        if (tableExpense.getColumnModel().getColumnCount() > 0) {
            tableExpense.getColumnModel().getColumn(0).setMinWidth(0);
            tableExpense.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableExpense.getColumnModel().getColumn(0).setMaxWidth(0);
            tableExpense.getColumnModel().getColumn(2).setMinWidth(0);
            tableExpense.getColumnModel().getColumn(2).setPreferredWidth(0);
            tableExpense.getColumnModel().getColumn(2).setMaxWidth(0);
        }

        tableIncome.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Income", "Amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.Object.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableIncome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableIncomeMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableIncome);
        if (tableIncome.getColumnModel().getColumnCount() > 0) {
            tableIncome.getColumnModel().getColumn(0).setMinWidth(0);
            tableIncome.getColumnModel().getColumn(0).setPreferredWidth(0);
            tableIncome.getColumnModel().getColumn(0).setMaxWidth(0);
            tableIncome.getColumnModel().getColumn(2).setMinWidth(0);
            tableIncome.getColumnModel().getColumn(2).setPreferredWidth(0);
            tableIncome.getColumnModel().getColumn(2).setMaxWidth(0);
        }

        btnIncomeAdd.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnIncomeAdd.setText("Add");
        btnIncomeAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncomeAddActionPerformed(evt);
            }
        });

        btnExpenseAdd.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnExpenseAdd.setText("Add");
        btnExpenseAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpenseAddActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("Ledger");

        comboIncomeLedger.setEditable(true);
        comboIncomeLedger.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboIncomeLedgerFocusGained(evt);
            }
        });
        comboIncomeLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboIncomeLedgerActionPerformed(evt);
            }
        });
        comboIncomeLedger.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboIncomeLedgerKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Ledger");

        comboExpenseLedger.setEditable(true);
        comboExpenseLedger.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboExpenseLedgerFocusGained(evt);
            }
        });
        comboExpenseLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboExpenseLedgerActionPerformed(evt);
            }
        });
        comboExpenseLedger.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboExpenseLedgerKeyPressed(evt);
            }
        });

        jLabel3.setText("Ledger Group");

        comboLedgerGroup.setEditable(true);
        comboLedgerGroup.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboLedgerGroupItemStateChanged(evt);
            }
        });
        comboLedgerGroup.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboLedgerGroupKeyPressed(evt);
            }
        });

        comboLedgerGroupExp.setEditable(true);
        comboLedgerGroupExp.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboLedgerGroupExpItemStateChanged(evt);
            }
        });
        comboLedgerGroupExp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboLedgerGroupExpKeyPressed(evt);
            }
        });

        jLabel4.setText("Ledger Group");

        btnIncomeLedgerGrpAdd.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnIncomeLedgerGrpAdd.setText("Add");
        btnIncomeLedgerGrpAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncomeLedgerGrpAddActionPerformed(evt);
            }
        });

        btnExpenseGrpAdd.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnExpenseGrpAdd.setText(" Add");
        btnExpenseGrpAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpenseGrpAddActionPerformed(evt);
            }
        });

        btnIncremove.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnIncremove.setText("Remove ");
        btnIncremove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncremoveActionPerformed(evt);
            }
        });

        btnExpremove.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnExpremove.setText("Remove");
        btnExpremove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpremoveActionPerformed(evt);
            }
        });

        btnClearExp.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClearExp.setText("Clear");
        btnClearExp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearExpActionPerformed(evt);
            }
        });

        btnClearInc.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClearInc.setText("Clear");
        btnClearInc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearIncActionPerformed(evt);
            }
        });

        btnTAupdate.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btnTAupdate.setText("Update");
        btnTAupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTAupdateActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jButton1.setText("Download");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(btnTAupdate, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnTAupdate, jButton1});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTAupdate, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(0, 0, 0))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnTAupdate, jButton1});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboIncomeLedger, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnIncomeAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnIncremove, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboLedgerGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnIncomeLedgerGrpAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnClearInc, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(80, 80, 80)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboExpenseLedger, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnExpenseAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(comboLedgerGroupExp, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnExpenseGrpAdd)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnExpremove, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                                    .addComponent(btnClearExp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnExpenseAdd, btnExpenseGrpAdd});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {comboExpenseLedger, comboLedgerGroupExp});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnClearInc, btnIncomeAdd, btnIncomeLedgerGrpAdd, btnIncremove});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel3});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {comboIncomeLedger, comboLedgerGroup});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, jLabel4});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIncomeAdd)
                    .addComponent(btnIncremove, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboExpenseLedger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(btnExpenseAdd)
                    .addComponent(btnExpremove, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboIncomeLedger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator10)
                    .addComponent(jSeparator9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboLedgerGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(btnIncomeLedgerGrpAdd)
                    .addComponent(jLabel4)
                    .addComponent(comboLedgerGroupExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExpenseGrpAdd)
                    .addComponent(btnClearInc)
                    .addComponent(btnClearExp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane5.setViewportView(jPanel1);

        jLabel24.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setMnemonic('l');
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel15.setText("BALANCE SHEET");

        jLabel16.setText("Company : ");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("----------");

        goHomebtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        goHomebtn.setMnemonic('h');
        goHomebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goHomebtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(146, 146, 146)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(goHomebtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel24)
                        .addComponent(jLabel15)
                        .addComponent(goHomebtn)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16)
                        .addComponent(labelCompanyName)))
                .addContainerGap())
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public LedgerGroup findLedgerGroup() {
        LedgerGroup getLedgerGroup = null;
        String ledgerGroupName = (String) comboLedgerGroup.getSelectedItem();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            if (ledgerGroupName.equals(ledgerGroup.getLedgerGroupName())) {
                getLedgerGroup = ledgerGroup;
                break;
            }
        }
        return getLedgerGroup;
    }

    public void fetch() {
        comboExpenseLedger.removeAllItems();
        comboIncomeLedger.removeAllItems();
        for (Ledger ledger : AllLedger) {
            comboIncomeLedger.addItem(ledger.getLedgerName());
            comboExpenseLedger.addItem(ledger.getLedgerName());
        }
        for (Ledger lg : tapfLedgers) {
            comboIncomeLedger.removeItem(lg.getLedgerName());
            comboExpenseLedger.removeItem(lg.getLedgerName());
        }
        btnExpremove.setEnabled(false);
        btnIncremove.setEnabled(false);
    }

    public LedgerGroup findLedgerGroupExpense() {
        LedgerGroup getLedgerGroup = null;
        String ledgerGroupName = (String) comboLedgerGroupExp.getSelectedItem();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            if (ledgerGroupName.equals(ledgerGroup.getLedgerGroupName())) {
                getLedgerGroup = ledgerGroup;
                break;
            }
        }
        return getLedgerGroup;
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
        //btn

    }

    public void setIncLedgerGroup() {
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            comboLedgerGroup.addItem(ledgerGroup.getLedgerGroupName());
        }
    }

    public void setExpLedgerGroup() {
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            comboLedgerGroupExp.addItem(ledgerGroup.getLedgerGroupName());
        }
    }

    public void setTableData() {
        DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        DefaultTableModel model1 = (DefaultTableModel) tableExpense.getModel();
        if (model1.getRowCount() > 0) {
            model1.setRowCount(0);
        }

        for (BalanceSheet bs : bsLedgers) {
            String strType = bs.getType();

            if (strType.equals("EXPENSE")) {
                Ledger lgExpense = bs.getLedger();
                if (lgExpense != null) {
                    String lgExpName = lgExpense.getLedgerName();
                    long expId = lgExpense.getLedgerId();
                    Object row[] = {expId, lgExpName};
                    model1.addRow(row);
                    tapfLedgers.add(lgExpense);
                }
            } else {
                Ledger lgIncome = bs.getLedger();
                BigDecimal ledSum;
                if (lgIncome != null) {
                    ledSum = new BalanceSheetLogic().getLedgerSum(lgIncome);
                    if (ledSum == null) {
                        ledSum = new BigDecimal(0);
                    }
                    String lgIncName = lgIncome.getLedgerName();
                    long incId = lgIncome.getLedgerId();
                    Object row[] = {incId, lgIncName, ledSum.floatValue()};
                    model.addRow(row);
                    tapfLedgers.add(lgIncome);
                }
            }
        }
    }
    private void comboIncomeLedgerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboIncomeLedgerFocusGained
        comboIncomeLedger.setPopupVisible(true);
        comboIncomeLedger.showPopup();
    }//GEN-LAST:event_comboIncomeLedgerFocusGained

    private void comboIncomeLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboIncomeLedgerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboIncomeLedgerActionPerformed

    private void comboIncomeLedgerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboIncomeLedgerKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnIncomeAdd.requestFocusInWindow();
        }
    }//GEN-LAST:event_comboIncomeLedgerKeyPressed

    private void comboExpenseLedgerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboExpenseLedgerFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comboExpenseLedgerFocusGained

    private void comboExpenseLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboExpenseLedgerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboExpenseLedgerActionPerformed

    private void comboExpenseLedgerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboExpenseLedgerKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboExpenseLedgerKeyPressed

    private void btnIncomeAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncomeAddActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
        String income = comboIncomeLedger.getSelectedItem().toString();
        Long id = null;

        Ledger selLedger = null;
        for (Ledger ledgers : AllLedger) {
            if (ledgers.getLedgerName().equals(income)) {
                id = ledgers.getLedgerId();
                selLedger = ledgers;
                break;
            }
        }
        if (addedIncLedgers.contains(income)) {
            JOptionPane.showMessageDialog(null, income + " already present in Income", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if (addedExpLedgers.contains(income)) {
            JOptionPane.showMessageDialog(null, income + " already present in Expense", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            BigDecimal ledSum = new BalanceSheetLogic().getLedgerSum(selLedger);
            if (ledSum == null) {
                ledSum = new BigDecimal(0);
            }
            Object[] row = {id, income, ledSum.floatValue()};
            model.addRow(row);
            addedIncLedgers.add(income);
            tapfLedgers.add(selLedger);
            fetch();
            fetchledGroups();
        }
//        if (tableExpense.getRowCount()> 0) {
//            for (int i = 0; i <= tableExpense.getRowCount(); i++) {
//                if (income.equals(tableExpense.getValueAt(i, 0).toString())) {
//                    JOptionPane.showMessageDialog(null, income + " already in the expense table. It cannot be added", "Alert", JOptionPane.WARNING_MESSAGE);
//                } else {
//                    Object[] row = {income};
//                    model.addRow(row);
//                }
//            }
//        
//            Object[] row = {income};
//            model.addRow(row);
//        }


    }//GEN-LAST:event_btnIncomeAddActionPerformed

    private void btnExpenseAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpenseAddActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableExpense.getModel();
        BigDecimal ledSum;
        String expense = comboExpenseLedger.getSelectedItem().toString();
        Ledger selLedger = null;
        Long id = null;
        for (Ledger ledgers : AllLedger) {
            if (ledgers.getLedgerName().equals(expense)) {
                id = ledgers.getLedgerId();
                selLedger = ledgers;
                break;
            }
        }
        if (addedIncLedgers.contains(expense)) {
            JOptionPane.showMessageDialog(null, expense + " already present in Income", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if (addedExpLedgers.contains(expense)) {
            JOptionPane.showMessageDialog(null, expense + " already present in the Expense", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            ledSum = new BalanceSheetLogic().getLedgerSum(selLedger);
            if (ledSum == null) {
                ledSum = new BigDecimal(0);
            }
            Object[] row = {id, expense, ledSum.floatValue()};
            model.addRow(row);
            tapfLedgers.add(selLedger);
            addedExpLedgers.add(expense);
            fetch();
            fetchledGroups();
        }
    }//GEN-LAST:event_btnExpenseAddActionPerformed

    private void comboLedgerGroupKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboLedgerGroupKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fetch();
            comboIncomeLedger.requestFocus();
        }
    }//GEN-LAST:event_comboLedgerGroupKeyPressed

    private void comboLedgerGroupItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboLedgerGroupItemStateChanged

    }//GEN-LAST:event_comboLedgerGroupItemStateChanged

    private void comboLedgerGroupExpItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboLedgerGroupExpItemStateChanged

    }//GEN-LAST:event_comboLedgerGroupExpItemStateChanged

    private void comboLedgerGroupExpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboLedgerGroupExpKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboExpenseLedger.requestFocus();
        }
    }//GEN-LAST:event_comboLedgerGroupExpKeyPressed

    private void btnTAupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTAupdateActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
        int incomeRowCount = model.getRowCount();
        DefaultTableModel model1 = (DefaultTableModel) tableExpense.getModel();
        int expenseRowCount = model1.getRowCount();
        if (incomeRowCount == 0 || expenseRowCount == 0) {
            JOptionPane.showMessageDialog(null, "Please Add Atleast One Income or Expense", "Alert", JOptionPane.WARNING_MESSAGE);
            comboLedgerGroup.requestFocus();
        } else {
            Ledger selIncledger = null, selExpledger = null;
            List<BalanceSheet> inctas = new ArrayList<BalanceSheet>();
            for (int i = 0; i < incomeRowCount; i++) {
                BalanceSheet inc = new BalanceSheet();
                String incledger = model.getValueAt(i, 1).toString();
                String transAmt = model.getValueAt(i, 2).toString();
                for (Ledger ledger : AllLedger) {
                    if (ledger.getLedgerName().equals(incledger)) {
                        selIncledger = ledger;
                        break;
                    }
                }
                String inctype = ("INCOME");
                inc.setLedger(selIncledger);
                inc.setType(inctype);
                inc.setTransactionAmount(new BigDecimal(transAmt));
                inctas.add(inc);

            }
            List<BalanceSheet> exptas = new ArrayList<BalanceSheet>();
            for (int i = 0; i < expenseRowCount; i++) {
                BalanceSheet exp = new BalanceSheet();
                String expledger = model1.getValueAt(i, 1).toString();
                String transAmt = model.getValueAt(i, 2).toString();
                for (Ledger ledger : AllLedger) {
                    if (ledger.getLedgerName().equals(expledger)) {
                        selExpledger = ledger;
                        break;
                    }
                }
                String exptype = ("EXPENSE");
                exp.setLedger(selExpledger);
                exp.setType(exptype);
                exp.setTransactionAmount(new BigDecimal(transAmt));
                exptas.add(exp);
            }
            inctas.addAll(exptas);
            EventStatus result = new BalanceSheetLogic().updateBalanceSheet(inctas);
            if (result.isUpdateDone()) {
                JOptionPane.showMessageDialog(null, "Balance Sheet have been saved successfully", "Message", JOptionPane.INFORMATION_MESSAGE);

                Ledger ledger = null;

                for (int row = 0; row < tableIncome.getRowCount(); row++) {
                    ledger = new Ledger();
                    ledger.setLedgerId((Long) tableIncome.getValueAt(row, 0));
                    ledger.setLedgerName((String) tableIncome.getValueAt(row, 1));
                    tableinclist.add(ledger);

                }
                for (int row = 0; row < tableExpense.getRowCount(); row++) {
                    ledger = new Ledger();
                    ledger.setLedgerId((Long) tableExpense.getValueAt(row, 0));
                    ledger.setLedgerName((String) tableExpense.getValueAt(row, 1));
                    tableinclist.add(ledger);
                }
                ledgerWithoutTrading();

            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        }

    }//GEN-LAST:event_btnTAupdateActionPerformed
    public void ledgerWithoutTrading() {
        for (Ledger ledger : AllLedger) {
            for (Ledger tablelist : tableinclist) {
                if (!(ledger.getLedgerId().toString().equals(tablelist.getLedgerId().toString()))) {//  (ledger.getLedgerName().equals(tablelist.getLedgerName()))) {
                    profitLossList.add(ledger);
                }
            }
        }
    }
    private void btnIncomeLedgerGrpAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncomeLedgerGrpAddActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
        BigDecimal ledSum;
        String income = comboLedgerGroup.getSelectedItem().toString();
        boolean isalreadyhereInc = false, isalreadyhereExp = false;
        String SelledgerGroupName = (String) comboLedgerGroup.getSelectedItem();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            if (SelledgerGroupName.equals(ledgerGroup.getLedgerGroupName())) {
                for (Ledger selLedger : ledgerGroup.getLedgers()) {
                    if (addedExpLedgers.contains(selLedger.getLedgerName())) {
                        isalreadyhereExp = true;
                        break;
                    } else if (addedIncLedgers.contains(selLedger.getLedgerName())) {
                        isalreadyhereInc = true;
                        break;
                    }
                }
            }
        }

        if (isalreadyhereInc) {
            JOptionPane.showMessageDialog(null, income + " already present in Income", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if (isalreadyhereExp) {
            JOptionPane.showMessageDialog(null, income + " already present in Expense", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            Set<Ledger> ledgers = null;
            comboIncomeLedger.removeAllItems();
            LedgerGroup ledgerGroup = findLedgerGroup();
            if (ledgerGroup == null) {
                JOptionPane.showMessageDialog(null, "Please Select LedgerGroup", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                ledgers = ledgerGroup.getLedgers();
                for (Ledger ledger : ledgers) {
                    ledSum = new BalanceSheetLogic().getLedgerSum(ledger);
                    if (ledSum == null) {
                        ledSum = new BigDecimal(0);
                    }
                    Object[] row = {ledger.getLedgerId(), ledger.getLedgerName(), ledSum.floatValue()};
                    model.addRow(row);
                    tapfLedgers.add(ledger);
                    addedIncLedgers.add(ledger.getLedgerName());
                }
                fetch();
                fetchledGroups();
            }

        }
    }//GEN-LAST:event_btnIncomeLedgerGrpAddActionPerformed

    public void fetchledGroups() {
        comboLedgerGroupExp.removeAllItems();
        comboLedgerGroup.removeAllItems();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            boolean bLedgerAlreadyHere = false;
            Set<Ledger> selLedgers = ledgerGroup.getLedgers();
            for (Ledger ledger : selLedgers) {
                long id = ledger.getLedgerId();
                if (bLedgerAlreadyHere == false) {
                    for (Ledger lg : tapfLedgers) {
                        if (lg.getLedgerId() == id) {
                            bLedgerAlreadyHere = true;
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            if (!bLedgerAlreadyHere) {
                comboLedgerGroupExp.addItem(ledgerGroup.getLedgerGroupName());
                comboLedgerGroup.addItem(ledgerGroup.getLedgerGroupName());
            }
        }
    }
    private void btnExpenseGrpAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpenseGrpAddActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableExpense.getModel();
        BigDecimal ledSum;
        String expense = comboLedgerGroupExp.getSelectedItem().toString();
        boolean isalreadyhereInc = false, isalreadyhereExp = false;
        String SelledgerGroupName = (String) comboLedgerGroupExp.getSelectedItem();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            if (SelledgerGroupName.equals(ledgerGroup.getLedgerGroupName())) {
                for (Ledger selLedger : ledgerGroup.getLedgers()) {
                    if (addedExpLedgers.contains(selLedger.getLedgerName())) {
                        isalreadyhereExp = true;
                        break;
                    } else if (addedIncLedgers.contains(selLedger.getLedgerName())) {
                        isalreadyhereInc = true;
                        break;
                    }
                }
            }
        }
        if (isalreadyhereInc) {
            JOptionPane.showMessageDialog(null, expense + " already present in Income", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if (isalreadyhereExp) {
            JOptionPane.showMessageDialog(null, expense + " already present in Expense", "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            Set<Ledger> ledgers = null;
            comboExpenseLedger.removeAllItems();
            LedgerGroup ledgerGroup = findLedgerGroupExpense();
            if (ledgerGroup == null) {
                JOptionPane.showMessageDialog(null, "Please Select LedgerGroup", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                ledgers = ledgerGroup.getLedgers();
                for (Ledger ledger : ledgers) {
                    ledSum = new BalanceSheetLogic().getLedgerSum(ledger);
                    if (ledSum == null) {
                        ledSum = new BigDecimal(0);
                    }
                    Object[] row = {ledger.getLedgerId(), ledger.getLedgerName(), ledSum.floatValue()};
                    model.addRow(row);
                    tapfLedgers.add(ledger);
                    addedExpLedgers.add(ledger.getLedgerName());
                }
                fetch();
                fetchledGroups();
            }
        }
    }//GEN-LAST:event_btnExpenseGrpAddActionPerformed

    private void btnIncremoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncremoveActionPerformed
        int i = tableIncome.getSelectedRow();
        if (i >= 0) {
            DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
            String ledgertoRemove = model.getValueAt(i, 1).toString();
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                for (Iterator<String> iterator = addedIncLedgers.iterator(); iterator.hasNext();) {
                    String value = iterator.next();
                    if (ledgertoRemove.equals(value)) {
                        iterator.remove();
                    }
                }
                String ledgertoRemoveId = model.getValueAt(i, 0).toString();
                long id = Long.parseLong(ledgertoRemoveId);
                for (Iterator<Ledger> iterator = tapfLedgers.iterator(); iterator.hasNext();) {
                    Ledger lg = iterator.next();
                    if (lg.getLedgerId() == id) {
                        iterator.remove();
                    }
                }
                model.removeRow(i);
                fetch();
                fetchledGroups();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select any Ledger", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnIncremoveActionPerformed

    private void btnExpremoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpremoveActionPerformed
        int i = tableExpense.getSelectedRow();
        if (i >= 0) {
            DefaultTableModel model = (DefaultTableModel) tableExpense.getModel();
            String ledgertoRemove = model.getValueAt(i, 1).toString();
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                for (Iterator<String> iterator = addedExpLedgers.iterator(); iterator.hasNext();) {
                    String value = iterator.next();
                    if (ledgertoRemove.equals(value)) {
                        iterator.remove();
                    }
                }
                String ledgertoRemoveId = model.getValueAt(i, 0).toString();
                long id = Long.parseLong(ledgertoRemoveId);
                for (Iterator<Ledger> iterator = tapfLedgers.iterator(); iterator.hasNext();) {
                    Ledger lg = iterator.next();
                    if (lg.getLedgerId() == id) {
                        iterator.remove();
                    }
                }
                model.removeRow(i);
                fetch();
                fetchledGroups();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select any Ledger", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnExpremoveActionPerformed

    private void tableExpenseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableExpenseMouseClicked
        int i = tableExpense.getSelectedRow();
        if (tableExpense.getRowSorter() != null) {
            i = tableExpense.getRowSorter().convertRowIndexToModel(i);
        }
        DefaultTableModel model = (DefaultTableModel) tableExpense.getModel();
        btnExpremove.setEnabled(true);
    }//GEN-LAST:event_tableExpenseMouseClicked

    private void tableIncomeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableIncomeMouseClicked
        int i = tableIncome.getSelectedRow();
        if (tableIncome.getRowSorter() != null) {
            i = tableIncome.getRowSorter().convertRowIndexToModel(i);
        }
        DefaultTableModel model = (DefaultTableModel) tableIncome.getModel();
        btnIncremove.setEnabled(true);
    }//GEN-LAST:event_tableIncomeMouseClicked

    private void btnClearIncActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearIncActionPerformed
        clearIncome();
        btnIncremove.setEnabled(false);
    }//GEN-LAST:event_btnClearIncActionPerformed

    private void btnClearExpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearExpActionPerformed
        clearExpense();
        btnExpremove.setEnabled(false);
    }//GEN-LAST:event_btnClearExpActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void goHomebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goHomebtnActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
            new DashBoardUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled && !isAccountEnabled) {
            new DashBoardSilverUI().setVisible(true);
            dispose();
        } else if (!isInventoryEnabled && !isAccountEnabled) {
            new DashBoardCopperUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_goHomebtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            List<BalanceSheet> bsLedger = new BalanceSheetLogic().fetchBSLedgers();
            new BalanceSheetPdf().createPdf(bsLedger);
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(BalanceSheetUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed
    public void clearIncome() {
        tableIncome.clearSelection();
        comboLedgerGroup.setSelectedItem("");
        comboIncomeLedger.setSelectedItem("");
    }

    public void clearExpense() {
        tableExpense.clearSelection();
        comboLedgerGroupExp.setSelectedItem("");
        comboExpenseLedger.setSelectedItem("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BalanceSheetUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BalanceSheetUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BalanceSheetUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BalanceSheetUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BalanceSheetUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearExp;
    private javax.swing.JButton btnClearInc;
    private javax.swing.JButton btnExpenseAdd;
    private javax.swing.JButton btnExpenseGrpAdd;
    private javax.swing.JButton btnExpremove;
    private javax.swing.JButton btnIncomeAdd;
    private javax.swing.JButton btnIncomeLedgerGrpAdd;
    private javax.swing.JButton btnIncremove;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnTAupdate;
    private javax.swing.JComboBox<String> comboExpenseLedger;
    private javax.swing.JComboBox<String> comboIncomeLedger;
    private javax.swing.JComboBox<String> comboLedgerGroup;
    private javax.swing.JComboBox<String> comboLedgerGroupExp;
    private javax.swing.JButton goHomebtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTable tableExpense;
    private javax.swing.JTable tableIncome;
    // End of variables declaration//GEN-END:variables
}
