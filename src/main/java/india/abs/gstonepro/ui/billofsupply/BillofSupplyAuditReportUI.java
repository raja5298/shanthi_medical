/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.billofsupply;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import india.abs.gstonepro.api.models.AppUser;
import india.abs.gstonepro.api.models.Audit;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BillOfSupplyLogic;
import india.abs.gstonepro.business.UserLogic;
import india.abs.gstonepro.ui.AddStockUI;
import india.abs.gstonepro.ui.BackupUI;
import india.abs.gstonepro.ui.BankBookUI;
import india.abs.gstonepro.ui.BankEntryUI;
import india.abs.gstonepro.ui.CashBookUI;
import india.abs.gstonepro.ui.CashEntryUI;
import india.abs.gstonepro.ui.ClosingStockUI;
import india.abs.gstonepro.ui.CompanyUI;
import india.abs.gstonepro.ui.ContactUsUI;
import india.abs.gstonepro.ui.CreditNoteReportsGroupUI;
import india.abs.gstonepro.ui.CreditNoteUI;
import india.abs.gstonepro.ui.CurrentStockReportwithGodownUI;
import india.abs.gstonepro.ui.DashBoardUI;
import india.abs.gstonepro.ui.DayBookUI;
import india.abs.gstonepro.ui.DebitNoteReportsGroupUI;
import india.abs.gstonepro.ui.DebitNoteUI;
import india.abs.gstonepro.ui.DeductionStockUI;
import india.abs.gstonepro.ui.EstimateReportsGroupUI;
import india.abs.gstonepro.ui.EstimateUI;
import india.abs.gstonepro.ui.GSTR1UI;
import india.abs.gstonepro.ui.JournalEntryUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsCOPPERUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsGOLDUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsSILVERUI;
import india.abs.gstonepro.ui.LedgerBookUI;
import india.abs.gstonepro.ui.LedgerGroupTrialBalanceUI;
import india.abs.gstonepro.ui.LedgerUI;
import india.abs.gstonepro.ui.LogInUI;
import india.abs.gstonepro.ui.OpeningBalanceUI;
import india.abs.gstonepro.ui.OpeningStockUI;
import india.abs.gstonepro.ui.PartyDetailsUI;
import india.abs.gstonepro.ui.PricingPolicyUI;
import india.abs.gstonepro.ui.ProductGroupReport;
import india.abs.gstonepro.ui.ProductGroupUI;
import india.abs.gstonepro.ui.ProductUI;
import india.abs.gstonepro.ui.PurchaseReportGroupUI;
import india.abs.gstonepro.ui.PurchaseUI;
import india.abs.gstonepro.ui.SalesB2BUI;
import india.abs.gstonepro.ui.SalesB2CUI;
import india.abs.gstonepro.ui.SalesPartyWiseReportUI;
import india.abs.gstonepro.ui.ServiceUI;
import india.abs.gstonepro.ui.SetCompanyUI;
import india.abs.gstonepro.ui.TaxInvoiceReportsGroupUI;
import india.abs.gstonepro.ui.TaxUI;
import india.abs.gstonepro.ui.TrialBalanceUI;
import india.abs.gstonepro.ui.UQCUI;
import india.abs.gstonepro.ui.UserUI;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.DateTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author Admin
 */
public class BillofSupplyAuditReportUI extends javax.swing.JFrame {

    /**
     * Creates new form BillofSupplyAuditReportUI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    List<AppUser> users = new UserLogic().fetchAllUsers();
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();

    public BillofSupplyAuditReportUI() {
        try {
            initComponents();
            goHome();
            //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
//        jMenuItem3.setEnabled(false);
//        jMenuItem9.setEnabled(false);
//        jMenuItem11.setEnabled(false);
//        jMenuItem12.setEnabled(false);
            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
            labelCompanyName.setText(company.getCompanyName());
            labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
            setExtendedState(this.MAXIMIZED_BOTH);
            setUsers();
            dateFrom.setDate(new Date());
            dateTo.setDate(new Date());
        } catch (Exception ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      private void goHome() {
          goHomebtn.setVisible(false);
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = goHomebtn.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = goHomebtn.getActionMap();
        actionMap.put("Action", actionListener);
        goHomebtn.setActionMap(actionMap);
    }

    public void setUsers() {
        for (AppUser user : users) {
            comboUser.addItem(user.getUserName());
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        labelTotal = new javax.swing.JLabel();
        btnFetch = new javax.swing.JButton();
        comboUser = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        dateTo = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        btnExcelReport = new javax.swing.JButton();
        btnDownloadReport = new javax.swing.JButton();
        btnPrintReport = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        goHomebtn = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp2 = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bill od Supply User Report");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Bill Number", "Party", "Bill Amount", "Created by", "Time", "Last Update by", "Update Time", "isCancel"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(0);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
        }

        labelTotal.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        labelTotal.setText("Total Amount : 0.00 ");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        comboUser.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboUser.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ALL" }));
        comboUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboUserActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("User");

        dateTo.setDateFormatString("dd-MM-yyyy");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("To");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("From");

        btnExcelReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnExcelReport.setText("Download Report (Excel)");
        btnExcelReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelReportActionPerformed(evt);
            }
        });

        btnDownloadReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDownloadReport.setText("Download Report (PDF)");
        btnDownloadReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadReportActionPerformed(evt);
            }
        });

        btnPrintReport.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnPrintReport.setText("Print Report");
        btnPrintReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintReportActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-------");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel4.setText("CURRENT STOCK");

        jLabel12.setText("User :");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout1.setMnemonic('l');
        btnLogout1.setText("Logout");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });

        goHomebtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(goHomebtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(labelCompanyName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUserName)
                        .addComponent(jLabel12)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(goHomebtn)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('t');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuBar.add(menuGST);

        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setText("Help");

        menuHelp2.setText("Keyboard Shortcuts");
        menuHelp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelp2ActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp2);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 122, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelTotal)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 985, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(comboUser, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnDownloadReport, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnExcelReport, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 123, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnFetch))
                    .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(dateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(labelTotal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrintReport, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownloadReport, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcelReport, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        fetch();
    }//GEN-LAST:event_btnFetchActionPerformed
    public void fetch() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strParty = "";
        String strUser = "";
        String strBillId = "";
        String strUpdatedBy = "";
        String strCreatedBy = "";
        String dateCreated = "";
        String dateUpdated = "";
        SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Audit data = null;
        String strBillNumber = "";
        BigDecimal billAmount = null;
        AppUser selUser = null;
        List<BillOfSupply> lBosData = new ArrayList<BillOfSupply>();

        try {
            strUser = comboUser.getSelectedItem().toString();
            Date dateFrom1 = dateFrom.getDate();
            Date dateTo1 = dateTo.getDate();
            dateFrom1.setHours(0);
            dateFrom1.setMinutes(0);
            dateFrom1.setSeconds(0);
            dateTo1.setHours(23);
            dateTo1.setMinutes(59);
            dateTo1.setSeconds(59);

            if (strUser.equalsIgnoreCase("All")) {
                lBosData = new BillOfSupplyLogic().readBOSAuditData(dateFrom1, dateTo1);
            } else {
                for (AppUser user : users) {
                    if (comboUser.getSelectedItem().equals(user.getUserName())) {
                        selUser = user;
                        break;
                    }
                }
                lBosData = new BillOfSupplyLogic().readBOSAuditDataByUser(dateFrom1, dateTo1, selUser);
            }
            
            DefaultTableModel modelAudit = (DefaultTableModel) jTable1.getModel();
            if (modelAudit.getRowCount() > 0) {
                modelAudit.setRowCount(0);
            }
            for (BillOfSupply bos : lBosData) {
                strBillId = bos.getBosId();
                data = bos.getAuditData();

                if (bos.getLedger().getLedgerName() == null) {
                    strParty = "";
                } else {
                    strParty = bos.getLedger().getLedgerName();
                }
                billAmount = bos.getBillAmount();
                strBillNumber = bos.getBosNo();
                boolean bCancel = bos.isCancelled();
                dateCreated = sft.format(data.getCreatedOn());
                strCreatedBy = data.getCreatedBy().getUserName();
                if (data.getUpdatedBy() == null) {
                    strUpdatedBy = "";
                } else {
                    strUpdatedBy = data.getUpdatedBy().getUserName();
                }
                if (data.getUpdatedOn() != null) {
                    dateUpdated = sft.format(data.getUpdatedOn());
                }
                Object[] row = {strBillId, strBillNumber, strParty, billAmount, strCreatedBy, dateCreated, strUpdatedBy, dateUpdated, bCancel};
                
                modelAudit.addRow(row);
            }
            // tableAudit.getColumnModel().getColumn(4).setCellRenderer(new DateTableCellRenderer());
            if (data != null && data.getUpdatedOn() != null) {
                jTable1.getColumnModel().getColumn(7).setCellRenderer(new DateTableCellRenderer());
            }
            TotalCalculate();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public void TotalCalculate() {
        float totalAmount = 0;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            totalAmount = totalAmount + Float.parseFloat(model.getValueAt(i, 3).toString());
        }
        labelTotal.setText(" Total Bill Amount : " + currency.format(totalAmount));
    }
    private void comboUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboUserActionPerformed

    }//GEN-LAST:event_comboUserActionPerformed

    private void btnExcelReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            generateExcelReport();
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnExcelReportActionPerformed

    private void btnDownloadReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(false);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnDownloadReportActionPerformed

    private void btnPrintReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintReportActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            pdfReportGeneration(true);
        } else {
            JOptionPane.showMessageDialog(null, "No Records Found", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintReportActionPerformed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    private void menuHelp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelp2ActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelp2ActionPerformed
    public void generateExcelReport() {
        try {
            java.util.Date from = dateFrom.getDate();
            java.util.Date to = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(from);
            String formatdateTo = sdf.format(to);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            String fileName = "Audit Report " + sdf.format(from) + " to " + sdf.format(to);
            JFrame parentFrame = new JFrame();

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    try {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    } catch (UnsupportedLookAndFeelException ex) {
                        Logger.getLogger(ProductGroupReport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Audit Excel Report");
            fileChooser.setSelectedFile(new File(fileName));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                String excelpath = fileToSave.getAbsolutePath() + ".xls";
                File f = new File(excelpath);
                WritableWorkbook myexcel;
                myexcel = Workbook.createWorkbook(f);
                WritableSheet mysheet = myexcel.createSheet("mysheet", 0);
                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);
                WritableCellFormat rupeeFormat = new WritableCellFormat();
                rupeeFormat.setAlignment(Alignment.RIGHT);
                rupeeFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat rightFormat = new WritableCellFormat(wf2);
                rightFormat.setAlignment(Alignment.RIGHT);
                rightFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat centerFormat = new WritableCellFormat(wf2);
                centerFormat.setAlignment(Alignment.CENTRE);
                centerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableFont wf4 = new WritableFont(WritableFont.TIMES);
                mysheet.setColumnView(0, 15);
                mysheet.setColumnView(1, 25);
                mysheet.setColumnView(2, 15);
                mysheet.setColumnView(3, 15);
                mysheet.setColumnView(4, 20);
                mysheet.setColumnView(5, 20);
                mysheet.setColumnView(6, 15);
                //mysheet.setAutobreaks(false);
                WritableCellFormat cellFormat = new WritableCellFormat(wf2);
                cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
                WritableCellFormat cellFormat1 = new WritableCellFormat(wf3);
                cellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
                mysheet.setName("Audit Report");

                mysheet.addCell(new Label(0, 0, "GSTIN/UIN: " + company.getGSTIN(), cellFormat));
                mysheet.mergeCells(0, 0, 2, 0);
                mysheet.addCell(new Label(3, 0, "Audit Group Report " + formatdateFrom + " to " + formatdateTo + "", rightFormat));
                mysheet.mergeCells(3, 0, 6, 0);
                mysheet.addCell(new Label(0, 1, company.getCompanyName(), centerFormat));
                mysheet.mergeCells(0, 1, 6, 1);
                if (!company.getAddress().equals("")) {
                    mysheet.addCell(new Label(0, 2, company.getAddress().replaceAll("/~/", ""), centerFormat));
                    mysheet.mergeCells(0, 2, 6, 2);
                }
                mysheet.addCell(new Label(0, 3, "City : " + company.getCity(), centerFormat));
                mysheet.mergeCells(0, 3, 6, 3);
                mysheet.addCell(new Label(0, 4, "State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), centerFormat));
                mysheet.mergeCells(0, 4, 6, 4);
                mysheet.addCell(new Label(0, 5, "Phone : " + company.getPhone(), centerFormat));
                mysheet.mergeCells(0, 5, 6, 5);
                mysheet.addCell(new Label(0, 6, "Mobile : " + company.getMobile(), centerFormat));
                mysheet.mergeCells(0, 6, 6, 6);
                mysheet.addCell(new Label(0, 7, "Email : " + company.getEmail(), centerFormat));
                mysheet.mergeCells(0, 7, 6, 7);
                mysheet.addCell(new Label(0, 8, "Bill Number", cellFormat));
                mysheet.addCell(new Label(1, 8, "Party", cellFormat));
                mysheet.addCell(new Label(2, 8, "Bill Amount", cellFormat));
                mysheet.addCell(new Label(3, 8, "Created By", cellFormat));
                mysheet.addCell(new Label(4, 8, "Time", cellFormat));
                mysheet.addCell(new Label(5, 8, "Last Updated by", cellFormat));
                mysheet.addCell(new Label(6, 8, "Update Time", cellFormat));

                float totalAmt = 0, totalProfit = 0;
                int totalPostion = 9 + model.getRowCount();
                

                for (int i = 0; i < model.getRowCount(); i++) {
//                        String tablevalue = null;
//                        try {
//                            System.err.println("tablevalue " + model.getValueAt(i, cols).toString() + " " + cols);
//                            tablevalue = model.getValueAt(i, cols).toString();
//                        } catch (NullPointerException ex) {
//                            if (tablevalue == null) {
//                                tablevalue = "Nil";
//                            }
//                        }
                    totalAmt = totalAmt + Float.parseFloat(model.getValueAt(i, 3).toString());
                    int rowValue = 9 + i;
                    mysheet.addCell(new Label(0, rowValue, jTable1.getValueAt(i, 1).toString(), cellFormat1));
                    mysheet.addCell(new Label(1, rowValue, jTable1.getValueAt(i, 2).toString(), cellFormat1));
                    mysheet.addCell(new Label(2, rowValue, jTable1.getValueAt(i, 3).toString(), rupeeFormat));
                    mysheet.addCell(new Label(3, rowValue, jTable1.getValueAt(i, 4).toString(), cellFormat1));
                    mysheet.addCell(new Label(4, rowValue, jTable1.getValueAt(i, 5).toString(), cellFormat1));
                    Object value = jTable1.getValueAt(i, 6);
                    

                    if (value.equals("")) {
                        mysheet.addCell(new Label(5, rowValue, " ", cellFormat1));
                    } else {
                        mysheet.addCell(new Label(5, rowValue, jTable1.getValueAt(i, 6).toString(), cellFormat1));
                    }
                    Object value1 = jTable1.getValueAt(i, 7);
                    
                    if (value.equals("")) {
                        mysheet.addCell(new Label(6, rowValue, " ", cellFormat1));
                    } else {
                        mysheet.addCell(new Label(6, rowValue, jTable1.getValueAt(i, 7).toString(), cellFormat1));
                    }
                    String strAmount = "";
                    strAmount = currency.format(Float.parseFloat(model.getValueAt(i, 3).toString()));
                }

                mysheet.addCell(new Label(0, totalPostion, "Total", centerFormat));
                mysheet.mergeCells(0, totalPostion, 1, totalPostion);
                mysheet.addCell(new Label(2, totalPostion, currency.format(totalAmt), rightFormat));

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String d = dtf.format(now);
                String date1 = "Report generated as on " + d;
                int datePosition = totalPostion + 2;
                mysheet.addCell(new Label(0, datePosition, date1, centerFormat));
                mysheet.mergeCells(0, datePosition, 6, datePosition + 1);
                myexcel.write();
                myexcel.close();
                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(fileToSave.getParent());
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, "Document not to be Opened");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Excel File Download At " + excelpath);
                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (WriteException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesPartyWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfReportGeneration(Boolean isPrint) {
        try {
            Date datefrom = dateFrom.getDate();
            Date dateto = dateTo.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formatdateFrom = sdf.format(datefrom);
            String formatdateTo = sdf.format(dateto);
            Document document = new Document(PageSize.A4);
            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Bill of Supply Report " + formatdateFrom + " to " + formatdateTo + ".pdf";
                isApproved = true;
            } else {
//                path = AppConstants.getDocPath() +"Bill of Supply Report " + formatdateFrom + " to " + formatdateTo  + ".pdf";

                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill of Supply Report");
                fileChooser.setSelectedFile(new File("Bill of Supply User Report " + formatdateFrom + " to " + formatdateTo));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                Phrase p;
                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(2);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell;

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN: " + company.getGSTIN(), reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                String reportdate = "Bill of Supply User Reports " + formatdateFrom + " to " + formatdateTo + "";
                taxcell = new PdfPCell(new Phrase(reportdate, reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                taxTable.addCell(taxcell);
                document.add(taxTable);

                PdfPTable companyTable = new PdfPTable(1);
                companyTable.setWidthPercentage(100);
                PdfPCell cell = null;

                cell = new PdfPCell(new Phrase(company.getCompanyName(), boldFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                if (!company.getAddress().equals("")) {
                    cell = new PdfPCell(new Phrase(company.getAddress().replaceAll("/~/", ","), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                cell = new PdfPCell(new Phrase("City : " + company.getCity(), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);

                cell = new PdfPCell(new Phrase("State : " + company.getState() + "  Code : " + StateCode.getStateCode(company.getState()), cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                companyTable.addCell(cell);
                if (!company.getPhone().equals("")) {
                    cell = new PdfPCell(new Phrase("Phone : " + company.getPhone(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getMobile().equals("")) {
                    cell = new PdfPCell(new Phrase("Mobile : " + company.getMobile(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }
                if (!company.getEmail().equals("")) {
                    cell = new PdfPCell(new Phrase("Email : " + company.getEmail(), cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    companyTable.addCell(cell);
                }

                document.add(companyTable);

                PdfPTable thirdTable = new PdfPTable(7);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{15, 20, 25, 20, 20, 20, 20});

                taxcell = new PdfPCell(new Phrase("Bill", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Party", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Bill Amount", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Created By", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Time", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Updated By", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Update Time", reportdateFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                float totalAmount = 0;
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                for (int rows = 0; rows < jTable1.getRowCount(); rows++) {
                    totalAmount = totalAmount + Float.parseFloat(model.getValueAt(rows, 3).toString());
                    for (int cols = 1; cols < jTable1.getColumnCount() - 1; cols++) {
                        String tablevalue = "";
//                        if (cols == 5 || cols == 7) {
//                            
//                            tablevalue = s2[2] + "-" + s2[1] + "-" + s2[0];
//                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
//                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                            thirdTable.addCell(taxcell);
//                        } else 
                        if (cols == 3) {
                            tablevalue = currency.format(model.getValueAt(rows, cols)).toString();
                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                            thirdTable.addCell(taxcell);
                        } else {
                            tablevalue = model.getValueAt(rows, cols).toString();
                            taxcell = new PdfPCell(new Phrase(tablevalue, cellFont));
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                            thirdTable.addCell(taxcell);
                        }
                    }
                }
                taxcell = new PdfPCell(new Phrase("Total ", reportdateFont));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                taxcell.setColspan(2);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(totalAmount), totalFont));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                thirdTable.addCell("");
                thirdTable.addCell("");
                thirdTable.addCell("");
                thirdTable.addCell("");

                document.add(thirdTable);
                document.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);
                        JOptionPane.showMessageDialog(null, "Bill of Supply User Report is Printing....... " + path);
                    } catch (PrintException ex) {
                        Logger.getLogger(BillofSupplyDateWiseReport.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(BillofSupplyDateWiseReport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }

                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BillofSupplyAuditReportUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BillofSupplyAuditReportUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDownloadReport;
    private javax.swing.JButton btnExcelReport;
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout1;
    private javax.swing.JButton btnPrintReport;
    private javax.swing.JComboBox<String> comboUser;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton goHomebtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuHelp2;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    // End of variables declaration//GEN-END:variables
}
