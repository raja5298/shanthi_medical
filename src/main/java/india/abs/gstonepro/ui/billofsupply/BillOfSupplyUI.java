/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.billofsupply;

import india.abs.gstonepro.pdf.NonGSTBill;
import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PricingPolicyLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BillChargeLogic;
import india.abs.gstonepro.business.BillOfSupplyLogic;
import india.abs.gstonepro.ui.AddStockUI;
import india.abs.gstonepro.ui.BackupUI;
import india.abs.gstonepro.ui.BankBookUI;
import india.abs.gstonepro.ui.BankEntryUI;
import india.abs.gstonepro.ui.CashBookUI;
import india.abs.gstonepro.ui.CashEntryUI;
import india.abs.gstonepro.ui.ClosingStockUI;
import india.abs.gstonepro.ui.CompanyUI;
import india.abs.gstonepro.ui.ContactUsUI;
import india.abs.gstonepro.ui.CreditNoteReportsGroupUI;
import india.abs.gstonepro.ui.CreditNoteUI;
import india.abs.gstonepro.ui.CurrentStockReportwithGodownUI;
import india.abs.gstonepro.ui.DashBoardUI;
import india.abs.gstonepro.ui.DayBookUI;
import india.abs.gstonepro.ui.DebitNoteReportsGroupUI;
import india.abs.gstonepro.ui.DebitNoteUI;
import india.abs.gstonepro.ui.DeductionStockUI;
import india.abs.gstonepro.ui.EstimateReportsGroupUI;
import india.abs.gstonepro.ui.EstimateUI;
import india.abs.gstonepro.ui.GSTR1UI;
import india.abs.gstonepro.ui.JournalEntryUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsCOPPERUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsGOLDUI;
import india.abs.gstonepro.ui.KeyBoardShortcutsSILVERUI;
import india.abs.gstonepro.ui.LedgerBookUI;
import india.abs.gstonepro.ui.LedgerGroupTrialBalanceUI;
import india.abs.gstonepro.ui.LedgerUI;
import india.abs.gstonepro.ui.OpeningBalanceUI;
import india.abs.gstonepro.ui.OpeningStockUI;
import india.abs.gstonepro.ui.PartyDetailsUI;
import india.abs.gstonepro.ui.PricingPolicyUI;
import india.abs.gstonepro.ui.ProductGroupUI;
import india.abs.gstonepro.ui.ProductUI;
import india.abs.gstonepro.ui.PurchaseReportGroupUI;
import india.abs.gstonepro.ui.PurchaseUI;
import india.abs.gstonepro.ui.SalesB2BUI;
import india.abs.gstonepro.ui.SalesB2CUI;
import india.abs.gstonepro.ui.ServiceUI;
import india.abs.gstonepro.ui.SetCompanyUI;
import india.abs.gstonepro.ui.TaxInvoiceReportsGroupUI;
import india.abs.gstonepro.ui.TaxUI;
import india.abs.gstonepro.ui.TrialBalanceUI;
import india.abs.gstonepro.ui.UQCUI;
import india.abs.gstonepro.ui.UserUI;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.FormValidation;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JLabel;

/**
 *
 * @author ABS
 */
public class BillOfSupplyUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String HSNCode;
    boolean isIGST = false, isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    int lastBillId;
    Long LedgerId;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0, BaseProductRate = 0;
    int productStock = 0;
    float billAmount = 0, billprofit, amountWithoutDiscount, amount, discount;
    Boolean isActive = true;

    Boolean isOldBill = false;
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Long companyId = company.getCompanyId();
    String CompanyState = SessionDataUtil.getSelectedCompany().getState();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    //Ledger ledgerData = new Ledger();
    Product product = new Product();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(companyId);
    List<BillCharge> charges = new BillChargeLogic().fetchAllBillCharges();

    List<PricingPolicy> pricingPolicy = new PricingPolicyLogic().fetchAllPP(companyId);
    Set<SingleProductPolicy> recentSingleProductPolicies = new HashSet<>();
    FormValidation formValidation = new FormValidation();

    BillOfSupply oldBos = new BillOfSupply();
    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();

    /**
     * Creates new form Bill
     */
    public BillOfSupplyUI() {
//        try {
        initComponents();
//            //            Image im =ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));
        //        setIconImage(im);
        //        im.flush();
        setMenuRoles();
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        Collections.sort(sortedLedgers, new Comparator<Ledger>() {
            public int compare(Ledger o1, Ledger o2) {
                return o1.getLedgerName().compareTo(o2.getLedgerName());
            }
        });
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        setExtendedState(this.MAXIMIZED_BOTH);
//        labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
//        labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
        dateBill.setDate(new Date());
        setParty();
        setProduct();
//        checkRadioButton();
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());

        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
//        totalTable.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));

        comboParty.requestFocus();
        comboParty.setSelectedItem("");

        KeyStroke right = KeyStroke.getKeyStroke("alt RIGHT");
        KeyStroke left = KeyStroke.getKeyStroke("alt LEFT");
//        InputMap inputMap = jTabbedPane1.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
//        inputMap.put(right, "navigateNext");
//        inputMap.put(left, "navigatePrevious");

////         Add keys to the tab's input map
//        Home.setMnemonic(KeyEvent.VK_H);
//        Sales.setMnemonic(KeyEvent.VK_S);
//        Purchase.setMnemonic(KeyEvent.VK_P);
//        Reports.setMnemonic(KeyEvent.VK_R);
//        Stock.setMnemonic(KeyEvent.VK_T);
//        Accounts.setMnemonic(KeyEvent.VK_A);
        btnAdd.setMnemonic(KeyEvent.VK_I);

        JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
        editor.addKeyListener(new KeyAdapter() {
            public void KeyPressed(java.awt.event.KeyEvent evt) throws ParseException {
                Boolean isPrint = false;
                if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_S) {
                    isPrint = false;
                    if (isOldBill) {
                        OldPrintAndSaveAction(isPrint);
                    } else {
                        NewPrintAndSaveAction(isPrint);
                    }

                } else if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_P) {
                    isPrint = true;
                    if (isOldBill) {
                        OldPrintAndSaveAction(isPrint);
                    } else {
                        NewPrintAndSaveAction(isPrint);
                    }
                }
            }

            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboProduct.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Product product : products) {
                        if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                            scripts.add(product.getName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboProduct.setModel(model);
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboProduct.setPopupVisible(false);
                        comboProduct.getEditor().setItem(val);
                        getProductDetails();

                    } else {
                        comboProduct.getEditor().setItem(val);
                        comboProduct.setPopupVisible(true);
                    }
                }
            }
        });

        JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
        editor1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboParty.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();
                    String noParty = "No Party", newParty = "New Party";
                    if (noParty.toLowerCase().contains(val.toLowerCase())) {
                        scripts.add(noParty);
                    }
                    if (newParty.toLowerCase().contains(val.toLowerCase())) {
                        scripts.add(newParty);
                    }
                    for (Ledger ledger : sortedLedgers) {

                        if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {

                            scripts.add(ledger.getLedgerName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboParty.setModel(model);

                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboParty.setPopupVisible(false);
                        comboParty.setSelectedItem(comboParty.getSelectedItem());
                        getPartyDetails();
                    } else {
                        comboParty.getEditor().setItem(val);
                        comboParty.setPopupVisible(true);
                    }
                }
            }
        });
//        } catch (IOException ex) {
//            Logger.getLogger(BillOfSupplyUI.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {

        menuProduct.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
//        menuCurrentStock.setVisible(companyUserRole.isInventoryView());
//        menuSales.setVisible(companyUserRole.isSalesCreate());
//        menuSalesReport.setVisible(companyUserRole.isSalesView());
//        menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
//        menuPurchaseReport.setVisible(companyUserRole.isPurchaseView());
//        menuSalesAndReturn.setVisible(companyUserRole.isCrdrNoteView());
//        menuPurchaseReturn.setVisible(companyUserRole.isCrdrNoteView());
//        menuCurrentStock.setVisible(companyUserRole.isInventoryView());
//        menuAddStock.setVisible(companyUserRole.isInventoryAdd());
//        menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
//        menuLedger.setVisible(companyUserRole.isAccountsLedger());
//        menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
//        menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
//        menuCash.setVisible(companyUserRole.isAccountsCash());
//        menuBank.setVisible(companyUserRole.isAccountsBank());
//        menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
    }

    public void EditOldBill(String id) {
        isOldBill = true;
        oldBos = new BillOfSupplyLogic().readABOS(id);
        List<BillOfSupplyLineItem> bosLineItem = oldBos.getBosLineItems();
        List<BillOfSupplyChargeItem> bosChargeItem = oldBos.getBosChargeItems();
        LedgerId = oldBos.getLedger().getLedgerId();
        comboParty.setSelectedItem(oldBos.getLedger().getLedgerName());
        txtPartyName.setText(oldBos.getLedger().getLedgerName());
        String address = oldBos.getLedger().getAddress();
        String[] words = address.split("/~/");
        txtAddr1.setText(words[0]);
        txtAddr2.setText(words[1]);
        txtAddr3.setText(words[2]);
        txtCity.setText(oldBos.getLedger().getCity());
        txtDistrict.setText(oldBos.getLedger().getDistrict());
        comboState.setSelectedItem(oldBos.getLedger().getState());
//        txtEmail.setText(oldBos.getLedger().getEmail());
        txtMobile.setText(oldBos.getLedger().getMobile());
//        txtPhone.setText(oldBos.getLedger().getPhone());
        txtGSTIN.setText(oldBos.getLedger().getGSTIN());
//        txtThrough.setText(oldSale.getThrough());
//        txtReferenceNumber.setText(oldBos.getReferenceNumber());
//        isIGST = oldSale.isIsIGST();

        txtRemarks.setText(oldBos.getRemarks());
//        messageCount();

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (BillOfSupplyLineItem item : bosLineItem) {

            Object[] row = {
                item.getLineNumber(),
                item.getProductName(),
                item.getHsnSac(),
                item.getUqcOne(),
                item.getUqcOneQuantity(),
                item.getUqcOneRate(),
                (new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate())).floatValue(),
                item.getUqcTwo(),
                item.getUqcTwoQuantity(),
                item.getUqcTwoRate(),
                (new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate())).floatValue(),
                item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                item.getStockQuantity(),
                item.getDiscountPercent(),
                item.getDiscountValue().floatValue(),
                item.getValue().floatValue(),
                item.getUqc1BaseRate().floatValue(),
                item.getUqc2BaseRate().floatValue(),
                item.getProfit().floatValue(),
                false};
            model.addRow(row);
        }
        for (BillOfSupplyChargeItem item : oldBos.getBosChargeItems()) {
            Object[] row = {null, item.getBillChargeName(), null, null, null, null, null,
                null, null, null, null, null, null, null, null, item.getValue(), null, null, null, true};
            model.addRow(row);
        }
        jTableRender();
        TotalCalculate();

    }

    public String setStockConverstion(int qty, String UQC1, String UQC2, int UQC2Value) {
        int UQC1Count = qty / UQC2Value;
        int UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void getProductDetails() {

        labelUQC2.setEnabled(true);
        txtUQC2Qty.setEnabled(true);
        labelUQC2forPrice.setEnabled(true);
        txtUQC2Rate.setEnabled(true);
        txtUQC2BaseRate.setEnabled(true);
        labelUQC2BasePrice.setEnabled(true);
        String productName = (String) comboProduct.getEditor().getItem();

        if ((productName != null) && (!(productName.equals("")))) {
            for (Product product : products) {
                if (product.getName().equals(productName)) {

                    labelUQC1.setText(product.getUQC1());
                    labelUQC1forPrice.setText(product.getUQC1());
                    labelUQC2.setText(product.getUQC2());
                    labelUQC2forPrice.setText(product.getUQC2());
                    labelUQC1BasePrice.setText(product.getUQC1());
                    labelUQC2BasePrice.setText(product.getUQC2());
//                    }

                    if (labelUQC1.getText().equals(labelUQC2.getText())) {
                        labelUQC2.setEnabled(false);
                        txtUQC2Qty.setEnabled(false);
                        labelUQC2forPrice.setEnabled(false);
                        txtUQC2Rate.setEnabled(false);
                        txtUQC2BaseRate.setEnabled(false);
                        labelUQC2BasePrice.setEnabled(false);
                    } else {
                        labelUQC2.setEnabled(true);
                        txtUQC2Qty.setEnabled(true);
                        labelUQC2forPrice.setEnabled(true);
                        txtUQC2Rate.setEnabled(true);
                        txtUQC2BaseRate.setEnabled(true);
                        labelUQC2BasePrice.setEnabled(true);
                    }
                    HSNCode = product.getHsnCode();
                    UQC2Value = product.getUQC2Value();

                    BaseProductRate = product.getBaseProductRate().floatValue();

                    isBasePriceProductTaxInclusive = product.isBasePriceInclusiveOfGST();

                    float basePrice = 0;
//                    if (isBasePriceProductTaxInclusive) {
//                        basePrice = (BaseProductRate / (100 + IGSTper)) * 100;
//                    } else {
                    basePrice = BaseProductRate;
//                    }
                    txtUQC1BaseRate.setText(currency.format(basePrice * UQC2Value));
                    txtUQC2BaseRate.setText(currency.format(basePrice));

                    ///////////Price Policy set//////////////////////////////////
//                    String nameOfPolicy = (String) comboPricingPolicy.getSelectedItem();
//                    if (nameOfPolicy.equals("Default")) {
                    ProductRate = product.getProductRate().floatValue();
                    isProductTaxInclusive = product.isInclusiveOfGST();
                    float price = 0;
//                        if (isProductTaxInclusive) {
//                            price = (ProductRate / (100 + IGSTper)) * 100;
//                        } else {
                    price = ProductRate;
//                        }
                    txtUQC1Rate.setText(currency.format(price * UQC2Value));
                    txtUQC2Rate.setText(currency.format(price));
                    txtUQC1Qty.requestFocus();
//                    } else {
//                        Boolean isNotHere = true;
//                        for (SingleProductPolicy productPolicy : recentSingleProductPolicies) {
//                            if (productPolicy.getProduct().getName().equals(productName)) {
//                                float uqc1Rate = productPolicy.getUqc1Rate().floatValue(), uqc2Rate = productPolicy.getUqc2Rate().floatValue();
//                                isProductTaxInclusive = productPolicy.isInclusiveOfGST();
//                                float Uqc1price = 0, Uqc2price = 0;
////                                if (isProductTaxInclusive) {
////                                    Uqc1price = (uqc1Rate / (100 + IGSTper)) * 100;
////                                    Uqc2price = (uqc2Rate / (100 + IGSTper)) * 100;
////                                } else {
//                                Uqc1price = uqc1Rate;
//                                Uqc2price = uqc2Rate;
////                                }
//                                txtUQC1Rate.setText(currency.format(Uqc1price));
//                                txtUQC2Rate.setText(currency.format(Uqc2price));
//                                txtUQC1Qty.requestFocus();
//                                isNotHere = false;
//                            }
//                        }
//                        if (isNotHere) {
//                            JOptionPane.showMessageDialog(null, productName + " product is not available in " + nameOfPolicy, "Alert", JOptionPane.WARNING_MESSAGE);
//
//                            ProductRate = product.getProductRate().floatValue();
//                            isProductTaxInclusive = product.isInclusiveOfGST();
//                            float price = 0;
////                            if (isProductTaxInclusive) {
////                                price = (ProductRate / (100 + IGSTper)) * 100;
////                            } else {
//                            price = ProductRate;
////                            }
//                            txtUQC1Rate.setText(currency.format(price * UQC2Value));
//                            txtUQC2Rate.setText(currency.format(price));
//                            txtUQC1Qty.requestFocus();
//
//                        }
//                    }
                    ///////////Price Policy set//////////////////////////////////
                    productStock = product.getCurrentStock();

//                    if (isOldBill) {
//                        List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
//                        for (PurchaseSaleLineItem item : psLIs) {
//                            if (item.getProductName().equals(productName)) {
//                                productStock = productStock + item.getStockQuantity();
//                            }
//                        }
//                    }
//                    labelStock.setText(setStockConverstion(productStock, product.getUQC1(), product.getUQC2(), product.getUQC2Value()));
                }
            }

        } else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtUQC1Rate.setText("");
//            txtUQC2Rate.setText("");
//            txtAmount.setText("");
//            txtIGSTper.setText("");
//            txtCGSTper.setText("");
//            txtSGSTper.setText("");
//            txtIGSTamt.setText("");
//            txtCGSTamt.setText("");
//            txtSGSTamt.setText("");
            labelStock.setText("0");
            comboProduct.setSelectedIndex(0);
            comboProduct.setPopupVisible(true);

            comboProduct.requestFocus();

        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");

    }

    public void setParty() {
        comboParty.addItem("No Party");
        comboParty.addItem("New Party");
        for (Ledger ledger : sortedLedgers) {
            comboParty.addItem(ledger.getLedgerName());
        };
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        comboProduct = new javax.swing.JComboBox<>();
        labelProduct = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        labelStock = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelBillAmount = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtPartyName = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtAddr1 = new javax.swing.JTextField();
        txtAddr2 = new javax.swing.JTextField();
        txtAddr3 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        comboState = new javax.swing.JComboBox<>();
        jLabel26 = new javax.swing.JLabel();
        txtMobile = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtGSTIN = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtRemarks = new javax.swing.JTextField();
        txtUQC1BaseRate = new javax.swing.JTextField();
        labelBasePrice = new javax.swing.JLabel();
        labelUQC1BasePrice = new javax.swing.JLabel();
        txtUQC2BaseRate = new javax.swing.JTextField();
        labelUQC2BasePrice = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtUQC1Rate = new javax.swing.JTextField();
        labelUQC1forPrice = new javax.swing.JLabel();
        txtUQC2Rate = new javax.swing.JTextField();
        labelUQC2forPrice = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtBillDiscount = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp2 = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bill Of supply");

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboProductFocusGained(evt);
            }
        });
        comboProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboProductKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboProductKeyTyped(evt);
            }
        });

        labelProduct.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelProduct.setText("Product ");
        labelProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                labelProductKeyReleased(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnPrint.setMnemonic('p');
        btnPrint.setText("Save & Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('l');
        btnClear.setText("Clear ");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel35.setText("Quantity");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Discount %", "Discount", "Amount", "UQC1BaseRate", "UQC2BaseRate", "Profit", "isBillofCharge"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(40);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(40);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(40);
            jTable1.getColumnModel().getColumn(1).setMinWidth(400);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(400);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(400);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(13).setMinWidth(75);
            jTable1.getColumnModel().getColumn(13).setPreferredWidth(75);
            jTable1.getColumnModel().getColumn(13).setMaxWidth(75);
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
        }

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        labelUQC2.setText("UQC2");
        labelUQC2.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        labelUQC1.setText("UQC1");
        labelUQC1.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel1.setText("Available Stock ");

        labelStock.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelStock.setText("N/A");

        jLabel9.setText("Bill Amount");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Date :");

        dateBill.setDateFormatString("dd-MM-yyyy");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Party ");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPartyActionPerformed(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboPartyKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        txtPartyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartyNameActionPerformed(evt);
            }
        });
        txtPartyName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartyNameKeyPressed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr1ActionPerformed(evt);
            }
        });
        txtAddr1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddr1KeyPressed(evt);
            }
        });

        txtAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr2ActionPerformed(evt);
            }
        });
        txtAddr2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddr2KeyPressed(evt);
            }
        });

        txtAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr3ActionPerformed(evt);
            }
        });
        txtAddr3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddr3KeyPressed(evt);
            }
        });

        jLabel20.setText("City");

        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });
        txtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCityKeyPressed(evt);
            }
        });

        jLabel25.setText("District");

        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });
        txtDistrict.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDistrictKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        comboState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboStateActionPerformed(evt);
            }
        });
        comboState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboStateKeyPressed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("Mobile");

        txtMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileActionPerformed(evt);
            }
        });
        txtMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMobileKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel21.setText("GSTIN/UIN");

        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });
        txtGSTIN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtGSTINKeyPressed(evt);
            }
        });

        jLabel10.setText("Remarks");

        txtRemarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRemarksActionPerformed(evt);
            }
        });

        txtUQC1BaseRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1BaseRateActionPerformed(evt);
            }
        });
        txtUQC1BaseRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1BaseRateKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1BaseRateKeyTyped(evt);
            }
        });

        labelBasePrice.setText("Base Price");

        labelUQC1BasePrice.setText("/ UQC1");
        labelUQC1BasePrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1BasePrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1BasePrice.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2BaseRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2BaseRateActionPerformed(evt);
            }
        });
        txtUQC2BaseRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2BaseRateKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2BaseRateKeyTyped(evt);
            }
        });

        labelUQC2BasePrice.setText("/ UQC2");
        labelUQC2BasePrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2BasePrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2BasePrice.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel3.setText("Price");

        txtUQC1Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateActionPerformed(evt);
            }
        });
        txtUQC1Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyTyped(evt);
            }
        });

        labelUQC1forPrice.setText("/ UQC1");
        labelUQC1forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateActionPerformed(evt);
            }
        });
        txtUQC2Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyTyped(evt);
            }
        });

        labelUQC2forPrice.setText("/ UQC2");
        labelUQC2forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel5.setText("Discount %");

        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        jLabel27.setText("Disount");

        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        jLabel13.setText("Amount");

        txtAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel4.setText("Bill Discount");

        txtBillDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillDiscountActionPerformed(evt);
            }
        });
        txtBillDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyTyped(evt);
            }
        });

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('t');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuBar.add(menuGST);

        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setText("Help");

        menuHelp2.setText("Keyboard Shortcuts");
        menuHelp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelp2ActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp2);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelProduct)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(27, 27, 27)
                                        .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(labelBasePrice))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel5)))
                                .addGap(2, 2, 2)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtDiscountPer)
                                    .addComponent(txtUQC1BaseRate, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(199, 199, 199)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBillDiscount, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelUQC1BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtUQC2BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelBillAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelUQC2BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel25)
                                            .addComponent(jLabel17))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(comboState, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel26)
                                            .addComponent(jLabel21)
                                            .addComponent(jLabel10))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtGSTIN)
                                            .addComponent(txtRemarks, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 19, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16)
                        .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel20)
                        .addComponent(jLabel26)
                        .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dateBill, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel25))
                            .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel21)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)
                            .addComponent(comboState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(txtRemarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelBillAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelProduct)
                    .addComponent(comboProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(txtUQC1BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC1BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUQC2BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC2BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelBasePrice))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelStock))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5)
                                .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel27)
                                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPartyActionPerformed
//        getPartyDetails();
    }//GEN-LAST:event_comboPartyActionPerformed

    public void getPartyDetails() {
        String name = (String) comboParty.getSelectedItem();
        name = name.replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (name == null) {
            name = "";
        }
        if (name.equals("")) {
            txtPartyName.setText("");
            txtAddr1.setText("");
            txtAddr2.setText("");
            txtAddr3.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            comboState.setSelectedItem("Tamil Nadu");
//            txtEmail.setText("");
            txtMobile.setText("");
//            txtPhone.setText("");
            txtGSTIN.setText("");
        } else {
            for (Ledger ledger : sortedLedgers) {
                if (ledger.getLedgerName().equals(name)) {
//                    ledgerData = ledger;
                    LedgerId = ledger.getLedgerId();
                    txtPartyName.setText(ledger.getLedgerName());
                    String address = ledger.getAddress();
                    String[] words = address.split("/~/");
                    txtAddr1.setText(words[0]);
                    txtAddr2.setText(words[1]);
                    txtAddr3.setText(words[2]);
                    txtCity.setText(ledger.getCity());
                    txtDistrict.setText(ledger.getDistrict());
                    comboState.setSelectedItem(ledger.getState());
//                    txtEmail.setText(ledger.getEmail());
                    txtMobile.setText(ledger.getMobile());
//                    txtPhone.setText(ledger.getPhone());
                    txtGSTIN.setText(ledger.getGSTIN());

                    String selectedState = (String) comboState.getSelectedItem();
                    isIGST = !(selectedState.equals(CompanyState));

                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    if (model.getRowCount() > 0) {
                        model.setRowCount(0);
                    }
//                    DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
//                    if (model2.getRowCount() > 0) {
//                        model2.removeRow(0);
//                    }
                    setTable();
                    checkMobile();

                    break;

                } else {
                    txtPartyName.setText("");
                    txtAddr1.setText("");
                    txtAddr2.setText("");
                    txtAddr3.setText("");
                    txtCity.setText("");
                    txtDistrict.setText("");
                    comboState.setSelectedItem("Tamil Nadu");
//            txtEmail.setText("");
                    txtMobile.setText("");
//            txtPhone.setText("");
                    txtGSTIN.setText("");
                }
            };
        }
        if (name.equalsIgnoreCase("no party")) {
            txtPartyName.requestFocus();

        } else if (name.equalsIgnoreCase("new party")) {
            txtPartyName.requestFocus();
        } else {
            comboProduct.requestFocus();
        }

    }

    public void checkMobile() {
        Boolean isCorrect = true;
        if (txtMobile.getText().equals("") || txtMobile.getText().length() != 10) {
            isCorrect = false;
        }
    }


    private void txtPartyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartyNameActionPerformed
        txtAddr1.requestFocus();
    }//GEN-LAST:event_txtPartyNameActionPerformed

    private void txtAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr1ActionPerformed
        txtAddr2.requestFocus();
    }//GEN-LAST:event_txtAddr1ActionPerformed

    private void txtAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr2ActionPerformed
        txtAddr3.requestFocus();
    }//GEN-LAST:event_txtAddr2ActionPerformed

    private void txtAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr3ActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddr3ActionPerformed
    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(14).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(16).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(17).setCellRenderer(new FloatValueTableCellRenderer());
//        jTable1.getColumnModel().getColumn(25).setCellRenderer(new FloatValueTableCellRenderer());

//        totalTable.getColumnModel().getColumn(0).setCellRenderer(new FloatValueTableCellRenderer());
//        totalTable.getColumnModel().getColumn(1).setCellRenderer(new FloatValueTableCellRenderer());
//        totalTable.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
//        totalTable.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
//        totalTable.getColumnModel().getColumn(4).setCellRenderer(new FloatValueTableCellRenderer());
//        totalTable.getColumnModel().getColumn(5).setCellRenderer(new FloatValueTableCellRenderer());
    }

    public void TotalCalculate() {
        float totalAmount = 0;

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
//        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
//        if (model2.getRowCount() > 0) {
//            model2.removeRow(0);
//        }

        for (int i = 0; i < model.getRowCount(); i++) {
            totalAmount = Float.parseFloat(model.getValueAt(i, 15).toString()) + totalAmount;
        }
        labelBillAmount.setText(currency.format(totalAmount));
        amountWithoutDiscount = Float.parseFloat(labelBillAmount.getText());
    }
    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void txtMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileActionPerformed
        txtGSTIN.requestFocus();
    }//GEN-LAST:event_txtMobileActionPerformed

    private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed
        txtRemarks.requestFocus();
    }//GEN-LAST:event_txtGSTINActionPerformed
    public void setTable() {
//        jTable1.getColumnModel().getColumn(16).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(16).setMaxWidth(100);
//        jTable1.getColumnModel().getColumn(17).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(17).setMaxWidth(100);
//
//        jTable1.getColumnModel().getColumn(18).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(18).setMaxWidth(100);
//        jTable1.getColumnModel().getColumn(19).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(19).setMaxWidth(100);
//        jTable1.getColumnModel().getColumn(20).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(20).setMaxWidth(100);
//        jTable1.getColumnModel().getColumn(21).setMinWidth(100);
//        jTable1.getColumnModel().getColumn(21).setMaxWidth(100);

//        totalTable.getColumnModel().getColumn(0).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(0).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(1).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(1).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(2).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(2).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(3).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(3).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(4).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
//        totalTable.getColumnModel().getColumn(5).setMinWidth(0);
//        totalTable.getColumnModel().getColumn(5).setMaxWidth(0);
//        if (isIGST) {
//            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);
//
//            totalTable.getColumnModel().getColumn(0).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(1).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(1).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(5).setMinWidth(400);
//            totalTable.getColumnModel().getColumn(5).setMaxWidth(400);
//
//        } else {
//            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
//            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
//            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);
//
//            totalTable.getColumnModel().getColumn(0).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(2).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(2).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(3).setMinWidth(300);
//            totalTable.getColumnModel().getColumn(3).setMaxWidth(300);
//            totalTable.getColumnModel().getColumn(5).setMinWidth(400);
//            totalTable.getColumnModel().getColumn(5).setMaxWidth(400);
//
//        }
    }
    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        comboState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    public void TaxvalueCalculation() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("") || !strUQC2rate.equals(""))) && ((!strUQC1qty.equals("") || !strUQC2qty.equals("")))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1qty.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2qty.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }

            if ((UQC2qty / UQC2Value) > 0) {
                UQC1qty += UQC2qty / UQC2Value;
                UQC2qty = UQC2qty % UQC2Value;
                txtUQC1Qty.setText(UQC1qty + "");
                txtUQC2Qty.setText(UQC2qty + "");
            }
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscountPer.equals("")) {
                DiscountPer = Float.parseFloat(strDiscountPer);
                Discount = (TotalAmount * DiscountPer) / 100;
                TotalAmount = TotalAmount - Discount;
                txtDiscount.setText(currency.format(Discount));
            } else {
                txtDiscount.setText("");
            }
//            float TaxValue = TotalAmount * (IGSTper / 100);
            txtAmount.setText(currency.format(TotalAmount));
//            if (isIGST) {
//                txtIGSTamt.setText(currency.format(TaxValue));
//                txtCGSTamt.setText("0.00");
//                txtSGSTamt.setText("0.00");
//            } else {
//                txtIGSTamt.setText("0.00");
//                txtCGSTamt.setText(currency.format(TaxValue / 2));
//                txtSGSTamt.setText(currency.format(TaxValue / 2));
//            }
        } else {
            txtAmount.setText("0.00");

//            txtIGSTamt.setText("0.00");
//            txtCGSTamt.setText("0.00");
//            txtSGSTamt.setText("0.00");
//
//            txtIGSTamt.setText("0.00");
//            txtCGSTamt.setText("0.00");
//            txtSGSTamt.setText("0.00");
        }
    }

    public void clear(boolean bBtnClear) {
        labelUQC2.setEnabled(true);
        txtUQC2Qty.setEnabled(true);
        labelUQC2forPrice.setEnabled(true);
        txtUQC2Rate.setEnabled(true);
        txtUQC2BaseRate.setEnabled(true);
        labelUQC2BasePrice.setEnabled(true);
        txtUQC1Qty.setText("");
        txtUQC2Qty.setText("");
        txtUQC1Rate.setText("");
        txtUQC2Rate.setText("");
        txtUQC1BaseRate.setText("");
        txtUQC2BaseRate.setText("");
//        txtIGSTper.setText("");
//        txtCGSTper.setText("");
//        txtSGSTper.setText("");
//        txtIGSTamt.setText("");
//        txtCGSTamt.setText("");
//        txtSGSTamt.setText("");
        txtAmount.setText("");
        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("UQC1");
        labelUQC2.setText("UQC2");
        labelUQC1forPrice.setText("UQC1");
        labelUQC2forPrice.setText("UQC2");
        comboProduct.setSelectedItem("");
        if (bBtnClear) {
            jTable1.clearSelection();
        }
        comboProduct.requestFocus();
        labelStock.setText("0");
        btnAdd.setEnabled(true);
    }

    public void SaveAndPrint(Boolean isPrint) throws ParseException {
        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);

        if (!isInBetweenFinancialYear) {
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            if (isOldBill) {

                OldPrintAndSaveAction(isPrint);
            } else {
                NewPrintAndSaveAction(isPrint);
            }
        }
    }

    public void OldPrintAndSaveAction(Boolean isPrint) throws ParseException {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = findLedgerData();
        if (ledgerData != null) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            int rowCount = model.getRowCount();
            if (rowCount > 0) {
                String value = "0.00";
                if (value.equals("")) {
                    value = "0.00";
                }
                for (int i = 0; i < rowCount; i++) {
                    if (model.getValueAt(i, 3) != null) {
                        billprofit = billprofit + Float.parseFloat(model.getValueAt(i, 18).toString());
                    }
                }
                float roundOff = Float.parseFloat(value);

                oldBos.setCompany(SessionDataUtil.getSelectedCompany());
                oldBos.setBosDate(dateBill.getDate());
                oldBos.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
                oldBos.setCancelled(false);

                oldBos.setLedger(ledgerData);
                oldBos.setPaymentType("Cash");//Payment Type
                oldBos.setRemarks(txtRemarks.getText());
                oldBos.setModeOfDelivery("");
                oldBos.setDiscountAmount(convertDecimal.Currency(txtBillDiscount.getText()));
                oldBos.setDiscountPercent(0);
//                String strAmountPaid = txtAmountPaid.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                if (strAmountPaid.equals("")) {
//                    strAmountPaid = "0";
//                }
//                oldBos.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));
                oldBos.setTotalAmountPaid(convertDecimal.Currency("0"));
                oldBos.setVariationAmount(convertDecimal.Currency("0"));
//                oldBos.setReferenceNumber(txtReferenceNumber.getText());
                oldBos.setReferenceNumber("");
//                oldBos.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
                oldBos.setPricingPolicyName("");
                oldBos.setSmsMessage(txtRemarks.getText());
                oldBos.setBillAmount(new BigDecimal(labelBillAmount.getText()));
                oldBos.setProfit(new BigDecimal(billprofit));

                List<BillOfSupplyLineItem> newBosLineItems = new ArrayList<BillOfSupplyLineItem>();
                List<BillOfSupplyChargeItem> newBosChargeItems = new ArrayList<BillOfSupplyChargeItem>();
                Long count = 1L;
                for (int i = 0; i < rowCount; i++) {
                    if (model.getValueAt(i, 3) != null) {
//                    billprofit = billprofit + Float.parseFloat(model.getValueAt(i, 18).toString());
                        BillOfSupplyLineItem bosLineItem = new BillOfSupplyLineItem();
                        bosLineItem.setLineNumber((int) model.getValueAt(i, 0));
                        bosLineItem.setProductName((String) model.getValueAt(i, 1));
                        bosLineItem.setHsnSac((String) model.getValueAt(i, 2));
                        product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                        bosLineItem.setProduct(product);

                        bosLineItem.setUqcOne((String) model.getValueAt(i, 3));
                        bosLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                        bosLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                        bosLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                        bosLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                        bosLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                        bosLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                        bosLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                        bosLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                        bosLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                        bosLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                        bosLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));

//           
                        bosLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 16).toString()));
                        bosLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                        bosLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 18).toString()));

                        newBosLineItems.add(bosLineItem);
                    } else {
                        BillOfSupplyChargeItem bosChargeItem = new BillOfSupplyChargeItem();
                        bosChargeItem.setBillChargeName((String) model.getValueAt(i, 1).toString());
                        bosChargeItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                        newBosChargeItems.add(bosChargeItem);
                    }

                }

//            EventStatus result = new PurchaseSaleLogic().createAPS("ESTIMATE", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);
                EventStatus result = new BillOfSupplyLogic().updateBOS(oldBos, newBosLineItems, newBosChargeItems);

                if (result.isUpdateDone()) {
                    if (isPrint) {
                        new NonGSTBill().pdfByBillId(true, false, oldBos.getBosId());
                    }
                    JOptionPane.showMessageDialog(null, "Old Bill has updated Successful", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clearAll();
//                    clearBillCharge(false);
                    new BillofSupplyDateWiseReport().setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please add atleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Choose Party in the Party List ", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void NewPrintAndSaveAction(Boolean isPrint) {

        Ledger ledgerData = null;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {
            billAmount = Float.parseFloat(labelBillAmount.getText());
            String value = "0.00";
            value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(billAmount));
            if (value.equals("")) {
                value = "0.00";
            }
            float roundOff = Float.parseFloat(value);
            billAmount = billAmount + roundOff;
            for (int i = 0; i < rowCount; i++) {
                if (model.getValueAt(i, 3) != null) {
                    billprofit = billprofit + Float.parseFloat(model.getValueAt(i, 18).toString());
                }
            }

            BillOfSupply newBos = new BillOfSupply();
            String partyName = (String) comboParty.getSelectedItem();
            partyName = partyName.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            Boolean isApproved = false;
            switch (partyName) {
                case "No Party":
                    isApproved = true;
                    break;
                case "New Party":
                    Boolean isSuccess = addNewPartyDetails();
                    if (isSuccess) { //Check this
//                    JOptionPane.showMessageDialog(null, "New Party is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                        ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
                        sortedLedgers = new ArrayList<>(ledgers);
                        Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                            @Override
                            public int compare(Ledger o1, Ledger o2) {
                                return o1.getLedgerName().compareTo(o2.getLedgerName());
                            }
                        });
                        setParty();
                        ledgerData = findLedgerData();
                        isApproved = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "New Party is not to be added", "Alert", JOptionPane.WARNING_MESSAGE);
                        isApproved = false;
                    }
                    break;
                default:
                    ledgerData = findLedgerData();
                    isApproved = true;
                    break;
            }
            if (isApproved) {
                newBos.setLedger(ledgerData);
                newBos.setCompany(SessionDataUtil.getSelectedCompany());
                newBos.setBosDate(dateBill.getDate());
                newBos.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
                newBos.setCancelled(false);

                newBos.setPaymentType("Cash");//Payment Type
                newBos.setRemarks(txtRemarks.getText());
                newBos.setModeOfDelivery("");

                newBos.setDiscountAmount(convertDecimal.Currency(labelBillAmount.getText()));
                newBos.setDiscountPercent(0);
//            String strAmountPaid = txtAmountPaid.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (strAmountPaid.equals("")) {
//                strAmountPaid = "0";
//            }
//            newBos.setTotalAmountPaid(convertDecimal.Currency(strAmountPaid));    
                newBos.setTotalAmountPaid(convertDecimal.Currency("0"));
                newBos.setVariationAmount(convertDecimal.Currency("0"));
//          newBos.setReferenceNumber(txtReferenceNumber.getText());
                newBos.setReferenceNumber("");
//          newBos.setPricingPolicyName((String) comboPricingPolicy.getSelectedItem());
                newBos.setPricingPolicyName("");
                newBos.setSmsMessage(txtRemarks.getText());
                newBos.setBillAmount(new BigDecimal(billAmount));

                newBos.setProfit(new BigDecimal(billprofit));

                List<BillOfSupplyLineItem> bosLineItems = new ArrayList<>();
                List<BillOfSupplyChargeItem> bosChargeItems = new ArrayList<>();
                Long count = 1L;
                for (int i = 0; i < rowCount; i++) {
                    if (model.getValueAt(i, 3) != null) {
//                    billprofit = billprofit + Float.parseFloat(model.getValueAt(i, 18).toString());
                        BillOfSupplyLineItem bosLineItem = new BillOfSupplyLineItem();
                        bosLineItem.setLineNumber((int) model.getValueAt(i, 0));
                        bosLineItem.setProductName((String) model.getValueAt(i, 1));
                        bosLineItem.setHsnSac((String) model.getValueAt(i, 2));
                        product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                        bosLineItem.setProduct(product);

                        bosLineItem.setUqcOne((String) model.getValueAt(i, 3));
                        bosLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                        bosLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                        bosLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                        bosLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                        bosLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                        bosLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                        bosLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                        bosLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                        bosLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                        bosLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                        bosLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));

//           
                        bosLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 16).toString()));
                        bosLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                        bosLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 18).toString()));

                        bosLineItems.add(bosLineItem);
                    } else {
                        BillOfSupplyChargeItem bosChargeItem = new BillOfSupplyChargeItem();
                        bosChargeItem.setBillChargeName((String) model.getValueAt(i, 1).toString());
                        bosChargeItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                        bosChargeItems.add(bosChargeItem);
                    }

                }

//            EventStatus result = new PurchaseSaleLogic().createAPS("ESTIMATE", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);
                EventStatus result = new BillOfSupplyLogic().createBOS(newBos, bosLineItems, bosChargeItems);

                if (result.isCreateDone()) {
                    if (isPrint) {
                        new NonGSTBill().pdfByBillId(true, false, result.getBillID());
                    }
                    JOptionPane.showMessageDialog(null, "New Bill has created Successful", "Message", JOptionPane.INFORMATION_MESSAGE);
                    clearAll();
//                    clearBillCharge(false);
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Bill is Canceled", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please add atleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public Boolean checkSamePartyName(String name) {
        Boolean isFound = false;
        for (Ledger ledger : ledgers) {
            if (ledger.getLedgerName().equals(name)) {
                isFound = true;
            }
        }
        return isFound;
    }

    public Boolean addNewPartyDetails() {
        Boolean isSuccess = false;

        String strPartyName = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isAlreadyHere = checkSamePartyName(strPartyName);
        String strMobileNumber = txtMobile.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        Boolean isMobileValid = false;
        if (!strMobileNumber.equals("")) {
            isMobileValid = formValidation.checkMobileNumber(strMobileNumber);
        }

        int errorReport = formValidation.checkGSTIN(txtGSTIN.getText());
//        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate());
        if (strPartyName.equals("")) {
            JOptionPane.showMessageDialog(null, "please enter the Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        } else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strPartyName + " is already here.", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        } else if (isMobileValid) {
            JOptionPane.showMessageDialog(null, "Please enter valid Mobile No", "Alert", JOptionPane.WARNING_MESSAGE);
            txtMobile.requestFocus();
        } else if (errorReport != 0) {
            if (errorReport == 1) {
                JOptionPane.showMessageDialog(null, "First two digits must be a State Code", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            } else if (errorReport == 2) {
                JOptionPane.showMessageDialog(null, "One Digit Prior to the last must be Z", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            } else if (errorReport == 3) {
                JOptionPane.showMessageDialog(null, "Invalid GSTIN Number", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            }
        } //        else if (!isInBetweenFinancialYear) {
        //            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
        //                    + " to <b>" + strFinancialYearEnd + "</b></html>";
        //            JLabel label = new JLabel(msg);
        //            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        //        } 
        else {
            String addr1 = txtAddr1.getText(), addr2 = txtAddr2.getText(), addr3 = txtAddr3.getText();
            if (addr1.equals("")) {
                addr1 = " ";
            }
            if (addr2.equals("")) {
                addr2 = " ";
            }
            if (addr3.equals("")) {
                addr3 = " ";
            }
            String Address = addr1 + "/~/" + addr2 + "/~/" + addr3;
//            Date d = dateOpeningBalance.getDate();
//            float openingAmount = 0;
//            String stropeningAmount = txtOpeningBalance.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!stropeningAmount.equals("")) {
//                openingAmount = Float.parseFloat(currency.format(Float.parseFloat(stropeningAmount)));
//                if (comboTransactionType.getSelectedItem().equals("Debit")) {
//                    openingAmount = (-1) * openingAmount;
//                }
//            }
            Ledger newLedger = new Ledger();

            newLedger.setLedgerName(strPartyName);
            newLedger.setAddress(Address);
            newLedger.setCity(txtCity.getText());
            newLedger.setDistrict(txtDistrict.getText());
            newLedger.setState((String) comboState.getSelectedItem());
            newLedger.setEmail("");
            newLedger.setPhone("");
            newLedger.setMobile(txtMobile.getText());
            newLedger.setGSTIN(txtGSTIN.getText());
            newLedger.setOpeningBalanceDate(new Date());
            newLedger.setOpeningBalance(new BigDecimal(0));

            isSuccess = new LedgerLogic().createPartyLedger(companyId, newLedger, false);

        }
        return isSuccess;
    }

    public void clearAll() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        comboParty.setSelectedItem("");
        txtPartyName.setText("");
        txtAddr1.setText("");
        txtAddr2.setText("");
        txtAddr3.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        comboState.setSelectedItem("Tamil Nadu");
//        txtEmail.setText("");
        txtMobile.setText("");
//        txtPhone.setText("");
        txtGSTIN.setText("");
//        txtReferenceNumber.setText("");
        txtRemarks.setText("");
        txtBillDiscount.setText("");
        labelBillAmount.setText("");
//        messageCount();
//        checkSMS.setSelected(true);
//        comboPricingPolicy.setSelectedItem("Default");

        clear(true);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
//        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
//        if (model2.getRowCount() > 0) {
//            model2.removeRow(0);
//        }
    }

    public Boolean isProductHereInTable(String productName, int position) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Boolean isProductAlreadyHere = false;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (i != position) {
                if (model.getValueAt(i, 1).toString().equals(productName)) {
                    isProductAlreadyHere = true;
                }
            }
        }
        return isProductAlreadyHere;
    }

    public void addProductList() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1Baserate = txtUQC1BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2Baserate = txtUQC2BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String productName = (String) comboProduct.getSelectedItem();
        productName = productName.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Boolean isProductAlreadyHere = isProductHereInTable(productName, -1);
        if (productName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please add UQC quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Rate.requestFocus();
        } else if (isProductAlreadyHere) {
            JOptionPane.showMessageDialog(null, productName + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else {

            int count = 1;
            int wholeQuantity = 0;

            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if (!strUQC1Baserate.equals("")) {
                UQC1Baserate = Float.parseFloat(strUQC1Baserate);
            }
            if (!strUQC2Baserate.equals("")) {
                UQC2Baserate = Float.parseFloat(strUQC2Baserate);
            }

            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
            float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                    //                    IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                    //                    CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                    //                    SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                    Amount = Float.parseFloat(txtAmount.getText());
            String Discountper = "0.00", Discount = "0.00";
            if (!txtDiscountPer.getText().equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            }
            if (!txtDiscount.getText().equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            }

            float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
            Profit = Profit - Float.parseFloat(Discount);
            Float TotalAmoutInclTax = Amount;// + IGST_amt + CGST_amt + SGST_amt;
            billAmount = billAmount + Amount;
            int pos = 0;
            for (int y = 0; y < model.getRowCount(); y++) {

                if ((Boolean) model.getValueAt(y, 19)) {
                    pos = y;
                    break;
                } else {
                    count++;
                    pos++;

                }
            }

            // if (wholeQuantity <= productStock) {
            Object[] row = {count, productName, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(),
                wholeQuantity, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount,
                UQC1Baserate, UQC2Baserate, Profit, false};
            model.insertRow(pos, row);
//                lblBillAmount.setText(String.valueOf(billAmount));
            clear(true);
            TotalCalculate();
            jTableRender();
            comboProduct.requestFocus();
            // } 
//            else {
//                JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
//            }

        }
    }

    public void updateProductsItems() {

        int i = jTable1.getSelectedRow();
        if (i >= 0) {

            String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC1Baserate = txtUQC1BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC2Baserate = txtUQC2BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String productName = (String) comboProduct.getSelectedItem();
            productName = productName.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            Boolean isProductAlreadyHere = isProductHereInTable(productName, i);

            if (productName.equals("")) {
                JOptionPane.showMessageDialog(null, "Please select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please add UQC quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Rate.requestFocus();
            } else if (isProductAlreadyHere) {
                JOptionPane.showMessageDialog(null, productName + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else {
                int count = jTable1.getRowCount() + 1;
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strUQC2rate.equals("")) {
                    UQC2rate = Float.parseFloat(strUQC2rate);
                }
                if (!strUQC1Baserate.equals("")) {
                    UQC1Baserate = Float.parseFloat(strUQC1Baserate);
                }
                if (!strUQC2Baserate.equals("")) {
                    UQC2Baserate = Float.parseFloat(strUQC2Baserate);
                }
                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
                float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                        //                        IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                        //                        CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                        //                        SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                        Amount = Float.parseFloat(txtAmount.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String product = (String) comboProduct.getSelectedItem();
                float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
                Profit = Profit - Float.parseFloat(Discount);
                Float TotalAmoutInclTax = Amount;// + IGST_amt + CGST_amt + SGST_amt;

                if (wholeQuantity <= productStock) {
                    model.setValueAt(product, i, 1);
                    model.setValueAt(HSNCode, i, 2);
                    model.setValueAt(labelUQC1.getText(), i, 3);
                    model.setValueAt(UQC1qty, i, 4);
                    model.setValueAt(UQC1rate, i, 5);
                    model.setValueAt(UQC1qty * UQC1rate, i, 6);
                    model.setValueAt(labelUQC2.getText(), i, 7);
                    model.setValueAt(UQC2qty, i, 8);
                    model.setValueAt(UQC2rate, i, 9);
                    model.setValueAt(UQC2qty * UQC2rate, i, 10);
                    model.setValueAt(UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(), i, 11);
                    model.setValueAt(wholeQuantity, i, 12);
                    model.setValueAt(Float.parseFloat(Discountper), i, 13);
                    model.setValueAt(Float.parseFloat(Discount), i, 14);
                    model.setValueAt(Amount, i, 15);
//                    model.setValueAt(IGSTper, i, 16);
//                    model.setValueAt(IGST_amt, i, 17);
//                    model.setValueAt(CGSTper, i, 18);
//                    model.setValueAt(CGST_amt, i, 19);
//                    model.setValueAt(SGSTper, i, 20);
//                    model.setValueAt(SGST_amt, i, 21);
                    model.setValueAt(UQC1Baserate, i, 16);
                    model.setValueAt(UQC2Baserate, i, 17);
//                    model.setValueAt(Profit, i, 18);

                    clear(true);
                    TotalCalculate();
                    jTableRender();
                    comboProduct.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                }

            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select any Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void txtUQC2RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateActionPerformed
        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateActionPerformed

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained
        comboParty.setPopupVisible(true);
    }//GEN-LAST:event_comboPartyFocusGained

    private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            String name = (String) comboParty.getSelectedItem();
            if (name.equalsIgnoreCase("No Party")) {
                txtPartyName.requestFocus();

            } else if (name.equalsIgnoreCase("new party")) {
                txtPartyName.requestFocus();
            } else {
                comboParty.requestFocus();
            }
        }
    }//GEN-LAST:event_comboPartyKeyPressed


    private void comboPartyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_comboPartyKeyReleased


    private void btnAdd1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAdd1KeyPressed

    }//GEN-LAST:event_btnAdd1KeyPressed

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
        String quantity = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if ((!rate.equals("")) && (!quantity.equals(""))) {
            int Qty = Integer.parseInt(quantity);
            float Rate = Float.parseFloat(rate);
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = Rate * Qty;
            Float DiscountAmount = Float.parseFloat(txtDiscount.getText());
            String strDiscountPer = currency.format((DiscountAmount / TotalAmount) * 100);
            txtDiscountPer.setText(strDiscountPer);
            TotalAmount = TotalAmount - DiscountAmount;
            //            float TaxValue = TotalAmount * (IGSTper / 100);
            txtAmount.setText(currency.format(TotalAmount));
            //            if (isIGST) {
            //                txtIGSTamt.setText(currency.format(TaxValue));
            //                txtCGSTamt.setText("0.00");
            //                txtSGSTamt.setText("0.00");
            //            } else {
            //                txtIGSTamt.setText("0.00");
            //                txtCGSTamt.setText(currency.format(TaxValue / 2));
            //                txtSGSTamt.setText(currency.format(TaxValue / 2));
            //            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtDiscountKeyReleased

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        } else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped

    }//GEN-LAST:event_txtDiscountPerKeyTyped

    private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtDiscountPerKeyReleased

    private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDiscountPerKeyPressed

    private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
        txtDiscount.requestFocus();
    }//GEN-LAST:event_txtDiscountPerActionPerformed

    private void txtUQC2RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyTyped

    private void txtUQC2RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC2RateKeyReleased

    private void txtUQC2BaseRateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2BaseRateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2BaseRateKeyTyped

    private void txtUQC1RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyTyped

    private void txtUQC1RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC1RateKeyReleased

    private void txtUQC1RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateActionPerformed
        float value = Float.parseFloat(txtUQC1Rate.getText());
        txtUQC1Rate.setText(currency.format(value));
        txtUQC2Rate.requestFocus();
    }//GEN-LAST:event_txtUQC1RateActionPerformed

    private void txtUQC1BaseRateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1BaseRateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1BaseRateKeyTyped

    private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC1QtyKeyTyped

    private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC1QtyKeyReleased

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        if (txtUQC2Qty.isEnabled()) {
            txtUQC2Qty.requestFocus();
        } else {
            btnAdd.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC2QtyKeyTyped

    private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC2QtyKeyReleased

    private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
        txtUQC1BaseRate.requestFocus();
    }//GEN-LAST:event_txtUQC2QtyActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (!((Boolean) model.getValueAt(i, 19))) {

            comboProduct.setSelectedItem(model.getValueAt(i, 1));
            HSNCode = (String) model.getValueAt(i, 2);
            labelUQC1.setText((String) model.getValueAt(i, 3));
            txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
            txtUQC1Rate.setText(currency.format(model.getValueAt(i, 5)));
            labelUQC2.setText((String) model.getValueAt(i, 7));
            txtUQC2Qty.setText(model.getValueAt(i, 8).toString());
            txtUQC2Rate.setText(currency.format(model.getValueAt(i, 9)));
            txtDiscountPer.setText(model.getValueAt(i, 13).toString());
            txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
            txtAmount.setText(currency.format(model.getValueAt(i, 15)));
            //        txtIGSTper.setText(model.getValueAt(i, 16).toString());
            //        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
            //        txtCGSTper.setText(model.getValueAt(i, 18).toString());
            //        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
            //        txtSGSTper.setText(model.getValueAt(i, 20).toString());
            //        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
            txtUQC1BaseRate.setText(currency.format(model.getValueAt(i, 16)));
            txtUQC2BaseRate.setText(currency.format(model.getValueAt(i, 17)));
            btnAdd.setEnabled(false);
            comboProduct.requestFocus();
            comboProduct.setPopupVisible(true);
            getProductDetails();
//            clearBillCharge(false);
        }

    }//GEN-LAST:event_jTable1MouseClicked

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear(true);
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        try {
            SaveAndPrint(true);
        } catch (ParseException ex) {
            Logger.getLogger(BillOfSupplyUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateProductsItems();
            comboProduct.setPopupVisible(true);
            evt.consume();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProductsItems();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            SaveAndPrint(false);
        } catch (ParseException ex) {
            Logger.getLogger(BillOfSupplyUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    if (!((Boolean) model.getValueAt(y, 19))) {
                        model.setValueAt(y + 1, y, 0);
                    } else {
                        break;
                    }
                }
                clear(true);
                TotalCalculate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select any Product Line Items.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String partyName = (String) comboParty.getSelectedItem();
            partyName = partyName.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (partyName.equals("")) {
                String party = (String) comboParty.getSelectedItem();
                party = party.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (party.equals("No Party")) {
                    addProductList();
                } else {

                    JOptionPane.showMessageDialog(null, "Please add Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
                    comboParty.requestFocus();
                }
            } else {
                addProductList();
            }
            evt.consume();
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if (txtPartyName.getText().equals("")) {
            String party = (String) comboParty.getSelectedItem();
            party = party.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (party.equals("No Party")) {
                addProductList();
            } else {

                JOptionPane.showMessageDialog(null, "Please add Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
                comboParty.requestFocus();
            }
        } else {
            addProductList();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_labelProductKeyReleased

    private void comboProductKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboProductKeyTyped

    }//GEN-LAST:event_comboProductKeyTyped

    private void comboProductKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboProductKeyPressed
        //        Boolean isPrint = false;
        //        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_S) {
        //            isPrint = false;
        //            if (isOldBill) {
        //                OldPrintAndSaveAction(isPrint);
        //            } else {
        //                NewPrintAndSaveAction(isPrint);
        //            }
        //
        //        } else if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_P) {
        //            isPrint = true;
        //            if (isOldBill) {
        //                OldPrintAndSaveAction(isPrint);
        //            } else {
        //                NewPrintAndSaveAction(isPrint);
        //            }
        //        }
        //        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
        //            txtUQC1Qty.requestFocus();
        //        }

    }//GEN-LAST:event_comboProductKeyPressed

    private void comboProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboProductFocusGained
        comboProduct.showPopup();
    }//GEN-LAST:event_comboProductFocusGained

    private void txtRemarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRemarksActionPerformed
        comboProduct.requestFocus();
    }//GEN-LAST:event_txtRemarksActionPerformed

    private void txtBillDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBillDiscount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        }


    }//GEN-LAST:event_txtBillDiscountKeyTyped

    private void txtBillDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyReleased

        if (evt.getKeyCode() != KeyEvent.VK_ENTER && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE && Character.isDigit(evt.getKeyChar())) {
            discount = Float.parseFloat(txtBillDiscount.getText());
            amount = amountWithoutDiscount - discount;
            labelBillAmount.setText(String.valueOf(amount));
        } else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && (amount < amountWithoutDiscount)) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscount.equals("")) {
                Float fDiscount = Float.parseFloat(txtBillDiscount.getText());
                amount = amount + (discount - fDiscount);
                discount = discount - (discount - fDiscount);
                labelBillAmount.setText(String.valueOf(amount));
            } else {
                labelBillAmount.setText(String.valueOf(amountWithoutDiscount));
            }
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnSave.requestFocus();
        }
    }//GEN-LAST:event_txtBillDiscountKeyReleased

    private void txtUQC1BaseRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1BaseRateActionPerformed
        txtUQC2BaseRate.requestFocus();
    }//GEN-LAST:event_txtUQC1BaseRateActionPerformed

    private void txtUQC2BaseRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2BaseRateActionPerformed
        txtUQC1Rate.requestFocus();
    }//GEN-LAST:event_txtUQC2BaseRateActionPerformed

    private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2QtyKeyPressed

    private void txtUQC1BaseRateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1BaseRateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1BaseRateKeyPressed

    private void txtUQC2BaseRateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2BaseRateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2BaseRateKeyPressed

    private void txtBillDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillDiscountActionPerformed

    }//GEN-LAST:event_txtBillDiscountActionPerformed

    private void txtUQC1RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyPressed

    private void txtBillDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtBillDiscountKeyPressed

    private void txtUQC2RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyPressed

    private void txtDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDiscountKeyPressed

    private void comboStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboStateActionPerformed

    }//GEN-LAST:event_comboStateActionPerformed

    private void comboStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboStateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtMobile.requestFocus();
        }
    }//GEN-LAST:event_comboStateKeyPressed

    private void txtPartyNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartyNameKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }

        }
    }//GEN-LAST:event_txtPartyNameKeyPressed

    private void txtAddr1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddr1KeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }

        }
    }//GEN-LAST:event_txtAddr1KeyPressed

    private void txtAddr2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddr2KeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtAddr2KeyPressed

    private void txtAddr3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddr3KeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtAddr3KeyPressed

    private void txtCityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCityKeyPressed

    private void txtDistrictKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDistrictKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDistrictKeyPressed

    private void txtMobileKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMobileKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtMobileKeyPressed

    private void txtGSTINKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGSTINKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtGSTINKeyPressed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    private void menuHelp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelp2ActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelp2ActionPerformed

//    public void clearBillCharge(boolean bBtnClear) {
//        comboBillCharge.setSelectedItem("");
//        txtBillchargeAmount.setText("");
//        if (bBtnClear) {
//            jTable1.clearSelection();
//        }
//
//    }
    public Ledger findLedgerData() {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : sortedLedgers) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                break;
            }
        }
        return ledgerData;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BillOfSupplyUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BillOfSupplyUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BillOfSupplyUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BillOfSupplyUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BillOfSupplyUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboProduct;
    private javax.swing.JComboBox<String> comboState;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelBasePrice;
    private javax.swing.JLabel labelBillAmount;
    private javax.swing.JLabel labelProduct;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1BasePrice;
    private javax.swing.JLabel labelUQC1forPrice;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2BasePrice;
    private javax.swing.JLabel labelUQC2forPrice;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuHelp2;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTextField txtAddr1;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtAddr3;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JTextField txtBillDiscount;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtPartyName;
    private javax.swing.JTextField txtRemarks;
    private javax.swing.JTextField txtUQC1BaseRate;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC1Rate;
    private javax.swing.JTextField txtUQC2BaseRate;
    private javax.swing.JTextField txtUQC2Qty;
    private javax.swing.JTextField txtUQC2Rate;
    // End of variables declaration//GEN-END:variables
}
