/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author hp
 */
public class GetProduct {

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);

    public Product getProductByName(String productName) {
        for (Product product : products) {

            if (Objects.equals(product.getName(), productName)) {
               

                return product;
            }
        }
        return null;
    }
}
