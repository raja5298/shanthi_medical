/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

/**
 *
 * @author ABS
 */
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GetPDFHeaders {

    CompanyPolicy companyPolicy = SessionDataUtil.getSelectedCompany().getCompanyPolicy();
    int noOfCopies = companyPolicy.getNumberOfInvoiceCopies();

    public List<String> getPdfHeaders() {
        List<String> nameOfPDFCopies = new ArrayList<String>();
        
        for (int i = 1; i <= noOfCopies; i++) {
            switch (i) {
                case 1:
                    nameOfPDFCopies.add(companyPolicy.getFirstInvoiceWord());
                    break;
                case 2:
                    nameOfPDFCopies.add(companyPolicy.getSecondInvoiceWord());
                    break;
                case 3:
                    nameOfPDFCopies.add(companyPolicy.getThirdInvoiceWord());
                    break;
                case 4:
                    nameOfPDFCopies.add(companyPolicy.getFourthInvoiceWord());
                    break;
                case 5:
                    nameOfPDFCopies.add(companyPolicy.getFifthInvoiceWord());
                    break;
            }
        }
        return nameOfPDFCopies;
    }

}
