/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ALPHA
 */
public class StringToDecimal {

    public BigDecimal Currency(String amount) {
        try {
            BigDecimal amountinDecimal = null;
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            String pattern = "0.000";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
            decimalFormat.setParseBigDecimal(true);
            amountinDecimal = (BigDecimal) decimalFormat.parse(amount);
            return amountinDecimal;
        } catch (ParseException ex) {
            Logger.getLogger(StringToDecimal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
