package india.abs.gstonepro.ui.utils;

import india.abs.gstonepro.api.utils.SystemPolicyUtil;

public class MasterUser{

    public static String getStrUserName() {
        return SystemPolicyUtil.getSystemPolicy().getMasterUsername();
    }

    public static String getStrPassword() {
        return SystemPolicyUtil.getSystemPolicy().getMasterPassword();
    }

}