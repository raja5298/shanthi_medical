/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import india.abs.gstonepro.api.models.CompanyPolicy;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class CheckFinancialYear {

    public Boolean toCheck(CompanyPolicy companyPolicy, Date checkDate, Boolean skipTheCheck) {
        
        
        if(skipTheCheck){
            return true;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(checkDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        checkDate = cal.getTime();
        return checkDate.compareTo(companyPolicy.getFinancialYearStart()) >= 0 && checkDate.compareTo(companyPolicy.getFinancialYearEnd()) <= 0;
    }

}
