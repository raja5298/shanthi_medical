/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import java.util.Arrays;

/**
 *
 * @author ABS
 */
public class StateCode {

    public static final String[] State = {
        "Jammu and Kashmir", "Himachal Pradesh", "Punjab", "Chandigarh", "Uttarakhand", "Haryana", "Delhi", "Rajasthan", "Uttar Pradesh", "Bihar",
        "Sikkim", "Arunachal Pradesh", "Nagaland", "Manipur", "Mizoram", "Tripura", "Meghalaya", "Assam", "West Bengal", "Jharkhand",
        "Odisha", "Chhattisgarh", "Madhya Pradesh", "Gujarat", "Daman and Diu", "Dadra and Nagar Haveli", "Maharashtra", " ", "Karnataka", "Goa",
        "Lakshadweep", "Kerala", "Tamil Nadu", "Puducherry", "Andaman and Nicobar Islands", "Telangana", "Andhra Pradesh"
    };

    public static String getStateCode(String s) {
        String code = "";
        int pos = Arrays.asList(State).indexOf(s) + 1;
        if (pos>0 && pos < 10) {
            code = "0" + pos;
        } else {
            code = "" + pos;
        }
        return code;
    }
}
