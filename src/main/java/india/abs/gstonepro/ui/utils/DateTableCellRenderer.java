package india.abs.gstonepro.ui.utils;

import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author optimus
 */
public class DateTableCellRenderer extends DefaultTableCellRenderer {

    public DateTableCellRenderer() {
        setHorizontalAlignment(JLabel.LEFT);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value != null && value != "Openinng Balance") {
            String string = value.toString();

            String[] parts = string.split("-");
            value = parts[2] + "-" + parts[1] + "-" + parts[0];
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
         return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    }

}
