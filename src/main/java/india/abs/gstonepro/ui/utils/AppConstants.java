/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import java.io.File;

/**
 *
 * @author ABS
 */
public class AppConstants {

    //private static String dbPath = "C:\\GST\\billh2";
    private static String docTempPath = ".\\temp\\";
    private static String docAssetsPath = ".\\assets\\";
//    private static String docPath = "C:\\GST\\PDF\\";
//    private static String excelPath = "C:\\GST\\excel\\";

    public static String getTempDocPath() {
        File directory = new File(docTempPath);
        if (!directory.exists()) {
            directory.mkdir();
        }
        DeleteFiles();
        return docTempPath;
    }

    public static String getAssetsDocPath() {
        File directory = new File(docAssetsPath);
        if (!directory.exists()) {
            directory.mkdir();
        }
//        DeleteFiles();
        return docAssetsPath;
    }

//    public static String getDocPath() {
//        File directory = new File(docPath);
//        if (!directory.exists()) {
//            directory.mkdir();
//        }
//        DeleteFiles();
//        return docPath;
//    }
//
//    public static String getExcelPath() {
//        File directory = new File(excelPath);
//        if (!directory.exists()) {
//            directory.mkdir();
//        }
//        DeleteFiles();
//        return excelPath;
//    }
    public static void DeleteFiles() {
        File dir = new File(docTempPath);
        File dir1 = new File(docAssetsPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        if (!dir1.exists()) {
            dir1.mkdir();
        }
        File[] children = dir.listFiles();
        if (children != null) {
            for (int i = 0; i < children.length; i++) {
                children[i].delete();
            }
        }

    }
}
