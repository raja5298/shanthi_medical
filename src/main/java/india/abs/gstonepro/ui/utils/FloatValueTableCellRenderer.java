package india.abs.gstonepro.ui.utils;

import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author optimus
 */
public class FloatValueTableCellRenderer extends DefaultTableCellRenderer {

    public FloatValueTableCellRenderer() {
        setHorizontalAlignment(JLabel.RIGHT);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        DecimalFormat twoPlaces = new DecimalFormat("0.000");
        String formatValue = "0.00";
         if(value==null){
             formatValue = "0.00";
         }else 
        if (value != "Bill Deleted") {
            formatValue = twoPlaces.format(value);
        } else {
            formatValue = "Bill Deleted";
        }
        return super.getTableCellRendererComponent(table, formatValue, isSelected, hasFocus, row, column);
    }

}
