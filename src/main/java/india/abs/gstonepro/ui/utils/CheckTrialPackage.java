/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class CheckTrialPackage {
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    
    public Boolean checkByDate(Date checkDate) {
        Boolean isNotExpired = true;
        
        if((checkDate.compareTo(companyPolicy.getAccountsExpiryDate())) >= 0){
            isNotExpired = false;
        }
        return isNotExpired;
    }
}
