 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class FormValidation {

    public Boolean checkMobileNumber(String mobile) {
        Boolean isWrong = false;
        if (mobile.length() != 10) {
            isWrong = true;
        }
        return isWrong;
    }

    public Boolean checkPhoneNumber(String phone) {
//        Boolean isWrong = true;
//        if (phone.contains("-")) {
//            isWrong = false;
//        }
//        return isWrong;
        return false;
    }

    public Boolean checkEmailID(String eMail) {
        Boolean isWrong = true;
        Pattern patternEmail = Pattern.compile(
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcherEmail = patternEmail.matcher(eMail);
        if (matcherEmail.matches()) {
            isWrong = false;
        }

        return isWrong;
    }

    public int checkGSTIN(String GSTIN) {
        int errorReport = 0;
        Pattern patterngstin = Pattern.compile("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$");
        Matcher matchergstin = patterngstin.matcher(GSTIN);
        if (!GSTIN.equals("")) {
            if (GSTIN.length() != 15) {
                errorReport = 3;
            } else if (!matchergstin.matches()) {
                if (!Pattern.compile("^[0-9]{2}$").matcher(GSTIN.substring(0, 2)).matches()) {
                    errorReport = 1;
                } else if (!GSTIN.substring(13, 14).equals("Z")) {
                    errorReport = 2;
                } else {
                    errorReport = 3;
                }
            } else {
                errorReport = 0;
            }
        } else {
            errorReport = 0;
        }
        return errorReport;
    }

}
