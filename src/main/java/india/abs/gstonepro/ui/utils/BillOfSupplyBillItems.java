/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui.utils;

import java.math.BigDecimal;

/**
 *
 * @author SGS
 */
public class BillOfSupplyBillItems {

    private String sno;

    private String productNameOrbillChargeName;

    private String hsnSac;

    private String productStock;

    private String uqcOne;

    private int uqcOneQuantity;

    private BigDecimal uqcOneRate;

    private float uqcOneValue;

    private String uqcTwo;

    private int uqcTwoQuantity;

    private BigDecimal uqcTwoRate;

    private float uqcTwoValue;

    private BigDecimal discountValue;

    private BigDecimal productvalueOrbillChargeValue;
    
    private Boolean isBillCharge;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getProductNameOrbillChargeName() {
        return productNameOrbillChargeName;
    }

    public void setProductNameOrbillChargeName(String productNameOrbillChargeName) {
        this.productNameOrbillChargeName = productNameOrbillChargeName;
    }

    public String getHsnSac() {
        return hsnSac;
    }

    public void setHsnSac(String hsnSac) {
        this.hsnSac = hsnSac;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getUqcOne() {
        return uqcOne;
    }

    public void setUqcOne(String uqcOne) {
        this.uqcOne = uqcOne;
    }

    public int getUqcOneQuantity() {
        return uqcOneQuantity;
    }

    public void setUqcOneQuantity(int uqcOneQuantity) {
        this.uqcOneQuantity = uqcOneQuantity;
    }

    public BigDecimal getUqcOneRate() {
        return uqcOneRate;
    }

    public void setUqcOneRate(BigDecimal uqcOneRate) {
        this.uqcOneRate = uqcOneRate;
    }

    public float getUqcOneValue() {
        return uqcOneValue;
    }

    public void setUqcOneValue(float uqcOneValue) {
        this.uqcOneValue = uqcOneValue;
    }

    public String getUqcTwo() {
        return uqcTwo;
    }

    public void setUqcTwo(String uqcTwo) {
        this.uqcTwo = uqcTwo;
    }

    public int getUqcTwoQuantity() {
        return uqcTwoQuantity;
    }

    public void setUqcTwoQuantity(int uqcTwoQuantity) {
        this.uqcTwoQuantity = uqcTwoQuantity;
    }

    public BigDecimal getUqcTwoRate() {
        return uqcTwoRate;
    }

    public void setUqcTwoRate(BigDecimal uqcTwoRate) {
        this.uqcTwoRate = uqcTwoRate;
    }

    public float getUqcTwoValue() {
        return uqcTwoValue;
    }

    public void setUqcTwoValue(float uqcTwoValue) {
        this.uqcTwoValue = uqcTwoValue;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getProductvalueOrbillChargeValue() {
        return productvalueOrbillChargeValue;
    }

    public void setProductvalueOrbillChargeValue(BigDecimal productvalueOrbillChargeValue) {
        this.productvalueOrbillChargeValue = productvalueOrbillChargeValue;
    }

    public Boolean getIsBillCharge() {
        return isBillCharge;
    }

    public void setIsBillCharge(Boolean isBillCharge) {
        this.isBillCharge = isBillCharge;
    }

}
