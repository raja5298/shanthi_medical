/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Estimate;
import india.abs.gstonepro.api.models.EstimateLineItem;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.PricingPolicy;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.SingleProductPolicy;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.PricingPolicyLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.business.BillChargeLogic;
import india.abs.gstonepro.business.EstimateLogic;
import india.abs.gstonepro.business.GoDownLogic;
import india.abs.gstonepro.pdf.EstimateBill;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.CheckTrialPackage;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.FormValidation;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import india.abs.gstonepro.unusedui.PaymentGrpUI;

/**
 *
 * @author ABS
 */
public class EstimateUI extends javax.swing.JFrame {

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String HSNCode;
    boolean isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0, BaseProductRate = 0;
    float amountWithoutDiscount = 0, amount = 0, discount = 0, discountPer = 0, oldDiscount = 0;
    float fTotalCess = 0;
    long productStock = 0;
    Long LedgerId;
    int nCount = 0;
    boolean bError = false;
    boolean bUQCSame = false;
    boolean isUnderStockPossible = false, isServiceProduct = true;
    boolean bCheckStock = false;
    boolean isnewPartyAdded = false;
    GoDownStockDetail stock = null;
    boolean isOldEstimate = false;
    boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isTrialPackage = SystemPolicyUtil.getSystemPolicy().isTrialPackage();
    boolean descAdded = false, noParty = false;

    String desc1 = "";
    String desc2 = "";
    String desc3 = "";
    String desc4 = "";
    String desc5 = "";
    String desc6 = "";
    String desc7 = "";
    String desc8 = "";
    String desc9 = "";
    String desc10 = "";
    int descCount = 0;

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Long companyId = company.getCompanyId();
    String CompanyState = SessionDataUtil.getSelectedCompany().getState();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<GoDown> godowns = new GoDownLogic().fetchAllGodowns(companyId);
    Ledger ledgerData = new Ledger();
    Product productDetail = new Product();
    EstimationDescriptionUI1 productDescriptionUI = null;
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<>(ledgers);
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(companyId);
    List<BillCharge> charges = new BillChargeLogic().fetchAllBillCharges();

    List<PricingPolicy> pricingPolicy = new PricingPolicyLogic().fetchAllPP(companyId);
    Set<SingleProductPolicy> recentSingleProductPolicies = new HashSet<>();
    StringToDecimal convertDecimal = new StringToDecimal();
    DecimalFormat currency = new DecimalFormat("0.000");
    Estimate oldEstimate = null;

    /**
     * Creates new form Bill
     */
    public EstimateUI() {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            setExtendedState(EstimateUI.MAXIMIZED_BOTH);
            comboState.setSelectedItem("Tamil Nadu");
            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setParty();
            saveData();
            printData();
//        loadComboProduct();
            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                public int compare(Ledger o1, Ledger o2) {
                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                }
            });

            Collections.sort(products, new Comparator<Product>() {
                public int compare(Product o1, Product o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            btnProductDescription.setEnabled(false);
            dateBill.setDate(new Date());
            setProduct();
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());

            setMenuRoles();
            jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));

            JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
            editor.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
                        setNewProduct();
                    }
                    if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
                        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                            String val = (String) comboProduct.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            for (Product product : products) {
                                if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(product.getName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];
                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboProduct.setModel(model);
                            comboProduct.getEditor().setItem(val);
                            comboProduct.setPopupVisible(true);
                        }
                    }
                }

                @Override
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_Z) {
                        new NewProductUI().setVisible(true);
                    } else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        products.forEach((product) -> {
                            scripts.add(product.getName());
                        });
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboProduct.setModel(model);
                        comboProduct.getEditor().setItem("");
                        comboProduct.setPopupVisible(true);
                    } else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboProduct.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboProduct.setSelectedItem("");
                        comboProduct.getEditor().setItem(selectedObj);
                        comboProduct.setPopupVisible(false);
                        getProductDetails();
                    }
                }
            });

            JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
            editor1.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
                        if (isnewPartyAdded || (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP)) {
                            isnewPartyAdded = false;
                            String val = (String) comboParty.getEditor().getItem();
                            ArrayList<String> scripts = new ArrayList<>();
                            String noParty = "No Party", newParty = "New Party";
                            if (noParty.toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(noParty);
                            }
                            if (newParty.toLowerCase().contains(val.toLowerCase())) {
                                scripts.add(newParty);
                            }
                            for (Ledger ledger : sortedLedgers) {
                                if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                                    scripts.add(ledger.getLedgerName());
                                }
                            }
                            String[] myArray = new String[scripts.size()];

                            scripts.toArray(myArray);
                            DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                            comboParty.setModel(model);
                            comboParty.getEditor().setItem(val);
                            comboParty.setPopupVisible(true);
                        }
                    }
                }

                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        comboProduct.requestFocus();
                    } else if (evt.isControlDown() && evt.getKeyCode() == java.awt.event.KeyEvent.VK_BACK_SPACE) {
                        ArrayList<String> scripts = new ArrayList<>();
                        String noParty = "No Party", newParty = "New Party";
                        scripts.add(noParty);
                        scripts.add(newParty);
                        for (Ledger ledger : sortedLedgers) {
                            scripts.add(ledger.getLedgerName());
                        }
                        String[] myArray = new String[scripts.size()];
                        scripts.toArray(myArray);
                        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                        comboParty.setModel(model);
                        comboParty.getEditor().setItem("");
                        comboParty.setPopupVisible(true);
                    } else if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        DefaultComboBoxModel model = (DefaultComboBoxModel) comboParty.getModel();
                        String selectedObj = (String) model.getSelectedItem();
                        comboParty.setPopupVisible(false);
                        comboParty.getEditor().setItem(selectedObj);
                        getPartyDetails();
                        dateBill.requestFocusInWindow();
                    }

                }
            });
            ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateBill.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateBill.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateBill.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    String strName = comboParty.getSelectedItem().toString();
                                    if (strName.equals("")) {
                                        comboParty.requestFocus();
                                    } else if (strName.equalsIgnoreCase("no party") || strName.equalsIgnoreCase("new Party")) {
                                        txtPartyName.requestFocus();
                                    } else {
                                        comboProduct.requestFocus();
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");

                            }

                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(EstimateUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(EstimateUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        //file
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate() && !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
            menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    public void EditOldBill(String id) {
        String strAddr = "";
        isOldEstimate = true;
        setPartyEditable(false);
        oldEstimate = new EstimateLogic().readEstimate(id);
        dateBill.setDate(oldEstimate.getEstimateDate());
        List<EstimateLineItem> estimateLineItems = oldEstimate.getEstimateLineItem();

//        oldEstimate.printCollections();
        noParty = oldEstimate.getNoParty();
        if (noParty) {
            LedgerId = null;
            txtPartyName.setText(oldEstimate.getNoPartyName());
            txtAddrLine.setText(oldEstimate.getNoPartyLineOne());
            txtCity.setText(oldEstimate.getNoPartyCity());
            txtDistrict.setText(oldEstimate.getNoPartyDistrict());
            comboState.setSelectedItem(oldEstimate.getNoPartyState());
            txtGSTIN.setText(oldEstimate.getNoPartyState());
        } else {
            LedgerId = oldEstimate.getLedger().getLedgerId();
            comboParty.setSelectedItem(oldEstimate.getLedger().getLedgerName());
            txtPartyName.setText(oldEstimate.getLedger().getLedgerName());
            String address = oldEstimate.getLedger().getAddress();
            String[] words = address.split("/~/");
            for (int ii = 0; ii < words.length; ii++) {
                if (ii == 0) {
                    strAddr = words[ii];
                } else if (!words[ii].equals(" ")) {
                    strAddr = strAddr + ", " + words[ii];
                }
            }
            txtAddrLine.setText(strAddr);
            txtCity.setText(oldEstimate.getLedger().getCity());
            txtDistrict.setText(oldEstimate.getLedger().getDistrict());
            comboState.setSelectedItem(oldEstimate.getLedger().getState());
            txtGSTIN.setText(oldEstimate.getLedger().getGSTIN());
        }
        txtBillDiscount.setText(oldEstimate.getDiscountAmount().toString());
        oldDiscount = oldEstimate.getDiscountAmount().floatValue();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (EstimateLineItem item : estimateLineItems) {
            Object[] row = {
                item.getLineNumber(),
                item.getProductName(),
                item.getHsnSac(),
                item.getUqcOne(),
                item.getUqcOneQuantity(),
                item.getUqc1BaseRate().floatValue(),
                item.getUqcOneRate().floatValue(),
                item.getUqcOneValue().floatValue(),
                item.getUqcTwo(),
                item.getUqcTwoQuantity(),
                item.getUqc2BaseRate().floatValue(),
                item.getUqcTwoRate().floatValue(),
                item.getUqcTwoValue().floatValue(),
                item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                item.getStockQuantity(),
                item.getValue().floatValue(),
                item.getDiscountPercent(),
                item.getDiscountValue().floatValue(),
                item.getNetValue().floatValue(),
                item.getProfit().floatValue(),
                item.getDescriptionOne(), item.getDescriptionTwo(), item.getDescriptionThree(), item.getDescriptionFour(),
                item.getDescriptionFive(), item.getDescriptionSix(), item.getDescriptionSeven(), item.getDescriptionEight(),
                item.getDescriptionNine(), item.getDescriptionTen(), item.getDescriptionCount(), item.getProduct().isService()
            };
            model.addRow(row);
        }

        TotalCalculate();
        jTableRender();
        comboProduct.requestFocus();
        clear(false);
    }

    public String setStockConverstion(long qty, String UQC1, String UQC2, int UQC2Value) {
        long UQC1Count = qty / UQC2Value;
        long UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void getProductDetails() {
        descAdded = false;
        bUQCSame = false;
        String productName = (String) comboProduct.getEditor().getItem();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if ((productName != null) && (!(productName.equals("")))) {
            for (Product product : products) {
                if (product.getName().equals(productName)) {
                    productDetail = product;
                    btnProductDescription.setEnabled(true);
                    isServiceProduct = product.isService();
                    HSNCode = product.getHsnCode();

                    if (isServiceProduct) {
                        bUQCSame = true;
                        labelUQC1.setText("");
                        labelUQC2.setText("");
                        txtUQC1Qty.setText("");
                        txtUQC1Qty.setEnabled(false);
                        txtUQC2Qty.setText("");
                        txtUQC2Qty.setEnabled(false);
                        labelUQC1forPrice.setText("");
                        labelUQC2forPrice.setText("");
                        btnProductDescription.setEnabled(false);
                        txtUQC2Rate.setText("");
                        txtUQC1Rate.setEnabled(true);
                        txtUQC2Rate.setEnabled(false);

                        UQC2Value = product.getUQC2Value();
                        if (product.getBaseProductRate() != null) {
                            BaseProductRate = product.getBaseProductRate().floatValue();
                        } else {
                            BaseProductRate = product.getProductRate().floatValue();
                        }
                        isBasePriceProductTaxInclusive = product.isBasePriceInclusiveOfGST();

                        float basePrice = 0;
                        if (isBasePriceProductTaxInclusive) {
                            basePrice = (BaseProductRate / (100 + product.getIgstPercentage())) * 100;
                        } else {
                            basePrice = BaseProductRate;
                        }
                        ProductRate = product.getProductRate().floatValue();
                        isProductTaxInclusive = product.isInclusiveOfGST();
                        float price = 0;
                        if (isProductTaxInclusive) {
                            price = (ProductRate / (100 + product.getIgstPercentage())) * 100;
                        } else {
                            price = ProductRate;
                        }
                        txtUQC1Rate.setText(currency.format(price));
                        txtUQC1Rate.requestFocus();
                    } else {
                        if (product.getUQC1().equalsIgnoreCase(product.getUQC2())) {
                            bUQCSame = true;
                            labelUQC1.setText(product.getUQC1());
                            labelUQC1forPrice.setText(product.getUQC1());
                            labelUQC2.setText(product.getUQC2());
                            labelUQC2forPrice.setText(product.getUQC2());

                            labelUQC1.setEnabled(true);
                            txtUQC1Qty.setEnabled(true);
                            labelUQC1forPrice.setEnabled(true);
                            txtUQC1Rate.setEnabled(true);
                            labelUQC2.setEnabled(false);
                            labelUQC2forPrice.setEnabled(false);
                            txtUQC2Qty.setEnabled(false);
                            txtUQC2Rate.setEnabled(false);
                        } else {
                            bUQCSame = false;

                            labelUQC1.setText(product.getUQC1());
                            labelUQC1forPrice.setText(product.getUQC1());
                            labelUQC2.setText(product.getUQC2());
                            labelUQC2forPrice.setText(product.getUQC2());

                            labelUQC1.setEnabled(true);
                            txtUQC1Qty.setEnabled(true);
                            labelUQC1forPrice.setEnabled(true);
                            txtUQC1Rate.setEnabled(true);
                            labelUQC2.setEnabled(true);
                            labelUQC2forPrice.setEnabled(true);
                            txtUQC2Qty.setEnabled(true);
                            txtUQC2Rate.setEnabled(true);
                        }
                        UQC2Value = product.getUQC2Value();
                        if (product.getBaseProductRate() != null) {
                            BaseProductRate = product.getBaseProductRate().floatValue();
                        } else {
                            BaseProductRate = product.getProductRate().floatValue();
                        }
                        isBasePriceProductTaxInclusive = product.isBasePriceInclusiveOfGST();

                        float basePrice = 0;
                        if (isBasePriceProductTaxInclusive) {
                            basePrice = (BaseProductRate / (100 + product.getIgstPercentage())) * 100;
                        } else {
                            basePrice = BaseProductRate;
                        }
                        ProductRate = product.getProductRate().floatValue();
                        isProductTaxInclusive = product.isInclusiveOfGST();
                        float price = 0;
                        if (isProductTaxInclusive) {
                            price = (ProductRate / (100 + product.getIgstPercentage())) * 100;
                        } else {
                            price = ProductRate;
                        }
                        txtUQC1Rate.setText(currency.format(price * UQC2Value));
                        txtUQC2Rate.setText(currency.format(price));
                        txtUQC1Qty.requestFocus();
                    }
                }
            }
        } else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtUQC1Rate.setText("");
            txtUQC2Rate.setText("");
            txtAmount.setText("");
            comboProduct.setSelectedItem("");
            comboProduct.requestFocus();
        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    public void setNewProduct() {
        products = new ProductLogic().fetchAllProducts(companyId);
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboProduct.setModel(model);
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtPartyName = new javax.swing.JTextField();
        txtAddrLine = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        comboState = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        txtGSTIN = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtRemarks = new javax.swing.JTextArea();
        txtBillDiscount = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txtAmount = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        panelUQC = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txtUQC1Qty = new javax.swing.JTextField();
        labelUQC1 = new javax.swing.JLabel();
        txtUQC2Qty = new javax.swing.JTextField();
        labelUQC2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtUQC1Rate = new javax.swing.JTextField();
        labelUQC1forPrice = new javax.swing.JLabel();
        txtUQC2Rate = new javax.swing.JTextField();
        labelUQC2forPrice = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        comboProduct = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        btnClear = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        labelTotalAmount = new javax.swing.JLabel();
        btnProductDescription = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Estimate");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product / Service", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1BaseRate", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2BaseRate", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Amount", "Discount %", "Discount", "NetAmount", "Profit", "desc1", "desc2", "desc3", "desc4", "desc5", "desc6", "desc7", "desc8", "desc9", "desc10", "desccount", "isService"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(60);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(60);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(60);
            jTable1.getColumnModel().getColumn(1).setMinWidth(400);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(400);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(400);
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(11).setMinWidth(0);
            jTable1.getColumnModel().getColumn(11).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(11).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(14).setMinWidth(0);
            jTable1.getColumnModel().getColumn(14).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(14).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(16).setPreferredWidth(75);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(22).setMinWidth(0);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(23).setMinWidth(0);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(24).setMinWidth(0);
            jTable1.getColumnModel().getColumn(24).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(24).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(25).setMinWidth(0);
            jTable1.getColumnModel().getColumn(25).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(25).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(26).setMinWidth(0);
            jTable1.getColumnModel().getColumn(26).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(26).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(27).setMinWidth(0);
            jTable1.getColumnModel().getColumn(27).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(27).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(28).setMinWidth(0);
            jTable1.getColumnModel().getColumn(28).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(28).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(29).setMinWidth(0);
            jTable1.getColumnModel().getColumn(29).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(29).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(30).setMinWidth(0);
            jTable1.getColumnModel().getColumn(30).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(30).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(31).setMinWidth(0);
            jTable1.getColumnModel().getColumn(31).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(31).setMaxWidth(0);
        }

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Date ");

        dateBill.setDateFormatString("dd-MM-yyyy");

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtPartyName.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtPartyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartyNameActionPerformed(evt);
            }
        });
        txtPartyName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartyNameKeyPressed(evt);
            }
        });

        txtAddrLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddrLineActionPerformed(evt);
            }
        });
        txtAddrLine.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddrLineKeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel20.setText("City");

        jLabel25.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel25.setText("District");

        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });
        txtDistrict.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDistrictKeyPressed(evt);
            }
        });

        txtCity.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });
        txtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCityKeyPressed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("GSTIN/UIN");

        comboState.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana\t", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur\t", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal" }));
        comboState.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboStateFocusGained(evt);
            }
        });
        comboState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboStateActionPerformed(evt);
            }
        });
        comboState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboStateKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("Party");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboPartyKeyReleased(evt);
            }
        });

        txtGSTIN.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGSTINFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtGSTINFocusLost(evt);
            }
        });
        txtGSTIN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtGSTINMouseClicked(evt);
            }
        });
        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });
        txtGSTIN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtGSTINKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboParty, 0, 184, Short.MAX_VALUE))
                .addGap(65, 65, 65)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPartyName)
                    .addComponent(txtAddrLine, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(59, 59, 59)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDistrict)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboState, 0, 190, Short.MAX_VALUE)
                    .addComponent(txtGSTIN))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel17, jLabel26});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(comboState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(txtAddrLine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtPartyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(2, 2, 2)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel15, jLabel16, jLabel17, jLabel2, jLabel20, jLabel25, jLabel26, jLabel6});

        jLabel14.setText("Remarks");

        txtRemarks.setColumns(20);
        txtRemarks.setRows(5);
        jScrollPane2.setViewportView(txtRemarks);

        txtBillDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBillDiscountActionPerformed(evt);
            }
        });
        txtBillDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBillDiscountKeyTyped(evt);
            }
        });

        jLabel21.setText("Bill Discount");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBillDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txtAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel13.setText("Amount ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addContainerGap())
        );

        jLabel35.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel35.setText("Quantity ");

        txtUQC1Qty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUQC1QtyFocusGained(evt);
            }
        });
        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        labelUQC1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1.setText("UQC1");
        labelUQC1.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        labelUQC2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2.setText("UQC2");
        labelUQC2.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("Price ");

        txtUQC1Rate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUQC1RateFocusGained(evt);
            }
        });
        txtUQC1Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateActionPerformed(evt);
            }
        });
        txtUQC1Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyTyped(evt);
            }
        });

        labelUQC1forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC1forPrice.setText("/ UQC1");
        labelUQC1forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        txtUQC2Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateActionPerformed(evt);
            }
        });
        txtUQC2Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyTyped(evt);
            }
        });

        labelUQC2forPrice.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelUQC2forPrice.setText("/ UQC2");
        labelUQC2forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Discount %");

        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel27.setText("Disount");

        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboProductFocusGained(evt);
            }
        });
        comboProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductActionPerformed(evt);
            }
        });
        comboProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboProductKeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel18.setText("Product / Service ");

        javax.swing.GroupLayout panelUQCLayout = new javax.swing.GroupLayout(panelUQC);
        panelUQC.setLayout(panelUQCLayout);
        panelUQCLayout.setHorizontalGroup(
            panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUQCLayout.createSequentialGroup()
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUQC1Rate, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE))
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelUQCLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDiscountPer))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtUQC1Rate, txtUQC2Qty, txtUQC2Rate});

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel27, jLabel35, jLabel5});

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelUQC2, labelUQC2forPrice});

        panelUQCLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtDiscount, txtDiscountPer});

        panelUQCLayout.setVerticalGroup(
            panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUQCLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel35)
                        .addComponent(comboProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel27)
                        .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelUQCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtUQC2Rate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        panelUQCLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {comboProduct, jLabel18, jLabel27, jLabel3, jLabel35, jLabel5, labelUQC1, labelUQC1forPrice, labelUQC2, labelUQC2forPrice, txtDiscount, txtDiscountPer, txtUQC1Qty, txtUQC1Rate, txtUQC2Qty, txtUQC2Rate});

        comboProduct.getAccessibleContext().setAccessibleName("");

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('c');
        btnClear.setText("Clear ");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setMnemonic('s');
        btnSave.setText("Save");
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnPrint.setMnemonic('p');
        btnPrint.setText("Save & Print");
        btnPrint.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        labelTotalAmount.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        labelTotalAmount.setText("Toal Amount : 0.00");

        btnProductDescription.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnProductDescription.setMnemonic('u');
        btnProductDescription.setText("Product Description");
        btnProductDescription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductDescriptionActionPerformed(evt);
            }
        });

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelUQC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnProductDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(labelTotalAmount)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelTotalAmount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelUQC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnProductDescription)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnPrint, btnSave});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdd, btnClear, btnDelete, btnUpdate});

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txtPartyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartyNameActionPerformed
        txtAddrLine.requestFocus();
    }//GEN-LAST:event_txtPartyNameActionPerformed

    private void txtAddrLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddrLineActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddrLineActionPerformed
    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(18).setCellRenderer(new FloatValueTableCellRenderer());
        jTable1.getColumnModel().getColumn(19).setCellRenderer(new FloatValueTableCellRenderer());

    }


    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        comboState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void comboProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboProductFocusGained
        //    comboProduct.showPopup();
    }//GEN-LAST:event_comboProductFocusGained

    private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_labelProductKeyReleased

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String party = comboParty.getSelectedItem().toString();
        if (party.equalsIgnoreCase("no party") || validateFields()) {
            if (isServiceProduct) {
                addServiceList();
            } else {
                addProductList();
            }
            clear(false);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    public void TotalCalculationForProduct(Boolean isDiscountPercentageBased) {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("") || !strUQC2rate.equals(""))) && ((!strUQC1qty.equals("") || !strUQC2qty.equals("")))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }

            if ((UQC2qty / UQC2Value) > 0) {
                UQC1qty += UQC2qty / UQC2Value;
                UQC2qty = UQC2qty % UQC2Value;
                txtUQC1Qty.setText(UQC1qty + "");
                txtUQC2Qty.setText(UQC2qty + "");
            }
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);

            if (isDiscountPercentageBased) {
                String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    DiscountPer = Float.parseFloat(strDiscountPer);
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                } else {
                    txtDiscount.setText("0.00");
                }
            } else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscount.equals("") && TotalAmount > 0) {
                    Discount = Float.parseFloat(strDiscount);
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                } else {
                    txtDiscountPer.setText("0.00");
                }
            }
            txtAmount.setText(currency.format(TotalAmount));
        } else {
            txtAmount.setText("0.00");
        }
    }

    public void TotalCalculationForService(Boolean isDiscountPercentageBased) {
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (!strUQC1rate.equals("")) {
            float UQC1rate = 0;

            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }

            float DiscountPer = 0, Discount = 0;
            float TotalAmount = UQC1rate;
            if (isDiscountPercentageBased) {
                String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscountPer.equals("") && TotalAmount > 0) {
                    DiscountPer = Float.parseFloat(strDiscountPer);
                    Discount = (TotalAmount * DiscountPer) / 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscount.setText(currency.format(Discount));
                } else {
                    txtDiscount.setText("0.00");
                }
            } else {
                String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                if (!strDiscount.equals("") && TotalAmount > 0) {
                    Discount = Float.parseFloat(strDiscount);
                    DiscountPer = (Discount / TotalAmount) * 100;
                    TotalAmount = TotalAmount - Discount;
                    txtDiscountPer.setText(currency.format(DiscountPer));
                } else {
                    txtDiscountPer.setText("0.00");
                }
            }
            txtAmount.setText(currency.format(TotalAmount));
        } else {
            txtAmount.setText("0.00");
        }
    }

    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String party = comboParty.getSelectedItem().toString();
            if (party.equalsIgnoreCase("no party") || validateFields()) {
                if (isServiceProduct) {
                    addServiceList();
                } else {
                    addProductList();
                }
                evt.consume();
            }
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    if (!(Boolean) model.getValueAt(y, 27)) {
                        model.setValueAt(y + 1, y, 0); //setValueAt(data,row,column)
                    }
                }
                clear(false);
                TotalCalculate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed
    public void clear(boolean bBtnClear) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        txtUQC1Qty.setText("");
        txtUQC2Qty.setText("");
        txtUQC1Rate.setText("");
        txtUQC2Rate.setText("");
        txtAmount.setText("");
        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("UQC1");
        labelUQC2.setText("UQC2");
        labelUQC1forPrice.setText("/UQC1");
        labelUQC2forPrice.setText("/UQC2");
        jTable1.clearSelection();
        btnProductDescription.setEnabled(false);
        comboProduct.setEnabled(true);
        comboProduct.setSelectedItem("");
        comboProduct.requestFocus();
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        btnProductDescription.setEnabled(false);
        if (bBtnClear) {
            comboParty.setSelectedItem("No Party");
            dateBill.setDate(new Date());
            txtPartyName.setText("");
            txtAddrLine.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            comboState.setSelectedItem("");
            txtGSTIN.setText("");
            txtBillDiscount.setText("");
            txtRemarks.setText("");
            labelTotalAmount.setText("Total Amount : 0.00");
            if (model.getRowCount() > 0) {
                model.setRowCount(0);
            }
            setParty();
        }

        btnAdd.setEnabled(true);
    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        SaveAndPrint(false);
    }//GEN-LAST:event_btnSaveActionPerformed
    public void SaveAndPrint(Boolean isPrint) {
        Boolean isNotExpired = true;
        if (isTrialPackage) {
            isNotExpired = new CheckTrialPackage().checkByDate(dateBill.getDate());
        }
        if (isNotExpired) {
            Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);
            if (!isInBetweenFinancialYear) {
                String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                        + " to <b>" + strFinancialYearEnd + "</b></html>";
                JLabel label = new JLabel(msg);
                JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                if (isOldEstimate) {
                    OldPrintAndSaveAction(isPrint);
                } else {
                    Boolean isApproved = false;
                    String partyName = (String) comboParty.getSelectedItem();
                    if (partyName.equals("No Party")) {
                        isApproved = true;
                    } else if (partyName.equals("New Party")) {
                        Boolean isSuccess = addNewPartyDetails();
                        if (isSuccess) { //Check this
//                    JOptionPane.showMessageDialog(null, "New Party is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                            ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
                            sortedLedgers = new ArrayList<Ledger>(ledgers);
                            Collections.sort(sortedLedgers, new Comparator<Ledger>() {
                                public int compare(Ledger o1, Ledger o2) {
                                    return o1.getLedgerName().compareTo(o2.getLedgerName());
                                }
                            });
                            comboParty.removeAllItems();
                            setParty();
                            ledgerData = findLedgerData();
                            isApproved = true;
                        } else {
                            JOptionPane.showMessageDialog(null, "New Party is not to be added", "Alert", JOptionPane.WARNING_MESSAGE);
                            isApproved = false;
                        }
                    } else {
                        ledgerData = findLedgerData();
                        isApproved = true;
                    }
                    if (isApproved) {
                        NewPrintAndSaveAction(isPrint);
                    }
                }
            }
        } else {
            new TrialPackageExpiryUI().setVisible(true);
        }
    }

    public void OldPrintAndSaveAction(Boolean isPrint) {
        setPartyEditable(true);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {

            try {

                float totalAmount = 0, totalProfit = 0, billDiscount = 0;

                for (int i = 0; i < model.getRowCount(); i++) {
                    totalAmount = (float) model.getValueAt(i, 18) + totalAmount;
                    totalProfit = (float) model.getValueAt(i, 19) + totalProfit;
                }
                float NetAmount = totalAmount;
                String strBillDiscount = txtBillDiscount.getText();
                if (!strBillDiscount.equals("")) {
                    billDiscount = Float.parseFloat(strBillDiscount);
                }
                NetAmount = NetAmount - billDiscount;
                float roundOff;
                String value = "0.00";
                try {
                    value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
                    if (value.equals("")) {
                        value = "0.00";
                    }
                    roundOff = Float.parseFloat(value);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please Enter Correct Value", "Alert", JOptionPane.WARNING_MESSAGE);
                    value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
                    if (value.equals("")) {
                        value = "0.00";
                    }
                    roundOff = Float.parseFloat(value);
                }
                NetAmount = NetAmount + roundOff;
                ledgerData = findLedgerData();

//            String addr = oldEstimate.getLedger().getAddress();
//            String[] words = addr.split("/~/");
                oldEstimate.setNoParty(noParty);
                oldEstimate.setEstimateDate(dateBill.getDate());
                oldEstimate.setLedger(ledgerData);
                oldEstimate.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
//newEstimate.setAuditData(auditData);
                oldEstimate.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
                oldEstimate.setCancelled(false);
                oldEstimate.setCompany(company);
                oldEstimate.setCurrentPaymentAmount(BigDecimal.ZERO);
                oldEstimate.setDiscountAmount(convertDecimal.Currency(currency.format(billDiscount)));
                oldEstimate.setDiscountPercent(0);
                oldEstimate.setModeOfDelivery("");
                oldEstimate.setPaymentType("");
                oldEstimate.setPricingPolicyName("");
                oldEstimate.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
                oldEstimate.setReferenceNumber("");
                oldEstimate.setRemarks(txtRemarks.getText());
                oldEstimate.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
                oldEstimate.setSmsMessage("");
                oldEstimate.setTotalAmountPaid(BigDecimal.ZERO);
                oldEstimate.setVariationAmount(convertDecimal.Currency(currency.format((NetAmount - totalAmount))));

                List<EstimateLineItem> estimateLineItems = new ArrayList<EstimateLineItem>();
                for (int i = 0; i < model.getRowCount(); i++) {

                    productDetail = new GetProduct().getProductByName((String) model.getValueAt(i, 1));
                    EstimateLineItem newEstimateLineItem = new EstimateLineItem();
                    //newEstimateLineItem.setAuditData(auditData);
                    newEstimateLineItem.setProduct(productDetail);
                    newEstimateLineItem.setLineNumber((int) model.getValueAt(i, 0));
                    newEstimateLineItem.setProductName((String) model.getValueAt(i, 1));
                    newEstimateLineItem.setHsnSac((String) model.getValueAt(i, 2));
                    newEstimateLineItem.setUqcOne((String) model.getValueAt(i, 3));
                    newEstimateLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                    newEstimateLineItem.setUqc1BaseRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 5))));
                    newEstimateLineItem.setUqcOneRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 6))));
                    newEstimateLineItem.setUqcOneValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 7))));
                    newEstimateLineItem.setUqcTwo((String) model.getValueAt(i, 8));
                    newEstimateLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 9));
                    newEstimateLineItem.setUqc2BaseRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 10))));
                    newEstimateLineItem.setUqcTwoRate(convertDecimal.Currency(currency.format(Float.parseFloat(model.getValueAt(i, 11).toString()))));
                    newEstimateLineItem.setUqcTwoValue(convertDecimal.Currency(currency.format(Float.parseFloat(model.getValueAt(i, 12).toString()))));
                    newEstimateLineItem.setStockQuantity((int) model.getValueAt(i, 14));
                    newEstimateLineItem.setValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 15))));
                    newEstimateLineItem.setDiscountPercent((float) model.getValueAt(i, 16));
                    newEstimateLineItem.setDiscountValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 17))));
                    newEstimateLineItem.setNetValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 18))));
                    newEstimateLineItem.setProfit(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 19))));
                    newEstimateLineItem.setCancelled(false);
                    newEstimateLineItem.setDescriptionOne("" + model.getValueAt(i, 20).toString());
                    newEstimateLineItem.setDescriptionTwo("" + model.getValueAt(i, 21).toString());
                    newEstimateLineItem.setDescriptionThree("" + model.getValueAt(i, 22).toString());
                    newEstimateLineItem.setDescriptionFour("" + model.getValueAt(i, 23).toString());
                    newEstimateLineItem.setDescriptionFive("" + model.getValueAt(i, 24).toString());
                    newEstimateLineItem.setDescriptionSix("" + model.getValueAt(i, 25).toString());
                    newEstimateLineItem.setDescriptionSeven("" + model.getValueAt(i, 26).toString());
                    newEstimateLineItem.setDescriptionEight("" + model.getValueAt(i, 27).toString());
                    newEstimateLineItem.setDescriptionNine("" + model.getValueAt(i, 28).toString());
                    newEstimateLineItem.setDescriptionTen("" + model.getValueAt(i, 29).toString());
                    newEstimateLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 30).toString()));

                    estimateLineItems.add(newEstimateLineItem);
                }
                EventStatus result = new EstimateLogic().updateEstimate(oldEstimate, estimateLineItems);
                if (result.isUpdateDone()) {
                    JOptionPane.showMessageDialog(null, "Estimate is Successfully updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                    if (isPrint) {
                        new EstimateBill().pdfByBillId(true, false, result.getBillID());
                    }
                    new EstimateDateWiseReportUI().setVisible(true);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);

                }
            } catch (ParseException ex) {
                Logger.getLogger(EstimateUI.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }

    public void NewPrintAndSaveAction(Boolean isPrint) {
        setPartyEditable(true);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {

            float totalAmount = 0, totalProfit = 0, billDiscount = 0;

            for (int i = 0; i < model.getRowCount(); i++) {
                totalAmount = (float) model.getValueAt(i, 18) + totalAmount;
                totalProfit = (float) model.getValueAt(i, 19) + totalProfit;
            }
            float NetAmount = totalAmount;
            String strBillDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strBillDiscount.equals("")) {
                billDiscount = Float.parseFloat(strBillDiscount);
            }
            NetAmount = NetAmount - billDiscount;
            float roundOff;
            String value = "0.00";
            try {
                value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
                if (value == null || value.equals("")) {
                    value = "0.00";
                }
                roundOff = Float.parseFloat(value);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Please Enter Correct Value", "Alert", JOptionPane.WARNING_MESSAGE);
                value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
                if (value.equals("")) {
                    value = "0.00";
                }
                roundOff = Float.parseFloat(value);
            }
            NetAmount = NetAmount + roundOff;
            ledgerData = findLedgerData();

//            String addr = oldEstimate.getLedger().getAddress();
//            String[] words = addr.split("/~/");
            Estimate newEstimate = new Estimate();

            if (noParty) {
                String strNoPartyName = txtPartyName.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyLineOne = txtAddrLine.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyCity = txtCity.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strNoPartyDist = txtDistrict.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                String strTxtGSTIN = txtGSTIN.getText().replaceAll("^\\s+", " ").replaceAll("^\\s+$", " ");
                newEstimate.setNoPartyName(strNoPartyName);
                newEstimate.setNoPartyLineOne(strNoPartyLineOne);
                newEstimate.setNoPartyCity(strNoPartyCity);
                newEstimate.setNoPartyDistrict(strNoPartyDist);
                newEstimate.setNoPartyState(comboState.getSelectedItem().toString());
                newEstimate.setNoPartyGSTIN(strTxtGSTIN);
//                newEstimate.setNoParty(noParty);
            } else {
                newEstimate.setNoPartyName("");
                newEstimate.setNoPartyLineOne("");
                newEstimate.setNoPartyCity("");
                newEstimate.setNoPartyDistrict("");
                newEstimate.setNoPartyState("");
                newEstimate.setNoPartyGSTIN("");
            }

            newEstimate.setNoParty(noParty);
            newEstimate.setEstimateDate(dateBill.getDate());
            newEstimate.setLedger(ledgerData);
            newEstimate.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
            //newEstimate.setAuditData(auditData);
            newEstimate.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
            newEstimate.setCancelled(false);
            newEstimate.setCompany(company);
            newEstimate.setCurrentPaymentAmount(BigDecimal.ZERO);
            newEstimate.setDiscountAmount(convertDecimal.Currency(currency.format(billDiscount)));
            newEstimate.setDiscountPercent(0);
            newEstimate.setModeOfDelivery("");
            newEstimate.setPaymentType("");
            newEstimate.setPricingPolicyName("");
            newEstimate.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
            newEstimate.setReferenceNumber("");
            newEstimate.setRemarks(txtRemarks.getText());
            newEstimate.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
            newEstimate.setSmsMessage("");
            newEstimate.setTotalAmountPaid(BigDecimal.ZERO);
            newEstimate.setVariationAmount(convertDecimal.Currency(currency.format((NetAmount - totalAmount))));

            List<EstimateLineItem> estimateLineItems = new ArrayList<EstimateLineItem>();
            for (int i = 0; i < model.getRowCount(); i++) {
                productDetail = new GetProduct().getProductByName((String) model.getValueAt(i, 1));
                EstimateLineItem newEstimateLineItem = new EstimateLineItem();
                newEstimateLineItem.setProduct(productDetail);
                newEstimateLineItem.setLineNumber((int) model.getValueAt(i, 0));
                newEstimateLineItem.setProductName((String) model.getValueAt(i, 1));
                newEstimateLineItem.setHsnSac((String) model.getValueAt(i, 2));
                newEstimateLineItem.setUqcOne((String) model.getValueAt(i, 3));
                newEstimateLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                newEstimateLineItem.setUqc1BaseRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 5))));
                newEstimateLineItem.setUqcOneRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 6))));
                newEstimateLineItem.setUqcOneValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 7))));
                newEstimateLineItem.setUqcTwo((String) model.getValueAt(i, 8));
                newEstimateLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 9));
                newEstimateLineItem.setUqc2BaseRate(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 10))));
                newEstimateLineItem.setUqcTwoRate(convertDecimal.Currency(currency.format(Float.parseFloat(model.getValueAt(i, 11).toString()))));
                newEstimateLineItem.setUqcTwoValue(convertDecimal.Currency(currency.format(Float.parseFloat(model.getValueAt(i, 12).toString()))));
                newEstimateLineItem.setStockQuantity((int) model.getValueAt(i, 14));
                newEstimateLineItem.setValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 15))));
                newEstimateLineItem.setDiscountPercent((float) model.getValueAt(i, 16));
                newEstimateLineItem.setDiscountValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 17))));
                newEstimateLineItem.setNetValue(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 18))));
                newEstimateLineItem.setProfit(convertDecimal.Currency(currency.format((float) model.getValueAt(i, 19))));
                newEstimateLineItem.setCancelled(false);
                newEstimateLineItem.setDescriptionOne("" + model.getValueAt(i, 20).toString());
                newEstimateLineItem.setDescriptionTwo("" + model.getValueAt(i, 21).toString());
                newEstimateLineItem.setDescriptionThree("" + model.getValueAt(i, 22).toString());
                newEstimateLineItem.setDescriptionFour("" + model.getValueAt(i, 23).toString());
                newEstimateLineItem.setDescriptionFive("" + model.getValueAt(i, 24).toString());
                newEstimateLineItem.setDescriptionSix("" + model.getValueAt(i, 25).toString());
                newEstimateLineItem.setDescriptionSeven("" + model.getValueAt(i, 26).toString());
                newEstimateLineItem.setDescriptionEight("" + model.getValueAt(i, 27).toString());
                newEstimateLineItem.setDescriptionNine("" + model.getValueAt(i, 28).toString());
                newEstimateLineItem.setDescriptionTen("" + model.getValueAt(i, 29).toString());
                newEstimateLineItem.setDescriptionCount(Integer.parseInt(model.getValueAt(i, 30).toString()));
                estimateLineItems.add(newEstimateLineItem);
            }

            EventStatus result = new EstimateLogic().createEstimate(newEstimate, estimateLineItems);
            if (result.isCreateDone()) {
                JOptionPane.showMessageDialog(null, "Estimate is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                comboParty.requestFocus();
                if (isPrint) {
                    new EstimateBill().pdfByBillId(true, false, result.getBillID());
                }
                clear(true);
            } else {
                JOptionPane.showMessageDialog(null, result.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        }

    }

    public void addServiceList() {
        boolean productUpdate = false;
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDisount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        String strProduct = "";
        strProduct = comboProduct.getSelectedItem().toString();
        if (comboProduct.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product to Add", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else if (strUQC1rate.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Rate.requestFocus();
//        } else if (isProductHereInTable(strProduct, -1) >= 0) {
//            isError = false;
//            JOptionPane.showMessageDialog(null, strProduct + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
//            comboProduct.requestFocus();
        } else {
            int count = jTable1.getRowCount() + 1;
            float UQC1rate = 0;
            float lineItemUQC1rate = 0;
            float lineItemAmt = 0, lineItemDiscount = 0, lineItemProfit = 0;

            int rowId = isProductHereInTable(strProduct, -1);

            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }

            if (rowId >= 0) {
                productUpdate = true;
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

                if ((model.getValueAt(rowId, 6) != null) && !(model.getValueAt(rowId, 6).equals(""))) {
                    lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 6).toString());
                }

                if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                    lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 17).toString());
                }
                if ((model.getValueAt(rowId, 18) != null) && !(model.getValueAt(rowId, 18).equals(""))) {
                    lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 18).toString());
                }
                if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                    lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 19).toString());
                }
                descAdded = true;
            }

            float netProductAmount = Float.parseFloat(txtAmount.getText());
            String Discountper = "0.00", Discount = "0.00";
            if (!strDiscountPer.equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            }
            if (!strDisount.equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            }
            int nUQC2Value = 0;
            float Baserate = 0;
            for (Product product : products) {
                if (product.getName().equals(comboProduct.getSelectedItem().toString())) {
                    if (product.getBaseProductRate() != null) {
                        Baserate = product.getBaseProductRate().floatValue();
                    } else {
                        Baserate = product.getProductRate().floatValue();
                    }
                    nUQC2Value = product.getUQC2Value();
                }
            }

            netProductAmount = netProductAmount + lineItemAmt;
            float discount = Float.parseFloat(Discount) + lineItemDiscount;
            if (discount != 0) {
                Discountper = currency.format((discount / netProductAmount) * 100);
                Discount = currency.format(discount);
            }

            float Profit = UQC1rate - Baserate;
            Profit = Profit - Float.parseFloat(Discount);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            bCheckStock = false;
            isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
//            if (isInventoryEnabled && !isUnderStockPossible) {
//                if (wholeQuantity <= productStock) {
//                    bCheckStock = true;
//                }
//            } else {
//                bCheckStock = true;
//            }
            Profit = Profit + lineItemProfit;
            Product selProduct = null;
            for (Product selectedProduct : products) {
                if (selectedProduct.getName().equalsIgnoreCase(strProduct)) {
                    selProduct = selectedProduct;
                    break;
                }
            }
            bCheckStock = true;
//            if (descAdded) {
//                desc1 = productDescriptionUI.prodDesc1;
//                desc2 = productDescriptionUI.prodDesc2;
//                desc3 = productDescriptionUI.prodDesc3;
//                desc4 = productDescriptionUI.prodDesc4;
//                desc5 = productDescriptionUI.prodDesc5;
//                desc6 = productDescriptionUI.prodDesc6;
//                desc7 = productDescriptionUI.prodDesc7;
//                desc8 = productDescriptionUI.prodDesc8;
//                desc9 = productDescriptionUI.prodDesc9;
//                desc10 = productDescriptionUI.prodDesc10;
//                descCount = productDescriptionUI.descCount;
//                productDescriptionUI.descAdded = false;
//            } else {
//                desc1 = selProduct.getDescriptionOne();
//                desc2 = selProduct.getDescriptionTwo();
//                desc3 = selProduct.getDescriptionThree();
//                desc4 = selProduct.getDescriptionFour();
//                desc5 = selProduct.getDescriptionFive();
//                desc6 = selProduct.getDescriptionSix();
//                desc7 = selProduct.getDescriptionSeven();
//                desc8 = selProduct.getDescriptionEight();
//                desc9 = selProduct.getDescriptionNine();
//                desc10 = selProduct.getDescriptionTen();
//                descCount = selProduct.getDescriptioncount();
//            }
            desc1 = "";
            desc2 = "";
            desc3 = "";
            desc4 = "";
            desc5 = "";
            desc6 = "";
            desc7 = "";
            desc8 = "";
            desc9 = "";
            desc10 = "";
            descCount = 0;
            if (bCheckStock) {
                Object[] row = {count, strProduct, HSNCode, "", 0, Baserate, UQC1rate, UQC1rate,
                    "", 0, Baserate, 0, 0.0, "", 0, netProductAmount + Float.parseFloat(Discount), Float.parseFloat(Discountper),
                    Float.parseFloat(Discount), netProductAmount, Profit,
                    desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, true};
                if (productUpdate) {
                    model.removeRow(rowId);
                    model.insertRow(rowId, row);
                } else {
                    model.addRow(row);
                }
                clear(false);
                jTableRender();
                TotalCalculate();
                comboProduct.requestFocus();
                btnProductDescription.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
        }
    }

    private void addProductList() {
        boolean isError = true;
        boolean productUpdate = false;
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strDisount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        String strProduct = "";
        strProduct = comboProduct.getSelectedItem().toString();
        if (comboProduct.getSelectedItem().equals("")) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Select the Product to Add", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else if (strUQC1qty.equals("") && strUQC2qty.equals("")) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
            isError = false;
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Rate.requestFocus();
//        } else if (isProductHereInTable(strProduct, -1) >= 0) {
//            isError = false;
//            JOptionPane.showMessageDialog(null, strProduct + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
//            comboProduct.requestFocus();
        } else {
            int count = jTable1.getRowCount() + 1;
            int wholeQuantity = 0;

            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
            float lineItemUQC1rate = 0, lineItemUQC2rate = 0;
            float lineItemAmt = 0, lineItemDiscount = 0, lineItemProfit = 0;
            int lineItemUQC1qty = 0, lineItemUQC2qty = 0;
            int rowId = isProductHereInTable(strProduct, -1);
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if (rowId >= 0) {
                productUpdate = true;
//                JOptionPane.showConfirmDialog(null,strProduct+" is Already in the list Do you want to add More", "CONFORM", JOptionPane.YES_NO_OPTION);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                if ((model.getValueAt(rowId, 4) != null) && !(model.getValueAt(rowId, 4).equals(""))) {
                    lineItemUQC1qty = Integer.parseInt(model.getValueAt(rowId, 4).toString());
                }
                if ((model.getValueAt(rowId, 6) != null) && !(model.getValueAt(rowId, 6).equals(""))) {
                    lineItemUQC1rate = Float.parseFloat(model.getValueAt(rowId, 6).toString());
                }

                if ((model.getValueAt(rowId, 9) != null) && !(model.getValueAt(rowId, 9).equals(""))) {
                    lineItemUQC2qty = Integer.parseInt(model.getValueAt(rowId, 9).toString());
                }
                if ((model.getValueAt(rowId, 11) != null) && !(model.getValueAt(rowId, 11).equals(""))) {
                    lineItemUQC2rate = Float.parseFloat(model.getValueAt(rowId, 11).toString());
                }

                if ((model.getValueAt(rowId, 17) != null) && !(model.getValueAt(rowId, 17).equals(""))) {
                    lineItemDiscount = Float.parseFloat(model.getValueAt(rowId, 17).toString());
                }
                if ((model.getValueAt(rowId, 18) != null) && !(model.getValueAt(rowId, 18).equals(""))) {
                    lineItemAmt = Float.parseFloat(model.getValueAt(rowId, 18).toString());
                }
                if ((model.getValueAt(rowId, 19) != null) && !(model.getValueAt(rowId, 19).equals(""))) {
                    lineItemProfit = Float.parseFloat(model.getValueAt(rowId, 19).toString());
                }
                descAdded = true;
                UQC1qty = UQC1qty + lineItemUQC1qty;
                //UQC1rate = (UQC1rate + lineItemUQC1rate) / 2;
                UQC2qty = UQC2qty + lineItemUQC2qty;
                //UQC2rate = (UQC2rate + lineItemUQC2rate) / 2;
            }

            String strQuantity = "";
            if (UQC1qty == 0) {
                strQuantity = UQC2qty + " " + labelUQC2.getText();
            } else if (UQC2qty == 0) {
                strQuantity = UQC1qty + " " + labelUQC1.getText();
            } else {
                strQuantity = UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText();
            }

            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;

            float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                    netProductAmount = Float.parseFloat(txtAmount.getText());
            String Discountper = "0.00", Discount = "0.00";
            if (!strDiscountPer.equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            }
            if (!strDisount.equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            }
            int nUQC2Value = 0;
            for (Product product : products) {
                if (product.getName().equals(comboProduct.getSelectedItem().toString())) {
                    if (product.getBaseProductRate() != null) {
                        UQC2Baserate = product.getBaseProductRate().floatValue();
                    } else {
                        UQC2Baserate = product.getProductRate().floatValue();
                    }

                    nUQC2Value = product.getUQC2Value();
                }
            }

            netProductAmount = netProductAmount + lineItemAmt;
            float discount = Float.parseFloat(Discount) + lineItemDiscount;
            if (discount != 0) {
                Discountper = currency.format((discount / netProductAmount) * 100);
                Discount = currency.format(discount);
            }

            float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - (((UQC1qty * nUQC2Value) + (UQC2qty)) * UQC2Baserate);
            Profit = Profit - Float.parseFloat(Discount);
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            bCheckStock = false;
            isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
//            if (isInventoryEnabled && !isUnderStockPossible) {
//                if (wholeQuantity <= productStock) {
//                    bCheckStock = true;
//                }
//            } else {
//                bCheckStock = true;
//            }
            Profit = Profit + lineItemProfit;
            Product selProduct = null;
            for (Product selectedProduct : products) {
                if (selectedProduct.getName().equalsIgnoreCase(strProduct)) {
                    selProduct = selectedProduct;
                    break;
                }
            }
            bCheckStock = true;
            if (descAdded) {
                desc1 = productDescriptionUI.prodDesc1;
                desc2 = productDescriptionUI.prodDesc2;
                desc3 = productDescriptionUI.prodDesc3;
                desc4 = productDescriptionUI.prodDesc4;
                desc5 = productDescriptionUI.prodDesc5;
                desc6 = productDescriptionUI.prodDesc6;
                desc7 = productDescriptionUI.prodDesc7;
                desc8 = productDescriptionUI.prodDesc8;
                desc9 = productDescriptionUI.prodDesc9;
                desc10 = productDescriptionUI.prodDesc10;
                descCount = productDescriptionUI.descCount;
                productDescriptionUI.descAdded = false;
            } else {
                desc1 = selProduct.getDescriptionOne();
                desc2 = selProduct.getDescriptionTwo();
                desc3 = selProduct.getDescriptionThree();
                desc4 = selProduct.getDescriptionFour();
                desc5 = selProduct.getDescriptionFive();
                desc6 = selProduct.getDescriptionSix();
                desc7 = selProduct.getDescriptionSeven();
                desc8 = selProduct.getDescriptionEight();
                desc9 = selProduct.getDescriptionNine();
                desc10 = selProduct.getDescriptionTen();
                descCount = selProduct.getDescriptioncount();
            }
            if (bCheckStock && isError) {
                Object[] row = {count, strProduct, HSNCode, labelUQC1.getText(), UQC1qty, UQC1Baserate, UQC1rate, UQC1qty * UQC1rate,
                    labelUQC2.getText(), UQC2qty, UQC2Baserate, UQC2rate, UQC2qty * UQC2rate, strQuantity,
                    wholeQuantity, netProductAmount + Float.parseFloat(Discount), Float.parseFloat(Discountper), Float.parseFloat(Discount), netProductAmount, Profit,
                    desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, false};
                if (productUpdate) {
                    model.removeRow(rowId);
                    model.insertRow(rowId, row);
                } else {
                    int position = findInsertRowofProduct();
                    if (position == 0) {
                        model.addRow(row);
                    } else {
                        model.insertRow(position, row);
                    }
                }
                clear(false);
                jTableRender();
                TotalCalculate();
                comboProduct.requestFocus();
                btnProductDescription.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
        }
    }

    private int findInsertRowofProduct() {
        int rowPosition = 0;
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if ((Boolean) model.getValueAt(i, 31)) {
                rowPosition = i;
                break;
            }
        }
        return rowPosition;
    }

    private void updateProductItem() {

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            boolean isError = true;
            String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strDisount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

            String strProduct = "";
            strProduct = comboProduct.getSelectedItem().toString();
            if (comboProduct.getSelectedItem().equals("")) {
                isError = false;
                JOptionPane.showMessageDialog(null, "Please Select the Product to Add", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else if (strUQC1qty.equals("") && strUQC2qty.equals("")) {
                isError = false;
                JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
                isError = false;
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Rate.requestFocus();
            } else if (isProductHereInTable(strProduct, i) > -1) {
                isError = false;
                JOptionPane.showMessageDialog(null, strProduct + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else {
                int count = (int) model.getValueAt(i, 0);
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strUQC2rate.equals("")) {
                    UQC2rate = Float.parseFloat(strUQC2rate);
                }

                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;

                float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                        netProductAmount = Float.parseFloat(txtAmount.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!strDiscountPer.equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!strDisount.equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                int nUQC2Value = 0;
                for (Product product : products) {
                    if (product.getName().equals(comboProduct.getSelectedItem().toString())) {
                        if (product.getBaseProductRate() != null) {
                            UQC2Baserate = product.getBaseProductRate().floatValue();
                        } else {
                            UQC2Baserate = product.getProductRate().floatValue();
                        }
                        nUQC2Value = product.getUQC2Value();
                    }
                }

                if (descAdded) {
                    desc1 = productDescriptionUI.prodDesc1;
                    desc2 = productDescriptionUI.prodDesc2;
                    desc3 = productDescriptionUI.prodDesc3;
                    desc4 = productDescriptionUI.prodDesc4;
                    desc5 = productDescriptionUI.prodDesc5;
                    desc6 = productDescriptionUI.prodDesc6;
                    desc7 = productDescriptionUI.prodDesc7;
                    desc8 = productDescriptionUI.prodDesc8;
                    desc9 = productDescriptionUI.prodDesc9;
                    desc10 = productDescriptionUI.prodDesc10;
                    descCount = productDescriptionUI.descCount;
                }
                float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - (((UQC1qty * nUQC2Value) + (UQC2qty)) * UQC2Baserate);
                Profit = Profit - Float.parseFloat(Discount);
                bCheckStock = false;
                isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
//            if (isInventoryEnabled && !isUnderStockPossible) {
//                if (wholeQuantity <= productStock) {
//                    bCheckStock = true;
//                }
//            } else {
//                bCheckStock = true;
//            }
                bCheckStock = true;
                if (bCheckStock && isError) {
                    Object[] row = {count, strProduct, HSNCode, labelUQC1.getText(), UQC1qty, UQC1Baserate, UQC1rate, UQC1qty * UQC1rate,
                        labelUQC2.getText(), UQC2qty, UQC2Baserate, UQC2rate, UQC2qty * UQC2rate, UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(),
                        wholeQuantity, netProductAmount + Float.parseFloat(Discount), Float.parseFloat(Discountper), Float.parseFloat(Discount), netProductAmount, Profit,
                        desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, false};
                    model.removeRow(i);
                    model.insertRow(i, row);
                    clear(false);
                    jTableRender();
                    TotalCalculate();
                    comboProduct.requestFocus();
                    btnProductDescription.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtUQC1Qty.requestFocus();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void updateServiceItem() {

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            String strDisount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

            String strProduct = "";
            strProduct = comboProduct.getSelectedItem().toString();
            if (comboProduct.getSelectedItem().equals("")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product to Add", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else if ((strUQC1rate.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Rate.requestFocus();
            } else if (isProductHereInTable(strProduct, i) > -1) {
                JOptionPane.showMessageDialog(null, strProduct + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
                comboProduct.requestFocus();
            } else {
                int count = (int) model.getValueAt(i, 0);

                float UQC1rate = 0, UQC1Baserate = 0;

                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }

                float netProductAmount = Float.parseFloat(txtAmount.getText());
                String Discountper = "0.00", Discount = "0.00";
                if (!strDiscountPer.equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!strDisount.equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                int nUQC2Value = 0;
                float baseRate = 0;
                for (Product product : products) {
                    if (product.getName().equals(comboProduct.getSelectedItem().toString())) {
                        if (product.getBaseProductRate() != null) {
                            baseRate = product.getBaseProductRate().floatValue();
                        } else {
                            baseRate = product.getProductRate().floatValue();
                        }
                        nUQC2Value = product.getUQC2Value();
                    }
                }

                if (descAdded) {
                    desc1 = productDescriptionUI.prodDesc1;
                    desc2 = productDescriptionUI.prodDesc2;
                    desc3 = productDescriptionUI.prodDesc3;
                    desc4 = productDescriptionUI.prodDesc4;
                    desc5 = productDescriptionUI.prodDesc5;
                    desc6 = productDescriptionUI.prodDesc6;
                    desc7 = productDescriptionUI.prodDesc7;
                    desc8 = productDescriptionUI.prodDesc8;
                    desc9 = productDescriptionUI.prodDesc9;
                    desc10 = productDescriptionUI.prodDesc10;
                    descCount = productDescriptionUI.descCount;
                }
                float Profit = UQC1rate - baseRate;
                Profit = Profit - Float.parseFloat(Discount);
                bCheckStock = false;
                isUnderStockPossible = SessionDataUtil.getCompanyPolicyData().isUnderStockPossible();
//            if (isInventoryEnabled && !isUnderStockPossible) {
//                if (wholeQuantity <= productStock) {
//                    bCheckStock = true;
//                }
//            } else {
//                bCheckStock = true;
//            }
                bCheckStock = true;
                if (bCheckStock) {
                    Object[] row = {count, strProduct, HSNCode, "", 0, baseRate, UQC1rate, UQC1rate,
                        "", 0, baseRate, 0, 0.0, "",
                        0, netProductAmount + Float.parseFloat(Discount), Float.parseFloat(Discountper), Float.parseFloat(Discount), netProductAmount, Profit,
                        desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10, descCount, true};
                    model.removeRow(i);
                    model.insertRow(i, row);
                    clear(false);
                    jTableRender();
                    TotalCalculate();
                    comboProduct.requestFocus();
                    btnProductDescription.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "Insufficient Stock", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtUQC1Qty.requestFocus();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }
    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (isServiceProduct) {
            updateServiceItem();
        } else {
            updateProductItem();
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        SaveAndPrint(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear(false);
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtUQC1RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateActionPerformed
        if (txtUQC2Rate.isEnabled()) {
            txtUQC2Rate.requestFocus();
        } else {
            txtDiscountPer.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1RateActionPerformed

    private void txtUQC1RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyReleased

        DecimalFormat dcm = new DecimalFormat("#.0000");
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strRate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strRate.equals("")) {
                txtUQC2Rate.setText("");
            } else {
                float fRate1 = Float.parseFloat(strRate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fRate2 = fRate1 / Uqc2Value;
                if (isServiceProduct) {
                    txtUQC2Rate.setText("");
                } else {
                    txtUQC2Rate.setText(dcm.format(fRate2));
                }
            }
        }

        if (isServiceProduct) {
            TotalCalculationForService(true);
        } else {
            TotalCalculationForProduct(true);
        }

    }//GEN-LAST:event_txtUQC1RateKeyReleased

    private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
        txtDiscount.requestFocus();

    }//GEN-LAST:event_txtDiscountPerActionPerformed

    private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDiscountPerKeyPressed

    private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
        if (isServiceProduct) {
            TotalCalculationForService(true);
        } else {
            TotalCalculationForProduct(true);
        }
    }//GEN-LAST:event_txtDiscountPerKeyReleased

    private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped

    }//GEN-LAST:event_txtDiscountPerKeyTyped

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        } else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
        if (isServiceProduct) {
            TotalCalculationForService(false);
        } else {
            TotalCalculationForProduct(false);
        }
    }//GEN-LAST:event_txtDiscountKeyReleased

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            comboProduct.setSelectedItem(model.getValueAt(i, 1));
            getProductDetails();
            btnProductDescription.setEnabled(isServiceProduct);
            comboProduct.setEnabled(false);
            HSNCode = (String) model.getValueAt(i, 2);
            labelUQC1.setText((String) model.getValueAt(i, 3));
            txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
            txtUQC1Rate.setText(currency.format(model.getValueAt(i, 6)));
            labelUQC2.setText((String) model.getValueAt(i, 8));
            txtUQC2Qty.setText(model.getValueAt(i, 9).toString());
            txtUQC2Rate.setText(currency.format(model.getValueAt(i, 11)));
            txtDiscountPer.setText(model.getValueAt(i, 16).toString());
            txtDiscount.setText(currency.format(model.getValueAt(i, 17)));
            txtAmount.setText(currency.format(model.getValueAt(i, 18)));
            if (isServiceProduct) {
                labelUQC1.setText("");
                txtUQC1Qty.setText("");
                labelUQC2.setText("");
                txtUQC2Qty.setText("");
                txtUQC2Rate.setText("");
            }
            btnAdd.setEnabled(false);
            btnUpdate.setEnabled(true);
            btnDelete.setEnabled(true);
            comboProduct.requestFocus();
            comboProduct.setPopupVisible(false);
            btnProductDescription.setEnabled(true);
        }

    }//GEN-LAST:event_jTable1MouseClicked

    private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
        TotalCalculationForProduct(true);
    }//GEN-LAST:event_txtUQC2QtyKeyReleased

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
        TotalCalculationForProduct(true);
    }//GEN-LAST:event_txtUQC1QtyKeyReleased

    private void txtUQC2RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyReleased
        if (evt.getKeyCode() != KeyEvent.VK_ENTER) {
            String strUQC2Rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strUQC2Rate.equals("")) {
                txtUQC1Rate.setText("");
            } else {
                float fUQC2Rate = Float.parseFloat(strUQC2Rate);
                int Uqc2Value = productDetail.getUQC2Value();
                float fUQC1Rate = fUQC2Rate * Uqc2Value;
                if (isServiceProduct) {
                    txtUQC2Rate.setText("");
                } else {
                    txtUQC1Rate.setText(currency.format(fUQC1Rate));
                }
            }
        }
        TotalCalculationForProduct(true);
    }//GEN-LAST:event_txtUQC2RateKeyReleased

    private void txtUQC1RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyTyped

    private void txtUQC2RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyTyped

    private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC1QtyKeyTyped

    private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC2QtyKeyTyped


    private void txtUQC2RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateActionPerformed

        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateActionPerformed

    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        if (bUQCSame) {
            txtUQC1Rate.requestFocus();
//            if (btnAdd.isEnabled()) {
//                btnAdd.requestFocus();
//            } else {
//                btnUpdate.requestFocus();
//            }
        } else {
            txtUQC2Qty.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
        txtUQC1Rate.requestFocus();

    }//GEN-LAST:event_txtUQC2QtyActionPerformed

    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (isServiceProduct) {
                updateServiceItem();
            } else {
                updateProductItem();
            }

            evt.consume();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed


    private void comboProductKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboProductKeyPressed

        Boolean isPrint = false;
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_S) {
            isPrint = false;
            if (isOldEstimate) {
                OldPrintAndSaveAction(isPrint);
            } else {
                NewPrintAndSaveAction(isPrint);
            }

        } else if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_P) {
            isPrint = true;
            if (isOldEstimate) {
                OldPrintAndSaveAction(isPrint);
            } else {
                NewPrintAndSaveAction(isPrint);
            }
        }
//        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//            getProductDetails();
//            txtUQC1Qty.requestFocus();
//            evt.consume();
//        }


    }//GEN-LAST:event_comboProductKeyPressed

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained

    }//GEN-LAST:event_comboPartyFocusGained

    private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed

    }//GEN-LAST:event_comboPartyKeyPressed

    private void comboPartyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyReleased

    }//GEN-LAST:event_comboPartyKeyReleased

    private void comboStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboStateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtGSTIN.requestFocus();
        }
    }//GEN-LAST:event_comboStateKeyPressed

    private void txtGSTINKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGSTINKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int errorReport = new FormValidation().checkGSTIN(txtGSTIN.getText());
            if (errorReport == 1) {
                JOptionPane.showMessageDialog(null, "First two digits must be a State Code", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            } else if (errorReport == 2) {
                JOptionPane.showMessageDialog(null, "One Digit Prior to the last must be Z", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            } else if (errorReport == 3) {
                JOptionPane.showMessageDialog(null, "Invalid GSTIN Number", "Alert", JOptionPane.WARNING_MESSAGE);
                txtGSTIN.requestFocus();
            } else if (errorReport == 0) {
                comboProduct.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_txtGSTINKeyPressed

    private void txtUQC1QtyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUQC1QtyFocusGained
        if (comboProduct.getSelectedItem().equals("") || comboProduct.getSelectedItem() == null) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyFocusGained

    private void comboStateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboStateFocusGained
        comboState.showPopup();
    }//GEN-LAST:event_comboStateFocusGained

    private void txtUQC2QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC2QtyKeyPressed

    private void txtUQC1RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyPressed

    private void txtUQC2RateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtDiscountPer.requestFocus();

        }
    }//GEN-LAST:event_txtUQC2RateKeyPressed

    private void txtDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
        } else {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            }
        }
    }//GEN-LAST:event_txtDiscountKeyPressed

    private void comboProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductActionPerformed

    }//GEN-LAST:event_comboProductActionPerformed

    private void txtPartyNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartyNameKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
                comboProduct.requestFocus();
            }
        }
    }//GEN-LAST:event_txtPartyNameKeyPressed

    private void txtAddrLineKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddrLineKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtAddrLineKeyPressed

    private void txtCityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtCityKeyPressed

    private void txtDistrictKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDistrictKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_txtDistrictKeyPressed

    private void txtUQC1RateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUQC1RateFocusGained
        String strUQC1 = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("^\\s+$", "");
        String strUQC2 = txtUQC2Qty.getText().replaceAll("\\s+", "").replaceAll("^\\s+$", "");
        if (!isServiceProduct) {
            if (strUQC1.equals("") && strUQC2.equals("")) {
                JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value check", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            }
        }
    }//GEN-LAST:event_txtUQC1RateFocusGained

    private void txtBillDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBillDiscountActionPerformed

    }//GEN-LAST:event_txtBillDiscountActionPerformed

    private void txtBillDiscountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyPressed
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (btnAdd.isEnabled()) {
                btnAdd.requestFocus();
            } else {
                btnUpdate.requestFocus();
            }
            evt.consume();
        }
    }//GEN-LAST:event_txtBillDiscountKeyPressed

    private void txtBillDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyReleased

        if (evt.getKeyCode() != KeyEvent.VK_ENTER && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE && Character.isDigit(evt.getKeyChar())) {
            discount = Float.parseFloat(txtBillDiscount.getText());
            amount = amountWithoutDiscount - discount;
            labelTotalAmount.setText("Total Ampount : " + String.valueOf(amount));
            discountPer = (discount / amountWithoutDiscount) * 100;
        } else if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && (amount < amountWithoutDiscount)) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscount.equals("")) {
                Float fDiscount = Float.parseFloat(txtBillDiscount.getText());
                if (!(isOldEstimate) || (!(fDiscount == oldDiscount))) {
                    amount = amount + (discount - fDiscount);
                    discount = discount - (discount - fDiscount);
                    labelTotalAmount.setText("Total Ampount : " + String.valueOf(amount));
                    discountPer = (discount / amountWithoutDiscount) * 100;
                    labelTotalAmount.setText("Total Ampount : " + (String.valueOf(amount)));
                }
            } else {
                labelTotalAmount.setText("Total Ampount : " + (String.valueOf(amountWithoutDiscount)));
                discountPer = 0;
            }
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (strDiscount.equals("")) {
                txtBillDiscount.setText("0");
            }
            btnSave.requestFocus();
        }
        evt.consume();
    }//GEN-LAST:event_txtBillDiscountKeyReleased

    private void txtBillDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtBillDiscount.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_MINUS))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtBillDiscountKeyTyped

    private void btnProductDescriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductDescriptionActionPerformed
        descAdded = true;
        Product selProduct = null;
        String productName = (String) comboProduct.getEditor().getItem();
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                selProduct = product;
                break;
            }
        }
        int p = isProductHereInTable(productName, -1);
        if (jTable1.getSelectedRow() > -1 || p > -1) {
            productDescriptionUI = new EstimationDescriptionUI1(desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc10);
            productDescriptionUI.setVisible(true);
        } else if (selProduct != null) {
            try {
                productDescriptionUI = new EstimationDescriptionUI1(selProduct);

            } catch (IOException ex) {
                Logger.getLogger(EstimateUI.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            productDescriptionUI.setVisible(true);
        } else {
            productDescriptionUI = new EstimationDescriptionUI1();
            productDescriptionUI.setVisible(true);
        }
    }//GEN-LAST:event_btnProductDescriptionActionPerformed

    private void comboStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboStateActionPerformed
//        StateCode obj = new StateCode();
//        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_comboStateActionPerformed

    private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGSTINActionPerformed

    private void txtGSTINFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGSTINFocusLost
        String str = txtGSTIN.getText();
        if (str.length() == 2) {
            txtGSTIN.setText("");
        }
    }//GEN-LAST:event_txtGSTINFocusLost

    private void txtGSTINFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGSTINFocusGained
//        StateCode obj = new StateCode();
//        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_txtGSTINFocusGained

    private void txtGSTINMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtGSTINMouseClicked
        StateCode obj = new StateCode();
        txtGSTIN.setText(obj.getStateCode((String) comboState.getSelectedItem()));
    }//GEN-LAST:event_txtGSTINMouseClicked

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        } else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        } else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void TotalCalculate() {
        float fTotal = 0.00f, discount = 0;
        DefaultTableModel tModel = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < tModel.getRowCount(); i++) {
            fTotal = fTotal + Float.parseFloat(tModel.getValueAt(i, 18).toString());
        }
        String strDiscount = txtBillDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if (!strDiscount.equals("")) {
            discount = Float.parseFloat(strDiscount);
        }
        amountWithoutDiscount = fTotal;
        amount = amountWithoutDiscount - discount;
        labelTotalAmount.setText("Total Amount : " + currency.format(amount));

    }

    public int isProductHereInTable(String productName, int row) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int bProductAlreadyAdded = -1;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (i != row) {
                if (model.getValueAt(i, 1).toString().equals(productName)) {
                    bProductAlreadyAdded = i;
                }
            }
        }
        return bProductAlreadyAdded;
    }

    public boolean validateFields() {
        boolean bValidate = false;
        int errorReport = new FormValidation().checkGSTIN(txtGSTIN.getText());
        if (dateBill.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please Select Date");
            dateBill.requestFocusInWindow();
            return bValidate;
        } else if (txtPartyName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Party Name");
            txtPartyName.requestFocus();
            return bValidate;
        } else if (comboState.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Select the State");
            comboState.requestFocus();
            return bValidate;
        } else if (errorReport == 1) {
            JOptionPane.showMessageDialog(null, "First two digits must be a State Code", "Alert", JOptionPane.WARNING_MESSAGE);
            txtGSTIN.requestFocus();
            return bValidate;
        } else if (errorReport == 2) {
            JOptionPane.showMessageDialog(null, "One Digit Prior to the last must be Z", "Alert", JOptionPane.WARNING_MESSAGE);
            txtGSTIN.requestFocus();
            return bValidate;
        } else if (errorReport == 3) {
            JOptionPane.showMessageDialog(null, "Invalid GSTIN Number", "Alert", JOptionPane.WARNING_MESSAGE);
            txtGSTIN.requestFocus();
            return bValidate;
        }

        return true;
    }

    public void getPartyDetails() {
        String strAddr = "";
        String name = (String) comboParty.getSelectedItem();
        if (name.equals("")) {
            txtPartyName.setText("");
            txtAddrLine.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            comboState.setSelectedItem("");
            txtGSTIN.setText("");
        } else {
            if (name.equalsIgnoreCase("no party")) {
                noParty = true;
                dateBill.requestFocusInWindow();
                txtPartyName.setText("");
                txtAddrLine.setText("");
                txtCity.setText("");
                txtDistrict.setText("");
                comboState.setSelectedItem("Tamil Nadu");
                txtGSTIN.setText("");
            } else if (name.equalsIgnoreCase("new party")) {
                noParty = false;
                dateBill.requestFocusInWindow();
                txtPartyName.setText("");
                txtAddrLine.setText("");
                txtCity.setText("");
                txtDistrict.setText("");
                comboState.setSelectedItem("Tamil Nadu");
                txtGSTIN.setText("");
            } else {
                noParty = false;
                for (Ledger ledger : sortedLedgers) {
                    if (ledger.getLedgerName().equals(name)) {
                        ledgerData = ledger;
                        LedgerId = ledger.getLedgerId();
                        txtPartyName.setText(ledger.getLedgerName());
                        String address = ledger.getAddress();
                        String[] words = address.split("/~/");
                        for (int ii = 0; ii < words.length; ii++) {
                            if (ii == 0) {
                                strAddr = words[ii];
                            } else if (!words[ii].equals(",") && !words[ii].trim().equals("")) {
                                strAddr = strAddr + ", " + words[ii];
                            }
                        }
                        txtAddrLine.setText(strAddr);
                        txtCity.setText(ledger.getCity());
                        txtDistrict.setText(ledger.getDistrict());
                        comboState.setSelectedItem(ledger.getState());
                        txtGSTIN.setText(ledger.getGSTIN());
                        dateBill.requestFocusInWindow();
//                        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
//                        if (model.getRowCount() > 0) {
//                            model.setRowCount(0);
//                        }
                        break;
                    }
                }
            }
        }
    }

    public void setParty() {
        String[] myArray = new String[0];
        DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
        comboParty.setModel(model);
        comboParty.addItem("No Party");
        comboParty.addItem("New Party");
        for (Ledger ledger : sortedLedgers) {
            comboParty.addItem(ledger.getLedgerName());
        }
    }

    private void printData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean isPrint = true;
                SaveAndPrint(isPrint);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnPrint.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnPrint.getActionMap();
        actionMap.put("Action", actionListener);
        btnPrint.setActionMap(actionMap);
    }

    private void saveData() {
        Action actionListener = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean isPrint = false;
                SaveAndPrint(isPrint);
            }
        };
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        InputMap inputMap = btnSave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ks, "Action");
        ActionMap actionMap = btnSave.getActionMap();
        actionMap.put("Action", actionListener);
        btnSave.setActionMap(actionMap);
    }

    public Boolean checkPartyName(String name) {
        Boolean isFound = false;
        for (Ledger ledger : ledgers) {
            if (ledger.getLedgerName().equals(name)) {
                isFound = true;
            }
        }
        return isFound;
    }

    public Boolean addNewPartyDetails() {
        String Address = "";
        Boolean isSuccess = false;
        FormValidation formValidate = new FormValidation();
        String strPartyName = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isAlreadyHere = checkPartyName(strPartyName);
        String strGSTIN = txtGSTIN.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        int errorReport = formValidate.checkGSTIN(txtGSTIN.getText());
//        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateOpeningBalance.getDate());
        if (strPartyName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter the Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        } else if (isAlreadyHere) {
            JOptionPane.showMessageDialog(null, strPartyName + " is already here.", "Alert", JOptionPane.WARNING_MESSAGE);
            txtPartyName.requestFocus();
        } else if (errorReport != 0) {
            switch (errorReport) {
                case 1:
                    JOptionPane.showMessageDialog(null, "First two digits must be a State Code", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null, "One Digit Prior to the last must be Z", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                case 3:
                    JOptionPane.showMessageDialog(null, "Invalid GSTIN Number", "Alert", JOptionPane.WARNING_MESSAGE);
                    txtGSTIN.requestFocus();
                    bError = true;
                    break;
                default:
                    break;
            }
        } else {
            String addr1 = txtAddrLine.getText();
            if (addr1.equals("")) {
                addr1 = " ";
            } else if (addr1.contains(",")) {
                String[] words = addr1.split(",");
                for (int ii = 0; ii < words.length; ii++) {
                    if (ii == 0) {
                        Address = words[ii];
                    } else if (!words[ii].equals(" ")) {
                        Address = Address + "/~/" + words[ii];
                    }
                }
            }
//             else {
//                String[] words = addr1.split(" ");
//                for (int ii = 0; ii < words.length; ii++) {
//                    if (ii == 0) {
//                        Address = words[ii];
//                    } else if (!words[ii].equals(" ")) {
//                        Address = Address + "/~/" + words[ii];
//                    }
//                }
//            }
            Ledger newLedger = new Ledger();

            newLedger.setLedgerName(strPartyName);
            newLedger.setAddress(Address);
            newLedger.setCity(txtCity.getText());
            newLedger.setDistrict(txtDistrict.getText());
            newLedger.setState((String) comboState.getSelectedItem());
            newLedger.setEmail("");
            newLedger.setPhone("");
            newLedger.setMobile("");
            newLedger.setGSTIN(txtGSTIN.getText());
            newLedger.setOpeningBalanceDate(new Date());
            newLedger.setOpeningBalance(new BigDecimal(0));

            isSuccess = new LedgerLogic().createPartyLedger(companyId, newLedger, false);

        }
        return isSuccess;
    }

    public Ledger findLedgerData() {
        String name = txtPartyName.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Ledger ledgerData = null;
        for (Ledger ledger : sortedLedgers) {
            if (ledger.getLedgerName().equals(name)) {
                ledgerData = ledger;
                LedgerId = ledger.getLedgerId();
                break;
            }
        }
        return ledgerData;
    }

//    public void setStock() {
//        GoDown selGodown = godowns.get(0);
////        for (GoDown godown : godowns) {
////            if (comboGodown.getSelectedItem().toString().equals(godown.getGoDownName())) {
////                selGodown = godown;
////                break;
////            }
////        }
//        Product selProduct = null;
//        String productName = (String) comboProduct.getEditor().getItem();
//        for (Product product : products) {
//            if (product.getName().equals(productName)) {
//                selProduct = product;
//                break;
//            }
//        }
//        stock = new GoDownStockCRUD().getGoDownStockDetail(selProduct.getProductId(), selGodown.getGoDownId());
//        productStock = stock.getCurrentStock();
//        if (isOldBill) {
//            List<PurchaseSaleLineItem> psLIs = oldEstimate.getPsLineItems();
//            for (PurchaseSaleLineItem item : psLIs) {
//                if (item.getProductName().equals(productName)) {
//                    productStock = productStock + item.getStockQuantity();
//                }
//            }
//        }
////        if (isInventoryEnabled) {
////            labelStock.setText(setStockConverstion(productStock, selProduct.getUQC1(), selProduct.getUQC2(), selProduct.getUQC2Value()));
////        } else {
////            labelStock.setText("N/A");
////        }
//    }
    public void setPartyEditable(boolean isEditable) {
        txtPartyName.setEnabled(isEditable);
        dateBill.setEnabled(isEditable);
        comboParty.setEnabled(isEditable);
        txtAddrLine.setEnabled(isEditable);
        txtCity.setEnabled(isEditable);
        txtDistrict.setEnabled(isEditable);
        comboState.setEnabled(isEditable);
        txtGSTIN.setEnabled(isEditable);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EstimateUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EstimateUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EstimateUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EstimateUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //WebLookAndFeel.install ();
        com.alee.laf.WebLookAndFeel.install();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EstimateUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProductDescription;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboProduct;
    private javax.swing.JComboBox<String> comboState;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelTotalAmount;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1forPrice;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2forPrice;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JPanel panelUQC;
    private javax.swing.JTextField txtAddrLine;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JTextField txtBillDiscount;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JTextField txtPartyName;
    private javax.swing.JTextArea txtRemarks;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC1Rate;
    private javax.swing.JTextField txtUQC2Qty;
    private javax.swing.JTextField txtUQC2Rate;
    // End of variables declaration//GEN-END:variables
}
