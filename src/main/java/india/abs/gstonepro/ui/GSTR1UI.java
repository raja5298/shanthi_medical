/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.ui;

import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.GstReport;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.GSTOneReportsLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.FloatValueTableCellRenderer;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.write.NumberFormat;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.DateTime;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import india.abs.gstonepro.unusedui.PaymentGrpUI;
/**
 *
 * @author Admin
 */
public class GSTR1UI extends javax.swing.JFrame {

    /**
     * Creates new form GSTR1UI
     */
    Company company = SessionDataUtil.getSelectedCompany();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();

    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Boolean isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled(),
            isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled();
    HashMap<String, Integer> stateCode = null;

    public GSTR1UI() {
        try {
            initComponents();
            //            Image im = ImageIO.read(getClass().getResource(SystemPolicyUtil.getSystemPolicy().getImageURL()));

            //        setIconImage(im);
            //        im.flush();
            setMenuRoles();
            jTabbedPane1.setTabPlacement(JTabbedPane.BOTTOM);
            jTabbedPane1.setEnabledAt(3, true);
            jTabbedPane1.setEnabledAt(4, true);
            jTabbedPane1.setEnabledAt(5, false);
            jTabbedPane1.setEnabledAt(6, false);
            jTabbedPane1.setEnabledAt(7, false);
            jTabbedPane1.setEnabledAt(8, false);
            jTabbedPane1.setEnabledAt(10, false);

            menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
            setRoles();
            setExtendedState(this.MAXIMIZED_BOTH);
            SimpleDateFormat sdfr = new SimpleDateFormat("dd-MMM-yy");
            strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
            strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
            labelCompanyName1.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
            labelUserName1.setText(SessionDataUtil.getSelectedUser().getUserName());

            Date date = new Date();
            dateFrom.setDate(date);
            dateTo.setDate(date);
            fetch();

            dateFrom.requestFocusInWindow();
            ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateFrom.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateFrom.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateFrom.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    dateTo.requestFocusInWindow();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    char vChar = evt.getKeyChar();
                    boolean dot = false;

                    if (dot == false) {
                        if (vChar == '.') {
                            dot = true;
                        } else if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();
                        }
                    } else {
                        if (!(Character.isDigit(vChar)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                            evt.consume();

                        }
                    }
                    if (Character.isDigit(vChar)) {
                        String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                        int len = strDate.length();
                        if (len == 1) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-");
                            evt.consume();
                        } else if (len == 4) {
                            ((JTextField) dateTo.getDateEditor().getUiComponent()).setText(strDate + vChar + "-20");
                            evt.consume();
                        }
                    }
                }
            });
            ((JTextField) dateTo.getDateEditor().getUiComponent()).addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
                            sft.setLenient(false);
                            String strDate = ((JTextField) dateTo.getDateEditor().getUiComponent()).getText();
                            if (!(strDate.trim().equals(""))) {
                                Date date = sft.parse(strDate);
                                if ((date.compareTo(companyPolicy.getFinancialYearStart())) >= 0 && (date.compareTo(companyPolicy.getFinancialYearEnd())) <= 0) {
                                    btnFetch.requestFocus();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Please Select The Date From " + sft.format(companyPolicy.getFinancialYearStart()) + " To " + sft.format(companyPolicy.getFinancialYearEnd()));
                                }
                                evt.consume();
                            } else {
                                JOptionPane.showMessageDialog(null, "Please Select the Date");
                            }
                        } catch (ParseException ex) {
                            JOptionPane.showMessageDialog(null, "Please Enter The Date In 'dd-MM-yyyy' Format");
                            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
//        } catch (Exception ex) {
//            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException ex) {
            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
            menuBar.remove(menuAccountsReport);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {
        if (!SessionDataUtil.getSelectedUser().isAdmin()) {
            menuBar.remove(menuSettings);
        }

        menuProduct.setVisible(companyUserRole.isProductView());
        menuProductGroup.setVisible(companyUserRole.isProductView());
        menuService.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        //newEntry
        if (!companyUserRole.isSalesCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isPurchaseCreate() && !companyUserRole.isCrdrNoteCreate() && !companyUserRole.isBosCreate()) {
            menuNewBill.setEnabled(false);
        } else {
            menuSaleB2B.setVisible(companyUserRole.isSalesCreate());
            menuSaleB2C.setVisible(companyUserRole.isSalesCreate());
            menuEstimate.setVisible(companyUserRole.isEstimateCreate());
            menuCreditNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
            menuDebitNote.setVisible(companyUserRole.isCrdrNoteCreate());
            menuBillOfSupply.setVisible(companyUserRole.isBosCreate());
        }

        //Inventory
        if (!companyUserRole.isInventoryAdd() && !companyUserRole.isInventoryDeduct() && !companyUserRole.isInventoryOpeningBalance()) {
            menuInventory.setEnabled(false);
        } else {
            menuAddStock.setVisible(companyUserRole.isInventoryAdd());
            menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
            menuOpeningStock.setVisible(companyUserRole.isInventoryOpeningBalance());
        }

        //EntryReports
        if (!companyUserRole.isSalesView() && !companyUserRole.isCrdrNoteView() && !companyUserRole.isPurchaseView() && !companyUserRole.isEstimateCreate()&& !companyUserRole.isBosView()) {
            menuReports.setEnabled(false);
        } else {
            menuSalesReportGroup.setVisible(companyUserRole.isSalesView());
            menuEstimateReportGroup.setVisible(companyUserRole.isEstimateView());
            menuCreditNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuPurchaseReportGroup.setVisible(companyUserRole.isPurchaseView());
            menuDebitNoteReport.setVisible(companyUserRole.isCrdrNoteView());
            menuBillofSupplyReport.setVisible(companyUserRole.isBosView());
        }

        //inventoryReports
        if (!companyUserRole.isInventoryView()) {
            menuInventoryReport.setEnabled(false);
        } else {
            menuCurrentStock.setVisible(companyUserRole.isInventoryView());
            menuClosingStock.setVisible(companyUserRole.isInventoryView());
             menuTransactionSummary.setVisible(companyUserRole.isInventoryView());
            menuProductTransaction.setVisible(companyUserRole.isInventoryView());
        }
        //GstR1
        if (!companyUserRole.isGstReports()) {
            menuGST.setEnabled(false);
        } else {
            menuGSTR1.setVisible(companyUserRole.isGstReports());
            menuGSTR2.setVisible(companyUserRole.isGstReports());
            menuGstPercentage.setVisible(companyUserRole.isGstReports());
        }

        //accounts
        if (!companyUserRole.isLedgerOpeningBalance() && !companyUserRole.isLedgerCreate() && !companyUserRole.isAccountsJournal()
                && !companyUserRole.isAccountsCash() && !companyUserRole.isAccountsBank()&& !companyUserRole.isAccountsBalanceSheet()) {
            menuAccounts.setEnabled(false);
        } else {
            menuOpeningBalance.setVisible(companyUserRole.isLedgerOpeningBalance());
            menuLedger.setVisible(companyUserRole.isLedgerCreate());
            menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
            menuCashEntry.setVisible(companyUserRole.isAccountsCash());
            menuBankEntry.setVisible(companyUserRole.isAccountsBank());
            jMenuItem6.setVisible(companyUserRole.isAccountsBalanceSheet());
            jMenuItem2.setVisible(companyUserRole.isAccountsBalanceSheet());
        }

        //accountsreports
        if (!companyUserRole.isLedgerView() && !companyUserRole.isAccountsDayBook() && !companyUserRole.isAccountsCash()
                && !companyUserRole.isAccountsBank() && !companyUserRole.isAccountsLedgerGroupBalance() && !companyUserRole.isAccountsTrailBalance()) {
            menuAccountsReport.setEnabled(false);
        } else {
            menuLedgerBook.setVisible(companyUserRole.isLedgerView());
            menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
            menuCashBook.setVisible(companyUserRole.isAccountsCash());
            menuBankBook.setVisible(companyUserRole.isAccountsBank());
            menuLedgerGroupBalance.setVisible(companyUserRole.isAccountsLedgerGroupBalance());
            menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
        }
    }

    public void fetch() throws ParseException {
        setB2B();
        setB2CL();
        setB2CS();
        setCdnr();
        setCdnur();
//        setExemp();
        setHsn();
//        setDocs();
    }

    public void setCdnr() {
        DefaultTableModel model = (DefaultTableModel) tableCdnr.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        Date fromDate = new Date();
        Date toDate = new Date();
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        SimpleDateFormat sft = new SimpleDateFormat("dd-MMM-yy");
        List<GstReport> gstReports = new GSTOneReportsLogic().getCdnr(fromDate, toDate);
        for (GstReport gr : gstReports) {
            BigDecimal cessAmount = new BigDecimal(0);
            String stateCode = StateCode.getStateCode(gr.getPlaceOfSupply());
            String strDate = (sft.format(gr.getInvoiceDate()));
            String strType = "";
            if (gr.getNoteType().equalsIgnoreCase("CREDIT-NOTE")) {
                strType = "C";
            } else {
                strType = "D";
            }
            if ((gr.getCessAmount() != null) && (gr.getCessAmount().compareTo(BigDecimal.ZERO) != 0)) {
                cessAmount = gr.getCessAmount();
            }
            Object[] row = {
                gr.getGstinOrUin(),
                gr.getInvoiceNumber(),
                strDate,
                gr.getNoteNumber(),
                sft.format(gr.getNoteDate()),
                strType,
                gr.getNoteReason(),
                stateCode + "-" + gr.getPlaceOfSupply(),
                gr.getNoteValue(),
                gr.getGstRate().setScale(2, RoundingMode.DOWN),
                gr.getTaxableValue(),
                cessAmount,
                "N",
                gr.getCdnNumber()
            };
            model.addRow(row);
        }
        HashSet<String> set = new HashSet<String>();
        HashSet<String> invoiceNumbers = new HashSet<String>();
        HashSet<String> noteNumbers = new HashSet<String>();

        BigDecimal totalInvoiceValue = new BigDecimal(0);
        BigDecimal totalTaxableValue = new BigDecimal(0);
        BigDecimal totalCessValue = new BigDecimal(0);

        int count = tableCdnr.getRowCount();
        tableTotalCdnr.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
        tableTotalCdnr.getColumnModel().getColumn(4).setCellRenderer(new FloatValueTableCellRenderer());
        tableTotalCdnr.getColumnModel().getColumn(5).setCellRenderer(new FloatValueTableCellRenderer());
        for (int i = 0; i < count; i++) {
            if (i == 0) {
                totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableCdnr.getValueAt(i, 8).toString()));
                set.add(tableCdnr.getValueAt(i, 0).toString());
                invoiceNumbers.add(tableCdnr.getValueAt(i, 1).toString());
                noteNumbers.add(tableCdnr.getValueAt(i, 3).toString());
            } else {
                if (!invoiceNumbers.contains(tableCdnr.getValueAt(i, 1).toString())) {
//                    totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableCdnr.getValueAt(i, 8).toString()));
                    invoiceNumbers.add(tableCdnr.getValueAt(i, 1).toString());
                }
                if (!noteNumbers.contains(tableCdnr.getValueAt(i, 3).toString())) {
                    totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableCdnr.getValueAt(i, 8).toString()));
                    noteNumbers.add(tableCdnr.getValueAt(i, 3).toString());
                }

            }

            totalTaxableValue = totalTaxableValue.add(new BigDecimal(tableCdnr.getValueAt(i, 10).toString()));
            if ((tableCdnr.getValueAt(i, 10) != null) && (!tableCdnr.getValueAt(i, 11).equals("")) && (!tableCdnr.getValueAt(i, 10).equals("0.00"))) {
                totalCessValue = totalCessValue.add(new BigDecimal(tableCdnr.getValueAt(i, 11).toString()));
            }
        }
        Iterator<String> itr = set.iterator();

        String[] array = (String[]) set.toArray(new String[set.size()]);

        tableTotalCdnr.setValueAt(set.size(), 0, 0);
        tableTotalCdnr.setValueAt(invoiceNumbers.size(), 0, 1);
        tableTotalCdnr.setValueAt(noteNumbers.size(), 0, 2);
        tableTotalCdnr.setValueAt(totalInvoiceValue, 0, 3);
        tableTotalCdnr.setValueAt(totalTaxableValue, 0, 4);
        tableTotalCdnr.setValueAt(totalCessValue, 0, 5);
    }

    public void setB2B() throws ParseException {

        DefaultTableModel model = (DefaultTableModel) tableB2B.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        Date fromDate = new Date();
        Date toDate = new Date();
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        SimpleDateFormat sft = new SimpleDateFormat("dd-MMM-yy");
        List<GstReport> gstReports = new GSTOneReportsLogic().getB2B(fromDate, toDate);

        for (GstReport gr : gstReports) {
            String strDate = (sft.format(gr.getInvoiceDate()));
            BigDecimal cessAmount = null;
            String stateCode = StateCode.getStateCode(gr.getPlaceOfSupply());
            if ((gr.getCessAmount() != null) && (gr.getCessAmount().compareTo(BigDecimal.ZERO) != 0)) {
                cessAmount = gr.getCessAmount();
                Object[] row = {
                    gr.getGstinOrUin(),
                    gr.getInvoiceNumber(),
                    strDate,
                    gr.getInvoiceValue(),
                    stateCode + "-" + gr.getPlaceOfSupply(),
                    gr.getReverseCharge(),
                    gr.getInvoiceType(),
                    gr.geteCommerceGSTIN(),
                    gr.getGstRate().setScale(2, RoundingMode.DOWN),
                    gr.getTaxableValue(),
                    cessAmount
                };
                model.addRow(row);
            } else {
                Object[] row = {
                    gr.getGstinOrUin(),
                    gr.getInvoiceNumber(),
                    strDate,
                    gr.getInvoiceValue(),
                    stateCode + "-" + gr.getPlaceOfSupply(),
                    gr.getReverseCharge(),
                    gr.getInvoiceType(),
                    gr.geteCommerceGSTIN(),
                    gr.getGstRate().setScale(2, RoundingMode.DOWN),
                    gr.getTaxableValue()

                };
                model.addRow(row);
            }

        }

        HashSet<String> set = new HashSet<String>();
        HashSet<String> invoiceSet = new HashSet<String>();

        int totalRecipients = 0;
        BigDecimal totalInvoiceValue = new BigDecimal(0);
        BigDecimal totalTaxableValue = new BigDecimal(0);
        BigDecimal totalCessValue = new BigDecimal(0);

        int count = tableB2B.getRowCount();
        tableTotalB2B.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
        tableTotalB2B.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
//        tableTotalB2B.getColumnModel().getColumn(4).setCellRenderer(new FloatValueTableCellRenderer());

        tableB2B.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
        tableB2B.getColumnModel().getColumn(9).setCellRenderer(new FloatValueTableCellRenderer());
//        tableB2B.getColumnModel().getColumn(10).setCellRenderer(new FloatValueTableCellRenderer());

        for (int i = 0; i < count; i++) {
            set.add(tableB2B.getValueAt(i, 0).toString());
            if (i == 0) {
                totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableB2B.getValueAt(i, 3).toString()));
                invoiceSet.add(tableB2B.getValueAt(i, 1).toString());
            } else if (!invoiceSet.contains(tableB2B.getValueAt(i, 1).toString())) {
                totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableB2B.getValueAt(i, 3).toString()));
                invoiceSet.add(tableB2B.getValueAt(i, 1).toString());
            }
            totalTaxableValue = totalTaxableValue.add(new BigDecimal(tableB2B.getValueAt(i, 9).toString()));
            if ((tableB2B.getValueAt(i, 10) != null) && (!tableB2B.getValueAt(i, 10).equals("")) && (!tableB2B.getValueAt(i, 10).equals("0.00"))) {
                totalCessValue = totalCessValue.add(new BigDecimal(tableB2B.getValueAt(i, 10).toString()));
            }
        }
        Iterator<String> itr = set.iterator();

        String[] array = (String[]) set.toArray(new String[set.size()]);

        tableTotalB2B.setValueAt(set.size(), 0, 0);
        tableTotalB2B.setValueAt(invoiceSet.size(), 0, 1);
        tableTotalB2B.setValueAt(totalInvoiceValue, 0, 2);
        tableTotalB2B.setValueAt(totalTaxableValue, 0, 3);
        tableTotalB2B.setValueAt(totalCessValue, 0, 4);
    }

    public void setB2CL() {

        try {
            DefaultTableModel model = (DefaultTableModel) tableB2CL.getModel();
            if (model.getRowCount() > 0) {
                model.setRowCount(0);
            }
            Date fromDate = new Date();
            Date toDate = new Date();
            fromDate = dateFrom.getDate();
            toDate = dateTo.getDate();

            List<GstReport> gstReports = new GSTOneReportsLogic().getB2CL(fromDate, toDate);
            SimpleDateFormat sft = new SimpleDateFormat("dd-MMM-yy");

            for (GstReport gr : gstReports) {
                String strDate = sft.format(gr.getInvoiceDate());
                String stateCode = StateCode.getStateCode(gr.getPlaceOfSupply());
                BigDecimal cessAmount = new BigDecimal(0);
                if (gr.getCessAmount() != null) {
                    cessAmount = gr.getCessAmount();
                    Object[] row = {
                        gr.getInvoiceNumber(),
                        strDate,
                        gr.getInvoiceValue(),
                        stateCode + "-" + gr.getPlaceOfSupply(),
                        gr.getGstRate().setScale(2, RoundingMode.DOWN),
                        gr.getTaxableValue(),
                        cessAmount,
                        gr.geteCommerceGSTIN()
                    };
                    model.addRow(row);
                } else {
                    Object[] row = {
                        gr.getInvoiceNumber(),
                        strDate,
                        gr.getInvoiceValue(),
                        stateCode + "-" + gr.getPlaceOfSupply(),
                        gr.getGstRate().setScale(2, RoundingMode.DOWN),
                        gr.getTaxableValue(),
                        null,
                        gr.geteCommerceGSTIN()
                    };
                    model.addRow(row);
                }
            }

            BigDecimal totalInvoiceValue = new BigDecimal(0);
            BigDecimal totalTaxableValue = new BigDecimal(0);
            BigDecimal totalCessValue = new BigDecimal(0);

            tableTotalB2CL.getColumnModel().getColumn(1).setCellRenderer(new FloatValueTableCellRenderer());
            tableTotalB2CL.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
            tableTotalB2CL.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());

            tableB2CL.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
            tableB2CL.getColumnModel().getColumn(5).setCellRenderer(new FloatValueTableCellRenderer());
            tableB2CL.getColumnModel().getColumn(6).setCellRenderer(new FloatValueTableCellRenderer());
            int count = tableB2CL.getRowCount();

            HashSet<String> invoiceSet = new HashSet<String>();
            for (int i = 0; i < count; i++) {

                if (i == 0) {
                    invoiceSet.add(tableB2CL.getValueAt(i, 0).toString());
                    totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableB2CL.getValueAt(i, 2).toString()));
                } else if (!invoiceSet.contains(tableB2CL.getValueAt(i, 0).toString())) {
                    totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tableB2CL.getValueAt(i, 2).toString()));
                    invoiceSet.add(tableB2CL.getValueAt(i, 0).toString());
                }
                totalTaxableValue = totalTaxableValue.add(new BigDecimal(tableB2CL.getValueAt(i, 5).toString()));
                if (tableB2CL.getValueAt(i, 6) != null) {
                    totalCessValue = totalCessValue.add(new BigDecimal(tableB2CL.getValueAt(i, 6).toString()));
                }
            }

            tableTotalB2CL.setValueAt(invoiceSet.size(), 0, 0);
            tableTotalB2CL.setValueAt(totalInvoiceValue, 0, 1);
            tableTotalB2CL.setValueAt(totalTaxableValue, 0, 2);
            tableTotalB2CL.setValueAt(totalCessValue, 0, 3);
        } catch (ParseException ex) {
            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setB2CS() {
        DefaultTableModel model = (DefaultTableModel) tableb2cs.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        Date fromDate = new Date();
        Date toDate = new Date();
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        List<GstReport> gstReports = new GSTOneReportsLogic().getB2C(fromDate, toDate);

        for (GstReport gr : gstReports) {

            String stateCode = StateCode.getStateCode(gr.getPlaceOfSupply());
//            BigDecimal gstRate = new BigDecimal(0);
//            if (gr.getGstRate() != null) {
//                gstRate = gr.getGstRate();
//            }
            if ((gr.getCessAmount() != null) && (gr.getCessAmount().compareTo(BigDecimal.ZERO) != 0)) {
                Object[] row = {
                    gr.getInvoiceType(),
                    stateCode + "-" + gr.getPlaceOfSupply(),
                    gr.getGstRate().setScale(2, RoundingMode.DOWN),
                    gr.getTaxableValue(),
                    gr.getCessAmount(),
                    gr.geteCommerceGSTIN()
                };
                model.addRow(row);
            } else {
                Object[] row = {
                    gr.getInvoiceType(),
                    stateCode + "-" + gr.getPlaceOfSupply(),
                    gr.getGstRate().setScale(2, RoundingMode.DOWN),
                    gr.getTaxableValue(),
                    gr.geteCommerceGSTIN()
                };
                model.addRow(row);
            }

            BigDecimal totalTaxableValue = new BigDecimal(0);
            BigDecimal totalCessValue = new BigDecimal(0);
            int count = tableb2cs.getRowCount();

            for (int i = 0; i < count; i++) {
                totalTaxableValue = totalTaxableValue.add(new BigDecimal(tableb2cs.getValueAt(i, 3).toString()));
                String strCess = "0";
                if (!(tableb2cs.getValueAt(i, 4) == null)) {
                    strCess = tableb2cs.getValueAt(i, 4).toString();
                }
                totalCessValue = totalCessValue.add(new BigDecimal(strCess));
            }
            tableTotalb2cs.setValueAt(totalTaxableValue.toString(), 0, 0);
            if ((totalCessValue != null) && (totalCessValue.compareTo(BigDecimal.ZERO) != 0)) {
                tableTotalb2cs.setValueAt(totalCessValue.toString(), 0, 1);
            }
        }
    }

    public void setDocs() {
        DefaultTableModel model = (DefaultTableModel) tabledocs.getModel();
        for (int i = 0; i < 5; i++) {
            Object[] row = {
                "Invoice for outward supply",
                "LKO/1001",
                "LKO/10090",
                90,
                5
            };
            model.addRow(row);
        }

    }

    public void setHsn() {

        DefaultTableModel model = (DefaultTableModel) tablehsn.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        Date fromDate = new Date();
        Date toDate = new Date();
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        List<UQC> UqcCode = new uqcCRUD().fetchAllUQCs();
        List<GstReport> gstReports = new GSTOneReportsLogic().getHSN(fromDate, toDate);

        for (GstReport gr : gstReports) {
            String strCode = "";
            for (UQC Uqc : UqcCode) {
                if (Uqc.getQuantityName().equalsIgnoreCase(gr.getUqc())) {
                    strCode = Uqc.getUQCCode();
                }
            }
            if (gr.getTaxableValue().equals(BigDecimal.ZERO) == false) {
                Object[] row = {
                    gr.getHsn(),
                    gr.getDescription(),
                    strCode + "-" + gr.getUqc(),
                    gr.getTotalQuantity(),
                    gr.getTotalValue(),
                    gr.getTaxableValue(),
                    gr.getIntegratedTaxAmount(),
                    gr.getCentralTaxAmount(),
                    gr.getStateTaxAmount(),
                    gr.getCessAmount()
                };
                model.addRow(row);
            }
        }
        BigDecimal totalValue = new BigDecimal(0);
        BigDecimal totalTaxableValue = new BigDecimal(0);
        BigDecimal totalITValue = new BigDecimal(0);
        BigDecimal totalCTValue = new BigDecimal(0);
        BigDecimal totalSTValue = new BigDecimal(0);
        BigDecimal totalCessValue = new BigDecimal(0);
        int count = tablehsn.getRowCount();
        HashSet hsnSet = new HashSet<String>();
        for (int i = 0; i < count; i++) {
            hsnSet.add(tablehsn.getValueAt(i, 0).toString());
            totalValue = totalValue.add(new BigDecimal(tablehsn.getValueAt(i, 4).toString()));
            totalTaxableValue = totalTaxableValue.add(new BigDecimal(tablehsn.getValueAt(i, 5).toString()));
            totalITValue = totalITValue.add(new BigDecimal(tablehsn.getValueAt(i, 6).toString()));
            totalCTValue = totalCTValue.add(new BigDecimal(tablehsn.getValueAt(i, 7).toString()));
            totalSTValue = totalSTValue.add(new BigDecimal(tablehsn.getValueAt(i, 8).toString()));
            totalCessValue = totalCessValue.add(new BigDecimal(tablehsn.getValueAt(i, 9).toString()));
        }
        tableTotalhsn.setValueAt(hsnSet.size(), 0, 0);
        tableTotalhsn.setValueAt(totalValue, 0, 1);
        tableTotalhsn.setValueAt(totalTaxableValue, 0, 2);
        tableTotalhsn.setValueAt(totalITValue, 0, 3);
        tableTotalhsn.setValueAt(totalCTValue, 0, 4);
        tableTotalhsn.setValueAt(totalSTValue, 0, 5);
        tableTotalhsn.setValueAt(totalCessValue, 0, 6);

    }
//    public void setExemp() {
//        DefaultTableModel model = (DefaultTableModel) tableexemp.getModel();
//        for (int i = 0; i < 5; i++) {
//            Object[] row = {
//                "Inter-State supplies to registered persons",
//                100000.00,
//                200000.00,
//                300000.00
//
//            };
//            model.addRow(row);
//        }
//
//        BigDecimal totalNilRatedSupplies = new BigDecimal(0);
//        BigDecimal totalExemptedSupplies = new BigDecimal(0);
//        BigDecimal totalNonGstSupplies = new BigDecimal(0);
//        int count = tableexemp.getRowCount();
//
//        for (int i = 0; i < count; i++) {
//            totalNilRatedSupplies = totalNilRatedSupplies.add(new BigDecimal(tableexemp.getValueAt(i, 1).toString()));
//            totalExemptedSupplies = totalExemptedSupplies.add(new BigDecimal(tableexemp.getValueAt(i, 2).toString()));
//            totalNonGstSupplies = totalNonGstSupplies.add(new BigDecimal(tableexemp.getValueAt(i, 3).toString()));
//        }
//        tableTotalexemp.setValueAt(totalNilRatedSupplies, 0, 0);
//        tableTotalexemp.setValueAt(totalExemptedSupplies, 0, 1);
//        tableTotalexemp.setValueAt(totalNonGstSupplies, 0, 2);
//
//    }
//    public void setcdnur() {
//        DefaultTableModel model = (DefaultTableModel) tablecdnur.getModel();
//        for (int i = 0; i < 5; i++) {
//            Object[] row = {
//                "B2CL",
//                90001,
//                "28-Apr-17", "C", 10003,
//                "05-Apr-17",
//                "Sales Return",
//                "32-Kerala",
//                25000.00,
//                5.00,
//                250000.00,
//                2010.00,
//                "Y"
//
//            };
//            model.addRow(row);
//        }
//        BigDecimal totalNoofNotes = new BigDecimal(0);
//        BigDecimal totalNoofInvoices = new BigDecimal(0);
//        BigDecimal totalNoteValue = new BigDecimal(0);
//        BigDecimal totaltaxValue = new BigDecimal(0);
//        BigDecimal totalCessValue = new BigDecimal(0);
//        int count = tablecdnur.getRowCount();
//
//        for (int i = 0; i < count; i++) {
//            totalNoteValue = totalNoteValue.add(new BigDecimal(tablecdnur.getValueAt(i, 8).toString()));
//            totaltaxValue = totaltaxValue.add(new BigDecimal(tablecdnur.getValueAt(i, 10).toString()));
//            totalCessValue = totalCessValue.add(new BigDecimal(tablecdnur.getValueAt(i, 11).toString()));
//        }
//        tableTotalcdnur.setValueAt(count, 0, 0);
//        tableTotalcdnur.setValueAt(count, 0, 1);
//        tableTotalcdnur.setValueAt(totalNoteValue, 0, 2);
//        tableTotalcdnur.setValueAt(totaltaxValue, 0, 3);
//        tableTotalcdnur.setValueAt(totalCessValue, 0, 4);
//
//    }

    public void setCdnur() {
        DefaultTableModel model = (DefaultTableModel) tablecdnur.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        Date fromDate = new Date();
        Date toDate = new Date();
        fromDate = dateFrom.getDate();
        toDate = dateTo.getDate();

        SimpleDateFormat sft = new SimpleDateFormat("dd-MMM-yy");
        List<GstReport> gstReports = new GSTOneReportsLogic().getCdnUr(fromDate, toDate);
        for (GstReport gr : gstReports) {
            BigDecimal cessAmount = new BigDecimal(0);
            String stateCode = StateCode.getStateCode(gr.getPlaceOfSupply());
            String strDate = (sft.format(gr.getInvoiceDate()));
            String strType = "";
            if (gr.getNoteType().equalsIgnoreCase("CREDIT-NOTE")) {
                strType = "C";
            } else {
                strType = "D";
            }
            if ((gr.getCessAmount() != null) && (gr.getCessAmount().compareTo(BigDecimal.ZERO) != 0)) {
                cessAmount = gr.getCessAmount();
            }
            Object[] row = {
                "B2CL",
                gr.getNoteNumber(),
                sft.format(gr.getNoteDate()),
                strType,
                gr.getInvoiceNumber(),
                strDate,
                gr.getNoteReason(),
                stateCode + "-" + gr.getPlaceOfSupply(),
                gr.getNoteValue(),
                gr.getGstRate().setScale(2, RoundingMode.DOWN),
                gr.getTaxableValue(),
                cessAmount,
                "N"
            };
            model.addRow(row);

        }

        BigDecimal totalInvoiceValue = new BigDecimal(0);
        BigDecimal totalTaxableValue = new BigDecimal(0);
        BigDecimal totalCessValue = new BigDecimal(0);

        int count = tablecdnur.getRowCount();
        HashSet<String> invoiceSet = new HashSet<String>();
        HashSet<String> noteSet = new HashSet<String>();
        tableTotalcdnur.getColumnModel().getColumn(2).setCellRenderer(new FloatValueTableCellRenderer());
        tableTotalcdnur.getColumnModel().getColumn(3).setCellRenderer(new FloatValueTableCellRenderer());
        tableTotalcdnur.getColumnModel().getColumn(4).setCellRenderer(new FloatValueTableCellRenderer());
        for (int i = 0; i < count; i++) {
            if (i == 0) {
                totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tablecdnur.getValueAt(i, 8).toString()));
                invoiceSet.add(tablecdnur.getValueAt(i, 4).toString());
                noteSet.add(tablecdnur.getValueAt(i, 1).toString());
            } else if ((i > 0) && (!invoiceSet.contains(tablecdnur.getValueAt(i, 4).toString()))) {
                totalInvoiceValue = totalInvoiceValue.add(new BigDecimal(tablecdnur.getValueAt(i, 8).toString()));
                invoiceSet.add(tablecdnur.getValueAt(i, 4).toString());
            }
            if ((i > 0) && !(noteSet.contains(tablecdnur.getValueAt(i, 1).toString()))) {
                noteSet.add(tablecdnur.getValueAt(i, 1).toString());
            }

            totalTaxableValue = totalTaxableValue.add(new BigDecimal(tablecdnur.getValueAt(i, 10).toString()));
            if ((tablecdnur.getValueAt(i, 10) != null) && (!tablecdnur.getValueAt(i, 11).equals("")) && (!tablecdnur.getValueAt(i, 10).equals("0.00"))) {
                totalCessValue = totalCessValue.add(new BigDecimal(tablecdnur.getValueAt(i, 11).toString()));
            }
        }

        tableTotalcdnur.setValueAt(noteSet.size(), 0, 0);
        tableTotalcdnur.setValueAt(invoiceSet.size(), 0, 1);
        tableTotalcdnur.setValueAt(totalInvoiceValue, 0, 2);
        tableTotalcdnur.setValueAt(totalTaxableValue, 0, 3);
        tableTotalcdnur.setValueAt(totalCessValue, 0, 4);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableB2B = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableTotalB2B = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableB2CL = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableTotalB2CL = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableTotalb2cs = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tableb2cs = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tableTotalCdnr = new javax.swing.JTable();
        jScrollPane14 = new javax.swing.JScrollPane();
        tableCdnr = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane13 = new javax.swing.JScrollPane();
        tableTotalcdnur = new javax.swing.JTable();
        jScrollPane15 = new javax.swing.JScrollPane();
        tablecdnur = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        tableTotalexemp = new javax.swing.JTable();
        jScrollPane12 = new javax.swing.JScrollPane();
        tableexemp = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        tableTotalhsn = new javax.swing.JTable();
        jScrollPane10 = new javax.swing.JScrollPane();
        tablehsn = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tableTotaldocs = new javax.swing.JTable();
        jScrollPane8 = new javax.swing.JScrollPane();
        tabledocs = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        dateFrom = new com.toedter.calendar.JDateChooser();
        dateTo = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        btnFetch = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        labelUserName1 = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        labelCompanyName1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuBillOfSupply = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuOpeningBalance = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuBillofSupplyReport = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuTransactionSummary = new javax.swing.JMenuItem();
        menuProductTransaction = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenu();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuGSTR2 = new javax.swing.JMenuItem();
        menuGstPercentage = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuMaintenance = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuHelp = new javax.swing.JMenuItem();
        menuContactUs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GSTR1");

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        jTabbedPane1.setFont(new java.awt.Font("Segoe UI", 3, 18)); // NOI18N

        tableB2B.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableB2B.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GSTIN/UIN of Recipient", "Invoice Number", "Invoice Date", "Invoice Value", "Place of Supply", "Reverse Charge", "Invoice Type", "E-Commerce GSTIN ", "Rate", "Taxable Value", "Cess Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableB2B);

        tableTotalB2B.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalB2B.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "No. of Recipients", "No. of Invoices", "Total Invoice Value", "Total Taxable Value", "Total Cess"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tableTotalB2B);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("b2b", jPanel1);

        tableB2CL.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableB2CL.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Invoice No", "Invoice Date", "Invoice Value", "Place of Supply", "Rate", "Taxable Value", "Cess Amount", "E-Commerce GSTIN "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tableB2CL);

        tableTotalB2CL.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalB2CL.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "No. of Invoice", "Total Inv Value", "Total Taxable Value", "TotalCess"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tableTotalB2CL);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(94, 94, 94))
        );

        jTabbedPane1.addTab("b2cl", jPanel2);

        jPanel3.setMaximumSize(new java.awt.Dimension(1129, 538));
        jPanel3.setRequestFocusEnabled(false);

        tableTotalb2cs.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalb2cs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Total Taxable  Value ", "Total Cess "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tableTotalb2cs);

        tableb2cs.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableb2cs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Type ", "Place Of Supply ", "Rate ", "Taxable Value ", "Cess Amount ", "E-Commerce GSTIN "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tableb2cs);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("b2cs", jPanel3);

        tableTotalCdnr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "No. of Recipients ", "No. of Invoices ", "No. of Notes/Vouchers ", "Total Note/Refund Voucher Value ", "Total Taxable Value ", "Total Cess "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane9.setViewportView(tableTotalCdnr);

        tableCdnr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GSTIN/UIN of Recipient ", "Invoice/Advance Receipt Number ", "Invoice/Advance Receipt date ", "Note/Refund Voucher Number ", "Note/Refund Voucher date ", "Document Type ", "Reason For Issuing document ", "Place Of Supply ", "Note/Refund Voucher Value ", "Rate ", "Taxable Value ", "Cess Amount ", "Pre GST ", "cdnNumber"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane14.setViewportView(tableCdnr);
        if (tableCdnr.getColumnModel().getColumnCount() > 0) {
            tableCdnr.getColumnModel().getColumn(13).setMinWidth(0);
            tableCdnr.getColumnModel().getColumn(13).setPreferredWidth(0);
            tableCdnr.getColumnModel().getColumn(13).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addComponent(jScrollPane14))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("cdnr", jPanel4);

        tableTotalcdnur.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalcdnur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "No. of Notes/Vouchers ", "No. of Invoices  ", "Total Note Value ", "Total Taxable Value ", "Total Cess "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane13.setViewportView(tableTotalcdnur);

        tablecdnur.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tablecdnur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "UR Type  ", "Note/Refund Voucher Number ", "Note/Refund Voucher date ", "Document Type ", "Invoice/Advance Receipt Number ", "Invoice/Advance Receipt date ", "Reason For Issuing document ", "Place Of Supply ", "Note/Refund Voucher Value ", "Rate ", "Taxable Value ", "Cess Amount ", "Pre GST "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, true, true, true, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane15.setViewportView(tablecdnur);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("cdnur", jPanel5);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1117, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 453, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("exp", jPanel6);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1117, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 453, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("at", jPanel7);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1117, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 453, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("atadj", jPanel8);

        tableTotalexemp.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalexemp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Total Nil Rated Supplies  ", "Total Exempted Supplies ", "Total Non-GST Supplies "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane11.setViewportView(tableTotalexemp);

        tableexemp.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableexemp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Description ", "Nil Rated Supplies ", "Exempted (other than nil rated/non GST supply ) ", "Non-GST supplies "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane12.setViewportView(tableexemp);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane11)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("exemp", jPanel9);

        tableTotalhsn.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotalhsn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "No. of HSN  ", "Total Value ", "Total Taxable Value ", "Total Integrated Tax ", "Total Central Tax ", "Total State/UT Tax ", "Total Cess "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane16.setViewportView(tableTotalhsn);

        tablehsn.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tablehsn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "HSN ", "Description ", "UQC  ", "Total Quantity ", "Total Value  ", "Taxable Value ", "Integrated Tax Amount ", "Central Tax Amount ", "State/UT Tax Amount ", "Cess Amount "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane10.setViewportView(tablehsn);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("hsn", jPanel10);

        tableTotaldocs.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tableTotaldocs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Total Number ", "Total Cancelled "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tableTotaldocs);

        tabledocs.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        tabledocs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nature  of Document ", "Sr. No. From ", "Sr. No. To ", "Total Number ", "Cancelled "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(tabledocs);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane7)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("docs", jPanel11);

        jButton1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jButton1.setMnemonic('e');
        jButton1.setText("Download Excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("From");

        dateFrom.setDateFormatString("dd-MM-yyyy");

        dateTo.setDateFormatString("dd-MM-yyyy");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel3.setText("To");

        btnFetch.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnFetch.setText("Fetch");
        btnFetch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFetchActionPerformed(evt);
            }
        });

        jLabel25.setText("User :");

        labelUserName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName1.setText("-");

        btnLogout1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout1.setMnemonic('l');
        btnLogout1.setText("Logout");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });

        jLabel18.setText("Company : ");

        labelCompanyName1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName1.setText("----------");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel9.setText("GST1 REPORT");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/home_button30px.png"))); // NOI18N
        jButton2.setMnemonic('h');
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCompanyName1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(153, 153, 153)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUserName1)
                        .addComponent(jLabel25)
                        .addComponent(jButton2))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(labelCompanyName1)
                        .addComponent(jLabel9))
                    .addComponent(btnLogout1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New Entry");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuBillOfSupply.setText("Bill of Supply");
        menuBillOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillOfSupplyActionPerformed(evt);
            }
        });
        menuNewBill.add(menuBillOfSupply);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuOpeningBalance.setText("Opening Balance");
        menuOpeningBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuOpeningBalance);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        jMenuItem2.setText("Profit Loss Account");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem2);

        jMenuItem6.setText("Balance Sheet");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem6);

        jMenuItem3.setText("Payment");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuAccounts.add(jMenuItem3);

        menuBar.add(menuAccounts);

        menuReports.setMnemonic('r');
        menuReports.setText("Entry Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuBillofSupplyReport.setText("Bill of Supply");
        menuBillofSupplyReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBillofSupplyReportActionPerformed(evt);
            }
        });
        menuReports.add(menuBillofSupplyReport);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventoryReport.setMnemonic('v');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuTransactionSummary.setText("Transaction Summary");
        menuTransactionSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTransactionSummaryActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuTransactionSummary);

        menuProductTransaction.setText("Product Transaction Summary");
        menuProductTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductTransactionActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuProductTransaction);

        menuBar.add(menuInventoryReport);

        menuAccountsReport.setMnemonic('o');
        menuAccountsReport.setText("Accounts Report");

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuCashBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuBankBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuLedgerBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerBookActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccountsReport.add(menuTrialBalance);

        jMenuItem7.setText("Sales Payment Report");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem7);

        jMenuItem11.setText("Purchase Payment Report");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        menuAccountsReport.add(jMenuItem11);

        menuBar.add(menuAccountsReport);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuGSTR2.setText("GSTR2");
        menuGSTR2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR2ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR2);

        menuGstPercentage.setText("GST Percentage ");
        menuGstPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGstPercentageActionPerformed(evt);
            }
        });
        menuGST.add(menuGstPercentage);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('t');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuMaintenance.setText("Maintenance");
        menuMaintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMaintenanceActionPerformed(evt);
            }
        });
        menuSettings.add(menuMaintenance);

        menuBar.add(menuSettings);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        menuHelp.setText("Keyboard Shortcuts");
        menuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHelpActionPerformed(evt);
            }
        });
        jMenu2.add(menuHelp);

        menuContactUs.setText("Contact Us");
        menuContactUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContactUsActionPerformed(evt);
            }
        });
        jMenu2.add(menuContactUs);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFetch, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addComponent(jTabbedPane1)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dateFrom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnFetch)
                        .addComponent(dateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            excel();
        } catch (WriteException ex) {
            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFetchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFetchActionPerformed
        Boolean isInBetweeFinancialYearStart = new CheckFinancialYear().toCheck(companyPolicy, dateFrom.getDate(), false);
        Boolean isInBetweeFinancialYearEnd = new CheckFinancialYear().toCheck(companyPolicy, dateTo.getDate(), false);
        if (isInBetweeFinancialYearStart && isInBetweeFinancialYearEnd) {
            try {
                fetch();
            } catch (ParseException ex) {
                Logger.getLogger(GSTR1UI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            dateFrom.setDate(companyPolicy.getFinancialYearEnd());
            dateTo.setDate(companyPolicy.getFinancialYearEnd());
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnFetchActionPerformed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (isInventoryEnabled && isAccountEnabled) {
                        new DashBoardUI().setVisible(true);
                        dispose();
                    } else if (isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardSilverUI().setVisible(true);
                        dispose();
                    } else if (!isInventoryEnabled && !isAccountEnabled) {
                        new DashBoardCopperUI().setVisible(true);
                        dispose();
                    }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
            //            new ProductUI().setVisible(true);
            //            dispose();
            //        } else {
            new ProductUI().setVisible(true);
            dispose();
            //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        new EstimateUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuBillOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillOfSupplyActionPerformed
        new BOSUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillOfSupplyActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuOpeningBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningBalanceActionPerformed
        new OpeningBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningBalanceActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new TradingAccountUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new BalanceSheetUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new PaymentGrpUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuBillofSupplyReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBillofSupplyReportActionPerformed
        new BOSReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBillofSupplyReportActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuTransactionSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTransactionSummaryActionPerformed
        new TransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTransactionSummaryActionPerformed

    private void menuProductTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductTransactionActionPerformed
        new ProductTransactionSummaryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductTransactionActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuCashBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashBookActionPerformed
        new CashBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashBookActionPerformed

    private void menuBankBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankBookActionPerformed
        new BankBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankBookActionPerformed

    private void menuLedgerBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerBookActionPerformed
        new LedgerBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new SalesPaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new PurchasePaymentUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuGSTR2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR2ActionPerformed
        new GSTR2UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR2ActionPerformed

    private void menuGstPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGstPercentageActionPerformed
        new GSTpercentageReport().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGstPercentageActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuMaintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMaintenanceActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuMaintenanceActionPerformed

    private void menuHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHelpActionPerformed
        if (isAccountEnabled) {
            new KeyBoardShortcutsGOLDUI().setVisible(true);
            dispose();
        }
        else if (isInventoryEnabled) {
            new KeyBoardShortcutsSILVERUI().setVisible(true);
            dispose();
        }
        else {
            new KeyBoardShortcutsCOPPERUI().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_menuHelpActionPerformed

    private void menuContactUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContactUsActionPerformed
        new ContactUsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuContactUsActionPerformed

    public void excel() throws WriteException, IOException, SQLException {
        try {
            DefaultTableModel model = (DefaultTableModel) tableb2cs.getModel();
            int count, countFromTot;
            String path = "";
            Boolean isApproved = false;

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

//            if (isPrint) {
//                path = AppConstants.getTempDocPath() + "Tax Invoice Date Wise Report " + sdf.format(from) + " to " + sdf.format(to) + ".xls";
//                isApproved = true;
//            } else {
            JFrame parentFrame = new JFrame();
            com.alee.laf.WebLookAndFeel.install();
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Report");
            fileChooser.setSelectedFile(new File("GSTR1 Report "));
            int userSelection = fileChooser.showSaveDialog(parentFrame);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                path = fileToSave.getAbsolutePath() + ".xls";
                isApproved = true;
            }
//            }

            if (isApproved) {
//        String excelpath = "C:\\Gst\\Excel\\GST1.xls";
                File f = new File(path);

                WritableWorkbook myexcel = Workbook.createWorkbook(f);

                WritableFont wf2 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                wf2.setColour(Colour.WHITE);
                WritableFont wf3 = new WritableFont(WritableFont.TIMES, 11);

                NumberFormat decimalNo = new NumberFormat("#.00");
                WritableCellFormat rightFormat = new WritableCellFormat(decimalNo);
                rightFormat.setFont(wf3);
                rightFormat.setAlignment(Alignment.RIGHT);

                WritableCellFormat headerTotalFormat = new WritableCellFormat(wf2);
                headerTotalFormat.setBackground(Colour.BLUE_GREY);
                headerTotalFormat.setFont(wf2);

                WritableCellFormat headerFormat = new WritableCellFormat(wf2);
                headerFormat.setBackground(Colour.LIGHT_ORANGE);
                headerFormat.setFont(wf2);

                WritableCellFormat dateFormat = new jxl.write.WritableCellFormat(new jxl.write.DateFormat("dd/MMM/yy"));
                dateFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat centerFormat = new WritableCellFormat(wf3);
                centerFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat rightTotalFormat = new WritableCellFormat(NumberFormats.INTEGER);
                rightTotalFormat.setFont(wf3);
                rightTotalFormat.setAlignment(Alignment.CENTRE);

                WritableCellFormat textFormat = new WritableCellFormat(wf3);

                WritableSheet b2b = myexcel.createSheet("b2b", 0);
                b2b.setColumnView(0, 25);
                b2b.setColumnView(1, 16);
                b2b.setColumnView(2, 16);
                b2b.setColumnView(3, 17);
                b2b.setColumnView(4, 18);
                b2b.setColumnView(5, 16);
                b2b.setColumnView(6, 18);
                b2b.setColumnView(7, 28);
                b2b.setColumnView(8, 18);
                b2b.setColumnView(9, 22);
                b2b.setColumnView(10, 10);
                b2b.setColumnView(11, 21);
                b2b.setColumnView(12, 14);

                b2b.setName("b2b");
                b2b.addCell(new Label(0, 0, "Summary For B2B(4)", headerTotalFormat));

                b2b.addCell(new Label(0, 1, "No.of Recipient", headerTotalFormat));
                b2b.addCell(new Label(1, 1, "", headerTotalFormat));
                b2b.addCell(new Label(3, 1, "", headerTotalFormat));
                b2b.addCell(new Label(5, 1, "", headerTotalFormat));

                b2b.addCell(new Label(6, 1, "", headerTotalFormat));
                b2b.addCell(new Label(7, 1, "", headerTotalFormat));
                b2b.addCell(new Label(8, 1, "", headerTotalFormat));
                b2b.addCell(new Label(9, 1, "", headerTotalFormat));
                b2b.addCell(new Label(10, 1, "", headerTotalFormat));

                b2b.addCell(new Label(2, 1, "No.of Invoices", headerTotalFormat));
                b2b.addCell(new Label(4, 1, "Total Inv Value", headerTotalFormat));
                b2b.addCell(new Label(11, 1, "Total Taxable Value", headerTotalFormat));
                b2b.addCell(new Label(12, 1, "Total Cess", headerTotalFormat));

                b2b.addCell(new Label(0, 3, "GSTIN/UIN of Recipient", headerFormat));
                b2b.addCell(new Label(1, 3, "Receiver Name", headerFormat));
                b2b.addCell(new Label(2, 3, "Invoice Number", headerFormat));
                b2b.addCell(new Label(3, 3, "Invoice Date", headerFormat));
                b2b.addCell(new Label(4, 3, "Invoice Value", headerFormat));
                b2b.addCell(new Label(5, 3, "Place Of Supply", headerFormat));
                b2b.addCell(new Label(6, 3, "Reverse Charge", headerFormat));
                b2b.addCell(new Label(7, 3, "Applicable % of Tax Rate", headerFormat));
                b2b.addCell(new Label(8, 3, "invoice Type", headerFormat));
                b2b.addCell(new Label(9, 3, "E-Commerce GSTIN", headerFormat));
                b2b.addCell(new Label(10, 3, "Rate", headerFormat));
                b2b.addCell(new Label(11, 3, "Taxable Value", headerFormat));
                b2b.addCell(new Label(12, 3, "Cess Amount", headerFormat));

                count = tableB2B.getRowCount();

                for (int i = 0; i < count; i++) {
                    b2b.addCell(new Label(0, (i + 4), tableB2B.getValueAt(i, 0).toString(), textFormat));
                    b2b.addCell(new Label(1, (i + 4), "", textFormat));
                    b2b.addCell(new Label(2, (i + 4), tableB2B.getValueAt(i, 1).toString(), textFormat));
                    b2b.addCell(new jxl.write.DateTime(3, (i + 4), new Date(tableB2B.getValueAt(i, 2).toString()), dateFormat));
                    b2b.addCell(new jxl.write.Number(4, (i + 4), Double.parseDouble(tableB2B.getValueAt(i, 3).toString()), rightFormat));
                    b2b.addCell(new Label(5, (i + 4), tableB2B.getValueAt(i, 4).toString(), textFormat));
                    b2b.addCell(new Label(6, (i + 4), tableB2B.getValueAt(i, 5).toString(), centerFormat));
                    if (tableB2B.getValueAt(i, 6) != null) {
                        b2b.addCell(new Label(8, (i + 4), tableB2B.getValueAt(i, 6).toString(), textFormat));
                    }
                    b2b.addCell(new Label(7, (i + 4), "", textFormat));
                    b2b.addCell(new Label(9, (i + 4), "", textFormat));

                    if (tableB2B.getValueAt(i, 8) != null) {
                        b2b.addCell(new jxl.write.Number(10, (i + 4), Double.parseDouble(tableB2B.getValueAt(i, 8).toString()), rightFormat));
                    }
                    if ((tableB2B.getValueAt(i, 9) != null) && (!tableB2B.getValueAt(i, 9).equals(BigDecimal.ZERO))) {
                        b2b.addCell(new jxl.write.Number(11, (i + 4), Double.parseDouble(tableB2B.getValueAt(i, 9).toString()), rightFormat));
                    }
                    if ((tableB2B.getValueAt(i, 10) != null) && (!tableB2B.getValueAt(i, 10).equals(BigDecimal.ZERO))) {
                        b2b.addCell(new jxl.write.Number(12, (i + 4), Double.parseDouble(tableB2B.getValueAt(i, 10).toString()), rightFormat));
                    }
                }
                countFromTot = tableTotalB2B.getRowCount();

                for (int i = 0; i < countFromTot; i++) {
                    b2b.addCell(new jxl.write.Number(0, (i + 2), Double.parseDouble(tableTotalB2B.getValueAt(i, 0).toString()), rightTotalFormat));
                    b2b.addCell(new jxl.write.Number(2, (i + 2), Double.parseDouble(tableTotalB2B.getValueAt(i, 1).toString()), rightTotalFormat));
                    b2b.addCell(new jxl.write.Number(4, (i + 2), Double.parseDouble(tableTotalB2B.getValueAt(i, 2).toString()), rightFormat));
                    b2b.addCell(new jxl.write.Number(11, (i + 2), Double.parseDouble(tableTotalB2B.getValueAt(i, 3).toString()), rightFormat));
                    if ((tableTotalB2B.getValueAt(i, 4) != null) && (!tableTotalB2B.getValueAt(i, 4).equals(BigDecimal.ZERO))) {
                        b2b.addCell(new jxl.write.Number(12, (i + 2), Double.parseDouble(tableTotalB2B.getValueAt(i, 4).toString()), rightFormat));
                    }
                }

                WritableSheet b2cl = myexcel.createSheet("b2cl", 1);
                b2cl.setColumnView(0, 23);
                b2cl.setColumnView(1, 13);
                b2cl.setColumnView(2, 16);
                b2cl.setColumnView(3, 16);
                b2cl.setColumnView(4, 26);
                b2cl.setColumnView(5, 11);
                b2cl.setColumnView(6, 20);
                b2cl.setColumnView(7, 20);
                b2cl.setColumnView(8, 16);

                b2cl.setName("b2cl");
                b2cl.addCell(new Label(0, 0, "Summary For B2CL(5)", headerTotalFormat));
                b2cl.addCell(new Label(7, 0, "HELP", headerTotalFormat));

                b2cl.addCell(new Label(1, 1, "", headerTotalFormat));
                b2cl.addCell(new Label(3, 1, "", headerTotalFormat));
                b2cl.addCell(new Label(4, 1, "", headerTotalFormat));
                b2cl.addCell(new Label(5, 1, "", headerTotalFormat));
                b2cl.addCell(new Label(8, 1, "", headerTotalFormat));

                b2cl.addCell(new Label(0, 1, "No.of Invoices", headerTotalFormat));
                b2cl.addCell(new Label(2, 1, "Total Inv Value", headerTotalFormat));
                b2cl.addCell(new Label(6, 1, "Total Taxable Value", headerTotalFormat));
                b2cl.addCell(new Label(7, 1, "Total Cess", headerTotalFormat));
                b2cl.addCell(new Label(0, 3, "Invoice Number", headerFormat));
                b2cl.addCell(new Label(1, 3, "Invoice Date", headerFormat));
                b2cl.addCell(new Label(2, 3, "Invoice Value", headerFormat));
                b2cl.addCell(new Label(3, 3, "Place Of Supply", headerFormat));
                b2cl.addCell(new Label(4, 3, "Applicable % of Tax Rate", headerFormat));
                b2cl.addCell(new Label(5, 3, "Rate", headerFormat));
                b2cl.addCell(new Label(6, 3, "Taxable Value", headerFormat));
                b2cl.addCell(new Label(7, 3, "Cess Amount", headerFormat));
                b2cl.addCell(new Label(8, 3, "E-Commerce", headerFormat));

                count = tableB2CL.getRowCount();
                for (int i = 0; i < count; i++) {
                    b2cl.addCell(new Label(0, (i + 4), tableB2CL.getValueAt(i, 0).toString(), textFormat));
                    b2cl.addCell(new DateTime(1, (i + 4), new Date(tableB2CL.getValueAt(i, 1).toString()), dateFormat));
                    b2cl.addCell(new Number(2, (i + 4), Double.parseDouble(tableB2CL.getValueAt(i, 2).toString()), rightFormat));
                    b2cl.addCell(new Label(3, (i + 4), tableB2CL.getValueAt(i, 3).toString(), textFormat));
                    if ((tableB2CL.getValueAt(i, 4) != null) && (!tableB2CL.getValueAt(i, 4).equals(BigDecimal.ZERO))) {
                        b2cl.addCell(new Number(5, (i + 4), Double.parseDouble(tableB2CL.getValueAt(i, 4).toString()), rightFormat));
                    }
                    b2cl.addCell(new Number(6, (i + 4), Double.parseDouble(tableB2CL.getValueAt(i, 5).toString()), rightFormat));
                    if ((tableB2CL.getValueAt(i, 6) != null) && (!tableB2CL.getValueAt(i, 6).toString().equals("0.00"))) {
                        b2cl.addCell(new Number(7, (i + 4), Double.parseDouble(tableB2CL.getValueAt(i, 6).toString()), rightFormat));
                    }
                    b2cl.addCell(new Label(4, (i + 4), "", rightFormat));
                }
                countFromTot = tableTotalB2CL.getRowCount();
                for (int i = 0; i < countFromTot; i++) {
                    b2cl.addCell(new Number(0, (i + 2), Double.parseDouble(tableTotalB2CL.getValueAt(i, 0).toString()), rightTotalFormat));
                    b2cl.addCell(new Number(2, (i + 2), Double.parseDouble(tableTotalB2CL.getValueAt(i, 1).toString()), rightFormat));
                    b2cl.addCell(new Number(6, (i + 2), Double.parseDouble(tableTotalB2CL.getValueAt(i, 2).toString()), rightFormat));
                    if ((tableTotalB2CL.getValueAt(i, 3) != null) && (!tableTotalB2CL.getValueAt(i, 3).equals(BigDecimal.ZERO))) {
                        b2cl.addCell(new Number(7, (i + 2), Double.parseDouble(tableTotalB2CL.getValueAt(i, 3).toString()), rightFormat));
                    }
                }

                WritableSheet b2cs = myexcel.createSheet("b2cs", 2);
                b2cs.setColumnView(0, 23);
                b2cs.setColumnView(1, 30);
                b2cs.setColumnView(2, 26);
                b2cs.setColumnView(3, 11);
                b2cs.setColumnView(4, 22);
                b2cs.setColumnView(5, 18);
                b2cs.setColumnView(6, 15);

                b2cs.setName("b2cs");
                b2cs.addCell(new Label(0, 0, "Summary For B2CS(2)", headerTotalFormat));
                b2cs.addCell(new Label(6, 0, "HELP", headerTotalFormat));
                b2cs.addCell(new Label(0, 1, "", headerTotalFormat));
                b2cs.addCell(new Label(1, 1, "", headerTotalFormat));
                b2cs.addCell(new Label(2, 1, "", headerTotalFormat));
                b2cs.addCell(new Label(3, 1, "", headerTotalFormat));
                b2cs.addCell(new Label(4, 1, "Total Taxable Value", headerTotalFormat));
                b2cs.addCell(new Label(5, 1, "Total Cess", headerTotalFormat));
                b2cs.addCell(new Label(6, 1, "", headerTotalFormat));
                b2cs.addCell(new Label(0, 3, "TYPE", headerFormat));
                b2cs.addCell(new Label(1, 3, "Place Of Supply", headerFormat));
                b2cs.addCell(new Label(2, 3, "Applicable % of Tax Rate", headerFormat));
                b2cs.addCell(new Label(3, 3, "Rate", headerFormat));
                b2cs.addCell(new Label(4, 3, "Taxable Value", headerFormat));
                b2cs.addCell(new Label(5, 3, "Cess Amount", headerFormat));
                b2cs.addCell(new Label(6, 3, "E-Commerce", headerFormat));

                count = tableb2cs.getRowCount();

                for (int i = 0; i < count; i++) {
                    b2cs.addCell(new Label(0, (i + 4), tableb2cs.getValueAt(i, 0).toString(), textFormat));
                    b2cs.addCell(new Label(1, (i + 4), tableb2cs.getValueAt(i, 1).toString(), textFormat));
                    b2cs.addCell(new Number(3, (i + 4), Double.parseDouble(tableb2cs.getValueAt(i, 2).toString()), rightFormat));
                    b2cs.addCell(new Number(4, (i + 4), Double.parseDouble(tableb2cs.getValueAt(i, 3).toString()), rightFormat));
                    String strCess = "";
                    if (!(tableb2cs.getValueAt(i, 4) == null) && (!tableb2cs.getValueAt(i, 4).equals(BigDecimal.ZERO))) {
                        strCess = tableb2cs.getValueAt(i, 4).toString();
                        b2cs.addCell(new Number(5, (i + 4), Double.parseDouble(strCess), rightFormat));
                    }
                }
                countFromTot = tableTotalb2cs.getRowCount();

                for (int i = 0; i < countFromTot; i++) {
                    if ((tableTotalb2cs.getValueAt(i, 0) != null) && (!tableTotalb2cs.getValueAt(i, 0).equals(BigDecimal.ZERO))) {
                        b2cs.addCell(new Number(4, (i + 2), Double.parseDouble(tableTotalb2cs.getValueAt(i, 0).toString()), rightFormat));
                    }
                    if ((tableTotalb2cs.getValueAt(i, 1) != null) && (!tableTotalb2cs.getValueAt(i, 1).equals(BigDecimal.ZERO))) {
                        b2cs.addCell(new Number(5, (i + 2), Double.parseDouble(tableTotalb2cs.getValueAt(i, 1).toString()), rightFormat));
                    }
                }

//        WritableSheet exemp = myexcel.createSheet("exemp", 3);
//        exemp.setColumnView(0, 68);
//        exemp.setColumnView(1, 25);
//        exemp.setColumnView(2, 48);
//        exemp.setColumnView(3, 23);
//        exemp.setColumnView(4, 25);
//
//        exemp.setName("exemp");
//        exemp.addCell(new Label(0, 0, "Summary For Nil rated, exempted and non GST outward supplies (8)", cellFormat));
//        exemp.addCell(new Label(4, 0, "HELP", cellFormat));
//        exemp.addCell(new Label(1, 1, "Total Nil Rated Supplies", cellFormat));
//        exemp.addCell(new Label(2, 1, "Total Exempted Supplies", cellFormat));
//        exemp.addCell(new Label(3, 1, "Total Exempted Supplies", cellFormat));
//        exemp.addCell(new Label(0, 3, "Description", cellFormat));
//        exemp.addCell(new Label(1, 3, "Nil Rated Supplies", cellFormat));
//        exemp.addCell(new Label(2, 3, "Exempted (other than nil rated/non GST supply )", cellFormat));
//        exemp.addCell(new Label(3, 3, "Non-GST supplies", cellFormat));
//
//        count = tableexemp.getRowCount();
//
//        for (int i = 0; i < count; i++) {
//            exemp.addCell(new Label(0, (i + 4), tableexemp.getValueAt(i, 0).toString(), textFormat));
//            exemp.addCell(new Label(1, (i + 4), tableexemp.getValueAt(i, 1).toString(), rightFormat));
//            exemp.addCell(new Label(2, (i + 4), tableexemp.getValueAt(i, 2).toString(), rightFormat));
//            exemp.addCell(new Label(3, (i + 4), tableexemp.getValueAt(i, 3).toString(), rightFormat));
//        }
//
//        countFromTot = tableTotalexemp.getRowCount();
//
//        for (int i = 0; i < countFromTot; i++) {
//            exemp.addCell(new Label(1, (i + 2), tableTotalexemp.getValueAt(i, 0).toString(), rightFormat));
//            exemp.addCell(new Label(2, (i + 2), tableTotalexemp.getValueAt(i, 1).toString(), rightFormat));
//            exemp.addCell(new Label(3, (i + 2), tableexemp.getValueAt(i, 2).toString(), rightFormat));
//        }
                WritableSheet hsn = myexcel.createSheet("hsn", 5);
                hsn.setColumnView(0, 23);
                hsn.setColumnView(1, 30);
                hsn.setColumnView(2, 16);
                hsn.setColumnView(3, 16);
                hsn.setColumnView(4, 20);
                hsn.setColumnView(5, 22);
                hsn.setColumnView(6, 22);
                hsn.setColumnView(7, 22);
                hsn.setColumnView(8, 22);
                hsn.setColumnView(9, 18);

                hsn.setName("hsn");
                hsn.addCell(new Label(0, 0, "Summary For HSN(12)", headerTotalFormat));
                hsn.addCell(new Label(9, 0, "HELP", headerTotalFormat));

                hsn.addCell(new Label(1, 1, "", headerTotalFormat));
                hsn.addCell(new Label(2, 1, "", headerTotalFormat));
                hsn.addCell(new Label(3, 1, "", headerTotalFormat));

                hsn.addCell(new Label(0, 1, "No. of HSN", headerTotalFormat));
                hsn.addCell(new Label(4, 1, "Total Value", headerTotalFormat));
                hsn.addCell(new Label(5, 1, "Total Taxable Value", headerTotalFormat));
                hsn.addCell(new Label(6, 1, "Total Integrated Tax", headerTotalFormat));
                hsn.addCell(new Label(7, 1, "Total Central Tax", headerTotalFormat));
                hsn.addCell(new Label(8, 1, "Total State/UT Tax", headerTotalFormat));
                hsn.addCell(new Label(9, 1, "Total Cess", headerTotalFormat));
                hsn.addCell(new Label(0, 3, "HSN", headerFormat));
                hsn.addCell(new Label(1, 3, "Description", headerFormat));
                hsn.addCell(new Label(2, 3, "UQC", headerFormat));
                hsn.addCell(new Label(3, 3, "Total Quantity", headerFormat));
                hsn.addCell(new Label(4, 3, "Total Value", headerFormat));
                hsn.addCell(new Label(5, 3, "Taxable Value", headerFormat));
                hsn.addCell(new Label(6, 3, "Integrated Tax Amount", headerFormat));
                hsn.addCell(new Label(7, 3, "Central Tax Amount", headerFormat));
                hsn.addCell(new Label(8, 3, "State/UT Tax Amount", headerFormat));
                hsn.addCell(new Label(9, 3, "Cess Amount", headerFormat));

                count = tablehsn.getRowCount();

                for (int i = 0; i < count; i++) {
                    hsn.addCell(new Label(0, (i + 4), tablehsn.getValueAt(i, 0).toString(), rightTotalFormat));
                    if ((tablehsn.getValueAt(i, 1).toString().equals("")) || (tablehsn.getValueAt(i, 1).toString().equals("-"))) {
                        hsn.addCell(new Label(1, (i + 4), "", textFormat));
                    } else {
                        hsn.addCell(new Label(1, (i + 4), tablehsn.getValueAt(i, 1).toString(), textFormat));
                    }

                    hsn.addCell(new Label(2, (i + 4), tablehsn.getValueAt(i, 2).toString(), textFormat));
                    hsn.addCell(new Number(3, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 3).toString()), rightFormat));
                    hsn.addCell(new Number(4, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 4).toString()), rightFormat));
                    hsn.addCell(new Number(5, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 5).toString()), rightFormat));
                    if ((tablehsn.getValueAt(i, 6) != null) && (!tablehsn.getValueAt(i, 6).toString().equals("0.00"))) {
                        hsn.addCell(new Number(6, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 6).toString()), rightFormat));
                    }
                    if ((tablehsn.getValueAt(i, 7) != null) && (!tablehsn.getValueAt(i, 7).toString().equals("0.00"))) {
                        hsn.addCell(new Number(7, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 7).toString()), rightFormat));
                    }
                    if ((tablehsn.getValueAt(i, 8) != null) && (!tablehsn.getValueAt(i, 8).toString().equals("0.00"))) {
                        hsn.addCell(new Number(8, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 8).toString()), rightFormat));
                    }
                    if ((tablehsn.getValueAt(i, 9) != null) && (!tablehsn.getValueAt(i, 9).toString().equals("0.00"))) {
                        hsn.addCell(new Number(9, (i + 4), Double.parseDouble(tablehsn.getValueAt(i, 9).toString()), rightFormat));
                    }
                }

                countFromTot = tableTotalhsn.getRowCount();

                for (int i = 0; i < countFromTot; i++) {
                    hsn.addCell(new Number(0, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 0).toString()), rightTotalFormat));
                    hsn.addCell(new Number(4, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 1).toString()), rightFormat));
                    hsn.addCell(new Number(5, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 2).toString()), rightFormat));
                    hsn.addCell(new Number(6, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 3).toString()), rightFormat));
                    hsn.addCell(new Number(7, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 4).toString()), rightFormat));
                    hsn.addCell(new Number(8, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 5).toString()), rightFormat));
                    hsn.addCell(new Number(9, (i + 2), Double.parseDouble(tableTotalhsn.getValueAt(i, 6).toString()), rightFormat));
                }

                WritableSheet cdnr = myexcel.createSheet("cdnr", 3);
                cdnr.setColumnView(0, 23);
                cdnr.setColumnView(1, 18);
                cdnr.setColumnView(2, 32);
                cdnr.setColumnView(3, 30);
                cdnr.setColumnView(4, 30);
                cdnr.setColumnView(5, 26);
                cdnr.setColumnView(6, 16);
                cdnr.setColumnView(7, 22);
                cdnr.setColumnView(8, 27);
                cdnr.setColumnView(9, 26);
                cdnr.setColumnView(10, 10);
                cdnr.setColumnView(11, 21);
                cdnr.setColumnView(12, 14);
                cdnr.setColumnView(13, 12);

                cdnr.setName("cdnr");
                cdnr.addCell(new Label(0, 0, "Summary For cdnr(9B)", headerTotalFormat));
                cdnr.addCell(new Label(0, 1, "No. of Recipients", headerTotalFormat));

                cdnr.addCell(new Label(0, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(1, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(3, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(5, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(6, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(7, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(9, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(10, 1, "", headerTotalFormat));
                cdnr.addCell(new Label(13, 1, "", headerTotalFormat));

                cdnr.addCell(new Label(2, 1, "No. of Invoices", headerTotalFormat));
                cdnr.addCell(new Label(4, 1, "No. of Notes/Vouchers", headerTotalFormat));
                cdnr.addCell(new Label(8, 1, "Total Note/Refund Voucher Value", headerTotalFormat));
                cdnr.addCell(new Label(11, 1, "Total Taxable Value", headerTotalFormat));
                cdnr.addCell(new Label(12, 1, "Total Cess", headerTotalFormat));
                cdnr.addCell(new Label(0, 3, "GSTIN/UIN of Recipient", headerFormat));
                cdnr.addCell(new Label(1, 3, "Receiver Name", headerFormat));
                cdnr.addCell(new Label(2, 3, "Invoice/Advance Receipt Number", headerFormat));
                cdnr.addCell(new Label(3, 3, "Invoice/Advance Receipt date", headerFormat));
                cdnr.addCell(new Label(4, 3, "Note/Refund Voucher Number", headerFormat));
                cdnr.addCell(new Label(5, 3, "Note/Refund Voucher date", headerFormat));
                cdnr.addCell(new Label(6, 3, "Document Type", headerFormat));
                cdnr.addCell(new Label(7, 3, "Place Of Supply", headerFormat));
                cdnr.addCell(new Label(8, 3, "Note/Refund Voucher Value", headerFormat));
                cdnr.addCell(new Label(9, 3, "Applicable % of Tax Rate", headerFormat));
                cdnr.addCell(new Label(10, 3, "Rate", headerFormat));
                cdnr.addCell(new Label(11, 3, "Taxable Value", headerFormat));
                cdnr.addCell(new Label(12, 3, "Cess Amount", headerFormat));
                cdnr.addCell(new Label(13, 3, "Pre GST", headerFormat));

                count = tableCdnr.getRowCount();

                for (int i = 0; i < count; i++) {
                    cdnr.addCell(new Label(0, (i + 4), tableCdnr.getValueAt(i, 0).toString(), textFormat));
                    cdnr.addCell(new Label(1, (i + 4), "", textFormat));
                    cdnr.addCell(new Label(2, (i + 4), tableCdnr.getValueAt(i, 1).toString(), textFormat));
                    cdnr.addCell(new DateTime(3, (i + 4), new Date(tableCdnr.getValueAt(i, 2).toString()), dateFormat));
                    cdnr.addCell(new Label(4, (i + 4), tableCdnr.getValueAt(i, 3).toString(), textFormat));
                    cdnr.addCell(new DateTime(5, (i + 4), new Date(tableCdnr.getValueAt(i, 4).toString()), dateFormat));
                    cdnr.addCell(new Label(6, (i + 4), tableCdnr.getValueAt(i, 5).toString(), centerFormat));
                    cdnr.addCell(new Label(7, (i + 4), tableCdnr.getValueAt(i, 7).toString(), textFormat));
                    cdnr.addCell(new Number(8, (i + 4), Double.parseDouble(tableCdnr.getValueAt(i, 8).toString()), rightFormat));
                    cdnr.addCell(new Label(9, (i + 4), "", textFormat));
                    if ((tableCdnr.getValueAt(i, 9)) != null && (!tableCdnr.getValueAt(i, 9).toString().equals("0"))) {
                        cdnr.addCell(new Number(10, (i + 4), Double.parseDouble(tableCdnr.getValueAt(i, 9).toString()), rightFormat));
                    }
                    if ((tableCdnr.getValueAt(i, 10)) != null && (!tableCdnr.getValueAt(i, 10).toString().equals("0"))) {
                        cdnr.addCell(new Number(11, (i + 4), Double.parseDouble(tableCdnr.getValueAt(i, 10).toString()), rightFormat));
                    }
                    if ((tableCdnr.getValueAt(i, 11)) != null && (!tableCdnr.getValueAt(i, 11).toString().equals("0"))) {
                        cdnr.addCell(new Number(12, (i + 4), Double.parseDouble(tableCdnr.getValueAt(i, 11).toString()), rightFormat));
                    }
                    cdnr.addCell(new Label(13, (i + 4), "N", centerFormat));
                }

                countFromTot = tableTotalCdnr.getRowCount();

                for (int i = 0; i < countFromTot; i++) {
                    cdnr.addCell(new Number(0, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 0).toString()), rightTotalFormat));
                    cdnr.addCell(new Number(2, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 1).toString()), rightTotalFormat));
                    cdnr.addCell(new Number(4, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 2).toString()), rightTotalFormat));
                    cdnr.addCell(new Number(8, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 3).toString()), rightFormat));
                    cdnr.addCell(new Number(11, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 4).toString()), rightFormat));
                    cdnr.addCell(new Number(12, (i + 2), Double.parseDouble(tableTotalCdnr.getValueAt(i, 5).toString()), rightFormat));
                }

                WritableSheet cdnur = myexcel.createSheet("cdnur", 4);
                cdnur.setColumnView(0, 27);
                cdnur.setColumnView(1, 30);
                cdnur.setColumnView(2, 27);
                cdnur.setColumnView(3, 17);
                cdnur.setColumnView(4, 30);
                cdnur.setColumnView(5, 29);
                cdnur.setColumnView(6, 22);
                cdnur.setColumnView(7, 27);
                cdnur.setColumnView(8, 25);
                cdnur.setColumnView(9, 12);
                cdnur.setColumnView(10, 20);
                cdnur.setColumnView(11, 18);
                cdnur.setColumnView(12, 12);

                cdnur.setName("cdnur");
                cdnur.addCell(new Label(0, 0, "Summary For CDNUR(9B)", headerTotalFormat));

                cdnur.addCell(new Label(0, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(2, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(3, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(5, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(6, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(8, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(9, 1, "", headerTotalFormat));
                cdnur.addCell(new Label(12, 1, "", headerTotalFormat));

                cdnur.addCell(new Label(1, 1, "No. of Notes/Vouchers", headerTotalFormat));
                cdnur.addCell(new Label(4, 1, "No. of Invoices", headerTotalFormat));
                cdnur.addCell(new Label(7, 1, "Total Note Value", headerTotalFormat));
                cdnur.addCell(new Label(10, 1, "Total Taxable Value", headerTotalFormat));
                cdnur.addCell(new Label(11, 1, "Total Cess", headerTotalFormat));
                cdnur.addCell(new Label(0, 3, "UR Type", headerFormat));
                cdnur.addCell(new Label(1, 3, "Note/Refund Voucher Number", headerFormat));
                cdnur.addCell(new Label(2, 3, "Note/Refund Voucher date", headerFormat));
                cdnur.addCell(new Label(3, 3, "Document Type", headerFormat));
                cdnur.addCell(new Label(4, 3, "Invoice/Advance Receipt Number", headerFormat));
                cdnur.addCell(new Label(5, 3, "Invoice/Advance Receipt date", headerFormat));
                cdnur.addCell(new Label(6, 3, "Place Of Supply", headerFormat));
                cdnur.addCell(new Label(7, 3, "Note/Refund Voucher Value", headerFormat));
                cdnur.addCell(new Label(8, 3, "Applicable % of Tax Rate", headerFormat));
                cdnur.addCell(new Label(9, 3, "Rate", headerFormat));
                cdnur.addCell(new Label(10, 3, "Taxable Value", headerFormat));
                cdnur.addCell(new Label(11, 3, "Cess Amount", headerFormat));
                cdnur.addCell(new Label(12, 3, "Pre GST", headerFormat));

                count = tablecdnur.getRowCount();

                for (int i = 0; i < count; i++) {
                    cdnur.addCell(new Label(0, (i + 4), tablecdnur.getValueAt(i, 0).toString(), centerFormat));
                    cdnur.addCell(new Label(1, (i + 4), tablecdnur.getValueAt(i, 1).toString(), textFormat));
                    cdnur.addCell(new DateTime(2, (i + 4), new Date(tablecdnur.getValueAt(i, 2).toString()), dateFormat));
                    cdnur.addCell(new Label(3, (i + 4), tablecdnur.getValueAt(i, 3).toString(), centerFormat));
                    cdnur.addCell(new Label(4, (i + 4), tablecdnur.getValueAt(i, 4).toString(), textFormat));
                    cdnur.addCell(new DateTime(5, (i + 4), new Date(tablecdnur.getValueAt(i, 5).toString()), dateFormat));
                    cdnur.addCell(new Label(6, (i + 4), tablecdnur.getValueAt(i, 7).toString(), textFormat));
                    cdnur.addCell(new Number(7, (i + 4), Double.parseDouble(tablecdnur.getValueAt(i, 8).toString()), rightFormat));
                    cdnur.addCell(new Label(8, (i + 4), "", textFormat));
                    if ((tablecdnur.getValueAt(i, 9)) != null && (!tablecdnur.getValueAt(i, 9).toString().equals("0"))) {
                        cdnur.addCell(new Number(9, (i + 4), Double.parseDouble(tablecdnur.getValueAt(i, 9).toString()), rightFormat));
                    }
                    if ((tablecdnur.getValueAt(i, 10)) != null && (!tablecdnur.getValueAt(i, 10).toString().equals("0"))) {
                        cdnur.addCell(new Number(10, (i + 4), Double.parseDouble(tablecdnur.getValueAt(i, 10).toString()), rightFormat));
                    }
                    if ((tablecdnur.getValueAt(i, 11)) != null && (!tablecdnur.getValueAt(i, 11).toString().equals("0"))) {
                        cdnur.addCell(new Number(11, (i + 4), Double.parseDouble(tablecdnur.getValueAt(i, 11).toString()), rightFormat));
                    }
                    cdnur.addCell(new Label(12, (i + 4), "N", centerFormat));

                }

                countFromTot = tableTotalcdnur.getRowCount();

                for (int i = 0; i < countFromTot; i++) {
                    cdnur.addCell(new Number(1, (i + 2), Double.parseDouble(tableTotalcdnur.getValueAt(i, 0).toString()), rightTotalFormat));
                    cdnur.addCell(new Number(4, (i + 2), Double.parseDouble(tableTotalcdnur.getValueAt(i, 1).toString()), rightTotalFormat));
                    cdnur.addCell(new Number(7, (i + 2), Double.parseDouble(tableTotalcdnur.getValueAt(i, 2).toString()), rightFormat));
                    cdnur.addCell(new Number(10, (i + 2), Double.parseDouble(tableTotalcdnur.getValueAt(i, 3).toString()), rightFormat));
                    cdnur.addCell(new Number(11, (i + 2), Double.parseDouble(tableTotalcdnur.getValueAt(i, 4).toString()), rightFormat));
                }
                myexcel.write();
                myexcel.close();
                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(path);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        // no application registered for PDFs
                    }
                }
//            JOptionPane.showMessageDialog(null, "Excel File Downloaded At " + excelpath);
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (WriteException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SalesDateWiseReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GSTR1UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GSTR1UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GSTR1UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GSTR1UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GSTR1UI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFetch;
    private javax.swing.JButton btnLogout1;
    private com.toedter.calendar.JDateChooser dateFrom;
    private com.toedter.calendar.JDateChooser dateTo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelCompanyName1;
    private javax.swing.JLabel labelUserName1;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenu menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuBillOfSupply;
    private javax.swing.JMenuItem menuBillofSupplyReport;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuContactUs;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenuItem menuGSTR2;
    private javax.swing.JMenuItem menuGstPercentage;
    private javax.swing.JMenuItem menuHelp;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenuItem menuMaintenance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningBalance;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuProductTransaction;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTransactionSummary;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JTable tableB2B;
    private javax.swing.JTable tableB2CL;
    private javax.swing.JTable tableCdnr;
    private javax.swing.JTable tableTotalB2B;
    private javax.swing.JTable tableTotalB2CL;
    private javax.swing.JTable tableTotalCdnr;
    private javax.swing.JTable tableTotalb2cs;
    private javax.swing.JTable tableTotalcdnur;
    private javax.swing.JTable tableTotaldocs;
    private javax.swing.JTable tableTotalexemp;
    private javax.swing.JTable tableTotalhsn;
    private javax.swing.JTable tableb2cs;
    private javax.swing.JTable tablecdnur;
    private javax.swing.JTable tabledocs;
    private javax.swing.JTable tableexemp;
    private javax.swing.JTable tablehsn;
    // End of variables declaration//GEN-END:variables
}
