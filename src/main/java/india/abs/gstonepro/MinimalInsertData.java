/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.dao.BillChargeCRUD;
import india.abs.gstonepro.api.dao.CompanyCRUD;
import india.abs.gstonepro.api.dao.GoDownCRUD;
import india.abs.gstonepro.api.dao.ProductCRUD;
import india.abs.gstonepro.api.dao.ProductGroupCRUD;
import india.abs.gstonepro.api.dao.TaxCRUD;
import india.abs.gstonepro.api.dao.UserCRUD;
import india.abs.gstonepro.api.dao.uqcCRUD;
import india.abs.gstonepro.api.models.BillCharge;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.GoDown;
import india.abs.gstonepro.api.models.GoDownStockDetail;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.ProductGroup;
import india.abs.gstonepro.api.models.ProductStockEntry;
import india.abs.gstonepro.business.CompanyLogic;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;

/**
 *
 * @author Admin
 */
public class MinimalInsertData {

    public static void main(String[] args) {

        insertCompany();
    }

    public static void insertCompany() {
        CompanyCRUD c4 = new CompanyCRUD();
        CompanyLogic co4 = new CompanyLogic();
        ProductCRUD p4 = new ProductCRUD();
        ProductGroupCRUD pg4 = new ProductGroupCRUD();
        GoDownCRUD g4 = new GoDownCRUD();

        
        Date start = new Date(1522540800);
        Calendar c = Calendar.getInstance();
        c.setTime(start);
        c.add(Calendar.YEAR, 1);
        c.add(Calendar.HOUR, -24);
        Date end = c.getTime();

        co4.createCompany(new Company("Doss Maran Soap", "Production", " 12 kk nagar/~/ /~/ ", "Tirunelveli", "Tirunelveli", "Tamil Nadu", "raja@gmail.com", "044-569858957", "8015835298", "331ZTRDGRERZTC", 0L, 0L, 0L, 0L),start,end);
        co4.createCompany(new Company("Upsia Traders", "Distribution", " 12 kk nagar/~/ /~/ ", "Tirunelveli", "Tirunelveli", "Tamil Nadu", "raja@gmail.com", "044-569858957", "8015835298", "331ZTFDGRERZTC", 0L, 0L, 0L, 0L),start,end);
        co4.createCompany(new Company("Bro Foods", "Production", " 12 kk nagar/~/ /~/ ", "Tirunelveli", "Tirunelveli", "Tamil Nadu", "raja@gmail.com", "044-569858957", "8015835298", "331ZERDGRERZTC", 0L, 0L, 0L, 0L),start,end);
        co4.createCompany(new Company("Vendhar Industries", "Production", " 12 kk nagar/~/ /~/ ", "Tirunelveli", "Tirunelveli", "Tamil Nadu", "raja@gmail.com", "044-569858957", "8015835298", "331GTRDGRERZTC", 0L, 0L, 0L, 0L),start,end);
        co4.createCompany(new Company("Sastha Traders", "Distribution", " 12 kk nagar/~/ /~/ ", "Tirunelveli", "Tirunelveli", "Tamil Nadu", "raja@gmail.com", "044-569858957", "8015835298", "331ITRDGRERZTC", 0L, 0L, 0L, 0L),start,end);


        for (Company com : c4.fetchAllCompanies()) {

            ProductGroup pg = new ProductGroup();
            pg.setProductGroupName("No Product Group");
            pg4.createProductGroup(pg, com, false, false);
            pg.setProductGroupName("Drinks & Beverages");
            pg4.createProductGroup(pg, com, true, true);
            pg.setProductGroupName("Washing Soap & Detergents");
            pg4.createProductGroup(pg, com, true, true);
            pg.setProductGroupName("Chocolate & Biscuits");
            pg4.createProductGroup(pg, com, true, true);

            List<GoDownStockDetail> gsds = new ArrayList<GoDownStockDetail>();
            for (GoDown god : g4.fetchAllGodowns(com.getCompanyId())) {
                GoDownStockDetail gsd = new GoDownStockDetail();
                gsd.setGodown(god);

                gsd.setOpeningStock(Long.decode("10000"));
                gsd.setOpeningStockDate(new Date());
                gsd.setOpeningStockValue(new BigDecimal(100000));
                gsds.add(gsd);
            }

            Product product1 = new Product("100gm Evergreen Smart Bar", "100?????? ??????????? ???????? ????", "ESB", "3401.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(436.88), true, "BOXES", "PIECES", 72, 100, false);
            p4.createProduct(com.getCompanyId(), product1, gsds);
            Product product2 = new Product("100gm Sakthi Bleaching Powder", "100?????? ????? ?????????? ?????", "SBP", "3401.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(873.77), true, "BOXES", "PIECES", 72, 1000, false);
            p4.createProduct(com.getCompanyId(), product2, gsds);
            Product product3 = new Product("100gm Sakthi Brass Powder", "100?????? ????? ?????? ?????", "SBP", "3401.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(436.88), true, "BOXES", "PIECES", 72, 1000, false);
            p4.createProduct(com.getCompanyId(), product3, gsds);
            Product product4 = new Product("100gm Sakthi Orange Pouch", "100?????? ????? ?????? ????", "SOP", "3401.0", new Float(28.0), new Float(14.0), new Float(14.0), new BigDecimal(221.14), true, "BOXES", "PIECES", 144, 1000, false);
            p4.createProduct(com.getCompanyId(), product4, gsds);
            Product product5 = new Product("100gm Sakthi Premium Soap", "100?????? ????? ????????? ????", "SPS", "3401.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(388.33), true, "BOXES", "PIECES", 144, 1000, false);
            p4.createProduct(com.getCompanyId(), product5, gsds);
            Product product6 = new Product("100gm Sakthi Sandal Soap", "100?????? ????? ?????? ????", "SSP", "3401.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(436.88), false, "BOXES", "PIECES", 144, 1000, false);
            p4.createProduct(com.getCompanyId(), product6, gsds);
            Product product7 = new Product("100ml 3G Oil", "100???? 3?? ?????", "3GO", "3402.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(268.49), false, "BOXES", "PIECES", 144, 1000, false);
            p4.createProduct(com.getCompanyId(), product7, gsds);
            Product product8 = new Product("100ml Climate(After Wash)", "100???? ???????? (???????? ????)", "CLI", "3402.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(975.56), false, "BOXES", "PIECES", 48, 1000, false);
            p4.createProduct(com.getCompanyId(), product8, gsds);
            Product product9 = new Product("100ml Remotewash Oil", "100???? ?????????? ?????", "RO", "3402.0", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(975.56), true, "BOXES", "PIECES", 48, 1000, false);
            p4.createProduct(com.getCompanyId(), product9, gsds);
            
            Product service1 = new Product("Freight Charges", "", "", "", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(200), false, "BOX", "BOX", 1, 1, true);
            p4.createProduct(com.getCompanyId(), service1, gsds);
            
            Product service2 = new Product("Cutting Charges", "", "", "", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(350), true, "BOX", "BOX", 1, 1, true);
            p4.createProduct(com.getCompanyId(), service2, gsds);
            
            Product service3 = new Product("Delivery Charge", "", "", "", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(400), true, "BOX", "BOX", 1, 1, true);
            p4.createProduct(com.getCompanyId(), service3, gsds);
            
            Product service4 = new Product("Bill Discount", "", "", "", new Float(18.0), new Float(9.0), new Float(9.0), new BigDecimal(-150), false, "BOX", "BOX", 1, 1, true);
            p4.createProduct(com.getCompanyId(), service4, gsds);

        }

        TaxCRUD t4 = new TaxCRUD();
        t4.createTax((float) 0.0);
        t4.createTax((float) 5.0);
        t4.createTax((float) 2.5);
        t4.createTax((float) 12.0);
        t4.createTax((float) 6.0);
        t4.createTax((float) 18.0);
        t4.createTax((float) 9.0);
        t4.createTax((float) 28.0);
        t4.createTax((float) 14.0);

        UserCRUD u4 = new UserCRUD();
        u4.createUser("siv", "77088896", "123", true);

        uqcCRUD uqc = new uqcCRUD();

        uqc.createUQC("BAGS", "Measure", "BAG");
        uqc.createUQC("BALE", "Measure", "BAL");
        uqc.createUQC("BUNDLES", "Measure", "BDL");
        uqc.createUQC("BUCKLES", "Measure", "BKL");
        uqc.createUQC("BILLIONS OF UNITS", "Measure", "BOU");
        uqc.createUQC("BOXES", "Measure", "BOX");
        uqc.createUQC("BOTTLES", "Measure", "BTL");
        uqc.createUQC("BUNCHES", "Measure", "BUN");
        uqc.createUQC("CANS", "Measure", "CAN");
        uqc.createUQC("CUBIC METER", "Volume", "CBM");
        uqc.createUQC("CUBIC CENTIMETER", "Volume", "CCM");
        uqc.createUQC("CENTIMETER", "Length", "CMS");
        uqc.createUQC("CARTONS", "Measure", "CTN");
        uqc.createUQC("DOZEN", "Measure", "DOZ");
        uqc.createUQC("DRUM", "Measure", "DRM");
        uqc.createUQC("GREAT GROSS", "Measure", "GGR");
        uqc.createUQC("GRAMS", "Weight", "GMS");
        uqc.createUQC("GROSS", "Measure", "GRS");
        uqc.createUQC("GROSS YARDS", "Length", "GYD");
        uqc.createUQC("KILOGRAMS", "Weight", "KGS");
        uqc.createUQC("KILOLITER", "Measure", "KLR");
        uqc.createUQC("KILOMETRE", "Length", "KME");
        uqc.createUQC("MILLILITRE", "Volume", "MLT");
        uqc.createUQC("METERS", "Length", "MTR");
        uqc.createUQC("NUMBERS", "Measure", "NOS");
        uqc.createUQC("PACKS", "Measure", "PAC");
        uqc.createUQC("PIECES", "Measure", "PCS");
        uqc.createUQC("PAIRS", "Measure", "PRS");
        uqc.createUQC("QUINTAL", "Measure", "QTL");
        uqc.createUQC("ROLLS", "Measure", "ROL");
        uqc.createUQC("SETS", "Measure", "SET");
        uqc.createUQC("SQUARE FEET", "Area", "SQF");
        uqc.createUQC("SQUARE METERS", "Area", "SQM");
        uqc.createUQC("SQUARE YARDS", "Area", "SQY");
        uqc.createUQC("TABLETS", "Measure", "TBS");
        uqc.createUQC("TEN GROSS", "Measure", "TGM");
        uqc.createUQC("THOUSANDS", "Measure", "THD");
        uqc.createUQC("TONNES", "Measure", "TON");
        uqc.createUQC("TUBES", "Measure", "TUB");
        uqc.createUQC("US GALLONS", "Measure", "UGS");
        uqc.createUQC("UNITS", "Measure", "UNT");
        uqc.createUQC("YARDS", "Measure", "YDS");
        uqc.createUQC("OTHERS", "", "OTH");

        for (Company com : c4.fetchAllCompaniesWithLedgerGroups()) {
            
            for (LedgerGroup l : com.getLedgerGroups()) {
                
                l.printLedgerNames();

            }
        }

    }
}
