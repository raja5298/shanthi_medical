/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.tools.Server;

/**
 *
 * @author SGS
 */
public class H2DBServer {

    static Server h2TCPServer;

    public static Server startH2Server() {

        try {
            h2TCPServer = Server.createTcpServer("-tcpPort", "9688").start();
            if(h2TCPServer.isRunning(true)){
                
            }
            return h2TCPServer;
        } catch (SQLException ex) {
            Logger.getLogger(H2DBServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Server getH2Server() {
        if (h2TCPServer.isRunning(false)) {
            return h2TCPServer;
        } else {
            startH2Server();
             return h2TCPServer;
        }
        
    }
    public static void main(String[] args) {
        startH2Server();
    }

    public static void stopH2Server() {
        if (h2TCPServer.isRunning(false)) {
            h2TCPServer.shutdown();
        }
    }

    
}
