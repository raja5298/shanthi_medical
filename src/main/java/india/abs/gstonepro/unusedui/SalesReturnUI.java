
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.unusedui;

import com.itextpdf.text.DocumentException;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.CompanyPolicy;
import india.abs.gstonepro.api.models.CompanyUserRole;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.ui.utils.NumberTableCellRenderer;
import india.abs.gstonepro.ui.utils.GetProduct;
import india.abs.gstonepro.api.utils.EventStatus;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.api.utils.SystemPolicyUtil;
import india.abs.gstonepro.pdf.TaxInvoiceNew;
import india.abs.gstonepro.ui.AddStockUI;
import india.abs.gstonepro.ui.BackupUI;
import india.abs.gstonepro.ui.BankEntryUI;
import india.abs.gstonepro.ui.CashEntryUI;
import india.abs.gstonepro.ui.ClosingStockUI;
import india.abs.gstonepro.ui.CompanyUI;
import india.abs.gstonepro.ui.CreditNoteDateWiseReportUI;
import india.abs.gstonepro.ui.CreditNoteUI;
import india.abs.gstonepro.ui.CurrentStockReportwithGodownUI;
import india.abs.gstonepro.ui.DashBoardUI;
import india.abs.gstonepro.ui.DayBookUI;
import india.abs.gstonepro.ui.DebitNoteDateWiseReportUI;
import india.abs.gstonepro.ui.DebitNoteUI;
import india.abs.gstonepro.ui.DeductionStockUI;
import india.abs.gstonepro.ui.EstimateReportsGroupUI;
import india.abs.gstonepro.ui.EstimateUI;
import india.abs.gstonepro.ui.GSTR1UI;
import india.abs.gstonepro.ui.JournalEntryUI;
import india.abs.gstonepro.ui.LedgerGroupTrialBalanceUI;
import india.abs.gstonepro.ui.LedgerUI;
import india.abs.gstonepro.ui.LogInUI;
import india.abs.gstonepro.ui.OpeningStockUI;
import india.abs.gstonepro.ui.PartyDetailsUI;
import india.abs.gstonepro.ui.PricingPolicyUI;
import india.abs.gstonepro.ui.ProductGroupUI;
import india.abs.gstonepro.ui.ProductUI;
import india.abs.gstonepro.ui.PurchaseAndPurchaseReturnReportUI;
import india.abs.gstonepro.ui.PurchaseReportGroupUI;
import india.abs.gstonepro.ui.PurchaseUI;
import india.abs.gstonepro.ui.SalesAndSalesReturnReportUI;
import india.abs.gstonepro.ui.SalesB2BUI;
import india.abs.gstonepro.ui.SalesB2CUI;
import india.abs.gstonepro.ui.SalesDateWiseReportUI;
import india.abs.gstonepro.ui.ServiceUI;
import india.abs.gstonepro.ui.SetCompanyUI;
import india.abs.gstonepro.ui.TaxInvoiceReportsGroupUI;
import india.abs.gstonepro.ui.TaxUI;
import india.abs.gstonepro.ui.TrialBalanceUI;
import india.abs.gstonepro.ui.UQCUI;
import india.abs.gstonepro.ui.UserUI;
import india.abs.gstonepro.ui.utils.CheckFinancialYear;
import india.abs.gstonepro.ui.utils.StringToDecimal;
import java.awt.AWTKeyStroke;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author ABS
 */
public class SalesReturnUI extends javax.swing.JFrame {

    DecimalFormat currency = new DecimalFormat("0.000");
    static String CompanyState = SessionDataUtil.getSelectedCompany().getState();

    String HSNCode;
    boolean isIGST = false, isProductTaxInclusive = false, isBasePriceProductTaxInclusive = false;
    int lastBillId;
    Long LedgerId;
    int BillNo = 1, UQC2Value;
    int currentBillNo;
    float ProductRate = 0, BaseProductRate = 0;
    float IGST = 0, CGST = 0, SGST = 0;

    Boolean isEditBill = false;
    Boolean isOldBill = false;

    Company company = SessionDataUtil.getSelectedCompany();
    CompanyPolicy companyPolicy = company.getCompanyPolicy();
    CompanyUserRole companyUserRole = SessionDataUtil.getCompanyUserRole();
    String strFinancialYearStart = "", strFinancialYearEnd = "";
    Ledger ledgerData = new Ledger();
    Product product = new Product();
    Set<Ledger> ledgers = new LedgerLogic().fetchAllPartyAndGOCLedgers();
    List<Ledger> sortedLedgers = new ArrayList<Ledger>(ledgers);
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(company.getCompanyId());

    List<Product> products = new ProductLogic().fetchAllProducts(company.getCompanyId());
    PurchaseSale oldSale = new PurchaseSale();
    StringToDecimal convertDecimal = new StringToDecimal();
    Boolean isInventoryEnabled = SystemPolicyUtil.getSystemPolicy().isInventoryEnabled(),
            isAccountEnabled = SystemPolicyUtil.getSystemPolicy().isAccountsEnabled();

    /**
     * Creates new form Bill
     */
    public SalesReturnUI() {
        initComponents();
        setMenuRoles();
        menuChangeCompany.setVisible(SessionDataUtil.isMultipleCompany());
        setRoles();
        jTable1.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));
        totalTable.getTableHeader().setFont(new Font("Segoe UI", Font.BOLD, 12));

        Collections.sort(sortedLedgers, new Comparator<Ledger>() {
            public int compare(Ledger o1, Ledger o2) {
                return o1.getLedgerName().compareTo(o2.getLedgerName());
            }
        });
        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        setExtendedState(this.MAXIMIZED_BOTH);
        labelCompanyName.setText(SessionDataUtil.getSelectedCompany().getCompanyName());
        labelUserName.setText(SessionDataUtil.getSelectedUser().getUserName());
        dateBill.setDate(new Date());
        setParty();
        setProduct();
        setBanks();
        radioCash.setSelected(true);
        checkRadioButton();

        labelPartyName.setText("");
        labelAddress.setText("");
        labelGSTIN.setText("");

        comboParty.requestFocus();
        SimpleDateFormat sdfr = new SimpleDateFormat("dd-MM-yyyy");
        strFinancialYearStart = sdfr.format(companyPolicy.getFinancialYearStart());
        strFinancialYearEnd = sdfr.format(companyPolicy.getFinancialYearEnd());
        KeyStroke ctrlTab = KeyStroke.getKeyStroke("ctrl TAB");
        KeyStroke ctrlShiftTab = KeyStroke.getKeyStroke("ctrl shift TAB");

        KeyStroke right = KeyStroke.getKeyStroke("alt RIGHT");
        KeyStroke left = KeyStroke.getKeyStroke("alt LEFT");

//         Remove ctrl-tab from normal focus traversal
        Set<AWTKeyStroke> forwardKeys = new HashSet<AWTKeyStroke>(jTabbedPane1.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        forwardKeys.remove(ctrlTab);
        jTabbedPane1.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, forwardKeys);

        // Remove ctrl-shift-tab from normal focus traversal
        Set<AWTKeyStroke> backwardKeys = new HashSet<AWTKeyStroke>(jTabbedPane1.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
        backwardKeys.remove(ctrlShiftTab);
        jTabbedPane1.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, backwardKeys);

//         Add keys to the tab's input map
        InputMap inputMap = jTabbedPane1.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(right, "navigateNext");
        inputMap.put(left, "navigatePrevious");

        JTextComponent editor = (JTextComponent) comboProduct.getEditor().getEditorComponent();
        editor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboProduct.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Product product : products) {
                        if (product.getName().toLowerCase().contains(val.toLowerCase())) {
                            scripts.add(product.getName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboProduct.setModel(model);
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboProduct.setPopupVisible(false);
                        comboProduct.setSelectedItem(comboProduct.getSelectedItem());
                        getProductDetails();
                    } else {
                        comboProduct.getEditor().setItem(val);
                        comboProduct.setPopupVisible(true);
                    }
                }
            }
        });

        JTextComponent editor1 = (JTextComponent) comboParty.getEditor().getEditorComponent();
        editor1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_DOWN && evt.getKeyCode() != java.awt.event.KeyEvent.VK_UP) {
                    String val = (String) comboParty.getEditor().getItem();
                    ArrayList<String> scripts = new ArrayList<>();

                    for (Ledger ledger : sortedLedgers) {
                        if (ledger.getLedgerName().toLowerCase().contains(val.toLowerCase())) {
                            scripts.add(ledger.getLedgerName());
                        }
                    }
                    String[] myArray = new String[scripts.size()];
                    scripts.toArray(myArray);
                    DefaultComboBoxModel model = new DefaultComboBoxModel(myArray);
                    comboParty.setModel(model);
                    if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                        comboParty.setPopupVisible(false);
                        comboParty.setSelectedItem(comboParty.getSelectedItem());
                        getPartyDetails();
                    } else {
                        comboParty.getEditor().setItem(val);
                        comboParty.setPopupVisible(true);
                    }
                }
            }
        });

    }

    public void setMenuRoles() {
        if (!isAccountEnabled) {
            menuBar.remove(menuAccounts);
        }
        if (!isInventoryEnabled) {
            menuBar.remove(menuInventory);
            menuBar.remove(menuInventoryReport);
        }
    }

    public void setRoles() {

        menuProduct.setVisible(companyUserRole.isProductView());
        menuPricingPolicy.setVisible(companyUserRole.isPricingPolicyView());
        menuParty.setVisible(companyUserRole.isPartyView());
        menuCurrentStock.setVisible(companyUserRole.isInventoryView());
//        menuSales.setVisible(companyUserRole.isSalesCreate());
//        menuSalesReport.setVisible(companyUserRole.isSalesView());
        menuPurchase.setVisible(companyUserRole.isPurchaseCreate());
        menuPurchaseReport.setVisible(companyUserRole.isPurchaseView());
//        menuSalesAndReturn.setVisible(companyUserRole.isCrdrNoteView());
//        menuPurchaseReturn.setVisible(companyUserRole.isCrdrNoteView());
        menuCurrentStock.setVisible(companyUserRole.isInventoryView());
        menuAddStock.setVisible(companyUserRole.isInventoryAdd());
        menuDeductionStock.setVisible(companyUserRole.isInventoryDeduct());
//        menuLedger.setVisible(companyUserRole.isAccountsLedger());
        menuDayBook.setVisible(companyUserRole.isAccountsDayBook());
        menuJournalEntry.setVisible(companyUserRole.isAccountsJournal());
//        menuCash.setVisible(companyUserRole.isAccountsCash());
//        menuBank.setVisible(companyUserRole.isAccountsBank());
        menuTrialBalance.setVisible(companyUserRole.isAccountsTrailBalance());
    }

    public void setBanks() {
        for (Ledger bank : Banks) {
            comboBank.addItem(bank.getLedgerName());
        };
    }

    public void checkRadioButton() {
        comboBank.setEnabled(radioBank.isSelected());
    }

    public void EditOldBill(String id) {
        isOldBill = true;
        oldSale = new PurchaseSaleLogic().getPSDetail(id);
        List<PurchaseSaleLineItem> psLIs = oldSale.getPsLineItems();
        List<PurchaseSaleTaxSummary> psTSs = oldSale.getPsTaxSummaries();
        oldSale.printCollections();

        comboParty.setSelectedItem(oldSale.getLedger().getLedgerName());
        txtBillNo.setText(oldSale.getBillNo());
        txtCompanyName.setText(oldSale.getLedger().getLedgerName());
        String address = oldSale.getLedger().getAddress();
        String[] words = address.split("/~/");
        txtAddr1.setText(words[0]);
        txtAddr2.setText(words[1]);
        txtAddr3.setText(words[2]);
        txtCity.setText(oldSale.getLedger().getCity());
        txtDistrict.setText(oldSale.getLedger().getDistrict());
        txtState.setText(oldSale.getLedger().getState());
        txtEmail.setText(oldSale.getLedger().getEmail());
        txtMobile.setText(oldSale.getLedger().getMobile());
        txtPhone.setText(oldSale.getLedger().getPhone());
        txtGSTIN.setText(oldSale.getLedger().getGSTIN());
        txtThrough.setText(oldSale.getThrough());
        txtReferenceNumber.setText(oldSale.getReferenceNumber());
        isIGST = oldSale.isIsIGST();

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        for (PurchaseSaleLineItem item : psLIs) {
            Object[] row = {item.getLineNumber(),
                item.getProductName(),
                item.getHsnSac(),
                item.getUqcOne(),
                item.getUqcOneQuantity(),
                item.getUqcOneRate(),
                new BigDecimal(item.getUqcOneQuantity()).multiply(item.getUqcOneRate()),
                item.getUqcTwo(),
                item.getUqcTwoQuantity(),
                item.getUqcTwoRate(),
                new BigDecimal(item.getUqcTwoQuantity()).multiply(item.getUqcTwoRate()),
                item.getUqcOneQuantity() + " " + item.getUqcOne() + "," + item.getUqcTwoQuantity() + " " + item.getUqcTwo(),
                item.getStockQuantity(),
                item.getDiscountPercent(),
                item.getDiscountValue(),
                item.getValue(),
                item.getIgstPercentage(), item.getIgstValue(),
                item.getCgstPercentage(), item.getCgstValue(),
                item.getSgstPercentage(), item.getSgstValue(),
                item.getUqc1BaseRate(), item.getUqc2BaseRate(), item.getProfit()};
            model.addRow(row);
        }
        clear();
        TotalCalculate();
        jTableRender();
        comboProduct.requestFocus();
    }

    public String setStockConverstion(int qty, String UQC1, String UQC2, int UQC2Value) {
        int UQC1Count = qty / UQC2Value;
        int UQC2Count = qty % UQC2Value;
        String result = UQC1Count + " " + UQC1 + ", " + UQC2Count + " " + UQC2;
        return result;
    }

    public void getProductDetails() {
        String productName = (String) comboProduct.getSelectedItem();
        if ((productName != null) && (!(productName.equals("")))) {
            for (Product product : products) {
                if (product.getName().equals(productName)) {

                    labelUQC1.setText(product.getUQC1());
                    labelUQC1forPrice.setText(product.getUQC1());
                    labelUQC2.setText(product.getUQC2());
                    labelUQC2forPrice.setText(product.getUQC2());
                    labelUQC1BasePrice.setText(product.getUQC1());
                    labelUQC2BasePrice.setText(product.getUQC2());
                    HSNCode = product.getHsnCode();
                    UQC2Value = product.getUQC2Value();

                    ProductRate = product.getProductRate().floatValue();

                    IGST = product.getIgstPercentage();
                    CGST = product.getCgstPercentage();
                    SGST = product.getSgstPercentage();
                    txtIGSTper.setText(String.valueOf(product.getIgstPercentage()));
                    txtCGSTper.setText(String.valueOf(product.getCgstPercentage()));
                    txtSGSTper.setText(String.valueOf(product.getSgstPercentage()));
                    isProductTaxInclusive = product.isInclusiveOfGST();
                    isBasePriceProductTaxInclusive = product.isBasePriceInclusiveOfGST();

                    float price = 0, basePrice = 0;
                    if (isBasePriceProductTaxInclusive) {
                        basePrice = (BaseProductRate / (100 + IGST)) * 100;
                    } else {
                        basePrice = BaseProductRate;
                    }
                    if (isProductTaxInclusive) {
                        price = (ProductRate / (100 + IGST)) * 100;
                    } else {
                        price = ProductRate;
                    }
                    txtUQC1Rate.setText(currency.format(price * UQC2Value));
                    txtUQC2Rate.setText(currency.format(price));
                    txtUQC1Qty.requestFocus();
                }
            }
        } else {
            txtDiscount.setText("");
            txtDiscount.setText("");
            txtUQC1Rate.setText("");
            txtUQC2Rate.setText("");
            txtAmount.setText("");
            txtIGSTper.setText("");
            txtCGSTper.setText("");
            txtSGSTper.setText("");
            txtIGSTamt.setText("");
            txtCGSTamt.setText("");
            txtSGSTamt.setText("");
        }
    }

    public void setProduct() {
        for (Product product : products) {
            comboProduct.addItem(product.getName());
        }
        comboProduct.setSelectedItem("");
    }

    public void setParty() {
        for (Ledger ledger : sortedLedgers) {
            comboParty.addItem(ledger.getLedgerName());
        };
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        comboParty = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtCompanyName = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtAddr1 = new javax.swing.JTextField();
        txtAddr2 = new javax.swing.JTextField();
        txtAddr3 = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDistrict = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtMobile = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtGSTIN = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        dateBill = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        txtState = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtThrough = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtReferenceNumber = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        totalTable = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        jPanel2 = new javax.swing.JPanel();
        comboProduct = new javax.swing.JComboBox<>();
        labelProduct = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        txtUQC1Rate = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDiscountPer = new javax.swing.JTextField();
        txtDiscount = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            }
        };
        txtUQC2Qty = new javax.swing.JTextField();
        txtUQC1Qty = new javax.swing.JTextField();
        txtUQC2Rate = new javax.swing.JTextField();
        txtUQC1BaseRate = new javax.swing.JTextField();
        txtUQC2BaseRate = new javax.swing.JTextField();
        labelBasePrice = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtBillNo = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        txtIGSTper = new javax.swing.JLabel();
        txtIGSTamt = new javax.swing.JLabel();
        txtCGSTamt = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        txtSGSTamt = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtCGSTper = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        txtSGSTper = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        labelUQC1 = new javax.swing.JLabel();
        labelUQC2 = new javax.swing.JLabel();
        labelUQC1BasePrice = new javax.swing.JLabel();
        labelUQC2BasePrice = new javax.swing.JLabel();
        labelUQC1forPrice = new javax.swing.JLabel();
        labelUQC2forPrice = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        txtAmountPaid = new javax.swing.JTextField();
        radioCash = new javax.swing.JRadioButton();
        jLabel30 = new javax.swing.JLabel();
        radioBank = new javax.swing.JRadioButton();
        jLabel31 = new javax.swing.JLabel();
        comboBank = new javax.swing.JComboBox<>();
        jLabel23 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        labelCompanyName = new javax.swing.JLabel();
        labelUserName = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        labelPartyName = new javax.swing.JLabel();
        labelAddress = new javax.swing.JLabel();
        labelGSTIN = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuDashboard = new javax.swing.JMenuItem();
        menuChangeCompany = new javax.swing.JMenuItem();
        menuParty = new javax.swing.JMenuItem();
        menuProductGroup = new javax.swing.JMenuItem();
        menuProduct = new javax.swing.JMenuItem();
        menuService = new javax.swing.JMenuItem();
        menuPricingPolicy = new javax.swing.JMenuItem();
        menuNewBill = new javax.swing.JMenu();
        menuSalesHeader = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSaleB2B = new javax.swing.JMenuItem();
        menuSaleB2C = new javax.swing.JMenuItem();
        menuEstimate = new javax.swing.JMenuItem();
        menuCreditNote = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseHeader = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuPurchase = new javax.swing.JMenuItem();
        menuDebitNote = new javax.swing.JMenuItem();
        menuReports = new javax.swing.JMenu();
        menuSalesReportHeader = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuSalesReportGroup = new javax.swing.JMenuItem();
        menuEstimateReportGroup = new javax.swing.JMenuItem();
        menuCreditNoteReport = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportHeader = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menuPurchaseReportGroup = new javax.swing.JMenuItem();
        menuDebitNoteReport = new javax.swing.JMenuItem();
        menuInventory = new javax.swing.JMenu();
        menuAddStock = new javax.swing.JMenuItem();
        menuDeductionStock = new javax.swing.JMenuItem();
        menuOpeningStock = new javax.swing.JMenuItem();
        menuInventoryReport = new javax.swing.JMenu();
        menuCurrentStock = new javax.swing.JMenuItem();
        menuClosingStock = new javax.swing.JMenuItem();
        menuAccounts = new javax.swing.JMenu();
        menuLedger = new javax.swing.JMenuItem();
        menuJournalEntry = new javax.swing.JMenuItem();
        menuCashEntry = new javax.swing.JMenuItem();
        menuBankEntry = new javax.swing.JMenuItem();
        menuSalesPaymnet = new javax.swing.JMenuItem();
        menuPurchaseReport = new javax.swing.JMenuItem();
        menuAccountsReport = new javax.swing.JMenuItem();
        menuDayBook = new javax.swing.JMenuItem();
        menuCashBook = new javax.swing.JMenuItem();
        menuBankBook = new javax.swing.JMenuItem();
        menuLedgerBook = new javax.swing.JMenuItem();
        menuLedgerGroupBalance = new javax.swing.JMenuItem();
        menuTrialBalance = new javax.swing.JMenuItem();
        menuGST = new javax.swing.JMenu();
        menuGSTR1 = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        menuTax = new javax.swing.JMenuItem();
        menuUQC = new javax.swing.JMenuItem();
        menuCompany = new javax.swing.JMenuItem();
        menuUsers = new javax.swing.JMenuItem();
        menuBackUP = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sales Return");

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel14.setText("Party ");

        comboParty.setEditable(true);
        comboParty.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboParty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboPartyFocusGained(evt);
            }
        });
        comboParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPartyActionPerformed(evt);
            }
        });
        comboParty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboPartyKeyPressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel15.setText("Party  Name ");

        txtCompanyName.setEditable(false);
        txtCompanyName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCompanyNameActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel16.setText("Address");

        txtAddr1.setEditable(false);
        txtAddr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr1ActionPerformed(evt);
            }
        });

        txtAddr2.setEditable(false);
        txtAddr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr2ActionPerformed(evt);
            }
        });

        txtAddr3.setEditable(false);
        txtAddr3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddr3ActionPerformed(evt);
            }
        });

        txtCity.setEditable(false);
        txtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCityActionPerformed(evt);
            }
        });

        jLabel20.setText("City");

        jLabel25.setText("District");

        txtDistrict.setEditable(false);
        txtDistrict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistrictActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel17.setText("State ");

        jLabel26.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel26.setText("Mobile Number");

        txtMobile.setEditable(false);
        txtMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileActionPerformed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel24.setText("Phone Number");

        txtPhone.setEditable(false);
        txtPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhoneActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel21.setText("GSTIN/UIN");

        txtGSTIN.setEditable(false);
        txtGSTIN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGSTINActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Date :");

        dateBill.setDateFormatString("dd-MM-yyyy");

        jLabel6.setText("Email");

        txtEmail.setEditable(false);

        txtState.setEditable(false);
        txtState.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N

        jLabel4.setText("Through");

        txtThrough.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtThroughActionPerformed(evt);
            }
        });

        jLabel18.setText("Reference No.");

        totalTable.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        totalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Total Amount", "IGST", "CGST", "SGST ", "Profit", "Total Amount (Incl.Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(totalTable);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateBill, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCity, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                            .addComponent(txtAddr1))
                        .addGap(18, 18, 18)
                        .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(txtCompanyName))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel18))
                                .addGap(29, 29, 29)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .addComponent(txtGSTIN)
                                    .addComponent(txtState)
                                    .addComponent(txtEmail)
                                    .addComponent(txtThrough)
                                    .addComponent(txtReferenceNumber))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 685, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(dateBill, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)
                        .addComponent(comboParty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtAddr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtGSTIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtThrough, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtReferenceNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(98, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Party Address", jPanel1);

        comboProduct.setEditable(true);
        comboProduct.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        comboProduct.setName(""); // NOI18N
        comboProduct.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboProductItemStateChanged(evt);
            }
        });
        comboProduct.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
                comboProductAncestorRemoved(evt);
            }
        });
        comboProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboProductFocusGained(evt);
            }
        });
        comboProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductActionPerformed(evt);
            }
        });

        labelProduct.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        labelProduct.setText("Product ");
        labelProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                labelProductKeyReleased(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                btnAddAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        btnAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddKeyPressed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnDelete.setMnemonic('d');
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnSave.setMnemonic('e');
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        btnUpdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnUpdateKeyPressed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnPrint.setText("Save & Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnClear.setMnemonic('l');
        btnClear.setText("Clear ");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel35.setText("Quantity");

        txtUQC1Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1RateActionPerformed(evt);
            }
        });
        txtUQC1Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1RateKeyTyped(evt);
            }
        });

        jLabel3.setText("Price");

        txtDiscountPer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountPerActionPerformed(evt);
            }
        });
        txtDiscountPer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountPerKeyTyped(evt);
            }
        });

        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });

        jLabel5.setText("Discount %");

        jLabel27.setText("Disount");

        jTable1.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Product", "HSN/SAC Code", "UQC1", "UQC1Qty", "UQC1Rate", "UQC1Value", "UQC2", "UQC2 Qty", "UQC2Rate", "UQC2Value", "Quantity", "stockQuantity", "Discount %", "Discount", "Amount", "IGST %", "IGST", "CGST %", "CGST", "SGST %", "SGST", "UQC1BaseRate", "UQC2BaseRate", "Profit", "Amount (Incl.Tax)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(3).setMinWidth(0);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(5).setMinWidth(0);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(5).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(6).setMinWidth(0);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(6).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(7).setMinWidth(0);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(7).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(8).setMinWidth(0);
            jTable1.getColumnModel().getColumn(8).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(8).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(9).setMinWidth(0);
            jTable1.getColumnModel().getColumn(9).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(9).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(10).setMinWidth(0);
            jTable1.getColumnModel().getColumn(10).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(10).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(12).setMinWidth(0);
            jTable1.getColumnModel().getColumn(12).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(12).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(22).setMinWidth(0);
            jTable1.getColumnModel().getColumn(22).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(22).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(23).setMinWidth(0);
            jTable1.getColumnModel().getColumn(23).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(23).setMaxWidth(0);
        }

        txtUQC2Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2QtyActionPerformed(evt);
            }
        });
        txtUQC2Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2QtyKeyTyped(evt);
            }
        });

        txtUQC1Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1QtyActionPerformed(evt);
            }
        });
        txtUQC1Qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC1QtyKeyTyped(evt);
            }
        });

        txtUQC2Rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2RateActionPerformed(evt);
            }
        });
        txtUQC2Rate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUQC2RateKeyTyped(evt);
            }
        });

        txtUQC1BaseRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC1BaseRateActionPerformed(evt);
            }
        });

        txtUQC2BaseRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUQC2BaseRateActionPerformed(evt);
            }
        });

        labelBasePrice.setText("Base Price");

        jLabel1.setText("Bill No");

        txtBillNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBillNoKeyPressed(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel38.setText("IGST %");

        txtIGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        txtIGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        txtCGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel39.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel39.setText("IGST");

        jLabel40.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel40.setText("CGST ");

        txtSGSTamt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel41.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel41.setText("SGST");

        txtCGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel42.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel42.setText("CGST %");

        txtSGSTper.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        jLabel43.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel43.setText("SGST %");

        jLabel44.setText("Amount");

        txtAmount.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        labelUQC1.setText("UQC1");
        labelUQC1.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC2.setText("UQC2");
        labelUQC2.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC1BasePrice.setText("UQC1");
        labelUQC1BasePrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1BasePrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1BasePrice.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC2BasePrice.setText("UQC2");
        labelUQC2BasePrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2BasePrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2BasePrice.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC1forPrice.setText("UQC1");
        labelUQC1forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC1forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        labelUQC2forPrice.setText("UQC2");
        labelUQC2forPrice.setMaximumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setMinimumSize(new java.awt.Dimension(97, 14));
        labelUQC2forPrice.setPreferredSize(new java.awt.Dimension(97, 14));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelProduct)
                                    .addComponent(comboProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnUpdate)
                                .addGap(18, 18, 18)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtUQC1BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC1BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel3)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(46, 46, 46)
                                        .addComponent(jLabel27))
                                    .addComponent(labelBasePrice))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtUQC2BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(labelUQC2BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel43)
                                    .addComponent(jLabel42))
                                .addGap(10, 10, 10)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtSGSTper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtCGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel40))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtIGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtIGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtBillNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelProduct)
                            .addComponent(jLabel35))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(137, 137, 137))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel44, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(1, 1, 1)
                                                .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel42))
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel39)
                                                        .addComponent(txtIGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(txtIGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(txtCGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(txtCGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel40))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel41)
                                                        .addComponent(txtSGSTper, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel43)
                                                        .addComponent(txtSGSTamt, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(comboProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(txtUQC1Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(txtUQC2Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(labelUQC2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(labelUQC1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(46, 46, 46)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(btnAdd)
                                            .addComponent(btnUpdate)
                                            .addComponent(btnDelete)
                                            .addComponent(btnClear))))
                                .addContainerGap())))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(labelBasePrice)
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUQC1BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC1BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUQC2BaseRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC2BasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUQC1Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUQC2Rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC1forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelUQC2forPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDiscountPer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );

        jTabbedPane1.addTab("Product Details", jPanel2);

        txtAmountPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAmountPaidKeyTyped(evt);
            }
        });

        buttonGroup1.add(radioCash);
        radioCash.setText("Cash");
        radioCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCashActionPerformed(evt);
            }
        });

        jLabel30.setText("Payment Type");

        buttonGroup1.add(radioBank);
        radioBank.setText("Bank");
        radioBank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioBankActionPerformed(evt);
            }
        });

        jLabel31.setText("Bank");

        jLabel23.setText("Amount");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(radioBank)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addComponent(radioCash))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBank, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(txtAmountPaid, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(jLabel31)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioCash)
                    .addComponent(txtAmountPaid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioBank)
                .addContainerGap(457, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Payment", jPanel6);

        jLabel19.setText("Company :");

        labelCompanyName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelCompanyName.setText("-");

        labelUserName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        labelUserName.setText("-");

        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        jLabel22.setText("User :");

        labelPartyName.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelPartyName.setText("name");

        labelAddress.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelAddress.setText("address");

        labelGSTIN.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        labelGSTIN.setText("gstin");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel7.setText("Sales Return");

        menuFile.setMnemonic('f');
        menuFile.setText("File");

        menuDashboard.setText("Dashboard");
        menuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDashboardActionPerformed(evt);
            }
        });
        menuFile.add(menuDashboard);

        menuChangeCompany.setText("Change Company");
        menuChangeCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChangeCompanyActionPerformed(evt);
            }
        });
        menuFile.add(menuChangeCompany);

        menuParty.setText("Party");
        menuParty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPartyActionPerformed(evt);
            }
        });
        menuFile.add(menuParty);

        menuProductGroup.setText("Product Group");
        menuProductGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductGroupActionPerformed(evt);
            }
        });
        menuFile.add(menuProductGroup);

        menuProduct.setText("Product");
        menuProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuProductActionPerformed(evt);
            }
        });
        menuFile.add(menuProduct);

        menuService.setText("Service");
        menuService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiceActionPerformed(evt);
            }
        });
        menuFile.add(menuService);

        menuPricingPolicy.setText("Pricing Policy");
        menuPricingPolicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPricingPolicyActionPerformed(evt);
            }
        });
        menuFile.add(menuPricingPolicy);

        menuBar.add(menuFile);

        menuNewBill.setMnemonic('n');
        menuNewBill.setText("New...");

        menuSalesHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesHeader.setText("Sales");
        menuSalesHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuSalesHeader);
        menuNewBill.add(jSeparator1);

        menuSaleB2B.setText("Sales B2B");
        menuSaleB2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2BActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2B);

        menuSaleB2C.setText("Sales B2C");
        menuSaleB2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaleB2CActionPerformed(evt);
            }
        });
        menuNewBill.add(menuSaleB2C);

        menuEstimate.setText("Estimate");
        menuEstimate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateActionPerformed(evt);
            }
        });
        menuNewBill.add(menuEstimate);

        menuCreditNote.setText("Credit Note");
        menuCreditNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuCreditNote);
        menuNewBill.add(jSeparator2);

        menuPurchaseHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseHeader.setText("Purchase");
        menuPurchaseHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuNewBill.add(menuPurchaseHeader);
        menuNewBill.add(jSeparator3);

        menuPurchase.setText("Purchase");
        menuPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseActionPerformed(evt);
            }
        });
        menuNewBill.add(menuPurchase);

        menuDebitNote.setText("Debit Note");
        menuDebitNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteActionPerformed(evt);
            }
        });
        menuNewBill.add(menuDebitNote);

        menuBar.add(menuNewBill);

        menuReports.setMnemonic('r');
        menuReports.setText("Reports");

        menuSalesReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuSalesReportHeader.setText("Sales");
        menuSalesReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuSalesReportHeader);
        menuReports.add(jSeparator4);

        menuSalesReportGroup.setText("Sales");
        menuSalesReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuSalesReportGroup);

        menuEstimateReportGroup.setText("Estimate");
        menuEstimateReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEstimateReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuEstimateReportGroup);

        menuCreditNoteReport.setText("Credit Note");
        menuCreditNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuCreditNoteReport);
        menuReports.add(jSeparator5);

        menuPurchaseReportHeader.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuPurchaseReportHeader.setText("Purchase");
        menuPurchaseReportHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuReports.add(menuPurchaseReportHeader);
        menuReports.add(jSeparator6);

        menuPurchaseReportGroup.setText("Purchase");
        menuPurchaseReportGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportGroupActionPerformed(evt);
            }
        });
        menuReports.add(menuPurchaseReportGroup);

        menuDebitNoteReport.setText("Debit Note");
        menuDebitNoteReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDebitNoteReportActionPerformed(evt);
            }
        });
        menuReports.add(menuDebitNoteReport);

        menuBar.add(menuReports);

        menuInventory.setMnemonic('i');
        menuInventory.setText("Inventory");

        menuAddStock.setText("Add Stock");
        menuAddStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuAddStock);

        menuDeductionStock.setText("Deduct Stock");
        menuDeductionStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDeductionStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuDeductionStock);

        menuOpeningStock.setText("Opening Stock");
        menuOpeningStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOpeningStockActionPerformed(evt);
            }
        });
        menuInventory.add(menuOpeningStock);

        menuBar.add(menuInventory);

        menuInventoryReport.setMnemonic('t');
        menuInventoryReport.setText("Inventory Reports");

        menuCurrentStock.setText("Current Stock");
        menuCurrentStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCurrentStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuCurrentStock);

        menuClosingStock.setText("Closing Stock");
        menuClosingStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClosingStockActionPerformed(evt);
            }
        });
        menuInventoryReport.add(menuClosingStock);

        menuBar.add(menuInventoryReport);

        menuAccounts.setMnemonic('a');
        menuAccounts.setText("Accounts");

        menuLedger.setText("Ledger");
        menuLedger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedger);

        menuJournalEntry.setText("Journal Entry");
        menuJournalEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJournalEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuJournalEntry);

        menuCashEntry.setText("Cash Entry");
        menuCashEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuCashEntry);

        menuBankEntry.setText("Bank Entry");
        menuBankEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBankEntryActionPerformed(evt);
            }
        });
        menuAccounts.add(menuBankEntry);

        menuSalesPaymnet.setText("Sales Payment");
        menuSalesPaymnet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalesPaymnetActionPerformed(evt);
            }
        });
        menuAccounts.add(menuSalesPaymnet);

        menuPurchaseReport.setText("Purchase Payment");
        menuPurchaseReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPurchaseReportActionPerformed(evt);
            }
        });
        menuAccounts.add(menuPurchaseReport);

        menuAccountsReport.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        menuAccountsReport.setText("Reports");
        menuAccountsReport.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        menuAccounts.add(menuAccountsReport);

        menuDayBook.setText("Day Book");
        menuDayBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDayBookActionPerformed(evt);
            }
        });
        menuAccounts.add(menuDayBook);

        menuCashBook.setText("Cash Book");
        menuAccounts.add(menuCashBook);

        menuBankBook.setText("Bank Book");
        menuAccounts.add(menuBankBook);

        menuLedgerBook.setText("Ledger Book");
        menuAccounts.add(menuLedgerBook);

        menuLedgerGroupBalance.setText("Ledger Group Balance");
        menuLedgerGroupBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuLedgerGroupBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuLedgerGroupBalance);

        menuTrialBalance.setText("Trial Balance");
        menuTrialBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTrialBalanceActionPerformed(evt);
            }
        });
        menuAccounts.add(menuTrialBalance);

        menuBar.add(menuAccounts);

        menuGST.setMnemonic('g');
        menuGST.setText("GST");

        menuGSTR1.setText("GSTR1");
        menuGSTR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGSTR1ActionPerformed(evt);
            }
        });
        menuGST.add(menuGSTR1);

        menuBar.add(menuGST);

        menuSettings.setMnemonic('e');
        menuSettings.setText("Settings");

        menuTax.setText("Tax");
        menuTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTaxActionPerformed(evt);
            }
        });
        menuSettings.add(menuTax);

        menuUQC.setText("UQC");
        menuUQC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUQCActionPerformed(evt);
            }
        });
        menuSettings.add(menuUQC);

        menuCompany.setText("Company");
        menuCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompanyActionPerformed(evt);
            }
        });
        menuSettings.add(menuCompany);

        menuUsers.setText("Users");
        menuUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsersActionPerformed(evt);
            }
        });
        menuSettings.add(menuUsers);

        menuBackUP.setText("Backup/Restore");
        menuBackUP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBackUPActionPerformed(evt);
            }
        });
        menuSettings.add(menuBackUP);

        menuBar.add(menuSettings);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelCompanyName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(labelPartyName)
                    .addComponent(labelAddress)
                    .addComponent(labelGSTIN)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelUserName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout)
                .addGap(8, 8, 8))
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1245, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(labelCompanyName)
                    .addComponent(labelUserName)
                    .addComponent(btnLogout)
                    .addComponent(jLabel22)
                    .addComponent(labelPartyName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelGSTIN)
                .addGap(11, 11, 11)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPartyActionPerformed
//        getPartyDetails();
    }//GEN-LAST:event_comboPartyActionPerformed

    public void getPartyDetails() {
        String name = (String) comboParty.getSelectedItem();

        if ("Select the Party".equals(name)) {
            txtCompanyName.setText("");
            txtAddr1.setText("");
            txtAddr2.setText("");
            txtAddr3.setText("");
            txtCity.setText("");
            txtDistrict.setText("");
            txtState.setText("");
            txtEmail.setText("");
            txtMobile.setText("");
            txtPhone.setText("");
            txtGSTIN.setText("");

            labelPartyName.setText("");
            labelAddress.setText("");
            labelGSTIN.setText("");

        } else {
            for (Ledger ledger : sortedLedgers) {
                if (ledger.getLedgerName().equals(name)) {

                    ledgerData = ledger;
                    LedgerId = ledger.getLedgerId();
                    txtCompanyName.setText(ledger.getLedgerName());
                    String address = ledger.getAddress();
                    String[] words = address.split("/~/");
                    txtAddr1.setText(words[0]);
                    txtAddr2.setText(words[1]);
                    txtAddr3.setText(words[2]);
                    txtCity.setText(ledger.getCity());
                    txtDistrict.setText(ledger.getDistrict());
                    txtState.setText(ledger.getState());
                    txtEmail.setText(ledger.getEmail());
                    txtMobile.setText(ledger.getMobile());
                    txtPhone.setText(ledger.getPhone());
                    txtGSTIN.setText(ledger.getGSTIN());

                    labelPartyName.setText(ledger.getLedgerName());
                    labelAddress.setText(words[0] + "  " + words[1] + "  " + words[2]);
                    labelGSTIN.setText(ledger.getGSTIN());

                    String selectedState = txtState.getText();
                    isIGST = !(selectedState.equals(CompanyState));

                    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                    if (model.getRowCount() > 0) {
                        model.setRowCount(0);
                    }
                    DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
                    if (model2.getRowCount() > 0) {
                        model2.removeRow(0);
                    }
                    setTable();
                    txtThrough.requestFocus();
                }
            };

        }
    }

    private void txtCompanyNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCompanyNameActionPerformed
        txtAddr1.requestFocus();
    }//GEN-LAST:event_txtCompanyNameActionPerformed

    private void txtAddr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr1ActionPerformed
        txtAddr2.requestFocus();
    }//GEN-LAST:event_txtAddr1ActionPerformed

    private void txtAddr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr2ActionPerformed
        txtAddr3.requestFocus();
    }//GEN-LAST:event_txtAddr2ActionPerformed

    private void txtAddr3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddr3ActionPerformed
        txtCity.requestFocus();
    }//GEN-LAST:event_txtAddr3ActionPerformed
    public void jTableRender() {
        //jTable1.getColumnModel().getColumn(6).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(14).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(15).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(17).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(19).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(21).setCellRenderer(new NumberTableCellRenderer());
        jTable1.getColumnModel().getColumn(25).setCellRenderer(new NumberTableCellRenderer());

        totalTable.getColumnModel().getColumn(0).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(1).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(2).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(3).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(4).setCellRenderer(new NumberTableCellRenderer());
        totalTable.getColumnModel().getColumn(5).setCellRenderer(new NumberTableCellRenderer());
    }

    public void TotalCalculate() {
        float totalIGST = 0, totalCGST = 0, totalSGST = 0, totalAmount = 0, totalProfit = 0, totalAmountWithTax = 0;

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }

        for (int i = 0; i < model.getRowCount(); i++) {
            totalAmount = (float) model.getValueAt(i, 15) + totalAmount;
            totalIGST = (float) model.getValueAt(i, 17) + totalIGST;
            totalCGST = (float) model.getValueAt(i, 19) + totalCGST;
            totalSGST = (float) model.getValueAt(i, 21) + totalSGST;
            totalProfit = (float) model.getValueAt(i, 24) + totalProfit;
            totalAmountWithTax = (float) model.getValueAt(i, 25) + totalAmountWithTax;
        }
        Object[] row = {totalAmount, totalIGST, totalCGST, totalSGST, totalProfit, totalAmountWithTax};
        model2.addRow(row);
    }
    private void txtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCityActionPerformed
        txtDistrict.requestFocus();
    }//GEN-LAST:event_txtCityActionPerformed

    private void txtPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhoneActionPerformed
        txtGSTIN.requestFocus();
    }//GEN-LAST:event_txtPhoneActionPerformed

    private void txtMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileActionPerformed
        txtPhone.requestFocus();
    }//GEN-LAST:event_txtMobileActionPerformed

    private void txtGSTINActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGSTINActionPerformed

    }//GEN-LAST:event_txtGSTINActionPerformed

    public void setTable() {
        jTable1.getColumnModel().getColumn(16).setMinWidth(100);
        jTable1.getColumnModel().getColumn(16).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(17).setMinWidth(100);
        jTable1.getColumnModel().getColumn(17).setMaxWidth(100);

        jTable1.getColumnModel().getColumn(18).setMinWidth(100);
        jTable1.getColumnModel().getColumn(18).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(19).setMinWidth(100);
        jTable1.getColumnModel().getColumn(19).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(20).setMinWidth(100);
        jTable1.getColumnModel().getColumn(20).setMaxWidth(100);
        jTable1.getColumnModel().getColumn(21).setMinWidth(100);
        jTable1.getColumnModel().getColumn(21).setMaxWidth(100);

        totalTable.getColumnModel().getColumn(0).setMinWidth(0);
        totalTable.getColumnModel().getColumn(0).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(1).setMinWidth(0);
        totalTable.getColumnModel().getColumn(1).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(2).setMinWidth(0);
        totalTable.getColumnModel().getColumn(2).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(3).setMinWidth(0);
        totalTable.getColumnModel().getColumn(3).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(4).setMinWidth(0);
        totalTable.getColumnModel().getColumn(4).setMaxWidth(0);
        totalTable.getColumnModel().getColumn(5).setMinWidth(0);
        totalTable.getColumnModel().getColumn(5).setMaxWidth(0);
        if (isIGST) {

            jTable1.getColumnModel().getColumn(18).setMinWidth(0);
            jTable1.getColumnModel().getColumn(18).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(19).setMinWidth(0);
            jTable1.getColumnModel().getColumn(19).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(20).setMinWidth(0);
            jTable1.getColumnModel().getColumn(20).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(21).setMinWidth(0);
            jTable1.getColumnModel().getColumn(21).setMaxWidth(0);

            totalTable.getColumnModel().getColumn(0).setMinWidth(300);
            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(1).setMinWidth(300);
            totalTable.getColumnModel().getColumn(1).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(4).setMinWidth(300);
            totalTable.getColumnModel().getColumn(4).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(5).setMinWidth(300);
            totalTable.getColumnModel().getColumn(5).setMaxWidth(300);

        } else {
            jTable1.getColumnModel().getColumn(16).setMinWidth(0);
            jTable1.getColumnModel().getColumn(16).setMaxWidth(0);
            jTable1.getColumnModel().getColumn(17).setMinWidth(0);
            jTable1.getColumnModel().getColumn(17).setMaxWidth(0);

            totalTable.getColumnModel().getColumn(0).setMinWidth(300);
            totalTable.getColumnModel().getColumn(0).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(2).setMinWidth(300);
            totalTable.getColumnModel().getColumn(2).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(3).setMinWidth(300);
            totalTable.getColumnModel().getColumn(3).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(4).setMinWidth(300);
            totalTable.getColumnModel().getColumn(4).setMaxWidth(300);
            totalTable.getColumnModel().getColumn(5).setMinWidth(300);
            totalTable.getColumnModel().getColumn(5).setMaxWidth(300);

        }
    }

    private void txtDistrictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistrictActionPerformed
        txtState.requestFocus();
    }//GEN-LAST:event_txtDistrictActionPerformed

    private void comboProductItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboProductItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_comboProductItemStateChanged

    private void comboProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboProductFocusGained
        comboProduct.setPopupVisible(true);
    }//GEN-LAST:event_comboProductFocusGained

    private void comboProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductActionPerformed
//        getProductDetails();
    }//GEN-LAST:event_comboProductActionPerformed

    private void labelProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_labelProductKeyReleased
        if (evt.getKeyCode() != java.awt.event.KeyEvent.VK_ENTER) {
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_labelProductKeyReleased

    private void btnAddAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_btnAddAncestorMoved

    }//GEN-LAST:event_btnAddAncestorMoved

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if (txtCompanyName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please Add Company Name", "Alert", JOptionPane.WARNING_MESSAGE);
            jTabbedPane1.setSelectedIndex(0);
            comboParty.requestFocus();
        } else {
            addProductList();
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    public void TaxvalueCalculation() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        if (((!strUQC1rate.equals("") || !strUQC2rate.equals(""))) && ((!strUQC1qty.equals("") || !strUQC2qty.equals("")))) {
            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1qty.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2qty.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }

            float DiscountPer = 0, Discount = 0;
            float TotalAmount = (UQC1qty * UQC1rate) + (UQC2qty * UQC2rate);
            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            if (!strDiscountPer.equals("")) {
                DiscountPer = Float.parseFloat(strDiscountPer);
                Discount = (TotalAmount * DiscountPer) / 100;
                TotalAmount = TotalAmount - Discount;
                txtDiscount.setText(currency.format(Discount));
            } else {
                txtDiscount.setText("");
            }
            float TaxValue = TotalAmount * (IGST / 100);
            txtAmount.setText(currency.format(TotalAmount));
            if (isIGST) {
                txtIGSTamt.setText(currency.format(TaxValue));
                txtCGSTamt.setText("0.00");
                txtSGSTamt.setText("0.00");
            } else {
                txtIGSTamt.setText("0.00");
                txtCGSTamt.setText(currency.format(TaxValue / 2));
                txtSGSTamt.setText(currency.format(TaxValue / 2));
            }
        } else {
            txtAmount.setText("0.00");

            txtIGSTamt.setText("0.00");
            txtCGSTamt.setText("0.00");
            txtSGSTamt.setText("0.00");

            txtIGSTamt.setText("0.00");
            txtCGSTamt.setText("0.00");
            txtSGSTamt.setText("0.00");

        }
    }
    private void btnAddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtCompanyName.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Please Add Party Name", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                addProductList();
            }
        }
    }//GEN-LAST:event_btnAddKeyPressed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (i >= 0) {
            int p = JOptionPane.showConfirmDialog(null, "SURE YOU WANT TO DELETE ?", "CONFORM", JOptionPane.YES_NO_OPTION);
            if (p == JOptionPane.YES_OPTION) {
                model.removeRow(i);
                for (int y = i; y < model.getRowCount(); y++) {
                    model.setValueAt(y + 1, y, 0); //setValueAt(data,row,column)
                }
                clear();
                TotalCalculate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select Any Row.", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed
    public void clear() {
        txtUQC1Qty.setText("");
        txtUQC2Qty.setText("");
        txtUQC1Rate.setText("");
        txtUQC2Rate.setText("");
        txtUQC1BaseRate.setText("");
        txtUQC2BaseRate.setText("");
        txtIGSTper.setText("");
        txtCGSTper.setText("");
        txtSGSTper.setText("");
        txtIGSTamt.setText("");
        txtCGSTamt.setText("");
        txtSGSTamt.setText("");
        txtAmount.setText("");
        txtDiscountPer.setText("");
        txtDiscount.setText("");
        labelUQC1.setText("UQC1");
        labelUQC2.setText("UQC2");
        labelUQC1forPrice.setText("UQC1");
        labelUQC2forPrice.setText("UQC2");
        comboProduct.setSelectedIndex(0);
        jTable1.clearSelection();
        btnAdd.setEnabled(true);
    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        SaveAndPrint(false);
    }//GEN-LAST:event_btnSaveActionPerformed
    public void SaveAndPrint(Boolean isPrint) {
        String BillNo = txtBillNo.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isInBetweenFinancialYear = new CheckFinancialYear().toCheck(companyPolicy, dateBill.getDate(), false);
        if (BillNo.equals("")) {
            JOptionPane.showMessageDialog(null, "Please Enter the Bill Number", "Alert", JOptionPane.WARNING_MESSAGE);
            txtBillNo.requestFocus();
        } else if (!isInBetweenFinancialYear) {
            String msg = "<html>Please select a Date from <b>" + strFinancialYearStart + "</b>"
                    + " to <b>" + strFinancialYearEnd + "</b></html>";
            JLabel label = new JLabel(msg);
            JOptionPane.showMessageDialog(null, label, "Alert", JOptionPane.WARNING_MESSAGE);
        } else {
            if (isOldBill) {
                OldPrintAndSaveAction(isPrint);
            } else {
                NewPrintAndSaveAction(isPrint);
            }
        }
    }

    public void OldPrintAndSaveAction(Boolean isPrint) {

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {

//            float Discount = 0, Discountper = 0;
//            String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strDiscount.equals("")) {
//                Discount = Float.parseFloat(strDiscount);
//            }
//            if (!strDiscount.equals("")) {
//                Discountper = Float.parseFloat(strDiscountPer);
//            }
            float totalAmount = (float) totalTable.getModel().getValueAt(0, 0),
                    totalIGST = (float) totalTable.getModel().getValueAt(0, 1),
                    totalCGST = (float) totalTable.getModel().getValueAt(0, 2),
                    totalSGST = (float) totalTable.getModel().getValueAt(0, 3),
                    totalProfit = (float) totalTable.getModel().getValueAt(0, 4);
            float NetAmount = totalAmount + totalIGST + totalCGST + totalSGST;
            String value = "0.00";
            value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
            if (value.equals("")) {
                value = "0.00";
            }
            float roundOff = Float.parseFloat(value);
            NetAmount = NetAmount + roundOff;

            PurchaseSale newPurchaseSale = new PurchaseSale();

            newPurchaseSale.setCompany(SessionDataUtil.getSelectedCompany());
            newPurchaseSale.setPsType("SR");
            newPurchaseSale.setPsId(oldSale.getPsId());
            newPurchaseSale.setBillNo(txtBillNo.getText());
            newPurchaseSale.setPsDate(dateBill.getDate());
            newPurchaseSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
            newPurchaseSale.setIsIGST(isIGST);
            newPurchaseSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            newPurchaseSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            newPurchaseSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            newPurchaseSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
            newPurchaseSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
            newPurchaseSale.setCancelled(false);
            newPurchaseSale.setThrough(txtThrough.getText());
            newPurchaseSale.setLedger(ledgerData);
            newPurchaseSale.setPaymentType("Cash");//Payment Type
            newPurchaseSale.setRemarks("Goods Delivery");
            newPurchaseSale.setModeOfDelivery("");
            newPurchaseSale.setReferenceNumber(HSNCode);
            newPurchaseSale.setTotalAmountPaid(new BigDecimal(0));
            newPurchaseSale.setVariationAmount(convertDecimal.Currency("0"));
            newPurchaseSale.setReferenceNumber(txtReferenceNumber.getText());
            newPurchaseSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));

//          newPurchaseSale.setPsLineItems(psLineItems);
//          newPurchaseSale.setPsTaxSummaries(psTaxSummaries);
            List<PurchaseSaleLineItem> psLineItems = new ArrayList<PurchaseSaleLineItem>();
            List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<PurchaseSaleTaxSummary>();
            Long count = 1L;

            for (int i = 0; i < rowCount; i++) {
                PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(product);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
                psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));

                psLineItems.add(psLineItem);

                Boolean isNewSalesTax = true;
                for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                    if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                        float amount = tax.getTaxableValue().floatValue();
                        amount = (float) model.getValueAt(i, 15) + amount;
                        float Igst = tax.getIgstValue().floatValue();
                        Igst = (float) model.getValueAt(i, 17) + Igst;
                        float Cgst = tax.getCgstValue().floatValue();
                        Cgst = (float) model.getValueAt(i, 19) + Cgst;
                        float Sgst = tax.getSgstValue().floatValue();
                        Sgst = (float) model.getValueAt(i, 21) + Sgst;
                        float taxAmount = Igst + Cgst + Sgst;

                        tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                        tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                        tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                        tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                        tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));

                        isNewSalesTax = false;
                    }
                }
                if (isNewSalesTax) {
                    PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                    float amount = (float) model.getValueAt(i, 15);
                    float Igst = (float) model.getValueAt(i, 17);
                    float Cgst = (float) model.getValueAt(i, 19);
                    float Sgst = (float) model.getValueAt(i, 21);
                    float taxAmount = Igst + Cgst + Sgst;

//                    newSaleTaxSummary.setPsTaxId(count);
//                    newSaleTaxSummary.setPurchaseSale(newPurchaseSale);
                    newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                    newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 17));
                    newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 19));
                    newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 21));
                    newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                    purchaseSalesTax.add(newSaleTaxSummary);

                    count++;
                }
            }

            if (isPrint) {
                try {
                    List<String> nameOfPDFCopies = new ArrayList<String>();
                    new TaxInvoiceNew().printPdf(isPrint, false, newPurchaseSale, psLineItems, purchaseSalesTax, nameOfPDFCopies);
                } catch (IOException ex) {
                    Logger.getLogger(SalesReturnUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Boolean isSuccess = new PurchaseSaleLogic().updateAPurchaseOrSale("SR", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);

            if (isSuccess) {
                JOptionPane.showMessageDialog(null, "Bill is Successfully updated", "Message", JOptionPane.INFORMATION_MESSAGE);
                new SalesDateWiseReportUI().setVisible(true);
                dispose();
                clear();
            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void NewPrintAndSaveAction(Boolean isPrint) {

        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
        if (rowCount > 0) {

//            float Discount = 0, Discountper = 0;
//            String strDiscount = txtDiscount.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            String strDiscountPer = txtDiscountPer.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            if (!strDiscount.equals("")) {
//                Discount = Float.parseFloat(strDiscount);
//            }
//            if (!strDiscount.equals("")) {
//                Discountper = Float.parseFloat(strDiscountPer);
//            }
            float totalAmount = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 0))),
                    totalIGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 1))),
                    totalCGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 2))),
                    totalSGST = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 3))),
                    totalProfit = Float.parseFloat(currency.format((float) totalTable.getModel().getValueAt(0, 4)));
            float NetAmount = totalAmount + totalIGST + totalCGST + totalSGST;
//            NetAmount = NetAmount - Discount;
            String value = "0.00";
            value = JOptionPane.showInputDialog(null, "Round Off " + currency.format(NetAmount));
            if (value.equals("")) {
                value = "0.00";
            }
            float roundOff = Float.parseFloat(value);
            NetAmount = NetAmount + roundOff;

            PurchaseSale newPurchaseSale = new PurchaseSale();

            newPurchaseSale.setCompany(SessionDataUtil.getSelectedCompany());
            newPurchaseSale.setPsType("SR");
//              newPurchaseSale.setPsId(value);
            newPurchaseSale.setBillNo(txtBillNo.getText());
            newPurchaseSale.setPsDate(dateBill.getDate());
            newPurchaseSale.setBillAmount(convertDecimal.Currency(currency.format(totalAmount)));
            newPurchaseSale.setIsIGST(isIGST);
            newPurchaseSale.setIgstValue(convertDecimal.Currency(currency.format(totalIGST)));
            newPurchaseSale.setCgstValue(convertDecimal.Currency(currency.format(totalCGST)));
            newPurchaseSale.setSgstValue(convertDecimal.Currency(currency.format(totalSGST)));
            newPurchaseSale.setRoundOffValue(convertDecimal.Currency(currency.format(roundOff)));
            newPurchaseSale.setCalculatedTotalValue(convertDecimal.Currency(currency.format(NetAmount)));
            newPurchaseSale.setCancelled(false);
            newPurchaseSale.setThrough(txtThrough.getText());
            newPurchaseSale.setLedger(ledgerData);
            newPurchaseSale.setPaymentType("Cash");//Payment Type
            newPurchaseSale.setRemarks("Goods Delivery");
            newPurchaseSale.setModeOfDelivery("");
            newPurchaseSale.setReferenceNumber(HSNCode);
            newPurchaseSale.setTotalAmountPaid(new BigDecimal(0));
            newPurchaseSale.setVariationAmount(convertDecimal.Currency("0"));
            newPurchaseSale.setReferenceNumber(txtReferenceNumber.getText());
            newPurchaseSale.setProfit(convertDecimal.Currency(currency.format(totalProfit)));
//              newPurchaseSale.setPsLineItems(psLineItems);
//              newPurchaseSale.setPsTaxSummaries(psTaxSummaries);

            List<PurchaseSaleLineItem> psLineItems = new ArrayList<PurchaseSaleLineItem>();
            List<PurchaseSaleTaxSummary> purchaseSalesTax = new ArrayList<PurchaseSaleTaxSummary>();
            Long count = 1L;
            for (int i = 0; i < rowCount; i++) {
                PurchaseSaleLineItem psLineItem = new PurchaseSaleLineItem();
                psLineItem.setLineNumber((int) model.getValueAt(i, 0));
                psLineItem.setProductName((String) model.getValueAt(i, 1));
                psLineItem.setHsnSac((String) model.getValueAt(i, 2));
                product = new GetProduct().getProductByName((String) model.getValueAt(i, 1));

                psLineItem.setProduct(product);

                psLineItem.setUqcOne((String) model.getValueAt(i, 3));
                psLineItem.setUqcOneQuantity((int) model.getValueAt(i, 4));
                psLineItem.setUqcOneRate(convertDecimal.Currency(model.getValueAt(i, 5).toString()));
                psLineItem.setUqcOneValue((float) model.getValueAt(i, 6));
                psLineItem.setUqcTwo((String) model.getValueAt(i, 7));
                psLineItem.setUqcTwoQuantity((int) model.getValueAt(i, 8));
                psLineItem.setUqcTwoRate(convertDecimal.Currency(model.getValueAt(i, 9).toString()));
                psLineItem.setUqcTwoValue((float) model.getValueAt(i, 10));

                psLineItem.setStockQuantity((int) model.getValueAt(i, 12));
                psLineItem.setDiscountPercent((float) model.getValueAt(i, 13));
                psLineItem.setDiscountValue(convertDecimal.Currency(model.getValueAt(i, 14).toString()));
                psLineItem.setValue(convertDecimal.Currency(model.getValueAt(i, 15).toString()));
                psLineItem.setIgstPercentage((float) model.getValueAt(i, 16));
                psLineItem.setIgstValue(convertDecimal.Currency(model.getValueAt(i, 17).toString()));
                psLineItem.setCgstPercentage((float) model.getValueAt(i, 18));
                psLineItem.setCgstValue(convertDecimal.Currency(model.getValueAt(i, 19).toString()));
                psLineItem.setSgstPercentage((float) model.getValueAt(i, 20));
                psLineItem.setSgstValue(convertDecimal.Currency(model.getValueAt(i, 21).toString()));
                psLineItem.setUqc1BaseRate(convertDecimal.Currency(model.getValueAt(i, 22).toString()));
                psLineItem.setUqc2BaseRate(convertDecimal.Currency(model.getValueAt(i, 23).toString()));
                psLineItem.setProfit(convertDecimal.Currency(model.getValueAt(i, 24).toString()));

                psLineItems.add(psLineItem);

                Boolean isNewSalesTax = true;
                for (PurchaseSaleTaxSummary tax : purchaseSalesTax) {
                    if (tax.getHsnSac().equals((String) model.getValueAt(i, 2))) {
                        float amount = tax.getTaxableValue().floatValue();
                        amount = (float) model.getValueAt(i, 15) + amount;
                        float Igst = tax.getIgstValue().floatValue();
                        Igst = (float) model.getValueAt(i, 17) + Igst;
                        float Cgst = tax.getCgstValue().floatValue();
                        Cgst = (float) model.getValueAt(i, 19) + Cgst;
                        float Sgst = tax.getSgstValue().floatValue();
                        Sgst = (float) model.getValueAt(i, 21) + Sgst;
                        float taxAmount = Igst + Cgst + Sgst;

                        tax.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                        tax.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                        tax.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                        tax.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                        tax.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));

                        isNewSalesTax = false;
                    }
                }
                if (isNewSalesTax) {
                    PurchaseSaleTaxSummary newSaleTaxSummary = new PurchaseSaleTaxSummary();

                    float amount = (float) model.getValueAt(i, 15);
                    float Igst = (float) model.getValueAt(i, 17);
                    float Cgst = (float) model.getValueAt(i, 19);
                    float Sgst = (float) model.getValueAt(i, 21);
                    float taxAmount = Igst + Cgst + Sgst;

//                    newSaleTaxSummary.setPsTaxId(count);
//                    newSaleTaxSummary.setPurchaseSale(newPurchaseSale);
                    newSaleTaxSummary.setHsnSac((String) model.getValueAt(i, 2));
                    newSaleTaxSummary.setTaxableValue(convertDecimal.Currency(currency.format(amount)));
                    newSaleTaxSummary.setIgstPercentage((float) model.getValueAt(i, 17));
                    newSaleTaxSummary.setIgstValue(convertDecimal.Currency(currency.format(Igst)));
                    newSaleTaxSummary.setCgstPercentage((float) model.getValueAt(i, 19));
                    newSaleTaxSummary.setCgstValue(convertDecimal.Currency(currency.format(Cgst)));
                    newSaleTaxSummary.setSgstPercentage((float) model.getValueAt(i, 21));
                    newSaleTaxSummary.setSgstValue(convertDecimal.Currency(currency.format(Sgst)));
                    newSaleTaxSummary.setTotalTaxValue(convertDecimal.Currency(currency.format(taxAmount)));
                    purchaseSalesTax.add(newSaleTaxSummary);

                    count++;
                }
            }

            EventStatus result = new PurchaseSaleLogic().createAPS("SR", LedgerId, newPurchaseSale, psLineItems, purchaseSalesTax);
            if (result.isCreateDone()) {
                if (isPrint) {
                    List<String> nameOfPDFCopies = new ArrayList<String>();
                    new TaxInvoiceNew().pdfByBillId(true, false, result.getBillID(), nameOfPDFCopies,false);
                }
                JOptionPane.showMessageDialog(null, "New Bill is Successfully Created", "Message", JOptionPane.INFORMATION_MESSAGE);
                clearAll();
            } else {
                JOptionPane.showMessageDialog(null, "Error Occured", "Alert", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Add aleast one Product", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void clearAll() {
        comboParty.setSelectedItem("Select the Party");
        txtCompanyName.setText("");
        txtAddr1.setText("");
        txtAddr2.setText("");
        txtAddr3.setText("");
        txtCity.setText("");
        txtDistrict.setText("");
        txtState.setText("");
        txtEmail.setText("");
        txtMobile.setText("");
        txtPhone.setText("");
        txtGSTIN.setText("");
        txtThrough.setText("");
        txtReferenceNumber.setText("");
        txtBillNo.setText("");

        clear();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if (model.getRowCount() > 0) {
            model.setRowCount(0);
        }
        DefaultTableModel model2 = (DefaultTableModel) totalTable.getModel();
        if (model2.getRowCount() > 0) {
            model2.removeRow(0);
        }
    }

    public void addProductList() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1Baserate = txtUQC1BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2Baserate = txtUQC2BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        String product = (String) comboProduct.getSelectedItem();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Boolean isProductAlreadyHere = false;
        for (int i = 0; i < model.getRowCount(); i++) {
            if (model.getValueAt(i, 1).toString().equals(product)) {
                isProductAlreadyHere = true;
            }
        }

        if (comboProduct.getSelectedItem().equals("Select the Product")) {
            JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
        } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Qty.requestFocus();
        } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
            JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
            txtUQC1Rate.requestFocus();
        } else if (isProductAlreadyHere) {
            JOptionPane.showMessageDialog(null, product + " is Already in the list", "Alert", JOptionPane.WARNING_MESSAGE);
            comboProduct.requestFocus();
        } else {
            int count = jTable1.getRowCount() + 1;
            int wholeQuantity = 0;

            int UQC1qty = 0, UQC2qty = 0;
            float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
            if (!strUQC1qty.equals("")) {
                UQC1qty = Integer.parseInt(strUQC1qty);
            }
            if (!strUQC2qty.equals("")) {
                UQC2qty = Integer.parseInt(strUQC2qty);
            }
            if (!strUQC1rate.equals("")) {
                UQC1rate = Float.parseFloat(strUQC1rate);
            }
            if (!strUQC2rate.equals("")) {
                UQC2rate = Float.parseFloat(strUQC2rate);
            }
            if (!strUQC1Baserate.equals("")) {
                UQC1Baserate = Float.parseFloat(strUQC1Baserate);
            }
            if (!strUQC2Baserate.equals("")) {
                UQC2Baserate = Float.parseFloat(strUQC2Baserate);
            }

            wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
            float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                    IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                    CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                    SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                    Amount = Float.parseFloat(txtAmount.getText());
            float amountWithTax = IGST_amt + CGST_amt + SGST_amt + Amount;
            String Discountper = "0.00", Discount = "0.00";
            if (!txtDiscountPer.getText().equals("")) {
                Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
            }
            if (!txtDiscount.getText().equals("")) {
                Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
            }
            int UQC1 = 0, UQC2 = 0;

            float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
            Profit = Profit - Float.parseFloat(Discount);
            Object[] row = {count, product, HSNCode, labelUQC1.getText(), UQC1qty, UQC1rate, UQC1qty * UQC1rate,
                labelUQC2.getText(), UQC2qty, UQC2rate, UQC2qty * UQC2rate, UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(),
                wholeQuantity, Float.parseFloat(Discountper), Float.parseFloat(Discount), Amount, IGST, IGST_amt, CGST, CGST_amt, SGST, SGST_amt,
                UQC1Baserate, UQC2Baserate, Profit, amountWithTax};
            model.addRow(row);
            clear();
            TotalCalculate();
            jTableRender();
            comboProduct.requestFocus();

        }
    }

    public void updateProductList() {
        String strUQC1qty = txtUQC1Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2qty = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2rate = txtUQC2Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC1Baserate = txtUQC1BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String strUQC2Baserate = txtUQC2BaseRate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");

        int i = jTable1.getSelectedRow();
        if (i >= 0) {
            if (comboProduct.getSelectedItem().equals("Select the Product")) {
                JOptionPane.showMessageDialog(null, "Please Select the Product", "Alert", JOptionPane.WARNING_MESSAGE);
            } else if ((strUQC1qty.equals("") && strUQC2qty.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add UQC Quantity", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Qty.requestFocus();
            } else if ((strUQC1rate.equals("") && strUQC2rate.equals(""))) {
                JOptionPane.showMessageDialog(null, "Please Add Price", "Alert", JOptionPane.WARNING_MESSAGE);
                txtUQC1Rate.requestFocus();
            } else {
                int count = jTable1.getRowCount() + 1;
                int wholeQuantity = 0;

                int UQC1qty = 0, UQC2qty = 0;
                float UQC1rate = 0, UQC2rate = 0, UQC1Baserate = 0, UQC2Baserate = 0;
                if (!strUQC1qty.equals("")) {
                    UQC1qty = Integer.parseInt(strUQC1qty);
                }
                if (!strUQC2qty.equals("")) {
                    UQC2qty = Integer.parseInt(strUQC2qty);
                }
                if (!strUQC1rate.equals("")) {
                    UQC1rate = Float.parseFloat(strUQC1rate);
                }
                if (!strUQC2rate.equals("")) {
                    UQC2rate = Float.parseFloat(strUQC2rate);
                }
                if (!strUQC1Baserate.equals("")) {
                    UQC1Baserate = Float.parseFloat(strUQC1Baserate);
                }
                if (!strUQC2Baserate.equals("")) {
                    UQC2Baserate = Float.parseFloat(strUQC2Baserate);
                }
                wholeQuantity = (UQC1qty * UQC2Value) + UQC2qty;
                float Rate = Float.parseFloat(txtUQC1Rate.getText()),
                        IGST_amt = Float.parseFloat(txtIGSTamt.getText()),
                        CGST_amt = Float.parseFloat(txtCGSTamt.getText()),
                        SGST_amt = Float.parseFloat(txtSGSTamt.getText()),
                        Amount = Float.parseFloat(txtAmount.getText());
                float amountWithTax = IGST_amt + CGST_amt + SGST_amt + Amount;
                String Discountper = "0.00", Discount = "0.00";
                if (!txtDiscountPer.getText().equals("")) {
                    Discountper = currency.format(Float.parseFloat(txtDiscountPer.getText()));
                }
                if (!txtDiscount.getText().equals("")) {
                    Discount = currency.format(Float.parseFloat(txtDiscount.getText()));
                }
                String product = (String) comboProduct.getSelectedItem();
                float Profit = ((UQC1qty * UQC1rate) + (UQC2qty * UQC2rate)) - ((UQC1qty * UQC1Baserate) + (UQC2qty * UQC2Baserate));
                Profit = Profit - Float.parseFloat(Discount);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                model.setValueAt(product, i, 1);
                model.setValueAt(HSNCode, i, 2);
                model.setValueAt(labelUQC1.getText(), i, 3);
                model.setValueAt(UQC1qty, i, 4);
                model.setValueAt(UQC1rate, i, 5);
                model.setValueAt(UQC1qty * UQC1rate, i, 6);
                model.setValueAt(labelUQC2.getText(), i, 7);
                model.setValueAt(UQC2qty, i, 8);
                model.setValueAt(UQC2rate, i, 9);
                model.setValueAt(UQC2qty * UQC2rate, i, 10);
                model.setValueAt(UQC1qty + " " + labelUQC1.getText() + "," + UQC2qty + " " + labelUQC2.getText(), i, 11);
                model.setValueAt(wholeQuantity, i, 12);
                model.setValueAt(Float.parseFloat(Discountper), i, 13);
                model.setValueAt(Float.parseFloat(Discount), i, 14);
                model.setValueAt(Amount, i, 15);
                model.setValueAt(IGST, i, 16);
                model.setValueAt(IGST_amt, i, 17);
                model.setValueAt(CGST, i, 18);
                model.setValueAt(CGST_amt, i, 19);
                model.setValueAt(SGST, i, 20);
                model.setValueAt(SGST_amt, i, 21);
                model.setValueAt(UQC1Baserate, i, 22);
                model.setValueAt(UQC2Baserate, i, 23);
                model.setValueAt(Profit, i, 24);
                model.setValueAt(amountWithTax, i, 25);
                clear();
                TotalCalculate();
                jTableRender();
                comboProduct.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select Any Row", "Alert", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateProductList();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        SaveAndPrint(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtUQC1RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1RateActionPerformed
        float value = Float.parseFloat(txtUQC1Rate.getText());
        txtUQC1Rate.setText(currency.format(value));
        txtUQC2Rate.requestFocus();
    }//GEN-LAST:event_txtUQC1RateActionPerformed

    private void txtUQC1RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyReleased

        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC1RateKeyReleased

    private void txtDiscountPerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountPerActionPerformed
        txtDiscount.requestFocus();

    }//GEN-LAST:event_txtDiscountPerActionPerformed

    private void txtDiscountPerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyPressed

    }//GEN-LAST:event_txtDiscountPerKeyPressed

    private void txtDiscountPerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtDiscountPerKeyReleased

    private void txtDiscountPerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountPerKeyTyped

    }//GEN-LAST:event_txtDiscountPerKeyTyped

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        int i = jTable1.getSelectedRow();
        if (i == -1) {
            btnAdd.requestFocus();
        } else {
            btnUpdate.requestFocus();
        }
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
        String quantity = txtUQC2Qty.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        String rate = txtUQC1Rate.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        if ((!rate.equals("")) && (!quantity.equals(""))) {
            int Qty = Integer.parseInt(quantity);
            float Rate = Float.parseFloat(rate);
            float DiscountPer = 0, Discount = 0;
            float TotalAmount = Rate * Qty;
            Float DiscountAmount = Float.parseFloat(txtDiscount.getText());
            String strDiscountPer = currency.format((DiscountAmount / TotalAmount) * 100);
            txtDiscountPer.setText(strDiscountPer);
            TotalAmount = TotalAmount - DiscountAmount;
            float TaxValue = TotalAmount * (IGST / 100);
            txtAmount.setText(currency.format(TotalAmount));
            if (isIGST) {
                txtIGSTamt.setText(currency.format(TaxValue));
                txtCGSTamt.setText("0.00");
                txtSGSTamt.setText("0.00");
            } else {
                txtIGSTamt.setText("0.00");
                txtCGSTamt.setText(currency.format(TaxValue / 2));
                txtSGSTamt.setText(currency.format(TaxValue / 2));
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please fill UQC and Price Value", "Alert", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_txtDiscountKeyReleased

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int i = jTable1.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        comboProduct.setSelectedItem(model.getValueAt(i, 1));
        HSNCode = (String) model.getValueAt(i, 2);
        labelUQC1.setText((String) model.getValueAt(i, 3));
        txtUQC1Qty.setText(model.getValueAt(i, 4).toString());
        txtUQC1Rate.setText(currency.format(model.getValueAt(i, 5)));
        labelUQC2.setText((String) model.getValueAt(i, 7));
        txtUQC2Qty.setText(model.getValueAt(i, 8).toString());
        txtUQC2Rate.setText(currency.format(model.getValueAt(i, 9)));
        txtDiscountPer.setText(model.getValueAt(i, 13).toString());
        txtDiscount.setText(currency.format(model.getValueAt(i, 14)));
        txtAmount.setText(currency.format(model.getValueAt(i, 15)));
        txtIGSTper.setText(model.getValueAt(i, 16).toString());
        txtIGSTamt.setText(currency.format(model.getValueAt(i, 17)));
        txtCGSTper.setText(model.getValueAt(i, 18).toString());
        txtCGSTamt.setText(currency.format(model.getValueAt(i, 19)));
        txtSGSTper.setText(model.getValueAt(i, 20).toString());
        txtSGSTamt.setText(currency.format(model.getValueAt(i, 21)));
        txtUQC1BaseRate.setText(currency.format(model.getValueAt(i, 22)));
        txtUQC2BaseRate.setText(currency.format(model.getValueAt(i, 23)));
        btnAdd.setEnabled(false);
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtUQC2QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC2QtyKeyReleased

    private void txtUQC1QtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyPressed
        if (evt.isAltDown() && evt.getKeyCode() == KeyEvent.VK_B) {
            txtBillNo.requestFocus();
        }
    }//GEN-LAST:event_txtUQC1QtyKeyPressed

    private void txtUQC1QtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC1QtyKeyReleased

    private void txtUQC2RateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyReleased
        TaxvalueCalculation();
    }//GEN-LAST:event_txtUQC2RateKeyReleased

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        new LogInUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void txtUQC1RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC1Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC1RateKeyTyped

    private void txtUQC2RateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2RateKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtUQC2RateKeyTyped

    private void txtUQC1QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC1QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC1QtyKeyTyped

    private void txtUQC2QtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUQC2QtyKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
            evt.consume();

        }
    }//GEN-LAST:event_txtUQC2QtyKeyTyped

    private void txtThroughActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtThroughActionPerformed
        txtReferenceNumber.requestFocus();
    }//GEN-LAST:event_txtThroughActionPerformed

    private void comboProductAncestorRemoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_comboProductAncestorRemoved
        // TODO add your handling code here:
    }//GEN-LAST:event_comboProductAncestorRemoved

    private void txtUQC1QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1QtyActionPerformed
        txtUQC2Qty.requestFocus();
    }//GEN-LAST:event_txtUQC1QtyActionPerformed

    private void txtUQC2QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2QtyActionPerformed
        if (btnAdd.isEnabled()) {
            btnAdd.requestFocus();
        } else {
            txtDiscountPer.requestFocus();
        }
    }//GEN-LAST:event_txtUQC2QtyActionPerformed

    private void txtUQC1BaseRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC1BaseRateActionPerformed
        txtUQC2BaseRate.requestFocus();
    }//GEN-LAST:event_txtUQC1BaseRateActionPerformed

    private void txtUQC2BaseRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2BaseRateActionPerformed
        txtUQC1Rate.requestFocus();
    }//GEN-LAST:event_txtUQC2BaseRateActionPerformed

    private void txtUQC2RateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUQC2RateActionPerformed
        txtDiscountPer.requestFocus();
    }//GEN-LAST:event_txtUQC2RateActionPerformed

    private void comboPartyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboPartyFocusGained
        comboParty.setPopupVisible(true);
    }//GEN-LAST:event_comboPartyFocusGained

    private void comboPartyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboPartyKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            txtThrough.requestFocus();
        }
    }//GEN-LAST:event_comboPartyKeyPressed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (jTabbedPane1.getSelectedIndex() == 0) {
            comboParty.setSelectedItem("");
            comboParty.requestFocus();
        } else {
//            clear();
            comboProduct.setSelectedItem("");
            comboProduct.requestFocus();
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void txtBillNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBillNoKeyPressed

        String BillNo = txtBillNo.getText().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        Boolean isPrint;
        if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_S) {
            if (BillNo.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Enter the Bill Number", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                isPrint = false;
                if (isOldBill) {
                    OldPrintAndSaveAction(isPrint);
                } else {
                    NewPrintAndSaveAction(isPrint);
                }
            }
        } else if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_P) {
            if (BillNo.equals("")) {
                JOptionPane.showMessageDialog(null, "Please Enter the Bill Number", "Alert", JOptionPane.WARNING_MESSAGE);
            } else {
                isPrint = true;
                if (isOldBill) {
                    OldPrintAndSaveAction(isPrint);
                } else {
                    NewPrintAndSaveAction(isPrint);
                }
            }
        }
    }//GEN-LAST:event_txtBillNoKeyPressed

    private void txtAmountPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAmountPaidKeyTyped
        char vChar = evt.getKeyChar();
        boolean dot = false;

        if (txtUQC2Rate.getText().equals("")) {
            dot = false;
        }
        if (dot == false) {
            if (vChar == '.') {
                dot = true;
            } else if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        } else {
            if (!(Character.isDigit(vChar)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_BACKSPACE)
                    || (vChar == com.sun.glass.events.KeyEvent.VK_DELETE))) {
                evt.consume();

            }
        }
    }//GEN-LAST:event_txtAmountPaidKeyTyped

    private void radioCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCashActionPerformed
        checkRadioButton();
    }//GEN-LAST:event_radioCashActionPerformed

    private void radioBankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioBankActionPerformed
        checkRadioButton();
    }//GEN-LAST:event_radioBankActionPerformed


    private void btnUpdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnUpdateKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateProductList();
        }
    }//GEN-LAST:event_btnUpdateKeyPressed

    private void menuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDashboardActionPerformed
        new DashBoardUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDashboardActionPerformed

    private void menuChangeCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChangeCompanyActionPerformed
        new SetCompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuChangeCompanyActionPerformed

    private void menuPartyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPartyActionPerformed
        new PartyDetailsUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPartyActionPerformed

    private void menuProductGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductGroupActionPerformed
        new ProductGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuProductGroupActionPerformed

    private void menuProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuProductActionPerformed

        //        if (SessionDataUtil.getGst1ProPolicy().getPackageName().equalsIgnoreCase("COPPER")) {
        //            new ProductUI().setVisible(true);
        //            dispose();
        //        } else {
        new ProductUI().setVisible(true);
        dispose();
        //        }
    }//GEN-LAST:event_menuProductActionPerformed

    private void menuServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiceActionPerformed
        new ServiceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuServiceActionPerformed

    private void menuPricingPolicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPricingPolicyActionPerformed
        new PricingPolicyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPricingPolicyActionPerformed

    private void menuSaleB2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2BActionPerformed
        new SalesB2BUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2BActionPerformed

    private void menuSaleB2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaleB2CActionPerformed
        new SalesB2CUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSaleB2CActionPerformed

    private void menuEstimateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateActionPerformed
        EstimateUI estimateui = new EstimateUI();
        estimateui.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuEstimateActionPerformed

    private void menuCreditNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteActionPerformed
        new CreditNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteActionPerformed

    private void menuPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseActionPerformed
        new PurchaseUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseActionPerformed

    private void menuDebitNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteActionPerformed
        new DebitNoteUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteActionPerformed

    private void menuSalesReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesReportGroupActionPerformed
        new TaxInvoiceReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesReportGroupActionPerformed

    private void menuEstimateReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEstimateReportGroupActionPerformed
        new EstimateReportsGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuEstimateReportGroupActionPerformed

    private void menuCreditNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditNoteReportActionPerformed
        new CreditNoteDateWiseReportUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCreditNoteReportActionPerformed

    private void menuPurchaseReportGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportGroupActionPerformed
        new PurchaseReportGroupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportGroupActionPerformed

    private void menuDebitNoteReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDebitNoteReportActionPerformed
        new DebitNoteDateWiseReportUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDebitNoteReportActionPerformed

    private void menuAddStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddStockActionPerformed
        new AddStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuAddStockActionPerformed

    private void menuDeductionStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDeductionStockActionPerformed
        new DeductionStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDeductionStockActionPerformed

    private void menuCurrentStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCurrentStockActionPerformed
        //        new StockReportUI().setVisible(true);
        //        dispose();
        new CurrentStockReportwithGodownUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCurrentStockActionPerformed

    private void menuClosingStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClosingStockActionPerformed
        new ClosingStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuClosingStockActionPerformed

    private void menuLedgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerActionPerformed
        new LedgerUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerActionPerformed

    private void menuJournalEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJournalEntryActionPerformed
        new JournalEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuJournalEntryActionPerformed

    private void menuCashEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashEntryActionPerformed
        new CashEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCashEntryActionPerformed

    private void menuBankEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBankEntryActionPerformed
        new BankEntryUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBankEntryActionPerformed

    private void menuSalesPaymnetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalesPaymnetActionPerformed
        new SalesAndSalesReturnReportUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuSalesPaymnetActionPerformed

    private void menuPurchaseReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPurchaseReportActionPerformed
        new PurchaseAndPurchaseReturnReportUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuPurchaseReportActionPerformed

    private void menuDayBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDayBookActionPerformed
        new DayBookUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuDayBookActionPerformed

    private void menuLedgerGroupBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuLedgerGroupBalanceActionPerformed
        new LedgerGroupTrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuLedgerGroupBalanceActionPerformed

    private void menuTrialBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTrialBalanceActionPerformed
        new TrialBalanceUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTrialBalanceActionPerformed

    private void menuGSTR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGSTR1ActionPerformed
        new GSTR1UI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuGSTR1ActionPerformed

    private void menuTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuTaxActionPerformed
        new TaxUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuTaxActionPerformed

    private void menuUQCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUQCActionPerformed
        new UQCUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUQCActionPerformed

    private void menuCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompanyActionPerformed
        new CompanyUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuCompanyActionPerformed

    private void menuUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsersActionPerformed
        new UserUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuUsersActionPerformed

    private void menuBackUPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBackUPActionPerformed
        new BackupUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuBackUPActionPerformed

    private void menuOpeningStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOpeningStockActionPerformed
        new OpeningStockUI().setVisible(true);
        dispose();
    }//GEN-LAST:event_menuOpeningStockActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SalesReturnUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SalesReturnUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SalesReturnUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SalesReturnUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SalesReturnUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboBank;
    private javax.swing.JComboBox<String> comboParty;
    private javax.swing.JComboBox<String> comboProduct;
    private com.toedter.calendar.JDateChooser dateBill;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelAddress;
    private javax.swing.JLabel labelBasePrice;
    private javax.swing.JLabel labelCompanyName;
    private javax.swing.JLabel labelGSTIN;
    private javax.swing.JLabel labelPartyName;
    private javax.swing.JLabel labelProduct;
    private javax.swing.JLabel labelUQC1;
    private javax.swing.JLabel labelUQC1BasePrice;
    private javax.swing.JLabel labelUQC1forPrice;
    private javax.swing.JLabel labelUQC2;
    private javax.swing.JLabel labelUQC2BasePrice;
    private javax.swing.JLabel labelUQC2forPrice;
    private javax.swing.JLabel labelUserName;
    private javax.swing.JMenu menuAccounts;
    private javax.swing.JMenuItem menuAccountsReport;
    private javax.swing.JMenuItem menuAddStock;
    private javax.swing.JMenuItem menuBackUP;
    private javax.swing.JMenuItem menuBankBook;
    private javax.swing.JMenuItem menuBankEntry;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCashBook;
    private javax.swing.JMenuItem menuCashEntry;
    private javax.swing.JMenuItem menuChangeCompany;
    private javax.swing.JMenuItem menuClosingStock;
    private javax.swing.JMenuItem menuCompany;
    private javax.swing.JMenuItem menuCreditNote;
    private javax.swing.JMenuItem menuCreditNoteReport;
    private javax.swing.JMenuItem menuCurrentStock;
    private javax.swing.JMenuItem menuDashboard;
    private javax.swing.JMenuItem menuDayBook;
    private javax.swing.JMenuItem menuDebitNote;
    private javax.swing.JMenuItem menuDebitNoteReport;
    private javax.swing.JMenuItem menuDeductionStock;
    private javax.swing.JMenuItem menuEstimate;
    private javax.swing.JMenuItem menuEstimateReportGroup;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuGST;
    private javax.swing.JMenuItem menuGSTR1;
    private javax.swing.JMenu menuInventory;
    private javax.swing.JMenu menuInventoryReport;
    private javax.swing.JMenuItem menuJournalEntry;
    private javax.swing.JMenuItem menuLedger;
    private javax.swing.JMenuItem menuLedgerBook;
    private javax.swing.JMenuItem menuLedgerGroupBalance;
    private javax.swing.JMenu menuNewBill;
    private javax.swing.JMenuItem menuOpeningStock;
    private javax.swing.JMenuItem menuParty;
    private javax.swing.JMenuItem menuPricingPolicy;
    private javax.swing.JMenuItem menuProduct;
    private javax.swing.JMenuItem menuProductGroup;
    private javax.swing.JMenuItem menuPurchase;
    private javax.swing.JMenuItem menuPurchaseHeader;
    private javax.swing.JMenuItem menuPurchaseReport;
    private javax.swing.JMenuItem menuPurchaseReportGroup;
    private javax.swing.JMenuItem menuPurchaseReportHeader;
    private javax.swing.JMenu menuReports;
    private javax.swing.JMenuItem menuSaleB2B;
    private javax.swing.JMenuItem menuSaleB2C;
    private javax.swing.JMenuItem menuSalesHeader;
    private javax.swing.JMenuItem menuSalesPaymnet;
    private javax.swing.JMenuItem menuSalesReportGroup;
    private javax.swing.JMenuItem menuSalesReportHeader;
    private javax.swing.JMenuItem menuService;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JMenuItem menuTax;
    private javax.swing.JMenuItem menuTrialBalance;
    private javax.swing.JMenuItem menuUQC;
    private javax.swing.JMenuItem menuUsers;
    private javax.swing.JRadioButton radioBank;
    private javax.swing.JRadioButton radioCash;
    private javax.swing.JTable totalTable;
    private javax.swing.JTextField txtAddr1;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtAddr3;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JTextField txtAmountPaid;
    private javax.swing.JTextField txtBillNo;
    private javax.swing.JLabel txtCGSTamt;
    private javax.swing.JLabel txtCGSTper;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtCompanyName;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtDiscountPer;
    private javax.swing.JTextField txtDistrict;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtGSTIN;
    private javax.swing.JLabel txtIGSTamt;
    private javax.swing.JLabel txtIGSTper;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtReferenceNumber;
    private javax.swing.JLabel txtSGSTamt;
    private javax.swing.JLabel txtSGSTper;
    private javax.swing.JTextField txtState;
    private javax.swing.JTextField txtThrough;
    private javax.swing.JTextField txtUQC1BaseRate;
    private javax.swing.JTextField txtUQC1Qty;
    private javax.swing.JTextField txtUQC1Rate;
    private javax.swing.JTextField txtUQC2BaseRate;
    private javax.swing.JTextField txtUQC2Qty;
    private javax.swing.JTextField txtUQC2Rate;
    // End of variables declaration//GEN-END:variables
}
