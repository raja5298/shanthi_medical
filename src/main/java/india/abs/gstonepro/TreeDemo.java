/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro;

import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.ui.LedgerUI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author ceo
 */
public class TreeDemo {

    public static void main(String[] args) {

        List<LedgerGroup> ledgerGroups = new ArrayList<LedgerGroup>();

        LedgerGroup lg1 = new LedgerGroup();
        lg1.setLedgerGroupName("1");
        lg1.setLedgerGroupId(new Long(1));

        LedgerGroup lg2 = new LedgerGroup();
        lg2.setLedgerGroupName("11");
        lg2.setLedgerGroupId(new Long(11));
        lg2.setParentLedgerGroup(lg1);

        LedgerGroup lg3 = new LedgerGroup();
        lg3.setLedgerGroupName("2");
        lg3.setLedgerGroupId(new Long(2));

        LedgerGroup lg22 = new LedgerGroup();
        lg22.setLedgerGroupName("22");
        lg22.setLedgerGroupId(new Long(22));
        lg22.setParentLedgerGroup(lg3);

        ledgerGroups.add(lg1);
        ledgerGroups.add(lg2);
        ledgerGroups.add(lg3);
         ledgerGroups.add(lg22);

        printTreeStrucure(ledgerGroups);

    }

    private static void printTreeStrucure(List<LedgerGroup> ledgerGroups) {

        Map<LedgerGroup, HashSet<LedgerGroup>> treeLg = new HashMap<>();
        ledgerGroups.forEach((ledgerGroup) -> {

            ledgerGroups.forEach((ledgerGroup2) -> {
                Long firstId = ledgerGroup.getLedgerGroupId();
                Long secondId = ledgerGroup2.getLedgerGroupId();
                LedgerGroup parentLG = ledgerGroup2.getParentLedgerGroup();
                if (!(firstId == secondId) && (parentLG != null) && (ledgerGroup.getParentLedgerGroup() == null)) {
                    if (firstId == parentLG.getLedgerGroupId()) {
                        if (treeLg.containsKey(ledgerGroup)) {
                            HashSet<LedgerGroup> children = treeLg.get(ledgerGroup);
                            children.add(ledgerGroup2);
                            treeLg.put(ledgerGroup, children);
                        } else {
                            HashSet<LedgerGroup> children = new HashSet<>();
                            children.add(ledgerGroup2);
                            treeLg.put(ledgerGroup, children);
                        }

                    } else {
                        HashSet<LedgerGroup> children = new HashSet<>();
                        treeLg.put(ledgerGroup, children);
                    }
                }

            });

        });

        
        treeLg.forEach((parent, children) -> {

            
            children.forEach((child) -> {
                

            });
        });

    }

}
