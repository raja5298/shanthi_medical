/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.BillOfSupplyLogic;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class A7Bill {

    static int totalproductcount = 0, productcount, hsncount, hsncountNon;
    int count = 0;
    int productLimit = 20;
    static int sno = 0, hsnno = 0;
    int random = 0;

    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";
//    public String companyName = "", companyLine1 = "", companyLine2 = "", companyLine3 = "", companyLine4 = "",
//            companyDistrict = "", CompanyState = "", companyGSTIN = "";

    public String companyName = "", companyAddr1 = "", companyAddr2 = "",
            companyAddr3 = "", companyCity = "", companyDistrict = "", companyEmail = "",
            companyState = "", companyMobile = "", companyPhone = "", companyGSTIN = "";

    public String billNo = "", billDate = "", billCompanyName = "", billCompanyAddr1 = "", billCompanyAddr2 = "",
            billCompanyAddr3 = "", billCompanyCity = "", billCompanyDistrict = "",
            billCompanyState = "", billCompanyMobile = "", billCompanyPhone = "", billCompanyEmail = "", billCompanyGSTIN = "", billPayment = "",
            billBuyingOrderNo = "", billOrderDate = "", billDispatchThorugh = "", billDestination = "", billTermOfDelivery = "", billThrough = "",
            billReferenceNo = "";
//    public String ShippingName = "", ShippingAddr1 = "", ShippingAddr2 = "", ShippingAddr3 = "", ShippingCity = "", ShippingPincode = "", ShippingDistrict = "", ShippingState = "",
//            ShippingMobile = "", ShippingPhone = "", ShippingGSTIN = "";
    public boolean isIGST = false, billIsSameAddress = true, isBill = false, isPdfOnly = false;
    public int billId = 0;
    public float billIGST = 0, billTotalTax = 0, billFlightAmount = 0, billCGST = 0, billSGST = 0, billTotal = 0,
            billNetAmount = 0, billDiscountPer = 0, billDiscount = 0, billRoundOff = 0;
    DecimalFormat currency = new DecimalFormat("##,##,##0.000");

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<BillOfSupplyLineItem> bosLineItems = new ArrayList<>();
    List<BillOfSupplyChargeItem> bosChargeItems = new ArrayList<>();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
    BillOfSupply newBos = new BillOfSupply();

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font normalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 6, BaseColor.BLACK);

    com.itextpdf.text.Font footerboldfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

    public void pdfByBillId(Boolean isPrint, Boolean isPreview, String billId) {

        BillOfSupply bos = new BillOfSupplyLogic().readABOS(billId);
        List<BillOfSupplyLineItem> bosLis = bos.getBosLineItems();
        List<BillOfSupplyChargeItem> bosCis = bos.getBosChargeItems();

//        printPdf(isPrint, isPreview, bos, bosLis, bosCis);
    }

//public void printPdf(Boolean isPrint, Boolean isPreview, BillOfSupply bos, List<BillOfSupplyLineItem> bosLis, List<BillOfSupplyChargeItem> bosCis) {
//        sno = 0;
//        hsnno = 0;
//        newBos = bos;
//        bosLineItems = bosLis;
//        bosChargeItems = bosCis;
//
//        productcount = bosLineItems.size();
//        hsncount = bosChargeItems.size();
//        setCompany();
//        setBillDetails();
//       
//        String paperSize = SessionDataUtil.getCompanyPolicyData().getPrintPaperSize();
//        switch (paperSize) {
//            case "A4_P":
//                pdfGenerateA4(isPrint, isPreview);
//                break;
//            case "A5_P":
//                try {
//                    createPdfA5P(isPrint, isPreview);
//                } catch (DocumentException ex) {
//                    Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                break;
//            case "A5_L":
//                pdfGenerateA4(isPrint, isPreview);
//                break;
////            case "A7_P":
////                createPdfA5P(isPrint, isPreview);
//            default:
//                break;
//        }
//    }
    public void setBillDetails() {
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");

        billNo = newBos.getBosId();
        billDate = sdfr.format(newBos.getBosDate());

        billCompanyName = newBos.getLedger().getLedgerName();
        String Address = newBos.getLedger().getAddress();
        if (Address != null) {
            billCompanyAddr1 = Address.replaceAll("/~/", ",");
        }

//        billCompanyAddr2 = "";
//        billCompanyAddr3 = "";
        if (newBos.getLedger().getCity() != null) {
            billCompanyCity = newBos.getLedger().getCity();
        }
        if (newBos.getLedger().getDistrict() != null) {
            billCompanyDistrict = newBos.getLedger().getDistrict();
        }
        if (newBos.getLedger().getState() != null) {
            billCompanyState = newBos.getLedger().getState();
        }
        if (newBos.getLedger().getMobile() != null) {
            billCompanyMobile = newBos.getLedger().getMobile();
        }
        if (newBos.getLedger().getEmail() != null) {
            billCompanyMobile = newBos.getLedger().getEmail();
        }
        if (newBos.getLedger().getPhone() != null) {
            billCompanyPhone = newBos.getLedger().getPhone();
        }
        if (newBos.getLedger().getGSTIN() != null) {
            billCompanyGSTIN = newBos.getLedger().getGSTIN();
        }

        billNetAmount = newBos.getBillAmount().floatValue();
        billRoundOff = newBos.getRoundOffValue().floatValue();
        billReferenceNo = newBos.getReferenceNumber();
    }

    public void setCompany() {
        companyName = SessionDataUtil.getSelectedCompany().getCompanyName();

        String Address = SessionDataUtil.getSelectedCompany().getAddress();
        if (Address != null) {
            companyAddr1 = Address.replaceAll("/~/", ",");
        }
        if (SessionDataUtil.getSelectedCompany().getCity() != null) {
            companyCity = SessionDataUtil.getSelectedCompany().getCity();
        }
        if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
            companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
        }
        if (SessionDataUtil.getSelectedCompany().getState() != null) {
            companyState = SessionDataUtil.getSelectedCompany().getState();
        }
        if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
            companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
        }
        if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
            companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
        }
        if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
            companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
        }
        if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
            companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
        }

    }

    public String getUQCcode(String value) {
        String strUQCcode = "";
        for (UQC uqc : UQCs) {
            if (uqc.getQuantityName().equals(value)) {
                strUQCcode = uqc.getUQCCode();
                break;
            }
        }
        return strUQCcode;
    }

    public static void main(String[] args) throws IOException, DocumentException, SQLException {
        A7Bill bill = new A7Bill();
        bill.createPdf();
    }

    public void createPdf() throws FileNotFoundException, DocumentException {
        try {
            
            //String filename = "C:\\Gst\\temp\\" + billNo + ".pdf";
           
            OutputStream file = new FileOutputStream(new File("C:\\GST\\temp\\bill.pdf"));
            //Document document = new Document(PageSize.A7);
            Rectangle layout = new Rectangle(PageSize.A7);
            Document document = new Document(layout, 15, 15, 15, 15);
            PdfWriter.getInstance(document, file);
            document.open();
            Phrase p;

            com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

           
            PdfPTable taxTable = new PdfPTable(1);
            taxTable.setWidthPercentage(100);
            PdfPCell taxcell = null;

           

            //String name = "ARASAN BAKERY";
            taxcell = new PdfPCell(new Phrase(companyName, boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxTable.addCell(taxcell);
            ///document.add(taxTable);
           

            taxcell = new PdfPCell(new Phrase(companyAddr1, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxTable.addCell(taxcell);
            //document.add(taxTable);

            taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxTable.addCell(taxcell);
            //document.add(taxTable);
            taxcell = new PdfPCell(new Phrase("State : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxTable.addCell(taxcell);

            if (!companyPhone.equals("")) {
                taxcell = new PdfPCell(new Phrase("Phone : " + companyPhone, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyMobile.equals("")) {
                taxcell = new PdfPCell(new Phrase("Mobile : " + companyMobile, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyEmail.equals("")) {
                taxcell = new PdfPCell(new Phrase("Email : " + companyEmail, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxTable.addCell(taxcell);
            //document.add(taxTable);

            taxcell = new PdfPCell(new Phrase("Cash Bill", boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.BOTTOM);
            taxTable.addCell(taxcell);

            document.add(taxTable);

            PdfPTable table2 = new PdfPTable(2);
            table2.setWidthPercentage(100);
            PdfPCell tablecell = null;

            tablecell = new PdfPCell(new Phrase("Invoice No: " + billNo, reportdateFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("Dated: " + billDate, reportdateFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("User: ", reportdateFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("Time:9.00AM", reportdateFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.BOTTOM);
            // tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            table2.addCell(tablecell);
            document.add(table2);

            PdfPTable table3 = new PdfPTable(5);
            table3.setWidthPercentage(100);
            //table.setWidths(new float[]{40, 20,10,10,})
            PdfPCell cell3 = null;

            cell3 = new PdfPCell(new Phrase("Desc", reportdateFont));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table3.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("HSN", reportdateFont));
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            table3.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("Rate", reportdateFont));
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table3.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("Qty", reportdateFont));
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table3.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("Amnt", reportdateFont));
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setBorder(Rectangle.BOTTOM);
            //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            table3.addCell(cell3);

            for (int count = 0; count < bosLineItems.size(); count++) {
                cell3 = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), cellFont));
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                int UQC2Value = 0;
                for (Product product : products) {
                    if (product.getName().equals(bosLineItems.get(sno).getProductName())) {
                        UQC2Value = product.getUQC2Value();
                    }
                }
                int UQC1qty = bosLineItems.get(sno).getUqcOneQuantity(), UQC2qty = bosLineItems.get(sno).getUqcTwoQuantity();
                String strQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                if (UQC1qty > 0) {
                    strQuantity = UQC1qty + " " + getUQCcode(bosLineItems.get(sno).getUqcOne());
                    strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                    if (UQC2qty > 0) {
                        strQuantity = strQuantity + ", ";
                        strAltQuantity = strAltQuantity + ", ";
                    }
                }
                if (UQC2qty > 0) {
                    if (UQC1qty == 0) {
                        strQuantity = "";
                        strAltQuantity = "";
                    }
                    strQuantity = strQuantity + UQC2qty + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                    strAltQuantity = strAltQuantity + bosLineItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                }

                Phrase phrase = new Phrase();
                phrase.add(new Chunk(strQuantity, cellFont));
                phrase.add(new Chunk(Chunk.NEWLINE + strAltQuantity, cellFont));
                cell3 = new PdfPCell(phrase);
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getUqcOneRate()), cellFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);
                sno++;
            }
            int countCharge = 0;
            for (countCharge = 0; countCharge < bosChargeItems.size(); countCharge++) {
                cell3 = new PdfPCell(new Phrase(bosChargeItems.get(count).getBillChargeName(), cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(bosChargeItems.get(count).getValue()), boldFont));
                
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                table3.addCell(cell3);
            }
            if (billRoundOff != 0) {
                cell3 = new PdfPCell(new Phrase("Round off", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                
                if (billRoundOff > 0) {
                    cell3 = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), boldFont));
                } else {
                    cell3 = new PdfPCell(new Phrase(currency.format(billRoundOff), boldFont));
                }
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                table3.addCell(cell3);
            }

            cell3 = new PdfPCell(new Phrase("Total Amount ", cellFont));
            cell3.setColspan(4);
            cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table3.addCell(cell3);

            cell3 = new PdfPCell(new Phrase(currency.format(billNetAmount), boldFont));
           
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            table3.addCell(cell3);
            document.add(table3);

            document.close();

            if (Desktop.isDesktopSupported()) {
                try {
                    File myFile = new File("C:\\GST\\temp\\bill.pdf");
                    Desktop.getDesktop().open(myFile);
                } catch (IOException ex) {
                    // no application registered for PDFs
                }
            }

        } catch (IOException | DocumentException ex) {
            Logger.getLogger(A7Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
