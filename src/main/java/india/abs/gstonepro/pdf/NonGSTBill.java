/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import india.abs.gstonepro.api.models.BillOfSupply;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.CurrencyInWords;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.BillOfSupplyLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.ui.utils.BillOfSupplyBillItems;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author user
 */
public class NonGSTBill {

    public class FooterTable extends PdfPageEventHelper {

        protected PdfPTable footer;

        public FooterTable(PdfPTable footer) {
            this.footer = footer;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            footer.writeSelectedRows(0, 5, 20, 80, writer.getDirectContent());
        }
    }
    static int totalProductCount = 0, productcount, hsncount, hsncountNon;
    int count = 0;
    int productLimitA4 = 20, productLimitA5L = 12, productLimitA5P = 13;
    static int sno = 0, hsnno = 0;
    int random = 0;
    BigDecimal zero = new BigDecimal("0.000");

    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";
//    public String companyName = "", companyLine1 = "", companyLine2 = "", companyLine3 = "", companyLine4 = "",
//            companyDistrict = "", CompanyState = "", companyGSTIN = "";

    public String companyName = "", companyA4Addr = "", companyA5LAddr = "", companyA5PAddr = "",
            companyA7Addr = "", companyCity = "", companyDistrict = "", companyEmail = "",
            companyState = "", companyMobile = "", companyPhone = "", companyGSTIN = "", invoiceHeading = "",
            companyFooterHeading = "", companyFooterLineOne = "", companyFooterLineTwo = "", companyFooterLineThree = "",
            companyFooterLineFour = "", companyA4AddrLine1 = "", companyA4AddrLine2 = "", companyA4AddrLine3 = "", companyA5LAddrLine1 = "", companyA5LAddrLine2 = "",
            companyA5LAddrLine3 = "", companyA5PAddrLine1 = "", companyA5PAddrLine2 = "", companyA5PAddrLine3 = "", companyA7AddrLine1 = "", companyA7AddrLine2 = "",
            companyA7AddrLine3 = "";

    public String billNo = "", billDate = "", billCompanyName = "", billCompanyAddr1 = "", billCompanyAddr2 = "",
            billCompanyAddr3 = "", billCompanyCity = "", billCompanyDistrict = "",
            billCompanyState = "", billCompanyMobile = "", billCompanyPhone = "", billCompanyEmail = "", billCompanyGSTIN = "", billPayment = "",
            billBuyingOrderNo = "", billOrderDate = "", billDispatchThorugh = "", billDestination = "", billTermOfDelivery = "", billThrough = "",
            billReferenceNo = "", billRemarks = "", firstBillCopy = "", secondBillCopy = "", thirdBillCopy = "", fourthBillCopy = "", fifthBillCopy = "";
//    public String ShippingName = "", ShippingAddr1 = "", ShippingAddr2 = "", ShippingAddr3 = "", ShippingCity = "", ShippingPincode = "", ShippingDistrict = "", ShippingState = "",
//            ShippingMobile = "", ShippingPhone = "", ShippingGSTIN = "";
    public boolean isIGST = false, billIsSameAddress = true, isBill = false, isPdfOnly = false;
    public int billId = 0, noOfCopies;
    public float billIGST = 0, billTotalTax = 0, billFlightAmount = 0, billCGST = 0, billSGST = 0, billTotal = 0,
            billNetAmount = 0, billDiscountPer = 0, billDiscount = 0, billRoundOff = 0;
    DecimalFormat currency = new DecimalFormat("##,##,##0.000");
    String ShippingName = "", ShippingAddr1 = "", ShippingAddr2 = "", ShippingAddr3 = "", ShippingCity = "", ShippingPincode = "", ShippingDistrict = "", ShippingState = "",
            ShippingMobile = "", ShippingPhone = "", ShippingGSTIN = "";

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<BillOfSupplyLineItem> bosLineItems = new ArrayList<>();
    List<BillOfSupplyChargeItem> bosChargeItems = new ArrayList<>();
    List<BillOfSupplyBillItems> bosBillItems = new ArrayList<>();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
    BillOfSupply newBos = new BillOfSupply();

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1Bold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 6, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont7 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

    com.itextpdf.text.Font footerboldfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

    public void pdfByBillId(Boolean isPrint, Boolean isPreview, String billId) {

        BillOfSupply bos = new BillOfSupplyLogic().readABOS(billId);
        List<BillOfSupplyLineItem> bosLis = bos.getBosLineItems();
        List<BillOfSupplyChargeItem> bosCis = bos.getBosChargeItems();

        printPdf(isPrint, isPreview, bos, bosLis, bosCis);

    }

    public void printPdf(Boolean isPrint, Boolean isPreview, BillOfSupply bos, List<BillOfSupplyLineItem> bosLis, List<BillOfSupplyChargeItem> bosCis) {
        try {
            sno = 0;
            hsnno = 0;
            newBos = bos;
            bosLineItems = bosLis;
            bosChargeItems = bosCis;

            hsncount = bosChargeItems.size();
            getBillItems();

            setCompany();
            setBillDetails();
            
            
            String paperSize = SessionDataUtil.getCompanyPolicyData().getPrintPaperSize();
            switch (paperSize) {
                case "A4_P":
                    createPdfA4(isPrint, isPreview);
                    break;
                case "A5_P":
                    try {
                        createPdfA5P(isPrint, isPreview);
                    } catch (DocumentException ex) {
                        Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "A5_L": {

                    try {
                        createPdfA5L(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
                case "A7_P": {

                    try {
                        createPdfA7(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
//            case "A7_P":
//                createPdfA5P(isPrint, isPreview);
                default:
                    break;
            }
        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getBillItems() {
        int Sno = 0;
        int produtSize = bosLineItems.size();
        for (int i = 0; i < bosLineItems.size(); i++) {
            BillOfSupplyBillItems p = new BillOfSupplyBillItems();
            p.setSno(String.valueOf(i));
            p.setProductNameOrbillChargeName(bosLineItems.get(i).getProductName());
            p.setHsnSac(bosLineItems.get(i).getHsnSac());
            p.setUqcOne(bosLineItems.get(i).getUqcOne());
            p.setUqcOneQuantity(bosLineItems.get(i).getUqcOneQuantity());
            p.setUqcOneRate(bosLineItems.get(i).getUqcOneRate());
            p.setUqcOneValue(bosLineItems.get(i).getUqcOneValue());
            p.setUqcTwo(bosLineItems.get(i).getUqcTwo());
            p.setUqcTwoQuantity(bosLineItems.get(i).getUqcTwoQuantity());
            p.setUqcTwoRate(bosLineItems.get(i).getUqcTwoRate());
            p.setUqcTwoValue(bosLineItems.get(i).getUqcTwoValue());
            p.setDiscountValue(bosLineItems.get(i).getDiscountValue());
            p.setProductvalueOrbillChargeValue(bosLineItems.get(i).getValue());
            p.setIsBillCharge(bosLineItems.get(i).getProduct().isService());
            bosBillItems.add(p);
        }

//        for (int j = 0; j < bosChargeItems.size(); j++) {
//            BillOfSupplyBillItems p = new BillOfSupplyBillItems();
//            p.setSno(String.valueOf(j + produtSize));
//            p.setProductNameOrbillChargeName(bosChargeItems.get(j).getBillChargeName());
//            p.setProductvalueOrbillChargeValue(bosChargeItems.get(j).getValue());
//            p.setIsBillCharge(true);
//            bosBillItems.add(p);
//        }
        for (int l = 0; l < bosBillItems.size(); l++) {
            
        }
        productcount = bosBillItems.size();
        
    }

    public void setBillDetails() {
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");

        billNo = newBos.getBosNo();
        billDate = sdfr.format(newBos.getBosDate());
        if (newBos.getLedger() == null) {
            billCompanyName = "";
            billCompanyAddr1 = "";
            billCompanyCity = "";
            billCompanyDistrict = "";
            billCompanyState = "";
            billCompanyMobile = "";
            billCompanyPhone = "";
            billCompanyGSTIN = "";
        } else {
            billCompanyName = newBos.getLedger().getLedgerName();

            String Address = newBos.getLedger().getAddress();
            if (Address != null) {
                billCompanyAddr1 = Address.replaceAll("/~/", ",");
            }

//        billCompanyAddr2 = "";
//        billCompanyAddr3 = "";
            if (newBos.getLedger().getCity() != null) {
                billCompanyCity = newBos.getLedger().getCity();
            }
            if (newBos.getLedger().getDistrict() != null) {
                billCompanyDistrict = newBos.getLedger().getDistrict();
            }
            if (newBos.getLedger().getState() != null) {
                billCompanyState = newBos.getLedger().getState();
            }
            if (newBos.getLedger().getMobile() != null) {
                billCompanyMobile = newBos.getLedger().getMobile();
            }
            if (newBos.getLedger().getEmail() != null) {
                billCompanyEmail = newBos.getLedger().getEmail();
            }
            if (newBos.getLedger().getPhone() != null) {
                billCompanyPhone = newBos.getLedger().getPhone();
            }
            if (newBos.getLedger().getGSTIN() != null) {
                billCompanyGSTIN = newBos.getLedger().getGSTIN();
            }
        }
        billNetAmount = newBos.getBillAmount().floatValue();
        billRoundOff = newBos.getRoundOffValue().floatValue();
        billReferenceNo = newBos.getReferenceNumber();
        billRemarks = newBos.getRemarks();
        billDiscount = Float.parseFloat(newBos.getDiscountAmount().toString());

        noOfCopies = newBos.getCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
        firstBillCopy = newBos.getCompany().getCompanyPolicy().getFirstInvoiceWord();
        secondBillCopy = newBos.getCompany().getCompanyPolicy().getSecondInvoiceWord();
        thirdBillCopy = newBos.getCompany().getCompanyPolicy().getThirdInvoiceWord();
        fourthBillCopy = newBos.getCompany().getCompanyPolicy().getFourthInvoiceWord();
        fifthBillCopy = newBos.getCompany().getCompanyPolicy().getFifthInvoiceWord();

    }

    public void setCompany() {
        companyName = SessionDataUtil.getSelectedCompany().getCompanyName();

        String A4AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineOne();
        String A4AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineTwo();
        String A4AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineThree();
        if (A4AddressLine1 != null) {
            companyA4AddrLine1 = A4AddressLine1.replaceAll("/~/", ",");
        }
        if (A4AddressLine2 != null) {
            companyA4AddrLine2 = A4AddressLine2.replaceAll("/~/", ",");
        }
        if (A4AddressLine3 != null) {
            companyA4AddrLine3 = A4AddressLine3.replaceAll("/~/", ",");
        }
        String A5PAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineOne();
        String A5PAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineTwo();
        String A5PAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineThree();
        if (A5PAddressLine1 != null) {
            companyA5PAddrLine1 = A5PAddressLine1.replaceAll("/~/", ",");
        }
        if (A5PAddressLine2 != null) {
            companyA5PAddrLine2 = A5PAddressLine2.replaceAll("/~/", ",");
        }
        if (A5PAddressLine3 != null) {
            companyA5PAddrLine3 = A5PAddressLine3.replaceAll("/~/", ",");
        }
        String A5LAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineOne();
        String A5LAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineTwo();
        String A5LAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineThree();
        if (A5LAddressLine1 != null) {
            companyA5LAddrLine1 = A5LAddressLine1.replaceAll("/~/", ",");
        }
        if (A5LAddressLine2 != null) {
            companyA5LAddrLine2 = A5LAddressLine2.replaceAll("/~/", ",");
        }
        if (A5LAddressLine3 != null) {
            companyA5LAddrLine3 = A5LAddressLine3.replaceAll("/~/", ",");
        }
        String A7AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineOne();
        String A7AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineTwo();
        String A7AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineThree();
        if (A7AddressLine1 != null) {
            companyA7AddrLine1 = A7AddressLine1.replaceAll("/~/", ",");
        }
        if (A7AddressLine2 != null) {
            companyA7AddrLine2 = A7AddressLine2.replaceAll("/~/", ",");
        }
        if (A7AddressLine3 != null) {
            companyA7AddrLine3 = A7AddressLine3.replaceAll("/~/", ",");
        }
        if (SessionDataUtil.getSelectedCompany().getCity() != null) {
            companyCity = SessionDataUtil.getSelectedCompany().getCity();
        }
        if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
            companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
        }
        if (SessionDataUtil.getSelectedCompany().getState() != null) {
            companyState = SessionDataUtil.getSelectedCompany().getState();
        }
        if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
            companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
        }
        if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
            companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
        }
        if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
            companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
        }
        if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
            companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getBillOfSupplyWord() != null) {
            invoiceHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getBillOfSupplyWord();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading() != null) {
            companyFooterHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne() != null) {
            companyFooterLineOne = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo() != null) {
            companyFooterLineTwo = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree() != null) {
            companyFooterLineThree = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour() != null) {
            companyFooterLineFour = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour();
        }

    }

    public void addInfo(Document document, int invoiceno, String invoiceCopyName) {
        try {

            PdfPTable taxTable = new PdfPTable(2);
            taxTable.setWidthPercentage(100);

            taxTable.setWidths(new float[]{55, 45});
            PdfPCell taxcell;

            Phrase p = new Phrase("INVOICE " + invoiceno, boldFont);
            taxcell = new PdfPCell(p);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);

            Phrase p1 = new Phrase(invoiceCopyName, cellFont1);
            taxcell = new PdfPCell(p1);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);
            document.add(taxTable);

            PdfPCell cell;

            PdfPTable Table1 = new PdfPTable(2);
            Table1.setWidthPercentage(100);
            PdfPCell cell1 = null;

            cell1 = new PdfPCell(new Phrase("Invoice No: " + billNo, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Date : " + billDate, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            document.add(Table1);

            taxTable = new PdfPTable(1);
            taxTable.setWidthPercentage(100);
            taxcell = null;

            taxcell = new PdfPCell(new Phrase(companyName, boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);
            companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
            if (!companyA4Addr.equals("")) {
                taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);
            if (!companyPhone.equals("")) {
                taxcell = new PdfPCell(new Phrase("Phone : " + companyPhone, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyMobile.equals("")) {
                taxcell = new PdfPCell(new Phrase("Mobile : " + companyMobile, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyEmail.equals("")) {
                taxcell = new PdfPCell(new Phrase("Email : " + companyEmail, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            document.add(taxTable);

            PdfPTable Table = new PdfPTable(1);
            Table.setWidthPercentage(100);
            cell = null;

            cell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            Table.addCell(cell);

            document.add(Table);

            PdfPTable table2 = new PdfPTable(2);
            table2.setWidthPercentage(100);
            PdfPCell tablecell = null;

            tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.LEFT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyAddr1, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.LEFT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("State: " + billCompanyState, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.LEFT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase("State Code: " + StateCode.getStateCode(billCompanyState), cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablecell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyPhone + " | " + billCompanyMobile + " | " + billCompanyEmail, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablecell.setBorder(Rectangle.NO_BORDER);
            tablecell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            table2.addCell(tablecell);
            document.add(table2);

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidthPercentage(100);
            PdfPCell cell3 = null;

            cell3 = new PdfPCell(new Phrase(billRemarks, cellFont1));
            cell3.setColspan(2);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            table3.addCell(cell3);

            document.add(table3);

        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProductItems(Document document, int productLimitInCounter) {

        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell taxcell, cell;
            PdfPTable pdfTable = new PdfPTable(8);
            pdfTable.setWidths(new float[]{8, 25, 12, 20, 10, 15, 10, 20});
            pdfTable.setTotalWidth(555);
            pdfTable.setLockedWidth(true);

            float ht = pdfTable.getTotalHeight();

            taxcell = new PdfPCell(new Phrase("S.No", totalFont));
            //taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Quantity", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("(per)", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Discount", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", totalFont));
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(taxcell);
//-------------------------------------------------------------------------------------------------------------------

//            productcount = bosLineItems.size();
            int linecount = 0;
            for (int counter = 0; counter < productLimitInCounter; counter++) {
                linecount++;
                
                String strSNO = Integer.toString(sno + 1);
                if (!bosBillItems.get(sno).getIsBillCharge()) {

                    
//-------------------------------------------------------------------------------------------------------------------

                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosBillItems.get(sno).getProductNameOrbillChargeName(), totalFontBold));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosBillItems.get(sno).getHsnSac(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    int UQC2Value = 0;
                    for (Product product : products) {
                        if (product.getName().equals(bosBillItems.get(sno).getProductNameOrbillChargeName())) {
                            UQC2Value = product.getUQC2Value();
                        }
                    }
                    int UQC1qty = bosBillItems.get(sno).getUqcOneQuantity(), UQC2qty = bosBillItems.get(sno).getUqcTwoQuantity();
                    String strQuantity = "0 " + getUQCcode(bosBillItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(bosBillItems.get(sno).getUqcTwo());
                    if (UQC1qty > 0) {
                        strQuantity = UQC1qty + " " + getUQCcode(bosBillItems.get(sno).getUqcOne());
                        strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(bosBillItems.get(sno).getUqcTwo());
                        if (UQC2qty > 0) {
                            strQuantity = strQuantity + ", ";
                            strAltQuantity = strAltQuantity + " ";
                        }
                    }
                    if (UQC2qty > 0) {
                        if (UQC1qty == 0) {
                            strQuantity = "";
                            strAltQuantity = "";
                        }
                        strQuantity = strQuantity + UQC2qty + " " + getUQCcode(bosBillItems.get(sno).getUqcTwo());
                        strAltQuantity = strAltQuantity + bosBillItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(bosBillItems.get(sno).getUqcTwo());
                    }
                    int isEqual = bosBillItems.get(sno).getUqcOne().compareTo(bosBillItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf(bosBillItems.get(sno).getUqcOneQuantity()), cellFont));
                    } else {
                        Phrase phrase = new Phrase();
                        phrase.add(new Chunk(strQuantity, totalFontBold));
                        phrase.add(new Chunk(Chunk.NEWLINE + "(" + strAltQuantity + ")", cellFont));
                        cell = new PdfPCell(phrase);
                    }

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    Phrase phrase = new Phrase();
                    phrase.add(new Chunk(currency.format(bosBillItems.get(sno).getUqcOneRate()), totalFontBold));
                    phrase.add(new Chunk(Chunk.NEWLINE + currency.format(bosBillItems.get(sno).getUqcTwoRate()), cellFont));
                    cell = new PdfPCell(phrase);
//                    cell = new PdfPCell(new Phrase(currency.format(bosBillItems.get(sno).getUqcOneRate()), cellFont));
                    cell.setBorder(Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    phrase = new Phrase();
                    phrase.add(new Chunk("(" + bosBillItems.get(sno).getUqcOne() + ")", totalFontBold));
                    phrase.add(new Chunk(Chunk.NEWLINE + "(" + bosBillItems.get(sno).getUqcTwo() + ")", cellFont));
                    cell = new PdfPCell(phrase);
//                    cell = new PdfPCell(new Phrase(getUQCcode(bosBillItems.get(sno).getUqcOne()), cellFont));
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    int isZero = bosBillItems.get(sno).getDiscountValue().compareTo(zero);
                    if (isZero == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf("-"), cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    } else {
                        cell = new PdfPCell(new Phrase("-" + String.valueOf(bosBillItems.get(sno).getDiscountValue()), cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    }

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(currency.format(bosBillItems.get(sno).getProductvalueOrbillChargeValue()), totalFontBold));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                } else {
                    
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosBillItems.get(sno).getProductNameOrbillChargeName(), HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    for (int i = 0; i < 2; i++) {
                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);
                    }
                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

//                    if (bosBillItems.get(sno).getProductvalueOrbillChargeValue().compareTo(zero) > 0) {
//                        cell = new PdfPCell(new Phrase("+" + currency.format(bosBillItems.get(sno).getProductvalueOrbillChargeValue()), HeaderFont));
//                    } else {
                    cell = new PdfPCell(new Phrase(currency.format(bosBillItems.get(sno).getProductvalueOrbillChargeValue()), HeaderFont));
//                    }
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);
                }
                sno++;

            }
            linecount = (productLimitA4 - linecount);
            Phrase phrase = new Phrase();
            
            for (int i = 0; i < linecount; i++) {
                phrase.add(new Chunk(" ", totalFontBold));
//                phrase.add(new Chunk(Chunk.NEWLINE + " 2", cellFont));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

            }

            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUQCcode(String value) {
        String strUQCcode = "";
        for (UQC uqc : UQCs) {
            if (uqc.getQuantityName().equals(value)) {
                strUQCcode = uqc.getUQCCode();
                break;
            }
        }
        return strUQCcode;
    }

    public void afterAddProductItemsContinued(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

            PdfPTable pdfTable = new PdfPTable(8);
            pdfTable.setWidths(new float[]{8, 25, 12, 20, 10, 15, 10, 20});
            pdfTable.setTotalWidth(555);
            pdfTable.setLockedWidth(true);

            pdfTable = new PdfPTable(2);
            pdfTable.setWidthPercentage(100);

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItems1(Document document) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(8);
        pdfTable.setWidths(new float[]{8, 25, 12, 20, 10, 15, 10, 20});
        pdfTable.setTotalWidth(555);
        pdfTable.setLockedWidth(true);
        if (billDiscount != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Bill Discount", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billDiscount), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        if (billRoundOff != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Round Off", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billRoundOff), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Net Amount", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(currency.format(billNetAmount), HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);
        document.add(pdfTable);

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase("Amount Chargeable(in words)", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("E. & O.E", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(new CurrencyInWords().AmountInWords(billNetAmount), HeaderFont));
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        for (int i = 0; i < 5; i++) {
            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

        }

        document.add(pdfTable);

    }

    public void addFooter(Document document) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPTable outerTable = new PdfPTable(2);
            outerTable.setWidthPercentage(100);

            PdfPTable pdfTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(new Phrase(companyFooterHeading, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            pdfTable = new PdfPTable(1);

            cell = new PdfPCell(new Phrase("for " + companyName.toUpperCase(), totalFontBold));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            document.add(outerTable);
        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createPdfA4(Boolean isPrint, Boolean isPreview) {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            Rectangle layout = new Rectangle(PageSize.A4);
            Document document = new Document(layout, 20, 20, 20, 20);
            String path = "";
            Boolean isApproved = false;
            String nowFilePath = "";
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Bill Of Supply" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Bill Of Supply" + "-" + billNo));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }

            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();

                for (int copies = 0; copies < noOfCopies; copies++) {
                    sno = 0;
                    document.newPage();
                    String invoiceCopyName = null;
                    switch (copies) {
                        case 0:
                            invoiceCopyName = firstBillCopy;
                            break;
                        case 1:
                            invoiceCopyName = secondBillCopy;
                            break;
                        case 2:
                            invoiceCopyName = thirdBillCopy;
                            break;
                        case 3:
                            invoiceCopyName = fourthBillCopy;
                            break;
                        case 4:
                            invoiceCopyName = fifthBillCopy;
                            break;
                        default:
                            break;
                    }

//-----------------------------------------------------------------------------------------------------------------------
                    int pagecount = 0;
                    pagecount = (productcount / productLimitA4);
                    if ((productcount % productLimitA4) > 0) {
                        pagecount += 1;
                    }
                    if (pagecount == 1) {
                        addInfo(document, pagecount, invoiceCopyName);
                        addProductItems(document, (productcount % productLimitA4));
                        afterAddProductItems1(document);
//                    addHSNTable(document, hsnno, hsncount);
                        addFooter(document);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (pagecount == i) {
                                addInfo(document, i, invoiceCopyName);
                                addProductItems(document, (productcount % productLimitA4));
                                afterAddProductItems1(document);
//                            addHSNTable(document, hsnno, hsncount);
                                addFooter(document);
                            } else {
                                addInfo(document, i, invoiceCopyName);
                                addProductItems(document, productLimitA4);
                                afterAddProductItemsContinued(document);
                                document.newPage();
                            }
                        }
                    }
                }
                document.close();
                fos.close();

                if (isPrint) {
                    try {
                        pdfPrint(path);
                    } catch (PrintException ex) {
                        Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);
                }

            }
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
//            if (!isPrint && isApproved) {
//                JOptionPane.showMessageDialog(null, "Bill Downloaded at " + path, "Message", JOptionPane.INFORMATION_MESSAGE);
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addInfoA5P(Document document, String invoiceCopyName) throws DocumentException, IOException {

        PdfPCell taxcell;
        Phrase phrase = new Phrase();

        PdfPTable nameTable2 = new PdfPTable(3);
        nameTable2.setWidthPercentage(100);
        nameTable2.setWidths(new float[]{43, 43, 26});

        phrase.add(new Chunk("GSTIN/UIN : ", cellFont1));
        phrase.add(new Chunk(companyGSTIN, cellFont1Bold));
        taxcell = new PdfPCell(phrase);
        taxcell.setBorder(Rectangle.LEFT | Rectangle.TOP);
        nameTable2.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("", totalFont));
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        taxcell.setPaddingBottom(-3);
        taxcell.setBorder(Rectangle.TOP);
        nameTable2.addCell(taxcell);

        Phrase p = new Phrase();

        p.add(new Chunk("Mobile : ", cellFont1));
        p.add(new Chunk(companyPhone, cellFont1Bold));
        taxcell = new PdfPCell(p);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.TOP);
        nameTable2.addCell(taxcell);

        document.add(nameTable2);

        PdfPTable nameTable1 = new PdfPTable(3);
        nameTable1.setWidthPercentage(100);
        nameTable1.setWidths(new float[]{43, 43, 26});

        taxcell = new PdfPCell(new Phrase(" ", totalFont));
        taxcell.setBorder(Rectangle.LEFT);
        taxcell.setPaddingBottom(-3);
        nameTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("   " + companyName, totalFont));
        taxcell.setPaddingBottom(-3);
        taxcell.setBorder(Rectangle.NO_BORDER);
        nameTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceCopyName, cellFont1));
        taxcell.setBorder(Rectangle.RIGHT);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setPaddingBottom(-3);
        nameTable1.addCell(taxcell);

        if (!companyA5PAddrLine1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine1, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine2, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else if (!companyA5PAddrLine2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine2, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else if (!companyA5PAddrLine3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else {
            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        }

        document.add(nameTable1);

        PdfPTable addressTable1 = new PdfPTable(3);
        addressTable1.setWidthPercentage(100);
        addressTable1.setWidths(new float[]{28, 44, 28});

        Phrase p2 = new Phrase(), p3 = new Phrase();

        p3.add(new Chunk("POS : ", cellFont1));
        p3.add(new Chunk(companyState + " (" + new StateCode().getStateCode(companyState) + ")", cellFont1Bold));
        taxcell = new PdfPCell(p3);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        taxcell.setBorder(Rectangle.LEFT);
        addressTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceHeading, totalFont));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOTTOM);
        addressTable1.addCell(taxcell);

        p2.add(new Chunk("Date : ", cellFont1));
        p2.add(new Chunk(billDate, cellFont1Bold));
        taxcell = new PdfPCell(p2);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.RIGHT);
        addressTable1.addCell(taxcell);

        document.add(addressTable1);

        PdfPTable revtable = new PdfPTable(3);
        revtable.setWidthPercentage(100);
        revtable.setWidths(new float[]{30, 30, 40});
        PdfPCell tablecell = null;

        Phrase p1 = new Phrase();

        p1.add(new Chunk("Bill No. : ", cellFont1));
        p1.add(new Chunk(String.valueOf(billNo), cellFont1Bold));
        taxcell = new PdfPCell(p1);
        taxcell.setBorder(Rectangle.BOX);
        revtable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billRemarks, totalFont));
        taxcell.setColspan(2);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOX);
        revtable.addCell(taxcell);

        document.add(revtable);

        PdfPTable outerTable1 = new PdfPTable(2);
        outerTable1.setWidthPercentage(100);
        outerTable1.setWidths(new float[]{50, 50});

        PdfPTable Table1 = new PdfPTable(1);
        taxcell = new PdfPCell(new Phrase("Bill to Party", cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyName + ",", totalFont));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyAddr1 + billCompanyAddr2 + billCompanyAddr3, cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        Phrase p7 = new Phrase();

        p7.add(new Chunk("GSTIN/UIN : ", cellFont1));
        p7.add(new Chunk(billCompanyGSTIN, cellFont1Bold));
        taxcell = new PdfPCell(p7);
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        outerTable1.addCell(Table1);

        Table1 = new PdfPTable(1);
        taxcell = new PdfPCell(new Phrase("Ship to Party", cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setColspan(2);
        Table1.addCell(taxcell);

        if (billIsSameAddress) {
            taxcell = new PdfPCell(new Phrase(billCompanyName + ",", totalFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr1 + ", " + billCompanyAddr2 + ", " + billCompanyAddr3 + ".", cellFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p4 = new Phrase();
            p4.add(new Chunk("GSTIN/UIN : ", cellFont1));
            p4.add(new Chunk(billCompanyGSTIN, cellFont1Bold));
            taxcell = new PdfPCell(p4);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", cellFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            taxcell = new PdfPCell(new Phrase(ShippingName + ",", totalFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(ShippingAddr1 + ", " + ShippingAddr2 + ", " + ShippingPincode + ".", cellFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p4 = new Phrase();
            p4.add(new Chunk("GSTIN/UIN : ", cellFont));
            p4.add(new Chunk(ShippingGSTIN, totalFont));
            taxcell = new PdfPCell(p4);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("State : " + ShippingState + "            Code : " + new StateCode().getStateCode(ShippingState), cellFont));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }
        outerTable1.addCell(Table1);

        document.add(outerTable1);

    }

    public void addProductItemsA5P(Document document, int Sno, int productLimitInCounter) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(6);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

        cell = new PdfPCell(new Phrase("S.No", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Description", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("HSN/  SAC", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Qty", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Rate", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);
        int count = 0;
        for (int counter = 0; counter < productLimitInCounter; counter++) {
            count++;
            int UQC2Value = 0;
            for (Product product : products) {
                if (product.getName().equals(bosLineItems.get(sno).getProductName())) {
                    UQC2Value = product.getUQC2Value();
                }
            }
            int UQC1qty = bosLineItems.get(sno).getUqcOneQuantity(), UQC2qty = bosLineItems.get(sno).getUqcTwoQuantity();
            String strQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
            if (UQC1qty > 0) {
                strQuantity = UQC1qty + " " + getUQCcode(bosLineItems.get(sno).getUqcOne());
                strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                if (UQC2qty > 0) {
                    strQuantity = strQuantity + ", ";
                    strAltQuantity = strAltQuantity + ", ";
                }
            }
            if (UQC2qty > 0) {
                if (UQC1qty == 0) {
                    strQuantity = "";
                    strAltQuantity = "";
                }
                strQuantity = strQuantity + UQC2qty + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                strAltQuantity = strAltQuantity + bosLineItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
            }
            if (!bosLineItems.get(sno).getProduct().isService()) {
                if ((counter + 1) == productLimitInCounter) {
                    String strSNO = Integer.toString(sno + 1);
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    int isEqual = bosLineItems.get(sno).getUqcOne().compareTo(bosLineItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), cellFont));
                    } else {
                        Phrase p = new Phrase();

                        p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), totalFont));
                        p.add(new Chunk(String.valueOf(" " + getUQCcode(bosLineItems.get(sno).getUqcOne()) + " "), footerfont7));
                        p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcTwoQuantity()), totalFont));
                        p.add(new Chunk(String.valueOf(" " + getUQCcode(bosLineItems.get(sno).getUqcTwo())), footerfont7));
                        cell = new PdfPCell(p);
                    }
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    Phrase p = new Phrase();
                    int isZero = bosLineItems.get(sno).getDiscountValue().compareTo(zero);

                    p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcOneRate()), cellFont));
                    if (isZero == 0) {
                        p.add(new Chunk("", cellFont));
//                p.add(new Chunk(" (" + String.valueOf(bosLineItems.get(sno).getDiscountValue()) + ")", cellFont));
                    } else {
                        p.add(new Chunk(" (-" + String.valueOf(bosLineItems.get(sno).getDiscountValue()) + ")", cellFont));
                    }

                    cell = new PdfPCell(p);

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getValue()), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);
                } else {
                    String strSNO = Integer.toString(sno + 1);
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    int isEqual = bosLineItems.get(sno).getUqcOne().compareTo(bosLineItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), cellFont));
                    } else {
                        Phrase p = new Phrase();

                        p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), totalFont));
                        p.add(new Chunk(String.valueOf(" " + getUQCcode(bosLineItems.get(sno).getUqcOne()) + " "), footerfont7));
                        p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcTwoQuantity()), totalFont));
                        p.add(new Chunk(String.valueOf(" " + getUQCcode(bosLineItems.get(sno).getUqcTwo())), footerfont7));
                        cell = new PdfPCell(p);
                    }
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    Phrase p = new Phrase();
                    int isZero = bosLineItems.get(sno).getDiscountValue().compareTo(zero);

                    p.add(new Chunk(String.valueOf(bosLineItems.get(sno).getUqcOneRate()), cellFont));
                    if (isZero == 0) {
                        p.add(new Chunk("", cellFont));
//                p.add(new Chunk(" (" + String.valueOf(bosLineItems.get(sno).getDiscountValue()) + ")", cellFont));
                    } else {
                        p.add(new Chunk(" (-" + String.valueOf(bosLineItems.get(sno).getDiscountValue()) + ")", cellFont));
                    }

                    cell = new PdfPCell(p);

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getValue()), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);
                }
            } else {
                if ((counter + 1) == productLimitInCounter) {
                    String strSNO = Integer.toString(sno + 1);
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), HeaderFont));
                    
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                    pdfTable.addCell(cell);
                } else {
                    String strSNO = Integer.toString(sno + 1);
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), cellFont));
                    
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);
                }
            }
            sno++;
        }

        count = (productLimitA5P - count);
        for (int i = 0; i < count; i++) {
            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setColspan(6);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);
        }
        document.add(pdfTable);

    }

    public void afteraddProductItemsA5P(Document document) throws DocumentException {

        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        Phrase p;
        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(6);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

        if (billDiscount != 0) {
            cell = new PdfPCell(new Phrase("Bill Discount", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), boldFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);

        }

        if (billRoundOff != 0) {
            cell = new PdfPCell(new Phrase("Round off", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            
            if (billRoundOff > 0) {
                cell = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), HeaderFont));
            } else {
                cell = new PdfPCell(new Phrase(currency.format(billRoundOff), HeaderFont));
            }
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("Total Amount ", cellFont));
        cell.setColspan(5);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(currency.format(billNetAmount), HeaderFont));
        
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);
        document.add(pdfTable);

    }

    public void afterAddProductItemsContinuedA5P(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

            PdfPTable pdfTable = new PdfPTable(6);
            pdfTable.setWidthPercentage(100);
            pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(6);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(6);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addFooterA5P(Document document) throws DocumentException {
        PdfPTable pdfTable;

        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        Phrase p;
        PdfPCell cell;

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{50, 50});
        cell = new PdfPCell(new Phrase(companyFooterHeading, HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        Phrase p2 = new Phrase();
        p2.add(new Chunk("for ", cellFont1));
        p2.add(new Chunk(companyName.toUpperCase(), HeaderFont));
        cell = new PdfPCell(p2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        document.add(pdfTable);

    }

    public void lastLineA5P(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    private void createPdfA5P(Boolean isPrint, Boolean isPreview) throws DocumentException {
        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            String sql = "";
            Rectangle layout = new Rectangle(PageSize.A5);
            Boolean isApproved = false;
            Document document = new Document(layout, 20, 15, 15, 15);
            String path = null;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "BillOfSupply" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("BillOfSupply" + "-" + newBos.getBosId()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                for (int copies = 0; copies < noOfCopies; copies++) {
                    sno = 0;
                    document.newPage();
                    String invoiceCopyName = null;
                    switch (copies) {
                        case 0:
                            invoiceCopyName = firstBillCopy;
                            break;
                        case 1:
                            invoiceCopyName = secondBillCopy;
                            break;
                        case 2:
                            invoiceCopyName = thirdBillCopy;
                            break;
                        case 3:
                            invoiceCopyName = fourthBillCopy;
                            break;
                        case 4:
                            invoiceCopyName = fifthBillCopy;
                            break;
                        default:
                            break;
                    }

                    int pagecount = 0;
                    pagecount = (productcount / productLimitA5P);
                    if ((productcount % productLimitA5P) > 0) {
                        pagecount += 1;
                    }
                    if (pagecount == 1) {
                        addInfoA5P(document, invoiceCopyName);
                        addProductItemsA5P(document, sno, (productcount % productLimitA5P));
                        afteraddProductItemsA5P(document);
                        addFooterA5P(document);
                        lastLineA5P(document, pagecount, pagecount);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (pagecount == i) {
                                addInfoA5P(document, invoiceCopyName);
                                addProductItemsA5P(document, sno, (productcount % productLimitA5P));
                                afteraddProductItemsA5P(document);
                                addFooterA5P(document);
                                lastLineA5P(document, i, pagecount);
                            } else {
                                addInfoA5P(document, invoiceCopyName);
                                addProductItemsA5P(document, sno, (productLimitA5P));
                                afterAddProductItemsContinuedA5P(document);
                                lastLineA5P(document, i, pagecount);
                                document.newPage();
                            }
                        }
                    }
                }
                document.close();
                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);
                    } catch (PrintException ex) {
                        Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addInfoA5L(Document document, String invoiceCopyName) throws DocumentException, IOException {
        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        Phrase p;
        PdfPCell taxcell;

        PdfPTable outerTable = new PdfPTable(3);
        outerTable.setWidthPercentage(100);
        outerTable.setWidths(new float[]{35, 35, 35});

        Phrase p1 = new Phrase();

        p1.add(new Chunk("Bill No. : ", footerfont));
        p1.add(new Chunk(String.valueOf(billNo), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceHeading, boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
        outerTable.addCell(taxcell);

        p1 = new Phrase();

        p1.add(new Chunk(invoiceCopyName + " | ", footerfont));
        p1.add(new Chunk("Date : ", footerfont));
        p1.add(new Chunk(String.valueOf(billDate), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        document.add(outerTable);

        PdfPTable outerTable1 = new PdfPTable(3);
        outerTable1.setWidthPercentage(100);
        outerTable1.setWidths(new float[]{50, 50, 50});

        PdfPTable Table1 = new PdfPTable(1);

        taxcell = new PdfPCell(new Phrase(companyName + ",", boldcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!companyA5LAddrLine1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }
        outerTable1.addCell(Table1);

        Table1 = new PdfPTable(1);
        taxcell = new PdfPCell(new Phrase("Bill to Party", normalcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyName + ",", boldcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!billCompanyAddr1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }

        outerTable1.addCell(Table1);

        Table1 = new PdfPTable(1);
        taxcell = new PdfPCell(new Phrase("Ship to Party", normalcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setColspan(2);
        Table1.addCell(taxcell);

//        if (billIsSameAddress) {
        taxcell = new PdfPCell(new Phrase(billCompanyName + ",", boldcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!billCompanyAddr1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase pg = new Phrase();
            pg.add(new Chunk("GSTIN/UIN : ", footerfont));
            pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
            taxcell = new PdfPCell(pg);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }
//        } else {
//            taxcell = new PdfPCell(new Phrase(ShippingName + ",", totalFont));
//            taxcell.setBorder(Rectangle.NO_BORDER);
//            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            Table1.addCell(taxcell);
//
//            taxcell = new PdfPCell(new Phrase(ShippingAddr1 + ", " + ShippingAddr2 + ", " + ShippingPincode + ".", normalcalibri));
//            taxcell.setBorder(Rectangle.NO_BORDER);
//            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            Table1.addCell(taxcell);
//
//            Phrase p4 = new Phrase();
//            p4.add(new Chunk("GSTIN/UIN : ", normalcalibri));
//            p4.add(new Chunk(ShippingGSTIN, totalFont));
//            taxcell = new PdfPCell(p4);
//            taxcell.setBorder(Rectangle.NO_BORDER);
//            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            Table1.addCell(taxcell);
//
//            taxcell = new PdfPCell(new Phrase("State : " + ShippingState + "            Code : " + new StateCode().getStateCode(ShippingState), normalcalibri));
//            taxcell.setBorder(Rectangle.NO_BORDER);
//            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            Table1.addCell(taxcell);
//        }
        outerTable1.addCell(Table1);

        document.add(outerTable1);
    }

    private int addProductItemsA5L(Document document, int productLimitInCounter) throws DocumentException, IOException {

        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        Phrase p;
        PdfPCell taxcell;
        System.err.println("productcount" + productcount);

        PdfPTable thirdTable = new PdfPTable(7);
        thirdTable.setWidthPercentage(100);
        thirdTable.setWidths(new float[]{5, 28, 11, 20, 10, 10, 18});

        taxcell = new PdfPCell(new Phrase("S.No", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("Description", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("HSN/SAC", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("Qty", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("Rate ", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("Discount ", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("Amount", boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
        thirdTable.addCell(taxcell);
        int lineCount = 0;

        for (int counter = 0; counter < productLimitInCounter; counter++) {
            lineCount++;
            int UQC2Value = 0;
            for (Product product : products) {
                if (product.getName().equals(bosLineItems.get(sno).getProductName())) {
                    UQC2Value = product.getUQC2Value();
                }
            }
            int UQC1qty = bosLineItems.get(sno).getUqcOneQuantity(), UQC2qty = bosLineItems.get(sno).getUqcTwoQuantity();
            String strQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
            if (UQC1qty > 0) {
                strQuantity = UQC1qty + " " + getUQCcode(bosLineItems.get(sno).getUqcOne());
                strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                if (UQC2qty > 0) {
                    strQuantity = strQuantity + ", ";
                    strAltQuantity = strAltQuantity + ", ";
                }
            }
            if (UQC2qty > 0) {
                if (UQC1qty == 0) {
                    strQuantity = "";
                    strAltQuantity = "";
                }
                strQuantity = strQuantity + UQC2qty + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                strAltQuantity = strAltQuantity + bosLineItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
            }

            if (!bosLineItems.get(sno).getProduct().isService()) {
                if ((counter + 1) == productLimitInCounter) {
                    String strSNO = Integer.toString(sno + 1);

                    taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), normalcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), normalcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    int isEqual = bosLineItems.get(sno).getUqcOne().compareTo(bosLineItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), normalcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(strQuantity, normalcalibri));
                    }
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneRate()), normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    int isZero = bosLineItems.get(sno).getDiscountValue().compareTo(zero);
                    if (isZero == 0) {
                        taxcell = new PdfPCell(new Phrase("-", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getDiscountValue()), normalcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase("-" + String.valueOf(bosLineItems.get(sno).getDiscountValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    }

                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getValue()), normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);
                } else {
                    String strSNO = Integer.toString(sno + 1);

                    taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), normalcalibri));
                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), normalcalibri));
                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    int isEqual = bosLineItems.get(sno).getUqcOne().compareTo(bosLineItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), normalcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(strQuantity, normalcalibri));
                    }
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneRate()), normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    int isZero = bosLineItems.get(sno).getDiscountValue().compareTo(zero);
                    if (isZero == 0) {
                        taxcell = new PdfPCell(new Phrase("-", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getDiscountValue()), normalcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase("-" + String.valueOf(bosLineItems.get(sno).getDiscountValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    }

                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getValue()), normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);
                }
            } else {
                if ((counter + 1) == productLimitInCounter) {
                    String strSNO = Integer.toString(sno + 1);

                    taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), normalcalibri));
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), normalcalibri));
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);
                } else {

                    String strSNO = Integer.toString(sno + 1);

                    taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), normalcalibri));
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("", normalcalibri));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), normalcalibri));
                    taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);
                }
            }
            sno++;
        }
//        
        lineCount = (12 - lineCount);
        for (int i = 1; i < lineCount; i++) {
            taxcell = new PdfPCell(new Phrase(" ", normalcalibri));
            taxcell.setColspan(7);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            thirdTable.addCell(taxcell);
        }

        document.add(thirdTable);
        return lineCount;
    }

    private int afterAddProductItemsA5L(Document document) {
        int billchargeCount = 0;
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            Phrase p;
            PdfPCell taxcell;

            PdfPTable thirdTable = new PdfPTable(7);
            thirdTable.setWidthPercentage(100);
            thirdTable.setWidths(new float[]{5, 30, 11, 20, 10, 10, 18});

            if (billDiscount != 0) {
                taxcell = new PdfPCell(new Phrase("Bill Discount", boldcalibri));
                taxcell.setColspan(6);
                taxcell.setBorder(Rectangle.BOX);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), boldcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                taxcell.setBorder(Rectangle.BOX);
                thirdTable.addCell(taxcell);

            }

            if (billRoundOff != 0) {
                taxcell = new PdfPCell(new Phrase("Round Off", boldcalibri));
                taxcell.setBorder(Rectangle.BOX);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setColspan(6);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(billRoundOff), boldcalibri));
                taxcell.setBorder(Rectangle.BOX);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setColspan(6);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(billNetAmount), boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);
                billchargeCount++;
            } else {
                taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                taxcell.setBorder(Rectangle.BOX);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setColspan(6);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(billNetAmount), boldcalibri));
                taxcell.setBorder(Rectangle.BOX);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);
                billchargeCount++;
            }
            document.add(thirdTable);

        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
        return billchargeCount;
    }

    public void afterAddProductItemsContinuedA5L(Document document) {
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new float[]{70, 30});
            table.setTotalWidth(560);
            table.setWidthPercentage(100);

            PdfPCell cellfooter = null;

            cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.TOP | Rectangle.LEFT);
            table.addCell(cellfooter);

            for (int i = 0; i < 3; i++) {
                cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
                cellfooter.setColspan(2);
                cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellfooter.setBorder(Rectangle.NO_BORDER);
                cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                table.addCell(cellfooter);
            }

            cellfooter = new PdfPCell(new Phrase("Continued... ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            table.addCell(cellfooter);
            document.add(table);

        } catch (DocumentException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addFooterA5L(Document document) throws DocumentException, IOException {
        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        PdfPTable Outertable = new PdfPTable(3);
        Outertable.setWidths(new float[]{40, 30, 30});

        Outertable.setTotalWidth(560);
        Outertable.setWidthPercentage(100);

        PdfPTable table = new PdfPTable(1);
        PdfPCell cellfooter = null;

        cellfooter = new PdfPCell(new Phrase(companyFooterHeading, footerfontbold));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineOne, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineTwo, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineThree, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineFour, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        cellfooter = new PdfPCell(new Phrase(billRemarks, boldcalibri));
        cellfooter.setRowspan(5);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        Phrase p2 = new Phrase();
        p2.add(new Chunk("for ", cellFont1));
        p2.add(new Chunk(companyName.toUpperCase(), footerfontbold));
        cellfooter = new PdfPCell(p2);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(" ", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        document.add(Outertable);

    }

    public void lastLineA5L(Document document, int currentPage, int pageCount) throws DocumentException {
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            PdfPTable footerTable = new PdfPTable(1);
            footerTable.setWidthPercentage(100);

            Phrase p;
            PdfPCell cell;

            p = new Phrase(" ", headercalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);
            document.add(footerTable);

            footerTable = new PdfPTable(2);
            footerTable.setWidths(new float[]{70, 30});
            footerTable.setWidthPercentage(100);

            p = new Phrase(" ", normalcalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);

            p = new Phrase("Page " + currentPage + " of " + pageCount, normalcalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);

//        p = new Phrase("This is a Computer Generated Invoice", cellFont);
//        cell = new PdfPCell(p);
//        cell.setColspan(2);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setBorder(Rectangle.NO_BORDER);
//        footerTable.addCell(cell);
            document.add(footerTable);
        } catch (IOException ex) {
            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createPdfA5L(Boolean isPrint, Boolean isPreview) throws IOException, DocumentException {
        try {

            Rectangle layout = new Rectangle(PageSize.A5.rotate());
            Document document = new Document(layout, 20, 15, 15, 15);
            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Bill Of Supply" + "-" + newBos.getBosNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Bill Of Supply" + "-" + newBos.getBosNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                for (int copies = 0; copies < noOfCopies; copies++) {
                    sno = 0;
                    document.newPage();
                    String invoiceCopyName = null;
                    switch (copies) {
                        case 0:
                            invoiceCopyName = firstBillCopy;
                            break;
                        case 1:
                            invoiceCopyName = secondBillCopy;
                            break;
                        case 2:
                            invoiceCopyName = thirdBillCopy;
                            break;
                        case 3:
                            invoiceCopyName = fourthBillCopy;
                            break;
                        case 4:
                            invoiceCopyName = fifthBillCopy;
                            break;
                        default:
                            break;
                    }

                    int pagecount = 0;
                    System.err.println(totalProductCount + "^^^^^^^^^^^^^^^^^^^^^^ " + totalProductCount % productLimitA5L);
                    pagecount = (productcount / productLimitA5L);
                    if ((productcount % productLimitA5L) > 0) {
                        pagecount += 1;
                    }
                    
                    if (pagecount == 1) {
                        System.err.println(pagecount + "if (pagecount == 1) ^^^^^^^^^^^^^^^^^^^^^^ " + productcount);
                        addInfoA5L(document, invoiceCopyName);
                        addProductItemsA5L(document, (productcount % productLimitA5L));
                        afterAddProductItemsA5L(document);
                        addFooterA5L(document);
                        lastLineA5L(document, pagecount, pagecount);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (pagecount == i) {

                                System.err.println(pagecount + " if (pagecount == i)^^^^^^^^^^^^^^^^^^^^^^ " + totalProductCount % productLimitA5L);
                                addInfoA5L(document, invoiceCopyName);
                                addProductItemsA5L(document, (productcount % productLimitA5L));
                                afterAddProductItemsA5L(document);
                                addFooterA5L(document);
                                lastLineA5L(document, i, pagecount);
                            } else {
                                System.err.println(pagecount + " else^^^^^^^^^^^^^^^^^^^^^^ " + productLimitA5L);
                                addInfoA5L(document, invoiceCopyName);
                                addProductItemsA5L(document, productLimitA5L);
                                afterAddProductItemsContinuedA5L(document);
                                lastLineA5L(document, i, pagecount);

                                document.newPage();
                            }
                        }
                    }
                }
                document.close();
                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);

                    } catch (PrintException ex) {
                        Logger.getLogger(NonGSTBill.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);

                }
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(NonGSTBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(NonGSTBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(NonGSTBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createImagePdf(String filePath) throws IOException {
        try {
            String path = AppConstants.getTempDocPath();
            File pdfFile;

            pdfFile = new File(filePath);
            PDDocument doc = PDDocument.load(new File(filePath));
            int count = doc.getNumberOfPages();
            for (int i = 0; i < count; i++) {
                RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
                FileChannel channel = raf.getChannel();
                MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                PDFFile pdf = new PDFFile(buf);

                PDFPage page = pdf.getPage(i);

                java.awt.Rectangle rect = new java.awt.Rectangle(0, 0, (int) page.getBBox().getWidth(),
                        (int) page.getBBox().getHeight());
                BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
                        BufferedImage.TYPE_INT_RGB);

                System.err.println(rect.width + "---" + rect.height);

                Image image = page.getImage(rect.width, rect.height, // width & height
                        rect, // clip rect
                        null, // null for the ImageObserver
                        true, // fill background with white
                        true // block until drawing is done
                );
                Graphics2D bufImageGraphics = bufferedImage.createGraphics();
                bufImageGraphics.drawImage(image, 0, 0, null);
                ImageIO.write(bufferedImage, "PNG", new File(path + "bill" + i + ".png"));
                raf.close();
            }
            random++;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(NonGSTBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createPdfA7(Boolean isPrint, Boolean isPreview) throws FileNotFoundException, DocumentException {
        try {
            //
            //String filename = "C:\\Gst\\temp\\" + billNo + ".pdf";
            //

            //Document document = new Document(PageSize.A7);
            Rectangle layout = new Rectangle(PageSize.A7);
            Document document = new Document(layout, 15, 15, 15, 15);

            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Bill Of Supply" + "-" + newBos.getBosNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();

                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        try {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (InstantiationException ex) {
                            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (UnsupportedLookAndFeelException ex) {
                            Logger.getLogger(NonGSTBill.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Bill Of Supply" + "-" + newBos.getBosNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();

                Phrase p;

                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell = null;

                

                //String name = "ARASAN BAKERY";
                taxcell = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);
                ///document.add(taxTable);
                //
                if (!companyA7AddrLine1.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine1, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine2.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine3.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else {
                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                }

//                taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
//                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                taxcell.setBorder(Rectangle.NO_BORDER);
//                taxTable.addCell(taxcell);
                //document.add(taxTable);
                taxcell = new PdfPCell(new Phrase("State : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

//                if (!companyPhone.equals("")) {
//                    taxcell = new PdfPCell(new Phrase("Phone : " + companyPhone, cellFont));
//                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    taxcell.setBorder(Rectangle.NO_BORDER);
//                    taxTable.addCell(taxcell);
//                }
//                if (!companyMobile.equals("")) {
//                    taxcell = new PdfPCell(new Phrase("Mobile : " + companyMobile, cellFont));
//                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    taxcell.setBorder(Rectangle.NO_BORDER);
//                    taxTable.addCell(taxcell);
//                }
//                if (!companyEmail.equals("")) {
//                    taxcell = new PdfPCell(new Phrase("Email : " + companyEmail, cellFont));
//                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    taxcell.setBorder(Rectangle.NO_BORDER);
//                    taxTable.addCell(taxcell);
//                }
                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);
                //document.add(taxTable);

                taxcell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                document.add(taxTable);

                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                tablecell = new PdfPCell(new Phrase("No: " + billNo, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table2.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billDate, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                table2.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase("User: " + companyName, reportdateFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                tablecell.setBorder(Rectangle.BOTTOM);
//                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
//                table2.addCell(tablecell);
//
//                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//                tablecell = new PdfPCell(new Phrase("Time: " + sdf.format(new Date()), reportdateFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                tablecell.setBorder(Rectangle.BOTTOM);
//                // tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                table2.addCell(tablecell);
                document.add(table2);

                PdfPTable table3 = new PdfPTable(5);
                table3.setWidthPercentage(100);
                table3.setWidths(new float[]{7, 40, 20, 14, 25});
                PdfPCell cell3 = null;

                cell3 = new PdfPCell(new Phrase(" ", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Description", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

//                cell3 = new PdfPCell(new Phrase("HSN", reportdateFont));
//                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell3.setBorder(Rectangle.NO_BORDER);
//                cell3.setBorder(Rectangle.BOTTOM);
//                //tablecell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                table3.addCell(cell3);
                cell3 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Qty", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                for (int count = 0; count < bosLineItems.size(); count++) {

                    String strSNO = Integer.toString(sno + 1);
                    if (!bosLineItems.get(sno).getProduct().isService()) {

                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

//                    cell3 = new PdfPCell(new Phrase(bosLineItems.get(sno).getHsnSac(), cellFont));
//                    cell3.setBorder(Rectangle.NO_BORDER);
//                    cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                    table3.addCell(cell3);
                        int UQC2Value = 0;
                        for (Product product : products) {
                            if (product.getName().equals(bosLineItems.get(sno).getProductName())) {
                                UQC2Value = product.getUQC2Value();
                            }
                        }
                        int UQC1qty = bosLineItems.get(sno).getUqcOneQuantity(), UQC2qty = bosLineItems.get(sno).getUqcTwoQuantity();
                        String strQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                        if (UQC1qty > 0) {
                            strQuantity = UQC1qty + " " + getUQCcode(bosLineItems.get(sno).getUqcOne());
                            strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                            if (UQC2qty > 0) {
                                strQuantity = strQuantity + ", ";
                                strAltQuantity = strAltQuantity + ", ";
                            }
                        }
                        if (UQC2qty > 0) {
                            if (UQC1qty == 0) {
                                strQuantity = "";
                                strAltQuantity = "";
                            }
                            strQuantity = strQuantity + UQC2qty + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                            strAltQuantity = strAltQuantity + bosLineItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(bosLineItems.get(sno).getUqcTwo());
                        }

                        cell3 = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getUqcOneRate()), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);
                        Phrase phrase = new Phrase();
                        int isEqual = bosLineItems.get(sno).getUqcOne().compareTo(bosLineItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            cell3 = new PdfPCell(new Phrase(String.valueOf(bosLineItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else {
                            phrase.add(new Chunk(strQuantity, cellFont));
                            phrase.add(new Chunk(Chunk.NEWLINE + strAltQuantity, cellFont));
                            cell3 = new PdfPCell(phrase);
                        }

                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                    } else {
                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(bosLineItems.get(sno).getProductName(), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(currency.format(bosLineItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);
                    }
                    sno++;
                }
//                int countCharge = 0;
//                for (countCharge = 0; countCharge < bosChargeItems.size(); countCharge++) {
//                    cell3 = new PdfPCell(new Phrase(bosChargeItems.get(countCharge).getBillChargeName(), cellFont));
//                    cell3.setColspan(4);
//                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
//                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                    table3.addCell(cell3);
//
//                    if (bosChargeItems.get(countCharge).getValue().compareTo(zero) > 0) {
//                        cell3 = new PdfPCell(new Phrase("+" + currency.format(bosChargeItems.get(countCharge).getValue()), boldFont));
//                    } else {
//                        cell3 = new PdfPCell(new Phrase(currency.format(bosChargeItems.get(countCharge).getValue()), boldFont));
//                    }
//                    
//                    cell3.setBorder(Rectangle.NO_BORDER);
//                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
//                    table3.addCell(cell3);
//                }
                if (billDiscount != 0) {
                    cell3 = new PdfPCell(new Phrase("Discount", cellFont));
                    cell3.setColspan(4);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(cell3);

                    cell3 = new PdfPCell(new Phrase(currency.format(billDiscount), boldFont));
                    cell3.setBorder(Rectangle.NO_BORDER);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);

                }
                if (billRoundOff != 0) {
                    cell3 = new PdfPCell(new Phrase("Round off", cellFont));
                    cell3.setColspan(4);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(cell3);

                    
                    if (billRoundOff > 0) {
                        cell3 = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), boldFont));
                    } else {
                        cell3 = new PdfPCell(new Phrase(currency.format(billRoundOff), boldFont));
                    }
                    cell3.setBorder(Rectangle.NO_BORDER);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);
                }

                cell3 = new PdfPCell(new Phrase("Total Amount ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(billNetAmount), boldFont));
                
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);
                document.add(table3);

                document.close();

                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);

                    } catch (PrintException ex) {
                        Logger.getLogger(NonGSTBill.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);

                }
            }

        } catch (IOException | DocumentException ex) {
            Logger.getLogger(A7Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }
}
