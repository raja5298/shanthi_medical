/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import india.abs.gstonepro.api.models.BalanceSheet;
import india.abs.gstonepro.api.models.Company;
import india.abs.gstonepro.api.models.Ledger;
import india.abs.gstonepro.api.models.LedgerGroup;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.LedgerGroupLogic;
import india.abs.gstonepro.business.LedgerLogic;
import india.abs.gstonepro.ui.utils.StateCode;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ABS
 */
public class BalanceSheetPdf {

    Company company = SessionDataUtil.getSelectedCompany();
    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    Set<Ledger> Banks = new LedgerLogic().fetchAllBankLedgers(company.getCompanyId());
    Set<Ledger> partyLedger = new LedgerLogic().fetchAllPartyLedgers(companyId);
    List<LedgerGroup> ledgerGroups = new LedgerGroupLogic().fetchAllLedgerGroups();
    HashMap<LedgerGroup, List<Ledger>> selectedLedgersInc = null;
    HashMap<LedgerGroup, List<Ledger>> selectedLedgersExp = null;

    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";

    public String companyName = "",
            companyAddr1 = "",
            companyAddr2 = "",
            companyAddr3 = "",
            companyCity = "",
            companyDistrict = "",
            companyEmail = "",
            companyState = "",
            companyMobile = "",
            companyPhone = "",
            companyGSTIN = "";
    DecimalFormat currency = new DecimalFormat("##,##,##0.000");

    boolean isLedGrpAdded = false;
    int nPageLimit = 40;
    HashMap<Long, String> ledgerTobeAdd = new HashMap<Long, String>();
    int nAsstCnt = 0;
    int nLibCnt = 0;
    int assetsLineCount = 0, liabilitiesLineCount = 0;

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
    com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
    com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    public void createPdf(List<BalanceSheet> bsLedgers) throws FileNotFoundException, DocumentException {
        try {
            OutputStream file = new FileOutputStream(new File("C:\\GST\\temp\\bill.pdf"));
            Rectangle layout = new Rectangle(PageSize.A4);
            Document document = new Document(layout, 15, 15, 15, 15);
            PdfWriter.getInstance(document, file);
            document.open();
            Phrase p;

            PdfPTable taxTable = new PdfPTable(1);
            taxTable.setWidthPercentage(100);
            PdfPCell taxcell = null;

            companyName = SessionDataUtil.getSelectedCompany().getCompanyName();
            String A4AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineOne();
            String A4AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineTwo();
            String A4AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineThree();
            if (A4AddressLine1 != null) {
                companyAddr1 = A4AddressLine1.replaceAll("/~/", ",");
            }
            if (A4AddressLine2 != null) {
                companyAddr2 = A4AddressLine2.replaceAll("/~/", ",");
            }
            if (A4AddressLine3 != null) {
                companyAddr3 = A4AddressLine3.replaceAll("/~/", ",");
            }

            if (SessionDataUtil.getSelectedCompany().getCity() != null) {
                companyCity = SessionDataUtil.getSelectedCompany().getCity();
            }
            if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
                companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
            }
            if (SessionDataUtil.getSelectedCompany().getState() != null) {
                companyState = SessionDataUtil.getSelectedCompany().getState();
            }
            if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
                companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
            }
            if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
                companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
            }
            if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
                companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
            }
            if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
                companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
            }
            taxcell = new PdfPCell(new Phrase(companyName, boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyAddr1 + companyAddr2 + companyAddr3, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("State : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            if (!companyPhone.equals("")) {
                taxcell = new PdfPCell(new Phrase("Phone : " + companyPhone, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyMobile.equals("")) {
                taxcell = new PdfPCell(new Phrase("Mobile : " + companyMobile, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }
            if (!companyEmail.equals("")) {
                taxcell = new PdfPCell(new Phrase("Email : " + companyEmail, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Balance Sheet", boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            taxTable.addCell(taxcell);

            document.add(taxTable);
            setTableData(bsLedgers, document);
            document.close();

            if (Desktop.isDesktopSupported()) {
                try {
                    File myFile = new File("C:\\GST\\temp\\bill.pdf");
                    Desktop.getDesktop().open(myFile);
                } catch (IOException ex) {
                    // no application registered for PDFs
                }
            }

        } catch (IOException | DocumentException ex) {
            Logger.getLogger(A7Bill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void setTableHeader(PdfPTable table3, String header) {
        PdfPCell cell3 = null;
        cell3 = new PdfPCell(new Phrase(header, reportdateFont));
        cell3.setColspan(2);
        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell3.setBorder(Rectangle.NO_BORDER);
        cell3.setBorder(Rectangle.BOTTOM);
        table3.addCell(cell3);

        cell3 = new PdfPCell(new Phrase(" ", reportdateFont));
        cell3.setColspan(2);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setBorder(Rectangle.NO_BORDER);
        table3.addCell(cell3);
        table3.setHeaderRows(1);

    }

    public void setTableData(List<BalanceSheet> bsLedger, Document document) throws DocumentException {
        try {

            PdfPTable outerTable1 = new PdfPTable(2);
            outerTable1.setWidthPercentage(100);
            outerTable1.setWidths(new float[]{50, 50});

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidths(new float[]{40, 10});
            PdfPCell cell3 = null;
            setTableHeader(table3, "ASSETS");

            HashMap<Ledger, BigDecimal> transAmtMap = new HashMap<>();
            HashMap<LedgerGroup, List<Ledger>> ledgerWithGrpInc = new HashMap<>();
            HashMap<LedgerGroup, List<Ledger>> ledgerWithGrpExp = new HashMap<>();
            for (BalanceSheet bs : bsLedger) {
                transAmtMap.put(bs.getLedger(), bs.getTransactionAmount());
                if (bs.getType().equalsIgnoreCase("INCOME")) {
                    if (ledgerWithGrpInc.get(bs.getLedger().getLedgerGroup()) != null) {
                        List<Ledger> ledTobeAdd = ledgerWithGrpInc.get(bs.getLedger().getLedgerGroup());
                        ledTobeAdd.add(bs.getLedger());
                        ledgerWithGrpInc.put(bs.getLedger().getLedgerGroup(), ledTobeAdd);
                    } else {
                        List<Ledger> ledTobeAdd = new ArrayList<>();
                        ledTobeAdd.add(bs.getLedger());
                        ledgerWithGrpInc.put(bs.getLedger().getLedgerGroup(), ledTobeAdd);
                    }
                } else {
                    if (ledgerWithGrpExp.get(bs.getLedger().getLedgerGroup()) != null) {
                        List<Ledger> ledTobeAdd = ledgerWithGrpExp.get(bs.getLedger().getLedgerGroup());
                        ledTobeAdd.add(bs.getLedger());
                        ledgerWithGrpExp.put(bs.getLedger().getLedgerGroup(), ledTobeAdd);
                    } else {
                        List<Ledger> ledTobeAdd = new ArrayList<>();
                        ledTobeAdd.add(bs.getLedger());
                        ledgerWithGrpExp.put(bs.getLedger().getLedgerGroup(), ledTobeAdd);
                    }
                }

            }

            for (Map.Entry<LedgerGroup, List<Ledger>> entry : ledgerWithGrpInc.entrySet()) {
                LedgerGroup lg = entry.getKey();
                List<Ledger> ledTobeAdd = entry.getValue();
                BigDecimal sum = new BigDecimal(0);
                if (ledTobeAdd.size() > 0) {
                    assetsLineCount += (ledTobeAdd.size() + 2);
                    String space = "";
                    String space2 = "       ";
                    if (lg.getParentLedgerGroup() != null) {
                        cell3 = new PdfPCell(new Phrase(lg.getParentLedgerGroup().getLedgerGroupName(), reportdateFont));
                        cell3.setColspan(2);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell3.setBorder(Rectangle.NO_BORDER);
                        table3.addCell(cell3);
                        space = "       ";
                        space2 = "              ";
                        assetsLineCount++;
                    }
                    cell3 = new PdfPCell(new Phrase(space + lg.getLedgerGroupName(), reportdateFont));
                    cell3.setColspan(2);
                    cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell3.setBorder(Rectangle.NO_BORDER);
                    table3.addCell(cell3);
                    for (Ledger led : ledTobeAdd) {
                        cell3 = new PdfPCell(new Phrase(space2 + led.getLedgerName(), cellFont));
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell3.setBorder(Rectangle.NO_BORDER);
                        table3.addCell(cell3);

                        BigDecimal balance = transAmtMap.get(led);
                        if (balance == null) {
                            balance = new BigDecimal(0);
                        }
                        cell3 = new PdfPCell(new Phrase(balance.toString(), cellFont));
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell3.setBorder(Rectangle.NO_BORDER);
                        table3.addCell(cell3);
                        sum = sum.add(balance);
                    }
                    cell3 = new PdfPCell(new Phrase(space2 + "Total " + lg.getLedgerGroupName() + " Assets", reportdateFont));
                    cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell3.setBorder(Rectangle.NO_BORDER);
                    table3.addCell(cell3);

                    cell3 = new PdfPCell(new Phrase(String.valueOf(sum), reportdateFont));
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);
                }
            }

            PdfPTable table4 = new PdfPTable(2);
            table4.setWidths(new float[]{40, 10});
            PdfPCell cell4 = null;
            table4.setWidths(new float[]{40, 10});
            cell4 = null;
            setTableHeader(table4, "LIABILITIES");
            for (Map.Entry<LedgerGroup, List<Ledger>> entry : ledgerWithGrpExp.entrySet()) {
                LedgerGroup lg = entry.getKey();
                List<Ledger> ledTobeAdd = entry.getValue();
                liabilitiesLineCount += (ledTobeAdd.size() + 2);
                String space = "";
                String space2 = "       ";
                if (lg.getParentLedgerGroup() != null) {
                    cell4 = new PdfPCell(new Phrase(lg.getParentLedgerGroup().getLedgerGroupName(), reportdateFont));
                    cell4.setColspan(2);
                    cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell4.setBorder(Rectangle.NO_BORDER);
                    table4.addCell(cell4);
                    space = "       ";
                    space2 = "              ";
                    liabilitiesLineCount++;
                }
                cell4 = new PdfPCell(new Phrase(space + lg.getLedgerGroupName(), reportdateFont));
                cell4.setColspan(2);
                cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell4.setBorder(Rectangle.NO_BORDER);
                table4.addCell(cell4);
                BigDecimal sum = new BigDecimal(0);

                for (Ledger led : ledTobeAdd) {
                    cell4 = new PdfPCell(new Phrase(space2 + led.getLedgerName(), cellFont));
                    cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell4.setBorder(Rectangle.NO_BORDER);
                    table4.addCell(cell4);

                    BigDecimal balance = transAmtMap.get(led);
                    if (balance == null) {
                        balance = new BigDecimal(0);
                    }
                    cell4 = new PdfPCell(new Phrase(balance.toString(), cellFont));
                    cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell4.setBorder(Rectangle.NO_BORDER);
                    table4.addCell(cell4);
                    sum = sum.add(balance);
                }
                cell4 = new PdfPCell(new Phrase(space2 + "Total " + lg.getLedgerGroupName() + " Liablities ", reportdateFont));
                cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell4.setBorder(Rectangle.NO_BORDER);
                table4.addCell(cell4);

                cell4 = new PdfPCell(new Phrase(String.valueOf(sum), reportdateFont));
                cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell4.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                table4.addCell(cell4);

            }
            int remaining = 0;
            PdfPCell cell = null;
            PdfPTable table = null;
            if (assetsLineCount > liabilitiesLineCount) {
                remaining = assetsLineCount - liabilitiesLineCount;
                cell = cell4;
                table = table4;
            } else {
                remaining = liabilitiesLineCount - assetsLineCount;
                cell = cell3;
                table = table3;
            }

            for (int i = 0; i < remaining; i++) {
                cell = new PdfPCell(new Phrase(" ", reportdateFont));
                cell.setColspan(2);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell);
            }
            outerTable1.addCell(table3);
            outerTable1.addCell(table4);
            document.add(outerTable1);
        } catch (DocumentException ex) {
            Logger.getLogger(BalanceSheetPdf.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
