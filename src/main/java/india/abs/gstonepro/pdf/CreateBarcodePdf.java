/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

/**
 *
 * @author Alpha
 */
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

//import sandbox.WrapToTest;
//@WrapToTest
public class CreateBarcodePdf {

    public static final String DEST = "./results/tables/barcode_table.pdf";

    public static void main(String[] args) {
//        File file = new File(DEST);
//        file.getParentFile().mkdirs();
    }

    public void createPdf(String dest, int barcode, int count) {
        try {
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
            document.open();
            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100);
            int remainvalue = count % 4;
            for (int i = 0; i < count; i++) {
                table.addCell(createBarcode(writer, String.format("%08d", barcode)));
            }
            for(int j=0;j<remainvalue;j++){
                table.addCell("");
            }
            document.add(table);
            document.close();

            if (Desktop.isDesktopSupported()) {
                try {
                    File myFile = new File(dest);
                    Desktop.getDesktop().open(myFile);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Document not to be Opened");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Excel File Download At " + dest);
            }
        } catch (DocumentException ex) {
            Logger.getLogger(CreateBarcodePdf.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreateBarcodePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static PdfPCell createBarcode(PdfWriter writer, String code) throws DocumentException, IOException {
        System.out.println("code " + code);
        BarcodeEAN barcode = new BarcodeEAN();
        barcode.setCodeType(Barcode.EAN8);
        barcode.setCode(code);
        PdfPCell cell = new PdfPCell(barcode.createImageWithBarcode(writer.getDirectContent(), BaseColor.BLACK, BaseColor.GRAY), true);
        cell.setPadding(10);
        return cell;
    }
}
