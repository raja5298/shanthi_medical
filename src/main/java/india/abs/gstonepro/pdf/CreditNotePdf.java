/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import india.abs.gstonepro.api.models.CreditDebitNote;
import india.abs.gstonepro.api.models.CreditDebitNoteLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.IndianCurrencyUtil;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.CreditDebitNoteLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.ui.utils.TaxBillItems;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author user
 */
public class CreditNotePdf {

    public class FooterTable extends PdfPageEventHelper {

        protected PdfPTable footer;

        public FooterTable(PdfPTable footer) {
            this.footer = footer;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            footer.writeSelectedRows(0, 5, 20, 80, writer.getDirectContent());
        }
    }
    static int totalProductCount = 0, productcount, hsncount, hsncountNon;

    int productLimitA4 = 20, productLimitA5L = 11, productLimitA5P = 8;
    static int sno = 0, hsnno = 0, hsnnoA4 = 0;

    int random = 0;
    int pdfA5LTotalDescCnt = 0;
    BigDecimal zero = new BigDecimal("0.000");
    String strNoteType = "";

    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";
//    public String companyName = "", companyLine1 = "", companyLine2 = "", companyLine3 = "", companyLine4 = "",
//            companyDistrict = "", CompanyState = "", companyGSTIN = "";

    public String companyName = "", companyA4Addr = "", companyA5LAddr = "", companyA5PAddr = "",
            companyA7Addr = "", companyCity = "", companyDistrict = "", companyEmail = "",
            companyState = "", companyMobile = "", companyPhone = "", companyGSTIN = "", invoiceHeading = "",
            companyFooterHeading = "", companyFooterLineOne = "", companyFooterLineTwo = "", companyFooterLineThree = "",
            companyFooterLineFour = "", companyA4AddrLine1 = "", companyA4AddrLine2 = "", companyA4AddrLine3 = "", companyA5LAddrLine1 = "", companyA5LAddrLine2 = "",
            companyA5LAddrLine3 = "", companyA5PAddrLine1 = "", companyA5PAddrLine2 = "", companyA5PAddrLine3 = "", companyA7AddrLine1 = "", companyA7AddrLine2 = "",
            companyA7AddrLine3 = "";

    public String billNo = " ", billDate = " ", billCompanyName = " ", billCompanyAddr1 = " ", billCompanyAddr2 = " ",
            billCompanyAddr3 = " ", billCompanyCity = " ", billCompanyDistrict = " ",
            billCompanyState = " ", billCompanyMobile = " ", billCompanyPhone = " ", billCompanyEmail = " ", billCompanyGSTIN = " ", billPayment = " ",
            billBuyingOrderNo = " ", billOrderDate = " ", billDispatchThorugh = " ", billDestination = " ", billTermOfDelivery = " ", billThrough = " ",
            billReferenceNo = " ", billRemarks = " ", firstBillCopy = " ", secondBillCopy = " ", thirdBillCopy = " ", fourthBillCopy = " ", fifthBillCopy = " ";
    public String ShippingName = " ", ShippingAddr1 = " ", ShippingAddr2 = " ", ShippingAddr3 = " ", ShippingCity = " ", ShippingPincode = " ", ShippingDistrict = " ", ShippingState = " ",
            ShippingMobile = " ", ShippingPhone = " ", ShippingGSTIN = " ";
    public boolean isIGST = false, billIsSameAddress = true, isBill = false, isPdfOnly = false;
    boolean pageEnd = false, descEnd = false, productAdded = false;
    public int billId = 0, noOfCopies;
    public BigDecimal billNetAmount = new BigDecimal(0);

//    public float billIGST = 0, billTotalTax = 0, billFlightAmount = 0, billCGST = 0, billSGST = 0, billTotal = 0, billDiscountPer = 0, billDiscount = 0, billRoundOff = 0, billCessamount = 0;
    DecimalFormat currency = new DecimalFormat("##,##,##0.000");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    int pdfDescCount = 0, pdfDescPrintCnt = 0;

    public BigDecimal billDiscount = new BigDecimal(0);
    public BigDecimal billRoundOff = new BigDecimal(0);
    public BigDecimal billIGST = new BigDecimal(0);
    public BigDecimal billCGST = new BigDecimal(0);
    public BigDecimal billSGST = new BigDecimal(0);
    public BigDecimal billTotal = new BigDecimal(0);
    public BigDecimal billTotalTax = new BigDecimal(0);
    public float billFlightAmount = 0, billDiscountPer = 0, billCessamount = 0;
//    DecimalFormat currency = new DecimalFormat("##,##,##0.00");

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<CreditDebitNoteLineItem> crItems = new ArrayList<>();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
//    List<TaxBillItems> taxBillItems = new ArrayList<>();
    ArrayList<String> productDescriptions = null;
    HashMap<Long, ArrayList<String>> productDescA5P = null;
    CreditDebitNote newCR = new CreditDebitNote();
    List<String> nameOfPDFCopies = new ArrayList<String>();

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont8 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font boldFont9 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1Bold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 6, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont7 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

    com.itextpdf.text.Font footerboldfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

    public void pdfByBillId(Boolean isPrint, Boolean isPreview, String billId) {

        try {
            CreditDebitNote CR = new CreditDebitNoteLogic().fetchCDNote(billId);
            List<CreditDebitNoteLineItem> crLIs = CR.getCdLis();
//            List<PurchaseSaleTaxSummary> psTSs = PS.getPsTaxSummaries();

            printPdf(isPrint, isPreview, CR, crLIs);
        } catch (IOException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printPdf(Boolean isPrint, Boolean isPreview, CreditDebitNote CR, List<CreditDebitNoteLineItem> Items) throws IOException {
        sno = 0;
        hsnnoA4 = 0;
        hsnno = 0;
        newCR = CR;
        crItems = Items;
        productcount = crItems.size();
//            hsncount = psTaxSummaries.size();
//            getBillItems();
        setCompany();
        setBillDetails();
        String paperSize = SessionDataUtil.getCompanyPolicyData().getPrintPaperSize();
        createPdfA4(isPrint, isPreview);
    }

    public String getUQCcode(String value) {
        String strUQCcode = "";
        for (UQC uqc : UQCs) {
            if (uqc.getQuantityName().equals(value)) {
                strUQCcode = uqc.getUQCCode();
                break;
            }
        }
        return strUQCcode;
    }

    public void setBillDetails() {
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
//        isIGST = newCR.isIsIGST();
        billNo = newCR.getCdNoteNo();
        strNoteType = newCR.getNoteType();
        billDate = sdfr.format(newCR.getNoteDate());
        if (newCR.getLedger() == null) { //|| !newPS.isNoParty()) {
//            if (!newCR.isNoParty()) {
//                billCompanyName = newCR.getNoPartyName().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                billCompanyAddr1 = newPS.getNoPartyLineOne().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                billCompanyCity = newPS.getNoPartyCity().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                billCompanyDistrict = newPS.getNoPartyDistrict().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                billCompanyState = newPS.getNoPartyState().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//                billCompanyGSTIN = newPS.getNoPartyGSTIN().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
//            } else {
            billCompanyName = "";
            billCompanyAddr1 = "";
            billCompanyCity = "";
            billCompanyDistrict = "";
            billCompanyState = "";
            billCompanyMobile = "";
            billCompanyPhone = "";
            billCompanyGSTIN = "";
//            }

        } else {

            billCompanyName = newCR.getLedger().getLedgerName();

            String Address = newCR.getLedger().getAddress();
            if (Address != null) {
                billCompanyAddr1 = Address.replaceAll("/~/", ",");
            }
            String[] words = Address.split("/~/");

            if (words.length == 1) {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = "";
                billCompanyAddr3 = "";
            } else if (words.length == 2) {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = words[1];
                billCompanyAddr3 = "";
            } else {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = words[1];
                billCompanyAddr3 = words[2];
            }

//        billCompanyAddr2 = "";
//        billCompanyAddr3 = "";
            if (newCR.getLedger().getCity() != null) {
                billCompanyCity = newCR.getLedger().getCity();
            }
            if (newCR.getLedger().getDistrict() != null) {
                billCompanyDistrict = newCR.getLedger().getDistrict();
            }
            if (newCR.getLedger().getState() != null) {
                billCompanyState = newCR.getLedger().getState();
            }
            if (newCR.getLedger().getMobile() != null) {
                billCompanyMobile = newCR.getLedger().getMobile();
            }
            if (newCR.getLedger().getEmail() != null) {
                billCompanyEmail = newCR.getLedger().getEmail();
            }
            if (newCR.getLedger().getPhone() != null) {
                billCompanyPhone = newCR.getLedger().getPhone();
            }
            if (newCR.getLedger().getGSTIN() != null) {
                billCompanyGSTIN = newCR.getLedger().getGSTIN();
            }
        }

        billTotal = newCR.getNoteValueInclusiveOfTax();
        if (isIGST) {
            billTotalTax = newCR.getIgstValue();
        } else {
            billTotalTax = newCR.getCgstValue().add(newCR.getSgstValue());
        }
        billNetAmount = newCR.getNoteValueInclusiveOfTax();

        billCGST = newCR.getCgstValue();
        billSGST = newCR.getSgstValue();
        billIGST = newCR.getIgstValue();
        billTotal = newCR.getNoteValueInclusiveOfTax();
        billNetAmount = newCR.getNoteValueInclusiveOfTax();
//        isIGST = newCR.isIsIGST();
//        billRoundOff = newCR.getRoundOffValue();
//        billReferenceNo = newPS.getReferenceNumber();
//        billRemarks = newPS.getRemarks();

//        }
//        noOfCopies = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
////        noOfCopies = newPS.getCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
//        firstBillCopy = newCR.getCompany().getCompanyPolicy().getFirstInvoiceWord();
//        secondBillCopy = newCR.getCompany().getCompanyPolicy().getSecondInvoiceWord();
//        thirdBillCopy = newCR.getCompany().getCompanyPolicy().getThirdInvoiceWord();
//        fourthBillCopy = newCR.getCompany().getCompanyPolicy().getFourthInvoiceWord();
//        fifthBillCopy = newCR.getCompany().getCompanyPolicy().getFifthInvoiceWord();
//        }
    }

    public void setCompany() {
        companyName = SessionDataUtil.getSelectedCompany().getCompanyName();

        String A4AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineOne();
        String A4AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineTwo();
        String A4AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineThree();
        if (A4AddressLine1 != null) {
            companyA4AddrLine1 = A4AddressLine1.replaceAll("/~/", ",");
        }
        if (A4AddressLine2 != null) {
            companyA4AddrLine2 = A4AddressLine2.replaceAll("/~/", ",");
        }
        if (A4AddressLine3 != null) {
            companyA4AddrLine3 = A4AddressLine3.replaceAll("/~/", ",");
        }
        String A5PAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineOne();
        String A5PAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineTwo();
        String A5PAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineThree();
        if (A5PAddressLine1 != null) {
            companyA5PAddrLine1 = A5PAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine2 != null) {
            companyA5PAddrLine2 = A5PAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine3 != null) {
            companyA5PAddrLine3 = A5PAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A5LAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineOne();
        String A5LAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineTwo();
        String A5LAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineThree();
        if (A5LAddressLine1 != null) {
            companyA5LAddrLine1 = A5LAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine2 != null) {
            companyA5LAddrLine2 = A5LAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine3 != null) {
            companyA5LAddrLine3 = A5LAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A7AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineOne();
        String A7AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineTwo();
        String A7AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineThree();
        if (A7AddressLine1 != null) {
            companyA7AddrLine1 = A7AddressLine1.replaceAll("/~/", ",");
        }
        if (A7AddressLine2 != null) {
            companyA7AddrLine2 = A7AddressLine2.replaceAll("/~/", ",");
        }
        if (A7AddressLine3 != null) {
            companyA7AddrLine3 = A7AddressLine3.replaceAll("/~/", ",");
        }
        if (SessionDataUtil.getSelectedCompany().getCity() != null) {
            companyCity = SessionDataUtil.getSelectedCompany().getCity();
        }
        if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
            companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
        }
        if (SessionDataUtil.getSelectedCompany().getState() != null) {
            companyState = SessionDataUtil.getSelectedCompany().getState();
        }
        if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
            companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
        }
        if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
            companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
        }
        if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
            companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
        }
        if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
            companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getTaxInvoiceWord() != null) {
            invoiceHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getTaxInvoiceWord();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading() != null) {
            companyFooterHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne() != null) {
            companyFooterLineOne = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo() != null) {
            companyFooterLineTwo = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree() != null) {
            companyFooterLineThree = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour() != null) {
            companyFooterLineFour = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour();
        }

    }

    public void addInfo(Document document, int invoiceno) {
        try {

            PdfPTable taxTable = new PdfPTable(2);
            taxTable.setWidthPercentage(100);

            taxTable.setWidths(new float[]{55, 45});
            PdfPCell taxcell;

            Phrase p = new Phrase("", boldFont);
            taxcell = new PdfPCell(p);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);

            Phrase p1 = new Phrase(strNoteType, cellFont1);
            taxcell = new PdfPCell(p1);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);
            document.add(taxTable);

            PdfPCell cell;

            PdfPTable Table1 = new PdfPTable(2);
            Table1.setWidthPercentage(100);
            PdfPCell cell1 = null;

            cell1 = new PdfPCell(new Phrase("Invoice No : " + billNo, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Date : " + billDate, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            document.add(Table1);

            taxTable = new PdfPTable(1);
            taxTable.setWidthPercentage(100);
            taxcell = null;

            taxcell = new PdfPCell(new Phrase(companyName, boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);
            companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
            if (!companyA4Addr.equals("")) {
                taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }

            Phrase phrase = new Phrase();
            phrase.add(new Chunk("City : " + companyCity, cellFont));
            phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
            taxcell = new PdfPCell(phrase);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            phrase = new Phrase();
            if (!companyPhone.equals("")) {
                phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            if (!companyMobile.equals("")) {
                phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            if (!companyEmail.equals("")) {
                phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            document.add(taxTable);

            PdfPTable Table = new PdfPTable(1);
            Table.setWidthPercentage(100);
            cell = null;

            cell = new PdfPCell(new Phrase(strNoteType, boldFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            Table.addCell(cell);

            document.add(Table);

            if (newCR.getLedger() != null) {
                PdfPTable innerTable = new PdfPTable(2);
                innerTable.setWidthPercentage(100);
                PdfPCell tablecell = null;

                tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.LEFT | Rectangle.TOP);
                innerTable.addCell(tablecell);

                if (!billCompanyGSTIN.equalsIgnoreCase("")) {
                    tablecell = new PdfPCell(new Phrase("GSTIN/UIN : " + billCompanyGSTIN, cellFont));
                    tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    tablecell.setBorder(Rectangle.RIGHT | Rectangle.TOP);
                    innerTable.addCell(tablecell);
                }

                tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.LEFT);
                innerTable.addCell(tablecell);

                if (!billCompanyState.equalsIgnoreCase("")) {
                    tablecell = new PdfPCell(new Phrase("State : " + billCompanyState, cellFont));
                    tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    tablecell.setBorder(Rectangle.RIGHT);
                    innerTable.addCell(tablecell);
                }

                tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.LEFT);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(" ", cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablecell.setBorder(Rectangle.RIGHT);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(" ", cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablecell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                innerTable.addCell(tablecell);

                document.add(innerTable);
            }

        } catch (DocumentException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProductItems(Document document, int productLimitInCounter) throws IOException {

        try {

            SimpleDateFormat sft = new SimpleDateFormat("dd-MM-yyyy");
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font productFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font qtyFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

            PdfPCell taxcell, cell;
            PdfPTable pdfTable = new PdfPTable(8);
            pdfTable.setWidths(new float[]{7, 10, 15, 20, 6, 24, 41, 13});
            pdfTable.setTotalWidth(555);
            pdfTable.setLockedWidth(true);

            float ht = pdfTable.getTotalHeight();

            taxcell = new PdfPCell(new Phrase("S.No", totalFont8));
            //taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Inv No", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Invoice Date", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("POS", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Line No", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Reason", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Product", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);
//
////            taxcell = new PdfPCell(new Phrase("Quantity", totalFont8));
////            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
////            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
////            pdfTable.addCell(taxcell);
//            taxcell = new PdfPCell(new Phrase("GST %", totalFont8));
//            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
//            pdfTable.addCell(taxcell);

            Phrase phrase2 = new Phrase();
            phrase2.add(new Chunk("Amount", totalFont8));
            phrase2.add(new Chunk(Chunk.NEWLINE + "(Incl of Tax)", qtyFont));
            taxcell = new PdfPCell(phrase2);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(taxcell);

//-------------------------------------------------------------------------------------------------------------------
//            productcount = bosLineItems.size();
            int linecount = 0;
            int counter = 0;

            if (pageEnd == true && pdfDescPrintCnt >= 0 && productAdded) {
                sno = sno - 1;
                pageEnd = false;
            }
            while (counter < productLimitInCounter) {
                counter++;
                linecount++;
                String strSNO = Integer.toString(sno + 1);
                if (pageEnd) {
                    break;
                }

                cell = new PdfPCell(new Phrase(strSNO, cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(crItems.get(sno).getInvoiceNumber(), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(sft.format(crItems.get(sno).getInvoiceDate()), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(crItems.get(sno).getGstPOS(), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(crItems.get(sno).getLineNumber()), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(crItems.get(sno).getGstReason(), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

//                cell = new PdfPCell(new Phrase(crItems.get(sno).getProductName(), productFont));
                int UQC2Value = 0;
                for (Product product : products) {
                    if (product.getName().equals(crItems.get(sno).getProductName())) {
                        UQC2Value = product.getUQC2Value();
                        break;
                    }
                }

                int UQC1qty = crItems.get(sno).getUqc1Qty(), UQC2qty = crItems.get(sno).getUqc2Qty();
                String strQuantity = "0 " + getUQCcode(crItems.get(sno).getProduct().getUQC1()), strAltQuantity = "0 " + getUQCcode(crItems.get(sno).getProduct().getUQC2());
                if (UQC1qty > 0) {
                    strQuantity = UQC1qty + " " + getUQCcode(crItems.get(sno).getProduct().getUQC1());
                    strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(crItems.get(sno).getProduct().getUQC2());
                    if (UQC2qty > 0) {
                        strQuantity = strQuantity + ", ";
                        strAltQuantity = strAltQuantity + " ";
                    }
                }
                if (UQC2qty > 0) {
                    if (UQC1qty == 0) {
                        strQuantity = "";
                        strAltQuantity = "";
                    }
                    strQuantity = strQuantity + UQC2qty + " " + getUQCcode(crItems.get(sno).getProduct().getUQC2());
                    strAltQuantity = strAltQuantity + crItems.get(sno).getUqc2Qty() + " " + getUQCcode(crItems.get(sno).getProduct().getUQC2());
                }
                int isEqual = crItems.get(sno).getProduct().getUQC1().compareTo(crItems.get(sno).getProduct().getUQC2());
                if (isEqual == 0) {
                    Phrase phrase1 = new Phrase();
                    phrase1.add(new Chunk(crItems.get(sno).getProductName(), productFont));
                    phrase1.add(new Chunk(Chunk.NEWLINE + String.valueOf(crItems.get(sno).getUqc1Qty()), qtyFont));
//                    cell = new PdfPCell(new Phrase(String.valueOf(crItems.get(sno).getUqc1Qty()), cellFont));
                } else {
                    counter++;
                    linecount++;
                    Phrase phrase = new Phrase();
                    phrase.add(new Chunk(crItems.get(sno).getProductName(), productFont));
                    phrase.add(new Chunk(Chunk.NEWLINE + strQuantity, qtyFont));
                    phrase.add(new Chunk(" (" + strAltQuantity + ")", qtyFont));
                    cell = new PdfPCell(phrase);
                }

                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfTable.addCell(cell);

//                cell = new PdfPCell(new Phrase(String.valueOf(crItems.get(sno).getGstPercentage()), productFont));
//                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                pdfTable.addCell(cell);
                cell = new PdfPCell(new Phrase(String.valueOf(crItems.get(sno).getValueInclusiveOfTax()), productFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
                sno++;
            }
            linecount = (productLimitA4 - linecount);
            Phrase phrase = new Phrase();
            for (int i = 0; i < linecount; i++) {
                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

//                phrase.add(new Chunk(" ", totalFontBold));
//                cell = new PdfPCell(phrase);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                pdfTable.addCell(cell);
                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

            }
            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItemsContinued(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

//            PdfPTable pdfTable = new PdfPTable(9);
//            pdfTable.setWidths(new float[]{8, 25, 12, 20, 15, 10, 15, 10, 20});
//            pdfTable.setTotalWidth(555);
//            pdfTable.setLockedWidth(true);
            PdfPTable pdfTable = new PdfPTable(2);
            pdfTable.setWidthPercentage(100);

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItems1(Document document) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(8);
        pdfTable.setWidths(new float[]{7, 10, 15, 20, 6, 24, 41, 13});
        pdfTable.setTotalWidth(555);
        pdfTable.setLockedWidth(true);
//
//        if (isIGST) {
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("IGST", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billIGST), HeaderFont));
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            pdfTable.addCell(cell);
//        } else {
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("CGST", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billCGST), HeaderFont));
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("SGST", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
//
//            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billSGST), HeaderFont));
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            pdfTable.addCell(cell);
//        }

        cell = new PdfPCell(new Phrase("Net Amount", HeaderFont));
        cell.setColspan(7);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("₹" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);
        document.add(pdfTable);

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase("Amount Chargeable(in words)", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("E. & O.E", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrency(billNetAmount.toString()), HeaderFont));
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        for (int i = 0; i < 5; i++) {
            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

        }

        document.add(pdfTable);

    }

    public void addFooter(Document document) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPTable outerTable = new PdfPTable(2);
            outerTable.setWidthPercentage(100);

            PdfPTable pdfTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(new Phrase(companyFooterHeading, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            pdfTable = new PdfPTable(1);

            cell = new PdfPCell(new Phrase("for " + companyName.toUpperCase(), totalFontBold));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            document.add(outerTable);
        } catch (DocumentException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void addHSNTable(Document document, int HSNno, int hsnLimitInCounter) {
//        try {
//            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
//            com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
//            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
//            com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
//
//            PdfPCell taxcell, cell;
//
//            PdfPTable pdfTable;
//            PdfPCell cell6 = null;
//
//            if (isIGST) {
//                pdfTable = new PdfPTable(5);
//                pdfTable.setWidthPercentage(100);
//                pdfTable.setSpacingAfter(10f);
//                pdfTable.setWidths(new float[]{20, 40, 20, 40, 50});
//
//                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
//
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("IGST", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                cell6.setColspan(2);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                cell6.setRowspan(2);
//                pdfTable.addCell(cell6);
//
//                //2nd row
//                cell6 = new PdfPCell(new Phrase("", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                pdfTable.addCell(cell6);
//
////-------------------------------------------------------------------------------------------------------------------
////                hsncount = psTaxSummaries.size();
//                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
//                    if ((counter + 1) == (hsnLimitInCounter)) {
//                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
//                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
////                        hsnno++;
//                    } else {
//                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
//                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                    }
//                    hsnno++;
//                }
//
//            } else {
//                pdfTable = new PdfPTable(7);
//                pdfTable.setWidthPercentage(100);
//                pdfTable.setSpacingAfter(10f);
//                pdfTable.setWidths(new float[]{20, 35, 20, 35, 20, 35, 55});
//
//                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
//
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("CGST", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                cell6.setColspan(2);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("SGST", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                cell6.setColspan(2);
//                //   cell6.setBorder(Rectangle.BOTTOM);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                cell6.setRowspan(2);
//                pdfTable.addCell(cell6);
//
//                //2nd row
//                cell6 = new PdfPCell(new Phrase("", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                pdfTable.addCell(cell6);
//
//                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
//                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//                //  cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
//                pdfTable.addCell(cell6);
//
////-------------------------------------------------------------------------------------------------------------------
////                hsncount = psTaxSummaries.size();
//                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
//
//                    System.err.println("########" + psTaxSummaries.get(0).getHsnSac());
////                    System.err.println("########" + psTaxSummaries.get(hsnno).getIgstPercentage());
//                    if ((counter + 1) == (hsnLimitInCounter)) {
//                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                    } else {
//                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
//
//                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
//                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
////                        hsnno++;
//                    }
//                    hsnno++;
//                }
//            }
//
//            document.add(pdfTable);
//        } catch (DocumentException ex) {
//            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    public void lastLineA4(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    public void createPdfA4(Boolean isPrint, Boolean isPreview) {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            Rectangle layout = new Rectangle(PageSize.A4);
            Document document = new Document(layout, 20, 20, 20, 20);
            String path = "";
            Boolean isApproved = false;
            String nowFilePath = "";
            if (isPrint) {
                path = AppConstants.getTempDocPath() + strNoteType + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File(strNoteType + "-" + newCR.getCdNoteNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }

            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));

                document.open();
//                for (int copies = 0; copies < noOfCopies; copies++) {
//                for (String copyName : nameOfPDFCopies) {
//                    sno = 0;
//                    hsnno = 0;
//                    document.newPage();
//                    String invoiceCopyName = copyName;

//-----------------------------------------------------------------------------------------------------------------------
                int totDescCount = 0;
//                    for (int descCount = 0; descCount < psItems.size(); descCount++) {
//                        totDescCount = totDescCount + psItems.get(sno).getDescriptionCount();
//                    }

                for (CreditDebitNoteLineItem crItem : crItems) {
                    totDescCount = totDescCount + crItem.getDescriptionCount();
                }
                productcount = productcount * 2;
                productcount = productcount + totDescCount;
                int pagecount = 0, remainingProduct = 0;
                pagecount = (productcount / productLimitA4);
                if ((productcount % productLimitA4) > 0) {
                    pagecount += 1;
                }
                if (pagecount == 1) {
                    int limitcount = 0;
                    addInfo(document, pagecount);
                    if (productcount == productLimitA4) {
                        limitcount = productcount;
                    } else {
                        limitcount = productcount % productLimitA4;
                    }
                    addProductItems(document, (limitcount));
                    afterAddProductItems1(document);
                    addFooter(document);
                    lastLineA4(document, pagecount, pagecount);
                } else {
                    for (int i = 1; i <= pagecount; i++) {
                        if (pagecount == i) {
                            remainingProduct = productcount - (remainingProduct * productLimitA4);
                            addInfo(document, i);
                            addProductItems(document, remainingProduct);
                            afterAddProductItems1(document);
                            addFooter(document);
                            lastLineA4(document, i, pagecount);
                        } else {
                            addInfo(document, i);
                            addProductItems(document, productLimitA4);
                            afterAddProductItemsContinued(document);
                            lastLineA4(document, i, pagecount);
                            document.newPage();
                            remainingProduct++;
                        }
//                        }
                    }
                    productcount = productcount / 2;
                }
                document.close();
                fos.close();

                if (isPrint) {
                    try {
                        pdfPrint(path);
                    } catch (PrintException ex) {
                        Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
//                    createImagePdf(path);
                }

            }
            com.alee.laf.WebLookAndFeel.install();
//            if (!isPrint && isApproved) {
//                JOptionPane.showMessageDialog(null, "Bill Downloaded at " + path, "Message", JOptionPane.INFORMATION_MESSAGE);
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException | IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(CreditNotePdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }
}
