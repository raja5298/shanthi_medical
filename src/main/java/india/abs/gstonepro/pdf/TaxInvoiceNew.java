
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.api.utils.IndianCurrencyUtil;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.business.UQCLogic;
import india.abs.gstonepro.ui.utils.TaxBillItems;
import india.abs.gstonepro.ui.BillPreview;
import static java.awt.Component.RIGHT_ALIGNMENT;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author user
 */
public class TaxInvoiceNew {

    public class FooterTable extends PdfPageEventHelper {

        protected PdfPTable footer;

        public FooterTable(PdfPTable footer) {
            this.footer = footer;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            footer.writeSelectedRows(0, 5, 20, 80, writer.getDirectContent());
        }
    }
    static int totalProductCount = 0, hsncount, hsncountNon;

    int productLimitA4 = 20, productLimitA5L = 11, productLimitA5P = 8;
    static int sno = 0, hsnno = 0, hsnnoA4 = 0;

    int random = 0;
    int pdfA5LTotalDescCnt = 0;
    BigDecimal zero = new BigDecimal("0.000");

    public static final String tamilfont = "/fonts/Baamini.ttf";
    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";
//    public String companyName = "", companyLine1 = "", companyLine2 = "", companyLine3 = "", companyLine4 = "",
//            companyDistrict = "", CompanyState = "", companyGSTIN = "";

    public String companyName = "", companyA4Addr = "", companyA5LAddr = "", companyA5PAddr = "",
            companyA7Addr = "", companyCity = "", companyDistrict = "", companyEmail = "",
            companyState = "", companyMobile = "", companyPhone = "", companyGSTIN = "", invoiceHeading = "",
            companyFooterHeading = "", companyFooterLineOne = "", companyFooterLineTwo = "", companyFooterLineThree = "",
            companyFooterLineFour = "", companyA4AddrLine1 = "", companyA4AddrLine2 = "", companyA4AddrLine3 = "", companyA5LAddrLine1 = "", companyA5LAddrLine2 = "",
            companyA5LAddrLine3 = "", companyA5PAddrLine1 = "", companyA5PAddrLine2 = "", companyA5PAddrLine3 = "", companyA7AddrLine1 = "", companyA7AddrLine2 = "",
            companyA7AddrLine3 = "";

    public String billNo = " ", billDate = " ", billCompanyName = " ", billCompanyAddr1 = " ", billCompanyAddr2 = " ",
            billCompanyAddr3 = " ", billCompanyCity = " ", billCompanyDistrict = " ",
            billCompanyState = " ", billCompanyMobile = " ", billCompanyPhone = " ", billCompanyEmail = " ", billCompanyGSTIN = " ", billPayment = " ",
            billBuyingOrderNo = " ", billOrderDate = " ", billDispatchThorugh = " ", billDestination = " ", billTermOfDelivery = " ", billThrough = " ",
            billReferenceNo = " ", billRemarks = " ", firstBillCopy = " ", secondBillCopy = " ", thirdBillCopy = " ", fourthBillCopy = " ", fifthBillCopy = " ";
    public String ShippingName = " ", ShippingAddr1 = " ", ShippingAddr2 = " ", ShippingAddr3 = " ", ShippingCity = " ", ShippingPincode = " ", ShippingDistrict = " ", ShippingState = " ",
            ShippingMobile = " ", ShippingPhone = " ", ShippingGSTIN = " ";
    public boolean isIGST = false, billIsSameAddress = true, isBill = false, isPdfOnly = false;
    boolean isRegional = false;
    boolean pageEnd = false, descEnd = false, productAdded = false;
    public int billId = 0, noOfCopies;
    public BigDecimal billNetAmount = new BigDecimal(0);

//    public float billIGST = 0, billTotalTax = 0, billFlightAmount = 0, billCGST = 0, billSGST = 0, billTotal = 0, billDiscountPer = 0, billDiscount = 0, billRoundOff = 0, billCessamount = 0;
    DecimalFormat currency = new DecimalFormat("##,##,##0.00000000");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    int pdfDescCount = 0, pdfDescPrintCnt = 0;

    public BigDecimal billRoundOff = new BigDecimal(0);
    public BigDecimal billIGST = new BigDecimal(0);
    public BigDecimal billCGST = new BigDecimal(0);
    public BigDecimal billSGST = new BigDecimal(0);
    public BigDecimal billTotal = new BigDecimal(0);
    public BigDecimal billTotalTax = new BigDecimal(0);
    public float billFlightAmount = 0, billCessamount = 0;
//    DecimalFormat currency = new DecimalFormat("##,##,##0.00");

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<PurchaseSaleLineItem> psItems = new ArrayList<>();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
    List<TaxBillItems> taxBillItems = new ArrayList<>();
    List<PurchaseSaleTaxSummary> psTaxSummaries = new ArrayList<>();
    ArrayList<String> productDescriptions = null;
    HashMap<Long, ArrayList<String>> productDescA5P = null;
    PurchaseSale newPS = new PurchaseSale();
    PurchaseSaleTaxSummary newTS = new PurchaseSaleTaxSummary();
    List<String> nameOfPDFCopies = new ArrayList<String>();
    BillPreview billPreview = null;

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont8 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font boldFont9 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1Bold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 6, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont7 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

    com.itextpdf.text.Font footerboldfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

    public static final String font = "/fonts/Baamini.ttf";

    Font productTamilFont = FontFactory.getFont(font, "cp1250", BaseFont.EMBEDDED);

    public void pdfByBillId(Boolean isPrint, Boolean isPreview, String billId, List<String> headers, boolean isTamil) {

        try {
            isRegional = isTamil;
            PurchaseSale PS = new PurchaseSaleLogic().getPSDetail(billId);
            List<PurchaseSaleLineItem> psLIs = PS.getPsLineItems();
            List<PurchaseSaleTaxSummary> psTSs = PS.getPsTaxSummaries();

            printPdf(isPrint, isPreview, PS, psLIs, psTSs, headers);
        } catch (IOException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printPdf(Boolean isPrint, Boolean isPreview, PurchaseSale PS, List<PurchaseSaleLineItem> Items, List<PurchaseSaleTaxSummary> TaxSummaries, List<String> headers) throws IOException {
        try {
            sno = 0;
            hsnnoA4 = 0;
            hsnno = 0;
            newPS = PS;
            psItems = Items;
            psTaxSummaries = TaxSummaries;
            nameOfPDFCopies = headers;
//            productcount = psItems.size();
            hsncount = psTaxSummaries.size();
//            getBillItems();

            setCompany();
            setBillDetails();
            String paperSize = SessionDataUtil.getCompanyPolicyData().getPrintPaperSize();
            switch (paperSize) {
                case "A4_P":
                    createPdfA4(isPrint, isPreview);
                    break;
                case "A5_P":
                    createPdfA5P(isPrint, isPreview);
                    break;
                case "A5_L": {
                    try {
                        createPdfA5L(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
                case "A7_P": {

                    try {
                        createPdfA7(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
//            case "A7_P":
//                createPdfA5P(isPrint, isPreview);
                default:
                    break;
            }
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUQCcode(String value) {
        String strUQCcode = "";
        for (UQC uqc : UQCs) {
            if (uqc.getQuantityName().equals(value)) {
                strUQCcode = uqc.getUQCCode();
                break;
            }
        }
        return strUQCcode;
    }

    public void getBillItems() {
        int Sno = 0;
        int produtSize = psItems.size();
        for (int i = 0; i < psItems.size(); i++) {
            TaxBillItems p = new TaxBillItems();
            p.setSno(String.valueOf(i));
            p.setProductNameOrbillChargeName(psItems.get(i).getProductName());
            p.setHsnSac(psItems.get(i).getHsnSac());
            p.setUqcOne(psItems.get(i).getUqcOne());
            p.setUqcOneQuantity(psItems.get(i).getUqcOneQuantity());
            p.setUqcOneRate(psItems.get(i).getUqcOneRate());
            p.setUqcOneValue(psItems.get(i).getUqcOneValue());
            p.setUqcTwo(psItems.get(i).getUqcTwo());
            p.setUqcTwoQuantity(psItems.get(i).getUqcTwoQuantity());
            p.setUqcTwoRate(psItems.get(i).getUqcTwoRate());
            p.setUqcTwoValue(psItems.get(i).getUqcTwoValue());
            p.setIgstPercentage(psItems.get(i).getIgstPercentage());
            p.setCgstPercentage(psItems.get(i).getCgstPercentage());
            p.setSgstPercentage(psItems.get(i).getSgstPercentage());
            p.setIgstValue(psItems.get(i).getIgstValue());
            p.setCgstValue(psItems.get(i).getCgstValue());
            p.setSgstValue(psItems.get(i).getSgstValue());
            p.setProductvalueOrbillChargeValue(psItems.get(i).getValue());
            p.setIsBillCharge(psItems.get(i).getProduct().isService());
            taxBillItems.add(p);
        }

//        for (int j = 0; j < bosChargeItems.size(); j++) {
//            TaxBillItems p = new TaxBillItems();
//            p.setSno(String.valueOf(j + produtSize));
//            p.setProductNameOrbillChargeName(bosChargeItems.get(j).getBillChargeName());
//            p.setProductvalueOrbillChargeValue(bosChargeItems.get(j).getValue());
//            p.setIsBillCharge(true);
//            taxBillItems.add(p);
//        }
//        productcount = taxBillItems.size();
    }

    public void setBillDetails() {
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
        isIGST = newPS.isIsIGST();
        billNo = newPS.getBillNo();
        billDate = sdfr.format(newPS.getPsDate());
        if (newPS.getLedger() == null) { //|| !newPS.isNoParty()) {
            if (newPS.isNoParty()) {
                billCompanyName = newPS.getNoPartyName().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                billCompanyAddr1 = newPS.getNoPartyLineOne().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                billCompanyCity = newPS.getNoPartyCity().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                billCompanyDistrict = newPS.getNoPartyDistrict().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                billCompanyState = newPS.getNoPartyState().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
                billCompanyGSTIN = newPS.getNoPartyGSTIN().replaceAll("^\\s+", "").replaceAll("\\s+$", "");
            } else {
                billCompanyName = "";
                billCompanyAddr1 = "";
                billCompanyCity = "";
                billCompanyDistrict = "";
                billCompanyState = "";
                billCompanyMobile = "";
                billCompanyPhone = "";
                billCompanyGSTIN = "";
            }

        } else {

            billCompanyName = newPS.getLedger().getLedgerName();

            String Address = newPS.getLedger().getAddress();
            if (Address != null) {
                billCompanyAddr1 = Address.replaceAll("/~/", ",");
            }
            String[] words = Address.split("/~/");

            if (words.length == 1) {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = "";
                billCompanyAddr3 = "";
            } else if (words.length == 2) {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = words[1];
                billCompanyAddr3 = "";
            } else {
                billCompanyAddr1 = (words[0]);
                billCompanyAddr2 = words[1];
                billCompanyAddr3 = words[2];
            }

//        billCompanyAddr2 = "";
//        billCompanyAddr3 = "";
            if (newPS.getLedger().getCity() != null) {
                billCompanyCity = newPS.getLedger().getCity();
            }
            if (newPS.getLedger().getDistrict() != null) {
                billCompanyDistrict = newPS.getLedger().getDistrict();
            }
            if (newPS.getLedger().getState() != null) {
                billCompanyState = newPS.getLedger().getState();
            }
            if (newPS.getLedger().getMobile() != null) {
                billCompanyMobile = newPS.getLedger().getMobile();
            }
            if (newPS.getLedger().getEmail() != null) {
                billCompanyEmail = newPS.getLedger().getEmail();
            }
            if (newPS.getLedger().getPhone() != null) {
                billCompanyPhone = newPS.getLedger().getPhone();
            }
            if (newPS.getLedger().getGSTIN() != null) {
                billCompanyGSTIN = newPS.getLedger().getGSTIN();
            }
        }

        billTotal = newPS.getBillAmount();
        if (isIGST) {
            billTotalTax = newPS.getIgstValue();
        } else {
            billTotalTax = newPS.getCgstValue().add(newPS.getSgstValue());
        }
        billNetAmount = newPS.getCalculatedTotalValue();

        billCGST = newPS.getCgstValue();
        billSGST = newPS.getSgstValue();
        billIGST = newPS.getIgstValue();
        billTotal = newPS.getBillAmount();
        billNetAmount = newPS.getCalculatedTotalValue();
        isIGST = newPS.isIsIGST();
        billRoundOff = newPS.getRoundOffValue();
        billReferenceNo = newPS.getReferenceNumber();
        billRemarks = newPS.getRemarks();
        ShippingName = newPS.getShippingAddressName();
        ShippingAddr1 = newPS.getShippingAddressOne().replaceAll("^\\s+", "").replaceAll("\\s+$", "");;
        ShippingAddr2 = newPS.getShippingAddressTwo().replaceAll("^\\s+", "").replaceAll("\\s+$", "");;
        ShippingAddr3 = newPS.getShippingAddressThree().replaceAll("^\\s+", "").replaceAll("\\s+$", "");;
        ShippingCity = newPS.getShippingAddressCity();
        ShippingDistrict = newPS.getShippingAddressDistrict();
        ShippingState = newPS.getShippingAddressState();
        ShippingPhone = newPS.getShippingAddressPhone();
        ShippingMobile = newPS.getShippingAddressMobile();

//        }
        noOfCopies = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
//        noOfCopies = newPS.getCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
        firstBillCopy = newPS.getCompany().getCompanyPolicy().getFirstInvoiceWord();
        secondBillCopy = newPS.getCompany().getCompanyPolicy().getSecondInvoiceWord();
        thirdBillCopy = newPS.getCompany().getCompanyPolicy().getThirdInvoiceWord();
        fourthBillCopy = newPS.getCompany().getCompanyPolicy().getFourthInvoiceWord();
        fifthBillCopy = newPS.getCompany().getCompanyPolicy().getFifthInvoiceWord();
//        }
    }

    public void setCompany() {
        companyName = SessionDataUtil.getSelectedCompany().getCompanyName();

        String A4AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineOne();
        String A4AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineTwo();
        String A4AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineThree();
        if (A4AddressLine1 != null) {
            companyA4AddrLine1 = A4AddressLine1.replaceAll("/~/", ",");
        }
        if (A4AddressLine2 != null) {
            companyA4AddrLine2 = A4AddressLine2.replaceAll("/~/", ",");
        }
        if (A4AddressLine3 != null) {
            companyA4AddrLine3 = A4AddressLine3.replaceAll("/~/", ",");
        }
        String A5PAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineOne();
        String A5PAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineTwo();
        String A5PAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineThree();
        if (A5PAddressLine1 != null) {
            companyA5PAddrLine1 = A5PAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine2 != null) {
            companyA5PAddrLine2 = A5PAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine3 != null) {
            companyA5PAddrLine3 = A5PAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A5LAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineOne();
        String A5LAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineTwo();
        String A5LAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineThree();
        if (A5LAddressLine1 != null) {
            companyA5LAddrLine1 = A5LAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine2 != null) {
            companyA5LAddrLine2 = A5LAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine3 != null) {
            companyA5LAddrLine3 = A5LAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A7AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineOne();
        String A7AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineTwo();
        String A7AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineThree();
        if (A7AddressLine1 != null) {
            companyA7AddrLine1 = A7AddressLine1.replaceAll("/~/", ",");
        }
        if (A7AddressLine2 != null) {
            companyA7AddrLine2 = A7AddressLine2.replaceAll("/~/", ",");
        }
        if (A7AddressLine3 != null) {
            companyA7AddrLine3 = A7AddressLine3.replaceAll("/~/", ",");
        }
        if (SessionDataUtil.getSelectedCompany().getCity() != null) {
            companyCity = SessionDataUtil.getSelectedCompany().getCity();
        }
        if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
            companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
        }
        if (SessionDataUtil.getSelectedCompany().getState() != null) {
            companyState = SessionDataUtil.getSelectedCompany().getState();
        }
        if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
            companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
        }
        if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
            companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
        }
        if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
            companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
        }
        if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
            companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getTaxInvoiceWord() != null) {
            invoiceHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getTaxInvoiceWord();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading() != null) {
            companyFooterHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne() != null) {
            companyFooterLineOne = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo() != null) {
            companyFooterLineTwo = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree() != null) {
            companyFooterLineThree = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour() != null) {
            companyFooterLineFour = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour();
        }

    }

    public void addInfo(Document document, int invoiceno, String invoiceCopyName) {
        try {

            PdfPTable taxTable = new PdfPTable(2);
            taxTable.setWidthPercentage(100);

            taxTable.setWidths(new float[]{55, 45});
            PdfPCell taxcell;

            Phrase p = new Phrase("", boldFont);
            taxcell = new PdfPCell(p);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);

            Phrase p1 = new Phrase(invoiceCopyName, cellFont1);
            taxcell = new PdfPCell(p1);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);
            document.add(taxTable);

            PdfPCell cell;

            PdfPTable Table1 = new PdfPTable(2);
            Table1.setWidthPercentage(100);
            PdfPCell cell1 = null;

            cell1 = new PdfPCell(new Phrase("Invoice No : " + billNo, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Date : " + billDate, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            document.add(Table1);

//            String imgName = companyName + "-logo.jpg";
//
//            URL url = getClass().getResource("/images/" + imgName);
//            if (url != null) {
            String imgPath = ".\\assets\\" + companyName + "-logo.jpg";

            File file = new File(imgPath);
            System.out.println("FILE-->" + file);
            if (file.exists()) {
                URI uri = file.toURI();
                URL imgURL = uri.toURL();
                PdfPTable companyTable = new PdfPTable(3);
                companyTable.setWidthPercentage(100);
                companyTable.setWidths(new float[]{15, 70, 15});

                PdfPTable logoTable = new PdfPTable(1);
                logoTable.setWidthPercentage(100);

                cell = new PdfPCell();

                com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance(imgURL);
                img.setAlignment((int) RIGHT_ALIGNMENT);
                img.scaleAbsolute(50f, 50f);
                cell.addElement(img);

                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
                logoTable.addCell(cell);

                cell = new PdfPCell(logoTable);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setRowspan(5);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM);

                companyTable.addCell(cell);
                document.add(companyTable);

                taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                taxcell = null;

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
//                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
                companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
                if (!companyA4Addr.equals("")) {
                    taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
//                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    taxTable.addCell(taxcell);
                }

                Phrase phrase = new Phrase();
                phrase.add(new Chunk("City : " + companyCity, cellFont));
                phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell = new PdfPCell(phrase);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
//                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                phrase = new Phrase();
                if (!companyPhone.equals("")) {
                    phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyMobile.equals("")) {
                    phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyEmail.equals("")) {
                    phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
//                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
//                taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                cell = new PdfPCell(taxTable);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM);

                companyTable.addCell(cell);
                document.add(companyTable);

                logoTable = new PdfPTable(1);
                logoTable.setWidthPercentage(100);

                cell = new PdfPCell();

//                com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance(url);
//                img.setAlignment((int) RIGHT_ALIGNMENT);
//                img.scaleAbsolute(50f, 50f);
//                cell.addElement(img);
//
//                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
//                logoTable.addCell(cell);
                cell = new PdfPCell(logoTable);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setRowspan(5);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.RIGHT);

                companyTable.addCell(cell);
                document.add(companyTable);

            } else {
                taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                taxcell = null;

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
                companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
                if (!companyA4Addr.equals("")) {
                    taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    taxTable.addCell(taxcell);
                }

                Phrase phrase = new Phrase();
                phrase.add(new Chunk("City : " + companyCity, cellFont));
                phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell = new PdfPCell(phrase);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                phrase = new Phrase();
                if (!companyPhone.equals("")) {
                    phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyMobile.equals("")) {
                    phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyEmail.equals("")) {
                    phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                document.add(taxTable);
            }

            PdfPTable Table = new PdfPTable(1);
            Table.setWidthPercentage(100);
            cell = null;

            cell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            Table.addCell(cell);

            document.add(Table);

            if (!newPS.isNoParty()) {
                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                PdfPTable innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                if (!"".equals(billCompanyGSTIN)) {
                    tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                }

                tablecell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(billCompanyPhone + " | " + billCompanyMobile + " | " + billCompanyEmail, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(ShippingName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingAddr1 + " " + ShippingAddr2 + " " + ShippingAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
//            tablecell.setBorder(Rectangle.NO_BORDER);
//            innerTable.addCell(tablecell);
                tablecell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                if (!"".equals(ShippingPhone) && "".equals(ShippingMobile)) {
                    tablecell = new PdfPCell(new Phrase(ShippingPhone + " | " + ShippingMobile, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                } else if (!"".equals(ShippingPhone)) {
                    tablecell = new PdfPCell(new Phrase(ShippingPhone, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                } else if (!"".equals(ShippingMobile)) {
                    tablecell = new PdfPCell(new Phrase(ShippingMobile, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                }

                table2.addCell(innerTable);

                document.add(table2);
            } else {
                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                PdfPTable innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyGSTIN, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyState, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(billCompanyPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(ShippingName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingAddr1 + " " + ShippingAddr2 + " " + ShippingAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
//            tablecell.setBorder(Rectangle.NO_BORDER);
//            innerTable.addCell(tablecell);
                tablecell = new PdfPCell(new Phrase(ShippingState, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(ShippingPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                document.add(table2);
            }

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidthPercentage(100);
            PdfPCell cell3 = null;

            cell3 = new PdfPCell(new Phrase(billRemarks, cellFont1));
            cell3.setColspan(2);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            table3.addCell(cell3);

            document.add(table3);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProductItems(Document document, int productLimitInCounter) throws IOException {

        try {

            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font productFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell taxcell, cell;
            PdfPTable pdfTable = new PdfPTable(9);
            pdfTable.setWidths(new float[]{4, 34, 7, 6, 15, 8, 6, 5, 10});
            pdfTable.setTotalWidth(555);
            pdfTable.setLockedWidth(true);

            float ht = pdfTable.getTotalHeight();

            taxcell = new PdfPCell(new Phrase("S.No", totalFont8));
            //taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("GST %", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Quantity", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Free", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("(per)", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", totalFont8));
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(taxcell);
//-------------------------------------------------------------------------------------------------------------------
//            productcount = bosLineItems.size();
            int linecount = 0;
            int counter = 0;

            if (pageEnd == true && pdfDescPrintCnt >= 0 && productAdded) {
                sno = sno - 1;
                pageEnd = false;
            }
            while (counter < productLimitInCounter) {
                counter++;
                linecount++;
                String strSNO = Integer.toString(sno + 1);
                if (pageEnd) {
                    break;
                }
                if (sno < psItems.size() && psItems.get(sno).getProduct().isService()) {

                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        if ((psItems.get(sno).getDescriptionOne() != null) && (!psItems.get(sno).getDescriptionOne().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionOne());
                        }
                        if ((psItems.get(sno).getDescriptionTwo() != null) && (!psItems.get(sno).getDescriptionTwo().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTwo());
                        }
                        if ((psItems.get(sno).getDescriptionThree() != null) && (!psItems.get(sno).getDescriptionThree().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionThree());
                        }
                        if ((psItems.get(sno).getDescriptionFour() != null) && (!psItems.get(sno).getDescriptionFour().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFour());
                        }
                        if ((psItems.get(sno).getDescriptionFive() != null) && (!psItems.get(sno).getDescriptionFive().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFive());
                        }
                        if ((psItems.get(sno).getDescriptionSix() != null) && (!psItems.get(sno).getDescriptionSix().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSix());
                        }
                        if ((psItems.get(sno).getDescriptionSeven() != null) && (!psItems.get(sno).getDescriptionSeven().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSeven());
                        }
                        if ((psItems.get(sno).getDescriptionEight() != null) && (!psItems.get(sno).getDescriptionEight().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionEight());
                        }
                        if ((psItems.get(sno).getDescriptionNine() != null) && (!psItems.get(sno).getDescriptionNine().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionNine());
                        }
                        if ((psItems.get(sno).getDescriptionTen() != null) && (!psItems.get(sno).getDescriptionTen().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTen());
                        }
                        pdfDescCount = productDescriptions.size();

                        cell = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        if (!isRegional) {
                            cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), productFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            cell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        }

                        for (int i = 0; i < 3; i++) {
                            cell = new PdfPCell(new Phrase("", HeaderFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        }
                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

//                    if (psItems.get(sno).getValue().compareTo(zero) > 0) {
//                        cell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), HeaderFont));
//                    } else {
                        cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), totalFontBold));
//                    }

                        Phrase phrase = new Phrase();
                        phrase.add(new Chunk(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getUqcOneRate()), totalFontBold));
//                    phrase.add(new Chunk(Chunk.NEWLINE + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getUqcTwoRate()), cellFont));
                        cell = new PdfPCell(phrase);
//                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell.setBorder(Rectangle.LEFT);

                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);
                    }
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 26) {
                            pageEnd = true;
                            break;
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);
                        for (int j = 0; j < 3; j++) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            pdfTable.addCell(cell);
                        }
                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        linecount++;
                        counter++;
                        pdfDescPrintCnt++;
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }

                } else if (sno < psItems.size()) {
                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        if ((psItems.get(sno).getDescriptionOne() != null) && (!psItems.get(sno).getDescriptionOne().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionOne());
                        }
                        if ((psItems.get(sno).getDescriptionTwo() != null) && (!psItems.get(sno).getDescriptionTwo().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTwo());
                        }
                        if ((psItems.get(sno).getDescriptionThree() != null) && (!psItems.get(sno).getDescriptionThree().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionThree());
                        }
                        if ((psItems.get(sno).getDescriptionFour() != null) && (!psItems.get(sno).getDescriptionFour().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFour());
                        }
                        if ((psItems.get(sno).getDescriptionFive() != null) && (!psItems.get(sno).getDescriptionFive().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFive());
                        }
                        if ((psItems.get(sno).getDescriptionSix() != null) && (!psItems.get(sno).getDescriptionSix().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSix());
                        }
                        if ((psItems.get(sno).getDescriptionSeven() != null) && (!psItems.get(sno).getDescriptionSeven().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSeven());
                        }
                        if ((psItems.get(sno).getDescriptionEight() != null) && (!psItems.get(sno).getDescriptionEight().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionEight());
                        }
                        if ((psItems.get(sno).getDescriptionNine() != null) && (!psItems.get(sno).getDescriptionNine().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionNine());
                        }
                        if ((psItems.get(sno).getDescriptionTen() != null) && (!psItems.get(sno).getDescriptionTen().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTen());
                        }
                        pdfDescCount = productDescriptions.size();
//-------------------------------------------------------------------------------------------------------------------
                        cell = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        if (!isRegional) {
                            cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), productFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            Phrase phrase = new Phrase(tamilName, productTamilFont);
                            cell = new PdfPCell(phrase);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        }

                        cell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getIgstPercentage()), cellFont));

                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        int UQC2Value = 0;
                        for (Product product : products) {
                            if (product.getName().equals(psItems.get(sno).getProductName())) {
                                UQC2Value = product.getUQC2Value();
                            }
                        }
                        int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();
                        String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
                        if (UQC1qty > 0) {
                            strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
                            strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            if (UQC2qty > 0) {
                                strQuantity = strQuantity + ", ";
                                strAltQuantity = strAltQuantity + " ";
                            }
                        }
                        if (UQC2qty > 0) {
                            if (UQC1qty == 0) {
                                strQuantity = "";
                                strAltQuantity = "";
                            }
                            strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        }
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else {
                            counter++;
                            linecount++;
                            Phrase phrase = new Phrase();
                            phrase.add(new Chunk(strQuantity, totalFontBold));
                            phrase.add(new Chunk(Chunk.NEWLINE + "(" + strAltQuantity + ")", cellFont));
                            cell = new PdfPCell(phrase);
                        }

                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        Phrase phrase = new Phrase();
                        phrase.add(new Chunk(currency.format(psItems.get(sno).getUqcOneRate()), totalFontBold));
                        if (isEqual != 0) {
                            phrase.add(new Chunk(Chunk.NEWLINE + currency.format(psItems.get(sno).getUqcTwoRate()), cellFont));
                        }
                        cell = new PdfPCell(phrase);
//                      cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        
                        
                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getFree()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        phrase = new Phrase();
                        phrase.add(new Chunk("(" + getUQCcode(psItems.get(sno).getUqcOne()) + ")", totalFontBold));
                        if (!psItems.get(sno).getUqcOne().equals(psItems.get(sno).getUqcTwo())) {
                            if (!psItems.get(sno).getUqcTwo().equals("")) {
                                phrase.add(new Chunk(Chunk.NEWLINE + "(" + getUQCcode(psItems.get(sno).getUqcTwo()) + ")", cellFont));
                            }
                        }
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);


                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 26) {
                            pageEnd = true;
                            break;
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);
                        for (int j = 0; j < 3; j++) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            pdfTable.addCell(cell);
                        }
                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        linecount++;
                        counter++;
                        pdfDescPrintCnt++;
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }
                }
                sno++;

            }
            linecount = (productLimitA4 - linecount);
            Phrase phrase = new Phrase();
            for (int i = 0; i < linecount; i++) {
                phrase.add(new Chunk(" ", totalFontBold));
//              phrase.add(new Chunk(Chunk.NEWLINE + " 2", cellFont));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

            }
            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItemsContinued(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

//            PdfPTable pdfTable = new PdfPTable(9);
//            pdfTable.setWidths(new float[]{8, 25, 12, 20, 15, 10, 15, 10, 20});
//            pdfTable.setTotalWidth(555);
//            pdfTable.setLockedWidth(true);
            PdfPTable pdfTable = new PdfPTable(2);
            pdfTable.setWidthPercentage(100);

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItems1(Document document) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(9);
        pdfTable.setWidths(new float[]{4, 34, 7, 6, 15, 8, 5, 6, 10});
        pdfTable.setTotalWidth(555);
        pdfTable.setLockedWidth(true);

        if (isIGST) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("IGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billIGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("CGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billCGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("SGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billSGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }

        if (billRoundOff.compareTo(BigDecimal.ZERO) != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Round Off", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Net Amount", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("₹" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);
        document.add(pdfTable);

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase("Amount Chargeable(in words)", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("E. & O.E", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrency(billNetAmount.toString()), HeaderFont));
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        for (int i = 0; i < 5; i++) {
            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

        }

        document.add(pdfTable);

    }

    public void addFooter(Document document) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPTable outerTable = new PdfPTable(2);
            outerTable.setWidthPercentage(100);

            PdfPTable pdfTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(new Phrase(companyFooterHeading, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            pdfTable = new PdfPTable(1);

            cell = new PdfPCell(new Phrase("for " + companyName.toUpperCase(), totalFontBold));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            document.add(outerTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addHSNTable(Document document, int HSNno, int hsnLimitInCounter) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);

            PdfPCell taxcell, cell;

            PdfPTable pdfTable;
            PdfPCell cell6 = null;

            if (isIGST) {
                pdfTable = new PdfPTable(5);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 40, 20, 40, 50});

                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);

                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("IGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
//                hsncount = psTaxSummaries.size();
                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
                    if ((counter + 1) == (hsnLimitInCounter)) {
                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
//                        hsnno++;
                    } else {
                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                    }
                    hsnno++;
                }

            } else {
                pdfTable = new PdfPTable(7);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 35, 20, 35, 20, 35, 55});

                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);

                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("CGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("SGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                //   cell6.setBorder(Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
//                hsncount = psTaxSummaries.size();
                for (int counter = 0; counter < hsnLimitInCounter; counter++) {

                    System.err.println("########" + psTaxSummaries.get(0).getHsnSac());
//                    System.err.println("########" + psTaxSummaries.get(hsnno).getIgstPercentage());
                    if ((counter + 1) == (hsnLimitInCounter)) {
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                    } else {
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
//                        hsnno++;
                    }
                    hsnno++;
                }
            }

            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void lastLineA4(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    public void createPdfA4(Boolean isPrint, Boolean isPreview) {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            Rectangle layout = new Rectangle(PageSize.A4);
            Document document = new Document(layout, 20, 20, 20, 20);
            String path = "";
            Boolean isApproved = false;
            String nowFilePath = "";
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Tax Invoice" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Tax Invoice" + "-" + newPS.getBillNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }

            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));

                document.open();
//                for (int copies = 0; copies < noOfCopies; copies++) {
                for (String copyName : nameOfPDFCopies) {
                    sno = 0;
                    hsnno = 0;
                    document.newPage();
                    String invoiceCopyName = copyName;
                    int productcount = psItems.size();
//-----------------------------------------------------------------------------------------------------------------------
                    int totDescCount = 0;
//                    for (int descCount = 0; descCount < psItems.size(); descCount++) {
//                        totDescCount = totDescCount + psItems.get(sno).getDescriptionCount();
//                    }

                    for (PurchaseSaleLineItem psItem : psItems) {
                        totDescCount = totDescCount + psItem.getDescriptionCount();
                    }
                    productcount = productcount * 2;
                    productcount = productcount + totDescCount;
                    int pagecount = 0, remainingProduct = 0;
                    pagecount = (productcount / productLimitA4);
                    if ((productcount % productLimitA4) > 0) {
                        pagecount += 1;
                    }
                    if (pagecount == 1) {
                        int limitcount = 0;
                        addInfo(document, pagecount, invoiceCopyName);
                        if (productcount == productLimitA4) {
                            limitcount = productcount;
                        } else {
                            limitcount = productcount % productLimitA4;
                        }
                        addProductItems(document, (limitcount));
                        afterAddProductItems1(document);
                        addHSNTable(document, hsnno, hsncount);
                        addFooter(document);
                        lastLineA4(document, pagecount, pagecount);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (pagecount == i) {
                                remainingProduct = productcount - (remainingProduct * productLimitA4);
                                addInfo(document, i, invoiceCopyName);
                                addProductItems(document, remainingProduct);
                                afterAddProductItems1(document);
                                addHSNTable(document, hsnno, hsncount);
                                addFooter(document);
                                lastLineA4(document, i, pagecount);
                            } else {
                                addInfo(document, i, invoiceCopyName);
                                addProductItems(document, productLimitA4);
                                afterAddProductItemsContinued(document);
                                lastLineA4(document, i, pagecount);
                                document.newPage();
                                remainingProduct++;
                            }
                        }
                    }
                    productcount = productcount / 2;
                }
                document.close();
                fos.close();

                if (isPrint) {
                    try {
                        pdfPrint(path, isPreview);
                    } catch (PrintException ex) {
                        Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
//                if (isPreview) {
//                    createImagePdf(path);
//                }

            }
            com.alee.laf.WebLookAndFeel.install();
//            if (!isPrint && isApproved) {
//                JOptionPane.showMessageDialog(null, "Bill Downloaded at " + path, "Message", JOptionPane.INFORMATION_MESSAGE);
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addInfoA5P(Document document, int invoiceno, String invoiceCopyName) {
        try {
            com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, BaseColor.BLACK);

            com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

            PdfPTable taxTable = new PdfPTable(2);
            taxTable.setWidthPercentage(100);

            taxTable.setWidths(new float[]{55, 45});
            PdfPCell taxcell;

            Phrase p = new Phrase("", boldFont);
            taxcell = new PdfPCell(p);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);

            Phrase p1 = new Phrase(invoiceCopyName, cellFont1);
            taxcell = new PdfPCell(p1);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);
            document.add(taxTable);

            PdfPCell cell;

            PdfPTable Table1 = new PdfPTable(2);
            Table1.setWidthPercentage(100);
            PdfPCell cell1 = null;

            cell1 = new PdfPCell(new Phrase("Invoice No : " + billNo, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Date : " + billDate, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            document.add(Table1);
            String imgPath = ".\\assets\\" + companyName + "-logo.jpg";

            File file = new File(imgPath);
            System.out.println("FILE-->" + file);
            if (file.exists()) {
                URI uri = file.toURI();
                URL imgURL = uri.toURL();

                PdfPTable companyTable = new PdfPTable(2);
                companyTable.setWidthPercentage(100);
                companyTable.setWidths(new float[]{20, 80});

                PdfPTable logoTable = new PdfPTable(1);
                logoTable.setWidthPercentage(100);

                cell = new PdfPCell();

                com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance(imgURL);
                img.setAlignment((int) RIGHT_ALIGNMENT);
                img.scaleAbsolute(50f, 50f);
                cell.addElement(img);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
                logoTable.addCell(cell);

                cell = new PdfPCell(logoTable);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setRowspan(5);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM);

                companyTable.addCell(cell);
                document.add(companyTable);

                taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                taxcell = null;

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);
                companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
                if (!companyA4Addr.equals("")) {
                    taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                }

                Phrase phrase = new Phrase();
                phrase.add(new Chunk("City : " + companyCity, cellFont));
                phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell = new PdfPCell(phrase);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                phrase = new Phrase();
                if (!companyPhone.equals("")) {
                    phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyMobile.equals("")) {
                    phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyEmail.equals("")) {
                    phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                cell = new PdfPCell(taxTable);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.RIGHT);

                companyTable.addCell(cell);
                document.add(companyTable);
            } //-------------------------------------------------------------------------------------------------------
            else {
                taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                taxcell = null;

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
                companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
                if (!companyA4Addr.equals("")) {
                    taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    taxTable.addCell(taxcell);
                }

                Phrase phrase = new Phrase();
                phrase.add(new Chunk("City : " + companyCity, cellFont));
                phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell = new PdfPCell(phrase);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                phrase = new Phrase();
                if (!companyPhone.equals("")) {
                    phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyMobile.equals("")) {
                    phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                if (!companyEmail.equals("")) {
                    phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                    taxcell = new PdfPCell(phrase);
                }
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);

                document.add(taxTable);
            }

            PdfPTable Table = new PdfPTable(1);
            Table.setWidthPercentage(100);
            cell = null;

            cell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            Table.addCell(cell);

            document.add(Table);

            if (!newPS.isNoParty()) {
                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                PdfPTable innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                if (!"".equals(billCompanyGSTIN)) {
                    tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                }

                tablecell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(billCompanyPhone + " | " + billCompanyMobile + " | " + billCompanyEmail, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(ShippingName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingAddr1 + " " + ShippingAddr2 + " " + ShippingAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
//            tablecell.setBorder(Rectangle.NO_BORDER);
//            innerTable.addCell(tablecell);
                tablecell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                if (!"".equals(ShippingPhone) && "".equals(ShippingMobile)) {
                    tablecell = new PdfPCell(new Phrase(ShippingPhone + " | " + ShippingMobile, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                } else if (!"".equals(ShippingPhone)) {
                    tablecell = new PdfPCell(new Phrase(ShippingPhone, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                } else if (!"".equals(ShippingMobile)) {
                    tablecell = new PdfPCell(new Phrase(ShippingMobile, cellFont));
                    tablecell.setBorder(Rectangle.NO_BORDER);
                    innerTable.addCell(tablecell);
                }

                table2.addCell(innerTable);

                document.add(table2);
            } else {
                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                PdfPTable innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyGSTIN, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billCompanyState, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(billCompanyPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                innerTable = new PdfPTable(1);

                tablecell = new PdfPCell(new Phrase(ShippingName, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingAddr1 + " " + ShippingAddr2 + " " + ShippingAddr3, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingCity, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(ShippingDistrict, cellFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
//            tablecell.setBorder(Rectangle.NO_BORDER);
//            innerTable.addCell(tablecell);
                tablecell = new PdfPCell(new Phrase(ShippingState, cellFont));
                tablecell.setBorder(Rectangle.NO_BORDER);
                innerTable.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(ShippingPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
                table2.addCell(innerTable);

                document.add(table2);
            }

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidthPercentage(100);
            PdfPCell cell3 = null;

            cell3 = new PdfPCell(new Phrase(billRemarks, cellFont1));
            cell3.setColspan(2);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            table3.addCell(cell3);

            document.add(table3);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProductItemsA5P(Document document, int productLimitInCounter) throws IOException, DocumentException {

        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font productFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font totalFont7 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
            PdfPCell taxcell, cell;
            PdfPTable pdfTable = new PdfPTable(8);
            pdfTable.setWidths(new float[]{5, 31, 6, 16, 11, 9, 10, 13});
            pdfTable.setWidthPercentage(100);
//            pdfTable.setTotalWidth(300);
//            pdfTable.setLockedWidth(true);

            float ht = pdfTable.getTotalHeight();

            taxcell = new PdfPCell(new Phrase("S.No", totalFont7));
            //taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Product/Service (HSN/SAC)", totalFont7));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

//            taxcell = new PdfPCell(new Phrase("HSN/SAC", totalFont8));
//            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
//            pdfTable.addCell(taxcell);
            taxcell = new PdfPCell(new Phrase("GST %", totalFont8));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);
            taxcell = new PdfPCell(new Phrase("Quantity", totalFont7));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate", totalFont7));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);
            
            taxcell = new PdfPCell(new Phrase("Free", totalFont7));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("(per)", totalFont7));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

           

            taxcell = new PdfPCell(new Phrase("Amount", totalFont7));
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(taxcell);
//-------------------------------------------------------------------------------------------------------------------
//            productcount = bosLineItems.size();
            int linecount = 0;
            int counter = 0;

            if (pageEnd == true && pdfDescPrintCnt >= 0 && productAdded) {
                sno = sno - 1;
                pageEnd = false;
            }
            while (counter < productLimitInCounter) {

                String strSNO = Integer.toString(sno + 1);
                if (pageEnd) {
                    break;
                }
                if (sno < psItems.size() && psItems.get(sno).getProduct().isService()) {

                    if (!productAdded) {
                        counter++;
                        linecount++;
                        productDescriptions = new ArrayList<String>();
                        if ((psItems.get(sno).getDescriptionOne() != null) && (!psItems.get(sno).getDescriptionOne().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionOne());
                        }
                        if ((psItems.get(sno).getDescriptionTwo() != null) && (!psItems.get(sno).getDescriptionTwo().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTwo());
                        }
                        if ((psItems.get(sno).getDescriptionThree() != null) && (!psItems.get(sno).getDescriptionThree().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionThree());
                        }
                        if ((psItems.get(sno).getDescriptionFour() != null) && (!psItems.get(sno).getDescriptionFour().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFour());
                        }
                        if ((psItems.get(sno).getDescriptionFive() != null) && (!psItems.get(sno).getDescriptionFive().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFive());
                        }
                        if ((psItems.get(sno).getDescriptionSix() != null) && (!psItems.get(sno).getDescriptionSix().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSix());
                        }
                        if ((psItems.get(sno).getDescriptionSeven() != null) && (!psItems.get(sno).getDescriptionSeven().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSeven());
                        }
                        if ((psItems.get(sno).getDescriptionEight() != null) && (!psItems.get(sno).getDescriptionEight().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionEight());
                        }
                        if ((psItems.get(sno).getDescriptionNine() != null) && (!psItems.get(sno).getDescriptionNine().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionNine());
                        }
                        if ((psItems.get(sno).getDescriptionTen() != null) && (!psItems.get(sno).getDescriptionTen().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTen());
                        }
                        pdfDescCount = productDescriptions.size();
//------------------

//                        counter++;
                        linecount++;
                        cell = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        if (!isRegional) {
                            Phrase phrase = new Phrase();
                            phrase.add(new Chunk(psItems.get(sno).getProductName(), productFont));
                            phrase.add(new Chunk(" (" + psItems.get(sno).getHsnSac() + ") ", totalFontBold));
                            cell = new PdfPCell(phrase);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            cell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        }
                        for (int i = 0; i < 2; i++) {
                            cell = new PdfPCell(new Phrase("", HeaderFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            pdfTable.addCell(cell);
                        }
                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

//                    if (psItems.get(sno).getValue().compareTo(zero) > 0) {
//                        cell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), HeaderFont));
//                    } else {
                        cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), totalFontBold));
//                    }

                        Phrase phrase = new Phrase();
                        phrase.add(new Chunk(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getUqcOneRate()), totalFontBold));
//                    phrase.add(new Chunk(Chunk.NEWLINE + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getUqcTwoRate()), cellFont));
                        cell = new PdfPCell(phrase);
//                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell.setBorder(Rectangle.LEFT);

                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);

                        pdfTable.addCell(cell);
                    }

                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 8) {
                            pageEnd = true;
                            break;
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        for (int j = 0; j < 2; j++) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            pdfTable.addCell(cell);
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        linecount++;
                        counter++;
                        pdfDescPrintCnt++;
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }

                } else if (sno < psItems.size()) {
                    if (!productAdded) {
                        counter++;
                        linecount++;
                        productDescriptions = new ArrayList<String>();
                        if ((psItems.get(sno).getDescriptionOne() != null) && (!psItems.get(sno).getDescriptionOne().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionOne());
                        }
                        if ((psItems.get(sno).getDescriptionTwo() != null) && (!psItems.get(sno).getDescriptionTwo().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTwo());
                        }
                        if ((psItems.get(sno).getDescriptionThree() != null) && (!psItems.get(sno).getDescriptionThree().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionThree());
                        }
                        if ((psItems.get(sno).getDescriptionFour() != null) && (!psItems.get(sno).getDescriptionFour().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFour());
                        }
                        if ((psItems.get(sno).getDescriptionFive() != null) && (!psItems.get(sno).getDescriptionFive().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionFive());
                        }
                        if ((psItems.get(sno).getDescriptionSix() != null) && (!psItems.get(sno).getDescriptionSix().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSix());
                        }
                        if ((psItems.get(sno).getDescriptionSeven() != null) && (!psItems.get(sno).getDescriptionSeven().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionSeven());
                        }
                        if ((psItems.get(sno).getDescriptionEight() != null) && (!psItems.get(sno).getDescriptionEight().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionEight());
                        }
                        if ((psItems.get(sno).getDescriptionNine() != null) && (!psItems.get(sno).getDescriptionNine().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionNine());
                        }
                        if ((psItems.get(sno).getDescriptionTen() != null) && (!psItems.get(sno).getDescriptionTen().equals(""))) {
                            productDescriptions.add(psItems.get(sno).getDescriptionTen());
                        }
                        pdfDescCount = productDescriptions.size();
//-------------------------------------------------------------------------------------------------------------------
                        cell = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        Phrase phrase = new Phrase();
                        if (!isRegional) {
                            phrase.add(new Chunk(psItems.get(sno).getProductName(), productFont));
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            phrase.add(new Chunk(tamilName, productTamilFont));
                        }

                        String hsn = psItems.get(sno).getHsnSac();

                        boolean isHsnNotNull = !(("".equals(hsn)) || (hsn == null));
                        if (isHsnNotNull) {
                            phrase.add(new Chunk(" (" + psItems.get(sno).getHsnSac() + ") ", totalFontBold));
                        }
                        cell = new PdfPCell(phrase);
//                        cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), productFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

//                        cell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), cellFont));
//                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        pdfTable.addCell(cell);
                        cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getIgstPercentage()), cellFont));

                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);
                        int UQC2Value = 0;
                        for (Product product : products) {
                            if (product.getName().equals(psItems.get(sno).getProductName())) {
                                UQC2Value = product.getUQC2Value();
                            }
                        }
                        int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();
                        String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
                        if (UQC1qty > 0) {
                            strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
                            strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            if (UQC2qty > 0) {
                                strQuantity = strQuantity + ", ";
                                strAltQuantity = strAltQuantity + " ";
                            }
                        }
                        if (UQC2qty > 0) {
                            if (UQC1qty == 0) {
                                strQuantity = "";
                                strAltQuantity = "";
                            }
                            strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        }
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else {
//                            counter++;
//                            linecount++;
                            phrase = new Phrase();
                            phrase.add(new Chunk(strQuantity, totalFontBold));
//                            phrase.add(new Chunk(Chunk.NEWLINE + "(" + strAltQuantity + ")", cellFont));
                            cell = new PdfPCell(phrase);
                        }

                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        phrase = new Phrase();
                        phrase.add(new Chunk(currency.format(psItems.get(sno).getUqcOneRate()), totalFontBold));
//                        phrase.add(new Chunk(Chunk.NEWLINE + currency.format(psItems.get(sno).getUqcTwoRate()), cellFont));
                        cell = new PdfPCell(phrase);
//                      cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        
                         cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getFree()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        phrase = new Phrase();
                        phrase.add(new Chunk("(" + getUQCcode(psItems.get(sno).getUqcOne()) + ")", totalFontBold));
//                        if (!psItems.get(sno).getUqcOne().equals(psItems.get(sno).getUqcTwo())) {
//                            if (!psItems.get(sno).getUqcTwo().equals("")) {
//                                phrase.add(new Chunk(Chunk.NEWLINE + "(" + getUQCcode(psItems.get(sno).getUqcTwo()) + ")", cellFont));
//                            }
//                        }
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 8) {
                            pageEnd = true;
                            break;
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);

                        for (int j = 0; j < 2; j++) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            pdfTable.addCell(cell);
                        }

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(" ", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        linecount++;
                        counter++;
                        pdfDescPrintCnt++;
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }
                }
                sno++;

            }
            linecount = (productLimitA5P - linecount);
            Phrase phrase = new Phrase();
            for (int i = 0; i < linecount; i++) {
                phrase.add(new Chunk(" ", totalFontBold));
//              phrase.add(new Chunk(Chunk.NEWLINE + " 2", cellFont));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

//                phrase.add(new Chunk(" ", totalFontBold));
//                cell = new PdfPCell(phrase);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                pdfTable.addCell(cell);
                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

            }
            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void afterAddProductItemsContinuedA5P(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

//            PdfPTable pdfTable = new PdfPTable(9);
//            pdfTable.setWidths(new float[]{8, 25, 12, 20, 15, 10, 15, 10, 20});
//            pdfTable.setTotalWidth(555);
//            pdfTable.setLockedWidth(true);
            PdfPTable pdfTable = new PdfPTable(2);
            pdfTable.setWidthPercentage(100);

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(3);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItems1A5P(Document document) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

        PdfPCell taxcell, cell;
        PdfPTable pdfTable = new PdfPTable(8);
        pdfTable.setWidths(new float[]{5, 31, 6, 16, 11, 9, 10, 13});
        pdfTable.setWidthPercentage(100);

        if (isIGST) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("IGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billIGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("CGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billCGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("SGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billSGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }

        if (billRoundOff.compareTo(BigDecimal.ZERO) != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Round Off", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

//            cell = new PdfPCell(new Phrase("", HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Net Amount", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

//        cell = new PdfPCell(new Phrase("", HeaderFont));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setBorder(Rectangle.BOX);
//        pdfTable.addCell(cell);
        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("₹" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);
        document.add(pdfTable);

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase("Amount Chargeable(in words)", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("E. & O.E", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrency(billNetAmount.toString()), HeaderFont));
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

//        for (int i = 0; i < 2; i++) {
//        cell = new PdfPCell(new Phrase(" ", HeaderFont));
//        cell.setColspan(3);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//        pdfTable.addCell(cell);
//        }
        document.add(pdfTable);

    }

    public void addFooterA5P(Document document) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

            PdfPTable outerTable = new PdfPTable(2);
            outerTable.setWidthPercentage(100);

            PdfPTable pdfTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(new Phrase(companyFooterHeading, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            pdfTable = new PdfPTable(1);

            cell = new PdfPCell(new Phrase("for " + companyName.toUpperCase(), totalFontBold));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            document.add(outerTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addHSNTableA5P(Document document, int HSNno, int hsnLimitInCounter) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD, BaseColor.BLACK);
            PdfPCell taxcell, cell;

            PdfPTable pdfTable;
            PdfPCell cell6 = null;

            if (isIGST) {
                pdfTable = new PdfPTable(5);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 40, 20, 40, 50});

                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);

                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("IGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
//                hsncount = psTaxSummaries.size();
                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
                    if ((counter + 1) == (hsnLimitInCounter)) {
                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
//                        hsnno++;
                    } else {
                        System.err.println("########" + psTaxSummaries.get(hsnno).getHsnSac());
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getTaxableValue().floatValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                    }
                    hsnno++;
                }

            } else {
                pdfTable = new PdfPTable(7);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 35, 20, 35, 20, 35, 55});

                cell6 = new PdfPCell(new Phrase("HSN/SAC", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Taxable\n value ", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);

                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("CGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("SGST", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                cell6.setColspan(2);
                //   cell6.setBorder(Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Total\nTax Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", totalFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
//                hsncount = psTaxSummaries.size();
                for (int counter = 0; counter < hsnLimitInCounter; counter++) {

                    System.err.println("########" + psTaxSummaries.get(0).getHsnSac());
//                    System.err.println("########" + psTaxSummaries.get(hsnno).getIgstPercentage());
                    if ((counter + 1) == (hsnLimitInCounter)) {
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                    } else {
                        cell = new PdfPCell(new Phrase(psTaxSummaries.get(hsnno).getHsnSac(), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTaxableValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()) + "%", cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), totalFontBold));
                        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
//                        hsnno++;
                    }
                    hsnno++;
                }
            }

            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void lastLineA5P(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    public void createPdfA5P(Boolean isPrint, Boolean isPreview) {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            Rectangle layout = new Rectangle(PageSize.A5);
            Document document = new Document(layout, 20, 20, 20, 20);
            String path = "";
            Boolean isApproved = false;
            String nowFilePath = "";
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Tax Invoice" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Tax Invoice" + "-" + newPS.getBillNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }

            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));

                document.open();

                for (String copyName : nameOfPDFCopies) {
                    sno = 0;
                    hsnno = 0;
                    document.newPage();
                    String invoiceCopyName = copyName;
                    int productcount = psItems.size();

//-----------------------------------------------------------------------------------------------------------------------
                    int totDescCount = 0;

                    //get total descriptions count
                    for (PurchaseSaleLineItem psItem : psItems) {
                        totDescCount = totDescCount + psItem.getDescriptionCount();
                    }

                    //total line count (sum of product count and product descriptions count)
                    productcount = productcount + totDescCount;
                    int pagecount = 0;
                    int remainingProduct = 0;

                    //set number of pages by page line count (here 8)
                    pagecount = (productcount / productLimitA5P);
                    if ((productcount % productLimitA5P) > 0) {
                        pagecount += 1;
                    }
                    if (hsncount > 3) {
                        pagecount += 1;
                    }
                    if (pagecount == 1) {
                        int limitcount = 0;

                        //add company details and bill info details
                        addInfoA5P(document, pagecount, invoiceCopyName);

                        //set current page linit count for lines
                        if (productcount == productLimitA5P) {
                            limitcount = productcount;
                        } else {
                            limitcount = productcount % productLimitA5P;
                        }
                        addProductItemsA5P(document, (limitcount));
                        afterAddProductItems1A5P(document);
                        addHSNTableA5P(document, hsnno, hsncount);
                        addFooterA5P(document);
                        lastLineA5P(document, pagecount, pagecount);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (hsncount > 3 && pagecount == i) {
                                addInfoA5P(document, i, invoiceCopyName);
                                addHSNTableA5P(document, hsnno, hsncount);
                                addFooterA5P(document);
                                lastLineA5P(document, i, pagecount);
                            } else if ((hsncount > 3) && pagecount == i + 1) {
                                remainingProduct = productcount - (remainingProduct * productLimitA5P);
                                addInfoA5P(document, i, invoiceCopyName);
                                addProductItemsA5P(document, remainingProduct);
                                afterAddProductItems1A5P(document);
                                afterAddProductItemsContinuedA5P(document);
                                lastLineA5P(document, i, pagecount);
                                document.newPage();
                            } else if (pagecount == i) {
                                remainingProduct = productcount - (remainingProduct * productLimitA5P);
                                addInfoA5P(document, i, invoiceCopyName);
                                addProductItemsA5P(document, remainingProduct);
                                afterAddProductItems1A5P(document);
                                addHSNTableA5P(document, hsnno, hsncount);
                                addFooterA5P(document);
                                lastLineA5P(document, i, pagecount);
                            } else {
                                addInfoA5P(document, i, invoiceCopyName);
                                addProductItemsA5P(document, productLimitA5P);
                                afterAddProductItemsContinuedA5P(document);
                                lastLineA5P(document, i, pagecount);
                                document.newPage();
                                remainingProduct++;
                            }
                        }
                    }
                    productcount = productcount / 2;
                }
                document.close();
                fos.close();

                if (isPrint) {
                    pdfPrint(path, isPreview);
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
//                if (isPreview) {
//                    createImagePdf(path);
//                }

            }
            com.alee.laf.WebLookAndFeel.install();
//            if (!isPrint && isApproved) {
//                JOptionPane.showMessageDialog(null, "Bill Downloaded at " + path, "Message", JOptionPane.INFORMATION_MESSAGE);
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PrintException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addInfoA5L(Document document, String invoiceCopyName) throws DocumentException, IOException {
//        BaseFont bf;

//        InputStream is = this.getClass().getResourceAsStream("/Fonts/calibri.ttf");
//        bf = BaseFont.createFont(is.toString(), "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);

//        com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
//            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        Phrase p;
        PdfPCell taxcell;

        PdfPTable outerTable = new PdfPTable(3);
        outerTable.setWidthPercentage(100);
        outerTable.setWidths(new float[]{35, 35, 35});

        Phrase p1 = new Phrase();

        p1.add(new Chunk("Bill No. : ", footerfont));
        p1.add(new Chunk(String.valueOf(billNo), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceHeading, boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
        outerTable.addCell(taxcell);

        p1 = new Phrase();

        p1.add(new Chunk(invoiceCopyName + " | ", footerfont));
        p1.add(new Chunk("Date : ", footerfont));
        p1.add(new Chunk(String.valueOf(billDate), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        document.add(outerTable);

        PdfPTable outerTable1 = new PdfPTable(3);
        outerTable1.setWidthPercentage(100);
        outerTable1.setWidths(new float[]{50, 50, 50});

        PdfPTable Table1 = new PdfPTable(1);

        taxcell = new PdfPCell(new Phrase(companyName + ",", boldcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!companyA5LAddrLine1.equalsIgnoreCase(" ")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }
        outerTable1.addCell(Table1);
        if (!newPS.isNoParty()) {
            Table1 = new PdfPTable(1);
            taxcell = new PdfPCell(new Phrase("Bill to Party", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyName + ",", boldcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            if (!billCompanyAddr1.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr1, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                if (!"".equals(billCompanyGSTIN)) {
                    Phrase pg = new Phrase();
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!billCompanyAddr2.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                if (!"".equals(billCompanyGSTIN)) {
                    Phrase pg = new Phrase();
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }
                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!billCompanyAddr3.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                if (!"".equals(billCompanyGSTIN)) {
                    Phrase pg = new Phrase();
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else {
                if (!"".equals(billCompanyGSTIN)) {
                    Phrase pg = new Phrase();
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            outerTable1.addCell(Table1);

            Table1 = new PdfPTable(1);
            taxcell = new PdfPCell(new Phrase("Ship to Party", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setColspan(2);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(ShippingName + ",", boldcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            if (!ShippingAddr1.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr1, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!ShippingAddr2.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!ShippingAddr3.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else {
                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }
            outerTable1.addCell(Table1);

            document.add(outerTable1);
        } else {
            Table1 = new PdfPTable(1);
            taxcell = new PdfPCell(new Phrase("Bill to Party", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            Table1.addCell(taxcell);

            if ("".equals(billCompanyName)) {
                taxcell = new PdfPCell(new Phrase(billCompanyName, boldcalibri));
            } else {
                taxcell = new PdfPCell(new Phrase(billCompanyName + ",", boldcalibri));
            }
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            if (!billCompanyAddr1.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr1, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                Phrase pg = new Phrase();
                if (!"".equals(billCompanyGSTIN)) {
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!billCompanyAddr2.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                Phrase pg = new Phrase();
                if (!"".equals(billCompanyGSTIN)) {
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!billCompanyAddr3.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                Phrase pg = new Phrase();
                if (!"".equals(billCompanyGSTIN)) {
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else {
                Phrase pg = new Phrase();
                if (!"".equals(billCompanyGSTIN)) {
                    pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                    pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                    taxcell = new PdfPCell(pg);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    Table1.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            outerTable1.addCell(Table1);

            Table1 = new PdfPTable(1);
            taxcell = new PdfPCell(new Phrase("Ship to Party", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setColspan(2);
            Table1.addCell(taxcell);

            if ("".equals(ShippingName)) {
                taxcell = new PdfPCell(new Phrase(ShippingName, boldcalibri));
            } else {
                taxcell = new PdfPCell(new Phrase(ShippingName + ",", boldcalibri));
            }
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            if (!ShippingAddr1.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr1, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!ShippingAddr2.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr2, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else if (!ShippingAddr3.equalsIgnoreCase("")) {
                taxcell = new PdfPCell(new Phrase(ShippingAddr3, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            } else {
                taxcell = new PdfPCell(new Phrase(ShippingCity + " " + ShippingDistrict, normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(ShippingState + " (" + new StateCode().getStateCode(ShippingState) + ")", normalcalibri));
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }
            outerTable1.addCell(Table1);

            document.add(outerTable1);
        }
    }

    private void addProductItemsA5L(Document document, int productLimitInCounter) throws DocumentException, IOException {
//
//        BaseFont bf;
//
//        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);

        Phrase p;
        PdfPCell taxcell, cell;
        if (isIGST) {
            PdfPTable thirdTable = new PdfPTable(7);
            thirdTable.setWidthPercentage(100);
            thirdTable.setWidths(new float[]{5, 29, 11, 13, 9,10, 15, 15});

            taxcell = new PdfPCell(new Phrase("S.No", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Qty", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate ", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);
            
            taxcell = new PdfPCell(new Phrase("Free ", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("IGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);
            int linecount = 0;
            int counter = 0;

            if (pageEnd == true && pdfDescPrintCnt >= 0 && productAdded) {
                sno = sno - 1;
                pageEnd = false;
            }

            while (counter < productLimitInCounter) {
                counter++;
                pdfA5LTotalDescCnt--;
                linecount++;
                String strSNO = Integer.toString(sno + 1);
                if (pageEnd) {
                    break;
                }
                if (sno < psItems.size() && psItems.get(sno).getProduct().isService()) {
                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        long productId = psItems.get(sno).getProduct().getProductId();
                        productDescriptions = productDescA5P.get(productId);
                        pdfDescCount = productDescriptions.size();
                        if (pdfA5LTotalDescCnt == 1) {
//                        String strSNO = Integer.toString(sno + 1);
                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                cell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(cell);
                            }

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                                taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            } else {
                                taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            }
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                        } else {
//                        String strSNO = Integer.toString(sno + 1);
                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                cell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(cell);
                            }

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                                taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            } else {
                                taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            }
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);
                        }
                    }
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 11) {
                            pageEnd = true;
                            break;
                        }

                         thirdTable.setWidths(new float[]{5, 29, 11, 13, 9,10, 15, 15});
                        if (pdfA5LTotalDescCnt == 1) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 5; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        } else {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 5; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        }

                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }
                } else if (sno < psItems.size()) {
                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        long productId = psItems.get(sno).getProduct().getProductId();
                        productDescriptions = productDescA5P.get(productId);
                        pdfDescCount = productDescriptions.size();
                        if (pdfA5LTotalDescCnt == 1) {
//                            String strSNO = Integer.toString(sno + 1);

                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            }
                            taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            p = new Phrase();
                            int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                            if (isEqual == 0) {
                                taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                            } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                taxcell = new PdfPCell(p);
                            } else {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                                taxcell = new PdfPCell(p);
                            }
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                            
                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getFree()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                        } else {
//                            String strSNO = Integer.toString(sno + 1);

                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            } else {

                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            }

                            taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            p = new Phrase();
                            int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                            if (isEqual == 0) {
                                taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                            } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                taxcell = new PdfPCell(p);
                            } else {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                                taxcell = new PdfPCell(p);
                            }
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);
                        }
                    }
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 11) {
                            pageEnd = true;
                            break;
                        }

                         thirdTable.setWidths(new float[]{5, 29, 11, 13, 9,10, 15, 15});
                        if (pdfA5LTotalDescCnt == 1) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 5; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        } else {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 5; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
//
//                            cell = new PdfPCell(new Phrase(" ", cellFont));
//                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        }

                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }
                }

                sno++;
            }
            linecount = (11 - linecount);
            for (int i = 0; i < linecount; i++) {
                taxcell = new PdfPCell(new Phrase(" ", normalcalibri));
                taxcell.setColspan(8);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                thirdTable.addCell(taxcell);
            }

            document.add(thirdTable);

        } else {
            PdfPTable thirdTable = new PdfPTable(8);
            thirdTable.setWidthPercentage(100);
            thirdTable.setWidths(new float[]{6, 31, 10, 13, 13, 15, 15, 14});

            taxcell = new PdfPCell(new Phrase("S.No", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Qty", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate ", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("CGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("SGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);
            int linecount = 0;
            int counter = 0;

            if (pageEnd == true && pdfDescPrintCnt >= 0 && productAdded) {
                sno = sno - 1;
                pageEnd = false;
            }
            while (counter < productLimitInCounter) {
                counter++;
                linecount++;
                String strSNO = Integer.toString(sno + 1);
                if (pageEnd) {
                    break;
                }
                if (sno < psItems.size() && !psItems.get(sno).getProduct().isService()) {
                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        long productId = psItems.get(sno).getProduct().getProductId();
                        productDescriptions = productDescA5P.get(productId);
                        pdfDescCount = productDescriptions.size();
                        if (pdfA5LTotalDescCnt == 1) {
                            pdfA5LTotalDescCnt--;
//                            String strSNO = Integer.toString(sno + 1);

                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            }

                            taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            p = new Phrase();
                            int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                            if (isEqual == 0) {
                                taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                            } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                taxcell = new PdfPCell(p);
                            } else {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                                taxcell = new PdfPCell(p);
                            }
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase2 = new Phrase();
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                            phrase2.add(new Chunk("(", normalcalibri));
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                            phrase2.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase2);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                        } else {
//                            String strSNO = Integer.toString(sno + 1);
                            pdfA5LTotalDescCnt--;
                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            }
                            taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            p = new Phrase();
                            int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                            if (isEqual == 0) {
                                taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                            } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                taxcell = new PdfPCell(p);
                            } else {
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                                p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                                taxcell = new PdfPCell(p);
                            }
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase2 = new Phrase();
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                            phrase2.add(new Chunk("(", normalcalibri));
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                            phrase2.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase2);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);
                        }
                    }

                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 11) {
                            pageEnd = true;
                            break;
                        }
                        if (pdfA5LTotalDescCnt == 1) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 2; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        } else {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 2; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        }
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }

                } else if (sno < psItems.size()) {
                    if (!productAdded) {
                        productDescriptions = new ArrayList<String>();
                        long productId = psItems.get(sno).getProduct().getProductId();
                        productDescriptions = productDescA5P.get(productId);
                        pdfDescCount = productDescriptions.size();
                        if (pdfA5LTotalDescCnt == 1) {
                            pdfA5LTotalDescCnt--;

                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                                thirdTable.addCell(taxcell);
                            }

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT | Rectangle.BOTTOM);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            Phrase phrase2 = new Phrase();
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                            phrase2.add(new Chunk("(", normalcalibri));
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                            phrase2.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase2);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);

                            if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                                taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            } else {
                                taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            }
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                            thirdTable.addCell(taxcell);
                        } else {
                            pdfA5LTotalDescCnt--;
//                            String strSNO = Integer.toString(sno + 1);

                            taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (!isRegional) {
                                taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            } else {
                                String name = psItems.get(sno).getProduct().getTamilName();
                                String tamilName = changeToBamini(name);
                                taxcell = new PdfPCell(new Phrase(tamilName, productTamilFont));
                                taxcell.setBorder(Rectangle.RIGHT);
                                thirdTable.addCell(taxcell);
                            }

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            taxcell = new PdfPCell(new Phrase("", normalcalibri));
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase1 = new Phrase();
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                            phrase1.add(new Chunk("(", normalcalibri));
                            phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                            phrase1.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase1);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            Phrase phrase2 = new Phrase();
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                            phrase2.add(new Chunk("(", normalcalibri));
                            phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                            phrase2.add(new Chunk(")", normalcalibri));

                            taxcell = new PdfPCell(phrase2);
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);

                            if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                                taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            } else {
                                taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), normalcalibri));
                            }
                            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            taxcell.setBorder(Rectangle.RIGHT);
                            thirdTable.addCell(taxcell);
                        }
                    }
                    while (pdfDescPrintCnt < pdfDescCount) {
                        descEnd = false;
                        productAdded = true;
                        if (counter == 11) {
                            pageEnd = true;
                            break;
                        }
                        if (pdfA5LTotalDescCnt == 1) {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 2; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        } else {
                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase("       " + productDescriptions.get(pdfDescPrintCnt), footerfont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            thirdTable.addCell(cell);

                            for (int j = 0; j < 2; j++) {
                                cell = new PdfPCell(new Phrase(" ", cellFont));
                                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                thirdTable.addCell(cell);
                            }

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);

                            cell = new PdfPCell(new Phrase(" ", cellFont));
                            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            thirdTable.addCell(cell);
                            linecount++;
                            counter++;
                            pdfA5LTotalDescCnt--;
                            pdfDescPrintCnt++;
                        }
                    }
                    if (pdfDescPrintCnt != 0 && (pdfDescPrintCnt % pdfDescCount == 0)) {
                        descEnd = true;
                        productAdded = false;
                        pdfDescPrintCnt = 0;
                    }
                }
                sno++;
            }

            linecount = (11 - linecount);
            for (int i = 0; i < linecount; i++) {
                taxcell = new PdfPCell(new Phrase(" ", normalcalibri));
                taxcell.setColspan(8);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                thirdTable.addCell(taxcell);
            }
            document.add(thirdTable);
        }
    }

    private void afterAddProductItemsA5L(Document document) {
        try {
//            BaseFont bf;
//
//            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font headercalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font normalcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);

            Phrase p;
            PdfPCell taxcell;

            if (isIGST) {
                PdfPTable thirdTable = new PdfPTable(7);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{5, 30, 11, 15, 9, 15, 15});

                if (billRoundOff.compareTo(BigDecimal.ZERO) != 0) {
                    taxcell = new PdfPCell(new Phrase("Round Off", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(6);
                    thirdTable.addCell(taxcell);

                    if (billRoundOff.compareTo(BigDecimal.ZERO) > 0) {
                        taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldcalibri));
                    }
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(6);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);
                }
                document.add(thirdTable);
            } else {
                PdfPTable thirdTable = new PdfPTable(8);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{6, 30, 10, 14, 13, 15, 15, 14});

                if (billRoundOff.compareTo(BigDecimal.ZERO) != 0) {
                    taxcell = new PdfPCell(new Phrase("Round Off", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(7);
                    thirdTable.addCell(taxcell);

                    if (billRoundOff.compareTo(BigDecimal.ZERO) > 0) {
                        taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldcalibri));
                    }
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                }

                taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setColspan(7);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                document.add(thirdTable);

            }

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void afterAddProductItemsContinuedA5L(Document document) {
        try {
//            BaseFont bf;
//
//            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new float[]{70, 30});
            table.setTotalWidth(560);
            table.setWidthPercentage(100);

            PdfPCell cellfooter = null;

            cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.TOP | Rectangle.LEFT);
            table.addCell(cellfooter);

            for (int i = 0; i < 3; i++) {
                cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
                cellfooter.setColspan(2);
                cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellfooter.setBorder(Rectangle.NO_BORDER);
                cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                table.addCell(cellfooter);
            }

            cellfooter = new PdfPCell(new Phrase("Continued... ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            table.addCell(cellfooter);
            document.add(table);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);

        }
    }

    private void addFooterA5L(Document document) throws DocumentException, IOException {
//        BaseFont bf;
//
//        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);

        PdfPTable Outertable = new PdfPTable(3);
        Outertable.setWidths(new float[]{40, 30, 30});

        Outertable.setTotalWidth(560);
        Outertable.setWidthPercentage(100);

        PdfPTable table = new PdfPTable(1);
        PdfPCell cellfooter = null;

        cellfooter = new PdfPCell(new Phrase(companyFooterHeading, footerfontbold));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineOne, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineTwo, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineThree, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineFour, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        cellfooter = new PdfPCell(new Phrase(billRemarks, boldcalibri));
        cellfooter.setRowspan(5);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        Phrase p2 = new Phrase();
        p2.add(new Chunk("for ", cellFont1));
        p2.add(new Chunk(companyName.toUpperCase(), footerfontbold));
        cellfooter = new PdfPCell(p2);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(" ", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        document.add(Outertable);

    }

    public void lastLineA5L(Document document, int currentPage, int pageCount) throws DocumentException {
        //            BaseFont bf;
//
//            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);
        Phrase p;
        PdfPCell cell;
        p = new Phrase(" ", headercalibri);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);
        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);
        p = new Phrase(" ", normalcalibri);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        p = new Phrase("Page " + currentPage + " of " + pageCount, normalcalibri);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
//        p = new Phrase("This is a Computer Generated Invoice", cellFont);
//        cell = new PdfPCell(p);
//        cell.setColspan(2);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setBorder(Rectangle.NO_BORDER);
//        footerTable.addCell(cell);
        document.add(footerTable);
    }

    private void createPdfA5L(Boolean isPrint, Boolean isPreview) throws IOException, DocumentException {
        try {

            Rectangle layout = new Rectangle(PageSize.A5.rotate());
            Document document = new Document(layout, 20, 15, 15, 15);
            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Tax Invoice" + "-" + newPS.getBillNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Tax Invoice" + "-" + newPS.getBillNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                for (String copyName : nameOfPDFCopies) {
                    sno = 0;
                    document.newPage();
                    String invoiceCopyName = copyName;

                    int productcount = psItems.size();

                    int totalDescCount = 0;
//                    for (PurchaseSaleLineItem psItem : psItems) {
//                        totDescCount = totDescCount + psItem.getDescriptionCount();
//                    }

                    productDescA5P = new HashMap<Long, ArrayList<String>>();
                    for (PurchaseSaleLineItem psItem : psItems) {
                        ArrayList<String> productDesc = new ArrayList<String>();
                        if ((psItem.getDescriptionOne() != null) && (!psItem.getDescriptionOne().equals(""))) {
                            String strLine = psItem.getDescriptionOne();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionTwo() != null) && (!psItem.getDescriptionTwo().equals(""))) {
                            String strLine = psItem.getDescriptionTwo();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionThree() != null) && (!psItem.getDescriptionThree().equals(""))) {
                            String strLine = psItem.getDescriptionThree();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionFour() != null) && (!psItem.getDescriptionFour().equals(""))) {
                            String strLine = psItem.getDescriptionFour();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionFive() != null) && (!psItem.getDescriptionFive().equals(""))) {
                            String strLine = psItem.getDescriptionFive();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionSix() != null) && (!psItem.getDescriptionSix().equals(""))) {
                            String strLine = psItem.getDescriptionSix();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionSeven() != null) && (!psItem.getDescriptionSeven().equals(""))) {
                            String strLine = psItem.getDescriptionSeven();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionEight() != null) && (!psItem.getDescriptionEight().equals(""))) {
                            String strLine = psItem.getDescriptionEight();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionNine() != null) && (!psItem.getDescriptionNine().equals(""))) {
                            String strLine = psItem.getDescriptionNine();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        if ((psItem.getDescriptionTen() != null) && (!psItem.getDescriptionTen().equals(""))) {
                            String strLine = psItem.getDescriptionTen();
//                            if (strLine.length() > 38) {
//                                productDesc.add(strLine.substring(0, 37));
//                                productDesc.add(strLine.substring(37));
//                                totalDescCount += 2;
//                            } else {
                            productDesc.add(strLine);
                            totalDescCount++;
//                            }
                        }
                        productDescA5P.put(psItem.getProduct().getProductId(), productDesc);
                    }

//                    productcount = productcount * 2;
                    productcount = productcount + totalDescCount;
                    pdfA5LTotalDescCnt = productcount;
                    int pagecount = 0, remainingProduct = 0;
                    pagecount = (productcount / productLimitA5L);
                    if ((productcount % productLimitA5L) > 0) {
                        pagecount += 1;
                    }
                    if (pagecount == 1) {
                        int limitcount = 0;
                        addInfoA5L(document, invoiceCopyName);
                        if (productcount == productLimitA5L) {
                            limitcount = productcount;
                        } else {
                            limitcount = productcount % productLimitA5L;
                        }
                        addProductItemsA5L(document, (limitcount));
                        afterAddProductItemsA5L(document);
                        addFooterA5L(document);
                        lastLineA5L(document, pagecount, pagecount);
                    } else {
                        for (int i = 1; i <= pagecount; i++) {
                            if (pagecount == i) {
                                remainingProduct = productcount - (remainingProduct * productLimitA5L);
                                addInfoA5L(document, invoiceCopyName);
                                addProductItemsA5L(document, remainingProduct);
                                afterAddProductItemsA5L(document);
                                addFooterA5L(document);
                                lastLineA5L(document, i, pagecount);
                            } else {
                                addInfoA5L(document, invoiceCopyName);
                                addProductItemsA5L(document, productLimitA5L);
                                afterAddProductItemsContinuedA5L(document);
                                lastLineA5L(document, i, pagecount);
                                document.newPage();
                                remainingProduct++;
                            }
                        }
                    }
                    productcount = productcount / 2;
                }
                document.close();
                fos.close();
                if (isPrint) {
                    pdfPrint(path, isPreview);
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
//                if (isPreview) {
//                    createImagePdf(path);
//
//                }
            }
            com.alee.laf.WebLookAndFeel.install();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (PrintException ex) {
            Logger.getLogger(TaxInvoiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createImagePdf(String filePath) throws IOException {
        try {
            File pdfFile;

            pdfFile = new File(filePath);
            PDDocument doc = PDDocument.load(pdfFile);
            int count = doc.getNumberOfPages();
            for (int i = 1; i <= count; i++) {
                RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
                FileChannel channel = raf.getChannel();
                MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                PDFFile pdf = new PDFFile(buf);

                PDFPage page = pdf.getPage(i);

                java.awt.Rectangle rect = new java.awt.Rectangle(0, 0, (int) page.getBBox().getWidth(),
                        (int) page.getBBox().getHeight());
                BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
                        BufferedImage.TYPE_INT_RGB);

                Image image = page.getImage(rect.width, rect.height, // width & height
                        rect, // clip rect
                        null, // null for the ImageObserver
                        true, // fill background with white
                        true // block until drawing is done
                );
                Graphics2D bufImageGraphics = bufferedImage.createGraphics();
                bufImageGraphics.drawImage(image, 0, 0, null);
                ImageIO.write(bufferedImage, "PNG", new File(filePath + "bill" + i + ".png"));
                raf.close();
            }
            random++;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addHSNTableA7(Document document, int HSNno, int hsnLimitInCounter) {
        try {
            com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

            PdfPCell taxcell, cell;

            PdfPTable pdfTable;
            PdfPCell cell6 = null;

            if (isIGST) {
                pdfTable = new PdfPTable(4);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 20, 30, 30});

                cell6 = new PdfPCell(new Phrase("GST %", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("IGST", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Tax\n Amount", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.TOP);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
                hsncount = psTaxSummaries.size();

                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
                    if ((counter + 1) == (hsnLimitInCounter)) {
                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), boldFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        hsnno++;
                    } else {
                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getIgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), boldFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        hsnno++;
                    }
                }

            } else {
                pdfTable = new PdfPTable(6);
                pdfTable.setWidthPercentage(100);
                pdfTable.setSpacingAfter(10f);
                pdfTable.setWidths(new float[]{20, 20, 30, 20, 30, 30});

                cell6 = new PdfPCell(new Phrase("GST %", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("CGST", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP);
                cell6.setColspan(2);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("SGST", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                //  cell6.setBorder(Rectangle.NO_BORDER);
                cell6.setBorder(Rectangle.TOP);
                cell6.setColspan(2);
                //   cell6.setBorder(Rectangle.BOTTOM);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Tax\n Amount", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.TOP);
                cell6.setRowspan(2);
                pdfTable.addCell(cell6);

                //2nd row
                cell6 = new PdfPCell(new Phrase("", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell6.setBorder(Rectangle.NO_BORDER);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

                cell6 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell6.setBorder(Rectangle.NO_BORDER);
//                cell6.setBorder(Rectangle.TOP);
                pdfTable.addCell(cell6);

//-------------------------------------------------------------------------------------------------------------------
                hsncount = psTaxSummaries.size();

                for (int counter = 0; counter < hsnLimitInCounter; counter++) {
                    if ((counter + 1) == (hsnLimitInCounter)) {

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), boldFont));
                        cell.setBorder(Rectangle.BOTTOM);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        hsnno++;
                    } else {
                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getIgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getCgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getCgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(Float.toString(psTaxSummaries.get(hsnno).getSgstPercentage()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getSgstValue()), cellFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);

                        cell = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psTaxSummaries.get(hsnno).getTotalTaxValue()), boldFont));
                        cell.setBorder(Rectangle.NO_BORDER);
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        pdfTable.addCell(cell);
                        hsnno++;
                    }

                }
            }

            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(TaxInvoiceNew.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createPdfA7(Boolean isPrint, Boolean isPreview) throws FileNotFoundException, DocumentException {
        try {
            Rectangle layout = new Rectangle(PageSize.A7);
            Document document = new Document(layout, 15, 15, 3, 3);

            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Tax Invoice" + "-" + newPS.getBillNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        try {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());

                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(TaxInvoiceNew.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (InstantiationException ex) {
                            Logger.getLogger(TaxInvoiceNew.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(TaxInvoiceNew.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (UnsupportedLookAndFeelException ex) {
                            Logger.getLogger(TaxInvoiceNew.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Tax Invoice" + "-" + newPS.getBillNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();

                Phrase p;

                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell = null;

                taxcell = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                if (!companyA7AddrLine1.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine1, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine2.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine3.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else {
                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase("State : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);
                //document.add(taxTable);

                taxcell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                document.add(taxTable);

                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                tablecell = new PdfPCell(new Phrase("No: " + billNo, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                table2.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billDate, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                table2.addCell(tablecell);

                document.add(table2);

                PdfPTable table3 = new PdfPTable(5);
                table3.setWidthPercentage(100);
                table3.setWidths(new float[]{7, 40, 20, 14, 25});
                PdfPCell cell3 = null;

                cell3 = new PdfPCell(new Phrase(" ", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Description", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Qty", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                for (int count = 0; count < psItems.size(); count++) {

                    String strSNO = Integer.toString(sno + 1);
                    if (!psItems.get(sno).getProduct().isService()) {

                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        if (!isRegional) {
                            cell3 = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                            cell3.setBorder(Rectangle.NO_BORDER);
                            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table3.addCell(cell3);
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            cell3 = new PdfPCell(new Phrase(tamilName, productTamilFont));
                            cell3.setBorder(Rectangle.NO_BORDER);
                            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table3.addCell(cell3);
                        }

                        int UQC2Value = 0;
                        for (Product product : products) {
                            if (product.getName().equals(psItems.get(sno).getProductName())) {
                                UQC2Value = product.getUQC2Value();
                            }
                        }
                        int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();
                        String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
                        if (UQC1qty > 0) {
                            strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
                            strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            if (UQC2qty > 0) {
                                strQuantity = strQuantity + ", ";
                                strAltQuantity = strAltQuantity + ", ";
                            }
                        }
                        if (UQC2qty > 0) {
                            if (UQC1qty == 0) {
                                strQuantity = "";
                                strAltQuantity = "";
                            }
                            strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        }

                        cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                        Phrase phrase = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            cell3 = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else {
                            phrase.add(new Chunk(strQuantity, cellFont));
                            phrase.add(new Chunk(Chunk.NEWLINE + strAltQuantity, cellFont));
                            cell3 = new PdfPCell(phrase);
                        }

                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                    } else {
                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        if (!isRegional) {
                            cell3 = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                            cell3.setBorder(Rectangle.NO_BORDER);
                            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table3.addCell(cell3);
                        } else {
                            String name = psItems.get(sno).getProduct().getTamilName();
                            String tamilName = changeToBamini(name);
                            cell3 = new PdfPCell(new Phrase(tamilName, productTamilFont));
                            cell3.setBorder(Rectangle.NO_BORDER);
                            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table3.addCell(cell3);
                        }

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(psItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);
                    }
                    sno++;
                }
                cell3 = new PdfPCell(new Phrase("Total Amount ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billTotal), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("GST Amount ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billTotalTax), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                if (billRoundOff.compareTo(BigDecimal.ZERO) != 0) {
                    cell3 = new PdfPCell(new Phrase("Round off", cellFont));
                    cell3.setColspan(4);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(cell3);

                    if (billRoundOff.compareTo(BigDecimal.ZERO) > 0) {
                        cell3 = new PdfPCell(new Phrase("+" + IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldFont));
                    } else {
                        cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billRoundOff), boldFont));
                    }
                    cell3.setBorder(Rectangle.NO_BORDER);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);
                }

                cell3 = new PdfPCell(new Phrase("NET AMOUNT ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                document.add(table3);

                addHSNTableA7(document, hsnno, hsncount);
                table3 = new PdfPTable(5);
                table3.setWidthPercentage(100);
                table3.setWidths(new float[]{7, 40, 20, 14, 25});

                cell3 = new PdfPCell(new Phrase("RS. " + IndianCurrencyUtil.convertToIndianCurrencyFormat(billNetAmount), boldFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);
                document.add(table3);

                document.close();

                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path, isPreview);

                    } catch (PrintException ex) {
                        Logger.getLogger(TaxInvoiceNew.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
//                if (isPreview) {
//                    createImagePdf(path);
//
//                }
            }
            com.alee.laf.WebLookAndFeel.install();

        } catch (IOException | DocumentException ex) {
            Logger.getLogger(A7Bill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfPrint(String path, Boolean isPrintPreview) throws PrintException, IOException {
        if (isPrintPreview) {
            createImagePdf(path);
            billPreview = new BillPreview();
            billPreview.showBillPreview(path, 1);
            billPreview.setVisible(true);

        } else {
            PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
            DocPrintJob printJob = printService.createPrintJob();
            PDDocument pdDocument = PDDocument.load(new File(path));
            PDFPageable pdfPageable = new PDFPageable(pdDocument);
            SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
            printJob.print(doc, null);
            pdDocument.close();
        }
    }

    public String changeToBamini(String text) {

        text = text.replace("ஸ்ரீ", "=");
        text = text.replace(",", ">");
        text = text.replace("ஜௌ", "n[s");
        text = text.replace("ஜோ", "N[h");
        text = text.replace("ஜொ", "n[h");
        text = text.replace("ஜா", "[h");
        text = text.replace("ஜி", "[p");
        text = text.replace("ஜீ", "[P");
        text = text.replace("ஜு", "[{");
        text = text.replace("ஜூ", "[_");
        text = text.replace("ஜெ", "n[");
        text = text.replace("ஜே", "N[");
        text = text.replace("ஜை", "i[");
        text = text.replace("ஜ்", "[;");
        text = text.replace("ஜ", "[");
        text = text.replace("கௌ", "nfs");
        text = text.replace("கோ", "Nfh");
        text = text.replace("கொ", "nfh");
        text = text.replace("கா", "fh");
        text = text.replace("கி", "fp");
        text = text.replace("கீ", "fP");
        text = text.replace("கு", "F");
        text = text.replace("கூ", "$");
        text = text.replace("கெ", "nf");
        text = text.replace("கே", "Nf");
        text = text.replace("கை", "if");
        text = text.replace("க்", "f;");
        text = text.replace("க", "f");
        text = text.replace("ஙௌ", "nqs");
        text = text.replace("ஙோ", "Nqh");
        text = text.replace("ஙொ", "nqh");
        text = text.replace("ஙா", "qh");
        text = text.replace("ஙி", "qp");
        text = text.replace("ஙீ", "qP");
        text = text.replace("ஙு", "*");
        text = text.replace("ஙூ", "*");
        text = text.replace("ஙெ", "nq");
        text = text.replace("ஙே", "Nq");
        text = text.replace("ஙை", "iq");
        text = text.replace("ங்", "q;");
        text = text.replace("ங", "q");
        text = text.replace("சௌ", "nrs");
        text = text.replace("சோ", "Nrh");
        text = text.replace("சொ", "nrh");
        text = text.replace("சா", "rh");
        text = text.replace("சி", "rp");
        text = text.replace("சீ", "rP");
        text = text.replace("சு", "R");
        text = text.replace("சூ", "R+");
        text = text.replace("செ", "nr");
        text = text.replace("சே", "Nr");
        text = text.replace("சை", "ir");
        text = text.replace("ச்", "r;");
        text = text.replace("ச", "r");
        text = text.replace("ஞௌ", "nQs");
        text = text.replace("ஞோ", "NQh");
        text = text.replace("ஞொ", "nQh");
        text = text.replace("ஞா", "Qh");
        text = text.replace("ஞி", "Qp");
        text = text.replace("ஞீ", "QP");
        text = text.replace("ஞு", "*");
        text = text.replace("ஞூ", "*");
        text = text.replace("ஞெ", "nQ");
        text = text.replace("ஞே", "NQ");
        text = text.replace("ஞை", "iQ");
        text = text.replace("ஞ்", "Q;");
        text = text.replace("ஞ", "Q");
        text = text.replace("டௌ", "nls");
        text = text.replace("டோ", "Nlh");
        text = text.replace("டொ", "nlh");
        text = text.replace("டா", "lh");
        text = text.replace("டி", "b");
        text = text.replace("டீ", "B");
        text = text.replace("டு", "L");
        text = text.replace("டூ", "^");
        text = text.replace("டெ", "nl");
        text = text.replace("டே", "Nl");
        text = text.replace("டை", "il");
        text = text.replace("ட்", "l;");
        text = text.replace("ட", "l");
        text = text.replace("ணௌ", "nzs");
        text = text.replace("ணோ", "Nzh");
        text = text.replace("ணொ", "nzh");
        text = text.replace("ணா", "zh");
        text = text.replace("ணி", "zp");
        text = text.replace("ணீ", "zP");
        text = text.replace("ணு", "Z");
        text = text.replace("ணூ", "Z}");
        text = text.replace("ணெ", "nz");
        text = text.replace("ணே", "Nz");
        text = text.replace("ணை", "iz");
        text = text.replace("ண்", "z;");
        text = text.replace("ண", "z");
        text = text.replace("தௌ", "njs");
        text = text.replace("தோ", "Njh");
        text = text.replace("தொ", "njh");
        text = text.replace("தா", "jh");
        text = text.replace("தி", "jp");
        text = text.replace("தீ", "jP");
        text = text.replace("து", "J");
        text = text.replace("தூ", "J}");
        text = text.replace("தெ", "nj");
        text = text.replace("தே", "Nj");
        text = text.replace("தை", "ij");
        text = text.replace("த்", "j;");
        text = text.replace("த", "j");
        text = text.replace("நௌ", "nes");
        text = text.replace("நோ", "Neh");
        text = text.replace("நொ", "neh");
        text = text.replace("நா", "eh");
        text = text.replace("நி", "ep");
        text = text.replace("நீ", "eP");
        text = text.replace("நு", "E");
        text = text.replace("நூ", "E}");
        text = text.replace("நெ", "ne");
        text = text.replace("நே", "Ne");
        text = text.replace("நை", "ie");
        text = text.replace("ந்", "e;");
        text = text.replace("ந", "e");
        text = text.replace("னௌ", "nds");
        text = text.replace("னோ", "Ndh");
        text = text.replace("னொ", "ndh");
        text = text.replace("னா", "dh");
        text = text.replace("னி", "dp");
        text = text.replace("னீ", "dP");
        text = text.replace("னு", "D");
        text = text.replace("னூ", "D}");
        text = text.replace("னெ", "nd");
        text = text.replace("னே", "Nd");
        text = text.replace("னை", "id");
        text = text.replace("ன்", "d;");
        text = text.replace("ன", "d");
        text = text.replace("பௌ", "ngs");
        text = text.replace("போ", "Ngh");
        text = text.replace("பொ", "ngh");
        text = text.replace("பா", "gh");
        text = text.replace("பி", "gp");
        text = text.replace("பீ", "gP");
        text = text.replace("பு", "G");
        text = text.replace("பூ", "G+");
        text = text.replace("பெ", "ng");
        text = text.replace("பே", "Ng");
        text = text.replace("பை", "ig");
        text = text.replace("ப்", "g;");
        text = text.replace("ப", "g");
        text = text.replace("மௌ", "nks");
        text = text.replace("மோ", "Nkh");
        text = text.replace("மொ", "nkh");
        text = text.replace("மா", "kh");
        text = text.replace("மி", "kp");
        text = text.replace("மீ", "kP");
        text = text.replace("மு", "K");
        text = text.replace("மூ", "%");
        text = text.replace("மெ", "nk");
        text = text.replace("மே", "Nk");
        text = text.replace("மை", "ik");
        text = text.replace("ம்", "k;");
        text = text.replace("ம", "k");
        text = text.replace("யௌ", "nas");
        text = text.replace("யோ", "Nah");
        text = text.replace("யொ", "nah");
        text = text.replace("யா", "ah");
        text = text.replace("யி", "ap");
        text = text.replace("யீ", "aP");
        text = text.replace("யு", "A");
        text = text.replace("யூ", "A+");
        text = text.replace("யெ", "na");
        text = text.replace("யே", "Na");
        text = text.replace("யை", "ia");
        text = text.replace("ய்", "a;");
        text = text.replace("ய", "a");
        text = text.replace("ரௌ", "nus");
        text = text.replace("ரோ", "Nuh");
        text = text.replace("ரொ", "nuh");
        text = text.replace("ரா", "uh");
        text = text.replace("ரி", "up");
        text = text.replace("ரீ", "uP");
        text = text.replace("ரு", "U");
        text = text.replace("ரூ", "&");
        text = text.replace("ரெ", "nu");
        text = text.replace("ரே", "Nu");
        text = text.replace("ரை", "iu");
        text = text.replace("ர்", "u;");
        text = text.replace("ர", "u");
        text = text.replace("லௌ", "nys");
        text = text.replace("லோ", "Nyh");
        text = text.replace("லொ", "nyh");
        text = text.replace("லா", "yh");
        text = text.replace("லி", "yp");
        text = text.replace("லீ", "yP");
        text = text.replace("லு", "Y");
        text = text.replace("லூ", "Y}");
        text = text.replace("லெ", "ny");
        text = text.replace("லே", "Ny");
        text = text.replace("லை", "iy");
        text = text.replace("ல்", "y;");
        text = text.replace("ல", "y");
        text = text.replace("ளௌ", "nss");
        text = text.replace("ளோ", "Nsh");
        text = text.replace("ளொ", "nsh");
        text = text.replace("ளா", "sh");
        text = text.replace("ளி", "sp");
        text = text.replace("ளீ", "sP");
        text = text.replace("ளு", "S");
        text = text.replace("ளூ", "Sh");
        text = text.replace("ளெ", "ns");
        text = text.replace("ளே", "Ns");
        text = text.replace("ளை", "is");
        text = text.replace("ள்", "s;");
        text = text.replace("ள", "s");
        text = text.replace("வௌ", "nts");
        text = text.replace("வோ", "Nth");
        text = text.replace("வொ", "nth");
        text = text.replace("வா", "th");
        text = text.replace("வி", "tp");
        text = text.replace("வீ", "tP");
        text = text.replace("வு", "T");
        text = text.replace("வூ", "T+");
        text = text.replace("வெ", "nt");
        text = text.replace("வே", "Nt");
        text = text.replace("வை", "it");
        text = text.replace("வ்", "t;");
        text = text.replace("வ", "t");
        text = text.replace("ழௌ", "nos");
        text = text.replace("ழோ", "Noh");
        text = text.replace("ழொ", "noh");
        text = text.replace("ழா", "oh");
        text = text.replace("ழி", "op");
        text = text.replace("ழீ", "oP");
        text = text.replace("ழு", "O");
        text = text.replace("ழூ", "*");
        text = text.replace("ழெ", "no");
        text = text.replace("ழே", "No");
        text = text.replace("ழை", "io");
        text = text.replace("ழ்", "o;");
        text = text.replace("ழ", "o");
        text = text.replace("றௌ", "nws");
        text = text.replace("றோ", "Nwh");
        text = text.replace("றொ", "nwh");
        text = text.replace("றா", "wh");
        text = text.replace("றி", "wp");
        text = text.replace("றீ", "wP");
        text = text.replace("று", "W");
        text = text.replace("றூ", "W}");
        text = text.replace("றெ", "nw");
        text = text.replace("றே", "Nw");
        text = text.replace("றை", "iw");
        text = text.replace("ற்", "w;");
        text = text.replace("ற", "w");
        text = text.replace("ஹௌ", "n`s");
        text = text.replace("ஹோ", "N`h");
        text = text.replace("ஹொ", "n`h");
        text = text.replace("ஹா", "`h");
        text = text.replace("ஹி", "`p");
        text = text.replace("ஹீ", "`P");
        text = text.replace("ஹு", "{`");
        text = text.replace("ஹூ", "`_");
        text = text.replace("ஹெ", "n`");
        text = text.replace("ஹே", "N`");
        text = text.replace("ஹை", "i`");
        text = text.replace("ஹ்", "`;");
        text = text.replace("ஹ", "`");
        text = text.replace("ஷௌ", "n\\s");
        text = text.replace("ஷோ", "N\\h");
        text = text.replace("ஷொ", "n\\h");
        text = text.replace("ஷா", "\\h");
        text = text.replace("ஷி", "\\p");
        text = text.replace("ஷீ", "\\P");
        text = text.replace("ஷு", "\\{");
        text = text.replace("ஷூ", "\\_");
        text = text.replace("ஷெ", "n\\");
        text = text.replace("ஷே", "N\\");
        text = text.replace("ஷை", "i\\");
        text = text.replace("ஷ்", "\\;");
        text = text.replace('ஷ', '\\');
        text = text.replace("ஸௌ", "n]s");
        text = text.replace("ஸோ", "N]h");
        text = text.replace("ஸொ", "n]h");
        text = text.replace("ஸா", "]h");
        text = text.replace("ஸி", "]p");
        text = text.replace("ஸீ", "]P");
        text = text.replace("ஸு", "]{");
        text = text.replace("ஸூ", "]_");
        text = text.replace("ஸெ", "n]");
        text = text.replace("ஸே", "N]");
        text = text.replace("ஸை", "i]");
        text = text.replace("ஸ்", "];");
        text = text.replace("ஸ", "]");
        text = text.replace("அ", "m");
        text = text.replace("ஆ", "M");
        text = text.replace("இ", "๳");
        text = text.replace("ஈ", "<");
        text = text.replace("உ", "c");
        text = text.replace("ஊ", "C");
        text = text.replace("எ", "v");
        text = text.replace("ஏ", "V");
        text = text.replace("ஐ", "I");
        text = text.replace("ஒ", "x");
        text = text.replace("ஓ", "X");
        text = text.replace("ஔ", "xs");

        return text = text.replace("ஃ", "/");

    }
}
