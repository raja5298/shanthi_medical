/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import india.abs.gstonepro.api.models.BillOfSupplyChargeItem;
import india.abs.gstonepro.api.models.BillOfSupplyLineItem;
import india.abs.gstonepro.api.models.Product;
import india.abs.gstonepro.api.models.PurchaseInvoice;
import india.abs.gstonepro.api.models.PurchaseInvoiceLineItem;
import india.abs.gstonepro.api.models.PurchaseSale;
import india.abs.gstonepro.api.models.PurchaseSaleLineItem;
import india.abs.gstonepro.api.models.PurchaseSaleTaxSummary;
import india.abs.gstonepro.api.models.UQC;
import india.abs.gstonepro.business.ProductLogic;
import india.abs.gstonepro.ui.utils.AppConstants;
import india.abs.gstonepro.ui.utils.CurrencyInWords;
import india.abs.gstonepro.ui.utils.StateCode;
import india.abs.gstonepro.api.utils.SessionDataUtil;
import india.abs.gstonepro.business.PurchaseLogic;
import india.abs.gstonepro.business.PurchaseSaleLogic;
import india.abs.gstonepro.business.UQCLogic;
import static india.abs.gstonepro.pdf.BOSBill.sno;
import india.abs.gstonepro.ui.utils.TaxBillItems;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author user
 */
public class PurchaseBill {

    public class FooterTable extends PdfPageEventHelper {

        protected PdfPTable footer;

        public FooterTable(PdfPTable footer) {
            this.footer = footer;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            footer.writeSelectedRows(0, 5, 20, 80, writer.getDirectContent());
        }
    }
    static int totalProductCount = 0, productcount, hsncount, hsncountNon;

    int productLimitA4 = 15, productLimitA5L = 11, productLimitA5P = 10;
    static int sno = 0, hsnno = 0, hsnnoA4 = 0;

    int random = 0;
    BigDecimal zero = new BigDecimal("0.000");

    public static final String FONT3 = "/fonts/ITF-Rupee.ttf";
//    public String companyName = "", companyLine1 = "", companyLine2 = "", companyLine3 = "", companyLine4 = "",
//            companyDistrict = "", CompanyState = "", companyGSTIN = "";

    public String companyName = "", companyA4Addr = "", companyA5LAddr = "", companyA5PAddr = "",
            companyA7Addr = "", companyCity = "", companyDistrict = "", companyEmail = "",
            companyState = "", companyMobile = "", companyPhone = "", companyGSTIN = "", invoiceHeading = "",
            companyFooterHeading = "", companyFooterLineOne = "", companyFooterLineTwo = "", companyFooterLineThree = "",
            companyFooterLineFour = "", companyA4AddrLine1 = "", companyA4AddrLine2 = "", companyA4AddrLine3 = "", companyA5LAddrLine1 = "", companyA5LAddrLine2 = "",
            companyA5LAddrLine3 = "", companyA5PAddrLine1 = "", companyA5PAddrLine2 = "", companyA5PAddrLine3 = "", companyA7AddrLine1 = "", companyA7AddrLine2 = "",
            companyA7AddrLine3 = "";

    public String billNo = " ", billDate = " ", billCompanyName = " ", billCompanyAddr1 = " ", billCompanyAddr2 = " ",
            billCompanyAddr3 = " ", billCompanyCity = " ", billCompanyDistrict = " ",
            billCompanyState = " ", billCompanyMobile = " ", billCompanyPhone = " ", billCompanyEmail = " ", billCompanyGSTIN = " ", billPayment = " ",
            billBuyingOrderNo = " ", billOrderDate = " ", billDispatchThorugh = " ", billDestination = " ", billTermOfDelivery = " ", billThrough = " ",
            billReferenceNo = " ", billRemarks = " ", firstBillCopy = " ", secondBillCopy = " ", thirdBillCopy = " ", fourthBillCopy = " ", fifthBillCopy = " ";

    public boolean isIGST = false, billIsSameAddress = true, isBill = false, isPdfOnly = false;
    public int billId = 0;
    public float billIGST = 0, billTotalTax = 0, billFlightAmount = 0, billCGST = 0, billSGST = 0, billTotal = 0,
            billNetAmount = 0, billDiscountPer = 0, billDiscount = 0, billRoundOff = 0, billCessamount = 0;
    DecimalFormat currency = new DecimalFormat("##,##,##0.000");
    NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

    Long companyId = SessionDataUtil.getSelectedCompany().getCompanyId();
    List<PurchaseInvoiceLineItem> psItems = new ArrayList<>();
    List<Product> products = new ProductLogic().fetchAllProducts(companyId);
    List<UQC> UQCs = new UQCLogic().fetchAllUQCs();
//    List<BillOfSupplyLineItem> bosLineItems = new ArrayList<>();
//    List<BillOfSupplyChargeItem> bosChargeItems = new ArrayList<>();
    List<TaxBillItems> taxBillItems = new ArrayList<>();
//    List<PurchaseSaleTaxSummary> psTaxSummaries = new ArrayList<>();
    PurchaseInvoice newPS = new PurchaseInvoice();
//    PurchaseSaleTaxSummary newTS = new PurchaseSaleTaxSummary();

    com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font boldFont9 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);

    com.itextpdf.text.Font cellFont1Bold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 6, BaseColor.BLACK);

    com.itextpdf.text.Font footerfont7 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

    com.itextpdf.text.Font footerboldfont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);

    public void pdfByBillId(Boolean isPrint, Boolean isPreview, String billId) {

        try {
            PurchaseInvoice PS = new PurchaseLogic().fetchAPurchaseInvoice(billId);
            List<PurchaseInvoiceLineItem> psLIs = PS.getPiLineItems();
//            List<PurchaseSaleTaxSummary> psTSs = PS.getPsTaxSummaries();

//            printPdf(isPrint, isPreview, PS, psLIs, psTSs);
            printPdf(isPrint, isPreview, PS, psLIs);
        } catch (IOException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printPdf(Boolean isPrint, Boolean isPreview, PurchaseInvoice PS, List<PurchaseInvoiceLineItem> Items) throws IOException {
        try {
            sno = 0;
            hsnnoA4 = 0;
            hsnno = 0;
            newPS = PS;
            psItems = Items;
//            psTaxSummaries = TaxSummaries;
            productcount = psItems.size();
//            hsncount = psTaxSummaries.size();
//            getBillItems();

            setCompany();
            setBillDetails();
            String paperSize = SessionDataUtil.getCompanyPolicyData().getPrintPaperSize();
            switch (paperSize) {
                case "A4_P":
                    createPdfA4(isPrint, isPreview);
                    break;
                case "A5_P":
                    try {
                        createPdfA5P(isPrint, isPreview);
                    } catch (DocumentException ex) {
                        Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "A5_L": {

                    try {
                        createPdfA5L(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
                case "A7_P": {

                    try {
                        createPdfA7(isPrint, isPreview);
//                        new A7Bill().createPdf();
                    } catch (IOException ex) {
                        Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
//            case "A7_P":
//                createPdfA5P(isPrint, isPreview);
                default:
                    break;
            }
        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUQCcode(String value) {
        String strUQCcode = "";
        for (UQC uqc : UQCs) {
            if (uqc.getQuantityName().equals(value)) {
                strUQCcode = uqc.getUQCCode();
                break;
            }
        }
        return strUQCcode;
    }

    public void getBillItems() {
        int Sno = 0;
        int produtSize = psItems.size();
        for (int i = 0; i < psItems.size(); i++) {
            TaxBillItems p = new TaxBillItems();
            p.setSno(String.valueOf(i));
            p.setProductNameOrbillChargeName(psItems.get(i).getProductName());
            p.setHsnSac(psItems.get(i).getHsnSac());
            p.setUqcOne(psItems.get(i).getUqcOne());
            p.setUqcOneQuantity(psItems.get(i).getUqcOneQuantity());
            p.setUqcOneRate(psItems.get(i).getUqcOneRate());
            p.setUqcOneValue(psItems.get(i).getUqcOneValue());
            p.setUqcTwo(psItems.get(i).getUqcTwo());
            p.setUqcTwoQuantity(psItems.get(i).getUqcTwoQuantity());
            p.setUqcTwoRate(psItems.get(i).getUqcTwoRate());
            p.setUqcTwoValue(psItems.get(i).getUqcTwoValue());
            p.setIgstPercentage(psItems.get(i).getIgstPercentage());
            p.setCgstPercentage(psItems.get(i).getCgstPercentage());
            p.setSgstPercentage(psItems.get(i).getSgstPercentage());
            p.setIgstValue(psItems.get(i).getIgstValue());
            p.setCgstValue(psItems.get(i).getCgstValue());
            p.setSgstValue(psItems.get(i).getSgstValue());
            p.setDiscountValue(psItems.get(i).getDiscountValue());
            p.setProductvalueOrbillChargeValue(psItems.get(i).getValue());
            p.setIsBillCharge(psItems.get(i).getProduct().isService());
            taxBillItems.add(p);
        }

//        for (int j = 0; j < bosChargeItems.size(); j++) {
//            TaxBillItems p = new TaxBillItems();
//            p.setSno(String.valueOf(j + produtSize));
//            p.setProductNameOrbillChargeName(bosChargeItems.get(j).getBillChargeName());
//            p.setProductvalueOrbillChargeValue(bosChargeItems.get(j).getValue());
//            p.setIsBillCharge(true);
//            taxBillItems.add(p);
//        }
        productcount = taxBillItems.size();
    }

    public void setBillDetails() {
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
        isIGST = newPS.isIsIGST();
        billNo = newPS.getPurchaseInvoiceNo();
        billDate = sdfr.format(newPS.getPurchaseInvoiceDate());
//        if (newPS.getLedger() == null) { //|| !newPS.isNoParty()) {
//            
//                billCompanyName = "";
//                billCompanyAddr1 = "";
//                billCompanyCity = "";
//                billCompanyDistrict = "";
//                billCompanyState = "";
//                billCompanyMobile = "";
//                billCompanyPhone = "";
//                billCompanyGSTIN = "";
//            
//
//        } else {

        billCompanyName = newPS.getLedger().getLedgerName();

        String Address = newPS.getLedger().getAddress();
        if (Address != null) {
            billCompanyAddr1 = Address.replaceAll("/~/", ",");
        }
        String[] words = Address.split("/~/");

        if (words.length == 1) {
            billCompanyAddr1 = (words[0]);
            billCompanyAddr2 = "";
            billCompanyAddr3 = "";
        } else if (words.length == 2) {
            billCompanyAddr1 = (words[0]);
            billCompanyAddr2 = words[1];
            billCompanyAddr3 = "";
        } else {
            billCompanyAddr1 = (words[0]);
            billCompanyAddr2 = words[1];
            billCompanyAddr3 = words[2];
        }

//        billCompanyAddr2 = "";
//        billCompanyAddr3 = "";
        if (newPS.getLedger().getCity() != null) {
            billCompanyCity = newPS.getLedger().getCity();
        }
        if (newPS.getLedger().getDistrict() != null) {
            billCompanyDistrict = newPS.getLedger().getDistrict();
        }
        if (newPS.getLedger().getState() != null) {
            billCompanyState = newPS.getLedger().getState();
        }
        if (newPS.getLedger().getMobile() != null) {
            billCompanyMobile = newPS.getLedger().getMobile();
        }
        if (newPS.getLedger().getEmail() != null) {
            billCompanyEmail = newPS.getLedger().getEmail();
        }
        if (newPS.getLedger().getPhone() != null) {
            billCompanyPhone = newPS.getLedger().getPhone();
        }
        if (newPS.getLedger().getGSTIN() != null) {
            billCompanyGSTIN = newPS.getLedger().getGSTIN();
        }
//        }

        billTotal = newPS.getPuchaseInvoiceAmount().floatValue();
        if (isIGST) {
            billTotalTax = newPS.getIgstValue().floatValue();
        } else {
            billTotalTax = newPS.getCgstValue().floatValue() + newPS.getSgstValue().floatValue();
        }
        billNetAmount = newPS.getCalculatedPurchaseInvoiceAmount().floatValue();

        billCGST = newPS.getCgstValue().floatValue();
        billSGST = newPS.getSgstValue().floatValue();
        billIGST = newPS.getIgstValue().floatValue();
        billTotal = newPS.getPuchaseInvoiceAmount().floatValue();
        billNetAmount = newPS.getCalculatedPurchaseInvoiceAmount().floatValue();
        isIGST = newPS.isIsIGST();
        billReferenceNo = newPS.geteWayBillNo();
        billRemarks = newPS.getRemarks();
//        }
        billDiscount = newPS.getDiscountAmount().floatValue();
//        noOfCopies = newPS.getCompany().getCompanyPolicy().getNumberOfInvoiceCopies();
        firstBillCopy = newPS.getCompany().getCompanyPolicy().getFirstInvoiceWord();
        secondBillCopy = newPS.getCompany().getCompanyPolicy().getSecondInvoiceWord();
        thirdBillCopy = newPS.getCompany().getCompanyPolicy().getThirdInvoiceWord();
        fourthBillCopy = newPS.getCompany().getCompanyPolicy().getFourthInvoiceWord();
        fifthBillCopy = newPS.getCompany().getCompanyPolicy().getFifthInvoiceWord();
//        }
    }

    public void setCompany() {
        companyName = SessionDataUtil.getSelectedCompany().getCompanyName();

        String A4AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineOne();
        String A4AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineTwo();
        String A4AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA4PAddressLineThree();
        if (A4AddressLine1 != null) {
            companyA4AddrLine1 = A4AddressLine1.replaceAll("/~/", ",");
        }
        if (A4AddressLine2 != null) {
            companyA4AddrLine2 = A4AddressLine2.replaceAll("/~/", ",");
        }
        if (A4AddressLine3 != null) {
            companyA4AddrLine3 = A4AddressLine3.replaceAll("/~/", ",");
        }
        String A5PAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineOne();
        String A5PAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineTwo();
        String A5PAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5PAddressLineThree();
        if (A5PAddressLine1 != null) {
            companyA5PAddrLine1 = A5PAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine2 != null) {
            companyA5PAddrLine2 = A5PAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5PAddressLine3 != null) {
            companyA5PAddrLine3 = A5PAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A5LAddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineOne();
        String A5LAddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineTwo();
        String A5LAddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA5LAddressLineThree();
        if (A5LAddressLine1 != null) {
            companyA5LAddrLine1 = A5LAddressLine1.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine2 != null) {
            companyA5LAddrLine2 = A5LAddressLine2.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        if (A5LAddressLine3 != null) {
            companyA5LAddrLine3 = A5LAddressLine3.replaceAll("/~/", ",").replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        String A7AddressLine1 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineOne();
        String A7AddressLine2 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineTwo();
        String A7AddressLine3 = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getA7AddressLineThree();
        if (A7AddressLine1 != null) {
            companyA7AddrLine1 = A7AddressLine1.replaceAll("/~/", ",");
        }
        if (A7AddressLine2 != null) {
            companyA7AddrLine2 = A7AddressLine2.replaceAll("/~/", ",");
        }
        if (A7AddressLine3 != null) {
            companyA7AddrLine3 = A7AddressLine3.replaceAll("/~/", ",");
        }
        if (SessionDataUtil.getSelectedCompany().getCity() != null) {
            companyCity = SessionDataUtil.getSelectedCompany().getCity();
        }
        if (SessionDataUtil.getSelectedCompany().getDistrict() != null) {
            companyDistrict = SessionDataUtil.getSelectedCompany().getDistrict();
        }
        if (SessionDataUtil.getSelectedCompany().getState() != null) {
            companyState = SessionDataUtil.getSelectedCompany().getState();
        }
        if (SessionDataUtil.getSelectedCompany().getMobile() != null) {
            companyMobile = SessionDataUtil.getSelectedCompany().getMobile();
        }
        if (SessionDataUtil.getSelectedCompany().getEmail() != null) {
            companyEmail = SessionDataUtil.getSelectedCompany().getEmail();
        }
        if (SessionDataUtil.getSelectedCompany().getPhone() != null) {
            companyPhone = SessionDataUtil.getSelectedCompany().getPhone();
        }
        if (SessionDataUtil.getSelectedCompany().getGSTIN() != null) {
            companyGSTIN = SessionDataUtil.getSelectedCompany().getGSTIN();
        }
//        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getBillOfSupplyWord() != null) {
//            invoiceHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getTaxInvoiceWord();
//        }
        invoiceHeading = "Purchase";
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading() != null) {
            companyFooterHeading = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterHeading();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne() != null) {
            companyFooterLineOne = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineOne();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo() != null) {
            companyFooterLineTwo = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineTwo();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree() != null) {
            companyFooterLineThree = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineThree();
        }
        if (SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour() != null) {
            companyFooterLineFour = SessionDataUtil.getSelectedCompany().getCompanyPolicy().getFooterLineFour();
        }

    }

    public void addInfo(Document document, int invoiceno, String invoiceCopyName) {
        try {

            PdfPTable taxTable = new PdfPTable(2);
            taxTable.setWidthPercentage(100);

            taxTable.setWidths(new float[]{55, 45});
            PdfPCell taxcell;

            Phrase p = new Phrase("", boldFont);
            taxcell = new PdfPCell(p);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);

            Phrase p1 = new Phrase(invoiceCopyName, cellFont1);
            taxcell = new PdfPCell(p1);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            taxTable.addCell(taxcell);
            document.add(taxTable);

            PdfPCell cell;

            PdfPTable Table1 = new PdfPTable(2);
            Table1.setWidthPercentage(100);
            PdfPCell cell1 = null;

            cell1 = new PdfPCell(new Phrase("Invoice No : " + billNo, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Date : " + billDate, cellFont));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.BOTTOM);
            Table1.addCell(cell1);

            document.add(Table1);

            taxTable = new PdfPTable(1);
            taxTable.setWidthPercentage(100);
            taxcell = null;

            taxcell = new PdfPCell(new Phrase(companyName, boldFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);
            companyA4Addr = companyA4AddrLine1 + " " + companyA4AddrLine2 + " " + companyA4AddrLine3;
            if (!companyA4Addr.equals("")) {
                taxcell = new PdfPCell(new Phrase(companyA4Addr, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                taxTable.addCell(taxcell);
            }

            Phrase phrase = new Phrase();
            phrase.add(new Chunk("City : " + companyCity, cellFont));
            phrase.add(new Chunk(" POS : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
            taxcell = new PdfPCell(phrase);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            phrase = new Phrase();
            if (!companyPhone.equals("")) {
                phrase.add(new Chunk("Phone : " + companyPhone, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            if (!companyMobile.equals("")) {
                phrase.add(new Chunk(" Mobile : " + companyMobile, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            if (!companyEmail.equals("")) {
                phrase.add(new Chunk(" Email : " + companyEmail, cellFont));
                taxcell = new PdfPCell(phrase);
            }
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.LEFT);
            taxTable.addCell(taxcell);

            document.add(taxTable);

            PdfPTable Table = new PdfPTable(1);
            Table.setWidthPercentage(100);
            cell = null;

            cell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            Table.addCell(cell);

            document.add(Table);

            PdfPTable table2 = new PdfPTable(1);
            table2.setWidthPercentage(100);
            PdfPCell tablecell = null;

            tablecell = new PdfPCell(new Phrase(billCompanyName, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyAddr1 + " " + billCompanyAddr2 + " " + billCompanyAddr3, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyCity, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyDistrict, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyGSTIN, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

            tablecell = new PdfPCell(new Phrase(billCompanyState, cellFont));
            tablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablecell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            table2.addCell(tablecell);

//                tablecell = new PdfPCell(new Phrase(billCompanyPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//                innerTable = new PdfPTable(1);
//
//                tablecell = new PdfPCell(new Phrase(ShippingName, cellFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//
//                tablecell = new PdfPCell(new Phrase(ShippingAddr1 + " " + ShippingAddr2 + " " + ShippingAddr3, cellFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//
//                tablecell = new PdfPCell(new Phrase(ShippingCity, cellFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//
//                tablecell = new PdfPCell(new Phrase(ShippingDistrict, cellFont));
//                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//
////            tablecell = new PdfPCell(new Phrase("GSTIN/UIN:" + billCompanyGSTIN, cellFont));
////            tablecell.setBorder(Rectangle.NO_BORDER);
////            innerTable.addCell(tablecell);
//                tablecell = new PdfPCell(new Phrase(ShippingState, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
//                tablecell = new PdfPCell(new Phrase(ShippingPhone, cellFont));
//                tablecell.setBorder(Rectangle.NO_BORDER);
//                innerTable.addCell(tablecell);
            document.add(table2);

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidthPercentage(100);
            PdfPCell cell3 = null;

            cell3 = new PdfPCell(new Phrase(billRemarks, cellFont1));
            cell3.setColspan(2);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            table3.addCell(cell3);

            document.add(table3);

        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProductItems(Document document, int productLimitInCounter) throws IOException {

        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font productFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell taxcell, cell;
            PdfPTable pdfTable = new PdfPTable(9);
            pdfTable.setWidths(new float[]{8, 33, 12, 12, 15, 10, 15, 12, 18});
            pdfTable.setTotalWidth(555);
            pdfTable.setLockedWidth(true);

            float ht = pdfTable.getTotalHeight();

            taxcell = new PdfPCell(new Phrase("S.No", totalFont));
            //taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("GST %", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Quantity", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("(per)", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Discount", totalFont));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", totalFont));
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(taxcell);
//-------------------------------------------------------------------------------------------------------------------
//            productcount = bosLineItems.size();
            int linecount = 0;
            for (int counter = 0; counter < productLimitInCounter; counter++) {
                linecount++;
                String strSNO = Integer.toString(sno + 1);
                if (!psItems.get(sno).getProduct().isService()) {

//-------------------------------------------------------------------------------------------------------------------
                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), productFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), cellFont));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getIgstPercentage()), cellFont));

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    int UQC2Value = 0;
                    for (Product product : products) {
                        if (product.getName().equals(psItems.get(sno).getProductName())) {
                            UQC2Value = product.getUQC2Value();
                        }
                    }
                    int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();
                    String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
                    if (UQC1qty > 0) {
                        strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
                        strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        if (UQC2qty > 0) {
                            strQuantity = strQuantity + ", ";
                            strAltQuantity = strAltQuantity + " ";
                        }
                    }
                    if (UQC2qty > 0) {
                        if (UQC1qty == 0) {
                            strQuantity = "";
                            strAltQuantity = "";
                        }
                        strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                    }
                    int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                    if (isEqual == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                    } else {
                        Phrase phrase = new Phrase();
                        phrase.add(new Chunk(strQuantity, totalFontBold));
                        phrase.add(new Chunk(Chunk.NEWLINE + "(" + strAltQuantity + ")", cellFont));
                        cell = new PdfPCell(phrase);
                    }

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    Phrase phrase = new Phrase();
                    phrase.add(new Chunk(currency.format(psItems.get(sno).getUqcOneRate()), totalFontBold));
                    phrase.add(new Chunk(Chunk.NEWLINE + currency.format(psItems.get(sno).getUqcTwoRate()), cellFont));
                    cell = new PdfPCell(phrase);
//                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                    cell.setBorder(Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                    phrase = new Phrase();
                    phrase.add(new Chunk("(" + psItems.get(sno).getUqcOne() + ")", totalFontBold));
                    if (!psItems.get(sno).getUqcOne().equals(psItems.get(sno).getUqcTwo())) {
                        if (!psItems.get(sno).getUqcTwo().equals("")) {
                            phrase.add(new Chunk(Chunk.NEWLINE + "(" + psItems.get(sno).getUqcTwo() + ")", cellFont));
                        }
                    }
                    cell = new PdfPCell(phrase);
                    cell.setBorder(Rectangle.RIGHT);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfTable.addCell(cell);

                    int isZero = psItems.get(sno).getDiscountValue().compareTo(zero);
                    if (isZero == 0) {
                        cell = new PdfPCell(new Phrase(String.valueOf("-"), cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    } else {
                        cell = new PdfPCell(new Phrase("-" + String.valueOf(psItems.get(sno).getDiscountValue()), cellFont));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    }

                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), totalFontBold));
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    pdfTable.addCell(cell);

                } else {

                    cell = new PdfPCell(new Phrase(strSNO, cellFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
//                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    for (int i = 0; i < 3; i++) {
                        cell = new PdfPCell(new Phrase("", HeaderFont));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        pdfTable.addCell(cell);
                    }
                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.LEFT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT);
                    pdfTable.addCell(cell);

                    cell = new PdfPCell(new Phrase("", HeaderFont));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);

//                    if (psItems.get(sno).getValue().compareTo(zero) > 0) {
//                        cell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), HeaderFont));
//                    } else {
                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), totalFontBold));
//                    }
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                    pdfTable.addCell(cell);
                }
                sno++;

            }
            linecount = (productLimitA4 - linecount);
            Phrase phrase = new Phrase();
            for (int i = 0; i < linecount; i++) {
                phrase.add(new Chunk(" ", totalFontBold));
//                phrase.add(new Chunk(Chunk.NEWLINE + " 2", cellFont));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                phrase.add(new Chunk(" ", totalFontBold));
                cell = new PdfPCell(phrase);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

            }

            document.add(pdfTable);
        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItemsContinued(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

//            PdfPTable pdfTable = new PdfPTable(9);
//            pdfTable.setWidths(new float[]{8, 25, 12, 20, 15, 10, 15, 10, 20});
//            pdfTable.setTotalWidth(555);
//            pdfTable.setLockedWidth(true);
            PdfPTable pdfTable = new PdfPTable(2);
            pdfTable.setWidthPercentage(100);

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void afterAddProductItems1(Document document) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(9);
        pdfTable.setWidths(new float[]{8, 33, 12, 12, 15, 10, 15, 12, 18});
        pdfTable.setTotalWidth(555);
        pdfTable.setLockedWidth(true);
        if (isIGST) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("IGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billIGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("CGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billCGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("SGST", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billSGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        if (billDiscount != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Bill Discount", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        if (billRoundOff != 0) {
            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Round Off", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("", HeaderFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billRoundOff), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);
        }
        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Net Amount", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("", HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(formatter.format(billNetAmount), HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);
        document.add(pdfTable);

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase("Amount Chargeable(in words)", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("E. & O.E", cellFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(new CurrencyInWords().AmountInWords(billNetAmount), HeaderFont));
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        for (int i = 0; i < 5; i++) {
            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);

        }

        document.add(pdfTable);

    }

    public void addFooter(Document document) {
        try {
            com.itextpdf.text.Font totalFontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
            com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);

            PdfPTable outerTable = new PdfPTable(2);
            outerTable.setWidthPercentage(100);

            PdfPTable pdfTable = new PdfPTable(1);

            PdfPCell cell = new PdfPCell(new Phrase(companyFooterHeading, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            pdfTable = new PdfPTable(1);

            cell = new PdfPCell(new Phrase("for " + companyName.toUpperCase(), totalFontBold));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            outerTable.addCell(pdfTable);

            document.add(outerTable);
        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void lastLineA4(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    public void createPdfA4(Boolean isPrint, Boolean isPreview) {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

            Rectangle layout = new Rectangle(PageSize.A4);
            Document document = new Document(layout, 20, 20, 20, 20);
            String path = "";
            Boolean isApproved = false;
            String nowFilePath = "";
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Purchase Invoice" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Purchase Invoice" + "-" + newPS.getPurchaseInvoiceNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }

            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));

                document.open();

//                for (int copies = 0; copies < noOfCopies; copies++) {
//                    sno = 0;
//                    String invoiceCopyName = null;
//                    switch (copies) {
//                        case 0:
//                            invoiceCopyName = firstBillCopy;
//                            break;
//                        case 1:
//                            invoiceCopyName = secondBillCopy;
//                            break;
//                        case 2:
//                            invoiceCopyName = thirdBillCopy;
//                            break;
//                        case 3:
//                            invoiceCopyName = fourthBillCopy;
//                            break;
//                        case 4:
//                            invoiceCopyName = fifthBillCopy;
//                            break;
//                        default:
//                            break;
//
//                    }
//-----------------------------------------------------------------------------------------------------------------------
                int pagecount = 0, remainingProduct = 0;
                pagecount = (productcount / productLimitA4);
                if ((productcount % productLimitA4) > 0) {
                    pagecount += 1;
                }
                if (pagecount == 1) {
                    addInfo(document, pagecount, "Purchase");
                    addProductItems(document, (productcount % productLimitA4));
                    afterAddProductItems1(document);
//                        addHSNTable(document, hsnno, hsncount);
                    addFooter(document);
                    lastLineA4(document, pagecount, pagecount);
                } else {
                    for (int i = 1; i <= pagecount; i++) {
                        if (pagecount == i) {
                            remainingProduct = productcount - (remainingProduct * productLimitA4);
                            addInfo(document, i, "Purchase");
                            addProductItems(document, remainingProduct);
                            afterAddProductItems1(document);
//                                addHSNTable(document, hsnno, hsncount);
                            addFooter(document);
                            lastLineA4(document, pagecount, pagecount);
                        } else {
                            addInfo(document, i, "Purchase");
                            addProductItems(document, productLimitA4);
                            afterAddProductItemsContinued(document);
                            lastLineA4(document, pagecount, pagecount);
                            document.newPage();
                            remainingProduct++;
                        }
                    }
                }
//                }
                document.close();
                fos.close();

                if (isPrint) {
                    try {
                        pdfPrint(path);
                    } catch (PrintException ex) {
                        Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);
                }

            }
            com.alee.laf.WebLookAndFeel.install();
//            if (!isPrint && isApproved) {
//                JOptionPane.showMessageDialog(null, "Bill Downloaded at " + path, "Message", JOptionPane.INFORMATION_MESSAGE);
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addInfoA5P(Document document, String invoiceCopyName) throws DocumentException, IOException {

        PdfPCell taxcell;
        Phrase phrase = new Phrase();

        PdfPTable nameTable2 = new PdfPTable(3);
        nameTable2.setWidthPercentage(100);
        nameTable2.setWidths(new float[]{43, 43, 26});

        phrase.add(new Chunk("GSTIN/UIN : ", cellFont1));
        phrase.add(new Chunk(companyGSTIN, cellFont1Bold));
        taxcell = new PdfPCell(phrase);
        taxcell.setBorder(Rectangle.LEFT | Rectangle.TOP);
        nameTable2.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("", totalFont));
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        taxcell.setPaddingBottom(-3);
        taxcell.setBorder(Rectangle.TOP);
        nameTable2.addCell(taxcell);

        Phrase p = new Phrase();

        p.add(new Chunk("Mobile : ", cellFont1));
        p.add(new Chunk(companyPhone, cellFont1Bold));
        taxcell = new PdfPCell(p);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.RIGHT | Rectangle.TOP);
        nameTable2.addCell(taxcell);

        document.add(nameTable2);

        PdfPTable nameTable1 = new PdfPTable(3);
        nameTable1.setWidthPercentage(100);
        nameTable1.setWidths(new float[]{43, 43, 26});

        taxcell = new PdfPCell(new Phrase(" ", totalFont));
        taxcell.setBorder(Rectangle.LEFT);
        taxcell.setPaddingBottom(-3);
        nameTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase("   " + companyName, totalFont));
        taxcell.setPaddingBottom(-3);
        taxcell.setBorder(Rectangle.NO_BORDER);
        nameTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceCopyName, cellFont1));
        taxcell.setBorder(Rectangle.RIGHT);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setPaddingBottom(-3);
        nameTable1.addCell(taxcell);

        if (!companyA5PAddrLine1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine1, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine2, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else if (!companyA5PAddrLine2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine2, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else if (!companyA5PAddrLine3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5PAddrLine3, cellFont));
            taxcell.setColspan(3);
            taxcell.setPaddingBottom(-3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        } else {
            taxcell = new PdfPCell(new Phrase(companyCity + ", " + companyDistrict + ", " + companyEmail + ".", cellFont));
            taxcell.setColspan(3);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            nameTable1.addCell(taxcell);
        }

        document.add(nameTable1);

        PdfPTable addressTable1 = new PdfPTable(3);
        addressTable1.setWidthPercentage(100);
        addressTable1.setWidths(new float[]{28, 44, 28});

        Phrase p2 = new Phrase(), p3 = new Phrase();

        p3.add(new Chunk("POS : ", cellFont1));
        p3.add(new Chunk(companyState + " (" + new StateCode().getStateCode(companyState) + ")", cellFont1Bold));
        taxcell = new PdfPCell(p3);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        taxcell.setBorder(Rectangle.LEFT);
        addressTable1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceHeading, totalFont));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOTTOM);
        addressTable1.addCell(taxcell);

        p2.add(new Chunk("Date : ", cellFont1));
        p2.add(new Chunk(billDate, cellFont1Bold));
        taxcell = new PdfPCell(p2);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.RIGHT);
        addressTable1.addCell(taxcell);

        document.add(addressTable1);

        PdfPTable revtable = new PdfPTable(3);
        revtable.setWidthPercentage(100);
        revtable.setWidths(new float[]{30, 30, 40});
        PdfPCell tablecell = null;

        Phrase p1 = new Phrase();

        p1.add(new Chunk("Bill No. : ", cellFont1));
        p1.add(new Chunk(String.valueOf(billNo), cellFont1Bold));
        taxcell = new PdfPCell(p1);
        taxcell.setBorder(Rectangle.BOX);
        revtable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billRemarks, totalFont));
        taxcell.setColspan(2);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOX);
        revtable.addCell(taxcell);

        document.add(revtable);

        PdfPTable Table1 = new PdfPTable(1);
        Table1.setWidthPercentage(100);

        taxcell = new PdfPCell(new Phrase("Bill to Party", cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyName, totalFont));
        taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(billCompanyAddr1 + billCompanyAddr2 + billCompanyAddr3, cellFont));
        taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        if (!"".equals(billCompanyGSTIN)) {
            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", cellFont1));
            p7.add(new Chunk(billCompanyGSTIN, cellFont1Bold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            Table1.addCell(taxcell);
        }
        if (!"".equals(billCompanyState)) {
            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", cellFont));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.LEFT | Rectangle.RIGHT);
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            Table1.addCell(taxcell);
        }

        document.add(Table1);

    }

    public void addProductItemsA5P(Document document, int productLimitInCounter) throws DocumentException {
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(6);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

        cell = new PdfPCell(new Phrase("S.No", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Description", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("GST %", boldFont9));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Qty", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Rate", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount", totalFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfTable.addCell(cell);
        int count = 0;
        for (int counter = 0; counter < productLimitInCounter; counter++) {
            count++;
            if (!psItems.get(sno).getProduct().isService()) {
                String strSNO = Integer.toString(sno + 1);
                cell = new PdfPCell(new Phrase(strSNO, cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getIgstPercentage()), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfTable.addCell(cell);

                int UQC2Value = 0;
                for (Product product : products) {
                    if (product.getName().equals(psItems.get(sno).getProductName())) {
                        UQC2Value = product.getUQC2Value();
                    }
                }
                int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();

//                String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()),
//                        strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
//                if (psItems.get(sno).getUqcOne().equals(psItems.get(sno).getUqcTwo()) || psItems.get(sno).getUqcTwo().equals("")) {
//
//                } else {
//                    if (UQC1qty > 0) {
//                        strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
//                        strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
//                        if (UQC2qty > 0) {
//                            strQuantity = strQuantity + ", ";
//                            strAltQuantity = strAltQuantity + ", ";
//                        }
//                    }
//                    if (UQC2qty > 0) {
//                        if (UQC1qty == 0) {
//                            strQuantity = "";
//                            strAltQuantity = "";
//                        }
//                        strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
//                        strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
//                    }
//                }
                int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                Phrase p = new Phrase();
                if (isEqual == 0) {
                    cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                    p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                    p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                    cell = new PdfPCell(p);
                } else {
                    p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                    p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                    p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                    p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                    cell = new PdfPCell(p);
                }
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfTable.addCell(cell);

                int isZero = psItems.get(sno).getDiscountValue().compareTo(zero);
                p = new Phrase();
                p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneRate()), cellFont));
                if (isZero == 0) {
                    p.add(new Chunk("", cellFont));
//                p.add(new Chunk(" (" + String.valueOf(bosLineItems.get(sno).getDiscountValue()) + ")", cellFont));
                } else {
                    p.add(new Chunk(" (-" + String.valueOf(psItems.get(sno).getDiscountValue()) + ")", cellFont));
                }

                cell = new PdfPCell(p);

                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pdfTable.addCell(cell);
            } else {
                String strSNO = Integer.toString(sno + 1);
                cell = new PdfPCell(new Phrase(strSNO, cellFont));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase("", cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase("", cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pdfTable.addCell(cell);

                cell = new PdfPCell(new Phrase("", cellFont));
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pdfTable.addCell(cell);

                if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                    cell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), HeaderFont));
                } else {
                    cell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), HeaderFont));
                }
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }
            sno++;
        }

        count = (productLimitA5P - count) * 6;
        for (int i = 0; i < count; i++) {
            cell = new PdfPCell(new Phrase(" ", cellFont));
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            pdfTable.addCell(cell);
        }
        document.add(pdfTable);

    }

    public void afteraddProductItemsA5P(Document document) throws DocumentException {

        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        Phrase p;
        PdfPCell cell;

        PdfPTable pdfTable = new PdfPTable(6);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

        cell = new PdfPCell(new Phrase("Total Amount Before Tax", cellFont));
        cell.setColspan(5);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(currency.format(billTotal), HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        if (isIGST) {
//            cell = new PdfPCell(new Phrase(new CurrencyInWords().AmountInWords(billNetAmount), HeaderFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(2);
//            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("(+) IGST", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billIGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
        } else {

//            cell = new PdfPCell(new Phrase(new CurrencyInWords().AmountInWords(billNetAmount), cellFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(2);
//            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("(+) CGST", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billCGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);

//            cell = new PdfPCell(new Phrase(" ", cellFont));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(2);
//            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
//            pdfTable.addCell(cell);
            cell = new PdfPCell(new Phrase("(+) SGST", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(currency.format(billSGST), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
        }
        if (billDiscount != 0) {
            cell = new PdfPCell(new Phrase("Discount", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), HeaderFont));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

        }
        if (billRoundOff != 0) {
            cell = new PdfPCell(new Phrase("Round off", cellFont));
            cell.setColspan(5);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pdfTable.addCell(cell);

            if (billRoundOff > 0) {
                cell = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), HeaderFont));
            } else {
                cell = new PdfPCell(new Phrase(currency.format(billRoundOff), HeaderFont));
            }
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("Total Amount After Tax ", cellFont));
        cell.setColspan(5);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(currency.format(billNetAmount), HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
        pdfTable.addCell(cell);
        document.add(pdfTable);

    }

    public void afterAddProductItemsContinuedA5P(Document document) {
        try {
            com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);

            PdfPCell cell;

            PdfPTable pdfTable = new PdfPTable(6);
            pdfTable.setWidthPercentage(100);
            pdfTable.setWidths(new float[]{9, 45, 12, 17, 25, 20});

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(6);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP);
            pdfTable.addCell(cell);
            for (int i = 0; i < 8; i++) {
                cell = new PdfPCell(new Phrase(" ", HeaderFont));
                cell.setColspan(6);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                pdfTable.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" ", HeaderFont));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);

            cell = new PdfPCell(new Phrase(" continued ... ", HeaderFont));
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            pdfTable.addCell(cell);
            document.add(pdfTable);

        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addFooterA5P(Document document) throws DocumentException {
        PdfPTable pdfTable;

        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
        com.itextpdf.text.Font HeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        Phrase p;
        PdfPCell cell;

        pdfTable = new PdfPTable(2);
        pdfTable.setWidthPercentage(100);
        pdfTable.setWidths(new float[]{50, 50});
        cell = new PdfPCell(new Phrase(companyFooterHeading, HeaderFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        Phrase p2 = new Phrase();
        p2.add(new Chunk("for ", cellFont1));
        p2.add(new Chunk(companyName.toUpperCase(), HeaderFont));
        cell = new PdfPCell(p2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineOne, cellFont));
        cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineTwo, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineThree, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("  ", HeaderFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase(companyFooterLineFour, cellFont));
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        cell = new PdfPCell(new Phrase("Authorised Signatory", cellFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
        pdfTable.addCell(cell);

        document.add(pdfTable);

    }

    public void lastLineA5P(Document document, int currentPage, int pageCount) throws DocumentException {
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        PdfPTable footerTable = new PdfPTable(1);
        footerTable.setWidthPercentage(100);

        Phrase p;
        PdfPCell cell;

        p = new Phrase(" ", boldFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);
        document.add(footerTable);

        footerTable = new PdfPTable(2);
        footerTable.setWidths(new float[]{70, 30});
        footerTable.setWidthPercentage(100);

        p = new Phrase(" ", cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        p = new Phrase("Page " + currentPage + " of " + pageCount, cellFont);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        footerTable.addCell(cell);

        document.add(footerTable);
    }

    private void createPdfA5P(Boolean isPrint, Boolean isPreview) throws DocumentException {
        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            String sql = "";
            Rectangle layout = new Rectangle(PageSize.A5);
            Boolean isApproved = false;
            Document document = new Document(layout, 20, 15, 15, 15);
            String path = null;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "TaxInvoice" + "-" + billNo + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("TaxInvoice" + "-" + newPS.getPurchaseInvoiceNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
//                for (int copies = 0; copies < noOfCopies; copies++) {
//                    sno = 0;
//                    document.newPage();
//                    String invoiceCopyName = null;
//                    switch (copies) {
//                        case 0:
//                            invoiceCopyName = firstBillCopy;
//                            break;
//                        case 1:
//                            invoiceCopyName = secondBillCopy;
//                            break;
//                        case 2:
//                            invoiceCopyName = thirdBillCopy;
//                            break;
//                        case 3:
//                            invoiceCopyName = fourthBillCopy;
//                            break;
//                        case 4:
//                            invoiceCopyName = fifthBillCopy;
//                            break;
//                        default:
//                            break;
//                    }

                int pagecount = 0, remainingProduct = 0;
                pagecount = (productcount / productLimitA5P);
                if ((productcount % productLimitA5P) > 0) {
                    pagecount += 1;
                }
                if (pagecount == 1) {
                    addInfoA5P(document, "Purchase");
                    addProductItemsA5P(document, (productcount % productLimitA5P));
                    afteraddProductItemsA5P(document);
                    addFooterA5P(document);
                    lastLineA5P(document, pagecount, pagecount);
                } else {
                    for (int i = 1; i <= pagecount; i++) {
                        if (pagecount == i) {
                            remainingProduct = productcount - (remainingProduct * productLimitA5P);
//                                remainingProduct = remainingProduct * productLimitA5P;
                            addInfoA5P(document, "Purchase");
//                                addProductItemsA5P(document, (productcount % productLimitA5P));
                            addProductItemsA5P(document, (remainingProduct));
                            afteraddProductItemsA5P(document);
                            addFooterA5P(document);
                            lastLineA5P(document, i, pagecount);
                        } else {
                            addInfoA5P(document, "Purchase");
                            addProductItemsA5P(document, (productLimitA5P));
                            afterAddProductItemsContinuedA5P(document);
                            lastLineA5P(document, i, pagecount);
                            document.newPage();
                            remainingProduct++;

                        }
                    }
                }
//                }
                document.close();
                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);
                    } catch (PrintException ex) {
                        Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);
                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addInfoA5L(Document document, String invoiceCopyName) throws DocumentException, IOException {
        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        Phrase p;
        PdfPCell taxcell;

        PdfPTable outerTable = new PdfPTable(3);
        outerTable.setWidthPercentage(100);
        outerTable.setWidths(new float[]{35, 35, 35});

        Phrase p1 = new Phrase();

        p1.add(new Chunk("Bill No. : ", footerfont));
        p1.add(new Chunk(String.valueOf(billNo), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        taxcell = new PdfPCell(new Phrase(invoiceHeading, boldcalibri));
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
        outerTable.addCell(taxcell);

        p1 = new Phrase();

        p1.add(new Chunk(invoiceCopyName + " | ", footerfont));
        p1.add(new Chunk("Date : ", footerfont));
        p1.add(new Chunk(String.valueOf(billDate), footerfontbold));
        taxcell = new PdfPCell(p1);
        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        taxcell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.TOP);
        outerTable.addCell(taxcell);

        document.add(outerTable);

        PdfPTable outerTable1 = new PdfPTable(3);
        outerTable1.setWidthPercentage(100);
        outerTable1.setWidths(new float[]{50, 50, 50});

        PdfPTable Table1 = new PdfPTable(1);

        taxcell = new PdfPCell(new Phrase(companyName + ",", boldcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!companyA5LAddrLine1.equalsIgnoreCase(" ")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!companyA5LAddrLine3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(companyA5LAddrLine3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase p7 = new Phrase();
            p7.add(new Chunk("GSTIN/UIN : ", footerfont));
            p7.add(new Chunk(companyGSTIN, footerfontbold));
            taxcell = new PdfPCell(p7);
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(companyState + " (" + new StateCode().getStateCode(companyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }
        outerTable1.addCell(Table1);

        Table1 = new PdfPTable(1);
        Table1.setWidthPercentage(100);
        taxcell = new PdfPCell(new Phrase("Bill to Party", normalcalibri));
        taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        taxcell.setPaddingBottom(-3);
        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Table1.addCell(taxcell);

        if ("".equals(billCompanyName)) {
            taxcell = new PdfPCell(new Phrase(billCompanyName, boldcalibri));
        } else {
            taxcell = new PdfPCell(new Phrase(billCompanyName + ",", boldcalibri));
        }
        taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
        taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
        Table1.addCell(taxcell);

        if (!billCompanyAddr1.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr1, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            if (!"".equals(billCompanyGSTIN)) {
                pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                taxcell = new PdfPCell(pg);
                taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr2.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr2, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            if (!"".equals(billCompanyGSTIN)) {
                pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                taxcell = new PdfPCell(pg);
                taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else if (!billCompanyAddr3.equalsIgnoreCase("")) {
            taxcell = new PdfPCell(new Phrase(billCompanyAddr3, normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);

            Phrase pg = new Phrase();
            if (!"".equals(billCompanyGSTIN)) {
                pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                taxcell = new PdfPCell(pg);
                taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        } else {
            Phrase pg = new Phrase();
            if (!"".equals(billCompanyGSTIN)) {
                pg.add(new Chunk("GSTIN/UIN : ", footerfont));
                pg.add(new Chunk(billCompanyGSTIN, footerfontbold));
                taxcell = new PdfPCell(pg);
                taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                Table1.addCell(taxcell);
            }

            taxcell = new PdfPCell(new Phrase(billCompanyState + " (" + new StateCode().getStateCode(billCompanyState) + ")", normalcalibri));
            taxcell.setBorder(Rectangle.NO_BORDER | Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            Table1.addCell(taxcell);
        }

//        outerTable1.addCell(Table1);
        document.add(Table1);
//        }
    }

    private void addProductItemsA5L(Document document, int productLimitInCounter) throws DocumentException, IOException {

        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        Phrase p;
        PdfPCell taxcell;
        if (isIGST) {
            PdfPTable thirdTable = new PdfPTable(7);
            thirdTable.setWidthPercentage(100);
            thirdTable.setWidths(new float[]{5, 30, 11, 15, 9, 15, 15});

            taxcell = new PdfPCell(new Phrase("S.No", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Qty", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate ", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("IGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            thirdTable.addCell(taxcell);
            int lineCount = 0;
            for (int counter = 0; counter < productLimitInCounter; counter++) {
                int UQC2Value = 0;
                for (Product product : products) {
                    if (product.getName().equals(psItems.get(sno).getProductName())) {
                        UQC2Value = product.getUQC2Value();
                    }
                }

                lineCount++;
                if (!psItems.get(sno).getProduct().isService()) {
                    if ((counter + 1) == productLimitInCounter) {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        p = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            taxcell = new PdfPCell(p);
                        } else {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                            taxcell = new PdfPCell(p);
                        }
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);
                    } else {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        p = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            taxcell = new PdfPCell(p);
                        } else {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                            taxcell = new PdfPCell(p);
                        }
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);
                    }
                } else {
                    if ((counter + 1) == productLimitInCounter) {
                        String strSNO = Integer.toString(sno + 1);
                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                            taxcell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), normalcalibri));
                        } else {
                            taxcell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), normalcalibri));
                        }
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);
                    } else {
                        String strSNO = Integer.toString(sno + 1);
                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getIgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                            taxcell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), normalcalibri));
                        } else {
                            taxcell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), normalcalibri));
                        }
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);
                    }

                }
                sno++;
            }
            lineCount = (11 - lineCount);
            for (int i = 0; i < lineCount; i++) {
                taxcell = new PdfPCell(new Phrase(" ", normalcalibri));
                taxcell.setColspan(8);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                thirdTable.addCell(taxcell);
            }

            document.add(thirdTable);

        } else {
            PdfPTable thirdTable = new PdfPTable(8);
            thirdTable.setWidthPercentage(100);
            thirdTable.setWidths(new float[]{6, 30, 10, 14, 13, 15, 15, 14});

            taxcell = new PdfPCell(new Phrase("S.No", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Description", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("HSN/SAC", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Qty", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Rate ", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("CGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("SGST (%)", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);

            taxcell = new PdfPCell(new Phrase("Amount", boldcalibri));
            taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            thirdTable.addCell(taxcell);
            int lineCount = 0;

            for (int counter = 0; counter < productLimitInCounter; counter++) {

                lineCount++;
                if (!psItems.get(sno).getProduct().isService()) {
                    if ((counter + 1) == productLimitInCounter) {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        p = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            taxcell = new PdfPCell(p);
                        } else {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                            taxcell = new PdfPCell(p);
                        }
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase2 = new Phrase();
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                        phrase2.add(new Chunk("(", normalcalibri));
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                        phrase2.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase2);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);
                    } else {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getHsnSac(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        p = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else if (psItems.get(sno).getUqcTwoQuantity() == 0 || psItems.get(sno).getUqcTwo().equals("")) {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            taxcell = new PdfPCell(p);
                        } else {
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcOneQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcOne()) + " "), footerfont7));
                            p.add(new Chunk(String.valueOf(psItems.get(sno).getUqcTwoQuantity()), totalFont));
                            p.add(new Chunk(String.valueOf(" " + getUQCcode(psItems.get(sno).getUqcTwo())), footerfont7));
                            taxcell = new PdfPCell(p);
                        }
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneRate()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase2 = new Phrase();
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                        phrase2.add(new Chunk("(", normalcalibri));
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                        phrase2.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase2);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getValue()), normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);
                    }
                } else {
                    if ((counter + 1) == productLimitInCounter) {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT | Rectangle.BOTTOM);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        Phrase phrase2 = new Phrase();
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                        phrase2.add(new Chunk("(", normalcalibri));
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                        phrase2.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase2);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);

                        if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                            taxcell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), normalcalibri));
                        } else {
                            taxcell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), normalcalibri));
                        }
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
                        thirdTable.addCell(taxcell);
                    } else {
                        String strSNO = Integer.toString(sno + 1);

                        taxcell = new PdfPCell(new Phrase(strSNO, normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        taxcell = new PdfPCell(new Phrase("", normalcalibri));
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase1 = new Phrase();
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstValue()), normalcalibri));
                        phrase1.add(new Chunk("(", normalcalibri));
                        phrase1.add(new Chunk(String.valueOf(psItems.get(sno).getCgstPercentage()), normalcalibri));
                        phrase1.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase1);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        Phrase phrase2 = new Phrase();
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstValue()), normalcalibri));
                        phrase2.add(new Chunk("(", normalcalibri));
                        phrase2.add(new Chunk(String.valueOf(psItems.get(sno).getSgstPercentage()), normalcalibri));
                        phrase2.add(new Chunk(")", normalcalibri));

                        taxcell = new PdfPCell(phrase2);
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);

                        if (psItems.get(sno).getValue().compareTo(zero) > 0) {
                            taxcell = new PdfPCell(new Phrase("+" + currency.format(psItems.get(sno).getValue()), normalcalibri));
                        } else {
                            taxcell = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), normalcalibri));
                        }
                        taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        taxcell.setBorder(Rectangle.RIGHT);
                        thirdTable.addCell(taxcell);
                    }
                }
                sno++;
            }
            lineCount = (10 - lineCount);
            for (int i = 0; i < lineCount; i++) {
                taxcell = new PdfPCell(new Phrase(" ", normalcalibri));
                taxcell.setColspan(8);
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                thirdTable.addCell(taxcell);
            }

            document.add(thirdTable);
        }
    }

    private void afterAddProductItemsA5L(Document document) {
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            Phrase p;
            PdfPCell taxcell;

            if (isIGST) {
                PdfPTable thirdTable = new PdfPTable(7);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{5, 30, 11, 15, 9, 15, 15});

                if (billDiscount != 0) {
                    taxcell = new PdfPCell(new Phrase("Discount", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(6);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), boldcalibri));
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                }
                if (billRoundOff != 0) {
                    taxcell = new PdfPCell(new Phrase("Round Off", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(6);
                    thirdTable.addCell(taxcell);

                    if (billRoundOff > 0) {
                        taxcell = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), boldcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(currency.format(billRoundOff), boldcalibri));
                    }
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(6);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(currency.format(billNetAmount), boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    thirdTable.addCell(taxcell);
                }
                document.add(thirdTable);
            } else {
                PdfPTable thirdTable = new PdfPTable(8);
                thirdTable.setWidthPercentage(100);
                thirdTable.setWidths(new float[]{6, 30, 10, 14, 13, 15, 15, 14});

                if (billDiscount != 0) {
                    taxcell = new PdfPCell(new Phrase("Discount", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(7);
                    thirdTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("-" + currency.format(billDiscount), boldcalibri));
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                }

                if (billRoundOff != 0) {
                    taxcell = new PdfPCell(new Phrase("Round Off", boldcalibri));
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setColspan(7);
                    thirdTable.addCell(taxcell);

                    if (billRoundOff > 0) {
                        taxcell = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), boldcalibri));
                    } else {
                        taxcell = new PdfPCell(new Phrase(currency.format(billRoundOff), boldcalibri));
                    }
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    taxcell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.TOP | Rectangle.BOTTOM);
                    thirdTable.addCell(taxcell);

                }

                taxcell = new PdfPCell(new Phrase("Net Amount", boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.TOP);
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setColspan(7);
                thirdTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(currency.format(billNetAmount), boldcalibri));
                taxcell.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
                taxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                thirdTable.addCell(taxcell);

                document.add(thirdTable);

            }

        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PurchaseBill.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void afterAddProductItemsContinuedA5L(Document document) {
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new float[]{70, 30});
            table.setTotalWidth(560);
            table.setWidthPercentage(100);

            PdfPCell cellfooter = null;

            cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.TOP | Rectangle.LEFT);
            table.addCell(cellfooter);

            for (int i = 0; i < 3; i++) {
                cellfooter = new PdfPCell(new Phrase(" ", footerfontbold));
                cellfooter.setColspan(2);
                cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellfooter.setBorder(Rectangle.NO_BORDER);
                cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
                table.addCell(cellfooter);
            }

            cellfooter = new PdfPCell(new Phrase("Continued... ", footerfontbold));
            cellfooter.setColspan(2);
            cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellfooter.setBorder(Rectangle.NO_BORDER);
            cellfooter.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            table.addCell(cellfooter);
            document.add(table);

        } catch (DocumentException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addFooterA5L(Document document) throws DocumentException, IOException {
        BaseFont bf;

        bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
        com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

        PdfPTable Outertable = new PdfPTable(3);
        Outertable.setWidths(new float[]{40, 30, 30});

        Outertable.setTotalWidth(560);
        Outertable.setWidthPercentage(100);

        PdfPTable table = new PdfPTable(1);
        PdfPCell cellfooter = null;

        cellfooter = new PdfPCell(new Phrase(companyFooterHeading, footerfontbold));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineOne, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineTwo, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineThree, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(companyFooterLineFour, footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        cellfooter = new PdfPCell(new Phrase(billRemarks, boldcalibri));
        cellfooter.setRowspan(5);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        table = new PdfPTable(1);

        Phrase p2 = new Phrase();
        p2.add(new Chunk("for ", cellFont1));
        p2.add(new Chunk(companyName.toUpperCase(), footerfontbold));
        cellfooter = new PdfPCell(p2);
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", boldcalibri));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase("", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        cellfooter = new PdfPCell(new Phrase(" ", footerfont));
        cellfooter.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellfooter.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellfooter);

        Outertable.addCell(table);

        document.add(Outertable);

    }

    public void lastLineA5L(Document document, int currentPage, int pageCount) throws DocumentException {
        try {
            BaseFont bf;

            bf = BaseFont.createFont("/fonts/calibri.ttf", "CP1250", BaseFont.EMBEDDED);
            com.itextpdf.text.Font headercalibri = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font boldcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.BOLD);
            com.itextpdf.text.Font normalcalibri = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfont = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.NORMAL);
            com.itextpdf.text.Font footerfontbold = new com.itextpdf.text.Font(bf, 8, com.itextpdf.text.Font.BOLD);

            PdfPTable footerTable = new PdfPTable(1);
            footerTable.setWidthPercentage(100);

            Phrase p;
            PdfPCell cell;

            p = new Phrase(" ", headercalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);
            document.add(footerTable);

            footerTable = new PdfPTable(2);
            footerTable.setWidths(new float[]{70, 30});
            footerTable.setWidthPercentage(100);

            p = new Phrase(" ", normalcalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);

            p = new Phrase("Page " + currentPage + " of " + pageCount, normalcalibri);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            footerTable.addCell(cell);

//        p = new Phrase("This is a Computer Generated Invoice", cellFont);
//        cell = new PdfPCell(p);
//        cell.setColspan(2);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setBorder(Rectangle.NO_BORDER);
//        footerTable.addCell(cell);
            document.add(footerTable);

        } catch (IOException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createPdfA5L(Boolean isPrint, Boolean isPreview) throws IOException, DocumentException {
        try {

            Rectangle layout = new Rectangle(PageSize.A5.rotate());
            Document document = new Document(layout, 20, 15, 15, 15);
            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Purchase Invoice" + "-" + newPS.getPurchaseInvoiceNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Purchase Invoice" + "-" + newPS.getPurchaseInvoiceNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
//                for (int copies = 0; copies < noOfCopies; copies++) {
//                    sno = 0;
//                    document.newPage();
//                    String invoiceCopyName = null;
//                    switch (copies) {
//                        case 0:
//                            invoiceCopyName = firstBillCopy;
//                            break;
//                        case 1:
//                            invoiceCopyName = secondBillCopy;
//                            break;
//                        case 2:
//                            invoiceCopyName = thirdBillCopy;
//                            break;
//                        case 3:
//                            invoiceCopyName = fourthBillCopy;
//                            break;
//                        case 4:
//                            invoiceCopyName = fifthBillCopy;
//                            break;
//                        default:
//                            break;
//                    }

                int pagecount = 0, remainingProduct = 0;
                pagecount = (productcount / productLimitA5L);
                if ((productcount % productLimitA5L) > 0) {
                    pagecount += 1;
                }
                if (pagecount == 1) {
                    addInfoA5L(document, "Purchase");
                    addProductItemsA5L(document, (productcount % productLimitA5L));
                    afterAddProductItemsA5L(document);
                    addFooterA5L(document);
                    lastLineA5L(document, pagecount, pagecount);
                } else {
                    for (int i = 1; i <= pagecount; i++) {
                        if (pagecount == i) {
                            remainingProduct = productcount - (remainingProduct * productLimitA5L);
                            addInfoA5L(document, "Purchase");
                            addProductItemsA5L(document, remainingProduct);
                            afterAddProductItemsA5L(document);
                            addFooterA5L(document);
                            lastLineA5L(document, i, pagecount);
                        } else {
                            addInfoA5L(document, "Purchase");
                            addProductItemsA5L(document, productLimitA5L);
                            afterAddProductItemsContinuedA5L(document);
                            lastLineA5L(document, i, pagecount);
                            document.newPage();
                            remainingProduct++;
                        }
                    }
                }
//                }
                document.close();
                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);

                    } catch (PrintException ex) {
                        Logger.getLogger(PurchaseBill.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);

                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createImagePdf(String filePath) throws IOException {
        try {
            String path = AppConstants.getTempDocPath();
            File pdfFile;

            pdfFile = new File(filePath);
            PDDocument doc = PDDocument.load(new File(filePath));
            int count = doc.getNumberOfPages();
            for (int i = 0; i < count; i++) {
                RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
                FileChannel channel = raf.getChannel();
                MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                PDFFile pdf = new PDFFile(buf);

                PDFPage page = pdf.getPage(i);

                java.awt.Rectangle rect = new java.awt.Rectangle(0, 0, (int) page.getBBox().getWidth(),
                        (int) page.getBBox().getHeight());
                BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
                        BufferedImage.TYPE_INT_RGB);

                Image image = page.getImage(rect.width, rect.height, // width & height
                        rect, // clip rect
                        null, // null for the ImageObserver
                        true, // fill background with white
                        true // block until drawing is done
                );
                Graphics2D bufImageGraphics = bufferedImage.createGraphics();
                bufImageGraphics.drawImage(image, 0, 0, null);
                ImageIO.write(bufferedImage, "PNG", new File(path + "bill" + i + ".png"));
                raf.close();
            }
            random++;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(PurchaseBill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createPdfA7(Boolean isPrint, Boolean isPreview) throws FileNotFoundException, DocumentException {
        try {
            Rectangle layout = new Rectangle(PageSize.A7);
            Document document = new Document(layout, 15, 15, 15, 15);

            String path = "";
            Boolean isApproved = false;
            if (isPrint) {
                path = AppConstants.getTempDocPath() + "Purchase Invoice" + "-" + newPS.getPurchaseInvoiceNo() + ".pdf";
                isApproved = true;
            } else {
                JFrame parentFrame = new JFrame();
                com.alee.laf.WebLookAndFeel.install();
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Windows".equals(info.getName())) {
                        try {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());

                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(PurchaseBill.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (InstantiationException ex) {
                            Logger.getLogger(PurchaseBill.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(PurchaseBill.class
                                    .getName()).log(Level.SEVERE, null, ex);

                        } catch (UnsupportedLookAndFeelException ex) {
                            Logger.getLogger(PurchaseBill.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Download Bill");
                fileChooser.setSelectedFile(new File("Purchase Invoice" + "-" + newPS.getPurchaseInvoiceNo()));
                int userSelection = fileChooser.showSaveDialog(parentFrame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    path = fileToSave.getAbsolutePath() + ".pdf";
                    isApproved = true;
                }
            }
            if (isApproved) {
                FileOutputStream fos = new FileOutputStream(path);
                PdfWriter.getInstance(document, fos);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();

                Phrase p;

                com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK);
                com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);
                com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 7, BaseColor.BLACK);

                PdfPTable taxTable = new PdfPTable(1);
                taxTable.setWidthPercentage(100);
                PdfPCell taxcell = null;

                taxcell = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase(companyName, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                if (!companyA7AddrLine1.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine1, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine2.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine2, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else if (!companyA7AddrLine3.equalsIgnoreCase("")) {
                    taxcell = new PdfPCell(new Phrase(companyA7AddrLine3, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);

                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                } else {
                    taxcell = new PdfPCell(new Phrase("City : " + companyCity, cellFont));
                    taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    taxcell.setBorder(Rectangle.NO_BORDER);
                    taxTable.addCell(taxcell);
                }

                taxcell = new PdfPCell(new Phrase("State : " + companyState + "  Code : " + StateCode.getStateCode(companyState), cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);

                taxcell = new PdfPCell(new Phrase("GSTIN/UIN : " + companyGSTIN, cellFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxTable.addCell(taxcell);
                //document.add(taxTable);

                taxcell = new PdfPCell(new Phrase(invoiceHeading, boldFont));
                taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                taxcell.setBorder(Rectangle.NO_BORDER);
                taxcell.setBorder(Rectangle.BOTTOM);
                taxTable.addCell(taxcell);

                document.add(taxTable);

                PdfPTable table2 = new PdfPTable(2);
                table2.setWidthPercentage(100);
                PdfPCell tablecell = null;

                tablecell = new PdfPCell(new Phrase("No: " + billNo, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                table2.addCell(tablecell);

                tablecell = new PdfPCell(new Phrase(billDate, reportdateFont));
                tablecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablecell.setBorder(Rectangle.NO_BORDER);
                table2.addCell(tablecell);

                document.add(table2);

                PdfPTable table3 = new PdfPTable(5);
                table3.setWidthPercentage(100);
                table3.setWidths(new float[]{7, 40, 20, 14, 25});
                PdfPCell cell3 = null;

                cell3 = new PdfPCell(new Phrase(" ", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Description", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Rate", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                //tablecell.setBorder(Rectangle.TOP | Rectangle.LEFT | Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Qty", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("Amount", reportdateFont));
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setBorder(Rectangle.BOTTOM);
                table3.addCell(cell3);

                for (int count = 0; count < psItems.size(); count++) {

                    String strSNO = Integer.toString(sno + 1);
                    if (!psItems.get(sno).getProduct().isService()) {
                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        int UQC2Value = 0;
                        for (Product product : products) {
                            if (product.getName().equals(psItems.get(sno).getProductName())) {
                                UQC2Value = product.getUQC2Value();
                            }
                        }
                        int UQC1qty = psItems.get(sno).getUqcOneQuantity(), UQC2qty = psItems.get(sno).getUqcTwoQuantity();
                        String strQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcOne()), strAltQuantity = "0 " + getUQCcode(psItems.get(sno).getUqcTwo());
                        if (UQC1qty > 0) {
                            strQuantity = UQC1qty + " " + getUQCcode(psItems.get(sno).getUqcOne());
                            strAltQuantity = (UQC1qty * UQC2Value) + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            if (UQC2qty > 0) {
                                strQuantity = strQuantity + ", ";
                                strAltQuantity = strAltQuantity + ", ";
                            }
                        }
                        if (UQC2qty > 0) {
                            if (UQC1qty == 0) {
                                strQuantity = "";
                                strAltQuantity = "";
                            }
                            strQuantity = strQuantity + UQC2qty + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                            strAltQuantity = strAltQuantity + psItems.get(sno).getUqcTwoQuantity() + " " + getUQCcode(psItems.get(sno).getUqcTwo());
                        }

                        cell3 = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getUqcOneRate()), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                        Phrase phrase = new Phrase();
                        int isEqual = psItems.get(sno).getUqcOne().compareTo(psItems.get(sno).getUqcTwo());
                        if (isEqual == 0) {
                            cell3 = new PdfPCell(new Phrase(String.valueOf(psItems.get(sno).getUqcOneQuantity()), cellFont));
                        } else {
                            phrase.add(new Chunk(strQuantity, cellFont));
                            phrase.add(new Chunk(Chunk.NEWLINE + strAltQuantity, cellFont));
                            cell3 = new PdfPCell(phrase);
                        }

                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                    } else {
                        cell3 = new PdfPCell(new Phrase(strSNO, cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(psItems.get(sno).getProductName(), cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(" ", cellFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table3.addCell(cell3);

                        cell3 = new PdfPCell(new Phrase(currency.format(psItems.get(sno).getValue()), boldFont));
                        cell3.setBorder(Rectangle.NO_BORDER);
                        cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(cell3);
                    }
                    sno++;
                }
                cell3 = new PdfPCell(new Phrase("Total Amount ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(billTotal), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("GST Amount ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(billTotalTax), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                if (billDiscount != 0) {
                    cell3 = new PdfPCell(new Phrase("Discount", cellFont));
                    cell3.setColspan(4);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(cell3);

                    cell3 = new PdfPCell(new Phrase("-" + currency.format(billDiscount), boldFont));
                    cell3.setBorder(Rectangle.NO_BORDER);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);

                }

                if (billRoundOff != 0) {
                    cell3 = new PdfPCell(new Phrase("Round off", cellFont));
                    cell3.setColspan(4);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(cell3);

                    if (billRoundOff > 0) {
                        cell3 = new PdfPCell(new Phrase("+" + currency.format(billRoundOff), boldFont));
                    } else {
                        cell3 = new PdfPCell(new Phrase(currency.format(billRoundOff), boldFont));
                    }
                    cell3.setBorder(Rectangle.NO_BORDER);
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                    table3.addCell(cell3);
                }

                cell3 = new PdfPCell(new Phrase("NET AMOUNT ", cellFont));
                cell3.setColspan(4);
                cell3.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(currency.format(billNetAmount), boldFont));
                cell3.setBorder(Rectangle.NO_BORDER | Rectangle.BOTTOM | Rectangle.TOP);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                document.add(table3);

//                addHSNTableA7(document, hsnno, hsncount);
                table3 = new PdfPTable(5);
                table3.setWidthPercentage(100);
                table3.setWidths(new float[]{7, 40, 20, 14, 25});

                cell3 = new PdfPCell(new Phrase("RS. " + currency.format(billNetAmount), boldFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase(" ", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);

                cell3 = new PdfPCell(new Phrase("absabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsabsab", cellFont));
                cell3.setColspan(5);
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table3.addCell(cell3);
                document.add(table3);

                document.close();

                fos.close();
                if (isPrint) {
                    try {
                        pdfPrint(path);

                    } catch (PrintException ex) {
                        Logger.getLogger(PurchaseBill.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            File myFile = new File(path);
                            Desktop.getDesktop().open(myFile);
                        } catch (IOException ex) {
                            // no application registered for PDFs
                        }
                    }
                }
                if (isPreview) {
                    createImagePdf(path);

                }
            }
            com.alee.laf.WebLookAndFeel.install();
        } catch (IOException | DocumentException ex) {
            Logger.getLogger(A7Bill.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pdfPrint(String path) throws PrintException, IOException {
        PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
        DocPrintJob printJob = printService.createPrintJob();
        PDDocument pdDocument = PDDocument.load(new File(path));
        PDFPageable pdfPageable = new PDFPageable(pdDocument);
        SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);
        printJob.print(doc, null);
        pdDocument.close();
    }
}
