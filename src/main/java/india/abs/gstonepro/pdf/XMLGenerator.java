/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import india.abs.gstonepro.api.models.Transaction;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import jxl.Workbook;
import jxl.write.WritableWorkbook;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author DELL
 */
public class XMLGenerator {

    public void createXmlUploadFile(List<Transaction> trans) {
        try {
            DocumentBuilderFactory DBFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder DBuilder = DBFactory.newDocumentBuilder();
            Document doc = DBuilder.newDocument();

            Element rootElement = doc.createElement("ENVELOPE");
            doc.appendChild(rootElement);

            // supercars element
            Element headerElement = doc.createElement("HEADER");
            rootElement.appendChild(headerElement);

            // setting attribute to element
            Element tallyRequest = doc.createElement("TALLYREQUEST");
            tallyRequest.appendChild(doc.createTextNode("Import Data"));
            headerElement.appendChild(tallyRequest);

            Element bodyElement = doc.createElement("BODY");
            rootElement.appendChild(bodyElement);

            Element importData = doc.createElement("IMPORTDATA");
            bodyElement.appendChild(importData);

            Element requestDesc = doc.createElement("REQUESTDESC");
            importData.appendChild(requestDesc);

            Element reportName = doc.createElement("REPORTNAME");
            reportName.appendChild(doc.createTextNode("All Masters"));
            requestDesc.appendChild(reportName);

            Element staticVariables = doc.createElement("STATICVARIABLES");
            requestDesc.appendChild(staticVariables);

            Element attrCmpny = doc.createElement("SVCURRENTCOMPANY");
            attrCmpny.appendChild(doc.createTextNode(""));
            staticVariables.appendChild(attrCmpny);

            Element requestData = doc.createElement("REQUESTDATA");
            importData.appendChild(requestData);

            Element tallyMsg = doc.createElement("TALLYMESSAGE");
            tallyMsg.setAttribute("xmlns:UDF", "TallyUDF");
            requestData.appendChild(tallyMsg);

            Element voucher = doc.createElement("VOUCHER");
            Attr vcType = doc.createAttribute("VCHTYPE");
            vcType.setValue("");
            voucher.setAttributeNode(vcType);

            Attr vcAction = doc.createAttribute("ACTION");
            vcAction.setValue("");
            voucher.setAttributeNode(vcAction);

            String fileName = "Day Book XML Report ";
            JFrame parentFrame = new JFrame();
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    try {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    } catch (UnsupportedLookAndFeelException ex) {
                        Logger.getLogger(XMLGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(XMLGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                        Logger.getLogger(XMLGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(XMLGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Download Day Book XML Report");
            fileChooser.setSelectedFile(new File(fileName));
            int userSelection = fileChooser.showSaveDialog(parentFrame);
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                String xmlpath = fileToSave.getAbsolutePath() + ".xml";

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(xmlpath));
                transformer.transform(source, result);

                // Output to console for testing
                StreamResult consoleResult = new StreamResult(System.out);
                transformer.transform(source, consoleResult);

                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File(xmlpath);
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException ex) {
                        // no application registered for PDFs
                    }
                }
            }
        } catch (ParserConfigurationException | TransformerException ex) {
            Logger.getLogger(XMLGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
