/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package india.abs.gstonepro.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author user
 */
public class Ewaybill {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws com.itextpdf.text.DocumentException
     */
    public static void main(String[] args) throws FileNotFoundException, DocumentException, IOException {
        // TODO code application logic here
        Ewaybill example = new Ewaybill();
        example.createEX();
    }

    public void createEX() throws FileNotFoundException, DocumentException, IOException {
        String Qr = "12345789";
        OutputStream filevk = new FileOutputStream(new File("C:\\Users\\Acer\\Desktop\\New folder\\ewaybill.pdf"));
        Rectangle layout = new Rectangle(PageSize.A4);
        Document doc = new Document(layout, 50, 50, 50, 50);
        PdfWriter.getInstance(doc, filevk);
        doc.open();
        Phrase p;
        BaseFont bf;
        //             bf = BaseFont.createFont("C:\\Windows\\fonts\\bapc___.ttf","CP1250",BaseFont.EMBEDDED);
        //            com.itextpdf.text.Font tamil = new com.itextpdf.text.Font(bf, 14, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font boldFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font reportdateFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font totalFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, com.itextpdf.text.Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font companyCellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
        com.itextpdf.text.Font cellFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.BLACK);

        PdfPTable headerTable1 = new PdfPTable(1);
        headerTable1.setWidthPercentage(100);
        PdfPCell headercell1 = null;
        headercell1 = new PdfPCell(new Phrase("\n \n \n \n e-Way Bill", boldFont));
        headercell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        headercell1.setBorder(Rectangle.NO_BORDER);
        headerTable1.addCell(headercell1);

//              Phrase phrase2 = new Phrase();
//              phrase2.add(new Chunk("\n", boldFont));
//              //phrase2.add(new Chunk(" Page", cellFont));
//              headercell1 = new PdfPCell(phrase2);
//              headercell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//              headercell1.setBorder(Rectangle.NO_BORDER);
        //headerTable1.addCell(headercell1);
        doc.add(headerTable1);

        PdfPTable Table2 = new PdfPTable(1);
        Table2.setWidthPercentage(100);
        PdfPCell tablecell2 = null;

        BarcodeQRCode my_code = new BarcodeQRCode(Qr, 100, 100, null);
        Image qr_image = my_code.getImage();
        qr_image.setAlignment(Element.ALIGN_CENTER);
        doc.add(qr_image);

        PdfPTable Table3 = new PdfPTable(2);
        // Table3.setWidthPercentage(100);
        Table3.setWidths(new float[]{70, 30});
        PdfPCell tablecell3 = null;

        Phrase phrase3 = new Phrase();
        phrase3.add(new Chunk(Chunk.NEWLINE + "E-Way Bill NO:\n", boldFont));
        //phrase3.add(new Chunk(Chunk.NEWLINE +"\n",boldFont));
        tablecell3 = new PdfPCell(phrase3);
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        Phrase phrase3A = new Phrase();
//              phrase3A.add(new Chunk(" ",boldFont));
        phrase3A.add(new Chunk(Chunk.NEWLINE + Qr, boldFont));
//              phrase3A.add(new Chunk(Chunk.NEWLINE +"\n",boldFont));
        tablecell3 = new PdfPCell(phrase3A);
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nE-Way Bill Date:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n13/03/2018\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nGenerated By:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\ntalento abs\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nGSTIN of Receipent:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nasdfghjkl\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nPlace of Delivery:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nxyz\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nInvoice No:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n12356787\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nInvoice Date:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n15/11/1989\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nBill Vehicle Name:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nashok leyland\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nBill Vehicle NO:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nTN 123\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nBill Transport Document:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n---------\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nBill Transport Date:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n10/10/1998\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\nReason for Transportation:\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        tablecell3 = new PdfPCell(new Phrase("\n----------\n", boldFont));
        tablecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        tablecell3.setBorder(Rectangle.NO_BORDER);
        Table3.addCell(tablecell3);

        doc.add(Table3);

        doc.close();

    }
}
