### GST ONE PRO ###

### What is this repository for? ###

This repository is the manufacturing unit for what it is going to be a ground breaking software which is going to help crores of Indians.

### How do I get set up? ###

Install Java (Minimum 1.8)
Install Maven (3.5.0)
Get either netbeans or eclispe.
Clone the repo.
Import the project, clean and build, RUN.

### Contribution guidelines ###

Certain people associated with Alpha Business Pvt.Ltd can contribute to this repository.

### Who do I talk to? ###

Sivaguru Srinivas P
CTO,
Alpha Business Solutions Private Limited,
7598889688,
abs.nellai@gmail.com.